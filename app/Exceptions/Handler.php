<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\Config;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $e
     * @return void
     */
    public function report(Exception $e)
    {
        parent::report($e);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $e
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $e)
    {
        return parent::render($request, $e);
        // if(auth()->check() && auth()->user()->isDeveloper())
        // {
        //     Config::set('app.debug', true);
        //     return parent::render($request, $e);
        // }else{
        //     Config::set('app.debug', false);
        //     if($e instanceof HttpException){
        //         $status = $e->getStatusCode();

        //         if($status == 404)
        //         {
        //             return response()->view('errors.404');
        //         }
        //     }
        //     else{
        //         return view('errors.503');
        //     }
        // }
    }
}
