<?php

return [
    // The label is used to represent the module's name
    'label' => 'definitions::backend.module-label',

    // Permissions are organized by groups
    // The level 1 key denotes the group name (label)
    // The level 1 value is an associative array
    // The level 2 key denotes the permission name (label)
    // The level 2 value is an array with the routes that requires the permission
    'permissions' => [
        'definitions::backend.permission.definitions.see' => [
            'definitions::backend.permission.definitions.see' => [
                'definitions::backend::definitions.index',
                'definitions::backend::definitions.add_more_conditions',
            ],
        ],
        'definitions::backend.permissions.definitions.create' => [
            'definitions::backend.permissions.definitions.create' => [
                'definitions::backend::definitions.store',
            ],
        ],
    ],

    // Menu items denote the menus that this module renders
    'menu-items' => [
        [
            // Label for the menu item (mandatory)
            'label' => 'definitions::backend.menu.definitions',
            // Route for the action (mandatory if the menu has no childs)
            'route' => 'definitions::backend::definitions.index',
            // Css class can be used to render the icon
            'css-class' => 'icon-icon-lateral-gestao-permissoes icon15menu',
            // Unique html id for the item
            'css-id' => null,
            // list of submenus
            'sub-menus' => [],
        ],
    ],
    // List of required providers for the module to work
    'providers' => [
        Brainy\Definitions\Providers\DefinitionsServiceProvider::class,
    ],

    // List of facades used by the module
    'aliases' => [
        // 'Recaptcha' => Greggilbert\Recaptcha\Facades\Recaptcha::class,
    ],

    // List of route middleware to register
    'middleware' => [
        //'acl' => \Brainy\DefinitionsRent\Middleware\SomeMiddleware::class,
    ],

    'commands' => [
        // \Brainy\DefinitionsRent\Commands\SomeCommand::class,
    ],

];
