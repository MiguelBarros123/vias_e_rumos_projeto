<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 24/05/2016
 * Time: 18:18
 */

namespace Brainy\Permissions\Repositories;



use Brainy\Framework\Models\Group;
use Brainy\Framework\Models\UserDetail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Lang;

class GroupRepository implements GroupRepositoryInterface
{

    private $group;

    public function _construct(Group $group){
        $this->group = $group;
    }


    public function destroyGroups($groups)
    {

        $has_users=collect();
        $sucessMessage=collect();
        $messages['error']="";
        $messages['success']="";



        foreach ($groups as $group) {
            $group = Group::findOrFail($group);
            $name=$group->name;
            $users = $group->users();


            if($users->count() != 0){
                $has_users->push($name);

            }else{
                Group::destroy($group->id);
                $sucessMessage->push($name);

            }
        }

        if($has_users->count()>0){
            $messages['error']=trans('permissions::backend.group.index.delete_group_users_error', ['grupo'=>implode(',',$has_users->toArray())]);
        }

        if($sucessMessage->count()>0){
            $messages['success']=trans('permissions::backend.group.index.delete_group_sucess', ['grupo'=>implode(',',$sucessMessage->toArray())]);
        }




        return $messages;
    }

    public function changeGroupForUsers($group, $users,$id)
    {
        DB::transaction(function () use($group,$users,$id) {
        foreach ($users as $usr) {
            UserDetail::where('id',$usr)->update(array('group_id'=>$group));
        }
            Group::findOrFail($id)->update(['updated_by'=>auth()->user()->email]);
            Group::findOrFail($group)->update(['updated_by'=>auth()->user()->email]);

        });
    }

    public function updateUserGroup($group, $user)
    {
        UserDetail::where('user_id','=',$user)->update(array('group_id'=>$group));
    }

    public function storeGroup($group)
    {
        $dados = array_except($group, '_token');
        $dados['created_by'] = auth()->user()->email;
        $dados['updated_by'] = auth()->user()->email;

        DB::transaction(function () use($dados,$group) {
        $grupo = Group::create($dados);

        foreach($group['permissoes'] as $permition){
            $grupo->permissions()->create(['permission'=>$permition,'type'=>$permition]);
        }

        });




    }
}