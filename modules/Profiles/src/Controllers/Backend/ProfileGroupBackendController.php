<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-10-2016
 * Time: 14:48
 */

namespace Brainy\Profiles\Controllers\Backend;
use App\User;
use Brainy\Cms\Models\FrontUser;
use Brainy\Cms\Models\MediaItem;
use Brainy\DefinitionsEcommerce\Models\Country;
use Brainy\Framework\Controllers\BackendController;
use Brainy\Framework\Models\Group;
use Brainy\Framework\Models\UserDetail;
use Brainy\Profiles\Models\Profile;
use Brainy\Profiles\Models\ProfileGroup;
use Brainy\Profiles\Repositories\ProfileRepositoryInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Http\Request;

class ProfileGroupBackendController extends BackendController{


    protected $profileRepo;

    public function __construct(ProfileRepositoryInterface $profileRepo)
    {
        parent::__construct();
        $this->profileRepo = $profileRepo;
    }

    public function index()
    {
        return view('profiles::backend.groups.index');
    }

    public function index_ajax()
    {

        return Datatables::of(ProfileGroup::orderBy('created_at','asc')->get())
            ->addColumn('name', function ($client) {

                return $client->name;

            })
            ->addColumn('total', function ($client) {

                $profiles = $client->profiles()->count();

                return $profiles;

            })
            ->addColumn('action', function ($client) {


                $delete_route = route('profiles::backend::profiles.delete_multiple_groups');
                $edit_route = route('profiles::backend::profiles.group.edit',[$client->id]);
                $view_route = route('profiles::backend::profiles.group.view',[$client->id]);

                if($client->profiles()->count()){
                    $impossible = 1;
                } else {
                    $impossible = 0;
                }


                $string = '
                        <div class="btn-group pull-right">
                           <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a  class="open_edit_atrribute_modal" href="'.$view_route.'" ><i class="icon-visivel"></i>  '.trans("profiles::backend.common.view").'</a></li>
                            <li><a  class="open_edit_atrribute_modal" href="'.$edit_route.'" ><i class="icon-editar"></i>  '.trans("profiles::backend.common.edit").'</a></li>
                            <li><a data-impossible-delete="'.$impossible.'" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="'. $client->id .'" data-route="'.$delete_route.'"><i class="icon-opcoes-eliminar"></i> '.trans("profiles::backend.common.delete").' </a></li>
                          </ul>
                        </div>

                       ';
                return $string;

            })
            ->addColumn('rota_final', function ($client) {
                return route('profiles::backend::profiles.group.view',[$client->id]);
            })
            ->make(true);
    }



    public function edit($id){
        $group=ProfileGroup::findOrFail($id);

        return view('profiles::backend.groups.edit',compact('group'));
    }

    public function view($id){
        $profile=Profile::findOrFail($id);
        return view('profiles::backend.profile.view',compact('profile'));
    }

    public function viewGroup($id){
        $group=ProfileGroup::findOrFail($id);
        return view('profiles::backend.groups.view',compact('group'));
    }

    public function create()
    {
        $profiles = Profile::doesntHave('profile_group')->get();
        return view('profiles::backend.groups.create', compact('profiles'));
    }

    public function store(Request $request){


        $group = DB::transaction(function () use ($request) {
            $p=ProfileGroup::create($request->input());
            if($request->company_users) {
                foreach ($request->company_users as $user){
                    $p->profiles()->save(Profile::findOrFail($user));
                }
            }

            return $p;
        });

        if($request->submit === 'stay'){
            return redirect()->route('profiles::backend::profiles.group.edit',[$group->id])->with('sucess-message', trans('profiles::backend.messages.success.create_group'));
        } else {
            return redirect()->route('profiles::backend::profiles.group.index')->with('sucess-message', trans('profiles::backend.messages.success.create_group'));
        }

    }

    public function update(Request $request,$id){

        $p=ProfileGroup::findOrFail($id);
        DB::transaction(function () use ($request,$id,$p) {
            $p->update($request->input());
            foreach ($p->profiles as $profile){
                $profile->profile_group()->dissociate();
                $profile->save();
            }
            if($request->company_users){
                foreach ($request->company_users as $user){
                    $p->profiles()->save(Profile::findOrFail($user));
                }
            }

        });

        if($request->submit === 'stay'){
            return redirect()->back()->with('sucess-message', trans('profiles::backend.messages.success.update_group'));
        } else {
            return redirect()->route('profiles::backend::profiles.group.index')->with('sucess-message', trans('profiles::backend.messages.success.update_group'));
        }


    }


    public function getPersonsNoGroup($me=null){

        $users = UserDetail::where('flags', 3)->pluck('id');

        if($me!=null){
            return Profile::doesntHave('profile_group')->whereNotIn('user_id', $users)->orWhere('type', 'company')->get()->toJson();
        }
        return Profile::doesntHave('profile_group')->whereNotIn('user_id', $users)->orWhere('type', 'company')->get()->toJson();
    }


    public function deleteMultipleGroups(Request $request)
    {

        foreach(explode(',',$request->deleted_rows) as $group){

            $p = ProfileGroup::findOrFail($group);

            if($p->profiles()->count()){
                return redirect()->back()->with('error-message', trans('profiles::backend.messages.error.delete_groups', ['name' => $p->name]));
            }


            $p->delete();

        }

        return redirect()->back()->with('sucess-message', trans('profiles::backend.messages.success.delete_groups'));

    }

    public function newProfile(Request $request)
    {

//        dd($request->except('_token'));

        if(array_key_exists('is_user', $request->input())){

            $validator = Validator::make($request->all(), [
                'profile_email' => 'required_if:is_user,on|unique:users,email',
                'profile_password' => 'required_if:is_user,on|min:8'
            ]);

            $data['type'] = 'error';

            if ($validator->fails()) {
                $data['message'] = trans('profiles::backend.messages.error.email_password_required');
                return $data;
            }

            if(array_key_exists('profile_email',$request->input())){
                $user = User::where('email', $request->profile_email)->first();
                if($user){
                    $data['message'] = trans('profiles::backend.messages.error.existing_user');
                    return $data;
                }
            }
        }

        $profile = $this->profileRepo->addProfile($request);

        $data['type'] = 'success';
        $data['message'] = trans('profiles::backend.messages.success.saved_profile');
        $data['profile'] = $profile;

        return $data;

    }


}
