<div id="biblio-menu">
    <div class="biblio-search-list-wrap hidden-xs hidden-sm">
        <ul class="biblio-search-list">
            <li>
                <a href="{{ route('cms::frontend::model.biblioteca') }}">Histórico</a>
            </li>
            
            <!-- @foreach($menus as $key => $menu)
            <li>
                <div class="dropdown">
                    <span  class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{$menu->name}}</span>
                    <div class="dropdown-menu  @if($key+1 == count($menus) || $key+1 == count($menus)-1 ) dropdown-menu-right @endif dropdown-list-wrap" aria-labelledby="dropdownMenuButton">

                        @if($menu->sub_menus_biblioteca()->count() > 10 && $menu->sub_menus_biblioteca()->count() <=20)
                        <ul class="dropdown-list drop-2">

                        @elseif($menu->sub_menus_biblioteca()->count() > 20 && $menu->sub_menus_biblioteca()->count() <=30)
                            <ul class="dropdown-list drop-3">
                        @elseif($menu->sub_menus_biblioteca()->count() > 30)
                            <ul class="dropdown-list drop-4">
                        @else
                             <ul class="dropdown-list">
                        @endif
                        
                            @foreach($menu->sub_menus_biblioteca()->get() as $sub_menu)
                            <li><a href="{{ route('cms::frontend::model.insolvencia', [$menu->name,$sub_menu->name]) }}" title="Insolvência pessoal">{{$sub_menu->name}}</a></li>
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
             </li>
             @endforeach -->
        </ul>
    </div>
    <div class="mobile-list-bib hidden-md hidden-lg">
        <ul class="biblio-search-list">
            <li>
                <a href="{{ route('cms::frontend::model.biblioteca') }}">Histórico</a>
            </li>
            <li class="open_bib">
                Documentos
            </li>
        </ul>
    </div>
    <div class="search-biblio">
        <div class="search-input search-input-biblio">
            <input placeholder="PESQUISA" />
        </div>
    </div>

    <div class="mobile-articles-wrap hidden-md hidden-lg">
        <div class="voltar_bib close_bib">
            < voltar
        </div>
        <ul class="first-menu">
         <!-- @foreach($menus as $key => $menu)
            <li>
                <div class="">
                    <span  class="men-title" data-toggle="" aria-haspopup="" aria-expanded="">{{$menu->name}}</span>
                    <div class="" aria-labelledby="">
                        <ul class="second-menu">

                            @foreach($menu->sub_menus_biblioteca()->get() as $sub_menu)
                            <li><a href="{{ route('cms::frontend::model.insolvencia', [$menu->name,$sub_menu->name]) }}" title="{{$sub_menu->name}}">{{$sub_menu->name}}</a></li>
                            @endforeach
                            
                        </ul>
                    </div>
                </div>
             </li>
             @endforeach -->
         </ul>
    </div>
</div>

@section('module-scripts')
    <script>
        $('body').on('click', '.open_bib', function(){
            $('.mobile-articles-wrap').addClass('set-bib');
        });

        $('body').on('click', '.close_bib', function(){
            $('.mobile-articles-wrap').removeClass('set-bib');
        });
    </script>
@endsection


