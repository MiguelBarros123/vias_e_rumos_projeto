<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRentDefinitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rent_definitions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('type_value', ['value','percentage'])->default('value');
            $table->double('percentage')->nullable();
            $table->boolean('same_day');
            $table->enum('reservation_deslocation', ['rent','station'])->default('rent');
            $table->integer('min_days');
            $table->integer('num_days');
            $table->string('transactional_email');
            $table->boolean('disable_rent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rent_definitions');
    }
}
