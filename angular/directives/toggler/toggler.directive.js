(function() {
	'use strict';

	function BootstrapTogglerController ($scope, $http) {
		$(function () {
			$scope.$toggler = $('input#' + $scope.togglerId + '.toggle-state[type="checkbox"]');

			$scope.$toggler.bootstrapToggle({
				on: ' ',
				off: ' ',
				size: 'mini',
	            width: '30px',
	            height: '10px',
				style: 'ios'
			});

			$('.toggle-on.ios, .toggle-off.ios').css({
				'border-radius': '18px'
			});

			$('.toggle.ios, .toggle-handle').css({
				'border-radius': '18px'
			});

			(function init() {
				$scope.$toggler.bootstrapToggle($scope.state ? 'on' : 'off');
			})();

			$scope.$watch ('state', function () {
				$scope.$toggler.bootstrapToggle($scope.state ? 'on' : 'off');
			});

		});

		var success = function (response) {
			$scope.state = response.data.state;

			if ($.isFunction($scope.callbackSuccess)) {
				$scope.callbackSuccess(response.data);
			}
		};

		var error = function (response) {
			//
			if ($.isFunction($scope.callbackError)) {
				$scope.callbackError(response.data);
			}
		};

		$scope.fire = function (e) {
			e.stopPropagation();

			if ($scope.url) {

				$http({
					method: 'POST',
					url: $scope.url,
					headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
				}).then(success, error);

			} else {
				success({ data: { state: !$scope.state , id:$scope.togglerId } });
			}
		};
	}

	function link($scope, elements, attributes) {
		$scope.togglerId = attributes.togglerId;
		$scope.url = attributes.url;
		
		if (attributes.windowCallbackSuccess) {
			$scope.callbackSuccess = window[attributes.windowCallbackSuccess];
		}

		if(attributes.windowCallbackError) {
			$scope.callbackError = window[attributes.windowCallbackError];
		}

		$scope.classes_state_0 = attributes.classesState0;
		$scope.classes_state_1 = attributes.classesState1;
		$scope.nome = attributes.togglerName;
	}

	function toggler() {
		return {
			controller: ['$scope', '$http', BootstrapTogglerController],
			scope: {
				callbackSuccess: '&',
				callbackError: '&',
				state: '='
			},
			link: link,
			restrict: 'EA',
			template: '<div class="toggler-container" ng-click="fire($event)">' +
    						'<span class="circle-toggle toggler-holder">' +
								'<i class="{{ classes_state_0 }}" ng-show="state" ng-style="{opacity: 0.5}"></i>' +
								'<i class="{{ classes_state_0 }}" ng-hide="state" ng-style="{opacity: 1}"></i>' +
								'<input id="{{ togglerId }}" class="toggle-state" type="checkbox" name="{{nome}}">' +
								'<i class="{{ classes_state_1 }}" ng-show="state" ng-style="{opacity: 1}"></i>' +
								'<i class="{{ classes_state_1 }}" ng-hide="state" ng-style="{opacity: 0.5}"></i>' +
							'</span>' +
						  '</div>'
		};
	}//info-lock icon-bloqueado icon20 //info-unlock icon-desbloquear icon20

	angular.module('BootstrapToggler', []).directive('bootstrapToggler', toggler);
})();