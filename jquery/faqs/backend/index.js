$(function () {
	'use strict';

    $('li.locale-chooser:first').addClass('active');

    $('#modal-faq li.locale-chooser:first').addClass('active');
    $('#modal-faq div.tab-pane:first').addClass('active');

    $('#modal-theme li.locale-chooser:first').addClass('active');
    $('#modal-theme div.tab-pane:first').addClass('active');
    
    $('#modal-subtheme li.locale-chooser:first').addClass('active');
    $('#modal-subtheme div.tab-pane:first').addClass('active');
});