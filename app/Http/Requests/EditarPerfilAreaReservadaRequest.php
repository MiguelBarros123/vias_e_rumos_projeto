<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EditarPerfilAreaReservadaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstname' => 'required',
            'lastname' => 'required',
            'nif' => 'required',
            'cartao_cidadao' => 'required',
            'data_validade' => 'required',
            'telefone' => 'required',
            'nome_fiscal' => 'required',
            'nipc' => 'required',
            'country' => 'required',
            'address' => 'required',
            'cod_postal' => 'required',
            'local' => 'required',
            'firstname.required' => 'Este campo é obrigatório',
            'lastname.required' => 'Este campo é obrigatório',
            'nif.required' => 'Este campo é obrigatório',
            'cartao_cidadao.required' => 'Este campo é obrigatório',
            'data_validade.required' => 'Este campo é obrigatório',
            'telefone.required' => 'Este campo é obrigatório',
            'nome_fiscal.required' => 'Este campo é obrigatório',
            'nipc.required' => 'Este campo é obrigatório',
            'country.required' => 'Este campo é obrigatório',
            'address.required' => 'Este campo é obrigatório',
            'cod_postal.required' => 'Este campo é obrigatório',
            'local.required' => 'Este campo é obrigatório',
        ];
    }
}
