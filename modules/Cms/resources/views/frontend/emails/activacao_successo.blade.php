<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>{{$request->firstname}} {{$request->lastname}}, agradecemos por se ter registado na Vias & Rumos.</p>
        <div>
            <p>Para aceder à sua conta e poder participar nos nossos leilões, precisamos que confirme o seu utilizador. Para realizar a validação, clique aqui: <a href="{{ route('cms::frontend::model.confirmar_conta' ,$confirmation_code) }}">{{ route('cms::frontend::model.confirmar_conta' ,$confirmation_code) }}</a></p>

            <p>Se recebeu este e-mail por engano, por favor elimine-o. O seu e-mail não será ativado senão confirmar o link acima.</p>
            <p>Para alguma questão, por favor contacte-nos:</p>
            <p>
                <a href="mailto:apoio@viaserumos.pt">apoio@viaserumos.pt</a>
            </p>

            <p>Atenciosamente,</p>
            <p>A equipa Vias & Rumos</p>
        </div>
    </body>
</html>

