
/**
 * Created by User on 22-04-2016.
 *
 * Description: load resources for gallery
 */
(function ($) { "use strict";
    var MediaFinder = function (element, options) {
        this.$el = $(element)
        this.options = options || {}
        this.$el.empty()
        this.init()

    }



    var req;

    MediaFinder.prototype.init = function() {

        var elemento=this.$el;
        var modo_lista=this.options.modo_lista;
        var arquivado=this.options.arquivado;
        var mediafinder=this;
        var token=this.options.token_laravel;
        var caminho_muda_items =this.options.caminho_muda_items;
        var items_atuais=this.options.items;

        var filtro=this.options.onfiltertype;
        var dados=[];
        if(filtro!= null){
            dados.push({'filtro':filtro,'items':this.options.filterObjects});

        }

        $('#loading_folders').fadeIn();

        if(req){
            req.abort();
        }

        req=$.ajax({
            type: "get",
            url: this.options.caminho,
            data:{d:dados},
            headers: {
                'X-CSRF-TOKEN': this.options.token_laravel
            },
            success: function (data) {


                if(data['folders'][0].length == 0 && data['files'][0].length == 0){
                    $('#empty_folder_gallery').fadeIn();
                    $('#loading_folders').fadeOut();
                }else{
                    items_atuais=data;
                    $('#empty_folder_gallery').hide();
                    $('#loading_folders').fadeOut();
                    if(modo_lista==1){

                        mediafinder.adiciona_lista(data,elemento);
                    }else{
                        mediafinder.adiciona_grelha(data,elemento);
                    }
                }




                if(arquivado == 0){
                    $('.item_galery').dblclick(function() {

                        window.location =$(this).attr('data-href');
                        return false;

                    });
                }






                //para arrastar items e mudar a sua localização

                var selected = $([]), offset = {top:0, left:0};

                $( "#selectable_cont .item_galery" ).draggable({
                    opacity: 0.7, helper: "clone",
                    start: function(ev, ui) {
                        if (!$(this).hasClass("ui-selected")){
                            selected = $([]);
                            $("#selectable_cont .item_galery").removeClass("ui-selected");
                            $(this).addClass("ui-selected");
                        }
                    },
                    drag: function(ev, ui) {
                        /*$(".tooltip").show().text($(".ui-selected, .ui-draggable-dragging").length);*/
                    }
                });

                $( "#selectable_cont" ).selectable({
                    filter: ".item_galery",
                    cancel: "a"
                });

                var prev = -1;
                $( "#selectable_cont .item_galery").click(function(e){

                    if (e.ctrlKey){
                        e.metaKey = e.ctrlKey;
                    }


                    if (e.metaKey == false) {

                        var curr = $(this).parent().parent().index();
                        console.log(curr);

                        if(e.shiftKey && prev > -1){
                            $('.modo').slice(Math.min(prev, curr), 1 + Math.max(prev, curr)).children().children().addClass("ui-selecting");
                            prev = -1; // and reset prev

                        }else {
                            prev = curr;
                            $("#selectable_cont .item_galery").removeClass("ui-selected");
                            $(this).addClass("ui-selecting");
                        }
                    }
                    else {
                        prev = curr;
                        if ($(this).hasClass("ui-selected")) {
                            $(this).removeClass("ui-selected");
                        }
                        else {
                            $(this).addClass("ui-selecting");
                        }


                    }
                    $( "#selectable_cont" ).data("ui-selectable")._mouseStop(null);
                });

                $( ".item_galery").contextmenu(function(e){

                    if (!$(this).hasClass("ui-selected")) {
                        $( ".item_galery" ).removeClass("ui-selected");
                        $(this).addClass("ui-selecting");

                    }
                    $( "#selectable_cont" ).data("ui-selectable")._mouseStop(null);
                });

                $( ".pasta_accept" ).droppable({
                    accept: ".ui-selected, .ui-draggable-dragging",
                    activeClass: "ui-state-hover",
                    hoverClass: "ui-state-active selecionado",
                    drop: function( event, ui ) {
                        var $this = $(this);
                        $this.addClass( "ui-state-highlight" );
                        var items={};
                        items.folders=[{}];
                        items.files=[{}];
                        var pasta_final=$(this).attr('data-id');
                        console.log(pasta_final);


                        $(".item_galery.ui-selected").each(function(){

                            if(!$(this).hasClass('ui-draggable-dragging')){
                                if($(this).hasClass('pasta_accept'))
                                {
                                    items.folders.push({'id' : $(this).attr('data-id')});
                                }else{
                                    items.files.push({'id' : $(this).attr('data-id')});
                                }
                            }

                        });

                        $('.menu_options_hidden').hide();
                        $(".ui-selected").parent().parent().remove();


                        $this.removeClass( "ui-state-highlight" );
                        selected = $([]);

                        $.ajax({
                            type: "post",
                            data: {'dados': items, 'pai': pasta_final},
                            url: caminho_muda_items,
                            headers: {
                                'X-CSRF-TOKEN': token
                            },
                            success: function (data) {
                                $('.location_message').fadeIn();
                            },
                            error: function (data) {
                                $('.location_message2').fadeIn();
                                setTimeout(function () {
                                    location.reload();
                                },2000);

                            }
                        });



                    }
                });

            }
        });

    }





    MediaFinder.prototype.adiciona_lista = function(data,elemento) {

        $(data['folders'][0]).each(function(indice,elem){

            elemento.append('' +
                '<div class="col-lg-12 modo"> ' +

                '<div class="container-fluid">' +

                '<div class="item_galery folder_ref pasta_accept row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                '<table class="tabela_list" >'+
                '<tr>'+
                '<td class="folder_icon2 col-lg-1">'+
                '<div class="  text-center "><i class="icon-icon-pasta icon30 "></i></div>' +
                '</td>'+

                '<td class="second_column_folder   fonte_12_lato_light_cinza col-lg-2">'+
                '<p class="title_gallery_list"><i class="icon-pasta pading_right"></i>'+elem.title+'</p>' +
                '<p>'+elem.n_folders+' pastas</p>'+
                '<p>'+elem.n_folders+' ficheiros</p>'+
                '</td>'+

                '<td class="second_column_folder col-lg-3">'+
                '<div class="  fonte_12_lato_light_cinza"><p class="title_gallery_list">Último update</p><p>'+elem.last_update+'</p></div>' +
                '</td>'+



                '<td class="col-lg-3">'+
                '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">tipo</p><p>Pasta de ficheiros</p></div>' +
                '</td>'+


                '<td class="col-lg-3">'+
                '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">Tamanho</p><p></p>'+elem.size+'</div>' +
                '</td>'+


                '</tr>'+
                '</table>'+


                '</div>'+

                '</div>'+
                '</div>');
        });

        $(data['files'][0]).each(function(indice,elem){


            switch(elem.type){

                case 'imagem':

                    elemento.append('<div class="col-lg-12 modo"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'"  data-id="'+elem.id+'">' +

                        '<table class="tabela_list" >'+
                        '<tr>'+
                        '<td class="folder_icon2 image_td col-lg-1">'+
                        '<div style="background: url('+elem.imagem+')no-repeat;" class=" image_galery3"></div>' +
                        '</td>'+

                        '<td class="second_column_folder   fonte_12_lato_light_cinza col-lg-2">'+
                        '<p class="title_gallery_list"><i class="icon-pasta pading_right"></i>'+elem.title+'</p>' +
                        '<p>'+elem.resolution+'px</p>'+
                        '</td>'+

                        '<td class="second_column_folder col-lg-3">'+
                        '<div class="  fonte_12_lato_light_cinza"><p class="title_gallery_list">Último update</p><p>'+elem.last_update+'</p></div>' +
                        '</td>'+

                        '<td class="col-lg-3">'+
                        '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">tipo</p><p>'+elem.format+'</p></div>' +
                        '</td>'+
                        '<td class="col-lg-3">'+
                        '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">Tamanho</p><p></p>'+elem.size+'</div>' +
                        '</td>'+

                        '</tr>'+



                        '</table>'+



                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                default :


                    elemento.append('<div class="col-lg-12 modo"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                        '<table class="tabela_list" >'+
                        '<tr>'+
                        '<td class="col-lg-1">'+
                        '<div class="  text-center "><i class="'+elem.imagem+' icon30 "></i></div>' +
                        '</td>'+


                        '<td class="second_column_folder   fonte_12_lato_light_cinza col-lg-2">'+
                        '<p class="title_gallery_list"><i class="icon-pasta pading_right"></i>'+elem.title+'</p>' +
                        '</td>'+

                        '<td class="second_column_folder col-lg-3">'+
                        '<div class="  fonte_12_lato_light_cinza"><p class="title_gallery_list">Último update</p><p>'+elem.last_update+'</p></div>' +
                        '</td>'+

                        '<td class="col-lg-3">'+
                        '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">tipo</p><p>'+elem.format+'</p></div>' +
                        '</td>'+
                        '<td class="col-lg-3">'+
                        '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">Tamanho</p><p></p>'+elem.size+'</div>' +
                        '</td>'+

                        '</tr>'+



                        '</table>'+

                        '</div>'+

                        '</div>'+
                        '</div>');

                    break;

            }


        });


    }

    MediaFinder.prototype.adiciona_grelha = function(data,elemento) {
        $(data['folders'][0]).each(function(indice,elem){

            elemento.append('' +
                '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +



                '<div class="container-fluid">' +




                '<div class="item_galery folder_ref row pasta_accept"  data-id="'+elem.id+'" data-href="'+elem.rota+'">' +


                '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center parent">' +
                '<i class="icon-icon-pasta icon80 child"></i>'+
                '</div>' +

                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"><i class="icon-icon-peq-pasta icon10"></i>'+elem.title+' </div>' +
                '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right fonte_12_lato_light_cinza">'+elem.size+'</div>' +


                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fonte_12_lato_light_cinza no_padding">'+
                '<div class="separacao"></div>' +
                '<div class="text-center foot_gallery_icon fonte_12_lato_light_cinza">'+elem.n_folders+' pastas</div>' +
                '<div class="text-center foot_gallery_icon2 fonte_12_lato_light_cinza">'+elem.n_files+' ficheiros</div>' +
                '</div>' +


                '</div>'+

                '</div>'+
                '</div>');
        });

        $(data['files'][0]).each(function(indice,elem){

console.log('type - '+ elem.type);
            switch(elem.type){

                case 'imagem':

                    elemento.append(
                        '' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div style="background: url('+elem.imagem+')no-repeat;" class=" part1 image_galery col-lg-12"></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"><i class="icon-icon-upload-img icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  text-right fonte_12_lato_light_cinza">'+elem.size+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size"><i class="icon-icon-peq-img-size pading_right"></i>'+elem.resolution+' px</div>' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                case 'documento':


                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +



                        '<div class="item_galery  row" data-href="'+elem.rota+'" data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"> <i class="icon-icon-upload-doc icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12   fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.size+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'video':

                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fonte_12_lato first_text_gallery"><i class="icon-icon-peq-video icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12   fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.size+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'audio':

                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"><i class="icon-icon-som icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12   fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.size+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'link':

                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"><i class="icon-embed-upload icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12   fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                default :
                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon_document2 fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

            }


        });
    }



    MediaFinder.prototype.adiciona_lista_archived = function(data,elemento) {

        $(data['folders'][0]).each(function(indice,elem){

            elemento.append('' +
                '<div class="col-lg-12 modo"> ' +

                '<div class="container-fluid">' +

                '<div class="item_galery folder_ref pasta_accept row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                '<div class="folder_icon2 col-lg-1 text-center parent"><i class="icon-icon-pasta icon40 child"></i></div>' +

                '<div class="col-lg-2 second_column_folder  fonte_12_lato parent">' +
                '<div class="child">' +
                '<p class="icon-pasta">'+elem.title+'</p>' +
                '<p>'+elem.n_folders+' pastas</p>'+
                '<p>'+elem.n_folders+' ficheiros</p>'+
                '</div>'+
                '</div>' +

                '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>Pasta de ficheiros</p></div>' +

                '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.size+' mb</div>' +
                '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                '<div class="menu_options_hidden">' +
                '<div class="text-right ">' +
                '<div class="dropdown">'+
                ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                '<i class="icon-roda-dentada icon20"></i>'+
                ' </a>'+
                ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                ' </ul>'+
                ' </div>'+
                ' </div>'+
                '</div>' +
                '</div>' +


                '</div>'+

                '</div>'+
                '</div>');
        });

        $(data['files'][0]).each(function(indice,elem){


            switch(elem.tipo){

                case 'imagem':

                    elemento.append('<div class="col-lg-12 modo"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'"  data-id="'+elem.id+'">' +

                        '<div style="background: url('+elem.imagem+')no-repeat;" class=" image_galery3 col-lg-1"></div>' +


                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '<p>'+elem.resolucao+' px</p>'+
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +

                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +


                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                        '' +
                        '<div class="menu_options_hidden">' +
                        '<div class="text-right ">' +
                        '<div class="dropdown">'+
                        ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<i class="icon-roda-dentada icon20"></i>'+
                        ' </a>'+
                        ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                        ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                        ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                        ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                        ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                        ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                        ' </ul>'+
                        ' </div>'+
                        ' </div>'+
                        '</div>' +
                        '' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                case 'documento':



                    elemento.append('<div class="col-lg-12"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                        '<div class="folder_icon2 col-lg-1 text-center parent"><i class="'+elem.imagem+' icon40 child"></i></div>' +


                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +

                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                        '' +
                        '<div class="menu_options_hidden">' +
                        '<div class="text-right ">' +
                        '<div class="dropdown">'+
                        ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<i class="icon-roda-dentada icon20"></i>'+
                        ' </a>'+
                        ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                        ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                        ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                        ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                        ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                        ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                        ' </ul>'+
                        ' </div>'+
                        ' </div>'+
                        '</div>' +
                        '' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                case 'video':

                    elemento.append('<div class="col-lg-12"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                        '<div class="folder_icon2 col-lg-1 text-center parent"><i class="'+elem.imagem+' icon40 child"></i></div>' +



                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +

                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                        '' +
                        '<div class="menu_options_hidden">' +
                        '<div class="text-right ">' +
                        '<div class="dropdown">'+
                        ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<i class="icon-roda-dentada icon20"></i>'+
                        ' </a>'+
                        ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                        ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                        ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                        ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                        ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                        ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                        ' </ul>'+
                        ' </div>'+
                        ' </div>'+
                        '</div>' +
                        '' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'audio':

                    elemento.append('<div class="col-lg-12"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'"  data-id="'+elem.id+'">' +

                        '<div class="folder_icon2 col-lg-1 text-center parent"><i class="'+elem.imagem+' icon40 child"></i></div>' +



                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +

                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                        '' +
                        '<div class="menu_options_hidden">' +
                        '<div class="text-right ">' +
                        '<div class="dropdown">'+
                        ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<i class="icon-roda-dentada icon20"></i>'+
                        ' </a>'+
                        ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                        ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                        ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                        ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                        ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                        ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                        ' </ul>'+
                        ' </div>'+
                        ' </div>'+
                        '</div>' +
                        '' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                default :
                    elemento.append('<div class="col-lg-12"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                        '<div class="folder_icon2 col-lg-1 text-center parent"><i class="'+elem.imagem+' icon40 child"></i></div>' +




                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +

                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery"><i class="icon-roda-dentada icon20"></i></div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

            }


        });


    }

    MediaFinder.prototype.adiciona_grelha_archived = function(data,elemento) {
        $(data['folders'][0]).each(function(indice,elem){

            elemento.append('' +
                '<div class="col-lg-15 col-md-4 modo "> ' +



                '<div class="container-fluid">' +




                '<div class="item_galery folder_ref row ">' +


                '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center parent">' +
                '<i class="icon-icon-pasta icon80 child"></i>'+
                '</div>' +

                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-peq-pasta fonte_12_lato first_text_gallery">'+elem.title+'</div>' +
                '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right fonte_12_lato_light_cinza">'+elem.size+' mb</div>' +


                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fonte_12_lato_light_cinza no_padding">'+
                '<div class="separacao"></div>' +
                '<div class="text-center foot_gallery_icon fonte_12_lato_light_cinza">'+elem.n_folders+' pastas</div>' +
                '<div class="text-center foot_gallery_icon2 fonte_12_lato_light_cinza">'+elem.n_folders+' ficheiros</div>' +
                '</div>' +


                '</div>'+

                '</div>'+
                '</div>');
        });

        $(data['files'][0]).each(function(indice,elem){

            console.log('type - '+ elem.tipo);
            switch(elem.tipo){

                case 'imagem':

                    elemento.append(
                        '' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div style="background: url('+elem.imagem+')no-repeat;" class=" part1 image_galery col-lg-12"></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-upload-img fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  text-right fonte_12_lato_light_cinza">'+elem.tamanho+' mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">'+elem.resolucao+' px</div>' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                case 'documento':


                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo "> ' +

                        '<div class="container-fluid">' +



                        '<div class="item_galery  row" data-href="'+elem.rota+'" data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-upload-doc fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'video':

                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-peq-video fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'audio':

                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-som fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'link':

                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-peq-upload fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                default :
                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon_document2 fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

            }


        });
    }


    MediaFinder.prototype.onfiltermediaItems = function (format,selected) {




        console.log(format);
        var items_atuais=this.options.items;
        console.log(items_atuais);

        var filtered = $(items_atuais).filter(function( el ) {
            console.log(el);
            return el;
        });

    }

    $.fn.filterMediaFinder = function (format,selected) {


        MediaFinder.prototype.onfiltermediaItems = function (format,selected) {




            console.log(format);
            var items_atuais=this.options.items;
            console.log(items_atuais);

            var filtered = $(items_atuais).filter(function( el ) {
                console.log(el);
                return el;
            });

        }



    }

    // -------------------PLUGIN DEFINITION-----------------------------------
    MediaFinder.DEFAULTS = {
        caminho: null,
        token_laravel: null,
        modo_lista:0,
        arquivado:0,
        caminho_muda_items:null,
        items:null,
        onfiltertype:null,
        filterObjects:null
    }

    $.fn.mediaFinder = function (option) {
        var args = arguments;


        return this.each(function () {
            var $this   = $(this)
            var options =  (!option)? MediaFinder.DEFAULTS: option
            new MediaFinder(this, options)

        })
    }








}(window.jQuery));
/*!
 * jQuery contextMenu v@VERSION - Plugin for simple contextMenu handling
 *
 * Version: v@VERSION
 *
 * Authors: Björn Brala (SWIS.nl), Rodney Rehm, Addy Osmani (patches for FF)
 * Web: http://swisnl.github.io/jQuery-contextMenu/
 *
 * Copyright (c) 2011-@YEAR SWIS BV and contributors
 *
 * Licensed under
 *   MIT License http://www.opensource.org/licenses/mit-license
 *   GPL v3 http://opensource.org/licenses/GPL-3.0
 *
 * Date: @DATE
 */

(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as anonymous module.
        define(['jquery'], factory);
    } else if (typeof exports === 'object') {
        // Node / CommonJS
        factory(require('jquery'));
    } else {
        // Browser globals.
        factory(jQuery);
    }
})(function ($) {

    'use strict';

    // TODO: -
    // ARIA stuff: menuitem, menuitemcheckbox und menuitemradio
    // create <menu> structure if $.support[htmlCommand || htmlMenuitem] and !opt.disableNative

    // determine html5 compatibility
    $.support.htmlMenuitem = ('HTMLMenuItemElement' in window);
    $.support.htmlCommand = ('HTMLCommandElement' in window);
    $.support.eventSelectstart = ('onselectstart' in document.documentElement);
    /* // should the need arise, test for css user-select
     $.support.cssUserSelect = (function(){
     var t = false,
     e = document.createElement('div');

     $.each('Moz|Webkit|Khtml|O|ms|Icab|'.split('|'), function(i, prefix) {
     var propCC = prefix + (prefix ? 'U' : 'u') + 'serSelect',
     prop = (prefix ? ('-' + prefix.toLowerCase() + '-') : '') + 'user-select';

     e.style.cssText = prop + ': text;';
     if (e.style[propCC] == 'text') {
     t = true;
     return false;
     }

     return true;
     });

     return t;
     })();
     */

    /* jshint ignore:start */
    if (!$.ui || !$.widget) {
        // duck punch $.cleanData like jQueryUI does to get that remove event
        $.cleanData = (function (orig) {
            return function (elems) {
                var events, elem, i;
                for (i = 0; elems[i] != null; i++) {
                    elem = elems[i];
                    try {
                        // Only trigger remove when necessary to save time
                        events = $._data(elem, 'events');
                        if (events && events.remove) {
                            $(elem).triggerHandler('remove');
                        }

                        // Http://bugs.jquery.com/ticket/8235
                    } catch (e) {}
                }
                orig(elems);
            };
        })($.cleanData);
    }
    /* jshint ignore:end */

    var // currently active contextMenu trigger
        $currentTrigger = null,
    // is contextMenu initialized with at least one menu?
        initialized = false,
    // window handle
        $win = $(window),
    // number of registered menus
        counter = 0,
    // mapping selector to namespace
        namespaces = {},
    // mapping namespace to options
        menus = {},
    // custom command type handlers
        types = {},
    // default values
        defaults = {
            // selector of contextMenu trigger
            selector: null,
            // where to append the menu to
            appendTo: null,
            // method to trigger context menu ["right", "left", "hover"]
            trigger: 'right',
            // hide menu when mouse leaves trigger / menu elements
            autoHide: false,
            // ms to wait before showing a hover-triggered context menu
            delay: 200,
            // flag denoting if a second trigger should simply move (true) or rebuild (false) an open menu
            // as long as the trigger happened on one of the trigger-element's child nodes
            reposition: true,

            // Default classname configuration to be able avoid conflicts in frameworks
            classNames : {

                hover: 'context-menu-hover', // Item hover
                disabled: 'context-menu-disabled', // Item disabled
                visible: 'context-menu-visible', // Item visible
                notSelectable: 'context-menu-not-selectable', // Item not selectable

                icon: 'context-menu-icon',
                iconEdit: 'context-menu-icon-edit',
                iconCut: 'context-menu-icon-cut',
                iconCopy: 'context-menu-icon-copy',
                iconPaste: 'context-menu-icon-paste',
                iconDelete: 'context-menu-icon-delete',
                iconAdd: 'context-menu-icon-add',
                iconQuit: 'context-menu-icon-quit'
            },

            // determine position to show menu at
            determinePosition: function ($menu) {
                // position to the lower middle of the trigger element
                if ($.ui && $.ui.position) {
                    // .position() is provided as a jQuery UI utility
                    // (...and it won't work on hidden elements)
                    $menu.css('display', 'block').position({
                        my: 'center top',
                        at: 'center bottom',
                        of: this,
                        offset: '0 5',
                        collision: 'fit'
                    }).css('display', 'none');
                } else {
                    // determine contextMenu position
                    var offset = this.offset();
                    offset.top += this.outerHeight();
                    offset.left += this.outerWidth() / 2 - $menu.outerWidth() / 2;
                    $menu.css(offset);
                }
            },
            // position menu
            position: function (opt, x, y) {
                var offset;
                // determine contextMenu position
                if (!x && !y) {
                    opt.determinePosition.call(this, opt.$menu);
                    return;
                } else if (x === 'maintain' && y === 'maintain') {
                    // x and y must not be changed (after re-show on command click)
                    offset = opt.$menu.position();
                } else {
                    // x and y are given (by mouse event)
                    offset = {top: y, left: x};
                }

                // correct offset if viewport demands it
                var bottom = $win.scrollTop() + $win.height(),
                    right = $win.scrollLeft() + $win.width(),
                    height = opt.$menu.outerHeight(),
                    width = opt.$menu.outerWidth();

                if (offset.top + height > bottom) {
                    offset.top -= height;
                }

                if (offset.top < 0) {
                    offset.top = 0;
                }

                if (offset.left + width > right) {
                    offset.left -= width;
                }

                if (offset.left < 0) {
                    offset.left = 0;
                }

                opt.$menu.css(offset);
            },
            // position the sub-menu
            positionSubmenu: function ($menu) {
                if ($.ui && $.ui.position) {
                    // .position() is provided as a jQuery UI utility
                    // (...and it won't work on hidden elements)
                    $menu.css('display', 'block').position({
                        my: 'left top',
                        at: 'right top',
                        of: this,
                        collision: 'flipfit fit'
                    }).css('display', '');
                } else {
                    // determine contextMenu position
                    var offset = {
                        top: 0,
                        left: this.outerWidth()
                    };
                    $menu.css(offset);
                }
            },
            // offset to add to zIndex
            zIndex: 1,
            // show hide animation settings
            animation: {
                duration: 50,
                show: 'slideDown',
                hide: 'slideUp'
            },
            // events
            events: {
                show: $.noop,
                hide: $.noop
            },
            // default callback
            callback: null,
            // list of contextMenu items
            items: {}
        },
    // mouse position for hover activation
        hoveract = {
            timer: null,
            pageX: null,
            pageY: null
        },
    // determine zIndex
        zindex = function ($t) {
            var zin = 0,
                $tt = $t;

            while (true) {
                zin = Math.max(zin, parseInt($tt.css('z-index'), 10) || 0);
                $tt = $tt.parent();
                if (!$tt || !$tt.length || 'html body'.indexOf($tt.prop('nodeName').toLowerCase()) > -1) {
                    break;
                }
            }
            return zin;
        },
    // event handlers
        handle = {
            // abort anything
            abortevent: function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
            },
            // contextmenu show dispatcher
            contextmenu: function (e) {
                var $this = $(this);

                // disable actual context-menu if we are using the right mouse button as the trigger
                if (e.data.trigger === 'right') {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }

                // abort native-triggered events unless we're triggering on right click
                if ((e.data.trigger !== 'right' && e.data.trigger !== 'demand') && e.originalEvent) {
                    return;
                }

                // Let the current contextmenu decide if it should show or not based on its own trigger settings
                if (e.mouseButton !== undefined && e.data) {
                    if (!(e.data.trigger === 'left' && e.mouseButton === 0) && !(e.data.trigger === 'right' && e.mouseButton === 2)) {
                        // Mouse click is not valid.
                        return;
                    }
                }

                // abort event if menu is visible for this trigger
                if ($this.hasClass('context-menu-active')) {
                    return;
                }

                if (!$this.hasClass('context-menu-disabled')) {
                    // theoretically need to fire a show event at <menu>
                    // http://www.whatwg.org/specs/web-apps/current-work/multipage/interactive-elements.html#context-menus
                    // var evt = jQuery.Event("show", { data: data, pageX: e.pageX, pageY: e.pageY, relatedTarget: this });
                    // e.data.$menu.trigger(evt);

                    $currentTrigger = $this;
                    if (e.data.build) {
                        var built = e.data.build($currentTrigger, e);
                        // abort if build() returned false
                        if (built === false) {
                            return;
                        }

                        // dynamically build menu on invocation
                        e.data = $.extend(true, {}, defaults, e.data, built || {});

                        // abort if there are no items to display
                        if (!e.data.items || $.isEmptyObject(e.data.items)) {
                            // Note: jQuery captures and ignores errors from event handlers
                            if (window.console) {
                                (console.error || console.log).call(console, 'No items specified to show in contextMenu');
                            }

                            throw new Error('No Items specified');
                        }

                        // backreference for custom command type creation
                        e.data.$trigger = $currentTrigger;

                        op.create(e.data);
                    }
                    var showMenu = false;
                    for (var item in e.data.items) {
                        if (e.data.items.hasOwnProperty(item)) {
                            var visible;
                            if ($.isFunction(e.data.items[item].visible)) {
                                visible = e.data.items[item].visible.call($(e.currentTarget), item, e.data);
                            } else if (typeof item.visible !== 'undefined') {
                                visible = e.data.items[item].visible === true;
                            } else {
                                visible = true;
                            }
                            if (visible) {
                                showMenu = true;
                            }
                        }
                    }
                    if (showMenu) {
                        // show menu
                        var menuContainer = (e.data.appendTo === null ? $('body') : $(e.data.appendTo));
                        var srcElement = e.target || e.srcElement || e.originalTarget;
                        op.show.call($this, e.data, e.pageX, e.pageY);
                    }
                }
            },
            // contextMenu left-click trigger
            click: function (e) {
                e.preventDefault();
                e.stopImmediatePropagation();
                $(this).trigger($.Event('contextmenu', {data: e.data, pageX: e.pageX, pageY: e.pageY}));
            },
            // contextMenu right-click trigger
            mousedown: function (e) {
                // register mouse down
                var $this = $(this);

                // hide any previous menus
                if ($currentTrigger && $currentTrigger.length && !$currentTrigger.is($this)) {
                    $currentTrigger.data('contextMenu').$menu.trigger('contextmenu:hide');
                }

                // activate on right click
                if (e.button === 2) {
                    $currentTrigger = $this.data('contextMenuActive', true);
                }
            },
            // contextMenu right-click trigger
            mouseup: function (e) {
                // show menu
                var $this = $(this);
                if ($this.data('contextMenuActive') && $currentTrigger && $currentTrigger.length && $currentTrigger.is($this) && !$this.hasClass('context-menu-disabled')) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    $currentTrigger = $this;
                    $this.trigger($.Event('contextmenu', {data: e.data, pageX: e.pageX, pageY: e.pageY}));
                }

                $this.removeData('contextMenuActive');
            },
            // contextMenu hover trigger
            mouseenter: function (e) {
                var $this = $(this),
                    $related = $(e.relatedTarget),
                    $document = $(document);

                // abort if we're coming from a menu
                if ($related.is('.context-menu-list') || $related.closest('.context-menu-list').length) {
                    return;
                }

                // abort if a menu is shown
                if ($currentTrigger && $currentTrigger.length) {
                    return;
                }

                hoveract.pageX = e.pageX;
                hoveract.pageY = e.pageY;
                hoveract.data = e.data;
                $document.on('mousemove.contextMenuShow', handle.mousemove);
                hoveract.timer = setTimeout(function () {
                    hoveract.timer = null;
                    $document.off('mousemove.contextMenuShow');
                    $currentTrigger = $this;
                    $this.trigger($.Event('contextmenu', {
                        data: hoveract.data,
                        pageX: hoveract.pageX,
                        pageY: hoveract.pageY
                    }));
                }, e.data.delay);
            },
            // contextMenu hover trigger
            mousemove: function (e) {
                hoveract.pageX = e.pageX;
                hoveract.pageY = e.pageY;
            },
            // contextMenu hover trigger
            mouseleave: function (e) {
                // abort if we're leaving for a menu
                var $related = $(e.relatedTarget);
                if ($related.is('.context-menu-list') || $related.closest('.context-menu-list').length) {
                    return;
                }

                try {
                    clearTimeout(hoveract.timer);
                } catch (e) {
                }

                hoveract.timer = null;
            },
            // click on layer to hide contextMenu
            layerClick: function (e) {
                var $this = $(this),
                    root = $this.data('contextMenuRoot'),
                    button = e.button,
                    x = e.pageX,
                    y = e.pageY,
                    target,
                    offset;

                e.preventDefault();
                e.stopImmediatePropagation();

                setTimeout(function () {
                    var $window;
                    var triggerAction = ((root.trigger === 'left' && button === 0) || (root.trigger === 'right' && button === 2));

                    // find the element that would've been clicked, wasn't the layer in the way
                    if (document.elementFromPoint && root.$layer) {
                        root.$layer.hide();
                        target = document.elementFromPoint(x - $win.scrollLeft(), y - $win.scrollTop());
                        root.$layer.show();
                    }

                    if (root.reposition && triggerAction) {
                        if (document.elementFromPoint) {
                            if (root.$trigger.is(target) || root.$trigger.has(target).length) {
                                root.position.call(root.$trigger, root, x, y);
                                return;
                            }
                        } else {
                            offset = root.$trigger.offset();
                            $window = $(window);
                            // while this looks kinda awful, it's the best way to avoid
                            // unnecessarily calculating any positions
                            offset.top += $window.scrollTop();
                            if (offset.top <= e.pageY) {
                                offset.left += $window.scrollLeft();
                                if (offset.left <= e.pageX) {
                                    offset.bottom = offset.top + root.$trigger.outerHeight();
                                    if (offset.bottom >= e.pageY) {
                                        offset.right = offset.left + root.$trigger.outerWidth();
                                        if (offset.right >= e.pageX) {
                                            // reposition
                                            root.position.call(root.$trigger, root, x, y);
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (target && triggerAction) {
                        root.$trigger.one('contextmenu:hidden', function () {
                            $(target).contextMenu({ x: x, y: y, button: button });
                        });
                    }

                    root.$menu.trigger('contextmenu:hide');
                }, 50);
            },
            // key handled :hover
            keyStop: function (e, opt) {
                if (!opt.isInput) {
                    e.preventDefault();
                }

                e.stopPropagation();
            },
            key: function (e) {

                var opt = {};

                // Only get the data from $currentTrigger if it exists
                if ($currentTrigger) {
                    opt = $currentTrigger.data('contextMenu') || {};
                }
                // If the trigger happen on a element that are above the contextmenu do this
                if (opt.zIndex === undefined) {
                    opt.zIndex = 0;
                }
                var targetZIndex = 0;
                var getZIndexOfTriggerTarget = function (target) {
                    if (target.style.zIndex !== '') {
                        targetZIndex = target.style.zIndex;
                    } else {
                        if (target.offsetParent !== null && target.offsetParent !== undefined) {
                            getZIndexOfTriggerTarget(target.offsetParent);
                        }
                        else if (target.parentElement !== null && target.parentElement !== undefined) {
                            getZIndexOfTriggerTarget(target.parentElement);
                        }
                    }
                };
                getZIndexOfTriggerTarget(e.target);
                // If targetZIndex is heigher then opt.zIndex dont progress any futher.
                // This is used to make sure that if you are using a dialog with a input / textarea / contenteditable div
                // and its above the contextmenu it wont steal keys events
                if (targetZIndex > opt.zIndex) {
                    return;
                }
                switch (e.keyCode) {
                    case 9:
                    case 38: // up
                        handle.keyStop(e, opt);
                        // if keyCode is [38 (up)] or [9 (tab) with shift]
                        if (opt.isInput) {
                            if (e.keyCode === 9 && e.shiftKey) {
                                e.preventDefault();
                                if(opt.$selected) {
                                    opt.$selected.find('input, textarea, select').blur();
                                }
                                opt.$menu.trigger('prevcommand');
                                return;
                            } else if (e.keyCode === 38 && opt.$selected.find('input, textarea, select').prop('type') === 'checkbox') {
                                // checkboxes don't capture this key
                                e.preventDefault();
                                return;
                            }
                        } else if (e.keyCode !== 9 || e.shiftKey) {
                            opt.$menu.trigger('prevcommand');
                            return;
                        }
                        break;
                    // omitting break;
                    // case 9: // tab - reached through omitted break;
                    case 40: // down
                        handle.keyStop(e, opt);
                        if (opt.isInput) {
                            if (e.keyCode === 9) {
                                e.preventDefault();
                                if(opt.$selected) {
                                    opt.$selected.find('input, textarea, select').blur();
                                }
                                opt.$menu.trigger('nextcommand');
                                return;
                            } else if (e.keyCode === 40 && opt.$selected.find('input, textarea, select').prop('type') === 'checkbox') {
                                // checkboxes don't capture this key
                                e.preventDefault();
                                return;
                            }
                        } else {
                            opt.$menu.trigger('nextcommand');
                            return;
                        }
                        break;

                    case 37: // left
                        handle.keyStop(e, opt);
                        if (opt.isInput || !opt.$selected || !opt.$selected.length) {
                            break;
                        }

                        if (!opt.$selected.parent().hasClass('context-menu-root')) {
                            var $parent = opt.$selected.parent().parent();
                            opt.$selected.trigger('contextmenu:blur');
                            opt.$selected = $parent;
                            return;
                        }
                        break;

                    case 39: // right
                        handle.keyStop(e, opt);
                        if (opt.isInput || !opt.$selected || !opt.$selected.length) {
                            break;
                        }

                        var itemdata = opt.$selected.data('contextMenu') || {};
                        if (itemdata.$menu && opt.$selected.hasClass('context-menu-submenu')) {
                            opt.$selected = null;
                            itemdata.$selected = null;
                            itemdata.$menu.trigger('nextcommand');
                            return;
                        }
                        break;

                    case 35: // end
                    case 36: // home
                        if (opt.$selected && opt.$selected.find('input, textarea, select').length) {
                            return;
                        } else {
                            (opt.$selected && opt.$selected.parent() || opt.$menu)
                                .children(':not(.' + opt.classNames.disabled + ', .' + opt.classNames.notSelectable + ')')[e.keyCode === 36 ? 'first' : 'last']()
                                .trigger('contextmenu:focus');
                            e.preventDefault();
                            return;
                        }
                        break;

                    case 13: // enter
                        handle.keyStop(e, opt);
                        if (opt.isInput) {
                            if (opt.$selected && !opt.$selected.is('textarea, select')) {
                                e.preventDefault();
                                return;
                            }
                            break;
                        }
                        if (typeof opt.$selected !== 'undefined' && opt.$selected !== null) {
                            opt.$selected.trigger('mouseup');
                        }
                        return;

                    case 32: // space
                    case 33: // page up
                    case 34: // page down
                        // prevent browser from scrolling down while menu is visible
                        handle.keyStop(e, opt);
                        return;

                    case 27: // esc
                        handle.keyStop(e, opt);
                        opt.$menu.trigger('contextmenu:hide');
                        return;

                    default: // 0-9, a-z
                        var k = (String.fromCharCode(e.keyCode)).toUpperCase();
                        if (opt.accesskeys && opt.accesskeys[k]) {
                            // according to the specs accesskeys must be invoked immediately
                            opt.accesskeys[k].$node.trigger(opt.accesskeys[k].$menu ? 'contextmenu:focus' : 'mouseup');
                            return;
                        }
                        break;
                }
                // pass event to selected item,
                // stop propagation to avoid endless recursion
                e.stopPropagation();
                if (typeof opt.$selected !== 'undefined' && opt.$selected !== null) {
                    opt.$selected.trigger(e);
                }
            },
            // select previous possible command in menu
            prevItem: function (e) {
                e.stopPropagation();
                var opt = $(this).data('contextMenu') || {};
                var root = $(this).data('contextMenuRoot') || {};

                // obtain currently selected menu
                if (opt.$selected) {
                    var $s = opt.$selected;
                    opt = opt.$selected.parent().data('contextMenu') || {};
                    opt.$selected = $s;
                }

                var $children = opt.$menu.children(),
                    $prev = !opt.$selected || !opt.$selected.prev().length ? $children.last() : opt.$selected.prev(),
                    $round = $prev;

                // skip disabled or hidden elements
                while ($prev.hasClass(root.classNames.disabled) || $prev.hasClass(root.classNames.notSelectable) || $prev.is(':hidden')) {
                    if ($prev.prev().length) {
                        $prev = $prev.prev();
                    } else {
                        $prev = $children.last();
                    }
                    if ($prev.is($round)) {
                        // break endless loop
                        return;
                    }
                }

                // leave current
                if (opt.$selected) {
                    handle.itemMouseleave.call(opt.$selected.get(0), e);
                }

                // activate next
                handle.itemMouseenter.call($prev.get(0), e);

                // focus input
                var $input = $prev.find('input, textarea, select');
                if ($input.length) {
                    $input.focus();
                }
            },
            // select next possible command in menu
            nextItem: function (e) {
                e.stopPropagation();
                var opt = $(this).data('contextMenu') || {};
                var root = $(this).data('contextMenuRoot') || {};

                // obtain currently selected menu
                if (opt.$selected) {
                    var $s = opt.$selected;
                    opt = opt.$selected.parent().data('contextMenu') || {};
                    opt.$selected = $s;
                }

                var $children = opt.$menu.children(),
                    $next = !opt.$selected || !opt.$selected.next().length ? $children.first() : opt.$selected.next(),
                    $round = $next;

                // skip disabled
                while ($next.hasClass(root.classNames.disabled) || $next.hasClass(root.classNames.notSelectable) || $next.is(':hidden')) {
                    if ($next.next().length) {
                        $next = $next.next();
                    } else {
                        $next = $children.first();
                    }
                    if ($next.is($round)) {
                        // break endless loop
                        return;
                    }
                }

                // leave current
                if (opt.$selected) {
                    handle.itemMouseleave.call(opt.$selected.get(0), e);
                }

                // activate next
                handle.itemMouseenter.call($next.get(0), e);

                // focus input
                var $input = $next.find('input, textarea, select');
                if ($input.length) {
                    $input.focus();
                }
            },
            // flag that we're inside an input so the key handler can act accordingly
            focusInput: function () {
                var $this = $(this).closest('.context-menu-item'),
                    data = $this.data(),
                    opt = data.contextMenu,
                    root = data.contextMenuRoot;

                root.$selected = opt.$selected = $this;
                root.isInput = opt.isInput = true;
            },
            // flag that we're inside an input so the key handler can act accordingly
            blurInput: function () {
                var $this = $(this).closest('.context-menu-item'),
                    data = $this.data(),
                    opt = data.contextMenu,
                    root = data.contextMenuRoot;

                root.isInput = opt.isInput = false;
            },
            // :hover on menu
            menuMouseenter: function () {
                var root = $(this).data().contextMenuRoot;
                root.hovering = true;
            },
            // :hover on menu
            menuMouseleave: function (e) {
                var root = $(this).data().contextMenuRoot;
                if (root.$layer && root.$layer.is(e.relatedTarget)) {
                    root.hovering = false;
                }
            },
            // :hover done manually so key handling is possible
            itemMouseenter: function (e) {
                var $this = $(this),
                    data = $this.data(),
                    opt = data.contextMenu,
                    root = data.contextMenuRoot;

                root.hovering = true;

                // abort if we're re-entering
                if (e && root.$layer && root.$layer.is(e.relatedTarget)) {
                    e.preventDefault();
                    e.stopImmediatePropagation();
                }

                // make sure only one item is selected
                (opt.$menu ? opt : root).$menu
                    .children('.' + root.classNames.hover).trigger('contextmenu:blur')
                    .children('.hover').trigger('contextmenu:blur');

                if ($this.hasClass(root.classNames.disabled) || $this.hasClass(root.classNames.notSelectable)) {
                    opt.$selected = null;
                    return;
                }

                $this.trigger('contextmenu:focus');
            },
            // :hover done manually so key handling is possible
            itemMouseleave: function (e) {
                var $this = $(this),
                    data = $this.data(),
                    opt = data.contextMenu,
                    root = data.contextMenuRoot;

                if (root !== opt && root.$layer && root.$layer.is(e.relatedTarget)) {
                    if (typeof root.$selected !== 'undefined' && root.$selected !== null) {
                        root.$selected.trigger('contextmenu:blur');
                    }
                    e.preventDefault();
                    e.stopImmediatePropagation();
                    root.$selected = opt.$selected = opt.$node;
                    return;
                }

                $this.trigger('contextmenu:blur');
            },
            // contextMenu item click
            itemClick: function (e) {
                var $this = $(this),
                    data = $this.data(),
                    opt = data.contextMenu,
                    root = data.contextMenuRoot,
                    key = data.contextMenuKey,
                    callback;

                // abort if the key is unknown or disabled or is a menu
                if (!opt.items[key] || $this.is('.' + root.classNames.disabled + ', .context-menu-submenu, .context-menu-separator, .' + root.classNames.notSelectable)) {
                    return;
                }

                e.preventDefault();
                e.stopImmediatePropagation();

                if ($.isFunction(root.callbacks[key]) && Object.prototype.hasOwnProperty.call(root.callbacks, key)) {
                    // item-specific callback
                    callback = root.callbacks[key];
                } else if ($.isFunction(root.callback)) {
                    // default callback
                    callback = root.callback;
                } else {
                    // no callback, no action
                    return;
                }

                // hide menu if callback doesn't stop that
                if (callback.call(root.$trigger, key, root) !== false) {
                    root.$menu.trigger('contextmenu:hide');
                } else if (root.$menu.parent().length) {
                    op.update.call(root.$trigger, root);
                }
            },
            // ignore click events on input elements
            inputClick: function (e) {
                e.stopImmediatePropagation();
            },
            // hide <menu>
            hideMenu: function (e, data) {
                var root = $(this).data('contextMenuRoot');
                op.hide.call(root.$trigger, root, data && data.force);
            },
            // focus <command>
            focusItem: function (e) {
                e.stopPropagation();
                var $this = $(this),
                    data = $this.data(),
                    opt = data.contextMenu,
                    root = data.contextMenuRoot;

                if ($this.hasClass(root.classNames.disabled) || $this.hasClass(root.classNames.notSelectable)) {
                    return;
                }

                $this
                    .addClass([root.classNames.hover, root.classNames.visible].join(' '))
                    // select other items and included items
                    .parent().find('.context-menu-item').not($this)
                    .removeClass(root.classNames.visible)
                    .filter('.' + root.classNames.hover)
                    .trigger('contextmenu:blur');

                // remember selected
                opt.$selected = root.$selected = $this;

                // position sub-menu - do after show so dumb $.ui.position can keep up
                if (opt.$node) {
                    root.positionSubmenu.call(opt.$node, opt.$menu);
                }
            },
            // blur <command>
            blurItem: function (e) {
                e.stopPropagation();
                var $this = $(this),
                    data = $this.data(),
                    opt = data.contextMenu,
                    root = data.contextMenuRoot;

                if (opt.autoHide) { // for tablets and touch screens this needs to remain
                    $this.removeClass(root.classNames.visible);
                }
                $this.removeClass(root.classNames.hover);
                opt.$selected = null;
            }
        },
    // operations
        op = {
            show: function (opt, x, y) {
                var $trigger = $(this),
                    css = {};

                // hide any open menus
                $('#context-menu-layer').trigger('mousedown');

                // backreference for callbacks
                opt.$trigger = $trigger;

                // show event
                if (opt.events.show.call($trigger, opt) === false) {
                    $currentTrigger = null;
                    return;
                }

                // create or update context menu
                op.update.call($trigger, opt);

                // position menu
                opt.position.call($trigger, opt, x, y);

                // make sure we're in front
                if (opt.zIndex) {
                    var additionalZValue = opt.zIndex;
                    // If opt.zIndex is a function, call the function to get the right zIndex.
                    if (typeof opt.zIndex === 'function') {
                        additionalZValue = opt.zIndex.call($trigger, opt);
                    }
                    css.zIndex = zindex($trigger) + additionalZValue;
                }

                // add layer
                op.layer.call(opt.$menu, opt, css.zIndex);

                // adjust sub-menu zIndexes
                opt.$menu.find('ul').css('zIndex', css.zIndex + 1);

                // position and show context menu
                opt.$menu.css(css)[opt.animation.show](opt.animation.duration, function () {
                    $trigger.trigger('contextmenu:visible');
                });
                // make options available and set state
                $trigger
                    .data('contextMenu', opt)
                    .addClass('context-menu-active');

                // register key handler
                $(document).off('keydown.contextMenu').on('keydown.contextMenu', handle.key);
                // register autoHide handler
                if (opt.autoHide) {
                    // mouse position handler
                    $(document).on('mousemove.contextMenuAutoHide', function (e) {
                        // need to capture the offset on mousemove,
                        // since the page might've been scrolled since activation
                        var pos = $trigger.offset();
                        pos.right = pos.left + $trigger.outerWidth();
                        pos.bottom = pos.top + $trigger.outerHeight();

                        if (opt.$layer && !opt.hovering && (!(e.pageX >= pos.left && e.pageX <= pos.right) || !(e.pageY >= pos.top && e.pageY <= pos.bottom))) {
                            /* Additional hover check after short time, you might just miss the edge of the menu */
                            setTimeout(function () {
                                if (!opt.hovering) { opt.$menu.trigger('contextmenu:hide'); }
                            }, 50);
                        }
                    });
                }
            },
            hide: function (opt, force) {
                var $trigger = $(this);
                if (!opt) {
                    opt = $trigger.data('contextMenu') || {};
                }

                // hide event
                if (!force && opt.events && opt.events.hide.call($trigger, opt) === false) {
                    return;
                }

                // remove options and revert state
                $trigger
                    .removeData('contextMenu')
                    .removeClass('context-menu-active');

                if (opt.$layer) {
                    // keep layer for a bit so the contextmenu event can be aborted properly by opera
                    setTimeout((function ($layer) {
                        return function () {
                            $layer.remove();
                        };
                    })(opt.$layer), 10);

                    try {
                        delete opt.$layer;
                    } catch (e) {
                        opt.$layer = null;
                    }
                }

                // remove handle
                $currentTrigger = null;
                // remove selected
                opt.$menu.find('.' + opt.classNames.hover).trigger('contextmenu:blur');
                opt.$selected = null;
                // collapse all submenus
                opt.$menu.find('.' + opt.classNames.visible).removeClass(opt.classNames.visible);
                // unregister key and mouse handlers
                // $(document).off('.contextMenuAutoHide keydown.contextMenu'); // http://bugs.jquery.com/ticket/10705
                $(document).off('.contextMenuAutoHide').off('keydown.contextMenu');
                // hide menu
                if(opt.$menu){
                    opt.$menu[opt.animation.hide](opt.animation.duration, function () {
                        // tear down dynamically built menu after animation is completed.
                        if (opt.build) {
                            opt.$menu.remove();
                            $.each(opt, function (key) {
                                switch (key) {
                                    case 'ns':
                                    case 'selector':
                                    case 'build':
                                    case 'trigger':
                                        return true;

                                    default:
                                        opt[key] = undefined;
                                        try {
                                            delete opt[key];
                                        } catch (e) {
                                        }
                                        return true;
                                }
                            });
                        }

                        setTimeout(function () {
                            $trigger.trigger('contextmenu:hidden');
                        }, 10);
                    });
                }
            },
            create: function (opt, root) {
                if (root === undefined) {
                    root = opt;
                }
                // create contextMenu
                opt.$menu = $('<ul class="context-menu-list"></ul>').addClass(opt.className || '').data({
                    'contextMenu': opt,
                    'contextMenuRoot': root
                });

                $.each(['callbacks', 'commands', 'inputs'], function (i, k) {
                    opt[k] = {};
                    if (!root[k]) {
                        root[k] = {};
                    }
                });

                if(!root.accesskeys){
                    root.accesskeys = {};
                }

                function createNameNode(item) {
                    var $name = $('<span></span>');
                    if (item._accesskey) {
                        if (item._beforeAccesskey) {
                            $name.append(document.createTextNode(item._beforeAccesskey));
                        }
                        $('<span></span>')
                            .addClass('context-menu-accesskey')
                            .text(item._accesskey)
                            .appendTo($name);
                        if (item._afterAccesskey) {
                            $name.append(document.createTextNode(item._afterAccesskey));
                        }
                    } else {
                        if (item.isHtmlName) {
                            // restrict use with access keys
                            if (typeof item.accesskey !== 'undefined') {
                                throw new Error('accesskeys are not compatible with HTML names and cannot be used together in the same item');
                            }
                            $name.html(item.name);
                        } else {
                            $name.text(item.name);
                        }
                    }
                    return $name;
                }

                // create contextMenu items
                $.each(opt.items, function (key, item) {
                    var $t = $('<li class="context-menu-item"></li>').addClass(item.className || ''),
                        $label = null,
                        $input = null;

                    // iOS needs to see a click-event bound to an element to actually
                    // have the TouchEvents infrastructure trigger the click event
                    $t.on('click', $.noop);

                    // Make old school string seperator a real item so checks wont be
                    // akward later.
                    if (typeof item === 'string') {
                        item = { type : 'cm_seperator' };
                    }

                    item.$node = $t.data({
                        'contextMenu': opt,
                        'contextMenuRoot': root,
                        'contextMenuKey': key
                    });

                    // register accesskey
                    // NOTE: the accesskey attribute should be applicable to any element, but Safari5 and Chrome13 still can't do that
                    if (typeof item.accesskey !== 'undefined') {
                        var aks = splitAccesskey(item.accesskey);
                        for (var i = 0, ak; ak = aks[i]; i++) {
                            if (!root.accesskeys[ak]) {
                                root.accesskeys[ak] = item;
                                var matched = item.name.match(new RegExp('^(.*?)(' + ak + ')(.*)$', 'i'));
                                if (matched) {
                                    item._beforeAccesskey = matched[1];
                                    item._accesskey = matched[2];
                                    item._afterAccesskey = matched[3];
                                }
                                break;
                            }
                        }
                    }

                    if (item.type && types[item.type]) {
                        // run custom type handler
                        types[item.type].call($t, item, opt, root);
                        // register commands
                        $.each([opt, root], function (i, k) {
                            k.commands[key] = item;
                            if ($.isFunction(item.callback)) {
                                k.callbacks[key] = item.callback;
                            }
                        });
                    } else {
                        // add label for input
                        if (item.type === 'cm_seperator') {
                            $t.addClass('context-menu-separator ' + root.classNames.notSelectable);
                        } else if (item.type === 'html') {
                            $t.addClass('context-menu-html ' + root.classNames.notSelectable);
                        } else if (item.type) {
                            $label = $('<label></label>').appendTo($t);
                            createNameNode(item).appendTo($label);

                            $t.addClass('context-menu-input');
                            opt.hasTypes = true;
                            $.each([opt, root], function (i, k) {
                                k.commands[key] = item;
                                k.inputs[key] = item;
                            });
                        } else if (item.items) {
                            item.type = 'sub';
                        }

                        switch (item.type) {
                            case 'cm_seperator':
                                break;

                            case 'text':
                                $input = $('<input type="text" value="1" name="" value="">')
                                    .attr('name', 'context-menu-input-' + key)
                                    .val(item.value || '')
                                    .appendTo($label);
                                break;

                            case 'textarea':
                                $input = $('<textarea name=""></textarea>')
                                    .attr('name', 'context-menu-input-' + key)
                                    .val(item.value || '')
                                    .appendTo($label);

                                if (item.height) {
                                    $input.height(item.height);
                                }
                                break;

                            case 'checkbox':
                                $input = $('<input type="checkbox" value="1" name="" value="">')
                                    .attr('name', 'context-menu-input-' + key)
                                    .val(item.value || '')
                                    .prop('checked', !!item.selected)
                                    .prependTo($label);
                                break;

                            case 'radio':
                                $input = $('<input type="radio" value="1" name="" value="">')
                                    .attr('name', 'context-menu-input-' + item.radio)
                                    .val(item.value || '')
                                    .prop('checked', !!item.selected)
                                    .prependTo($label);
                                break;

                            case 'select':
                                $input = $('<select name="">')
                                    .attr('name', 'context-menu-input-' + key)
                                    .appendTo($label);
                                if (item.options) {
                                    $.each(item.options, function (value, text) {
                                        $('<option></option>').val(value).text(text).appendTo($input);
                                    });
                                    $input.val(item.selected);
                                }
                                break;

                            case 'sub':
                                createNameNode(item).appendTo($t);

                                item.appendTo = item.$node;
                                op.create(item, root);
                                $t.data('contextMenu', item).addClass('context-menu-submenu');
                                item.callback = null;
                                break;

                            case 'html':
                                $(item.html).appendTo($t);
                                break;

                            default:
                                $.each([opt, root], function (i, k) {
                                    k.commands[key] = item;
                                    if ($.isFunction(item.callback)) {
                                        k.callbacks[key] = item.callback;
                                    }
                                });
                                createNameNode(item).appendTo($t);
                                break;
                        }

                        // disable key listener in <input>
                        if (item.type && item.type !== 'sub' && item.type !== 'html' && item.type !== 'cm_seperator') {
                            $input
                                .on('focus', handle.focusInput)
                                .on('blur', handle.blurInput);

                            if (item.events) {
                                $input.on(item.events, opt);
                            }
                        }

                        // add icons
                        if (item.icon) {
                            if ($.isFunction(item.icon)) {
                                item._icon = item.icon.call(this, this, $t, key, item);
                            } else {
                                item._icon = root.classNames.icon + ' ' + root.classNames.icon + '-' + item.icon;
                            }
                            $t.addClass(item._icon);
                        }
                    }

                    // cache contained elements
                    item.$input = $input;
                    item.$label = $label;

                    // attach item to menu
                    $t.appendTo(opt.$menu);

                    // Disable text selection
                    if (!opt.hasTypes && $.support.eventSelectstart) {
                        // browsers support user-select: none,
                        // IE has a special event for text-selection
                        // browsers supporting neither will not be preventing text-selection
                        $t.on('selectstart.disableTextSelect', handle.abortevent);
                    }
                });
                // attach contextMenu to <body> (to bypass any possible overflow:hidden issues on parents of the trigger element)
                if (!opt.$node) {
                    opt.$menu.css('display', 'none').addClass('context-menu-root');
                }
                opt.$menu.appendTo(opt.appendTo || document.body);
            },
            resize: function ($menu, nested) {
                var domMenu;
                // determine widths of submenus, as CSS won't grow them automatically
                // position:absolute within position:absolute; min-width:100; max-width:200; results in width: 100;
                // kinda sucks hard...

                // determine width of absolutely positioned element
                $menu.css({position: 'absolute', display: 'block'});
                // don't apply yet, because that would break nested elements' widths
                $menu.data('width',
                    (domMenu = $menu.get(0)).getBoundingClientRect ?
                        Math.ceil(domMenu.getBoundingClientRect().width) :
                    $menu.outerWidth() + 1); // outerWidth() returns rounded pixels
                // reset styles so they allow nested elements to grow/shrink naturally
                $menu.css({
                    position: 'static',
                    minWidth: '0px',
                    maxWidth: '100000px'
                });
                // identify width of nested menus
                $menu.find('> li > ul').each(function () {
                    op.resize($(this), true);
                });
                // reset and apply changes in the end because nested
                // elements' widths wouldn't be calculatable otherwise
                if (!nested) {
                    $menu.find('ul').addBack().css({
                        position: '',
                        display: '',
                        minWidth: '',
                        maxWidth: ''
                    }).outerWidth(function () {
                        return $(this).data('width');
                    });
                }
            },
            update: function (opt, root) {
                var $trigger = this;
                if (root === undefined) {
                    root = opt;
                    op.resize(opt.$menu);
                }
                // re-check disabled for each item
                opt.$menu.children().each(function () {
                    var $item = $(this),
                        key = $item.data('contextMenuKey'),
                        item = opt.items[key],
                        disabled = ($.isFunction(item.disabled) && item.disabled.call($trigger, key, root)) || item.disabled === true,
                        visible;
                    if ($.isFunction(item.visible)) {
                        visible = item.visible.call($trigger, key, root);
                    } else if (typeof item.visible !== 'undefined') {
                        visible = item.visible === true;
                    } else {
                        visible = true;
                    }
                    $item[visible ? 'show' : 'hide']();

                    // dis- / enable item
                    $item[disabled ? 'addClass' : 'removeClass'](root.classNames.disabled);

                    if ($.isFunction(item.icon)) {
                        $item.removeClass(item._icon);
                        item._icon = item.icon.call(this, $trigger, $item, key, item);
                        $item.addClass(item._icon);
                    }

                    if (item.type) {
                        // dis- / enable input elements
                        $item.find('input, select, textarea').prop('disabled', disabled);

                        // update input states
                        switch (item.type) {
                            case 'text':
                            case 'textarea':
                                item.$input.val(item.value || '');
                                break;

                            case 'checkbox':
                            case 'radio':
                                item.$input.val(item.value || '').prop('checked', !!item.selected);
                                break;

                            case 'select':
                                item.$input.val(item.selected || '');
                                break;
                        }
                    }

                    if (item.$menu) {
                        // update sub-menu
                        op.update.call($trigger, item, root);
                    }
                });
            },
            layer: function (opt, zIndex) {
                // add transparent layer for click area
                // filter and background for Internet Explorer, Issue #23
                var $layer = opt.$layer = $('<div id="context-menu-layer" style="position:fixed; z-index:' + zIndex + '; top:0; left:0; opacity: 0; filter: alpha(opacity=0); background-color: #000;"></div>')
                    .css({height: $win.height(), width: $win.width(), display: 'block'})
                    .data('contextMenuRoot', opt)
                    .insertBefore(this)
                    .on('contextmenu', handle.abortevent)
                    .on('mousedown', handle.layerClick);

                // IE6 doesn't know position:fixed;
                if (document.body.style.maxWidth === undefined) { // IE6 doesn't support maxWidth
                    $layer.css({
                        'position': 'absolute',
                        'height': $(document).height()
                    });
                }

                return $layer;
            }
        };

    // split accesskey according to http://www.whatwg.org/specs/web-apps/current-work/multipage/editing.html#assigned-access-key
    function splitAccesskey(val) {
        var t = val.split(/\s+/),
            keys = [];

        for (var i = 0, k; k = t[i]; i++) {
            k = k.charAt(0).toUpperCase(); // first character only
            // theoretically non-accessible characters should be ignored, but different systems, different keyboard layouts, ... screw it.
            // a map to look up already used access keys would be nice
            keys.push(k);
        }

        return keys;
    }

// handle contextMenu triggers
    $.fn.contextMenu = function (operation) {
        var $t = this, $o = operation;
        if (this.length > 0) {  // this is not a build on demand menu
            if (operation === undefined) {
                this.first().trigger('contextmenu');
            } else if (operation.x !== undefined && operation.y !== undefined) {
                this.first().trigger($.Event('contextmenu', { pageX: operation.x, pageY: operation.y, mouseButton: operation.button }));
            } else if (operation === 'hide') {
                var $menu = this.first().data('contextMenu') ? this.first().data('contextMenu').$menu : null;
                if($menu){
                    $menu.trigger('contextmenu:hide');
                }
            } else if (operation === 'destroy') {
                $.contextMenu('destroy', {context: this});
            } else if ($.isPlainObject(operation)) {
                operation.context = this;
                $.contextMenu('create', operation);
            } else if (operation) {
                this.removeClass('context-menu-disabled');
            } else if (!operation) {
                this.addClass('context-menu-disabled');
            }
        } else {
            $.each(menus, function () {
                if (this.selector === $t.selector) {
                    $o.data = this;

                    $.extend($o.data, {trigger: 'demand'});
                }
            });

            handle.contextmenu.call($o.target, $o);
        }

        return this;
    };

    // manage contextMenu instances
    $.contextMenu = function (operation, options) {
        if (typeof operation !== 'string') {
            options = operation;
            operation = 'create';
        }

        if (typeof options === 'string') {
            options = {selector: options};
        } else if (options === undefined) {
            options = {};
        }

        // merge with default options
        var o = $.extend(true, {}, defaults, options || {});
        var $document = $(document);
        var $context = $document;
        var _hasContext = false;

        if (!o.context || !o.context.length) {
            o.context = document;
        } else {
            // you never know what they throw at you...
            $context = $(o.context).first();
            o.context = $context.get(0);
            _hasContext = o.context !== document;
        }

        switch (operation) {
            case 'create':
                // no selector no joy
                if (!o.selector) {
                    throw new Error('No selector specified');
                }
                // make sure internal classes are not bound to
                if (o.selector.match(/.context-menu-(list|item|input)($|\s)/)) {
                    throw new Error('Cannot bind to selector "' + o.selector + '" as it contains a reserved className');
                }
                if (!o.build && (!o.items || $.isEmptyObject(o.items))) {
                    throw new Error('No Items specified');
                }
                counter++;
                o.ns = '.contextMenu' + counter;
                if (!_hasContext) {
                    namespaces[o.selector] = o.ns;
                }
                menus[o.ns] = o;

                // default to right click
                if (!o.trigger) {
                    o.trigger = 'right';
                }

                if (!initialized) {
                    // make sure item click is registered first
                    $document
                        .on({
                            'contextmenu:hide.contextMenu': handle.hideMenu,
                            'prevcommand.contextMenu': handle.prevItem,
                            'nextcommand.contextMenu': handle.nextItem,
                            'contextmenu.contextMenu': handle.abortevent,
                            'mouseenter.contextMenu': handle.menuMouseenter,
                            'mouseleave.contextMenu': handle.menuMouseleave
                        }, '.context-menu-list')
                        .on('mouseup.contextMenu', '.context-menu-input', handle.inputClick)
                        .on({
                            'mouseup.contextMenu': handle.itemClick,
                            'contextmenu:focus.contextMenu': handle.focusItem,
                            'contextmenu:blur.contextMenu': handle.blurItem,
                            'contextmenu.contextMenu': handle.abortevent,
                            'mouseenter.contextMenu': handle.itemMouseenter,
                            'mouseleave.contextMenu': handle.itemMouseleave
                        }, '.context-menu-item');

                    initialized = true;
                }

                // engage native contextmenu event
                $context
                    .on('contextmenu' + o.ns, o.selector, o, handle.contextmenu);

                if (_hasContext) {
                    // add remove hook, just in case
                    $context.on('remove' + o.ns, function () {
                        $(this).contextMenu('destroy');
                    });
                }

                switch (o.trigger) {
                    case 'hover':
                        $context
                            .on('mouseenter' + o.ns, o.selector, o, handle.mouseenter)
                            .on('mouseleave' + o.ns, o.selector, o, handle.mouseleave);
                        break;

                    case 'left':
                        $context.on('click' + o.ns, o.selector, o, handle.click);
                        break;
                    /*
                     default:
                     // http://www.quirksmode.org/dom/events/contextmenu.html
                     $document
                     .on('mousedown' + o.ns, o.selector, o, handle.mousedown)
                     .on('mouseup' + o.ns, o.selector, o, handle.mouseup);
                     break;
                     */
                }

                // create menu
                if (!o.build) {
                    op.create(o);
                }
                break;

            case 'destroy':
                var $visibleMenu;
                if (_hasContext) {
                    // get proper options
                    var context = o.context;
                    $.each(menus, function (ns, o) {
                        if (o.context !== context) {
                            return true;
                        }

                        $visibleMenu = $('.context-menu-list').filter(':visible');
                        if ($visibleMenu.length && $visibleMenu.data().contextMenuRoot.$trigger.is($(o.context).find(o.selector))) {
                            $visibleMenu.trigger('contextmenu:hide', {force: true});
                        }

                        try {
                            if (menus[o.ns].$menu) {
                                menus[o.ns].$menu.remove();
                            }

                            delete menus[o.ns];
                        } catch (e) {
                            menus[o.ns] = null;
                        }

                        $(o.context).off(o.ns);

                        return true;
                    });
                } else if (!o.selector) {
                    $document.off('.contextMenu .contextMenuAutoHide');
                    $.each(menus, function (ns, o) {
                        $(o.context).off(o.ns);
                    });

                    namespaces = {};
                    menus = {};
                    counter = 0;
                    initialized = false;

                    $('#context-menu-layer, .context-menu-list').remove();
                } else if (namespaces[o.selector]) {
                    $visibleMenu = $('.context-menu-list').filter(':visible');
                    if ($visibleMenu.length && $visibleMenu.data().contextMenuRoot.$trigger.is(o.selector)) {
                        $visibleMenu.trigger('contextmenu:hide', {force: true});
                    }

                    try {
                        if (menus[namespaces[o.selector]].$menu) {
                            menus[namespaces[o.selector]].$menu.remove();
                        }

                        delete menus[namespaces[o.selector]];
                    } catch (e) {
                        menus[namespaces[o.selector]] = null;
                    }

                    $document.off(namespaces[o.selector]);
                }
                break;

            case 'html5':
                // if <command> or <menuitem> are not handled by the browser,
                // or options was a bool true,
                // initialize $.contextMenu for them
                if ((!$.support.htmlCommand && !$.support.htmlMenuitem) || (typeof options === 'boolean' && options)) {
                    $('menu[type="context"]').each(function () {
                        if (this.id) {
                            $.contextMenu({
                                selector: '[contextmenu=' + this.id + ']',
                                items: $.contextMenu.fromMenu(this)
                            });
                        }
                    }).css('display', 'none');
                }
                break;

            default:
                throw new Error('Unknown operation "' + operation + '"');
        }

        return this;
    };

// import values into <input> commands
    $.contextMenu.setInputValues = function (opt, data) {
        if (data === undefined) {
            data = {};
        }

        $.each(opt.inputs, function (key, item) {
            switch (item.type) {
                case 'text':
                case 'textarea':
                    item.value = data[key] || '';
                    break;

                case 'checkbox':
                    item.selected = data[key] ? true : false;
                    break;

                case 'radio':
                    item.selected = (data[item.radio] || '') === item.value;
                    break;

                case 'select':
                    item.selected = data[key] || '';
                    break;
            }
        });
    };

// export values from <input> commands
    $.contextMenu.getInputValues = function (opt, data) {
        if (data === undefined) {
            data = {};
        }

        $.each(opt.inputs, function (key, item) {
            switch (item.type) {
                case 'text':
                case 'textarea':
                case 'select':
                    data[key] = item.$input.val();
                    break;

                case 'checkbox':
                    data[key] = item.$input.prop('checked');
                    break;

                case 'radio':
                    if (item.$input.prop('checked')) {
                        data[item.radio] = item.value;
                    }
                    break;
            }
        });

        return data;
    };

// find <label for="xyz">
    function inputLabel(node) {
        return (node.id && $('label[for="' + node.id + '"]').val()) || node.name;
    }

// convert <menu> to items object
    function menuChildren(items, $children, counter) {
        if (!counter) {
            counter = 0;
        }

        $children.each(function () {
            var $node = $(this),
                node = this,
                nodeName = this.nodeName.toLowerCase(),
                label,
                item;

            // extract <label><input>
            if (nodeName === 'label' && $node.find('input, textarea, select').length) {
                label = $node.text();
                $node = $node.children().first();
                node = $node.get(0);
                nodeName = node.nodeName.toLowerCase();
            }

            /*
             * <menu> accepts flow-content as children. that means <embed>, <canvas> and such are valid menu items.
             * Not being the sadistic kind, $.contextMenu only accepts:
             * <command>, <menuitem>, <hr>, <span>, <p> <input [text, radio, checkbox]>, <textarea>, <select> and of course <menu>.
             * Everything else will be imported as an html node, which is not interfaced with contextMenu.
             */

            // http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#concept-command
            switch (nodeName) {
                // http://www.whatwg.org/specs/web-apps/current-work/multipage/interactive-elements.html#the-menu-element
                case 'menu':
                    item = {name: $node.attr('label'), items: {}};
                    counter = menuChildren(item.items, $node.children(), counter);
                    break;

                // http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#using-the-a-element-to-define-a-command
                case 'a':
                // http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#using-the-button-element-to-define-a-command
                case 'button':
                    item = {
                        name: $node.text(),
                        disabled: !!$node.attr('disabled'),
                        callback: (function () {
                            return function () {
                                $node.click();
                            };
                        })()
                    };
                    break;

                // http://www.whatwg.org/specs/web-apps/current-work/multipage/commands.html#using-the-command-element-to-define-a-command

                case 'menuitem':
                case 'command':
                    switch ($node.attr('type')) {
                        case undefined:
                        case 'command':
                        case 'menuitem':
                            item = {
                                name: $node.attr('label'),
                                disabled: !!$node.attr('disabled'),
                                icon: $node.attr('icon'),
                                callback: (function () {
                                    return function () {
                                        $node.click();
                                    };
                                })()
                            };
                            break;

                        case 'checkbox':
                            item = {
                                type: 'checkbox',
                                disabled: !!$node.attr('disabled'),
                                name: $node.attr('label'),
                                selected: !!$node.attr('checked')
                            };
                            break;
                        case 'radio':
                            item = {
                                type: 'radio',
                                disabled: !!$node.attr('disabled'),
                                name: $node.attr('label'),
                                radio: $node.attr('radiogroup'),
                                value: $node.attr('id'),
                                selected: !!$node.attr('checked')
                            };
                            break;

                        default:
                            item = undefined;
                    }
                    break;

                case 'hr':
                    item = '-------';
                    break;

                case 'input':
                    switch ($node.attr('type')) {
                        case 'text':
                            item = {
                                type: 'text',
                                name: label || inputLabel(node),
                                disabled: !!$node.attr('disabled'),
                                value: $node.val()
                            };
                            break;

                        case 'checkbox':
                            item = {
                                type: 'checkbox',
                                name: label || inputLabel(node),
                                disabled: !!$node.attr('disabled'),
                                selected: !!$node.attr('checked')
                            };
                            break;

                        case 'radio':
                            item = {
                                type: 'radio',
                                name: label || inputLabel(node),
                                disabled: !!$node.attr('disabled'),
                                radio: !!$node.attr('name'),
                                value: $node.val(),
                                selected: !!$node.attr('checked')
                            };
                            break;

                        default:
                            item = undefined;
                            break;
                    }
                    break;

                case 'select':
                    item = {
                        type: 'select',
                        name: label || inputLabel(node),
                        disabled: !!$node.attr('disabled'),
                        selected: $node.val(),
                        options: {}
                    };
                    $node.children().each(function () {
                        item.options[this.value] = $(this).text();
                    });
                    break;

                case 'textarea':
                    item = {
                        type: 'textarea',
                        name: label || inputLabel(node),
                        disabled: !!$node.attr('disabled'),
                        value: $node.val()
                    };
                    break;

                case 'label':
                    break;

                default:
                    item = {type: 'html', html: $node.clone(true)};
                    break;
            }

            if (item) {
                counter++;
                items['key' + counter] = item;
            }
        });

        return counter;
    }

// convert html5 menu
    $.contextMenu.fromMenu = function (element) {
        var $this = $(element),
            items = {};

        menuChildren(items, $this.children());

        return items;
    };

// make defaults accessible
    $.contextMenu.defaults = defaults;
    $.contextMenu.types = types;
// export internal functions - undocumented, for hacking only!
    $.contextMenu.handle = handle;
    $.contextMenu.op = op;
    $.contextMenu.menus = menus;


});

/*! jQuery UI - v1.11.4 - 2016-05-20
* http://jqueryui.com
* Includes: core.js, widget.js, mouse.js, draggable.js, droppable.js, selectable.js
* Copyright jQuery Foundation and other contributors; Licensed MIT */

(function(e){"function"==typeof define&&define.amd?define(["jquery"],e):e(jQuery)})(function(e){function t(t,s){var n,a,o,r=t.nodeName.toLowerCase();return"area"===r?(n=t.parentNode,a=n.name,t.href&&a&&"map"===n.nodeName.toLowerCase()?(o=e("img[usemap='#"+a+"']")[0],!!o&&i(o)):!1):(/^(input|select|textarea|button|object)$/.test(r)?!t.disabled:"a"===r?t.href||s:s)&&i(t)}function i(t){return e.expr.filters.visible(t)&&!e(t).parents().addBack().filter(function(){return"hidden"===e.css(this,"visibility")}).length}e.ui=e.ui||{},e.extend(e.ui,{version:"1.11.4",keyCode:{BACKSPACE:8,COMMA:188,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,LEFT:37,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SPACE:32,TAB:9,UP:38}}),e.fn.extend({scrollParent:function(t){var i=this.css("position"),s="absolute"===i,n=t?/(auto|scroll|hidden)/:/(auto|scroll)/,a=this.parents().filter(function(){var t=e(this);return s&&"static"===t.css("position")?!1:n.test(t.css("overflow")+t.css("overflow-y")+t.css("overflow-x"))}).eq(0);return"fixed"!==i&&a.length?a:e(this[0].ownerDocument||document)},uniqueId:function(){var e=0;return function(){return this.each(function(){this.id||(this.id="ui-id-"+ ++e)})}}(),removeUniqueId:function(){return this.each(function(){/^ui-id-\d+$/.test(this.id)&&e(this).removeAttr("id")})}}),e.extend(e.expr[":"],{data:e.expr.createPseudo?e.expr.createPseudo(function(t){return function(i){return!!e.data(i,t)}}):function(t,i,s){return!!e.data(t,s[3])},focusable:function(i){return t(i,!isNaN(e.attr(i,"tabindex")))},tabbable:function(i){var s=e.attr(i,"tabindex"),n=isNaN(s);return(n||s>=0)&&t(i,!n)}}),e("<a>").outerWidth(1).jquery||e.each(["Width","Height"],function(t,i){function s(t,i,s,a){return e.each(n,function(){i-=parseFloat(e.css(t,"padding"+this))||0,s&&(i-=parseFloat(e.css(t,"border"+this+"Width"))||0),a&&(i-=parseFloat(e.css(t,"margin"+this))||0)}),i}var n="Width"===i?["Left","Right"]:["Top","Bottom"],a=i.toLowerCase(),o={innerWidth:e.fn.innerWidth,innerHeight:e.fn.innerHeight,outerWidth:e.fn.outerWidth,outerHeight:e.fn.outerHeight};e.fn["inner"+i]=function(t){return void 0===t?o["inner"+i].call(this):this.each(function(){e(this).css(a,s(this,t)+"px")})},e.fn["outer"+i]=function(t,n){return"number"!=typeof t?o["outer"+i].call(this,t):this.each(function(){e(this).css(a,s(this,t,!0,n)+"px")})}}),e.fn.addBack||(e.fn.addBack=function(e){return this.add(null==e?this.prevObject:this.prevObject.filter(e))}),e("<a>").data("a-b","a").removeData("a-b").data("a-b")&&(e.fn.removeData=function(t){return function(i){return arguments.length?t.call(this,e.camelCase(i)):t.call(this)}}(e.fn.removeData)),e.ui.ie=!!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase()),e.fn.extend({focus:function(t){return function(i,s){return"number"==typeof i?this.each(function(){var t=this;setTimeout(function(){e(t).focus(),s&&s.call(t)},i)}):t.apply(this,arguments)}}(e.fn.focus),disableSelection:function(){var e="onselectstart"in document.createElement("div")?"selectstart":"mousedown";return function(){return this.bind(e+".ui-disableSelection",function(e){e.preventDefault()})}}(),enableSelection:function(){return this.unbind(".ui-disableSelection")},zIndex:function(t){if(void 0!==t)return this.css("zIndex",t);if(this.length)for(var i,s,n=e(this[0]);n.length&&n[0]!==document;){if(i=n.css("position"),("absolute"===i||"relative"===i||"fixed"===i)&&(s=parseInt(n.css("zIndex"),10),!isNaN(s)&&0!==s))return s;n=n.parent()}return 0}}),e.ui.plugin={add:function(t,i,s){var n,a=e.ui[t].prototype;for(n in s)a.plugins[n]=a.plugins[n]||[],a.plugins[n].push([i,s[n]])},call:function(e,t,i,s){var n,a=e.plugins[t];if(a&&(s||e.element[0].parentNode&&11!==e.element[0].parentNode.nodeType))for(n=0;a.length>n;n++)e.options[a[n][0]]&&a[n][1].apply(e.element,i)}};var s=0,n=Array.prototype.slice;e.cleanData=function(t){return function(i){var s,n,a;for(a=0;null!=(n=i[a]);a++)try{s=e._data(n,"events"),s&&s.remove&&e(n).triggerHandler("remove")}catch(o){}t(i)}}(e.cleanData),e.widget=function(t,i,s){var n,a,o,r,h={},l=t.split(".")[0];return t=t.split(".")[1],n=l+"-"+t,s||(s=i,i=e.Widget),e.expr[":"][n.toLowerCase()]=function(t){return!!e.data(t,n)},e[l]=e[l]||{},a=e[l][t],o=e[l][t]=function(e,t){return this._createWidget?(arguments.length&&this._createWidget(e,t),void 0):new o(e,t)},e.extend(o,a,{version:s.version,_proto:e.extend({},s),_childConstructors:[]}),r=new i,r.options=e.widget.extend({},r.options),e.each(s,function(t,s){return e.isFunction(s)?(h[t]=function(){var e=function(){return i.prototype[t].apply(this,arguments)},n=function(e){return i.prototype[t].apply(this,e)};return function(){var t,i=this._super,a=this._superApply;return this._super=e,this._superApply=n,t=s.apply(this,arguments),this._super=i,this._superApply=a,t}}(),void 0):(h[t]=s,void 0)}),o.prototype=e.widget.extend(r,{widgetEventPrefix:a?r.widgetEventPrefix||t:t},h,{constructor:o,namespace:l,widgetName:t,widgetFullName:n}),a?(e.each(a._childConstructors,function(t,i){var s=i.prototype;e.widget(s.namespace+"."+s.widgetName,o,i._proto)}),delete a._childConstructors):i._childConstructors.push(o),e.widget.bridge(t,o),o},e.widget.extend=function(t){for(var i,s,a=n.call(arguments,1),o=0,r=a.length;r>o;o++)for(i in a[o])s=a[o][i],a[o].hasOwnProperty(i)&&void 0!==s&&(t[i]=e.isPlainObject(s)?e.isPlainObject(t[i])?e.widget.extend({},t[i],s):e.widget.extend({},s):s);return t},e.widget.bridge=function(t,i){var s=i.prototype.widgetFullName||t;e.fn[t]=function(a){var o="string"==typeof a,r=n.call(arguments,1),h=this;return o?this.each(function(){var i,n=e.data(this,s);return"instance"===a?(h=n,!1):n?e.isFunction(n[a])&&"_"!==a.charAt(0)?(i=n[a].apply(n,r),i!==n&&void 0!==i?(h=i&&i.jquery?h.pushStack(i.get()):i,!1):void 0):e.error("no such method '"+a+"' for "+t+" widget instance"):e.error("cannot call methods on "+t+" prior to initialization; "+"attempted to call method '"+a+"'")}):(r.length&&(a=e.widget.extend.apply(null,[a].concat(r))),this.each(function(){var t=e.data(this,s);t?(t.option(a||{}),t._init&&t._init()):e.data(this,s,new i(a,this))})),h}},e.Widget=function(){},e.Widget._childConstructors=[],e.Widget.prototype={widgetName:"widget",widgetEventPrefix:"",defaultElement:"<div>",options:{disabled:!1,create:null},_createWidget:function(t,i){i=e(i||this.defaultElement||this)[0],this.element=e(i),this.uuid=s++,this.eventNamespace="."+this.widgetName+this.uuid,this.bindings=e(),this.hoverable=e(),this.focusable=e(),i!==this&&(e.data(i,this.widgetFullName,this),this._on(!0,this.element,{remove:function(e){e.target===i&&this.destroy()}}),this.document=e(i.style?i.ownerDocument:i.document||i),this.window=e(this.document[0].defaultView||this.document[0].parentWindow)),this.options=e.widget.extend({},this.options,this._getCreateOptions(),t),this._create(),this._trigger("create",null,this._getCreateEventData()),this._init()},_getCreateOptions:e.noop,_getCreateEventData:e.noop,_create:e.noop,_init:e.noop,destroy:function(){this._destroy(),this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(e.camelCase(this.widgetFullName)),this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName+"-disabled "+"ui-state-disabled"),this.bindings.unbind(this.eventNamespace),this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus")},_destroy:e.noop,widget:function(){return this.element},option:function(t,i){var s,n,a,o=t;if(0===arguments.length)return e.widget.extend({},this.options);if("string"==typeof t)if(o={},s=t.split("."),t=s.shift(),s.length){for(n=o[t]=e.widget.extend({},this.options[t]),a=0;s.length-1>a;a++)n[s[a]]=n[s[a]]||{},n=n[s[a]];if(t=s.pop(),1===arguments.length)return void 0===n[t]?null:n[t];n[t]=i}else{if(1===arguments.length)return void 0===this.options[t]?null:this.options[t];o[t]=i}return this._setOptions(o),this},_setOptions:function(e){var t;for(t in e)this._setOption(t,e[t]);return this},_setOption:function(e,t){return this.options[e]=t,"disabled"===e&&(this.widget().toggleClass(this.widgetFullName+"-disabled",!!t),t&&(this.hoverable.removeClass("ui-state-hover"),this.focusable.removeClass("ui-state-focus"))),this},enable:function(){return this._setOptions({disabled:!1})},disable:function(){return this._setOptions({disabled:!0})},_on:function(t,i,s){var n,a=this;"boolean"!=typeof t&&(s=i,i=t,t=!1),s?(i=n=e(i),this.bindings=this.bindings.add(i)):(s=i,i=this.element,n=this.widget()),e.each(s,function(s,o){function r(){return t||a.options.disabled!==!0&&!e(this).hasClass("ui-state-disabled")?("string"==typeof o?a[o]:o).apply(a,arguments):void 0}"string"!=typeof o&&(r.guid=o.guid=o.guid||r.guid||e.guid++);var h=s.match(/^([\w:-]*)\s*(.*)$/),l=h[1]+a.eventNamespace,u=h[2];u?n.delegate(u,l,r):i.bind(l,r)})},_off:function(t,i){i=(i||"").split(" ").join(this.eventNamespace+" ")+this.eventNamespace,t.unbind(i).undelegate(i),this.bindings=e(this.bindings.not(t).get()),this.focusable=e(this.focusable.not(t).get()),this.hoverable=e(this.hoverable.not(t).get())},_delay:function(e,t){function i(){return("string"==typeof e?s[e]:e).apply(s,arguments)}var s=this;return setTimeout(i,t||0)},_hoverable:function(t){this.hoverable=this.hoverable.add(t),this._on(t,{mouseenter:function(t){e(t.currentTarget).addClass("ui-state-hover")},mouseleave:function(t){e(t.currentTarget).removeClass("ui-state-hover")}})},_focusable:function(t){this.focusable=this.focusable.add(t),this._on(t,{focusin:function(t){e(t.currentTarget).addClass("ui-state-focus")},focusout:function(t){e(t.currentTarget).removeClass("ui-state-focus")}})},_trigger:function(t,i,s){var n,a,o=this.options[t];if(s=s||{},i=e.Event(i),i.type=(t===this.widgetEventPrefix?t:this.widgetEventPrefix+t).toLowerCase(),i.target=this.element[0],a=i.originalEvent)for(n in a)n in i||(i[n]=a[n]);return this.element.trigger(i,s),!(e.isFunction(o)&&o.apply(this.element[0],[i].concat(s))===!1||i.isDefaultPrevented())}},e.each({show:"fadeIn",hide:"fadeOut"},function(t,i){e.Widget.prototype["_"+t]=function(s,n,a){"string"==typeof n&&(n={effect:n});var o,r=n?n===!0||"number"==typeof n?i:n.effect||i:t;n=n||{},"number"==typeof n&&(n={duration:n}),o=!e.isEmptyObject(n),n.complete=a,n.delay&&s.delay(n.delay),o&&e.effects&&e.effects.effect[r]?s[t](n):r!==t&&s[r]?s[r](n.duration,n.easing,a):s.queue(function(i){e(this)[t](),a&&a.call(s[0]),i()})}}),e.widget;var a=!1;e(document).mouseup(function(){a=!1}),e.widget("ui.mouse",{version:"1.11.4",options:{cancel:"input,textarea,button,select,option",distance:1,delay:0},_mouseInit:function(){var t=this;this.element.bind("mousedown."+this.widgetName,function(e){return t._mouseDown(e)}).bind("click."+this.widgetName,function(i){return!0===e.data(i.target,t.widgetName+".preventClickEvent")?(e.removeData(i.target,t.widgetName+".preventClickEvent"),i.stopImmediatePropagation(),!1):void 0}),this.started=!1},_mouseDestroy:function(){this.element.unbind("."+this.widgetName),this._mouseMoveDelegate&&this.document.unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate)},_mouseDown:function(t){if(!a){this._mouseMoved=!1,this._mouseStarted&&this._mouseUp(t),this._mouseDownEvent=t;var i=this,s=1===t.which,n="string"==typeof this.options.cancel&&t.target.nodeName?e(t.target).closest(this.options.cancel).length:!1;return s&&!n&&this._mouseCapture(t)?(this.mouseDelayMet=!this.options.delay,this.mouseDelayMet||(this._mouseDelayTimer=setTimeout(function(){i.mouseDelayMet=!0},this.options.delay)),this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=this._mouseStart(t)!==!1,!this._mouseStarted)?(t.preventDefault(),!0):(!0===e.data(t.target,this.widgetName+".preventClickEvent")&&e.removeData(t.target,this.widgetName+".preventClickEvent"),this._mouseMoveDelegate=function(e){return i._mouseMove(e)},this._mouseUpDelegate=function(e){return i._mouseUp(e)},this.document.bind("mousemove."+this.widgetName,this._mouseMoveDelegate).bind("mouseup."+this.widgetName,this._mouseUpDelegate),t.preventDefault(),a=!0,!0)):!0}},_mouseMove:function(t){if(this._mouseMoved){if(e.ui.ie&&(!document.documentMode||9>document.documentMode)&&!t.button)return this._mouseUp(t);if(!t.which)return this._mouseUp(t)}return(t.which||t.button)&&(this._mouseMoved=!0),this._mouseStarted?(this._mouseDrag(t),t.preventDefault()):(this._mouseDistanceMet(t)&&this._mouseDelayMet(t)&&(this._mouseStarted=this._mouseStart(this._mouseDownEvent,t)!==!1,this._mouseStarted?this._mouseDrag(t):this._mouseUp(t)),!this._mouseStarted)},_mouseUp:function(t){return this.document.unbind("mousemove."+this.widgetName,this._mouseMoveDelegate).unbind("mouseup."+this.widgetName,this._mouseUpDelegate),this._mouseStarted&&(this._mouseStarted=!1,t.target===this._mouseDownEvent.target&&e.data(t.target,this.widgetName+".preventClickEvent",!0),this._mouseStop(t)),a=!1,!1},_mouseDistanceMet:function(e){return Math.max(Math.abs(this._mouseDownEvent.pageX-e.pageX),Math.abs(this._mouseDownEvent.pageY-e.pageY))>=this.options.distance},_mouseDelayMet:function(){return this.mouseDelayMet},_mouseStart:function(){},_mouseDrag:function(){},_mouseStop:function(){},_mouseCapture:function(){return!0}}),e.widget("ui.draggable",e.ui.mouse,{version:"1.11.4",widgetEventPrefix:"drag",options:{addClasses:!0,appendTo:"parent",axis:!1,connectToSortable:!1,containment:!1,cursor:"auto",cursorAt:!1,grid:!1,handle:!1,helper:"original",iframeFix:!1,opacity:!1,refreshPositions:!1,revert:!1,revertDuration:500,scope:"default",scroll:!0,scrollSensitivity:20,scrollSpeed:20,snap:!1,snapMode:"both",snapTolerance:20,stack:!1,zIndex:!1,drag:null,start:null,stop:null},_create:function(){"original"===this.options.helper&&this._setPositionRelative(),this.options.addClasses&&this.element.addClass("ui-draggable"),this.options.disabled&&this.element.addClass("ui-draggable-disabled"),this._setHandleClassName(),this._mouseInit()},_setOption:function(e,t){this._super(e,t),"handle"===e&&(this._removeHandleClassName(),this._setHandleClassName())},_destroy:function(){return(this.helper||this.element).is(".ui-draggable-dragging")?(this.destroyOnClear=!0,void 0):(this.element.removeClass("ui-draggable ui-draggable-dragging ui-draggable-disabled"),this._removeHandleClassName(),this._mouseDestroy(),void 0)},_mouseCapture:function(t){var i=this.options;return this._blurActiveElement(t),this.helper||i.disabled||e(t.target).closest(".ui-resizable-handle").length>0?!1:(this.handle=this._getHandle(t),this.handle?(this._blockFrames(i.iframeFix===!0?"iframe":i.iframeFix),!0):!1)},_blockFrames:function(t){this.iframeBlocks=this.document.find(t).map(function(){var t=e(this);return e("<div>").css("position","absolute").appendTo(t.parent()).outerWidth(t.outerWidth()).outerHeight(t.outerHeight()).offset(t.offset())[0]})},_unblockFrames:function(){this.iframeBlocks&&(this.iframeBlocks.remove(),delete this.iframeBlocks)},_blurActiveElement:function(t){var i=this.document[0];if(this.handleElement.is(t.target))try{i.activeElement&&"body"!==i.activeElement.nodeName.toLowerCase()&&e(i.activeElement).blur()}catch(s){}},_mouseStart:function(t){var i=this.options;return this.helper=this._createHelper(t),this.helper.addClass("ui-draggable-dragging"),this._cacheHelperProportions(),e.ui.ddmanager&&(e.ui.ddmanager.current=this),this._cacheMargins(),this.cssPosition=this.helper.css("position"),this.scrollParent=this.helper.scrollParent(!0),this.offsetParent=this.helper.offsetParent(),this.hasFixedAncestor=this.helper.parents().filter(function(){return"fixed"===e(this).css("position")}).length>0,this.positionAbs=this.element.offset(),this._refreshOffsets(t),this.originalPosition=this.position=this._generatePosition(t,!1),this.originalPageX=t.pageX,this.originalPageY=t.pageY,i.cursorAt&&this._adjustOffsetFromHelper(i.cursorAt),this._setContainment(),this._trigger("start",t)===!1?(this._clear(),!1):(this._cacheHelperProportions(),e.ui.ddmanager&&!i.dropBehaviour&&e.ui.ddmanager.prepareOffsets(this,t),this._normalizeRightBottom(),this._mouseDrag(t,!0),e.ui.ddmanager&&e.ui.ddmanager.dragStart(this,t),!0)},_refreshOffsets:function(e){this.offset={top:this.positionAbs.top-this.margins.top,left:this.positionAbs.left-this.margins.left,scroll:!1,parent:this._getParentOffset(),relative:this._getRelativeOffset()},this.offset.click={left:e.pageX-this.offset.left,top:e.pageY-this.offset.top}},_mouseDrag:function(t,i){if(this.hasFixedAncestor&&(this.offset.parent=this._getParentOffset()),this.position=this._generatePosition(t,!0),this.positionAbs=this._convertPositionTo("absolute"),!i){var s=this._uiHash();if(this._trigger("drag",t,s)===!1)return this._mouseUp({}),!1;this.position=s.position}return this.helper[0].style.left=this.position.left+"px",this.helper[0].style.top=this.position.top+"px",e.ui.ddmanager&&e.ui.ddmanager.drag(this,t),!1},_mouseStop:function(t){var i=this,s=!1;return e.ui.ddmanager&&!this.options.dropBehaviour&&(s=e.ui.ddmanager.drop(this,t)),this.dropped&&(s=this.dropped,this.dropped=!1),"invalid"===this.options.revert&&!s||"valid"===this.options.revert&&s||this.options.revert===!0||e.isFunction(this.options.revert)&&this.options.revert.call(this.element,s)?e(this.helper).animate(this.originalPosition,parseInt(this.options.revertDuration,10),function(){i._trigger("stop",t)!==!1&&i._clear()}):this._trigger("stop",t)!==!1&&this._clear(),!1},_mouseUp:function(t){return this._unblockFrames(),e.ui.ddmanager&&e.ui.ddmanager.dragStop(this,t),this.handleElement.is(t.target)&&this.element.focus(),e.ui.mouse.prototype._mouseUp.call(this,t)},cancel:function(){return this.helper.is(".ui-draggable-dragging")?this._mouseUp({}):this._clear(),this},_getHandle:function(t){return this.options.handle?!!e(t.target).closest(this.element.find(this.options.handle)).length:!0},_setHandleClassName:function(){this.handleElement=this.options.handle?this.element.find(this.options.handle):this.element,this.handleElement.addClass("ui-draggable-handle")},_removeHandleClassName:function(){this.handleElement.removeClass("ui-draggable-handle")},_createHelper:function(t){var i=this.options,s=e.isFunction(i.helper),n=s?e(i.helper.apply(this.element[0],[t])):"clone"===i.helper?this.element.clone().removeAttr("id"):this.element;return n.parents("body").length||n.appendTo("parent"===i.appendTo?this.element[0].parentNode:i.appendTo),s&&n[0]===this.element[0]&&this._setPositionRelative(),n[0]===this.element[0]||/(fixed|absolute)/.test(n.css("position"))||n.css("position","absolute"),n},_setPositionRelative:function(){/^(?:r|a|f)/.test(this.element.css("position"))||(this.element[0].style.position="relative")},_adjustOffsetFromHelper:function(t){"string"==typeof t&&(t=t.split(" ")),e.isArray(t)&&(t={left:+t[0],top:+t[1]||0}),"left"in t&&(this.offset.click.left=t.left+this.margins.left),"right"in t&&(this.offset.click.left=this.helperProportions.width-t.right+this.margins.left),"top"in t&&(this.offset.click.top=t.top+this.margins.top),"bottom"in t&&(this.offset.click.top=this.helperProportions.height-t.bottom+this.margins.top)},_isRootNode:function(e){return/(html|body)/i.test(e.tagName)||e===this.document[0]},_getParentOffset:function(){var t=this.offsetParent.offset(),i=this.document[0];return"absolute"===this.cssPosition&&this.scrollParent[0]!==i&&e.contains(this.scrollParent[0],this.offsetParent[0])&&(t.left+=this.scrollParent.scrollLeft(),t.top+=this.scrollParent.scrollTop()),this._isRootNode(this.offsetParent[0])&&(t={top:0,left:0}),{top:t.top+(parseInt(this.offsetParent.css("borderTopWidth"),10)||0),left:t.left+(parseInt(this.offsetParent.css("borderLeftWidth"),10)||0)}},_getRelativeOffset:function(){if("relative"!==this.cssPosition)return{top:0,left:0};var e=this.element.position(),t=this._isRootNode(this.scrollParent[0]);return{top:e.top-(parseInt(this.helper.css("top"),10)||0)+(t?0:this.scrollParent.scrollTop()),left:e.left-(parseInt(this.helper.css("left"),10)||0)+(t?0:this.scrollParent.scrollLeft())}},_cacheMargins:function(){this.margins={left:parseInt(this.element.css("marginLeft"),10)||0,top:parseInt(this.element.css("marginTop"),10)||0,right:parseInt(this.element.css("marginRight"),10)||0,bottom:parseInt(this.element.css("marginBottom"),10)||0}},_cacheHelperProportions:function(){this.helperProportions={width:this.helper.outerWidth(),height:this.helper.outerHeight()}},_setContainment:function(){var t,i,s,n=this.options,a=this.document[0];return this.relativeContainer=null,n.containment?"window"===n.containment?(this.containment=[e(window).scrollLeft()-this.offset.relative.left-this.offset.parent.left,e(window).scrollTop()-this.offset.relative.top-this.offset.parent.top,e(window).scrollLeft()+e(window).width()-this.helperProportions.width-this.margins.left,e(window).scrollTop()+(e(window).height()||a.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top],void 0):"document"===n.containment?(this.containment=[0,0,e(a).width()-this.helperProportions.width-this.margins.left,(e(a).height()||a.body.parentNode.scrollHeight)-this.helperProportions.height-this.margins.top],void 0):n.containment.constructor===Array?(this.containment=n.containment,void 0):("parent"===n.containment&&(n.containment=this.helper[0].parentNode),i=e(n.containment),s=i[0],s&&(t=/(scroll|auto)/.test(i.css("overflow")),this.containment=[(parseInt(i.css("borderLeftWidth"),10)||0)+(parseInt(i.css("paddingLeft"),10)||0),(parseInt(i.css("borderTopWidth"),10)||0)+(parseInt(i.css("paddingTop"),10)||0),(t?Math.max(s.scrollWidth,s.offsetWidth):s.offsetWidth)-(parseInt(i.css("borderRightWidth"),10)||0)-(parseInt(i.css("paddingRight"),10)||0)-this.helperProportions.width-this.margins.left-this.margins.right,(t?Math.max(s.scrollHeight,s.offsetHeight):s.offsetHeight)-(parseInt(i.css("borderBottomWidth"),10)||0)-(parseInt(i.css("paddingBottom"),10)||0)-this.helperProportions.height-this.margins.top-this.margins.bottom],this.relativeContainer=i),void 0):(this.containment=null,void 0)},_convertPositionTo:function(e,t){t||(t=this.position);var i="absolute"===e?1:-1,s=this._isRootNode(this.scrollParent[0]);return{top:t.top+this.offset.relative.top*i+this.offset.parent.top*i-("fixed"===this.cssPosition?-this.offset.scroll.top:s?0:this.offset.scroll.top)*i,left:t.left+this.offset.relative.left*i+this.offset.parent.left*i-("fixed"===this.cssPosition?-this.offset.scroll.left:s?0:this.offset.scroll.left)*i}},_generatePosition:function(e,t){var i,s,n,a,o=this.options,r=this._isRootNode(this.scrollParent[0]),h=e.pageX,l=e.pageY;return r&&this.offset.scroll||(this.offset.scroll={top:this.scrollParent.scrollTop(),left:this.scrollParent.scrollLeft()}),t&&(this.containment&&(this.relativeContainer?(s=this.relativeContainer.offset(),i=[this.containment[0]+s.left,this.containment[1]+s.top,this.containment[2]+s.left,this.containment[3]+s.top]):i=this.containment,e.pageX-this.offset.click.left<i[0]&&(h=i[0]+this.offset.click.left),e.pageY-this.offset.click.top<i[1]&&(l=i[1]+this.offset.click.top),e.pageX-this.offset.click.left>i[2]&&(h=i[2]+this.offset.click.left),e.pageY-this.offset.click.top>i[3]&&(l=i[3]+this.offset.click.top)),o.grid&&(n=o.grid[1]?this.originalPageY+Math.round((l-this.originalPageY)/o.grid[1])*o.grid[1]:this.originalPageY,l=i?n-this.offset.click.top>=i[1]||n-this.offset.click.top>i[3]?n:n-this.offset.click.top>=i[1]?n-o.grid[1]:n+o.grid[1]:n,a=o.grid[0]?this.originalPageX+Math.round((h-this.originalPageX)/o.grid[0])*o.grid[0]:this.originalPageX,h=i?a-this.offset.click.left>=i[0]||a-this.offset.click.left>i[2]?a:a-this.offset.click.left>=i[0]?a-o.grid[0]:a+o.grid[0]:a),"y"===o.axis&&(h=this.originalPageX),"x"===o.axis&&(l=this.originalPageY)),{top:l-this.offset.click.top-this.offset.relative.top-this.offset.parent.top+("fixed"===this.cssPosition?-this.offset.scroll.top:r?0:this.offset.scroll.top),left:h-this.offset.click.left-this.offset.relative.left-this.offset.parent.left+("fixed"===this.cssPosition?-this.offset.scroll.left:r?0:this.offset.scroll.left)}},_clear:function(){this.helper.removeClass("ui-draggable-dragging"),this.helper[0]===this.element[0]||this.cancelHelperRemoval||this.helper.remove(),this.helper=null,this.cancelHelperRemoval=!1,this.destroyOnClear&&this.destroy()},_normalizeRightBottom:function(){"y"!==this.options.axis&&"auto"!==this.helper.css("right")&&(this.helper.width(this.helper.width()),this.helper.css("right","auto")),"x"!==this.options.axis&&"auto"!==this.helper.css("bottom")&&(this.helper.height(this.helper.height()),this.helper.css("bottom","auto"))},_trigger:function(t,i,s){return s=s||this._uiHash(),e.ui.plugin.call(this,t,[i,s,this],!0),/^(drag|start|stop)/.test(t)&&(this.positionAbs=this._convertPositionTo("absolute"),s.offset=this.positionAbs),e.Widget.prototype._trigger.call(this,t,i,s)},plugins:{},_uiHash:function(){return{helper:this.helper,position:this.position,originalPosition:this.originalPosition,offset:this.positionAbs}}}),e.ui.plugin.add("draggable","connectToSortable",{start:function(t,i,s){var n=e.extend({},i,{item:s.element});s.sortables=[],e(s.options.connectToSortable).each(function(){var i=e(this).sortable("instance");i&&!i.options.disabled&&(s.sortables.push(i),i.refreshPositions(),i._trigger("activate",t,n))})},stop:function(t,i,s){var n=e.extend({},i,{item:s.element});s.cancelHelperRemoval=!1,e.each(s.sortables,function(){var e=this;e.isOver?(e.isOver=0,s.cancelHelperRemoval=!0,e.cancelHelperRemoval=!1,e._storedCSS={position:e.placeholder.css("position"),top:e.placeholder.css("top"),left:e.placeholder.css("left")},e._mouseStop(t),e.options.helper=e.options._helper):(e.cancelHelperRemoval=!0,e._trigger("deactivate",t,n))})},drag:function(t,i,s){e.each(s.sortables,function(){var n=!1,a=this;a.positionAbs=s.positionAbs,a.helperProportions=s.helperProportions,a.offset.click=s.offset.click,a._intersectsWith(a.containerCache)&&(n=!0,e.each(s.sortables,function(){return this.positionAbs=s.positionAbs,this.helperProportions=s.helperProportions,this.offset.click=s.offset.click,this!==a&&this._intersectsWith(this.containerCache)&&e.contains(a.element[0],this.element[0])&&(n=!1),n})),n?(a.isOver||(a.isOver=1,s._parent=i.helper.parent(),a.currentItem=i.helper.appendTo(a.element).data("ui-sortable-item",!0),a.options._helper=a.options.helper,a.options.helper=function(){return i.helper[0]},t.target=a.currentItem[0],a._mouseCapture(t,!0),a._mouseStart(t,!0,!0),a.offset.click.top=s.offset.click.top,a.offset.click.left=s.offset.click.left,a.offset.parent.left-=s.offset.parent.left-a.offset.parent.left,a.offset.parent.top-=s.offset.parent.top-a.offset.parent.top,s._trigger("toSortable",t),s.dropped=a.element,e.each(s.sortables,function(){this.refreshPositions()}),s.currentItem=s.element,a.fromOutside=s),a.currentItem&&(a._mouseDrag(t),i.position=a.position)):a.isOver&&(a.isOver=0,a.cancelHelperRemoval=!0,a.options._revert=a.options.revert,a.options.revert=!1,a._trigger("out",t,a._uiHash(a)),a._mouseStop(t,!0),a.options.revert=a.options._revert,a.options.helper=a.options._helper,a.placeholder&&a.placeholder.remove(),i.helper.appendTo(s._parent),s._refreshOffsets(t),i.position=s._generatePosition(t,!0),s._trigger("fromSortable",t),s.dropped=!1,e.each(s.sortables,function(){this.refreshPositions()}))})}}),e.ui.plugin.add("draggable","cursor",{start:function(t,i,s){var n=e("body"),a=s.options;n.css("cursor")&&(a._cursor=n.css("cursor")),n.css("cursor",a.cursor)},stop:function(t,i,s){var n=s.options;n._cursor&&e("body").css("cursor",n._cursor)}}),e.ui.plugin.add("draggable","opacity",{start:function(t,i,s){var n=e(i.helper),a=s.options;n.css("opacity")&&(a._opacity=n.css("opacity")),n.css("opacity",a.opacity)},stop:function(t,i,s){var n=s.options;n._opacity&&e(i.helper).css("opacity",n._opacity)}}),e.ui.plugin.add("draggable","scroll",{start:function(e,t,i){i.scrollParentNotHidden||(i.scrollParentNotHidden=i.helper.scrollParent(!1)),i.scrollParentNotHidden[0]!==i.document[0]&&"HTML"!==i.scrollParentNotHidden[0].tagName&&(i.overflowOffset=i.scrollParentNotHidden.offset())},drag:function(t,i,s){var n=s.options,a=!1,o=s.scrollParentNotHidden[0],r=s.document[0];o!==r&&"HTML"!==o.tagName?(n.axis&&"x"===n.axis||(s.overflowOffset.top+o.offsetHeight-t.pageY<n.scrollSensitivity?o.scrollTop=a=o.scrollTop+n.scrollSpeed:t.pageY-s.overflowOffset.top<n.scrollSensitivity&&(o.scrollTop=a=o.scrollTop-n.scrollSpeed)),n.axis&&"y"===n.axis||(s.overflowOffset.left+o.offsetWidth-t.pageX<n.scrollSensitivity?o.scrollLeft=a=o.scrollLeft+n.scrollSpeed:t.pageX-s.overflowOffset.left<n.scrollSensitivity&&(o.scrollLeft=a=o.scrollLeft-n.scrollSpeed))):(n.axis&&"x"===n.axis||(t.pageY-e(r).scrollTop()<n.scrollSensitivity?a=e(r).scrollTop(e(r).scrollTop()-n.scrollSpeed):e(window).height()-(t.pageY-e(r).scrollTop())<n.scrollSensitivity&&(a=e(r).scrollTop(e(r).scrollTop()+n.scrollSpeed))),n.axis&&"y"===n.axis||(t.pageX-e(r).scrollLeft()<n.scrollSensitivity?a=e(r).scrollLeft(e(r).scrollLeft()-n.scrollSpeed):e(window).width()-(t.pageX-e(r).scrollLeft())<n.scrollSensitivity&&(a=e(r).scrollLeft(e(r).scrollLeft()+n.scrollSpeed)))),a!==!1&&e.ui.ddmanager&&!n.dropBehaviour&&e.ui.ddmanager.prepareOffsets(s,t)}}),e.ui.plugin.add("draggable","snap",{start:function(t,i,s){var n=s.options;s.snapElements=[],e(n.snap.constructor!==String?n.snap.items||":data(ui-draggable)":n.snap).each(function(){var t=e(this),i=t.offset();this!==s.element[0]&&s.snapElements.push({item:this,width:t.outerWidth(),height:t.outerHeight(),top:i.top,left:i.left})})},drag:function(t,i,s){var n,a,o,r,h,l,u,d,c,p,f=s.options,m=f.snapTolerance,g=i.offset.left,v=g+s.helperProportions.width,_=i.offset.top,b=_+s.helperProportions.height;for(c=s.snapElements.length-1;c>=0;c--)h=s.snapElements[c].left-s.margins.left,l=h+s.snapElements[c].width,u=s.snapElements[c].top-s.margins.top,d=u+s.snapElements[c].height,h-m>v||g>l+m||u-m>b||_>d+m||!e.contains(s.snapElements[c].item.ownerDocument,s.snapElements[c].item)?(s.snapElements[c].snapping&&s.options.snap.release&&s.options.snap.release.call(s.element,t,e.extend(s._uiHash(),{snapItem:s.snapElements[c].item})),s.snapElements[c].snapping=!1):("inner"!==f.snapMode&&(n=m>=Math.abs(u-b),a=m>=Math.abs(d-_),o=m>=Math.abs(h-v),r=m>=Math.abs(l-g),n&&(i.position.top=s._convertPositionTo("relative",{top:u-s.helperProportions.height,left:0}).top),a&&(i.position.top=s._convertPositionTo("relative",{top:d,left:0}).top),o&&(i.position.left=s._convertPositionTo("relative",{top:0,left:h-s.helperProportions.width}).left),r&&(i.position.left=s._convertPositionTo("relative",{top:0,left:l}).left)),p=n||a||o||r,"outer"!==f.snapMode&&(n=m>=Math.abs(u-_),a=m>=Math.abs(d-b),o=m>=Math.abs(h-g),r=m>=Math.abs(l-v),n&&(i.position.top=s._convertPositionTo("relative",{top:u,left:0}).top),a&&(i.position.top=s._convertPositionTo("relative",{top:d-s.helperProportions.height,left:0}).top),o&&(i.position.left=s._convertPositionTo("relative",{top:0,left:h}).left),r&&(i.position.left=s._convertPositionTo("relative",{top:0,left:l-s.helperProportions.width}).left)),!s.snapElements[c].snapping&&(n||a||o||r||p)&&s.options.snap.snap&&s.options.snap.snap.call(s.element,t,e.extend(s._uiHash(),{snapItem:s.snapElements[c].item})),s.snapElements[c].snapping=n||a||o||r||p)}}),e.ui.plugin.add("draggable","stack",{start:function(t,i,s){var n,a=s.options,o=e.makeArray(e(a.stack)).sort(function(t,i){return(parseInt(e(t).css("zIndex"),10)||0)-(parseInt(e(i).css("zIndex"),10)||0)});o.length&&(n=parseInt(e(o[0]).css("zIndex"),10)||0,e(o).each(function(t){e(this).css("zIndex",n+t)}),this.css("zIndex",n+o.length))}}),e.ui.plugin.add("draggable","zIndex",{start:function(t,i,s){var n=e(i.helper),a=s.options;n.css("zIndex")&&(a._zIndex=n.css("zIndex")),n.css("zIndex",a.zIndex)},stop:function(t,i,s){var n=s.options;n._zIndex&&e(i.helper).css("zIndex",n._zIndex)}}),e.ui.draggable,e.widget("ui.droppable",{version:"1.11.4",widgetEventPrefix:"drop",options:{accept:"*",activeClass:!1,addClasses:!0,greedy:!1,hoverClass:!1,scope:"default",tolerance:"intersect",activate:null,deactivate:null,drop:null,out:null,over:null},_create:function(){var t,i=this.options,s=i.accept;
this.isover=!1,this.isout=!0,this.accept=e.isFunction(s)?s:function(e){return e.is(s)},this.proportions=function(){return arguments.length?(t=arguments[0],void 0):t?t:t={width:this.element[0].offsetWidth,height:this.element[0].offsetHeight}},this._addToManager(i.scope),i.addClasses&&this.element.addClass("ui-droppable")},_addToManager:function(t){e.ui.ddmanager.droppables[t]=e.ui.ddmanager.droppables[t]||[],e.ui.ddmanager.droppables[t].push(this)},_splice:function(e){for(var t=0;e.length>t;t++)e[t]===this&&e.splice(t,1)},_destroy:function(){var t=e.ui.ddmanager.droppables[this.options.scope];this._splice(t),this.element.removeClass("ui-droppable ui-droppable-disabled")},_setOption:function(t,i){if("accept"===t)this.accept=e.isFunction(i)?i:function(e){return e.is(i)};else if("scope"===t){var s=e.ui.ddmanager.droppables[this.options.scope];this._splice(s),this._addToManager(i)}this._super(t,i)},_activate:function(t){var i=e.ui.ddmanager.current;this.options.activeClass&&this.element.addClass(this.options.activeClass),i&&this._trigger("activate",t,this.ui(i))},_deactivate:function(t){var i=e.ui.ddmanager.current;this.options.activeClass&&this.element.removeClass(this.options.activeClass),i&&this._trigger("deactivate",t,this.ui(i))},_over:function(t){var i=e.ui.ddmanager.current;i&&(i.currentItem||i.element)[0]!==this.element[0]&&this.accept.call(this.element[0],i.currentItem||i.element)&&(this.options.hoverClass&&this.element.addClass(this.options.hoverClass),this._trigger("over",t,this.ui(i)))},_out:function(t){var i=e.ui.ddmanager.current;i&&(i.currentItem||i.element)[0]!==this.element[0]&&this.accept.call(this.element[0],i.currentItem||i.element)&&(this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("out",t,this.ui(i)))},_drop:function(t,i){var s=i||e.ui.ddmanager.current,n=!1;return s&&(s.currentItem||s.element)[0]!==this.element[0]?(this.element.find(":data(ui-droppable)").not(".ui-draggable-dragging").each(function(){var i=e(this).droppable("instance");return i.options.greedy&&!i.options.disabled&&i.options.scope===s.options.scope&&i.accept.call(i.element[0],s.currentItem||s.element)&&e.ui.intersect(s,e.extend(i,{offset:i.element.offset()}),i.options.tolerance,t)?(n=!0,!1):void 0}),n?!1:this.accept.call(this.element[0],s.currentItem||s.element)?(this.options.activeClass&&this.element.removeClass(this.options.activeClass),this.options.hoverClass&&this.element.removeClass(this.options.hoverClass),this._trigger("drop",t,this.ui(s)),this.element):!1):!1},ui:function(e){return{draggable:e.currentItem||e.element,helper:e.helper,position:e.position,offset:e.positionAbs}}}),e.ui.intersect=function(){function e(e,t,i){return e>=t&&t+i>e}return function(t,i,s,n){if(!i.offset)return!1;var a=(t.positionAbs||t.position.absolute).left+t.margins.left,o=(t.positionAbs||t.position.absolute).top+t.margins.top,r=a+t.helperProportions.width,h=o+t.helperProportions.height,l=i.offset.left,u=i.offset.top,d=l+i.proportions().width,c=u+i.proportions().height;switch(s){case"fit":return a>=l&&d>=r&&o>=u&&c>=h;case"intersect":return a+t.helperProportions.width/2>l&&d>r-t.helperProportions.width/2&&o+t.helperProportions.height/2>u&&c>h-t.helperProportions.height/2;case"pointer":return e(n.pageY,u,i.proportions().height)&&e(n.pageX,l,i.proportions().width);case"touch":return(o>=u&&c>=o||h>=u&&c>=h||u>o&&h>c)&&(a>=l&&d>=a||r>=l&&d>=r||l>a&&r>d);default:return!1}}}(),e.ui.ddmanager={current:null,droppables:{"default":[]},prepareOffsets:function(t,i){var s,n,a=e.ui.ddmanager.droppables[t.options.scope]||[],o=i?i.type:null,r=(t.currentItem||t.element).find(":data(ui-droppable)").addBack();e:for(s=0;a.length>s;s++)if(!(a[s].options.disabled||t&&!a[s].accept.call(a[s].element[0],t.currentItem||t.element))){for(n=0;r.length>n;n++)if(r[n]===a[s].element[0]){a[s].proportions().height=0;continue e}a[s].visible="none"!==a[s].element.css("display"),a[s].visible&&("mousedown"===o&&a[s]._activate.call(a[s],i),a[s].offset=a[s].element.offset(),a[s].proportions({width:a[s].element[0].offsetWidth,height:a[s].element[0].offsetHeight}))}},drop:function(t,i){var s=!1;return e.each((e.ui.ddmanager.droppables[t.options.scope]||[]).slice(),function(){this.options&&(!this.options.disabled&&this.visible&&e.ui.intersect(t,this,this.options.tolerance,i)&&(s=this._drop.call(this,i)||s),!this.options.disabled&&this.visible&&this.accept.call(this.element[0],t.currentItem||t.element)&&(this.isout=!0,this.isover=!1,this._deactivate.call(this,i)))}),s},dragStart:function(t,i){t.element.parentsUntil("body").bind("scroll.droppable",function(){t.options.refreshPositions||e.ui.ddmanager.prepareOffsets(t,i)})},drag:function(t,i){t.options.refreshPositions&&e.ui.ddmanager.prepareOffsets(t,i),e.each(e.ui.ddmanager.droppables[t.options.scope]||[],function(){if(!this.options.disabled&&!this.greedyChild&&this.visible){var s,n,a,o=e.ui.intersect(t,this,this.options.tolerance,i),r=!o&&this.isover?"isout":o&&!this.isover?"isover":null;r&&(this.options.greedy&&(n=this.options.scope,a=this.element.parents(":data(ui-droppable)").filter(function(){return e(this).droppable("instance").options.scope===n}),a.length&&(s=e(a[0]).droppable("instance"),s.greedyChild="isover"===r)),s&&"isover"===r&&(s.isover=!1,s.isout=!0,s._out.call(s,i)),this[r]=!0,this["isout"===r?"isover":"isout"]=!1,this["isover"===r?"_over":"_out"].call(this,i),s&&"isout"===r&&(s.isout=!1,s.isover=!0,s._over.call(s,i)))}})},dragStop:function(t,i){t.element.parentsUntil("body").unbind("scroll.droppable"),t.options.refreshPositions||e.ui.ddmanager.prepareOffsets(t,i)}},e.ui.droppable,e.widget("ui.selectable",e.ui.mouse,{version:"1.11.4",options:{appendTo:"body",autoRefresh:!0,distance:0,filter:"*",tolerance:"touch",selected:null,selecting:null,start:null,stop:null,unselected:null,unselecting:null},_create:function(){var t,i=this;this.element.addClass("ui-selectable"),this.dragged=!1,this.refresh=function(){t=e(i.options.filter,i.element[0]),t.addClass("ui-selectee"),t.each(function(){var t=e(this),i=t.offset();e.data(this,"selectable-item",{element:this,$element:t,left:i.left,top:i.top,right:i.left+t.outerWidth(),bottom:i.top+t.outerHeight(),startselected:!1,selected:t.hasClass("ui-selected"),selecting:t.hasClass("ui-selecting"),unselecting:t.hasClass("ui-unselecting")})})},this.refresh(),this.selectees=t.addClass("ui-selectee"),this._mouseInit(),this.helper=e("<div class='ui-selectable-helper'></div>")},_destroy:function(){this.selectees.removeClass("ui-selectee").removeData("selectable-item"),this.element.removeClass("ui-selectable ui-selectable-disabled"),this._mouseDestroy()},_mouseStart:function(t){var i=this,s=this.options;this.opos=[t.pageX,t.pageY],this.options.disabled||(this.selectees=e(s.filter,this.element[0]),this._trigger("start",t),e(s.appendTo).append(this.helper),this.helper.css({left:t.pageX,top:t.pageY,width:0,height:0}),s.autoRefresh&&this.refresh(),this.selectees.filter(".ui-selected").each(function(){var s=e.data(this,"selectable-item");s.startselected=!0,t.metaKey||t.ctrlKey||(s.$element.removeClass("ui-selected"),s.selected=!1,s.$element.addClass("ui-unselecting"),s.unselecting=!0,i._trigger("unselecting",t,{unselecting:s.element}))}),e(t.target).parents().addBack().each(function(){var s,n=e.data(this,"selectable-item");return n?(s=!t.metaKey&&!t.ctrlKey||!n.$element.hasClass("ui-selected"),n.$element.removeClass(s?"ui-unselecting":"ui-selected").addClass(s?"ui-selecting":"ui-unselecting"),n.unselecting=!s,n.selecting=s,n.selected=s,s?i._trigger("selecting",t,{selecting:n.element}):i._trigger("unselecting",t,{unselecting:n.element}),!1):void 0}))},_mouseDrag:function(t){if(this.dragged=!0,!this.options.disabled){var i,s=this,n=this.options,a=this.opos[0],o=this.opos[1],r=t.pageX,h=t.pageY;return a>r&&(i=r,r=a,a=i),o>h&&(i=h,h=o,o=i),this.helper.css({left:a,top:o,width:r-a,height:h-o}),this.selectees.each(function(){var i=e.data(this,"selectable-item"),l=!1;i&&i.element!==s.element[0]&&("touch"===n.tolerance?l=!(i.left>r||a>i.right||i.top>h||o>i.bottom):"fit"===n.tolerance&&(l=i.left>a&&r>i.right&&i.top>o&&h>i.bottom),l?(i.selected&&(i.$element.removeClass("ui-selected"),i.selected=!1),i.unselecting&&(i.$element.removeClass("ui-unselecting"),i.unselecting=!1),i.selecting||(i.$element.addClass("ui-selecting"),i.selecting=!0,s._trigger("selecting",t,{selecting:i.element}))):(i.selecting&&((t.metaKey||t.ctrlKey)&&i.startselected?(i.$element.removeClass("ui-selecting"),i.selecting=!1,i.$element.addClass("ui-selected"),i.selected=!0):(i.$element.removeClass("ui-selecting"),i.selecting=!1,i.startselected&&(i.$element.addClass("ui-unselecting"),i.unselecting=!0),s._trigger("unselecting",t,{unselecting:i.element}))),i.selected&&(t.metaKey||t.ctrlKey||i.startselected||(i.$element.removeClass("ui-selected"),i.selected=!1,i.$element.addClass("ui-unselecting"),i.unselecting=!0,s._trigger("unselecting",t,{unselecting:i.element})))))}),!1}},_mouseStop:function(t){var i=this;return this.dragged=!1,e(".ui-unselecting",this.element[0]).each(function(){var s=e.data(this,"selectable-item");s.$element.removeClass("ui-unselecting"),s.unselecting=!1,s.startselected=!1,i._trigger("unselected",t,{unselected:s.element})}),e(".ui-selecting",this.element[0]).each(function(){var s=e.data(this,"selectable-item");s.$element.removeClass("ui-selecting").addClass("ui-selected"),s.selecting=!1,s.selected=!0,s.startselected=!0,i._trigger("selected",t,{selected:s.element})}),this._trigger("stop",t),this.helper.remove(),!1}})});
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

/**
 * Created by User on 20-05-2016.
 */

$(function () {
    var StorageLib = function StorageLib() {
        this.init();
    };

    StorageLib.prototype.init = function () {
        if (typeof Storage == "undefined") {

            console.log('no-support');
        }
    };

    $.setStorageObject = function (key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    };

    $.getStorageObject = function (key) {
        return JSON.parse(localStorage.getItem(key));
    };

    $.clearStorageObjects = function (key) {
        localStorage.removeItem(key);
    };

    $.storagelib = function () {
        return new StorageLib();
    };
});

},{}]},{},[1]);

//# sourceMappingURL=local_storage.js.map

/**
 * Created by User on 13-05-2016.
 */
(function($) {
    "use strict";
    var SelectSlideMenu = function(element, options) {
        this.$el = $(element);
        this.options = options || {};
        console.log(this.options);
        this.init();
        var menu = this;
        $('.back_destiny').click(function() {
            if ($(this).attr('data-id') !== 0) {
                menu.open_folder($(this).attr('data-id'));
            }

        });

        $('.btn_save_folder').click(function() {
            
            if (options.gallery === 0) {
                menu.setFinalFolder($(this).attr('data-id'));
                $('.listagem_pastas').hide();
            }



        });

        $('.new_folder_here').click(function() {
            $('#pasta_atual_pai').val($('.pasta_atual').attr('data-id'));

            $('.nova_pasta_escond').show();
            $('.input_pasta').focus().select();
            $('.new_folder_selected').show();
        });

        $('.btn_cancel_new_folder').click(function() {
            $('.new_folder_selected').hide();
            $('.nova_pasta_escond').hide();

        });

        $('.btn_save_new_folder').click(function() {


            if ($.trim($('.input_pasta').val())) {

                $.ajax({
                    type: "post",
                    data: { parent_folder_id: $('#pasta_atual_pai').val(), title: $('.input_pasta').val() },
                    url: $(this).attr('data-href'),
                    headers: {
                        'X-CSRF-TOKEN': menu.options.token
                    },
                    success: function(data) {
                        $('.new_folder_selected').hide();
                        $('.nova_pasta_escond').hide();

                        menu.options.dados = data.conteudo;
                        menu.open_folder(data.currnt_f);

                        /*$('.listagem_pastas').selectslidemenu({ dados: data.conteudo,inicial:data.currnt_f });*/
                        /* menu.open_folder(data.currnt_f);*/



                    }
                });


            } else {
                console.log('nao submete');
            }


        });


    };



    SelectSlideMenu.prototype.init = function() {

        var menu = this;
        var inicial = this.options.inicial;

        menu.open_folder(inicial);
        this.setFinalFolder(inicial);


    };


    SelectSlideMenu.prototype.open_folder = function(inicial) {
        var menu = this;
        var dados = jQuery.parseJSON(this.options.dados);
        var galery = this.options.gallery;
        if (galery == inicial) {
            //hide the save button
            $('.innactive_save').show();
        } else {
            $('.innactive_save').hide();
        }

        var folders = $.grep(dados, function(element, index) {
            return element.parent_folder_id == inicial;
        });


        this.setCurrentFolder(inicial);
        this.setSubfolders(folders);
        this.setPreviousFolder(inicial);


    };


    SelectSlideMenu.prototype.setFinalFolder = function(inicial) {


        var dados = jQuery.parseJSON(this.options.dados);
        var pasta = $.grep(dados, function(element, index) {
            return element.id == inicial;
        });

        $('.btn_save_folder').attr('data-id', pasta[0].id);
        $('#dest_f').text(pasta[0].title);
        $('#dest_f_inp').val(pasta[0].id);
        $('*[id^="dest_f_"]').text(pasta[0].title);
        $('*[id^="dest_f_inp_"]').val(pasta[0].id);
    };


    SelectSlideMenu.prototype.setSubfolders = function(folders) {

        var menu = this;
        $('.destinys').empty();

        var blocked = this.options.blocked_folders;


        console.log(blocked);

        if (folders.length === 0) {
            $('.destinys').append('' +
                '<li class="empty-folder">' +
                'A pasta está vazia.' +
                '</li>');
        } else {
            $.each(folders, function(index, value) {
                if ($.inArray("" + value.id + "", blocked) != -1) {
                    $('.destinys').append('' +
                        '<li class="selected_item innactive_option">' +
                        '<div class="innactive_option"></div>' +
                        '<i class="icon-icon-peq-pasta icon_titulo_pasta"></i>' + value.title +
                        '<a class="go_to_folder" data-id="' + value.id + '"><i class="icon-seta-direita"></i></a>' +
                        '</li>');

                } else {
                    $('.destinys').append('' +
                        '<li id="' + 'li-select-' + index + '" class="selected_item">' +
                        '<div></div>' +
                        '<i class="icon-icon-peq-pasta icon_titulo_pasta"></i>' + value.title +
                        '<a class="go_to_folder" data-id="' + value.id + '"><i class="icon-seta-direita"></i></a>' +
                        '</li>');
                }
            });

            $("li[id^='li-select-']").click(function(event) {
                var $element = $(event.target);

                var isRootFolder = $('.back_destiny').attr('data-id'),
                    currentFolder = $element.first().hasClass('innactive_option');

                var $btnSaveFolder = $('.btn_save_folder');

                if (isRootFolder  == '0' || currentFolder) {
                    return;
                } else {
                    $element.toggleClass('selectedfilter');

                    if ($element.hasClass('selectedfilter')) {
                        $btnSaveFolder.text('GUARDAR')
                                      .attr({ 'data-id': $($element).children().last().attr('data-id') });
                    } else {
                        $btnSaveFolder.text('GUARDAR AQUI')
                                      .attr({ 'data-id': $('.pasta_atual').attr('data-id') });
                    }
                }
            });
        }

        $('.go_to_folder').click(function() {
            menu.open_folder($(this).attr('data-id'));
        });




    };
    SelectSlideMenu.prototype.setPreviousFolder = function(inicial) {


        var dados = jQuery.parseJSON(this.options.dados);
        var pasta = $.grep(dados, function(element, index) {
            return element.id == inicial;
        });

        console.log(pasta[0].parent_folder_id);

        if (pasta[0].parent_folder_id !== 0) {
            $('.back_destiny').removeClass('opacidade_back');
            $('.btn_save_folder').removeClass('opacidade_create');

            $('.back_destiny').attr('data-id', pasta[0].parent_folder_id);
        } else {
            $('.back_destiny').attr('data-id', 0);
            $('.back_destiny').addClass('opacidade_back');
            $('.btn_save_folder').addClass('opacidade_create');
        }



    };


    SelectSlideMenu.prototype.setCurrentFolder = function(current) {
        var dados = jQuery.parseJSON(this.options.dados);
        var pasta = $.grep(dados, function(element, index) {
            return element.id == current;
        });

        $('.pasta_atual').html(pasta[0].title);
        $('.pasta_atual').attr('data-id', pasta[0].id);

        if (pasta[0].parent_folder_id === 0) {
            $('.innactive_btns').show();
        } else {
            $('.innactive_btns').hide();
            $('.btn_save_folder').text('GUARDAR AQUI').attr('data-id', pasta[0].id);
        }

    };













    // -------------------PLUGIN DEFINITION-----------------------------------
    SelectSlideMenu.DEFAULTS = {
        dados: null,
        inicial: null,
        token: null,
        gallery: 0,
        blocked_folders: 0

    };

    var old = $.fn.selectslidemenu;

    $.fn.selectslidemenu = function(option) {
        var args = arguments;

        return this.each(function() {
            var $this = $(this);
            var data = $this.data('oc.selectslidemenu');

            var options = $.extend({}, SelectSlideMenu.DEFAULTS, $this.data(), typeof option == 'object' && option);
            if (!data) {

            }

            $this.data('oc.selectslidemenu', (data = new SelectSlideMenu(this, options)));

            /*var options =  (!option)? SelectSlideMenu.DEFAULTS: option*/

            /* new SelectSlideMenu(this, options)*/
        });
    };

    $.fn.selectslidemenu.Constructor = SelectSlideMenu;

    $.fn.selectslidemenu.noConflict = function() {
        $.fn.selectslidemenu = old;
        return this;
    };




}(window.jQuery));

//# sourceMappingURL=backend.js.map
