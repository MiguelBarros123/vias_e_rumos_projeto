<?php

return [
    'module-label'=>'Perfis',
    'menu' => [
        'profiles' => 'Perfis'
    ],
    'common'=>[
        'email' => 'Email',
        'name' =>  'Nome',
        'state' => 'Estado',
        'aproved' => 'Aprovado',
        'date' => 'Data de Registo',
        'active' => 'Ativo',
        'inactive' => 'Inativo',
        'see_edit' => 'Ver/Editar',
        'tabs' => 'Secções',
    ],
    'messages' => [
        'success' => [
        
        ],
        'error' => [
            
        ],
        'email' => [
            
        ],
    ],

    'permission' => [
        'profilereservedarea' => [
            'see' => 'Ver Perfis Área Reservada',
            'create' => 'Criar Perfis Área Reservada',
            'delete' => 'Apagar Perfis Área Reservada'
        ],
        'personalized_fields'=>[
            
        ]
    ],
];
