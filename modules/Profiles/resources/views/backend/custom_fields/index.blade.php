@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/profiles/backend.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('back/css/spectrum.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
@endsection

@section('module-scripts')
    <script src="{{asset('js/profiles/backend.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
    <script src="{{asset('back/js/spectrum.js')}}"></script>

    <script>
        $(document).ready(function () {

            var options = {

                rota: "{{ route('profiles::backend::profiles.custom_fields.index_ajax')}}",
                colunas: [
                    {data: 'name', name: 'name'},
                    {data: 'type', name: 'type'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    initComplete: function (settings, json) {
                        $(function () {
                            $.logicDatatable(options);

                        });
                    },
                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                        //for clicable rows
                        $(nRow).attr('data-rota-see', aData['rota_final']);
                        var tr2 = $(nRow).closest('tr');
                        tr2.find("td:first").addClass('details-control');
                        tr2.find("td:not(:last, :nth-last-child(2))").addClass('clickable_url');
                    },

                },
                searchableElementId: 'search'
            };


            $('.default-table').mydatatable(options);

            $('.default-table').on('click', 'td.clickable_url', function () {
                var tr = $(this).closest('tr');
                window.location.href = tr.attr('data-rota-see');

            } );

        });
    </script>

@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 ">

                    <a class="pull-right icon_eliminar icon-adicionar pading_right icon_delete_inside_view" href="{{ route('profiles::backend::backend.profiles.custom_fields.create') }}">@lang('profiles::backend.menu.create_new_personalized_fields')</a>
                    @include('profiles::backend.layouts.top_menu')



                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="search" class="search search_icon" type="text"
                                   placeholder="@lang('profiles::backend.common.search')">
                        </div>
                    </div>
                    @include('layouts.backend.messages')

                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.custom_fields') !!}



                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                            <table id="unpaid_table" class="table default-table table-hover">
                                <thead>
                                <tr>
                                    <th>@lang('profiles::backend.common.name')</th>
                                    <th>@lang('profiles::backend.common.options')</th>

                                    <th class="multiple-options">
                                        @include('layouts.backend.partials.delete_datatables',['id'=>'unpaid_table','route'=>'profiles::backend::backend.profiles.custom_fields.delete'])
                                    </th>
                                    <th>
                                        <label class="checkbox-default">
                                            <input type='checkbox' id="sel_all_perm_unpaid_table">
                                            <span></span>
                                        </label>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

    @include('layouts.backend.partials.delete_form',['delete_message'=>'cms::backend.common.sure_delete'])
@endsection
