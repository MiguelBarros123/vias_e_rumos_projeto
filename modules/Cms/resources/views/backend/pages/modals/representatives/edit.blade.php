<div class="modal modal-aux" id="modal-representative-update" tabindex="-1" role="dialog" aria-labelledby="modal-new-representative">
    <div class="modal-dialog modal-dialog-aux" role="document">
        <form id="form-representative-update" action="{{ route('cms::backend::representative.store') }}" method="POST">
            <input type="hidden" name="_method" value="PUT">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="panel-titulo">
					<span class="titulo">
						<i class="icon-adicionar"></i>&nbsp;EDITAR REPRESENTANTE
					</span>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group fom-style-hidden3 @if($errors->first('name') ) has-error @endif">
                                <label for="name">@lang('cms::backend.model.pages.contacts.name')</label>

                                <input type="text" class="form-control" value="{{ old('name') }}" name="name">
                                {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="form-group fom-style-hidden3 @if($errors->first('order') ) has-error @endif">
                                <label for="order">@lang('cms::backend.model.pages.contacts.order')</label>

                                <input type="number" min="0" step="1" class="form-control" value="{{ old('order') }}" name="order">
                                {!! $errors->first('order','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-10">
                            <div class="form-group fom-style-hidden3 @if($errors->first('job_position') ) has-error @endif">
                                <label for="job_position">@lang('cms::backend.model.pages.contacts.job_position')</label>

                                <input type="text" class="form-control" value="{{ old('job_position') }}" name="job_position">
                                {!! $errors->first('job_position','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group fom-style-hidden3">
                                <label for="contact">@lang('cms::backend.model.pages.contacts.contacts')</label>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="form-group fom-style-hidden3 @if($errors->first('contact') ) has-error @endif">
                                        <input id="add-new-contact" type="text" placeholder="ex: 91X XXX XXX" class="form-control" value="{{ old('contact') }}" name="contact">
                                        {!! $errors->first('contact','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                </div>

                                <div class="col-lg-3 text-center">
                                    <button id="btn-add-contact" class="btn btn-yellow" type="button">Adicionar</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <ul id="new-contacts"></ul>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group fom-style-hidden3">
                                <label for="contact">@lang('cms::backend.model.pages.contacts.emails')</label>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-9">
                                    <div class="form-group fom-style-hidden3 @if($errors->first('email') ) has-error @endif">
                                        <input id="add-new-email" type="text" placeholder="ex: geral@thesilverfactory.pt" class="form-control" value="{{ old('email') }}" name="email">
                                        {!! $errors->first('email','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                </div>
                                <div class="col-lg-3 text-center">
                                    <button id="btn-add-email" class="btn btn-yellow" type="button">Adicionar</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <ul id="new-emails"></ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button id="submit-new-representative" class="btn btn-default btn-yellow">ADICIONAR</button>
                            <button class="btn btn-default btn-cancelar" data-dismiss="modal">CANCELAR</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>