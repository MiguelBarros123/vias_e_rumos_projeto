<?php

namespace Brainy\Seo\Controllers\Frontend;

use Brainy\Framework\Controllers\FrontendController;

class ModelFrontendController extends FrontendController
{
    public function index()
    {
        return view('seo::frontend.model.index');
    }
}
