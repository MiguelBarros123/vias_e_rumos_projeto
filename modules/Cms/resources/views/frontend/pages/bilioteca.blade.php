@include('cms::frontend.pages.layouts.biblio-menu')
<div class="vendas-prods history">
    <div class="top-bar hidden-sm hidden-xs">
        <div class="row prod-row">
            <div class="col-lg-9 col-md-9">
                <div class="row">
                    <div class="col-lg-2 col-md-2 text-center text-area-wrap">
                        data
                    </div>
                    <div class="col-lg-3 col-md-3 text-center text-area-wrap">
                        designação
                    </div>
                    <div class="col-lg-3 col-md-3 text-center text-area-wrap">
                        local
                    </div>
                    <div class="col-lg-4 col-md-4 text-center text-area-wrap">
                        descrição
                    </div>
                </div>
            </div>  
        
        </div>
    </div>
    <div class="each_prod">
        <div class="row prod-row">
            <div class="top-prod-list">
                <div class="prod-title">
                    <h3>Negociação Particular</h3>
                </div>
                
            </div>

            <div class="displ-flex">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 flex-col">
                    <div class="row hidden-md hidden-lg">
                        <div class="col-xs-12 col-sm-12 text-area-wrap img-wrap">
                            <div class="cover prod-img-list"  style="background-image: url('assets/prod1.jpg')">
                                <div class="more-prod-bg">

                                </div>
                                <div class="more-prod-text">
                                    Saber mais
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">17/12/2017</div>
                                    <div class="text-prod">9h</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">José Maria das Neves Pereira e Sousa Salgado dos Santos</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">Rua Dr. Castro nº20 1º Esq.,4001-001 Matosinhos,Porto</div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">
                                    Apartamento T4
                                    </div>
                                    <div class="text-prod">
                                        1 sala, cozinha, 4 quartos, 2 WCS, 5 roupeiros, 3 varandas, 1 garagem
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-area-wrap img-wrap hidden-xs hidden-sm">
                    <div class=" cover prod-img-list"  style="background-image: url('assets/prod1.jpg')">
                        <div class="more-prod-bg">

                        </div>
                        <div class="more-prod-text">
                            Saber mais
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="line-separator">
    </div>
    
    <div class="each_prod">
        <div class="row prod-row">
            <div class="top-prod-list">
                <div class="prod-title">
                    <h3>Leilão presencial</h3>
                </div>

            </div>
        
            <div class="displ-flex">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 flex-col">
                    <div class="row hidden-md hidden-lg">
                        <div class="col-xs-12 col-sm-12 text-area-wrap img-wrap">
                            <div class="cover prod-img-list"  style="background-image: url('assets/prod2.jpg')">
                                <div class="more-prod-bg">

                                </div>
                                <div class="more-prod-text">
                                    Saber mais
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">19/12/2017</div>
                                    <div class="text-prod">10h</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">José Maria das Neves Pereira e Sousa Salgado dos Santos</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">Rua Dr. Castro nº20 1º Esq.,4001-001 Matosinhos,Porto</div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">
                                    Máquina CAT 953D 

                                    </div>
                                    <div class="text-prod">
                                        Tipo de Sapatas de Esteiras Garra Dupla
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-area-wrap img-wrap hidden-xs hidden-sm">
                    <div class=" cover prod-img-list"  style="background-image: url('assets/prod2.jpg')">
                        <div class="more-prod-bg">

                        </div>
                        <div class="more-prod-text">
                            Saber mais
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    
    <div class="line-separator">
    </div>
    

    <div class="each_prod">
        <div class="row prod-row">
            <div class="top-prod-list">
                <div class="prod-title">
                    <h3>Negociação Particular</h3>
                </div>
                
            </div>

            <div class="displ-flex">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 flex-col">
                    <div class="row hidden-md hidden-lg">
                        <div class="col-xs-12 col-sm-12 text-area-wrap img-wrap">
                            <div class="cover prod-img-list"  style="background-image: url('assets/prod3.jpg')">
                                <div class="more-prod-bg">

                                </div>
                                <div class="more-prod-text">
                                    Saber mais
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">17/12/2017</div>
                                    <div class="text-prod">9h</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">José Maria das Neves Pereira e Sousa Salgado dos Santos</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">Rua Dr. Castro nº20 1º Esq.,4001-001 Matosinhos,Porto</div>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">
                                    Apartamento T4
                                    </div>
                                    <div class="text-prod">
                                        1 sala, cozinha, 4 quartos, 2 WCS, 5 roupeiros, 3 varandas, 1 garagem
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-area-wrap img-wrap hidden-xs hidden-sm">
                    <div class=" cover prod-img-list"  style="background-image: url('assets/prod3.jpg')">
                        <div class="more-prod-bg">

                        </div>
                        <div class="more-prod-text">
                            Saber mais
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
