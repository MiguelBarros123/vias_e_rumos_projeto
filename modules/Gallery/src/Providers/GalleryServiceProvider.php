<?php

namespace Brainy\Gallery\Providers;

use Brainy\Framework\Facades\Brainy;
use Brainy\Gallery\Models\MediaFolder;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider as LaravelProvider;

class GalleryServiceProvider extends LaravelProvider
{
    protected $defer = true;

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {


        $containers=Brainy::containers();
        if (Schema::hasTable('media_folders'))
        {
            $titles = [];
            $names = [];
            foreach($containers as $c) {
                $titles[] = $c->label;
                $names[] = $c->name;
            }

            $mediaFolders=MediaFolder::whereIn('title',$titles)
                ->whereIn('container_name',$names)
                ->where('parent_folder_id',1)
                ->pluck('title', 'container_name');
            foreach($containers as $c){
                if ($c->name == 'permissions::backend.module_name' || isset($mediaFolders[$c->name])) {
                    continue;
                }

                MediaFolder::create(['title'=>$c->label, 'localization'=>$c->label,'container_name'=>$c->name,'parent_folder_id'=>1]);
                MediaFolder::create(['title'=>$c->label.'_lixo', 'localization'=>$c->label.'_lixo','container_name'=>$c->name,'parent_folder_id'=>1,'garbage'=>1]);
                


            }
            //dd("DONE");
        }





    }

    /**
     * Register the application services.
     */
    public function register()
    {


        $this->app->bind('Brainy\Gallery\Repositories\MediaRepositoryInterface','Brainy\Gallery\Repositories\MediaRepository');
        $this->app->bind('Brainy\Gallery\Repositories\MediaItemRepositoryInterface','Brainy\Gallery\Repositories\MediaItemRepository');
    }


}
