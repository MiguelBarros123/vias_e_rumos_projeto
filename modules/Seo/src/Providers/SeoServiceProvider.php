<?php

namespace Brainy\Seo\Providers;

use Illuminate\Support\ServiceProvider as LaravelProvider;

class SeoServiceProvider extends LaravelProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
    }

    /**
     * Register the application services.
     */
    public function register()
    {
    }
}
