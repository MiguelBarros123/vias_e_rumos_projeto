/**
 * Created by User on 21-04-2016.
 */
$(document).ready(function(){

    $('.check_perm').click(function (){

        //se nao estiver selecionado
        if($(this).is(":checked")) {

            //se tem filhos atualiza os filhos
            if($(this).parent().next().is('ul')){

                $(this).parent().next().children().find('.check_perm').each(function( index ) {
                    $( this ).prop('checked', true);
                });

            }

            //se todos os irmaos estao selecionados e tem pai que não esta selecionado clica no pai
            if ($(this).parent().parent().siblings().find(' > label > input:checked').length == $(this).parent().parent().siblings().find(' > label > input').length ) {
                $(this).parent().parent().parent().prev().find('.check_perm').click();
            }

        }else{
            //se tem filhos atualiza os filhos
            if($(this).parent().next().is('ul')){
                $(this).parent().next().children().find('.check_perm').each(function( index ) {
                    $( this ).prop('checked', false);
                });
            }

            //se tem pai
            //se todos os irmaos estao selecionados e o pai esta selecionado
            if ($(this).parent().parent().siblings().find(' > label > input:checked').length == $(this).parent().parent().siblings().find(' > label > input').length ) {
                var pai=$(this).parent().parent().parent().prev().find('.check_perm');
                pai.prop('checked', false);

                if (pai.siblings().find(' > label > input:checked').length == pai.siblings().find(' > label > input').length ) {
                    pai.parent().parent().parent().prev().find('.check_perm').prop('checked', false);
                }

            }


        }

    });


});