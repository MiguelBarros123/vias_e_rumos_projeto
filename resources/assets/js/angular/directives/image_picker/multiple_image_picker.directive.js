(function() {
    'use strict';

    function isFolder (item) {
        return true;
    }

    function isPicture(item) {
        return true;
    }

    function ImagePickerController($scope, $http, $sce) {
        $scope.rootFolder = $scope.rootFolder || 1;
        $scope.currentFolderId = $scope.rootFolder;

        function isRootFolder (id) {
            return id == $scope.rootFolder;
        }

        $scope.getPublicPath = function (url) {
          $scope.public = url;
        };

        $scope.fetchNewFiles = function () {

            var route = window.Brainy.module.routes['gallery::backend::fetch_new_files'];
            var urlTo = url+'/'+route;

            $.ajax({
                url         : urlTo,
                type        : 'get',
                dataType    : 'json',
                success:function(data)
                {
                    $scope.galleryItems = angular.fromJson(data);

                    $scope.currentFolderId = 1;
                    $scope.buildBreadcrumb(1);
                    $scope.renderGallery();
                }
            });

        };

        $scope.redraw = function (event) {

            //$('div.item_galery.ui-selected').removeClass('ui-selected');

            var $selected = $(event.target).parents('div[data-media-item-type^="f"]');

            $('div.item_galery.ui-selected').removeClass('ui-selected');

            var $child = $selected.children().first();

            if ($child.hasClass('ui-selected')) {
                $child.removeClass('ui-selected');
            } else {
                $('div.media_item.ui-selected').removeClass('ui-selected');
                $child.addClass('ui-selected');
            }

            var type = $selected.attr('data-media-item-type');
            var id = $selected.attr('data-media-item-id');

            if (type == 'folder') {
                $scope.setCurrentFolder(id);
            } else {

            }

            $scope.renderGallery();
        };

        var prev = -1;

        $scope.redraw2 = function (event) {


            var $selected = $(event.target).parents('div[data-media-item-type^="f"]');
            var $child = $selected.children().first();

            if (event.ctrlKey){
                event.metaKey = event.ctrlKey;
            }


            if (event.metaKey == false) {

                var curr = $child.parent().index();

                if(event.shiftKey && prev > -1){
                    $('#selectable_cont2').children().slice(Math.min(prev, curr), 1 + Math.max(prev, curr)).children().addClass("ui-selected");
                    prev = -1; // and reset prev

                }else {
                    prev = curr;
                    // $("#selectable_cont2 .item_galery").removeClass("ui-selected");
                    $child.addClass("ui-selecting");
                }
            }
            else {
                prev = curr;
            }


        };


        $scope.navToFolder = function(event) {
            var id = $(event.target).attr('data-folder-id');
            //if (! isRootFolder(id)) {
                $scope.currentFolderId = id;
                $scope.buildBreadcrumb(id);
                $scope.renderGallery();
            //}
        };

        $scope.setCurrentFolder = function(id) {
            if (! isRootFolder(id)) {
                $scope.currentFolderId = id;
                $scope.buildBreadcrumb(id);
            }
        };

        $scope.breadcrumb = [];

        $scope.buildBreadcrumb = function (id) {
            var route = window.Brainy.module.routes['gallery::backend::breadcrumb'];
            route = route.replace('{pathId}', id);
            var urlTo = url+'/'+route;
            return $http.get(urlTo)
                .then(function(response, textStatus, xhr) {

                    $scope.breadcrumb = response.data;
                }, function(response) {
                    $scope.breadcrumb = [];
                });
        };

        function findFolderById (id) {
            for (var i = 0; i < $scope.galleryItems.length; ++i) {
                if (id == $scope.galleryItems[i].id) {
                    return $scope.galleryItems[i];
                }
            }
            return null;
        }

        $scope.renderGallery = function() {



            $scope.gallery = '<div id="selectable_cont2" class="row" style="background-color:#f1f2f2; height: 425px; overflow-y: auto">';

            $scope.folder = findFolderById($scope.currentFolderId);

            if ($scope.folder !== null) {
                for (var i = 0; i < $scope.folder.media_items.length; ++i) {
                    var element = $scope.folder.media_items[i];
                    var route = window.Brainy.module.routes['gallery::backend::storage_image'];
                    route = route.replace('{id}', element.identifier_token);
                    route = route.replace('{with?}', 231);
                    route = route.replace('{height?}', 250);
                    var urlToImage = url+'/'+route;
                    switch(element.type){

                        case 'imagem':
                            $scope.gallery +=
                                '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery item_galery_multi row multiple_seletees" >' +
                                '<div style="background: url('+urlToImage+')no-repeat;"' +
                                'class="part1 image_galery image_galery_multi col-lg-12">' +
                                '</div>' +
                                '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                '<i class="icon-icon-upload-img icon10"></i>' + element.title +
                                '</div>' +
                                '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                '<i class="icon-icon-peq-upload icon10"></i>' + element.created_at +
                                '</div>' +
                                '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">'+element.size+'</div>' +
                                '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                '<div class="separacao"></div>' +
                                '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza padding-left-15">' +
                                '<i class="icon-icon-peq-img-size pading_right"></i>'+element.resolution+' px' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            break;
                        case 'documento':
                            $scope.gallery +=
                                '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery item_galery_multi row multiple_seletees" >' +
                                '<div class="folder_icon folder_icon_multi col-lg-12 text-center parent">' +
                                '<i class="icon-icon-upload-doc icon80 child"></i>' +
                                '</div>' +
                                '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                '<i class="icon-icon-upload-doc icon10"></i>' + element.title +
                                '</div>' +
                                '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                '<i class="icon-icon-peq-upload icon10"></i>' + element.created_at +
                                '</div>' +
                                '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                '<div class="separacao"></div>' +
                                '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            break;
                        case 'video':
                            $scope.gallery +=
                                '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery item_galery_multi row multiple_seletees" >' +
                                '<div class="folder_icon folder_icon_multi col-lg-12 text-center parent">' +
                                '<i class="icon-icon-video icon80 child"></i>' +
                                '</div>' +
                                '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                '<i class="icon-icon-video icon10"></i>' + element.title +
                                '</div>' +
                                '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                '<i class="icon-icon-peq-upload icon10"></i>' + element.created_at +
                                '</div>' +
                                '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                '<div class="separacao"></div>' +
                                '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            break;
                        case 'audio':
                            $scope.gallery +=
                                '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery item_galery_multi row multiple_seletees">' +
                                '<div class="folder_icon folder_icon_multi col-lg-12 text-center parent">' +
                                '<i class="icon-icon-som icon80 child"></i>' +
                                '</div>' +
                                '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                '<i class="icon-icon-som icon10"></i>' + element.title +
                                '</div>' +
                                '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                '<i class="icon-icon-peq-upload icon10"></i>' + element.created_at +
                                '</div>' +
                                '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                '<div class="separacao"></div>' +
                                '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            break;
                        case 'link':
                            $scope.gallery +=
                                '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery item_galery_multi row multiple_seletees">' +
                                '<div class="folder_icon folder_icon_multi col-lg-12 text-center parent">' +
                                '<i class="icon-icon-peq-embed icon80 child"></i>' +
                                '</div>' +
                                '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                '<i class="icon-icon-peq-embed icon10"></i>' + element.title +
                                '</div>' +
                                '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                '<i class="icon-icon-peq-upload icon10"></i>' + element.created_at +
                                '</div>' +
                                '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                '<div class="separacao"></div>' +
                                '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            break;
                        default:
                            $scope.gallery +=
                                '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery item_galery_multi row multiple_seletees">' +
                                '<div class="part1 image_galery image_galery_multi col-lg-12">' +
                                '</div>' +
                                '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                '<i class="icon-icon-upload-img icon10"></i>' + element.title +
                                '</div>' +
                                '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                '<i class="icon-icon-peq-upload icon10"></i>' + element.created_at +
                                '</div>' +
                                '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                '<div class="separacao"></div>' +
                                '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            break;
                    }

                }

                for (var j = 0; j < $scope.galleryItems.length; ++j) {
                    var otherFolder = $scope.galleryItems[j];

                    if (otherFolder.parent_folder_id == $scope.folder.id) {
                        $scope.gallery += '<div class="col-lg-4" data-media-item-type="folder" data-media-item-id="' + otherFolder.id + '">' +
                            '<div class="item_galery item_galery_multi folder_ref row">' +
                            '<div class="folder_icon folder_icon_multi col-lg-12 text-center parent">' +
                            '<i class="icon-icon-pasta icon80 child"></i>' +
                            '</div>' +
                            '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                            '<i class="icon-icon-peq-pasta icon10"></i>' + otherFolder.title +
                            '</div>' +
                            '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                            '<i class="icon-icon-peq-upload icon10"></i>'+ otherFolder.created_at +
                            '</div>' +
                            '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">' + (otherFolder.size || '0 bytes') + '</div>' +
                            '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                            '<div class="separacao"></div>' +
                            '<div class="text-center foot_gallery_icon fonte_12_lato_light_cinza">' + $scope.galleryItems.filter(function(e) { return e.parent_folder_id === otherFolder.id }).length + ' pastas</div>' +
                            '<div class="text-center foot_gallery_icon2 fonte_12_lato_light_cinza">' + otherFolder.media_items.length + ' ficheiros</div>' +
                            '</div>' +
                            '</div>' +
                            '</div>';
                    }
                }
            }
            $scope.gallery += '</div>';
            $scope.gallery = $sce.trustAsHtml($scope.gallery);
        };

        $(function() {
            $scope.buildBreadcrumb($scope.rootFolder);
            $scope.renderGallery();
        });
    }

    function link($scope, elements, attributes) {
        $scope.galleryItems = angular.fromJson(attributes.jsonGalleryItems);
        $scope.rootFolder = attributes.rootFolder;
        $(elements).selectable({
            filter: ".multiple_seletees",
            cancel: "a"
        });

        var mousetimer = {
            timer: null,
            timing: false,
            down: function(){
                if(!mousetimer.timing)
                {
                    mousetimer.timing = true;
                    mousetimer.timer = setTimeout(function(){
                        mousetimer.trigger();
                    }, 250);
                }
            },
            trigger: function(){
                $('.ui-selectable-helper').css('position', 'absolute');
                mousetimer.cancel();
            },
            cancel: function(){
                mousetimer.timing = false;

                clearTimeout(mousetimer.timer);
            }
        };

        $(document).on('mousedown', '#selectable_cont2', function(){
            mousetimer.down();
        }).on('mouseup', function(){
            mousetimer.cancel();
            $('.ui-selectable-helper').css('position', 'relative');
        });

    }

    function picker() {
        return {
            scope: true,
            controller: ['$scope', '$http', '$sce', ImagePickerController],
            link: link,
            restrict: 'EA',
            template: '<div class="image_picker">'+
            '<div class="row">' +
            '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">' +
            '<ul class="breadcrumb">' +
            '<li data-folder-id="{{folder.id}}" ng-repeat="folder in breadcrumb" ng-click="navToFolder($event)">{{ folder.title }}</li>' +
            '</ul>' +
            '</div>' +
            '</div>' +
            '<div class="row">' +
            '<div class="col-lg-12" ng-bind-html="gallery" ng-dblclick="redraw($event)" ng-click="redraw2($event)">' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>'
        };
    }

    function filterFolderOrPicture () {
        return function(input, id) {
            var out = [];

            for (var i = 0; i < input.length; ++i) {
                var item = input[i];
                if ( item.id >= id && (isFolder(item) || isPicture(item)) ) {
                    out.push(item);
                }
            }

            return out;
        };
    }

    angular.module('ImagePicker', ['ngSanitize'])
        .directive('imagePicker', picker)
        .filter('folderOrPicture', filterFolderOrPicture);




})();
