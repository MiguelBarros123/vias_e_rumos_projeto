@extends('layouts.backend.app')

@inject('folders', 'Brainy\Gallery\Models\MediaFolder')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/cms/backend.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
@endsection

@section('module-scripts')
    <script src="{{asset('js/cms/backend.js')}}"></script>
    <script src="{{asset('back/js/topmenuselects.js')}}"></script>

    <!---->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script src="{{ asset('back/js/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/directives/ng-js-tree/ngJsTree.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/modules/brainy.module.js') }}" type="text/javascript"></script>




    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/rowreorder/1.1.2/js/dataTables.rowReorder.min.js"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>



    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-sanitize.js"></script>
    <script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('angular/directives/image_picker/multiple_image_picker.directive.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ asset('angular/directives/toggler/toggler.directive.js') }}"></script>



    <script src="{{route('framework::backend::brainy', 'gallery')}}"></script>


    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
    <script src="{{asset('js/gallery/fileupload/jquery.ui.widget.js')}}"></script>
    <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
    <!-- The Canvas to Blob plugin is included for image resizing functionality -->
    <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
    <script src="{{asset('js/gallery/fileupload/jquery.iframe-transport.js')}}"></script>
    <!-- The basic File Upload plugin -->
    <script src="{{asset('js/gallery/fileupload/jquery.fileupload.js')}}"></script>
    <!-- The File Upload processing plugin -->
    <script src="{{asset('js/gallery/fileupload/jquery.fileupload-process.js')}}"></script>
    <!-- The File Upload image preview & resize plugin -->
    <script src="{{asset('js/gallery/fileupload/jquery.fileupload-image.js')}}"></script>
    <!-- The File Upload audio preview plugin -->
    <script src="{{asset('js/gallery/fileupload/jquery.fileupload-audio.js')}}"></script>
    <!-- The File Upload video preview plugin -->
    <script src="{{asset('js/gallery/fileupload/jquery.fileupload-video.js')}}"></script>
    <!-- The File Upload validation plugin -->
    <script src="{{asset('js/gallery/fileupload/jquery.fileupload-validate.js')}}"></script>
    <!---->

    <script src="{{asset('vendor/laravel-filemanager/js/lfm.js')}}"></script>

    <script src="{{asset('back/js/onload.js')}}"></script>

    @foreach($locales as $loc)
        <script>
            CKEDITOR.replace('editor_embed_d_{{$loc}}');
        </script>
    @endforeach

    @foreach($page->highlights as $highlight)
      @foreach($locales as $loc)
        <script>
          CKEDITOR.replace('editor_{{$highlight->id.$loc}}');
        </script>
      @endforeach

      <script>

        $(document).ready(function(){

            $('#lfm').filemanager('image', {prefix: "../../{{config('lfm.prefix')}}" });

          $('.lang_tabs_h_{{ $highlight->id }} li:first-child').tab('show');
              $('.lang_content_h_{{ $highlight->id }}').children().first().addClass('active');

        });

      </script>

    @endforeach

    @foreach($page->texts as $text)
        @foreach($locales as $loc)
            <script>
                CKEDITOR.replace( 'text_editor_{{$text->id.$loc}}');
            </script>
        @endforeach

        <script>
            $(document).ready(function() {
                var id2="{{$text->id}}";
                $('.lang_tabs_t_'+id2+' li:first-child').tab('show');
                $('.lang_content_t_'+id2+'').children().first().addClass('active');

                $('.lang_tabs_text_t_'+id2+' li:first-child').tab('show');
                $('.lang_content_text_t_'+id2+'').children().first().addClass('active');
            });
        </script>

    @endforeach

    @foreach($page->texts_simple as $text)
        @foreach($locales as $loc)
            <script>
                CKEDITOR.replace( 'text_editor_{{$text->id.$loc}}');
            </script>
        @endforeach

        <script>
            $(document).ready(function() {
                var id2="{{$text->id}}";
                $('.lang_tabs_t_'+id2+' li:first-child').tab('show');
                $('.lang_content_t_'+id2+'').children().first().addClass('active');

                $('.lang_tabs_text_t_'+id2+' li:first-child').tab('show');
                $('.lang_content_text_t_'+id2+'').children().first().addClass('active');
            });
        </script>

    @endforeach

    <script>
        var list_tables = [];
        var options_resources = [];
    </script>

    @foreach($page->composeds as $composed)
      <script>

        var $id = '{{$composed->id}}';

        list_tables[$id] = {
                rota: "{{ route('cms::backend::pages.ajax_table', [$composed->id])}}",
                rowReorder: {
                    selector: 'i.icon-icon-peq-img-size'
                },
                colunas: [
                    {data: 'order', name: 'order'},
                    {data: 'photo', name: 'photo'},
                    {data: 'title', name: 'title'},
                    {data: 'identifier', name: 'identifier'},
                    {data: 'date', name:'date'},
                    {data: 'visibleMessage', name: 'visibleMessage', orderable: false, searchable: false},
                    {data: 'visible', name: 'visible'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: null, defaultContent: '', orderable: false, searchable: false},
                    {data: 'composedVisibility', name: 'composedVisibility', visible: false}
                ],
                config: {
                    initComplete: function (settings, json) {
                        $(function () {
                             $.logicDatatable(list_tables[$id]);
                        });
                    },
                    fnCreatedRow: function (nRow, aData, iDataIndex) {

                    },
                    'fnDrawCallback': function () {

                      $('*[id^="toggle-one"]').bootstrapToggle();

                        $('*[id^="toggle-one"]').on('change', function(){
                            line = this.getAttribute('data-row');

                            var url = '{{ route("cms::backend::model.change_composed_visibility", ":id") }}';
                                url = url.replace(':id', line);

                                $.ajax({
                                    url: url,
                                    type: 'post',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                    },
                                    dataType: 'json',
                                    success:function(data)
                                    {

                                      state = !$('#toggle-one'+line).parent().hasClass('off');
                                      $('#info-hidden-composed-' + line).css('visibility', state == '1' ? 'hidden' : 'visible');
                                      $row = list_tables[$id].datatable.row($('#info-hidden-composed-' + line).parent()).index();

                                    }
                                });

                        });

                    }

                },
                searchableElementId: 'searchbox_'+$id
            };

            $('.default-table_'+$id).mydatatable(list_tables[$id]);

//        list_tables[$id] = options;

        list_tables[$id].datatable.on('row-reorder', function (e, diff, edit) {
            var elem = list_tables[$id].datatable.row(edit.triggerRow.selector.rows).data();
            var chosen = diff.find(function(element) { return list_tables[$id].datatable.row(element.node).data() === elem });
            var oldPosition = elem.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                if(diff[i].oldPosition === chosen.newPosition){
                    var rowData = list_tables[$id].datatable.row( diff[i].node ).data();
                    var newPosition = rowData.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');
                    $.ajax({
                        type: 'get',
                        url: '{{route('cms::backend::composed.reorder',[$composed->id])}}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: elem.id,
                            fromPosition: oldPosition,
                            toPosition: newPosition
                        },
                        success: function (data) {
                            list_tables[$id].datatable.ajax.reload(null, false);
                        }
                    });
                }
            }

        });

      </script>
    @endforeach

    @foreach($product_sections as $p_section)
      <script>

        var $p_id = '{{$p_section->id}}';

        options_resources[$p_id] = {
                rota: "{{ route('cms::backend::pages.ajax_section_table', [$p_section->id])}}",
                rowReorder: {
                    selector: 'i.icon-icon-peq-img-size'
                },
                colunas: [
                    {data: 'order', name: 'order', orderable: true},
                    {data: 'photo', name: 'photo'},
                    {data: 'identifier', name: 'identifier'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    initComplete: function (settings, json) {
                        $(function () {
                             $.logicDatatable(options_resources[$p_id]);
                        });
                    },
                    fnCreatedRow: function (nRow, aData, iDataIndex) {

                    },
                    'fnDrawCallback': function () {

                    }

                },
                searchableElementId: 'searchbox_section_'+$p_id
            };

            $('.resources-table_'+$p_id).mydatatable(options_resources[$p_id]);

//        options_resources[$p_id] = options;

        options_resources[$p_id].datatable.on('row-reorder', function (e, diff, edit) {
            var elem = options_resources[$p_id].datatable.row(edit.triggerRow.selector.rows).data();
            var chosen = diff.find(function(element) { return options_resources[$p_id].datatable.row(element.node).data() === elem });
            var oldPosition = elem.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');

            for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                if(diff[i].oldPosition === chosen.newPosition){
                    var rowData = options_resources[$p_id].datatable.row( diff[i].node ).data();
                    var newPosition = rowData.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');
                    $.ajax({
                        type: 'get',
                        url: '{{route('cms::backend::resource.reorder', [$p_section->id])}}',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: {
                            id: elem.id,
                            fromPosition: oldPosition,
                            toPosition: newPosition
                        },
                        success: function (data) {
                            options_resources[$p_id].datatable.ajax.reload(null, false);
                        }
                    });
                }
            }

        });

      </script>
    @endforeach

    <script>
        var save_asset_url = "{{route('gallery::backend::save_asset')}}";
        var fetch_new_files = "{{route('gallery::backend::fetch_new_files')}}";

        var folders='{!! $folders::all()->toJson() !!}';
        var inicial='4';

        $(document).ready(function () {

            var $injector =angular.injector(['ng','ImagePicker']);
            var html = document.getElementsByTagName('image-picker');
            $injector.invoke(function($compile, $rootScope){
                var $scope = $rootScope.$new();
                var result= $compile(html)($scope);
                var cScope = result.scope(); // This is scope in directive controller. :  )
                var myBtn = document.getElementById('ulpload-directive');
                myBtn.addEventListener('click', cScope.fetchNewFiles);
            });

            $('.btn-highpight-image').on('click', function(){
                $('#variant_id').val($(this).attr('data-variant-id'));
            });


            $('#btn_upload_image').on('click', function () {

                var highlight_id = $('#variant_id').val();

                var selected = $('div.item_galery.ui-selected');
                if(selected.length){

                document.getElementById('image_'+highlight_id).value = selected.parent().attr('data-media-item-id');
                $('#upload_resource').modal('hide');

                var bgUrl = selected.children().first().css('background-image');
                bgUrl = /^url\((['"]?)(.*)\1\)$/.exec(bgUrl);
                bgUrl = bgUrl ? bgUrl[2] : '';

                $('.image_highlight_'+highlight_id).attr('src', bgUrl);

                } else {
                alert('Selecione uma imagem');
                }

            });


        });

    </script>
    <script src="{{asset('back/js/upload_files.js')}}"></script>

@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                @include('cms::backend.pages.layouts.top_menu')
                @include('layouts.backend.messages')

                {!! DaveJamesMiller\Breadcrumbs\Facade::render('cms.pages.show',$page) !!}

                <form method="POST" action="{{ route('cms::backend::pages.save_page') }}">
                    <input type="hidden" name="_method" value="PUT">
                    {!! csrf_field() !!}
                    @foreach($page->highlights as $highlight)
                        @include('cms::backend.pages.template.elements.highlights', $highlight)
                    @endforeach
                    @foreach($page->composeds as $composed)
                        @include('cms::backend.pages.template.elements.composeds', $composed)
                    @endforeach
                    @foreach($page->texts as $text)
                        @include('cms::backend.pages.template.elements.texts', $text)
                    @endforeach
                    @foreach($page->texts_simple as $text)
                        @include('cms::backend.pages.template.elements.texts-simple', $text)
                    @endforeach
                    @if($page->id == 1)
                        @foreach($product_sections as $p_section)
                            @include('cms::backend.pages.template.elements.locations', $p_section)
                        @endforeach
                    @endif
                    @if($page->highlights->count() > 0 || $product_sections->count() > 0 || $page->texts->count() > 0 || $page->is_contacts)
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin_btn_pages">
                                <button type="submit" class="btn btn-default btn-yellow">@lang('cms::backend.model.common.save')</button>
                            </div>
                        </div>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>
{{--<div ng-app="ImagePicker" ng-init="getPublicPath(url)">--}}
    {{--@foreach($page->highlights as $highlight)--}}
        {{--@include('cms::backend.pages.modals.multiple-image-upload', ['highlight' => $highlight])--}}
    {{--@endforeach--}}
{{--</div>--}}

    {{--@include('catalog::backend.products.edit_product.modals.upload_files')--}}
    @include('cms::backend.pages.template.elements.upload_files')

    @include('layouts.backend.partials.delete_form',['delete_message'=>'cms::backend.common.sure_delete'])



@endsection
