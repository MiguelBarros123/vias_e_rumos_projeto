<?php

namespace Brainy\Cms\Controllers\Backend;
use Brainy\Framework\Controllers\BackendController;

use Yajra\Datatables\Facades\Datatables;
use Brainy\Cms\Models\MenuBiblioteca;

class MenuBibliotecaBackendController extends BackendController
{
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        return view('cms::backend.menus_biblioteca.index');
    }

    public function index_ajax()
    {
        $menus = MenuBiblioteca::all();

        return Datatables::of($menus)
            ->editColumn('name', function($menu){
                return $menu->name;
            })
            ->addColumn('action', function ($menu) {
                $rota=route('cms::backend::backend.cms.menu_biblioteca.delete');
                $rotaeditar=route('cms::backend::backend.cms.menu_biblioteca.edit',[$menu->id]);
                $impossible=0;


                $string = '
                        <div class="btn-group pull-right">
                           <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a  class="edit_item_datatable" href="'.$rotaeditar.'" ><i class="icon-editar"></i>  ' . trans("cms::backend.model.common.edit") . '</a></li>
                            <li><a data-impossible-delete="'.$impossible.'" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $menu->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("cms::backend.model.common.delete") . ' </a></li>
                          </ul>
                        </div>

                       ';
                return $string;
            })
            ->addColumn('rota_final', function ($menu) {
                return route('cms::backend::backend.cms.menu_biblioteca.edit',[$menu->id]);
            })
            ->make(true);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */    
    public function create()
    {
        return view('cms::backend.menus_biblioteca.create');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  
    * @return Response
    */
    public function edit($id)
    {
        $menu = MenuBiblioteca::find($id);
        return view('cms::backend.menus_biblioteca.edit', compact('menu'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function store()
    {
        $menu = MenuBiblioteca::create(['name' => request('name')]);

        if(request('submit') === 'stay'){
            return redirect()->route('cms::backend::backend.cms.menu_biblioteca.edit',[$menu->id])->with('sucess-message', trans('cms::backend.messages.success.global_create'));
        }

        return redirect()->route('cms::backend::backend.cms.menu_biblioteca.index')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }

    /**
    * Update a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function update($id)
    {
        $menu = MenuBiblioteca::find($id);
        $menu->update(['name' => request('name')]);

        if(request('submit') === 'stay'){
            return redirect()->route('cms::backend::backend.cms.menu_biblioteca.edit',[$menu->id])->with('sucess-message', trans('cms::backend.messages.success.global_create'));
        }

        return redirect()->route('cms::backend::backend.cms.menu_biblioteca.index')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function delete()
    {
        $rows=explode(',',request('deleted_rows'));

        foreach($rows as $id){
            $menu = MenuBiblioteca::findOrFail($id);
            $menu->delete();
            MenuBiblioteca::where('order','>',$menu->order)->decrement('order');
        }

        return redirect()->route('cms::backend::backend.cms.menu_biblioteca.index')->with('sucess-message', trans('cms::backend.messages.success.global_delete'));
    }
}
