<?php

return [
    'model' => [
        'index' => [
            //@lang('seo::frontend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view frontend page',
        ],
    ],
    'messages' => [
        //@lang('seo::frontend.messages.notifications.new-task')
        'notifications' => [
            'new-task' => 'You have a new pending task',
        ],
    ],
];
