/**
 * Created by User on 27-09-2016.
 */

$(document).ready(function() {
    var options = {
        rota: rota,
        colunas: [
            {data: 'email', name: 'email'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: null, defaultContent: '', orderable: false, searchable: false}
        ],
        config: {
            initComplete: function (settings, json) {
                $(function () {
                    $.logicDatatable(options);
                    $('.edit_item_datatable').click(function(){
                        $('#form_update_from').attr('action',$(this).attr('data-route'));
                        $('#email_from').val($(this).attr('data-email'));
                    });
                });
            },
            fnCreatedRow: function (nRow, aData, iDataIndex) {

            }

        },
        searchableElementId: 'search'
    };

    $('.default-table').mydatatable(options);

});

//# sourceMappingURL=datatable_from.js.map
