/**
 * Created by User on 15-02-2017.
 */
(function( $ ) {

    $.fn.showBrainyNotification = function() {
        var sel_elem_brainy=this;

        sel_elem_brainy.show().removeClass('success_saving_email').addClass('fadeInDown');
        setTimeout(function () {
            sel_elem_brainy.removeClass('fadeInDown').addClass('fadeOutUp');
            setTimeout(function () {
                sel_elem_brainy.remove();
            },400);
        },3000);

        return this;

    };

    $(document).ready(function () {
        setTimeout(function () {
            $('.initial_alert').removeClass('fadeInDown').addClass('fadeOutUp');
            setTimeout(function () {
                $('.initial_alert').remove();
            },400);
        },3000);
    });

}( jQuery ));
