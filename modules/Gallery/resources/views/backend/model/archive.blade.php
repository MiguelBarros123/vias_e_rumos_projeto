@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/gallery/backend.css')}}">
    <link rel="stylesheet" href="{{asset('back/css/context_menu.css')}}" >
@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>
    <script src="{{asset('back/js/topmenuselects.js')}}"></script>
    <script src="{{asset('back/js/brainy-framework/settings-button.js')}}"></script>

    @if(\Illuminate\Support\Facades\Session::has('errors'))
        <script>
            $('#myModal2').modal();
        </script>
    @endif


    @if(\Illuminate\Support\Facades\Session::has('error-message'))
        <script>
            $('#delete_media2').modal();
        </script>
    @endif

    <script>


        $(document).ready(function(){

            $.storagelib();


            $(function(){
                $.contextMenu({
                    selector: ".item_galery",
                    className: 'icon20 roda_dentada_topo',
                    items: {
                        editar: {name: "Recuperar", icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-editar icon12"></i> @lang("gallery::backend.model.index.recover")');
                                    return true;
                                }, callback: function(key, opt){


                            var items2={};
                            items2.folders=[];
                            items2.files=[];

                            $(".item_galery.ui-selected").each(function(){

                                if(!$(this).hasClass('ui-draggable-dragging')){
                                    if($(this).hasClass('pasta_accept'))
                                    {
                                        items2.folders.push($(this).attr('data-id'));
                                    }else{
                                        items2.files.push($(this).attr('data-id'));
                                    }
                                }

                            });

                            //window.console && console.log(items.files);
                            $('#recovered_files').val(items2.files);
                            $('#recovered_folders').val(items2.folders);
                            $('#restore_media').modal('show');


                        },disabled:function(key, opt){
                            //window.console && console.log($(".ui-selected").length);


                        }},

                        eliminar: {name: "Eliminar",icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-opcoes-eliminar icon15"></i> Eliminar');
                                    return true;
                                }, callback: function(key, opt){
                            //window.console && console.log(opt.$trigger.attr('data-delete_route'));
                            //get all the selected send to delete

                            var items={};
                            items.folders=[];
                            items.files=[];

                            $(".item_galery.ui-selected").each(function(){

                                if(!$(this).hasClass('ui-draggable-dragging')){
                                    if($(this).hasClass('pasta_accept'))
                                    {
                                        items.folders.push($(this).attr('data-id'));
                                    }else{
                                        items.files.push($(this).attr('data-id'));
                                    }
                                }

                            });

                            //window.console && console.log(items.files);
                            $('#deleted_files').val(items.files);
                            $('#deleted_folders').val(items.folders);
                            $('#delete_media').modal('show');
                        }}

                    },
                    callback: function(key, options) {
                        alert("Clicked on " + key + " on element " + options.$trigger.attr('id').substr(3));
                    }
                });
            });



            var caminho="{{route('gallery::backend::archived_storage_assets')}}";
            var caminho_muda_item="{{route('gallery::backend::change_item_location')}}";
            var token=$('meta[name="csrf-token"]').attr('content');
            var modo_lista="{{$list_mode}}";



            $('.gallery_content').mediaFinder({ caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item ,arquivado:1});

            $("#tipo option:selected").removeAttr("selected");

            $('#tipo').multiselect({
                buttonText: function(options, select) {
                    return 'tipo';
                },
                onChange: function(option, checked, select) {
                    var selected=[];
                    var selectedOptions = $('#tipo option:selected');
                    $(selectedOptions).each(function(index, brand){
                        selected.push($(this).val());
                    });

                    if(selected.length > 0 ){
                        $('.gallery_content').mediaFinder({caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:1, onfiltertype: 'format', filterObjects:selected});
                    }else{
                        $('.gallery_content').mediaFinder({ caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:1 });
                    }

                }
            });

            $('#search_gallery').on('input', function() {
                var search_string = $(this).val();

                if(search_string.length > 0){
                    $('.gallery_content').mediaFinder({caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:1, onfiltertype: 'search',filterObjects:search_string});
                }else{
                    $('.gallery_content').mediaFinder({ caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:1 });
                }

            });


            $('.btn-settings').on("settings.gallery.archive.list-mode", function(event) {
                $('.gallery_content').mediaFinder({ caminho: caminho, token_laravel:token, modo_lista:event.value, caminho_muda_items: caminho_muda_item, arquivado:0 });
            });

        })
    </script>
@endsection



@section('content')
    <div class="conteudo_central">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">


                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                   
                            <ul class="list-inline lista_menu">
                                <li class="">
                                    <select id="tipo" multiple="multiple">


                                        <optgroup label="Imagens" data-icon="icon-icon-upload-img">
                                            <option value="jpg">JPEG</option>
                                            <option value="png">PNG</option>
                                            <option value="gif">Gif</option>
                                        </optgroup>

                                        <optgroup class="" label='Vídeo'>
                                            <option value="mkv">MKV</option>
                                            <option value="avi">AVI</option>
                                            <option value="mp4">MP4</option>
                                            <option value="h.264">H.264</option>
                                        </optgroup>

                                        <optgroup class="" label='Áudio'>
                                            <option value="wav">WAV</option>
                                            <option value="mp3">MP3</option>
                                        </optgroup>

                                        <optgroup class="" label='Documentos'>
                                            <option value="pdf">PDF</option>
                                            <option value="doc">DOC</option>
                                            <option value="docx">DOCX</option>
                                            <option value="xls">XLS</option>
                                            <option value="ppt">PPT</option>
                                        </optgroup>


                                        <optgroup class="" label='Links'>
                                            <option value="link">Embed link</option>
                                        </optgroup>

                                    </select>
                                </li>
                                <li class="">

                                </li>
                            </ul>
                        </div>
                    </div>




                </div>

                <div class="col-lg-12">
                    <div class="row tabs-back row-eq-height-center">
                        <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">

                            <ul class="list-inline fonte_12_lato">
                                <li class="  "><a class="icon_voltar" href="{{route('gallery::backend::galery',array(1,'all'))}}">@lang("gallery::backend.model.archive.gallery")</a></li>
                                <li><a class="icon_voltar ativo" href="{{route('gallery::backend::galery_archive')}}" >@lang("gallery::backend.model.archive.gallery_archive")</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="search_gallery" class="search search_icon" type="text"  placeholder="@lang("gallery::backend.model.index.seach_gallery")">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 no_padding">
                            <ul class="breadcrumb">
                                <li>@lang("gallery::backend.model.archive.gallery_archive")</li>
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3  breadcrumb text-right icon_select_gallery">

                            {!! button('user-route', 'btn-brainy icon-vistas-galeria-list icon15  ', 'gallery.archive.list-mode', 1, true) !!}
                            {!! button('user-route', 'btn-brainy icon-vistas-galeria-cards icon15', 'gallery.archive.list-mode', 0, true) !!}

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  msgs_gallery">



                            @if(\Illuminate\Support\Facades\Session::get('sucess-message'))
                                <div class="alert alert-sucess alert-dismissible " role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <i class="icon-permissoes icon9 pading_right" ></i>{{\Illuminate\Support\Facades\Session::get('sucess-message')}}
                                </div>
                            @endif

                            @if(\Illuminate\Support\Facades\Session::get('error-message'))
                                <div class="alert alert-erro alert-dismissible " role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <i class="icon-opcoes-eliminar icon9  pading_right"></i>{!!\Illuminate\Support\Facades\Session::get('error-message')!!}
                                </div>
                            @endif


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div id="empty_folder_gallery">
                <div class="col-lg-12 no_padding">
                    <p class="fonte_10_lato_light padding-top-20">@lang('gallery::backend.model.index.no_results')</p>
                    {{--<a href="{{route('gallery::backend::upload_asset',array($id_pasta))}}" class="btn btn-default btn-yellow"> <i class="icon-icon-peq-upload"></i>@lang('gallery::backend.model.index.upload')</a>--}}
                    {{--<a class="btn btn-default btn-yellow"  data-toggle="modal" data-target="#myModal2"> <i class="icon-criar-pasta "></i>@lang('gallery::backend.model.index.new_folder')</a>--}}
                </div>

            </div>
            <div id="selectable_cont" class="row gallery_content">

            </div>
            <div class="tooltip"></div>
        </div>
    </div>
    <!----------------------------------------------------- MODAL DELETE RECORD ----------------------------------------------------->
    <div class="modal modal-aux" id="delete_media" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux" role="document">
            <form class="form_elimina_media" method="POST" action="{{route('gallery::backend::destroy_media_items')}}" >
                <input id="deleted_files" name="deleted_files" type="hidden" value="">
                <input id="deleted_folders" name="deleted_folders" type="hidden" value="">
                                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-opcoes-eliminar"></i> @lang("gallery::backend.model.archive.delete")</span></div>
                    <div class="modal-body">
                        <div class="col-md-12 padding-top-10"><p class="text-center"><i class="icon-grupo icon40"></i></p></div>
                        <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">@lang("gallery::backend.model.archive.remove_selected")</p></div>
                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30">
                        <button type="submit" class="btn btn-default btn-yellow">@lang("gallery::backend.model.archive.delete")</button>
                        <button type="button" class="btn fonte_12_lato_regular" data-dismiss="modal"> @lang("gallery::backend.model.archive.cancel") </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!---------------------------------------------------MODAL DELETE RECORD  ----------------------------------------------------->

    <!----------------------------------------------------- MODAL RESTORE RECORDS ----------------------------------------------------->
    <div class="modal modal-aux" id="restore_media" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux" role="document">
            <form class="form_elimina_media" method="POST" action="{{route('gallery::backend::recover_media_items')}}" >
                <input id="recovered_files" name="deleted_files" type="hidden" value="">
                <input id="recovered_folders" name="deleted_folders" type="hidden" value="">
                                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-opcoes-eliminar"></i> @lang("gallery::backend.model.archive.recover")</span></div>
                    <div class="modal-body">
                        <div class="col-md-12 padding-top-10"><p class="text-center"><i class="icon-grupo icon40"></i></p></div>
                        <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">@lang("gallery::backend.model.archive.recover_selected")</p></div>
                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30">
                        <button type="submit" class="btn btn-default btn-yellow">@lang("gallery::backend.model.archive.recover")</button>
                        <button type="button" class="btn fonte_12_lato_regular" data-dismiss="modal"> @lang("gallery::backend.model.archive.cancel") </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!---------------------------------------------------MODAL RESTORE RECORDS  ----------------------------------------------------->
@endsection



