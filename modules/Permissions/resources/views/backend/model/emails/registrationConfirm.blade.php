<!DOCTYPE html>
<html lang="en-US">
<head>
    <meta charset="utf-8">
</head>
<body>
<h2>Confirmação de registo.</h2>

<div>

    Por favor, clique no link para ativar a sua conta {{ url("/register/confirm/$user->token") }}

    <br/>

</div>

</body>
</html>