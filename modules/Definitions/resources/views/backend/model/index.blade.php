@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
@endsection

@section('module-scripts')

<script>
    $(document).ready(function(){
        $(document).on('click', '.more_button_seasons', function(){
            $.ajax({
                url: "{{route('definitions::backend::definitions.add_more_conditions')}}",
                type: 'get',
                success: function(data){
                    var number = parseInt($('#conditions > .fom-style-hidden2').last().attr('data-id'));
                    $('#conditions').append(data);
                    var last_element = $('#conditions > .fom-style-hidden2').last();
                    number += 1;
                    last_element.attr('data-id', number);
                    last_element.find('.min_licitacao').attr('name','licitacoes['+number+'][min_licitacao]');
                    last_element.find('.max_licitacao').attr('name','licitacoes['+number+'][max_licitacao]');
                    last_element.find('.proxima_licitacao').attr('name','licitacoes['+number+'][proxima_licitacao]');
                    last_element.attr('data-id', number);
                }
            })
        });

        $(document).on('click','.icon-opcoes-eliminar', function(){
            $(this).parent().parent().parent().parent().remove();
        });
    });
</script>
@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
    <div class="container-fluid">
        <div class="row tabs-back">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                <ul class="list-inline">
                    <li><a href="{{route('definitions::backend::definitions.index')}}" class="icon_voltar"><i
                        class="pading_right"></i>Definições</a></li>
                    </ul>
                </div>
            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('profiles.create') !!}
            <div class="row">
                <form action="{{ route('definitions::backend::definitions.store')}}" method="POST" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                         <div class="panel">
                            <div class="panel-titulo">
                                <span class="titulo">
                                    <i class="icon-editar-utilizador icon20"></i>
                                    Definições Vendas Online
                                </span>
                            </div>
                            <div class="panel-body">
                                <div class="row margem_20">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="row">
                                            <div id="conditions">
                                                @if(isset($biddings))
                                                    @foreach($biddings as $key => $bidding)
                                                        @include('definitions::backend.partials.add_more_conditions')
                                                    @endforeach
                                                @else
                                                    @include('definitions::backend.partials.add_more_conditions')
                                                @endif        
                                            </div>
                                        </div>
                                        <button class="btn btn-default btn-yellow-icon margem_5 more_button_seasons" type="button"><i class="icon-adicionar"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('rent::backend.common.save')</button>
                        <button type="submit" name="submit" value="stay" class="btn btn-default btn-yellow btn-stay btn-submit-all">@lang('rent::backend.common.save_and_stay')</button>
                        <a class="btn btn-default btn-cancelar"
                        href="{{route('profilereservedarea::backend::profilereservedarea.index')}}">@lang('rent::backend.common.cancel')</a>
                    </div>   
                </div>
            </form>
        </div>    
    </div>
</div>
@endsection
