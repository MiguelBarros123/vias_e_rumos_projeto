$(function() {
var calcTime = function(start, end){

    var time = [];

    var hours1 = Math.floor(start / 60);
    var minutes1 = start - (hours1 * 60);

    if (hours1.toString().length == 1) hours1 = '0' + hours1;
    if (minutes1.toString().length == 1) minutes1 = '0' + minutes1;
    if (minutes1 == 0) minutes1 = '00';
    if (hours1 >= 12) {
        if (hours1 == 12) {
            hours1 = hours1;
            minutes1 = minutes1;
        } else {
            hours1 = hours1;
            minutes1 = minutes1;
        }
    } else {
        hours1 = hours1;
        minutes1 = minutes1;
    }
    if (hours1 == 0) {
        hours1 = '00';
        minutes1 = minutes1;
    }

    time[0] = hours1 + ":" + minutes1;

    var hours2 = Math.floor(end / 60);
    var minutes2 = end - (hours2 * 60);

    if (hours2.toString().length == 1) hours2 = '0' + hours2;
    if (minutes2.toString().length == 1) minutes2 = '0' + minutes2;
    if (minutes2 == 0) minutes2 = '00';
    if (hours2 >= 12) {
        if (hours2 == 12) {
            hours2 = hours2;
            minutes2 = minutes2;
        } else if (hours2 == 24) {
            hours2 = 23;
            minutes2 = "59";
        } else {
            hours2 = hours2;
            minutes2 = minutes2;
        }
    } else {
        hours2 = hours2;
        minutes2 = minutes2;
    }

    time[1] = hours2 + ":" + minutes2;

    return time;
}

var schedule;

$( "#slider-range" ).slider({
    range: true,
    min: 0,
    max: 1440,
    values: [ 540, 1080 ],
    slide: function( event, ui ) {

        schedule = calcTime(ui.values[0], ui.values[1]);

        $('.range-label-start').html(schedule[0] + "h");

        $('.range-label-end').html(schedule[1] + "h");

        $( "#skedule_hours" ).val(schedule[0] + "-" + schedule[1] );

    }
});

schedule = calcTime($( "#slider-range" ).slider( "values", 0 ), $( "#slider-range" ).slider( "values", 1 ));

$( "#skedule_hours" ).val(schedule[0] + "-" + schedule[1] );

$("#slider-range .ui-slider-range").html('<span class="range-label-start"></span><span class="range-label-end"></span>');

$('.range-label-start').html(schedule[0] + "h");
$('.range-label-end').html(schedule[1] + "h");

});
