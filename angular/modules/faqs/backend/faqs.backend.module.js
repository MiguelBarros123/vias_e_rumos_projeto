(function() {
	'use strict';

	angular.module('brainy.faqs.backend', ['brainy.faqs', 'ngJsTree', 'BootstrapToggler', 'ckeditor']);
})();