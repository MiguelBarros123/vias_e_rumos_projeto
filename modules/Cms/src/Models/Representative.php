<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class Representative extends Model
{
    protected $table = 'representatives';

    public $timestamps = false;

    protected $with = ['contacts', 'emails'];

    protected $fillable = [
        'name',
        'job_position',
        'position',
    ];

    public function contacts()
    {
    	return $this->hasMany(RepresentativeContact::class);
    }

    public function emails()
    {
    	return $this->hasMany(RepresentativeEmail::class);
    }
}