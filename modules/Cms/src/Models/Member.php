<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:28
 */

namespace Brainy\Cms\Models;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Member extends Model {

    use Translatable;

    public $fillable = [
        'name',
        'phone',
        'email',
        'photo'
    ];

    public $translationModel = MemberTranslation::class;
    public $translatedAttributes = [
        'job'
    ];
}