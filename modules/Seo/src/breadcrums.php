<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-01-2017
 * Time: 09:46
 */



DaveJamesMiller\Breadcrumbs\Facade::register('cms.seo', function($breadcrumbs)
{
    $breadcrumbs->parent('cms');
    $breadcrumbs->push('Seo', route('seo::backend::model.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.seo.show', function($breadcrumbs)
{
    $breadcrumbs->parent('cms.seo');
    $breadcrumbs->push('Seo', route('seo::backend::model.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.favicon.show', function($breadcrumbs)
{
    $breadcrumbs->parent('cms.seo');
    $breadcrumbs->push('Favicon', route('seo::backend::model.index_favicon'));
});