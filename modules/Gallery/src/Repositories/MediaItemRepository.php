<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18-05-2016
 * Time: 11:15
 */

namespace Brainy\Gallery\Repositories;

use Brainy\Gallery\Exceptions\ChunkUploadException;
use Brainy\Gallery\Models\MediaFolder;
use Brainy\Gallery\Models\MediaItem;
use Brainy\Gallery\Models\MediaItemTranslation;
use Brainy\Framework\Models\Tag;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Filesystem\Filesystem;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class MediaItemRepository implements MediaItemRepositoryInterface{

    private $media;
    private $tempFiles;

    public function __construct(MediaItem $media){
        $this->media = $media;
        $this->tempFiles = [];

        $this->fileTypes = collect([
                ['imagem' => ['jpeg','jpg','png','gif','svg']],
                ['documento' => ['pdf','doc','docx','xls','ppt']],
                ['video' => ['mkv','avi','mp4','h.264']],
                ['audio' => ['wav','mp3']]
        ]);
    }


    public function updateTags($id, $tags)
    {
        $item=$this->media->find($id);
        if($tags != ""){
            $ids=[];
            foreach($tags as $t){
                $tag=Tag::where('name',$t)->first();
                if($tag!=null){
                    array_push($ids,$tag->id);
                }else{
                    $new=Tag::create(['name'=>$t]);
                    array_push($ids,$new->id);
                }
            }
            $item->tags()->sync($ids);
            $item->save();
        }else{
            $item->tags()->sync([]);
            $item->save();
        }

        //dd('ok');

        }

    public function updateTitleAndDescription($id, $data)
    {

        $this->traslateItem($data,$this->media->findOrFail($id));

    }

    public function traslateItem($data,$item){

        $locales=config('translatable.locales');
        foreach ($locales as $locale) {
            $item->translateOrNew($locale)->title = $data['titulo_'.$locale];
            $item->translateOrNew($locale)->description = $data['descricao_'.$locale];
        }

        $item->save();

    }

    public function moveTo($id, $nova_localizacao)
    {
        $item=$this->media->find($id);

            //if parent folder changes
            if($item->media_folder->id != $nova_localizacao){
                $item->update(array('media_folder_id'=>$nova_localizacao));
            }
    }

    public function writeToTempFile($filename, $file)
    {


            if(!$output=fopen(Storage::disk('tmp')->getDriver()->getAdapter()->getPathPrefix().auth()->user()->id.'_'.$filename,'ab')){
                throw new ChunkUploadException('Failed to open output stream', 102);
            }

            if(!$input=fopen($file,'rb')){
                throw new ChunkUploadException('Failed to open input stream', 101);
            }

            while ($buff = fread($input, filesize ($file))) {
                fwrite($output, $buff);
                flush();
                ob_flush();
            }

            fclose($output);
            fclose($input);




    }

    public function getTempFile($filename)
    {

        if (Storage::disk('tmp')->exists(auth()->user()->id.'_'.$filename)) {

            return new UploadedFile(
                Storage::disk('tmp')->getDriver()->getAdapter()->getPathPrefix().auth()->user()->id.'_'.$filename,
                $filename,
                Storage::disk('tmp')->mimeType(auth()->user()->id.'_'.$filename),
                Storage::disk('tmp')->size(auth()->user()->id.'_'.$filename),
                null,
                false // we must pass the true as test to force the upload file
            // to use a standart copy method, not move uploaded file
            );


        }
        return null;
    }


    public function deleteTempFile ($filename)
    {
        Storage::disk('tmp')->delete(auth()->user()->id.'_'.$filename);
    }

    public function storeItems($request, $id)
    {
        $folder=MediaFolder::findOrFail($id);
        $errors=collect();

        if(isset($request['croppedImage'])){
            $new_item=null;
            $errors=$this->create_a_file($request['croppedImage'],$folder,$errors,$new_item);
            $errors->put('new_id', $new_item);
            return $errors;
        }



        if(array_key_exists('files',$request)){
            foreach($request['files'] as $file) {



                if(strtolower(pathinfo($file->getClientOriginalName(),PATHINFO_EXTENSION)) =="zip"){



                    $za = new \ZipArchive();
                    $za->open($file->getRealPath(),\ZipArchive::CHECKCONS);

                    if($za==true){
                        //create a dir for user
                        $nova_dir=$this->tempdir();

                        $result=$za->extractTo($nova_dir);

                        $za->close();

                        $new_temp_name=strtolower(basename($nova_dir));


                        $dirs=Storage::disk('local_temp')->allDirectories($new_temp_name);


                        $mapa=collect();

                        foreach($dirs as $d){
                            $tit = strtolower(pathinfo(basename($d),PATHINFO_FILENAME));
                            $parent_name =strtolower(pathinfo(strtolower(pathinfo($d,PATHINFO_DIRNAME)),PATHINFO_FILENAME));

                            if($mapa->has($parent_name)){
                                $id_pai=$mapa->get($parent_name);
                            }else{
                                $id_pai=$folder->id;
                            }


                            $imp['title']=$tit;
                            $imp['parent_folder_id']=$id_pai;
                            $imp['localization']=$folder->localization."/".$tit;
                            $imp['container_name']=$folder->container_name;
                            $f=MediaFolder::create($imp);


                            $mapa->put($tit,$f->id);


                            $files = Storage::disk('local_temp')->files($d);

                            foreach($files as $fx){


                                $zzz=Storage::disk('local_temp')->getDriver()->getAdapter()->getPathPrefix();
                                $caminho_fic=$zzz.$fx;


                                $hash=hash_file ( 'md5' ,$caminho_fic);
                                $pa=explode('.', $fx);
                                $extension=end($pa);
                                $this->validateFile($errors,$hash,$extension);
                                if($errors->isEmpty()){
                                    $type_f=$this->getFileType($extension);
                                    $title=strtolower(pathinfo(basename($caminho_fic),PATHINFO_FILENAME));

                                    $thumb=$this->generateFileThumbnail($extension,$type_f);

                                    $token=$this->generateFileToken($extension);
                                    $size=$this->getFileSize($caminho_fic);
                                    $resolution=$this->getFileResolution($file,$type_f);

                                    Storage::disk('galeria')->put($token,  $caminho_fic );

                                    $mi= new MediaItem([
                                        'tipo'=>$type_f,
                                        'titulo'=>$title,
                                        'thumbnail'=>$thumb,
                                        'tamanho'=>$size,
                                        'resolucao'=>$resolution,
                                        'md5'=>$hash,
                                        'identifier_token'=>$token,
                                        'format'=>$extension,
                                    ]);



                                    $f->media_items()->save($mi);

                                    $this->generateTranslation($mi->id,$extension,$title);

                                }

                            }




                        }

                        Storage::disk('local_temp')->deleteDirectory($nova_dir);


                    }else{
                        $errors->put('zip','Não foi possivel abrir o ficheiro.');
                    }



                }
                else{
                    //var_dump($file);
                    $errors=$this->create_a_file($file,$folder,$errors);
                }


            }
        }else{
            foreach($request as $file) {



                if(strtolower(pathinfo($file[0]->getClientOriginalName(),PATHINFO_EXTENSION)) =="zip"){



                    $za = new \ZipArchive();
                    $za->open($file[0]->getRealPath(),\ZipArchive::CHECKCONS);

                    if($za==true){
                        //create a dir for user
                        $nova_dir=$this->tempdir();

                        $result=$za->extractTo($nova_dir);

                        $za->close();

                        $new_temp_name=strtolower(basename($nova_dir));


                        $dirs=Storage::disk('local_temp')->allDirectories($new_temp_name);


                        $mapa=collect();

                        foreach($dirs as $d){
                            $tit = strtolower(pathinfo(basename($d),PATHINFO_FILENAME));
                            $parent_name =strtolower(pathinfo(strtolower(pathinfo($d,PATHINFO_DIRNAME)),PATHINFO_FILENAME));

                            if($mapa->has($parent_name)){
                                $id_pai=$mapa->get($parent_name);
                            }else{
                                $id_pai=$folder->id;
                            }


                            $imp['title']=$tit;
                            $imp['parent_folder_id']=$id_pai;
                            $imp['localization']=$folder->localization."/".$tit;
                            $imp['container_name']=$folder->container_name;
                            $f=MediaFolder::create($imp);


                            $mapa->put($tit,$f->id);


                            $files = Storage::disk('local_temp')->files($d);

                            foreach($files as $fx){


                                $zzz=Storage::disk('local_temp')->getDriver()->getAdapter()->getPathPrefix();
                                $caminho_fic=$zzz.$fx;


                                $hash=hash_file ( 'md5' ,$caminho_fic);
                                $pa=explode('.', $fx);
                                $extension=end($pa);
                                $this->validateFile($errors,$hash,$extension);
                                if($errors->isEmpty()){
                                    $type_f=$this->getFileType($extension);
                                    $title=strtolower(pathinfo(basename($caminho_fic),PATHINFO_FILENAME));

                                    $thumb=$this->generateFileThumbnail($extension,$type_f);

                                    $token=$this->generateFileToken($extension);
                                    $size=$this->getFileSize($caminho_fic);
                                    $resolution=$this->getFileResolution($file,$type_f);

                                    Storage::disk('galeria')->put($token,  $caminho_fic );

                                    $mi= new MediaItem([
                                        'tipo'=>$type_f,
                                        'titulo'=>$title,
                                        'thumbnail'=>$thumb,
                                        'tamanho'=>$size,
                                        'resolucao'=>$resolution,
                                        'md5'=>$hash,
                                        'identifier_token'=>$token,
                                        'format'=>$extension,
                                    ]);



                                    $f->media_items()->save($mi);

                                    $this->generateTranslation($mi->id,$extension,$title);

                                }

                            }




                        }

                        Storage::disk('local_temp')->deleteDirectory($nova_dir);


                    }else{
                        $errors->put('zip','Não foi possivel abrir o ficheiro.');
                    }



                }
                else{
                    //var_dump($file);
                    $errors=$this->create_a_file($file[0],$folder,$errors);
                }


            }
        }












        return $errors;

    }

public function build_final_file($filename, $file, $id)
    {
        dd($file->size());

        return new UploadedFile(
            $filename,
            $file->getClientOriginalName(),
            $file->getClientMimeType(),
            filesize($filename),null,
            true // we must pass the true as test to force the upload file
        // to use a standart copy method, not move uploaded file
        );
        /*$folder = MediaFolder::findOrFail($id);
        $errors=collect();

        if(strtolower(pathinfo($filename, PATHINFO_EXTENSION)) =="zip"){

                    $za = new \ZipArchive();
                    $za->open($file[0]->getRealPath(),\ZipArchive::CHECKCONS);

                    if($za==true){
                        //create a dir for user
                        $nova_dir=$this->tempdir();

                        $result=$za->extractTo($nova_dir);

                        $za->close();

                        $new_temp_name=strtolower(basename($nova_dir));


                        $dirs=Storage::disk('local_temp')->allDirectories($new_temp_name);


                        $mapa=collect();

                        foreach($dirs as $d){
                            $tit = strtolower(pathinfo(basename($d),PATHINFO_FILENAME));
                            $parent_name =strtolower(pathinfo(strtolower(pathinfo($d,PATHINFO_DIRNAME)),PATHINFO_FILENAME));

                            if($mapa->has($parent_name)){
                                $id_pai=$mapa->get($parent_name);
                            }else{
                                $id_pai=$folder->id;
                            }


                            $imp['title']=$tit;
                            $imp['parent_folder_id']=$id_pai;
                            $imp['localization']=$folder->localization."/".$tit;
                            $imp['container_name']=$folder->container_name;
                            $f=MediaFolder::create($imp);


                            $mapa->put($tit,$f->id);


                            $files = Storage::disk('local_temp')->files($d);

                            foreach($files as $fx){


                                $zzz=Storage::disk('local_temp')->getDriver()->getAdapter()->getPathPrefix();
                                $caminho_fic=$zzz.$fx;


                                $hash=hash_file ( 'md5' ,$caminho_fic);
                                $pa=explode('.', $fx);
                                $extension=end($pa);
                                $this->validateFile($errors,$hash,$extension);
                                if($errors->isEmpty()){
                                    $type_f=$this->getFileType($extension);
                                    $title=strtolower(pathinfo(basename($caminho_fic),PATHINFO_FILENAME));

                                    $thumb=$this->generateFileThumbnail($extension,$type_f);

                                    $token=$this->generateFileToken($extension);
                                    $size=$this->getTempFileSize($file);
                                    $resolution=$this->getFileResolution($file,$type_f);

                                    Storage::disk('galeria')->put($token,  $caminho_fic );

                                    $mi= new MediaItem([
                                        'tipo'=>$type_f,
                                        'titulo'=>$title,
                                        'thumbnail'=>$thumb,
                                        'tamanho'=>$size,
                                        'resolucao'=>$resolution,
                                        'md5'=>$hash,
                                        'identifier_token'=>$token,
                                        'format'=>$extension,
                                    ]);



                                    $f->media_items()->save($mi);

                                    $this->generateTranslation($mi->id,$extension,$title);

                                }

                            }




                        }

                        Storage::disk('local_temp')->deleteDirectory($nova_dir);


                    }else{
                        $errors->put('zip','Não foi possivel abrir o ficheiro.');
                    }



                }
                else{
                    $errors=$this->chunked_create_a_file($filename, $file, $folder, $errors);
                }

        return $errors;*/

    }





    private function  tempdir() {

    $tempfile=tempnam(sys_get_temp_dir(),'');
    if (file_exists($tempfile)) { unlink($tempfile); }
    mkdir($tempfile);
    if (is_dir($tempfile)) { return $tempfile; }

}

    private function validateFile($errors,$hash,$type){


        if($existent=$this->hashexist($hash)!= false){
            $errors->put('duplicado', 'O ficheiro já existe na plataforma');

        }


        if($this->validateFileType($type)==false){
            $errors->put('formato', 'O formato .'.$type.' não é valido!');
        }

    }

    private function hashexist($hash){

        $exist=MediaItem::where('md5','like',$hash)->first();
        if(!is_null($exist)){
            return $exist;
        }
        return false;
    }

    private function validateFileType($type){



        foreach($this->fileTypes as $v){
            foreach ($v as $x => $v2){
                if(in_array($type,$v2)){
                    return true;
                }

            }

        }
        return false;
    }

    private function getFileType($type){

        foreach($this->fileTypes as $val){
            foreach ($val as $key=>$value){
                if(in_array($type,$value)){
                    return $key;
                }
            }

        }

        return false;

    }

    private function generateFileThumbnail($extension,$type){
        //if file is an image generate a image thumbnails for it

        switch($type){
            case "imagem":
                //todo
                return 'test.jpg';
                break;
            case "documento":
                if($extension=='doc'){return 'icon-icon-word';}
                if($extension=='docx'){return 'icon-icon-word';}
                if($extension=='pdf'){return 'icon-icon-pdf';}
                if($extension=='xls'){return 'icon-icon-pdf';}
                if($extension=='ppt'){return 'icon-icon-powerpoint';}

                break;
            case "video":
                return 'back/icons/play.png';
                break;
            case "audio":
                return 'back/icons/sound.png';
                break;
            default:
                return null;
                break;

        }


    }

    private function generateFileToken($extension){

        $exist='start';
        while($exist!=null){
            $token=$this->generateToken();
            $exist=MediaItem::where('identifier_token',$token)->first();
        }

        return  $token . '.'. $extension;

    }

    private function generateToken(){
        return uniqid('',true);
    }

    private function getTempFileSize($file){

        $bytes=Storage::disk('tmp')->size($file);

        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 0) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 0) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 0) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;



    }

    private function getFileSize($file){

        $bytes=filesize($file);

        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 0) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 0) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 0) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;



    }

    private function getFileResolution($file,$type_f){


        if($type_f=='imagem'){
            list($width, $height) = getimagesize($file);
            return $width.'x'.$height;

        }
        return null;

    }

    public function generateTranslation($file_id,$type,$original_name){

        //while slug exists
        //generate titles and descriptions
        //generates a slug for the titles
        //check if slug exists

        //run translation for title description and slug

        $slug_check=1;
        $n_of_attempts=0;
        $data=collect();

        while($slug_check != 0){

            if($n_of_attempts==0){

                foreach (config('translatable.locales') as $locale) {
                    if($locale == 'pt'){
                        $data['titulo_'.$locale] = $original_name;
                        $data['descricao_'.$locale] = $original_name;
                    }else {
                        $data['titulo_'.$locale] = $original_name.'_'.$locale;
                        $data['descricao_'.$locale] = $original_name.'_'.$locale;
                    }
                }
//
//                $data['titulo_pt'] = $original_name;
//                $data['descricao_pt'] = $original_name;

//                $data=[
//                    'titulo_pt'=>$original_name,
//                    'titulo_en'=>$original_name.'_en',
//                    'titulo_fr'=>$original_name.'_fr',
//                    'descricao_pt'=>$original_name,
//                    'descricao_en'=>$original_name.'_en',
//                    'descricao_fr'=>$original_name.'_fr',
//                ];


                $result=$this->checkSlugForEachLang($data,$type,$file_id);
//                var_dump($result);
                if(getType($result)=='integer'){$slug_check=$result;} else{$data=$result; $slug_check=0;  }


            }else{

                foreach (config('translatable.locales') as $locale) {
                    if($locale == 'pt'){
                        $data['titulo_'.$locale] = $original_name.$n_of_attempts;
                        $data['descricao_'.$locale] = $original_name;
                    }else {
                        $data['titulo_'.$locale] = $original_name.'_'.$locale.$n_of_attempts;
                        $data['descricao_'.$locale] = $original_name.'_'.$locale;
                    }
                }

//                $data['titulo_pt'] = $original_name.$n_of_attempts;
//                $data['descricao_pt'] = $original_name;

//                $data=[
//                    'titulo_pt'=>$original_name.$n_of_attempts,
//                    'titulo_en'=>$original_name.'_en'.$n_of_attempts,
//                    'titulo_fr'=>$original_name.'_fr'.$n_of_attempts,
//                    'descricao_pt'=>$original_name,
//                    'descricao_en'=>$original_name.'_en',
//                    'descricao_fr'=>$original_name.'_fr',
//                ];

                $result=$this->checkSlugForEachLang($data,$type,$file_id);
//                var_dump(getType($result));
                if(getType($result)=='integer'){$slug_check=$result;} else{$data=$result; $slug_check=0;  }


            }

            $n_of_attempts++;
        }





        $item=$this->media->find($file_id);

        foreach (config('translatable.locales') as $locale) {
            $item->translateOrNew($locale)->title = $data['titulo_'.$locale];
            $item->translateOrNew($locale)->description = $data['descricao_'.$locale];
            $item->translateOrNew($locale)->slug = $data['slug_'.$locale];
        }

        $item->save();



    }

    private function checkSlugForEachLang($data,$type,$file_id){

        foreach (config('translatable.locales') as $locale) {
            $data['titulo_'.$locale]=$this->clearTitle($data['titulo_'.$locale]);
            $slug=str_slug($data['titulo_'.$locale], "-").'.'.$type;
            $data['slug_'.$locale]=$slug;


            if($this->checkifSlugExists($slug,$file_id) != 0){
               return 1;
            }
        }

        return $data;
    }

    private function clearTitle($title){

        $nome = iconv( "UTF-8" , "ASCII//TRANSLIT//IGNORE" , $title );
        return preg_replace( array( '/[ ]/' , '/[^A-Za-z0-9\-]/' ) , array( '' , '' ) , $nome);

    }

    public function checkifSlugExists($slug,$id){


        return MediaItemTranslation::whereNotIn('media_item_id',[$id])->where('slug','like',$slug)->get()->count();
    }

    public function chunked_create_a_file($filename, $file, $folder,$errors) {

        $hash=hash_file ( 'md5', $file );

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

        $this->validateFile($errors,$hash,$extension);

        if($errors->isEmpty()){

            //if no errors
            $type_f=$this->getFileType($extension);
            $title=strtolower(pathinfo($filename,PATHINFO_FILENAME));

            $thumb=$this->generateFileThumbnail($extension,$type_f);

            $token=$this->generateFileToken($extension);
            $size=$this->getFileSize($file);
            $resolution=$this->getFileResolution($file,$type_f);

            Storage::disk('galeria')->put($token,  File::get($file));

            $f= new MediaItem([
                'type'=>$type_f,
                'title'=>$title,
                'thumbnail'=>$thumb,
                'size'=>$size,
                'resolution'=>$resolution,
                'md5'=>$hash,
                'identifier_token'=>$token,
                'format'=>$extension,
            ]);



            $folder->media_items()->save($f);

            $this->generateTranslation($f->id,$extension,pathinfo($filename,PATHINFO_FILENAME));

        }

        return $errors;
    }

    public function create_a_file($file,$folder,$errors,&$new_item=null){


        $hash=hash_file ( 'md5' ,realpath($file) );

        $extension=$file->guessExtension();

        $this->validateFile($errors,$hash,$extension);



        if($errors->isEmpty()){

            //if no errors
            $type_f=$this->getFileType($extension);

            $title=$this->generateFileTitle($file);

            $thumb=$this->generateFileThumbnail($extension,$type_f);

            $token=$this->generateFileToken($extension);
            $size=$this->getFileSize($file);
            $resolution=$this->getFileResolution($file,$type_f);

            Storage::disk('galeria')->put($token,  File::get($file));

            $f= new MediaItem([
                'type'=>$type_f,
                'title'=>$title,
                'thumbnail'=>$thumb,
                'size'=>$size,
                'resolution'=>$resolution,
                'md5'=>$hash,
                'identifier_token'=>$token,
                'format'=>$extension,
            ]);



            $folder->media_items()->save($f);

            $this->generateTranslation($f->id,$extension,pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME));
            $new_item=$f->id;

        }

        return $errors;
    }



    private function generateFileTitle($file){
        return strtolower(pathinfo($file->getClientOriginalName(),PATHINFO_FILENAME));
    }

    public function storeEmbedLink($request)
    {

        $folder=MediaFolder::findOrFail($request['media_folder_id']);


        $request['type']="link";
        $request['format']="link";
        $request['thumbnail']="icon-icon-peq-embed";


        $arr=array_except($request,array('titulo_pt','descricao_pt','titulo_en','descricao_en','titulo_fr','descricao_fr'));

        $item=new MediaItem($arr);


        $folder->media_items()->save($item);

        $this->traslateItem($request,$item);

    }

    public function updateSlug($id, $data)
    {
        $errors=collect();

        $item=MediaItem::find($id);
        foreach (config('translatable.locales') as $locale) {
            $data['titulo_'.$locale]=$this->clearTitle($data['titulo_'.$locale]);
            $slug=str_slug($data['titulo_'.$locale], "-").'.'.$item->format;
            $data['slug_'.$locale]=$slug;


            if($this->checkifSlugExists($slug,$id) != 0){
                $errors->push($data['titulo_'.$locale]);
            }
        }


        if($errors->count() > 0){ return $errors;}


        foreach (config('translatable.locales') as $locale) {
            $item->translateOrNew($locale)->slug = $data['slug_'.$locale];

        }
        $item->save();


        return $errors;



    }


    public function create_a_file_for_excell($filename, $folder,$errors,&$new_item) {



        $folder=MediaFolder::findOrFail($folder);
        $exists=Storage::disk('upload_images')->exists($filename);

        if(!$exists){
            return $errors->put('error','Occorreu um erro ao fazer upload ao ficheiro '.$filename);
        }
        $file=Storage::disk('upload_images')->get($filename);
        $hash=hash_file ( 'md5', $file );

        $extension = pathinfo($filename, PATHINFO_EXTENSION);

       // $this->validateFile($errors,$hash,$extension);

        if($existent=$this->hashexist($hash)!= false){
            $found=MediaItem::where('md5',$hash)->first();
            $new_item=$found->id;
        }


        if($errors->isEmpty()){

            //if no errors
            $type_f=$this->getFileType($extension);
            $title=strtolower(pathinfo($filename,PATHINFO_FILENAME));
            $thumb=$this->generateFileThumbnail($extension,$type_f);
            $token=$this->generateFileToken($extension);
            $size=$this->getFileSizeStorage($filename);
            $resolution=$this->getFileResolution(Storage::disk('upload_images')->getDriver()->getAdapter()->getPathPrefix().$filename,$type_f);


            Storage::disk('galeria')->put($token,  File::get(Storage::disk('upload_images')->getDriver()->getAdapter()->getPathPrefix().$filename));



            $f= new MediaItem([
                'type'=>$type_f,
                'title'=>$title,
                'thumbnail'=>$thumb,
                'size'=>$size,
                'resolution'=>$resolution,
                'md5'=>$hash,
                'identifier_token'=>$token,
                'format'=>$extension,
            ]);



            $folder->media_items()->save($f);

            $this->generateTranslation($f->id,$extension,pathinfo($filename,PATHINFO_FILENAME));

            $new_item=$f->id;


        }

        return $errors;
    }

    private function getFileSizeStorage($file){

        $bytes=Storage::disk('upload_images')->size($file);

        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 0) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 0) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 0) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;



    }
}