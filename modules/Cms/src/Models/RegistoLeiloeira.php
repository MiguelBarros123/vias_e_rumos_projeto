<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class RegistoLeiloeira extends Model
{
    protected $fillable = ['registo_leiloeira'];
}
