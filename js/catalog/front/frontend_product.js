$('.product-images').flexslider({
    animation: "slide",
    controlNav: "thumbnails",
    directionNav: false
});
$(document).ready(function(){


    var options = {
        zoomFactor: 6,
        paneContainer: document.getElementById('zoom-container'),
        inlinePane: 900,
        inlineOffsetY: 25,
        containInline: true,
        hoverBoundingBox: true
    };

    $('#product-info').on('click', 'a', function(e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('.product-select select').on('change', function() {

        $('#add-to-cart').button('loading');

        var id = product_id;

        var $attributes = [];

        $('select[name="attribute[]"]').each(function() {
            $attributes.push($(this).val());
        });

        var url = url_variant;
        url = url.replace(':attributes', $attributes);
        url = url.replace(':id', id);

        $.ajax({
            url         : url,
            type        : 'get',
            dataType    : 'json',
            success:function(data)
            {
                //console.log(data);
                var link = url_detail;
                link = link.replace(':variant', data.variant);
                link = link.replace(':name', data.name);

                location = link;
            }
        });

    });

    if(showCart){
        $('.cart-menu').velocity("fadeIn", { display: "block", duration: 1500 }).velocity("fadeOut", { display: "none", delay: 3000, duration: 1500 });
    }

    if($(window).width() > 900){
        $('.mobile-pdf-link').hide();
        $('.desktop-pdf-link').show();
    } else {
        $('.mobile-pdf-link').show();
        $('.desktop-pdf-link').hide();
    }

    $('.zoom-img').on('click',function(){
        if($(window).width() > 900) {
            var imgElement = $(this);
            var src = imgElement.attr('src');


            var img = '<img src="' + src + '" class="img-responsive marginAuto imagem_do_prod_fina2" data-dismiss="modal" aria-label="Close" />';

            $('#zoomModal').modal();
            $('#zoomModal').on('shown.bs.modal', function () {
                $('#zoomModal .modal-body').html(img);
            });
            $('#zoomModal').on('hidden.bs.modal', function () {
                $('#zoomModal .modal-body').html('');
            });
        }

    });

    $("#zoomModal").mousemove(function(e) {



        $("#zoomModal").stop().scrollTop(function(i, v) {
            var h = $(window).height();
            var y = e.clientY - h / 2;
            return v + y * 0.1;
        });



    });

    $('#add-to-cart').on('click', function () {
        //$(this).button('loading');
        $('#packsModal').modal();
    });

    $('#packsModal').on('shown.bs.modal', function () {
        var slider_packs = $('.slider_packs');
        slider_packs.slick();
    });

    $('.address-actions .gold-btn').hide();

    $(document).on('click', '.slick-slide', function(){

        $('.slick-slide').removeClass('active_slide');

        $(this).children('input')[0].click();
        $(this).addClass('active_slide');

        $('.address-actions .gold-btn').show();

    });

});
//# sourceMappingURL=frontend_product.js.map
