<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextSimpleTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_simple_translations', function(Blueprint $table){
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('text_simple_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->unique(['text_simple_id','locale']);
            $table->foreign('text_simple_id')->references('id')->on('texts_simple')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('text_simple_translations');
    }
}
