<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 31-05-2016
 * Time: 12:31
 */

namespace Brainy\Gallery\Requests;
use App\Http\Requests\Request;


class CreateEmbedRequest  extends Request{


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo_pt'=>'required',
            'titulo_en'=>'required',
            'titulo_fr'=>'required',
            'descricao_pt'=>'required',
            'descricao_en'=>'required',
            'descricao_fr'=>'required',
            'identifier_token'=>'required'
        ];
    }

}