<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:30
 */

namespace Brainy\Cms\Models;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class CHighlight extends Model
{

    use Translatable;
    public $translationModel = ComposedHighlightTranslation::class;
    public $translatedAttributes = ['title', 'subtitle', 'description', 'image_alt'];
    
    protected $fillable = ['order', 'visible', 'photo', 'date'];

    public function composed()
    {
        return $this->belongsTo(Composed::class);
    }
}