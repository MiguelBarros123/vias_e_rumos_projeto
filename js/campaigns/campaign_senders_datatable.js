$( function() {

    $(document).ready(function () {

        var urlSendersAjax = rota_senders.replace(':id', campaign_id);

        var options = {
            rota: urlSendersAjax,
            metodo: 'get',
            colunas: [
                {data: 'fullname', name: 'fullname'},
                {data: 'email', name: 'email'},
                {data: 'birth_date', name: 'birth_date'},
                {data: 'company_name', name: 'company_name'}
            ],
            config: {
                initComplete: function (settings, json) {
                    $(function () {
                        $.logicDatatable(options);
                    });
                },
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    //for clicable rows
                }

            },
            searchableElementId: 'search'
        };


        $('.default-table2').mydatatable(options);

    });

});
//# sourceMappingURL=campaign_senders_datatable.js.map
