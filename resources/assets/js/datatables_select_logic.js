/**
 * Created by User on 04-08-2016.
 */

$.logicDatatable=function (options){

        // if (typeof angular != 'undefined') {
            // if (!angular.element('body').injector()) {
            //     console.log('inject bootstrap');
            //     angular.bootstrap('body', ['BootstrapToggler']);
            // }
            // angular.bootstrap('body', ['BootstrapToggler']);
        // }

        $('.btn_move_to').click(function () {
            $('.form_move_order').attr('action', $(this).attr('data-route'));
            $('#modal_move').modal('show');
        });



    var id=options.id_of_table;
    var datatable=options.datatable;

         $('#sel_all_perm_'+id).prop('checked', false);

    $('.delete_item_table').click(function(){

        datatable.rows().deselect();
        $('#'+id+' thead .btn-group').fadeOut();
        $('#sel_all_perm_'+id).prop('checked', false);

        $('#impossible_records_to_delete'+id).val('');

        var rota=$(this).attr('data-route');
        var id_a_apagar=$(this).attr('data-id');

        if($(this).attr('data-impossible-delete') == 1){
            $('#impossible_records_to_delete'+id).val(id_a_apagar);
            // $('.form_delete_order .modal-body .fonte_15_lato p').html('Não é possivel eliminar este elemento!');
            $('.possible_delete_message').css('display', 'none');
            $('.impossible_delete_message').css('display', 'block');
            $('.form_delete_order .btn-yellow').attr('disabled','disabled');
            $('.form_delete_order').attr('action', rota);
        }else{
            $('#deleted_rows').val(id_a_apagar);
            // $('.form_delete_order .modal-body .fonte_15_lato p').html('Tem a certeza que pretente eliminar este elemento?');
            $('.possible_delete_message').css('display', 'block');
            $('.impossible_delete_message').css('display', 'none');
            $('.form_delete_order .btn-yellow').removeAttr('disabled');
            $('.form_delete_order').attr('action',rota);
        }

    });


    $('.delete_modal_record').click(function(){

        var rota=$(this).attr('data-route');
        $('#deleted_rows').val('');
        $('#deleted_rows').val($('#delete_records_'+id).val());
        $('.form_delete_order').attr('action',rota);


        //se existirem recursos impossiveis de eliminar devido a relações
        if($('#impossible_records_to_delete'+id).val().length > 0){
            $('.impossible_delete').show();
            $('.possible_delete_message').css('display', 'none');
            $('.impossible_delete_message').css('display', 'block');
            $('.form_delete_order .btn-yellow').attr('disabled','disabled');
            //todo add impossible records to delete to the modal
        }else{
            $('.impossible_delete').hide();
            $('.possible_delete_message').css('display', 'block');
            $('.impossible_delete_message').css('display', 'none');
            $('.form_delete_order .btn-yellow').removeAttr('disabled');
        }


    });


    $('#sel_all_perm_'+id).click(function () {
        if ($(this).is(":checked")) {

            $('#'+id+' thead .btn-group').fadeIn();
            datatable.rows().select();

            var dataRows = datatable.rows({ selected: true }).data();
            var delete_records = [];
            var impossible_delete_record = [];

            for (var i = 0, len = dataRows.length; i < len; i++) {
                if (dataRows[i]['impossible_delete'] == 1) {
                    impossible_delete_record.push(dataRows[i]['id']);
                }  else {
                    delete_records.push(dataRows[i]['id']);
                }
            }
            $('#delete_records_'+id).val('');
            $('#impossible_records_to_delete'+id).val('');

            $('#delete_records_'+id).val(delete_records);
            $('#impossible_records_to_delete'+id).val(impossible_delete_record);

        } else {
            $('#'+id+' thead .btn-group').fadeOut();
            datatable.rows().deselect();
            $('#delete_records_'+id).val('');
            $('#impossible_records_to_delete'+id).val('');
        }
    });


    $('.default-table tbody').on('click', 'td:last-child', function () {
        var rows = datatable.rows({ selected: true }).count();
       var  dataRows = datatable.rows({ selected: true }).data();

        var total_selecteds = datatable.rows().count();


        if (total_selecteds == rows) {

            $('#sel_all_perm_'+id).prop('checked', true);
        }else{
            console.log('remove');
            $('#sel_all_perm_'+id).prop('checked', false);
        }

        if (rows > 0) {

            $('#'+id+' thead .btn-group').fadeIn();
            var delete_records = [];
            var impossible_delete_record = [];

            for (var i = 0, len = dataRows.length; i < len; i++) {
                if (dataRows[i]['impossible_delete'] == 1) {
                    impossible_delete_record.push(dataRows[i]['id']);
                }  else {
                    delete_records.push(dataRows[i]['id']);
                }
            }

            $('#delete_records_'+id).val('');
            $('#impossible_records_to_delete'+id).val('');

            $('#delete_records_'+id).val(delete_records);
            $('#impossible_records_to_delete'+id).val(impossible_delete_record);

        } else {
            $('#'+id+' thead .btn-group').fadeOut();
            $('#delete_records_'+id).val('');
            $('#impossible_records_to_delete'+id).val('');
        }
    });

};
