var inicial='4';

var $injector =angular.injector(['ng','ImagePicker']);
var html = document.getElementsByTagName('image-picker');
$injector.invoke(function($compile, $rootScope){
    var $scope = $rootScope.$new();
    var result= $compile(html)($scope);
    var cScope = result.scope(); // This is scope in directive controller. :  )
    var myBtn = document.getElementById('ulpload-directive');
    myBtn.addEventListener('click', cScope.fetchNewFiles);
});

$('#btn_upload_image').on('click', function () {

    var selected = $('div.item_galery.ui-selected');
    if(selected.length){

        document.getElementById('image').value = selected.parent().attr('data-media-item-id');
        $('#upload_resource').modal('hide');

        var bgUrl = selected.children().first().css('background-image');
        bgUrl = /^url\((['"]?)(.*)\1\)$/.exec(bgUrl);
        bgUrl = bgUrl ? bgUrl[2] : '';

        $('.selected_element_now img').attr('src', bgUrl);

        $('.selected_element_now img').attr('data-item-id', selected.parent().attr('data-media-item-id'));



        $('.selected_element_now img').addClass('special_image');

        var value = $('#width_image').val();

        $('.selected_element_now img').width(value +'%');
        var width = $('.selected_element_now img').width();
        var height = $('.selected_element_now img').height();

        $('.selected_element_now img').attr('width', width + 'px').attr('height', height + 'px');

        // $('.selected_element_now img').attr('style', '');

        // $('.selected_element_now img').attr('style', 'width:' + width +';height:auto;');
        $('.selected_element_now img').css('width', width);
        $('.selected_element_now img').css('height', 'auto');

        if( $('#contour_image').is(':checked')){

            var sanitized = $('#width_px_contour_image').val().replace(/[^0-9\.\/]/g, '');
            $(this).val(sanitized);

            if(sanitized!=''){
                var color=$('.image_contour_minicolors').val();
                $('.selected_element_now img').css('border',sanitized+'px solid '+color);
            }

        }else{
            $('.selected_element_now img').css('border', 'none');
        }

        $('.selected_element_now img').removeClass('special_image');

    } else {
        alert('Selecione uma imagem');
    }

    updateImageAttributes();

});

// $(document).on('focusout', '#alt_image', function () {
//     var alt = $(this).val();
//     $('.selected_element_now img').attr('alt', alt);
//
//     updateImageAttributes();
//
// });

$(document).on('input','#alt_image', function(){
    console.log('alt');
    var alt = $(this).val();
    $('.selected_element_now img').attr('alt', alt);

    updateImageAttributes();
});

// $(document).on('focusout', '#width_image', function()
// {
//
//     $('.selected_element_now img').addClass('special_image');
//
//     var value = $(this).val();
//
//     $('.selected_element_now img').width(value +'%');
//     var width = $('.selected_element_now img').width();
//     var height = $('.selected_element_now img').height();
//
//     $('.selected_element_now img').attr('width', width + 'px').attr('height', height + 'px');
//
//     $('.selected_element_now img').attr('style', '');
//
//     $('.selected_element_now img').removeClass('special_image');
//
//     updateImageAttributes();
//
// });

$(document).on('input','#width_image', function(){


    var sanitized = $(this).val().replace(/[^0-9\.\/]/g, '');
    $(this).val(sanitized);

    $('.selected_element_now img').addClass('special_image');

    if($(this).val().length > 1){
        if($(this).val() > 100){
            $(this).val(100);
        } else if($(this).val() < 1){
            $(this).val(1);
        }
    }

    var value = $(this).val();

    $('.selected_element_now img').width(value +'%');
    var width = $('.selected_element_now img').width();
    var height = $('.selected_element_now img').height();

    $('.selected_element_now img').attr('width', width).attr('height', height);

    // $('.selected_element_now img').attr('style', '');

    // $('.selected_element_now img').attr('style', 'width:' + width +';height:auto;');
    $('.selected_element_now img').css('width', width);
    $('.selected_element_now img').css('height', 'auto');

    if( $('#contour_image').is(':checked')){

        var sanitized = $('#width_px_contour_image').val().replace(/[^0-9\.\/]/g, '');
        $('#width_px_contour_image').val(sanitized);

        if(sanitized!=''){
            var color=$('.image_contour_minicolors').val();
            $('.selected_element_now img').css('border',sanitized+'px solid '+color);
        }

    }else{
        $('.selected_element_now img').css('border', 'none');
    }

    $('.selected_element_now img').removeClass('special_image');

    // updateImageAttributes();
});

$(document).on('input', '#width_px_contour_image',function (e) {
    var sanitized = $(this).val().replace(/[^0-9\.\/]/g, '');
    $(this).val(sanitized);

    if(sanitized!=''){
        var color=$('.image_contour_minicolors').val();
        $('.selected_element_now img').css('border',sanitized+'px solid '+color);
    }

    updateImageAttributes();

});


$(document).on('change', '.countour_item', function (e) {
    if( $(this).is(':checked')){
        $(this).parent().next().removeClass('hidden_contour');
    }else{
        $(this).parent().next().addClass('hidden_contour');
    }

    updateImageAttributes();

});

$(document).on('change', '#contour_image', function (e) {
    if( $(this).is(':checked')){

        var sanitized = $('#width_px_contour_image').val().replace(/[^0-9\.\/]/g, '');
        $(this).val(sanitized);

        if(sanitized!=''){
            var color=$('.image_contour_minicolors').val();
            $('.selected_element_now img').css('border',sanitized+'px solid '+color);
        }

    }else{
        $('.selected_element_now img').css('border', 'none');
    }

    updateImageAttributes();

});

$(document).ready(function(){

    var crop_init = 0;

    $('#edit_image').on('shown.bs.modal', function(){

        if(crop_init != 1){
            crop_init = 1;

            $('#image_crop').attr('src', $('.selected_element_now img').attr('src'));

            console.log($('#image_crop').attr('src'));

            var $previews = $('.preview');

            $('#image_crop').cropper({

                viewMode:0,

                build: function (e) {
                    var $clone = $(this).clone();
                    $clone.css({
                        display: 'block',
                        width: '100%',
                        minWidth: 0,
                        minHeight: 0,
                        maxWidth: 'none',
                        maxHeight: 'none'
                    });

                    $previews.css({
                        width: '100%',
                        overflow: 'hidden'
                    }).html($clone);
                },

                crop: function(e) {
                    // For preview
                    var imageData = $(this).cropper('getImageData');
                    var previewAspectRatio = e.width / e.height;

                    $previews.each(function () {
                        var $preview = $(this);
                        var previewWidth = $preview.width();
                        var previewHeight = previewWidth / previewAspectRatio;
                        var imageScaledRatio = e.width / previewWidth;

                        $preview.height(previewHeight).find('img').css({
                            width: imageData.naturalWidth / imageScaledRatio,
                            height: imageData.naturalHeight / imageScaledRatio,
                            marginLeft: -e.x / imageScaledRatio,
                            marginTop: -e.y / imageScaledRatio
                        });
                    });
                }
            });

            $('.docs-buttons button').click(function(){
                $('#image_crop').cropper($(this).attr('data-method'),$(this).attr('data-option'),$(this).attr('data-second-option'));
            });

            $('.buttons_aspect_ratio label').click(function(){
                var as=$(this).find('input').val();
                console.log($(this).find('input').val());
                $('#image_crop').cropper('destroy');

                $('#image_crop').cropper({

                    aspectRatio: as,
                    viewMode:0,

                    build: function (e) {
                        var $clone = $(this).clone();
                        $clone.css({
                            display: 'block',
                            width: '100%',
                            minWidth: 0,
                            minHeight: 0,
                            maxWidth: 'none',
                            maxHeight: 'none'
                        });

                        $previews.css({
                            width: '100%',
                            overflow: 'hidden'
                        }).html($clone);
                    },

                    crop: function(e) {
                        // For preview
                        var imageData = $(this).cropper('getImageData');
                        var previewAspectRatio = e.width / e.height;

                        $previews.each(function () {
                            var $preview = $(this);
                            var previewWidth = $preview.width();
                            var previewHeight = previewWidth / previewAspectRatio;
                            var imageScaledRatio = e.width / previewWidth;

                            $preview.height(previewHeight).find('img').css({
                                width: imageData.naturalWidth / imageScaledRatio,
                                height: imageData.naturalHeight / imageScaledRatio,
                                marginLeft: -e.x / imageScaledRatio,
                                marginTop: -e.y / imageScaledRatio
                            });
                        });
                    }
                });

            });



            $('#store_blob').submit(function(e){

                e.preventDefault();

                var formData = new FormData();
                $('#image_crop').cropper('getCroppedCanvas').toBlob(function (blob2) {


                    formData.append('croppedImage', blob2);

                    var UrlRota = rota.replace(':item_id', $('.selected_element_now img').attr('data-item-id'));


                    $.ajax(UrlRota, {
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {

                            $('#edit_image').modal('toggle');

                            console.log('img', data.errors.new_id);

                            $('.err_mesages').empty();
                            $('.msg_sucesso_ajax').hide();
                            $('.msg_erro_ajax').hide();

                            if(data.errors.length != 0){
                                $('.msg_erro_ajax').fadeIn();

                                $.each(data.errors,function( index , el) {
                                    $('.err_mesages').append('<p>'+el+'</p>');
                                });
                            }else{
                                $('.msg_sucesso_ajax').fadeIn();
                            }


                        },
                        error: function (data) {

                        }
                    });
                });

                $(this).append('<input type="hidden" name="field_name" value="'+formData+'" /> ');
                return true;
            });

        } else {
            $('#image_crop').cropper('replace', $('.selected_element_now img').attr('src'));
        }



    });

    $(document).on('change', 'input[name="image_alignment"]', function(){

        var alignment = $(this).val();

        $('.selected_element_now img').removeClass('float-left').removeClass('float-center').removeClass('float-right').addClass($(this).val());
        $('.selected_element_now').removeClass('special-align-left').removeClass('special-align-center').removeClass('special-align-right');

        switch (alignment){
            case('float-left'):
                $('.selected_element_now').addClass('special-align-left');
                break;
            case('float-center'):
                $('.selected_element_now').addClass('special-align-center');
                break;
            case('float-right'):
                $('.selected_element_now').addClass('special-align-right');
                break;
        }


        $('.selected_element_now img').removeClass('float-left').removeClass('float-center').removeClass('float-right').addClass($(this).val());

        updateImageAttributes();

    });

});

function updateImageAttributes(){

    var attributes = $.parseJSON(atob($('.selected_element_now').attr('data-attributes')));

    $.each(attributes, function (index, value) {
        if(index == 'alignment'){
            attributes[index] = $('input[name="image_alignment"]:checked').val();
        }else if(index == 'width'){
            attributes[index] = $('#width_image').val().replace('%', '');
        }else if(index == 'border' || index == 'border_width' || index == 'border_color'){
            if($('.countour_item').prop('checked')){
                attributes['border'] = true;
                attributes['border_width'] = $('#width_px_contour_image').val();
                attributes['border_color'] = $('.image_contour_minicolors').val();
            } else {
                attributes['border'] = false;
            }
        } else {
            attributes[index] = $('.selected_element_now img').attr(index);
        }

    });

    $('.selected_element_now').attr('data-attributes', btoa(JSON.stringify(attributes)));

    $.saveState();

}

function updateImageHeight(element){

    console.log('element', element);

    var attributes = $.parseJSON(atob(element.item.attr('data-attributes')));

    $.each(attributes, function (index, value) {

        if(index == 'alignment'){
            attributes[index] = $('input[name="image_alignment"]:checked').val();
        }else if(index == 'width'){
            attributes[index] = $('#width_image').val().replace('%', '');
        } else if(index == 'border' || index == 'border_width' || index == 'border_color'){
            console.log($('.countour_item').prop('checked'));

            if($('.countour_item').prop('checked')){
                attributes['border'] = true;
                attributes['border_width'] = $('#width_px_contour_image').val();
                attributes['border_color'] = $('.image_contour_minicolors').val();
            } else {
                attributes['border'] = false;
            }
        } else {
            attributes[index] = $('.selected_element_now img').attr(index);
        }



    });

    element.item.attr('data-attributes', btoa(JSON.stringify(attributes)));

    $.saveState();

}
//# sourceMappingURL=image.js.map
