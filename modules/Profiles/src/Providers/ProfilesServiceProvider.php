<?php

namespace Brainy\Profiles\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as LaravelProvider;

class ProfilesServiceProvider extends LaravelProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        Blade::directive('activePageTopMenu', function ($menu) {
            return "<?= (Route::currentRouteName() == $menu) ? 'ativo' : '' ?>";
        });
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->bind('Brainy\Profiles\Repositories\ProfileRepositoryInterface','Brainy\Profiles\Repositories\ProfileRepository');
    }
}
