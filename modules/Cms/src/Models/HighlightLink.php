<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 10:10
 */

namespace Brainy\Cms\Models;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class HighlightLink extends Model {

    use Translatable;

    public $timestamps = false;

    public $translatedAttributes = [
        'name_link',
        'link'
    ];

    public $translationModel = HighlightLinksTranslation::class;
    
}