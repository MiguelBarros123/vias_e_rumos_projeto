<select class="form-control {{(isset($class)? $class: '')}}" name="{{$name}}" {{(isset($disabled)? $disabled: '')}}>
    @foreach($array as $key => $a)
        <option value="{{ $key }}" {{ ($key == $selected)? 'selected': ''}}>{{$a}}</option>
    @endforeach
</select>