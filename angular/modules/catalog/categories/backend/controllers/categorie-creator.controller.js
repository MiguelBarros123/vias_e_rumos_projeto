(function () {
	'use strict';

	function CategoriesCreatorController($scope, $http) {

		$scope.publicUrl = 'http://localhost/brainy-web/public/';


		var faqModel = {
			question: {
				translations: {}
			},
			visible: true,
			parent_id: null,
			tags: []
		};
		
		$scope.getPublicPath = function (url) {
			$scope.public = url;
		};

		$scope.faq = $.extend({}, faqModel);

		$scope.option = {
            language: $scope.locale,
            allowedContent: true,
            entities: false
        };

        // Called when the editor is completely ready.
        $scope.onReady = function() {
            //
        };

		$scope.toggleVisibility = function () {
			$scope.faq.visible = !$scope.faq.visible;
		};


		function httpPostSuccess(event) {
			return function (response) {
				$scope.$emit(event, response);
				$scope.dismiss('#modal-categorie');
			};
		}

		function httpFaqCreationError(response) {
			$('#error_tabs1').fadeIn();
		}

		$scope.updateOrNewFaqNode = function () {
			var event = 'edit faq';

			if ($scope.isEmptySelection()) {
				$scope.faq.id = 'new-faq-' + Date.now();
				event = 'new categorie';
			}

			var rota=window.Brainy.module.routes['catalog::backend::categories.save'];

			var url = $scope.public+'/'+rota;
			console.log(url);

			$http.post(url, $scope.faq)
				.success(httpPostSuccess(event))
				.error(httpFaqCreationError);
		};

		$(function() {
			$scope.ms = $('#magicsuggest-faq').magicSuggest({
                data: $scope.getTags,
                allowDuplicates: false,
                valueField: 'name'
            });

            $($scope.ms).on('selectionchange', function() {
                var tags = angular.fromJson(this.getSelection());

                for (var i = 0; i < tags.length; ++i) {
                	var tag = tags[i];
                	if (tag.id === undefined) {
                		tag.id = 'new-tag-' + Date.now();
                	}
                }

                $scope.faq.tags = tags;
            });

			var fullDate= new Date();
			var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) :(fullDate.getMonth()+1);
			var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();

			$('#range').daterangepicker({
				"autoUpdateInput": false,
				"locale": {
					"format": 'DD/MM/YYYY',
					"cancelLabel": 'Limpar',
					"applyLabel": 'Guardar'
				},
				"minDate": currentDate
			}, function(start, end, label) {
				$('#range span').html('<b>Início:</b> '+start.format('YYYY-MM-DD') + '&nbsp <b>Fim: </b>' + end.format('YYYY-MM-DD'));
				$scope.faq.start_date = start.format("YYYY-MM-DD");
				$scope.faq.end_date = end.format("YYYY-MM-DD");
			});

			$('#range').on('cancel.daterangepicker', function(ev, picker) {
				$('#range span').html('');
				$scope.faq.start_date = null;
				$scope.faq.end_date = null;
			});


            $('#modal-categorie').on('hidden.bs.modal', function () {
    			$scope.faq = angular.copy(faqModel);
                $scope.clearSelection();
                $scope.ms.clear();
			});

			$('#modal-categorie').on('shown.bs.modal', function () {

				$('#range span').html('');

                if (! $scope.isEmptySelection()) {
					console.log($scope.selectedNode);
                	$scope.faq.id = $scope.selectedNode.id;
                    $scope.faq.visible = $scope.selectedNode.visible;
                    $scope.faq.visible2 = $scope.selectedNode.visible_reserved_area;
                    $scope.faq.parent_id = $scope.selectedNode.parent_id;
					if($scope.selectedNode.min_date!=null && $scope.selectedNode.max_date!=null){
						$('#range span').html('<b>Início:</b> '+$scope.selectedNode.min_date + '&nbsp <b>Fim: </b>' + $scope.selectedNode.max_date);
						$scope.faq.start_date = $scope.selectedNode.min_date;
						$scope.faq.end_date = $scope.selectedNode.max_date;
					}





                    for (var i = 0; i < $scope.selectedNode.translations.length; ++i) {
                        var locale = $scope.selectedNode.translations[i].locale;
						$scope.faq.question.translations[locale] = $scope.selectedNode.translations[i].name;

					}

                    $scope.faq.tags = $scope.selectedNode.tags;
                    if ($scope.faq.tags && $scope.faq.tags.length) {
                        $scope.ms.setSelection($scope.faq.tags);
                    }
                    
                } else {
                    $scope.faq = angular.copy(faqModel);
                }
				if($scope.isInCreationMode()==true){
					$scope.faq.parent_id = $scope.selectedNode.id;
					$scope.faq.question.translations={};
					$scope.setInCreationMode()
				}

                if (! $scope.$$phase) {
                    $scope.$apply();
                }
            });
        });
	}

	angular.module('brainy.categories.backend').controller('CategoriesCreatorController', ['$scope', '$http', CategoriesCreatorController]);
})();