/**
 * Created by User on 08-03-2017.
 */

$(document).ready(function () {

    
    $('#type-field').change(function () {
        if($(this).val()=='client'){
            $('.send_to_all_company_users').fadeOut();
        }else{
            $('.send_to_all_company_users').fadeIn();
        }
    });

$(document).on('click','.add_cond_btn',function () {
    $('#add_new_condition').attr('data-type', $(this).attr('data-type'));
    $('#add_new_condition').attr('data-id', $(this).attr('data-id'));
});


    $('#add_new_condition').click(function () {

        var and_section='';
        var or_section='';
        var id_of_block=1;



        if($(this).attr('data-type')=='block'){


            if($('.container_of_blocks').children().length!=0){
                or_section='<div class="row or_interjection"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_condition"> <div>ou</div> </div> </div>';
                id_of_block=parseInt($('.container_of_blocks').children().last().find('.add_cond_btn').attr('data-id'))+1;
            }
            $('.container_of_blocks').append(or_section+'<div class="row  group_of_conditions" id="bloco_'+id_of_block+'"> <div class="col-lg-12  conditions_container "> </div> <button class="btn btn-default btn-yellow-icon   add_cond_btn"type="button" data-type="condition" data-id="'+id_of_block+'" data-target="#select_condition" data-toggle="modal"><iclass="icon-adicionar"></i> adicionar condição </button> </div>');
            if($('.container_of_blocks').children().last().find('.conditions_container').children().length!=0){
                var and_section='<div class="row and_interjection"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_condition_and"> <span class="e_item">e</span> </div> </div>';
            }
            $('.container_of_blocks').children().last().find('.conditions_container').append(and_section+$(".radio_filters input[type='radio']:checked").attr('data-html'));

        }else{


            var id_of_block=$('#add_new_condition').attr('data-id');


            if($('#bloco_'+id_of_block).find('.conditions_container').children().length!=0){
                var and_section='<div class="row and_interjection"> <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_condition_and"> <span class="e_item">e</span> </div> </div>';
            }

            $('#bloco_'+id_of_block).find('.conditions_container').append(and_section+$(".radio_filters input[type='radio']:checked").attr('data-html'));
        }



        $('#select_condition').modal('hide');

    });

    $(document).on('click','.delete_condition',function () {

        var pai=$(this).parent().parent().parent();
        if($(this).parent().parent().next().hasClass('and_interjection')){
            $(this).parent().parent().next().remove();
        }else if($(this).parent().parent().prev().hasClass('and_interjection')){
            $(this).parent().parent().prev().remove();
        }

        $(this).parent().parent().remove();


        if(pai.children().length==0){
            if(pai.parent().prev().hasClass('or_interjection')){
                pai.parent().prev().remove();
            }else if(pai.parent().next().hasClass('or_interjection')){
                pai.parent().next().remove();
            }

            pai.parent().remove();
        }

    });


});
//# sourceMappingURL=conditions.js.map
