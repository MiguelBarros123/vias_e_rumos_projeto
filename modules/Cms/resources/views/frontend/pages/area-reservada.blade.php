@extends('layouts.frontend.app')

@section('title', 'Vias e Rumos - Novo Registo')

@section('module-scripts')
    <script>
        $(document).ready(function(){
            var tab = "{{$tab}}";

            function changeTab(){
                var historico = $('a[href="#historico"]');
                var dados = $('a[href="#dados"]');
                if(tab == 'tab')
                {
                    $('a[href="#historico"]').tab("show");
                }else{
                    $('a[href="#dados"]').tab("show");
                }
            }

            changeTab();
        });
    </script>
@endsection

@section('content')
<div class="empresa-body cred-body reservada-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>Área Reservada</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <ul class="res-list">
                <li class="active" role="presentation">
                    <a href="#historico" data-toggle="tab" aria-expanded="true">
                        <h3>Histórico de Leilões</h3>
                    </a>
                </li>
                <li role="presentation">
                    <a href="#dados" data-toggle="tab">
                        <h3>Dados de Utilizador</h3>
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="tab-content">
        <div id="historico" class="tab-pane active">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            @foreach($auctions as $auction)
                    <div class="each_leilao">
                        <div class="top-leilao-list">
                            <div class="prod-title">
                                <h3>{{ $auction->executed}}</h3>
                                <div class="ref-top">
                                    Referência {{$auction->ref_intern}}
                                </div>
                            </div>
                            <div class="prod-top-wrap">
                                <div class="prod-data">
                                    <div class="prod-dad">
                                        <div class="info-title">
                                            Data:
                                        </div>
                                        <div class="info-value">
                                            @if($auction->date)
                                                {{str_replace(':','h',substr(\Carbon\Carbon::parse($auction->date)->toDateTimeString(),0,-3))}}
                                            @else
                                                Sem Data
                                            @endif         
                                        </div>
                                    </div>
                                    
                                    <div class="prod-dad">
                                        <div class="info-title">
                                            Local:
                                        </div>
                                        <div class="info-value">
                                            {{$auction->local}}
                                        </div>
                                    </div>

                                </div>
                                <div class="prod-files">
                                    <div class="auction_label_wrap">
                                        @if($auction->id%2 == 0)
                                            <div class="auction_label superada">
                                                Licitação superada
                                            </div>

                                        @else
                                            <div class="auction_label primeiro">
                                                Licitação em primeiro
                                            </div>
                                        @endif
                                        
                                    </div>
                                </div>
                            </div>
                            
                            @php
                                $auction_resource = $auction->lots()->first();
                            
                            @endphp
                            <div class="each_prod">
                                <div class="row prod-row">
                                    <div class="top-prod-list">
                                        <div class="prod-title">
                                            <h3>{{$auction_resource->designacao}}</h3>
                                        </div>
                                    </div>
                                    <div class="participar">
                                        <a href="{{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                                            <button>
                                                ver +
                                            </button>
                                        </a>
                                        <div class="prod-share hidden-md hidden-lg">
                                            <div class="share-word">
                                                partilha
                                            </div>
                                            <div class="share-icons">
                                                <ul class="icons-list">
                                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}"><span class="icon-facebook"></span></a></li>
                                                     <li><a href="https://plus.google.com/share?url={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span class="icon-google-plus"></span></a></li>
                                                                              <li><a href="http://twitter.com/share?text={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a></li>
                                                    <li><a href="mailto:?subject={{$auction_resource->name}} - {{$auction_resource->designacao}} &body={{$auction_resource->description}} Link: {{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}"><span class="icon-mail"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="displ-flex">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 flex-col">
                                            <div class="row hidden-md hidden-lg">
                                                <div class="col-xs-12 col-sm-12 text-area-wrap img-wrap">
                                                    @if($auction_resource->lots_resources()->first())
                                                        <div class="cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image_p',[$auction_resource->lots_resources()->first()->identifier_token,280,140])  }}')">
                                                             <a href="{{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                                                                <div class="more-prod-bg">
                                        
                                                                </div>
                                                                <div class="more-prod-text">
                                                                    Saber mais
                                                                </div>
                                                            </a>
                                                        </div>
                                                    @else
                                                        <div class="cover prod-img-list"  style="background-image: url('{{asset('front/imgs/lotes/placeholder.jpg')}}')">
                                                             <a href="{{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                                                                <div class="more-prod-bg">
                                        
                                                                </div>
                                                                <div class="more-prod-text">
                                                                    Saber mais
                                                                </div>
                                                            </a>
                                                        </div>    
                                                    @endif
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                                                    <div class="text-area">
                                                        <div class="text-wrap">
                                                            <div class="text-prod">
                                                            <span class="mobile-title hidden-md hidden-lg">Descrição:</span>
                                                            <!-- <div class="text-prod">Nome do lote</div>  -->


                                                            </div>
                                                            <div class="text-prod text-left">
                                                                @if(strlen($auction_resource->description)>100)
                                                                 {{ mb_substr(html_entity_decode($auction_resource->description, null, 'ISO-8859-1'),0,100)}}...

                                                                @else
                                                                    {{$auction_resource->description}}
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                                                    <div class="text-area">
                                                        <div class="text-wrap">
                                                            <span class="mobile-title hidden-md hidden-lg">Local:</span>
                                                            <div class="text-prod">
                                                                <a href="http://www.google.com/maps/place/{{$auction_resource->latitude}},{{$auction_resource->longitude}}" target="_blank">{{ $auction_resource->local}}</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                                                    <div class="text-area valor-area">
                                                        <div class="text-wrap">
                                                            <div class="text-prod">
                                                                @if($auction_resource->price)
                                                                    <div class="val_div">
                                                                        <b>Valor Base:</b> {{ $auction_resource->price}}
                                                                    </div>
                                                                @endif    
                                                                @if($auction_resource->min_price)
                                                                    <div class="val_div">
                                                                        <b>Valor Mínimo:</b> {{ $auction_resource->min_price}}
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <div class="row displ-flex bot-prod-row">
                                                <div class="col-lg-8 col-md-8 pad-5">
                                                    <div class="infos-wrap">
                                                        <div class="info">
                                                            <div class="val_div">
                                                                <b>Licitação Atual: </b> 12345€
                                                            </div>
                                                            <div class="val_div">
                                                                <b>Valor Licitado: </b> 12345€
                                                            </div>
                                                            <div class="val_div">
                                                                <b>Valor de Abertura: </b> 12345€
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 pad-5 hidden-xs hidden-sm">
                                                    <div class="files-share">
                                                        <ul class="files-share-list">
                                                            <li class="share-li">
                                                                <div class="prod-share">
                                                                    <div class="share-word">
                                                                        partilha
                                                                    </div>
                                                                    <div class="share-icons">
                                                                        <ul class="icons-list">
                                                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}"><span class="icon-facebook"></span></a></li>
                                                                               <li><a href="https://plus.google.com/share?url={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span class="icon-google-plus"></span></a></li>
                                                                              <li><a href="http://twitter.com/share?text={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a></li>

                                                                            <li><a href="mailto:?subject={{$auction_resource->name}} - {{$auction_resource->designacao}} &body={{$auction_resource->description}} Link: {{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}" ><span class="icon-mail"></span></a></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-area-wrap img-wrap hidden-xs hidden-sm">
                                            <!-- <div class=" cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image',[$auction_resource->first()->identifier_token])  }}');"> -->
                                            @if($auction_resource->lots_resources()->first())
                                                <div class=" cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image_p',[$auction_resource->lots_resources()->first()->identifier_token,280,140])  }}');">
                                                     <a href="{{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                                                        <div class="more-prod-bg">

                                                        </div>
                                                        <div class="more-prod-text">
                                                            Saber mais
                                                        </div>
                                                    </a>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                             <div class="line-separator">
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div id="dados" class="tab-pane">
            <div class="row m-t-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h3>Dados de Faturação</h3>
                        </div>
                    </div>

                    <form action="{{ route('cms::frontend::model.invoice_personal_information', $profile->id)}}" method="POST">
                        {{ csrf_field() }}
                        {{ method_field('PUT')}}
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="inp_label">Nome Fiscal * </div>
                                <div class="inps_wrap_cred">
                                    <input class="inp_av" placeholder="" value="{{$profile->nome_fiscal}}" name="nome_fiscal">
                                    @if($errors->has("nome_fiscal"))
                                        <span class="help-block">{{ $errors->first('nome_fiscal') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="inp_label">NIF/NIPC *  </div>
                                <div class="inps_wrap_cred">
                                    <input class="inp_av" placeholder="" value="{{$profile->nipc}}" name="nipc">
                                    @if($errors->has("nipc"))
                                        <span class="help-block">{{ $errors->first('nipc') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="inp_label">País * </div>
                                <div class="inps_wrap_cred">
                                     <select class="select_av" name="country">
                                        @foreach($countries as $country)
                                            <option value="{{$country->id}}" {{$country->id == $profile->country ? 'selected' : ''}}>{{$country->value}}</option>
                                        @endforeach
                                    </select>
                                    @if($errors->has("country"))
                                        <span class="help-block">{{ $errors->first('country') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="inp_label">Morada * </div>
                                <div class="inps_wrap_cred">
                                    <input class="inp_av" placeholder="" value="{{$profile->address}}" name="address">
                                    @if($errors->has("address"))
                                        <span class="help-block">{{ $errors->first('address') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="inp_label">Código Postal * </div>
                                <div class="inps_wrap_cred">
                                    <input class="inp_av" placeholder="" value="{{$profile->cod_postal}}" name="cod_postal">
                                    @if($errors->has("cod_postal"))
                                        <span class="help-block">{{ $errors->first('cod_postal') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                <div class="inp_label">Localidade * </div>
                                <div class="inps_wrap_cred">
                                    <input class="inp_av" placeholder="" value="{{$profile->local}}" name="local">
                                    @if($errors->has("local"))
                                        <span class="help-block">{{ $errors->first('local') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="btn-line">
                                    <button type="submit" name="" id="change_dados" class="cred-btn">Guardar Alterações</button>
                                </span>
                            </div>
                        </div>
                    </form>

                    <form action="{{route('cms::frontend::model.change_personal_information',$user->id)}}" method="POST">
                        {{csrf_field()}}
                        {{method_field('PUT')}}
                        <div class="row m-t-20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <h3>Password</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="inp_label">Password Antiga </div>
                                <div class="inps_wrap_cred">
                                    <input class="inp_av" placeholder="" name="old_password" type="password" value="{{old('old_password')}}">
                                    @if($errors->has("old_password"))
                                        <span class="help-block">{{ $errors->first('old_password') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="inp_label">Nova Password </div>
                                <div class="inps_wrap_cred">
                                    <input class="inp_av" placeholder="" name="new_password" type="password" value="">
                                    @if($errors->has("new_password"))
                                        <span class="help-block">{{ $errors->first('new_password') }}</span>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="inp_label">Confirmar Nova Password </div>
                                <div class="inps_wrap_cred">
                                    <input class="inp_av" placeholder="" name="new_password_confirmation" type="password" value="">
                                    @if($errors->has("new_password_confirmation"))
                                        <span class="help-block">{{ $errors->first('new_password_confirmation') }}</span>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <span class="btn-line">
                                    <button type="submit" name="" id="change_pass" class="cred-btn">Alterar Password</button>
                                </span>
                            </div>
                        </div>
                    </form>



                </div>
            </div>
        </div>
   </div>

</div>

@endsection
