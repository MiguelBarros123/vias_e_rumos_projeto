<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\Brainy\Catalog\Models\Categorie::class, function (Faker\Generator $faker) {
    return [
        'tax' => 0,
        'parent_id'=>0
    ];
});
$factory->define(\Brainy\Catalog\Models\Attribute::class, function (Faker\Generator $faker) {
    return [];
});

$factory->define(\Brainy\Catalog\Models\AttributeRegister::class, function (Faker\Generator $faker) {
    return [
            'pt'=>['name'=>$faker->title],
            'en'=>['name'=>$faker->title],
            'fr'=>['name'=>$faker->title]
    ];
});

$factory->define(\Brainy\Catalog\Models\Product::class, function (Faker\Generator $faker) {
    return [
            'pt'=>['name'=>$faker->title, 'description'=>$faker->text(50)],
            'en'=>['name'=>$faker->title,'description'=>$faker->text(50)],
            'fr'=>['name'=>$faker->title,'description'=>$faker->text(50)],
        'reference'=>$faker->title,
        'brand'=>$faker->title,
        'subbrand'=>$faker->title,
        'has_attributes'=>true,
        'tax'=>1,
    ];
});



