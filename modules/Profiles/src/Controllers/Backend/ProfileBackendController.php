<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-10-2016
 * Time: 14:48
 */

namespace Brainy\Profiles\Controllers\Backend;
use Brainy\Framework\Models\UserDetail;
use Brainy\Profiles\Models\CustomField;
use Brainy\Profiles\Models\ProfileAddress;
use Brainy\Profiles\Models\ProfileUser;
use Brainy\Profiles\Models\Sector;
use Brainy\Profiles\Repositories\ProfileRepositoryInterface;
use Brainy\Profiles\Requests\ProfileRequest;
use Carbon\Carbon;
use \Illuminate\Http\Request;
use App\User;
use Brainy\Cms\Models\FrontUser;
use Brainy\Cms\Models\MediaItem;
use Brainy\DefinitionsEcommerce\Models\Country;
use Brainy\Framework\Controllers\BackendController;
use Brainy\Framework\Models\Group;
use Brainy\Profiles\Models\Profile;
use Brainy\Profiles\Models\ProfileGroup;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\Types\This;
use Yajra\Datatables\Facades\Datatables;

class ProfileBackendController extends BackendController{

    protected $profileRepo;

    public function __construct(ProfileRepositoryInterface $profileRepo)
    {
        parent::__construct();
        $this->profileRepo = $profileRepo;
    }

    public function index()
    {
        //for correcting patch
     /*   foreach( User::all() as $user){
            if($user->profile==null){
                if($user->details!=null){
                    $user->profile()->create(['name'=>$user->name,'type'=>'back_user','birth_at'=>Carbon::now()]);
                }else{
                    $user->profile()->create(['name'=>$user->name,'type'=>'login_user','birth_at'=>Carbon::now()]);
                }
            }
        }*/

        return view('profiles::backend.clients.index');
    }

    public function indexCompany()
    {
        return view('profiles::backend.companys.index');
    }

    public function index_ajax()
    {

        $users = UserDetail::where('flags', 3)->pluck('id');

        return Datatables::of(Profile::where('type','!=','company')->whereNotIn('user_id', $users)
            ->orWhereIn('type',['back_user','no_login_user','login_user','reserved_area_user'])
            ->get())
            ->addColumn('name', function ($client) {

                if($client->thumbnail === ''){
                   $data = '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
                } else {
                   $data = '<img class="image_user_picked2" src="'.route('gallery::frontend::storage_image_p',[$client->thumbnail,50,50]).'">';
                }

                return $data .' '. $client->name .' '. $client->surname;

            })
            ->editColumn('job', function($client) {

                $company = Profile::find($client->company_id);

                if($client->job != '' && $company){
                    $company_name = ', <span class="profile_input">'. $company->name .'</span>';
                } else {
                    $company_name = '';
                }

                return $client->job .''. $company_name;

            })
            ->addColumn('contacts', function ($client) {

                $user = User::find($client->user_id);

                if($user){
                    $email = $user->email;
                } else {
                    $personalEmail = $client->profile_contacts()->where('type','personal_email')->first();

                    if($personalEmail) {
                        $email = $personalEmail->contact;
                    } else {
                        $email = '';
                    }
                }

                return $email;

            })
            ->addColumn('action', function ($client) {


                $delete_route = route('profiles::backend::profiles.delete_multiple_clients');
                $view_route = route('profiles::backend::profiles.view',[$client->id]);
                $edit_route = route('profiles::backend::profiles.edit',[$client->id,'person']);


                $string = '
                        <div class="btn-group pull-right">
                           <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a  class="open_edit_atrribute_modal" href="'.$view_route.'" ><i class="icon-visivel"></i>  '.trans("profiles::backend.common.view").'</a></li>
                            <li><a  class="open_edit_atrribute_modal" href="'.$edit_route.'" ><i class="icon-editar"></i>  '.trans("profiles::backend.common.edit").'</a></li>
                            <li><a data-impossible-delete="0" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="'. $client->id .'" data-route="'.$delete_route.'"><i class="icon-opcoes-eliminar"></i> '.trans("profiles::backend.common.delete").' </a></li>
                          </ul>
                        </div>

                       ';
                return $string;

            })
            ->addColumn('rota_final', function ($client) {
                return route('profiles::backend::profiles.view',[$client->id]);
            })
            ->make(true);
    }

    public function index_companies_ajax()
    {
        //for correcting patch
        foreach( User::all() as $user){
            if($user->profile==null){
                if($user->details!=null){
                    $user->profile()->create(['type'=>'back_user']);
                }
                if($user->front_user!=null){
                    if($user->front_user->is_active == 1){
                        $user->profile()->create(['type'=>'login_user']);
                    }
                    if($user->front_user->reserved_area == 1){
                        $user->profile()->create(['type'=>'reserved_area_user']);
                    }

                }

            }
        }

        return Datatables::of(Profile::whereIn('type',['company'])->orderBy('created_at','asc')->get())
            ->addColumn('name', function ($client) {

                if($client->thumbnail === ''){
                    $data = '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
                } else {
                    $data = '<img class="image_user_picked2" src="'.route('gallery::frontend::storage_image_p',[$client->thumbnail,50,50]).'">';
                }

                return $data .' '. $client->name;

            })
            ->addColumn('contacts', function ($client) {

                $companyEmail = $client->profile_contacts()->where('type','company_email')->first();

                if($companyEmail){
                    $email = $companyEmail->contact;
                } else {
                    $email = '';
                }

                return $email;

            })
            ->addColumn('action', function ($client) {


                $delete_route = route('profiles::backend::profiles.delete_multiple_companies');
                $view_route = route('profiles::backend::profiles.view',[$client->id]);
                $edit_route = route('profiles::backend::profiles.edit',[$client->id,'company']);


                $string = '
                        <div class="btn-group pull-right">
                           <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a  class="open_edit_atrribute_modal" href="'.$view_route.'" ><i class="icon-visivel"></i>  '.trans("profiles::backend.common.view").'</a></li>
                            <li><a  class="open_edit_atrribute_modal" href="'.$edit_route.'" ><i class="icon-editar"></i>  '.trans("profiles::backend.common.edit").'</a></li>
                            <li><a data-impossible-delete="0" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="'. $client->id .'" data-route="'.$delete_route.'"><i class="icon-opcoes-eliminar"></i> '.trans("profiles::backend.common.delete").' </a></li>
                          </ul>
                        </div>

                       ';
                return $string;

            })
            ->addColumn('rota_final', function ($client) {
                return route('profiles::backend::profiles.view',[$client->id]);
            })
            ->make(true);
    }

    public function edit($id,$type){

        $locales=config('translatable.locales');
        $companies=Profile::where('type','company')->get();
        
        $data = $this->profileRepo->edit($id);

        $months = $this->profileRepo->getMonths();

        $languages = $this->profileRepo->getLanguages();


        return view('profiles::backend.profile.edit',compact('companies'))
            ->with('locales',$locales)
            ->with('type',$type)
            ->with('months',$months)
            ->with('languages',$languages)
            ->with($data);
    }

    public function view($id){
        $profile=Profile::findOrFail($id);

        $languages = $this->profileRepo->getLanguages();

        if($profile->language){
            $language = $languages[$profile->language];
        } else {
            $language = null;
        }

        $data = [
            'facebook' => $profile->profile_contacts()->where('type','facebook')->first(),
            'instagram' => $profile->profile_contacts()->where('type','instagram')->first(),
            'google' => $profile->profile_contacts()->where('type','google')->first(),
            'youtube' => $profile->profile_contacts()->where('type','youtube')->first(),
            'others' => $profile->profile_contacts()->where('type','other')->get()
        ];

        if($profile->type === 'company'){

            $users = $profile->users();

            $data['email'] = $profile->profile_contacts()->where('type','company_email')->first();
            $data['phone'] = $profile->profile_contacts()->where('type','company_phone')->first();
            $data['mobile'] = $profile->profile_contacts()->where('type','company_mobile')->first();
            $data['fax'] = $profile->profile_contacts()->where('type','company_fax')->first();

            return view('profiles::backend.companys.view',compact('profile'))->with('users', $users)->with('language', $language)->with($data);
        } else {

            $company = null;

            if($profile->company_id){
                $company = Profile::findOrFail($profile->company_id);
            }

            return view('profiles::backend.profile.view',compact('profile'))->with('company', $company)->with('language', $language)->with($data);
        }
    }

    public function create($type){

        $locales=config('translatable.locales');
        $companies=Profile::where('type','company')->get();

        $data = $this->profileRepo->create();

        $months = $this->profileRepo->getMonths();

        $languages = $this->profileRepo->getLanguages();
        $personalized_fields = CustomField::all();



        return view('profiles::backend.profile.create',compact('companies'))
            ->with('locales',$locales)
            ->with('type',$type)
            ->with('months',$months)
            ->with('languages',$languages)
            ->with('personalized_fields',$personalized_fields)
            ->with($data);
    }


    public function store(Request $request)
    {
//        dd($request->except('_token'));

        $validator = Validator::make($request->all(), [
            'email' => 'required_if:is_user,on|unique:users,email',
            'password' => 'required_if:is_user,on|min:8'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withInput(request()->input())->withErrors($validator);
        }

        if(array_key_exists('is_user', $request->input())){
            if(array_key_exists('email',$request->input())){
                $user = User::where('email', $request->email)->first();
                if($user){
                    return redirect()->back()->with('error-message', trans('profiles::backend.messages.error.existing_user'));
                }
            }
        }

        $profile = $this->profileRepo->store($request);

        if($request->submit === 'stay') {
            if($request->is_company==='company'){
                return redirect()->route('profiles::backend::companies.edit', [$profile->id, 'company'])->with('sucess-message', trans('profiles::backend.messages.success.create_profile'));
            } else {
                return redirect()->route('profiles::backend::profiles.edit', [$profile->id, 'person'])->with('sucess-message', trans('profiles::backend.messages.success.create_profile'));
            }

        }

        if($request->is_company==='company'){
            return redirect()->route('profiles::backend::companies.index')->with('sucess-message', trans('profiles::backend.messages.success.create_profile'));
        }
        return redirect()->route('profiles::backend::profiles.index')->with('sucess-message', trans('profiles::backend.messages.success.create_profile'));
    }

    public function update(Request $request,$id)
    {

//        $validator = Validator::make($request->all(), [
//            'email' => 'required_if:is_user,on|unique:users,email',
//            'password' => 'required_if:is_user,on|min:8'
//        ]);
//
//        if ($validator->fails()) {
//            return redirect()->back()->withErrors($validator);
//        }

//        $profile = Profile::findOrFail($id);

//        if(array_key_exists('is_user', $request->input())){
//            if(array_key_exists('email',$request->input())){
//                $user = User::where('email', $request->email)->first();
//                if($user){
//                    if($user->id != $profile->user_id){
//                        return redirect()->back()->with('error-message', trans('profiles::backend.messages.error.existing_user'));
//                    }
//                }
//            }
//        }

        if(array_key_exists('email', $request->input())){
            $validator = Validator::make($request->all(), [
                'email' => 'required_if:is_user,on|unique:users,email',
                'password' => 'required_if:is_user,on|min:8'
            ]);

            if ($validator->fails()) {
                return redirect()->back()->withErrors($validator);
            }

            if(array_key_exists('is_user', $request->input())){
                if(array_key_exists('email',$request->input())){
                    $user = User::where('email', $request->email)->first();
                    if($user){
                        return redirect()->back()->with('error-message', trans('profiles::backend.messages.error.existing_user'));
                    }
                }
            }
        }

        $this->profileRepo->update($request, $id);

        if($request->submit === 'stay') {
            return redirect()->back()->with('sucess-message', trans('profiles::backend.messages.success.update_profile'));
        }

        if($request->is_company==='company'){
            return redirect()->route('profiles::backend::companies.index')->with('sucess-message', trans('profiles::backend.messages.success.update_profile'));
        }

        return redirect()->route('profiles::backend::profiles.index')->with('sucess-message', trans('profiles::backend.messages.success.update_profile'));
    }

    public function getPersonsNoCompany($me=null){

        $users = UserDetail::where('flags', 3)->pluck('id');

        if($me!=null){
            return Profile::doesntHave('company')->where('id','!=',$me)->whereNotIn('id', $users)->get()->toJson();
        }
        return Profile::doesntHave('company')->whereNotIn('id', $users)->get()->toJson();
    }

    public function getProfiles($id=null)
    {
        $users = UserDetail::where('flags', '!=', 3)->pluck('id');

        if($id!=null){
            $profiles = Profile::where('type', '!=','company')->where('type', '!=','no_login_user')->whereNotIn('id', $users)->where('company_id', null)->orWhere('company_id', $id)->where('id','!=',$id)->get();
        } else {
            $profiles = Profile::where('type', '!=','company')->where('type', '!=','no_login_user')->whereNotIn('id', $users)->where('company_id', null)->get();
        }

        return Datatables::of($profiles)
            ->editColumn('name', function($profile) {
                if($profile->thumbnail === ''){
                    $img = '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
                } else {
                    $url = route("gallery::frontend::storage_image_p",[$profile->thumbnail,50,50]);
                    $img = '<img class="image_user_picked2" src="'.$url.'" />';
                }

                return $img .' '. $profile->name .' '. $profile->surname;
            })
            ->editColumn('email', function($profile) {
                if($profile->user){
                    return $profile->user->email;
                } else {
                    return '';
                }
            })
            ->addColumn('reserved_area', function($profile) {
                if($profile->reserved_area){
                    return 'Sim';
                } else {
                    return 'Não';
                }
            })
            ->addColumn('already_selected', function ($profile)use($id) {
                if($id!=null){
                    if($profile->company_id == $id){
                        return 1;
                    }
                }
                return 0;
            })
            ->make(true);

    }


    public function deleteMultipleClients(Request $request)
    {
        foreach(explode(',',$request->deleted_rows) as $client){

            $profile = Profile::findOrFail($client);
            $profile->delete();            
        }

        return redirect()->back()->with('sucess-message', trans('profiles::backend.messages.success.delete_clients'));
    }

    public function deleteMultipleCompanies(Request $request)
    {
        foreach(explode(',',$request->deleted_rows) as $company){

            $profiles = Profile::whereIn('type',['company'])->where('company_id', $company)->get();

            foreach ($profiles as $profile) {
                $profile->delete();
            }

            Profile::destroy($company);

        }

        return redirect()->back()->with('sucess-message', trans('profiles::backend.messages.success.delete_companies'));
    }

    public function getGroup($id)
    {

        $group = ProfileGroup::findOrFail($id);

        $comercial_condition = $group->condition()->first();

        $data = [];

        if($comercial_condition){
            $data['reserved_area'] = true;
            $data['message'] = trans('profiles::backend.common.has_access_reserved_area');
        } else {
            $data['reserved_area'] = false;
            $data['message'] = trans('profiles::backend.common.has_not_access_reserved_area');
        }

        return $data;

    }

    public function getCompanies()
    {
        return Profile::where('type','company')->get()->toJson();
    }

}
