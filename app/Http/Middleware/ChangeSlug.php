<?php

namespace App\Http\Middleware;

use Brainy\Cms\Models\Page;
use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Foundation\Application;
use Illuminate\Routing\Redirector;
use Illuminate\Http\Request;

class ChangeSlug
{

    public function __construct(Application $app, Redirector $redirector, Request $request) {
        $this->app = $app;
        $this->redirector = $redirector;
        $this->request = $request;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        //dd('easy');


        // Make sure current locale exists.
        $locale = $request->segment(1);




        if ( !in_array($locale,config('translatable.locales'))) {




            $segments = $request->segments();


            if(Session::has('locale')){
                $segments[0]=Session::get('locale');
            }else{
                Session::put('locale', 'en');
                $segments[0]=Session::get('locale');
            }

            //dd($segments);
            return $this->redirector->to(implode('/', $segments));
        }else{




            if(session('locale') != $locale){

                $segments = $request->segments();

                if(session('c_lang')===1){
                    $segments[0]=session('locale');
                }else{
                    $segments[0]=$locale;
                }


                $novo_seg=Page::convertUrl($request->route()->getName(),$segments[0]);


                if($novo_seg!=false){
                    $segments[1]=$novo_seg;
                }

                Session::put('locale', $segments[0]);

                $this->app->setLocale($locale);

                return $this->redirector->to(implode('/', $segments));
            }


            //Session::put('locale', $locale);
            $this->app->setLocale(session('locale'));


        }





        return $next($request);
    }
}
