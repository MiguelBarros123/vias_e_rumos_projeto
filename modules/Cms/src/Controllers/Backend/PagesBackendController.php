<?php

namespace Brainy\Cms\Controllers\Backend;

use Brainy\Cms\Models\CHighlight;
use Brainy\Catalog\Models\Location;
use Brainy\Catalog\Models\Product;
use Brainy\Catalog\Models\Variant;
use Brainy\Catalog\Models\ProductResource;
use Brainy\Cms\Models\Composed;
use Brainy\Cms\Models\ContactsInput;
use Brainy\Cms\Models\Page;
use Brainy\Cms\Models\PageContact;
use Brainy\Cms\Models\Representative;
use Brainy\Cms\Models\RepresentativeContact;
use Brainy\Cms\Models\RepresentativeEmail;
use Brainy\Cms\Requests\RepresentativeRequest;
use Brainy\Cms\Requests\RepresentativeReorderRequest;
use Brainy\Cms\Models\Highlight;
use Brainy\Cms\Requests\HighlightRequest;
use Brainy\Cms\Models\Text;
use Brainy\Framework\Controllers\BackendController;
use Brainy\Gallery\Models\MediaFolder;
use Brainy\Gallery\Models\MediaItem;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use PhpParser\Node\Stmt\TraitUseAdaptation\Precedence;
use Yajra\Datatables\Facades\Datatables;
use Exception;

class PagesBackendController extends BackendController
{

    public function index_edit_page($id = 0)
    {

    //$data = [
    //            'name' => 'José',
    //            'phone' => '111222333',
    //            'email' => 'jose.venancio@thesilverfactory.pt',
    //            'subject' => 'Email',
    //            'lang' => session('locale')
    //        ];

    //return view('cms::frontend.emails.contact')->with($data);

        $page = ($id == 0) ? Page::first() : Page::findOrFail($id);

        $locales = config('translatable.locales');

        $gallery_content = MediaFolder::with('media_items')->get()->toJson();

        $pasta = MediaFolder::findOrFail(1);

        $all_folders = MediaFolder::all();

        $product_sections = collect();

        if(class_exists('Brainy\Catalog\Models\Location')){
            $product_sections = Location::where('is_a_page_section', 1)->get();
        }

        if($page->is_contacts) {
            $contacts_info = PageContact::first();
            $inputs = ContactsInput::all();
            return view('cms::backend.pages.contacts', compact('page'))
                    ->with('locales',$locales)
                    ->with('contacts_info',$contacts_info)
                    ->with('inputs',$inputs);
        }

        return view('cms::backend.pages.template.index', compact('page'))
            ->with('locales',$locales)
            ->with('gallery_content',$gallery_content)
            ->with('pasta', $pasta)
            ->with('product_sections', $product_sections)
            ->with('all_folders', $all_folders);
    }

    public function edit_composed_highlight($id)
    {
        $highlight = CHighlight::findOrFail($id);

        $locales = config('translatable.locales');
        $page_id = $highlight->composed->page->id;

        $gallery_content = MediaFolder::with('media_items')->get()->toJson();

        $pasta = MediaFolder::findOrFail(1);

        $all_folders = MediaFolder::all();

        //return view('cms::backend.pages.template.edit', compact('highlight', 'locales', 'page_id', 'gallery_content'));

        return view('cms::backend.pages.template.edit', [
            'highlight' => $highlight,
            'locales' => $locales,
            'page_id' => $page_id,
            'gallery_content' => $gallery_content,
            'pasta' => $pasta,
            'all_folders' => $all_folders
        ]);
    }

    public function edit_representative(Request $request, $id)
    {

        dd($request->except('_token'));

        $representative = Representative::findOrFail($id);

        $page = Page::findOrFail(4);

        return view('cms::backend.pages.modals.representatives.edit', compact('representative', 'page'));
    }

    public function save_page(Request $request)
    {
//        dd($request->except('_token'));
        $locales = config('translatable.locales');
        $highlights = $request->input('highlight');

        if($highlights) {
            foreach($highlights as $key => $highlight) {

                $high = Highlight::findOrFail($key);
//                $high->update(['link' => $highlight['link']]);
                foreach($locales as $loc) {
                    $high->translateOrNew($loc)->title = $highlight[$loc]['title'];
                    $high->translateOrNew($loc)->description = $highlight[$loc]['description'];
//                    $high->translateOrNew($loc)->name_link = $highlight[$loc]['name_link'];
                }

                if($high->links){
                    foreach ($high->links as $link){
                        foreach($locales as $loc) {
                            $link->translateOrNew($loc)->name_link = $highlight[$loc]['name_link'][$link->id];
                            $link->translateOrNew($loc)->link = $highlight[$loc]['link'][$link->id];
                        }

                        $link->save();
                    }
                }

                if($highlight['image_id'] != '') {
                    $old = '';
                    if ($high->image) {
                        $old = MediaItem::where('identifier_token', $high->image)->first();
                    }
                    //$old = MediaItem::where('identifier_token', $high->image)->firstOrFail();
                    if($old) {
                        $old->used = 0;
                        $old->save();
                    }
                    $image = MediaItem::findOrFail($highlight['image_id']);
                    $image->used = 1;
                    $image->save();
                    $high->image = $image->identifier_token;
                }

                $high->save();


            }
        }

        $texts = $request->input('text');

        if($texts) {
            foreach($texts as $key => $text) {

                $t = Text::findOrFail($key);

                foreach($locales as $loc) {
                    /*$t->translate($loc)->update([
                        'title' => $text[$loc]['title'],
                        'subtitle' => $text[$loc]['subtitle'],
                        'description' => $text[$loc]['description']
                        ]);*/
                    $t->translateOrNew($loc)->title = $text[$loc]['title'];
                    $t->translateOrNew($loc)->subtitle = $text[$loc]['subtitle'];
                    $t->translateOrNew($loc)->description = $text[$loc]['description'];
                }

                $t->save();
            }
        }

        return redirect()->back()->with('sucess-message',trans('cms::backend.messages.success.update_pages'));

    }

    public function create_composed_highlight($id)
    {

        $page = Composed::findOrFail($id)->page->id;

        $locales = config('translatable.locales');

        $gallery_content = MediaFolder::with('media_items')->get()->toJson();

        $pasta = MediaFolder::findOrFail($id);

        $all_folders = MediaFolder::all();

        return view('cms::backend.pages.template.create', [
            'page_id' => $page,
            'locales' => $locales,
            'composed_id' => $id,
            'gallery_content' => $gallery_content,
            'pasta' => $pasta,
            'all_folders' => $all_folders
        ]);
    }

    public function create_resource($location){

      $products = Product::all();
      $variants = '';

      return view('cms::backend.pages.template.create_resource', [
        'page_id' => 1,
        'location' => $location,
        'products' => $products,
        'variants' => $variants,
      ]);
    }

    public function getProductsVariant($id) {

      $product = Product::findOrFail($id);
      $variants = $product->variants;

        $result = '';

      foreach ($variants as $variant){
          $result .='<option value="'.$variant->id.'">'.$variant->reference.'
                                                    ( ';
          foreach($variant->attribute_registers as $reg){
              $result .= $reg->name;
              $result .= ' ';
          }

          $result .=') </option>';
      }

      return response()->json(['result' => $result]);
    }

    public function getVariantsResource($prod_id, $var_id) {

      // $product = Product::findOrFail($prod_id);
      //
      // $location = Location::where('resource_location', 'catalog::backend.common.store')->firstOrFail();
      // $first = $product->product_resources()->whereHas('locations', function ($query) use ($location) {
      //     $query->where('order', 1)->where('location_id', $location->id);
      // })->first();
      //
      // $secound = $product->product_resources()->whereHas('locations', function ($query) use ($location) {
      //     $query->where('order', 2)->where('location_id', $location->id);
      // })->first();

      $variant = Variant::findOrFail($var_id);

      $resources = $variant->product_resources;

      return response()->json([
        // 'first' => $first,
        // 'secound' => $secound,
        'variant' => $variant,
        'resources' => $resources
      ]);

    }


    public function edit_contacts()
    {
        return view('cms::backend.pages.template.create');
    }

    public function getRepresentatives()
    {
        $representatives = Representative::all();

        return Datatables::of($representatives)
                ->editColumn('position', function($representative) {
                    return "<i class=\"icon-icon-peq-img-size\"></i>$representative->position";
                })
                ->editColumn('emails', function($representative) {
                    return join('<br>', $representative->emails->pluck('email')->toArray());
                })
                ->editColumn('contacts', function($representative) {
                    return join('<br>', $representative->contacts->pluck('contact')->toArray());
                })
                ->addColumn('action', function ($representative) {
                    $route_del = route('cms::backend::representative.destroy', [$representative->id]);
                    $route_edit = route('cms::backend::representative.update', [$representative->id]); //route('cms::backend::representative.update', [$user->id]);

                    return '
                            <div class="btn-group pull-right">
                              <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                  <i class="icon-roda-dentada icon10"></i>
                              </button>
                              <ul class="dropdown-menu">
                                <li>
                                    <a  class="open_edit_representative_modal" data-route="'.$route_edit.'" data-names="" data-attributes="'.base64_encode($representative->toJson()).'">
                                        <i class="icon-editar"></i>  '.trans("cms::backend.model.pages.representatives.common.edit").'
                                    </a>
                                </li>
                                <li>
                                    <a data-target="#delete_resource"
                                        data-toggle="modal"
                                        class="delete_item_table"
                                        data-id="' . $representative->id . '"
                                        data-route="'.$route_del.'">
                                    <i class="icon-opcoes-eliminar"></i>Eliminar</a>
                                </li>
                              </ul>
                            </div>
                           ';
                })
                ->make(true);
    }

    public function storeCompoded($id, HighlightRequest $request)
    {

        $locales = config('translatable.locales');

        $c_highlight = new CHighlight();

        $last = CHighlight::where('composed_id', $id)->orderBy('order', 'desc')->first();

        if($last) {
          $c_highlight->order = $last->order + 1;
        } else {
          $c_highlight->order = 1;
        }

        if($request['image_id'] != ''){
            if($c_highlight->photo){
              $old = MediaItem::where('identifier_token', $c_highlight->photo)->firstOrFail();
              if($old){
                  $old->used = 0;
                  $old->save();
              }
            }
            $image = MediaItem::findOrFail($request['image_id']);
            $image->used = 1;
            $image->save();
            $c_highlight->photo = $image->identifier_token;
        }

        $c_highlight->composed_id = $id;

        if($request['visible']){
            $c_highlight->visible = 1;
        }else{
            $c_highlight->visible = 0;
        }

        $c_highlight->save();

        if($request->submit == 'stay'){
            return redirect()
                ->back()
                ->with('sucess-message',
                    trans('cms::backend.messages.success.added'));
        } else {
            return redirect()
                ->route('cms::backend::pages.edit_page', [$request->page_id])
                ->with('sucess-message',
                    trans('cms::backend.messages.success.added'));
        }

    }

    public function storeRepresentative(RepresentativeRequest $request)
    {
        $representative = new Representative();

        if($request->order){
            Representative::where('position','>=',$request->order)->increment('position');

            $representative->position = $request->order;
        } else {
            $last = Representative::orderBy('position', 'desc')->first();
            if($last){
                $representative->position = $last->position + 1;
            } else {
                $representative->position = 1;
            }
        }

        $representative->name = $request->name;
        $representative->job_position = $request->job_position;

        $representative->save();

        if($request->contacts){
            foreach ($request->contacts as $contact){
                if(RepresentativeContact::where('contact', $contact)->first() == null){
                    $r_contact = new RepresentativeContact();
                    $r_contact->representative_id = $representative->id;
                    $r_contact->contact = $contact;

                    $r_contact->save();
                }
            }
        }

        if($request->emails){
            foreach ($request->emails as $email){
                if(RepresentativeEmail::where('email', $email)->first() == null){
                    $r_email = new RepresentativeEmail();
                    $r_email->representative_id = $representative->id;
                    $r_email->email = $email;

                    $r_email->save();
                }
            }
        }

        return redirect()
            ->back()
            ->with('sucess-message',
                trans('cms::backend.messages.success.added'));

    }

    public function storeResource($id, Request $request)
    {
      $last = DB::table('location_product_resource')->where('location_id', $id)->orderBy('order', 'desc')->first();

      $resource = ProductResource::findOrFail($request->resource);

      $location = Location::findOrFail($id);

      if($last){
          $resource->locations()->save($location, ['order' => $last->order + 1]);
      } else {
          $resource->locations()->save($location, ['order' => 1]);
      }

      return redirect()
          ->back()
          ->with('sucess-message',
              trans('cms::backend.messages.success.added'));
    }

    public function updateRepresentative($id, RepresentativeRequest $request)
    {
        $representative = Representative::findOrFail($id);

    }

    public function deleteRepresentative($id)
    {
        try {
            $r = Representative::findOrFail($id);
            Representative::where('position','>',$r->position)->decrement('position');
            Representative::destroy($id);
            return redirect()->back()->with('sucess-message', trans('cms::backend.messages.success.delete.representative', [$id]));
        } catch (Exception $e) {
            //var_dump($e->getMessage());
            return redirect()->back()->with('error-message', trans('cms::backend.message.error.delete.representative', [$id]));
        }
    }

    public function deleteResource($id)
    {
        try {
            $recurso=ProductResource::findOrFail($id);
            $localizacoes=$recurso->locations()->where('is_a_page_section',true)->get();

            foreach($localizacoes as $loc){

                DB::table('location_product_resource')->where('location_id',$loc->id)->whereIn('product_resource_id',$recurso->id)->where('order','>',$loc->pivot->order)->decrement('order');

            }

            $recurso->delete();
            return response()->json(['success' => trans('cms::backend.messages.success.delete.representative'), $id]);
        } catch (Exception $e) {
            return response()->json(['error' => trans('cms::backend.message.error.delete.representative', $id)]);
        }
    }

    public function get_ajax($id)
    {

        $composed = Composed::findOrFail($id)->c_hilights();

        return Datatables::of($composed)
            ->addColumn('action', function ($user) {
                $rota = route('cms::backend::pages.delete_multiple_composeds');
                $edit_route = route('cms::backend::pages.edit', array($user->id));

                $tab = Composed::findOrFail($user->composed_id);

                    $string = '
                        <div class="btn-group pull-right">
                          <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a href="' . $edit_route . '" class=""><i class="icon-editar"></i>'. trans("cms::backend.model.common.edit") .'</a></li>
                            <li><a data-impossible-delete="0" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $user->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("cms::backend.model.common.delete") . ' </a></li>
                          </ul>
                        </div>
                       ';


//                $string = '
//                        <div class="btn-group pull-right">
//                          <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//                              <i class="icon-roda-dentada icon10"></i>
//                          </button>
//                          <ul class="dropdown-menu">
//                            <li><a href="' . $edit_route . '" class=""><i class="icon-editar"></i>'. trans("cms::backend.model.common.edit") .'</a></li>
//                            <li><a data-impossible-delete="0" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $user->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("cms::backend.model.common.delete") . ' </a></li>
//                          </ul>
//                        </div>
//                       ';
                return $string;
            })
            ->editColumn('photo', function ($table) {
                if($table->photo){
                    $photo = route('gallery::backend::storage_image', $table->photo);
                    return '<img class="image_datatable_preview" src="'.$photo.'">';
                } else {
                  return '<div class="icon-icon-upload-img icon30"></div>';
                }
            })
            // ->addColumn('identifier', function ($table) {
            //     return $table->photo;
            // })
            ->addColumn('visibleMessage', function ($composed) {
                return '<div id="info-hidden-composed-' . $composed->id . '"
                            class="composed-hidden-info"
                            style="visibility: ' . ($composed->visible ? 'hidden' : 'visible') . ';">
                                <i class="icon-bloqueado icon20"></i>
                                <span>' . trans('cms::backend.composed.index.info-hidden') . '</span>
                        </div>';
            })
            ->addColumn('visible', function ($user) {

                $routeState = route('cms::backend::model.change_composed_visibility', [$user->id]);
                
                $tab = Composed::findOrFail($user->composed_id);

                return '<span>'.trans("cms::backend.model.common.no").'</span><input id="toggle-one'.$user->id.'" data-row="'.$user->id.'" type="checkbox" '.($user->visible ? 'checked' : '').' data-on=" " data-off=" " name="apply" data-style="ios new_toggle"> <span class="block-icon">'.trans("cms::backend.model.common.yes").'</span>';

            })
            ->editColumn('order', function ($table) {
                return '<i class="icon-icon-peq-img-size"></i>'.$table->order;
            })
            ->addColumn('composedVisibility', function ($composed) {
                return $composed->visible;
            })
            // ->editColumn('date', function ($composed) {
            //     return Carbon::parse($composed->date)->format('d/m/Y');
            // })
            ->make(true);
    }

    public function get_section_ajax($id)
    {
        $location = Location::findOrFail($id)->product_resources();

        return Datatables::of($location)
            ->addColumn('action', function ($user) {
                $rota = route('cms::backend::pages.delete_multiple_resources');

                $string = '
                        <div class="btn-group pull-right">
                          <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a data-impossible-delete="0" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $user->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("cms::backend.model.common.delete") . ' </a></li>
                          </ul>
                        </div>
                       ';
                return $string;
            })
            ->editColumn('photo', function ($table) {
                if($table->identifier_token){
                    $photo = route('gallery::backend::storage_image', $table->identifier_token);
                    return '<div class="resource_datatable_preview" style="background:url('.$photo.')"></div>';
                } else {
                  return '<div class="icon-icon-upload-img icon30"></div>';
                }
            })
            ->addColumn('identifier', function ($table) {
                return $table->identifier_token;
            })
            ->editColumn('order', function ($table) {
                return '<i class="icon-icon-peq-img-size"></i>'.$table->order;
            })
            ->make(true);
    }

    public function reorderRepresentative(RepresentativeReorderRequest $request)
    {

        //forward
        if($request['fromPosition']  < $request['toPosition'] ) {
            Representative::where('position','>',$request['fromPosition'])->where('position','<=',$request['toPosition'])->decrement('position');
        }

        //back
        if($request['fromPosition'] > $request['toPosition']) {
            Representative::where('position','>=',$request['toPosition'])->where('position','<',$request['fromPosition'])->increment('position');
        }

        Representative::findOrFail($request['id'])->update(array('position'=>$request['toPosition']));

    }

    public function reorderComposedElements(Request $request, $composed)
    {
        //forward
        if($request->fromPosition < $request->toPosition) {
            CHighlight::where('composed_id',$composed)->where('order','>',$request->fromPosition)->where('order','<=',$request->toPosition)->decrement('order');
        }

        //back
        if($request['fromPosition'] > $request['toPosition']) {
            CHighlight::where('composed_id',$composed)->where('order','>=',$request->toPosition)->where('order','<',$request->fromPosition)->increment('order');
        }

        CHighlight::where('composed_id',$composed)->where('id', $request->id)->update(array('order'=>$request->toPosition));

    }

    public function reorderResourceElements(Request $request, $location)
    {
        //forward
        if($request->fromPosition < $request->toPosition) {
            DB::table('location_product_resource')->where('location_id', $location)
              ->where('order', '>', $request->fromPosition)
              ->where('order', '<=', $request->toPosition)->decrement('order');
        }

        //back
        if($request->fromPosition > $request->toPosition) {
            DB::table('location_product_resource')->where('location_id', $location)
              ->where('order', '>=', $request->toPosition)
              ->where('order', '<', $request->fromPosition)->increment('order');
        }

        DB::table('location_product_resource')->where('location_id', $location)
          ->where('id', $request->id)
          ->update(array('order' => $request->toPosition));

    }

    public function update_contacts(Request $request)
    {
        $trans=DB::transaction(function () use ($request) {

            PageContact::findOrFail(1)->update(['address'=>$request->address,'lat'=>$request->latitude,'long'=>$request->longitude]);

            $inputs=array_except($request->input(),['_method','_token','address','latitude','longitude']);
            foreach($inputs as $key=>$val){
                ContactsInput::where('key','like',$key)->update(['value'=>$val]);
            }

        });

        return redirect()->back()->with('sucess-message',trans('cms::backend.messages.success.update_contacts'));
    }

    public function save_highlight($id, Request $request)
    {

        $locales = config('translatable.locales');

        $c_highlight = CHighlight::findOrFail($id);

        $old = MediaItem::where('identifier_token', $c_highlight->photo)->first();
        if($old){
            $old->used = 0;
            $old->save();
        }

        if($request['image_id'] != ''){
            $image = MediaItem::findOrFail($request['image_id']);
            $image->used = 1;
            $image->save();
            $c_highlight->photo = $image->identifier_token;
        }

        if($request['visible']){
            $c_highlight->visible = 1;
        }else{
            $c_highlight->visible = 0;
        }

        $c_highlight->save();

        if($request->submit == 'stay'){
            return redirect()->back()->with('sucess-message',trans('cms::backend.messages.success.update_contacts'));
        } else {
            return redirect()
                ->route('cms::backend::pages.edit_page', [$request->page_id])->with('sucess-message',trans('cms::backend.messages.success.update_contacts'));
        }

        return redirect()->back()->with('sucess-message',trans('cms::backend.messages.success.update_contacts'));

    }

    public function save_representative($id, Request $request)
    {

        $representative = Representative::findOrFail($id);

        if($representative){
            $representative->name = $request->name;
            $representative->job_position = $request->job_position;

            $representative->save();
        }

        return redirect()->back()->with('sucess-message',trans('cms::backend.messages.success.update_contacts'));

    }

    public function upload_image(Request $request)
    {
        dd($request->allFiles());
        return $request->except('input');
    }

    public function saveAsset(Request $request)
    {
        dd($request->except('input'));
    }

    public function changeComposedVisibility($id)
    {
//        try {
//            $state = $this->userRepo->changeUserState($id);
//            $msg = [
//                'userId' => $id,
//                'state' => $state,
//                'successMsg' => trans('permissions::backend.messages.success.edit.change_user_state'),
//            ];
//        } catch (\Exception $e) {
//            $msg = [
//                'userId' => $id,
//                'errorMsg' => trans('permissions::backend.messages.error.edit.change_user_state'),
//            ];
//        }

        $composed = CHighlight::findOrFail($id);

        if($composed->visible == 0){
            $composed->visible = 1;
        } else {
            $composed->visible = 0;
        }

        if($composed->save()){
            $msg = [
                'userId' => $id,
                'state' => $composed->visible,
                'successMsg' => trans('permissions::backend.messages.success.edit.change_user_state'),
            ];
        }else{
            $msg = [
                'userId' => $id,
                'errorMsg' => trans('permissions::backend.messages.error.edit.change_user_state'),
            ];
        }

        return json_encode($msg);
    }

    public function deleteMultipleComposeds(Request $request)
    {
        $c_highlights = $request->input('deleted_rows');
        $all_c_highlights = explode(",", $c_highlights);

        foreach ($all_c_highlights as $c_high) {
            $c = CHighlight::findOrFail($c_high);
            CHighlight::destroy($c->id);
            CHighlight::where('order', '>', $c->order)->decrement('order');
        }

        return redirect()->back()->with('sucess-message',trans('cms::backend.messages.success.deleted_group'));
    }

    public function deleteMultipleResources(Request $request)
    {
        $resources = $request->input('deleted_rows');
        $all_resources = explode(",", $resources);

        foreach ($all_resources as $resource) {
          $image = DB::select('select * from location_product_resource where id = ?', array($resource));
          DB::table('location_product_resource')->where('id', $resource)->delete();
          DB::table('location_product_resource')->where('location_id', $image[0]->location_id)->where('order', '>', $image[0]->order)->decrement('order');
          // $image = DB::table('location_product_resource')->where('id', $resource)->first();
          // dd($image);
          //   $image->delete();
          //   DB::table('location_product_resource')->where('location_id', $image->location_id)->where('order', '>', $image->order)->decrement('order');
        }

        return redirect()->back()->with('sucess-message',trans('cms::backend.messages.success.deleted_group'));
    }

    public function deleteMultipleRepresentatives(Request $request)
    {
        $representatives = $request->input('deleted_rows');
        $all_representatives = explode(",", $representatives);

        foreach ($all_representatives as $rep) {
            $r = Representative::findOrFail($rep);
            Representative::where('position','>',$r->position)->decrement('position');
            Representative::destroy($r->id);
        }

        return redirect()->back()->with('sucess-message',trans('cms::backend.messages.success.deleted_representatives'));
    }

}
