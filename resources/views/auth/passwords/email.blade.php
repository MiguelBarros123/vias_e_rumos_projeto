@extends('layouts.app')

<!-- Main Content -->
@section('content')
<div class="container reset-form">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel panel-default">
                <div class="panel-titulo">
                    <span class="titulo">
                        <i class="icon-recuperar-password icon20"></i>
                        Alterar Password
                    </span>
                </div>
                <div class="panel-body login-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} login-input">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="email" class="col-md-12"><i class="icon-email icon10 icon-padding"></i>Email</label>
                                <div class="login-error">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="ex. brainy@tsf.pt">
                                    @if($errors->has('email'))
                                        <i class="icon-opcoes-eliminar"></i>
                                    @endif
                                </div>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="login-btn">
                                    <button type="submit" class="btn btn-default btn-yellow">
                                        Pedir Nova Password
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
