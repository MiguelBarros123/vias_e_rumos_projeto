<?php

return [
    'model' => [
        'index' => [
            //@lang('permissions::frontend.model.index.welcome')
            'welcome' => 'Bem-vindo(a)!'//'Welcome message on the model\'s index view frontend page',
        ],
        'edit' => [
            'recover-password' => [
                'title' => 'recuperar password',
                'confirm-question' => 'Tem a certeza que pretende recuperar a password do utilizador',
                'confirm-true' => 'recuperar',
                'confirm-false' => 'cancelar'
            ],
            'archive-user' => [
                'title' => 'arquivar utilizador',
                'confirm-question' => 'Tem a certeza que pretende arquivar o(s) utilizador(es)',
                'confirm-true' => 'arquivar',
                'confirm-false' => 'cancelar'
            ],
            'change-group' => [
                'title' => 'Mudar de Grupo',
            ],
            'save' => 'guardar',
            'cancel' => 'cancelar',
        ],
    ],
    'messages' => [
        //@lang('permissions::frontend.messages.notifications.new-task')
        'notifications' => [
            'new-task' => 'Existem novas tarefas em espera',
        ],
    ],
];
