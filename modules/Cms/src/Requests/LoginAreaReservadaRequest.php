<?php

namespace Brainy\Cms\Requests;

use App\Http\Requests\Request;

class LoginAreaReservadaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:10|max:255|unique:users,email|unique:profile_reserved_areas,email',
            'email_confirmation' => 'required|email|max:255|same:email',
            'password' => 'required|min:6',
            'password_confirmation' => 'required|min:6|same:password',
            'firstname' => 'required',
            'lastname' => 'required',
            'nif' => 'required',
            'cartao_cidadao' => 'required',
            'data_validade' => 'required',
            'telefone' => 'required',
            'nome_fiscal' => 'required',
            'nipc' => 'required',
            'country' => 'required',
            'address' => 'required',
            'cod_postal' => 'required',
            'local' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Este campo é obrigatório',
            'email.email' => 'Este campo tem que ser um e-mail válido',
            'email.max' => 'Este campo excede o limite máximo de caracteres',
            'email.unique' => 'Email tem de ser único',
            'email_confirmation.required' => 'Este campo é obrigatório',
            'email_confirmation.email' => 'Este campo tem de ser um e-mail válido',
            'email_confirmation.max' => 'Este campo passou o limite máximo de caracteres',
            'email_confirmation.same' => 'Emails não são iguais',
            'password.required' => 'Este campo é obrigatório',
            'password.min' => 'Este campo não tem o numero mínimo de caracteres',
            'password_confirmation.required' => 'Este campo é obrigatório',
            'password_confirmation.min' => 'Este campo não tem o numero mínimo de caracteres',
            'password_confirmation.same' => 'Passwords tem de ser iguais',
            'firstname.required' => 'Este campo é obrigatório',
            'lastname.required' => 'Este campo é obrigatório',
            'nif.required' => 'Este campo é obrigatório',
            'cartao_cidadao.required' => 'Este campo é obrigatório',
            'data_validade.required' => 'Este campo é obrigatório',
            'telefone.required' => 'Este campo é obrigatório',
            'nome_fiscal.required' => 'Este campo é obrigatório',
            'nipc.required' => 'Este campo é obrigatório',
            'country.required' => 'Este campo é obrigatório',
            'address.required' => 'Este campo é obrigatório',
            'cod_postal.required' => 'Este campo é obrigatório',
            'local.required' => 'Este campo é obrigatório',
        ];
    }


}
