@extends('layouts.backend.app')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/profiles/backend.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/backend.css') }}">
@endsection

@section('module-scripts')



@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <a class="pull-right icon_eliminar icon-editar pading_right icon_delete_inside_view_show"
                       href="{{ route('profiles::backend::profiles.group.edit', $group->id) }}">
                        Editar
                    </a>
                    <ul class="list-inline">
                        <li><a href="{{route('profiles::backend::profiles.group.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.groups.show',$group) !!}
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo"><i class="icon-ficha icon10"></i>@lang('profiles::backend.common.group_sheet')</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <div class="col-lg-12 margem_20">

                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="name">@lang('profiles::backend.common.name')</label>
                                                <p class="form-control-static">{{ $group->name }}</p>
                                            </div>
                                        </div>

                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label for="name">@lang('profiles::backend.common.description')</label>
                                                <p class="form-control-static">{!! $group->description !!}</p>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo"><i class="icon-links icon10"></i>@lang('profiles::backend.common.profiles')</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <table class="table dataTable default-table2">
                                    <thead>
                                    <tr>
                                        <th>@lang('profiles::backend.menu.clients')</th>
                                        <th>@lang('profiles::backend.common.contacts')</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($group->profiles->count())
                                        @foreach($group->profiles as $user)
                                            @php
                                            $email = $user->profile_contacts()->where('type', 'personal_email')->first();
                                            $phone = $user->profile_contacts()->where('type', 'personal_phone')->first();
                                            @endphp
                                            <tr>
                                                <td>
                                                    <div class="company_user_img">
                                                        @if($user->thumbnail === '')
                                                            <div class="pic-circle-corner2 text-center profile_no_pic">
                                                                <i class="icon-avatar-person icon10 "></i>
                                                            </div>
                                                        @else
                                                            <img class="image_user_picked2" src="{{route('gallery::frontend::storage_image_p',[$user->thumbnail,50,50])}}">
                                                        @endif
                                                    </div>
                                                    <div class="company_user_info">
                                                        <div>{{ $user->name }}</div>
                                                        <div>{{ $user->job }}</div>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if($email)
                                                        <i class="icon-mail"></i> {{ $email->contact }}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($phone)
                                                        <i class="icon-telefone"></i> {{ $phone->contact }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="4">
                                                @lang('definitionsecommerce::backend.common.no_results')
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>

@endsection



