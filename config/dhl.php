<?php
/**
 * Default Sample Configuration file
 *
 *
 * @project DHL
 */
return array(
    // AutoloadManager options
    'autoloader' => array(

        // Only scan once when a class is not found in the class map (this should be set to SCAN_NONE on production environment
        //'scanOptions' => autoloadManager::SCAN_ONCE,

        // complete path to autoload file that contains the class map 
        'dir' => sys_get_temp_dir() . '/dhl-api-autoload.php',
    ),

    // DHL related settings    
    'dhl' => array(

        'id' => 'ASKPURELDA',
        'pass' => '1nm2P47AsW',

        // Shipper, Billing and Duty Account numbers
        'shipperAccountNumber' => '308002900',
        'billingAccountNumber' => '308002900',
        'dutyAccountNumber' => '308002900',
    ),
);
