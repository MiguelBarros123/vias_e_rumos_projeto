<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:28
 */

namespace Brainy\Cms\Models;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Composed extends Model {

	//use Translatable;

    public $fillable = [
    	'image',
    	'panel_name',
    	'link'
    ];

    public $translatedAttributes = [
    	'title',
    	'description',
        'subtitle'
    ];

    public $translationModel = ComposedHighlightTranslation::class;

    public function c_hilights()
    {
        return $this->hasMany(CHighlight::class);
    }

    public function page()
    {
        return $this->belongsTo(Page::class);
    }
}