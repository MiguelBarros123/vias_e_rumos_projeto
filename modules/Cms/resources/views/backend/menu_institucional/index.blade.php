@extends('layouts.backend.app')

@section('title', 'Registo Leiloeira')

@section('module-styles')
@endsection

@section('module-scripts')
<script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace( 'description');
</script>
@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
    <div class="container-fluid">
        <div class="row tabs-back">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                <ul class="list-inline">
                    <li><a href="{{route('cms::backend::backend.cms.menu_institucional.index')}}"
                       class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                   </ul>
               </div>
           </div>
           @include('layouts.backend.messages')
           <div class="row">
            @if($menu_institucional == null)
            <form method="POST" action="{{ route('cms::backend::backend.cms.menu_institucional.store')}}" autocomplete="off">
                {{ csrf_token() }}
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20">
                                </i>
                                Menu Institucional
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">

                                    <div class="row">

                                        <div class="col-lg-12">

                                            <div class="form-group @if($errors->has('description')) has-error @endif">
                                                <label for="description-field">@lang('rent::backend.common.description')</label>
                                                <textarea class="form-control" id="description-field" rows="3"
                                                name="description">{{ old("description") }}</textarea>
                                                @if($errors->has("description"))
                                                <span class="help-block">{{ $errors->first("description") }}</span>
                                                @endif
                                            </div>

                                        </div>

                                    </div>   

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-lg-12">

                    <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('rent::backend.common.save')</button>

                    <a class="btn btn-default btn-cancelar" href="{{route('cms::backend::backend.cms.menu_institucional.index')}}">@lang('rent::backend.common.cancel')</a>

                </div>

            </form>
            @else
            <form method="POST" action="{{ route('cms::backend::backend.cms.menu_institucional.update', $menu_institucional->id)}}" autocomplete="off">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                {{ method_field('PUT')}}
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20">
                                </i>
                                Menu Institucional
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group @if($errors->has('description')) has-error @endif">
                                                <label for="description-field">@lang('rent::backend.common.description')</label>
                                                <textarea class="form-control" id="description-field" rows="3"
                                                name="description">{{ $menu_institucional->description }}</textarea>
                                                @if($errors->has("description"))
                                                <span class="help-block">{{ $errors->first("description") }}</span>
                                                @endif
                                            </div>

                                        </div>

                                    </div>   

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

                <div class="col-lg-12">

                    <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('rent::backend.common.save')</button>

                    <a class="btn btn-default btn-cancelar" href="{{route('cms::backend::backend.cms.menu_institucional.index')}}">@lang('rent::backend.common.cancel')</a>

                </div>

            </form>
            @endif
        </div>
    </div>
</div>

@endsection
