/**
 * Created by User on 09-03-2017.
 */


$(document).ready(function () {

    $('.option_subscription_date').change(function () {

        $(this).parent().next().find('.range_date').val('');

        //se o valor for entre
        if($(this).val()=='7'){

            $(this).parent().next().find('.range_date').daterangepicker({
                "timePicker": true,
                "locale": {
                    "format": 'DD/MM/YYYY'
                },
            });

        }else{
            $(this).parent().next().find('.range_date').daterangepicker({
                singleDatePicker: true,
                "locale": {
                    "format": 'DD/MM/YYYY'
                }
            });
        }
    });

    $('.range_date').daterangepicker({
        singleDatePicker: true,
        "locale": {
            "format": 'DD/MM/YYYY'
        },
    }, function(start, end, label) {



    });
});


//# sourceMappingURL=subscription_date_edit.js.map
