<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-10-2016
 * Time: 17:31
 */

namespace Brainy\Profiles\Models;
use Brainy\DefinitionsEcommerce\Models\ComercialCondition;
use Brainy\DefinitionsRent\Models\Condition;
use Illuminate\Database\Eloquent\Model;

class ProfileGroup extends Model{

    protected $fillable=['name', 'description'];

    public function profiles(){
        return $this->hasMany(Profile::class,'profile_group_id','id');
    }

    public function comercial_condition()
    {
        return $this->belongsToMany(ComercialCondition::class, 'comercial_groups', 'group_id', 'comercial_id');
    }

    public function condition()
    {
        return $this->belongsToMany(Condition::class, 'condition_groups', 'group_id', 'condition_id');
    }

    public function reward()
    {
        return $this->belongsToMany(Reward::class, 'reward_groups', 'group_id', 'reward_id');
    }

}