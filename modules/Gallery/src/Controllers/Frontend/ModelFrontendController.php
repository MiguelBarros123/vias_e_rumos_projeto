<?php

namespace Brainy\Gallery\Controllers\Frontend;

use Brainy\Framework\Controllers\FrontendController;
use Brainy\Gallery\Models\MediaItem;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Intervention\Image\ImageCache;
use Illuminate\Http\Response as IlluminateResponse;

class ModelFrontendController extends FrontendController
{
    public function index()
    {
        return view('gallery::frontend.model.index');
    }

    public function getImageThumbnail($id,$with=null,$height=null)
    {

        $file = Storage::disk('galeria')->get($id);
        $caminho=Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$id;
        $type = Storage::disk('galeria')->mimeType($id);

        if($type === 'application/pdf'){
            return Response::make(file_get_contents($caminho), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$id.'"'
            ]);
        }
        else if(strcmp($type,".svg"))
        {

        }
        else
        {
            if($with!=null && $height!=null){

                $img = Image::cache(function($image) use($caminho, $with, $height){
                    return $image->make($caminho)->resize($with, $height);
                }, 43200);

                return $this->buildResponse($img);

            }
        }

        return response($file,200,['Content-Type'=>$type]);
    }

    public function getImageParams($id,$with,$height)
    {
        $caminho=Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$id;

        $img = Image::cache(function($image) use($caminho, $with, $height){
            return $image->make($caminho)->fit($with, $height);
        }, 43200);

        return $this->buildResponse($img);

    }

    public function buildResponse($img) {
        // define mime type
        $mime = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $img);

        // return http response
        return new IlluminateResponse($img, 200, array(
            'Content-Type' => $mime,
            'Cache-Control' => 'max-age='.(config('imagecache.lifetime')*60).', public',
            'Etag' => md5($img)
        ));
    }

}
