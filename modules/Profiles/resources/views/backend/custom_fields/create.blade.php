@extends('layouts.backend.app')
@inject('folders', 'Brainy\Gallery\Models\MediaFolder')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" href="{{asset('css/profiles/backend.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{asset('back/js/onload.js')}}"></script>
    <script>

        $('#toggle-type').bootstrapToggle();

        $('#toggle-type').change(function () {

            console.log($(this).is(':checked'));
            if($(this).is(':checked')){
                $('.section_of_multiples').show();
                $('.section_of_singles').hide();
            }else{
                $('.section_of_singles').show();
                $('.section_of_multiples').hide();
            }
        });

        var sugest=$('#magicsuggest-fields').magicSuggest({
            placeholder: "",
            allowDuplicates: false,
            valueField: 'name'
        });
    </script>

@endsection
@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">

                        <li><a href="{{route('profiles::backend::backend.profiles.custom_fields.index')}}"
                               class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.custom_fields.create') !!}
            <div class="row">
                <form method="POST" action="{{ route('profiles::backend::backend.profiles.custom_fields.store') }}"
                      autocomplete="off">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">


                    <div class="col-lg-12">
                        <div class="panel ">
                            <div class="panel-titulo">
                                <span class="titulo">@lang('profiles::backend.menu.personalized_fields')</span>
                            </div>
                            <div class="panel-body">

                                <div class="row">
                                    <div class="col-lg-12 margem_20">

                                        <div class="form-group @if($errors->has('name')) has-error @endif">
                                            <label for="name-field">@lang('profiles::backend.common.name')</label>
                                            <input type="text" id="name-field" name="name" class="form-control"
                                                   value="{{ old("name") }}"/>
                                            @if($errors->has("name"))
                                                <span class="help-block">{{ $errors->first("name") }}</span>
                                            @endif
                                        </div>

                                        <div class="form-group">
                                            <label for="name-field">@lang('profiles::backend.common.type')</label>
                                            <div>
                                                <span>@lang('profiles::backend.common.text')</span><input id="toggle-type" type="checkbox" data-on=" " data-off=" " name="subscript" data-style="ios new_toggle" ><span>@lang('profiles::backend.common.multiple')</span>

                                            </div>

                                        </div>




                                       <div class="section_of_multiples hide_muliple">
                                           @include('layouts.backend.info_message',['info_message'=>trans('profiles::backend.common.fields_message')])
                                           <div class="form-group margin_7_top">
                                               <label for="tags">@lang('profiles::backend.common.fields')</label>
                                               <input class="form-control" id="magicsuggest-fields" name="fields">
                                           </div>
                                       </div>

                                        @if($errors->has("fields"))
                                            <span class="help-block">{{ $errors->first("fields") }}</span>
                                        @endif


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="col-lg-12">
                        <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('profiles::backend.common.save')</button>
                        <button type="submit" name="submit" value="stay" class="btn btn-default btn-yellow btn-stay btn-submit-all">@lang('profiles::backend.common.save_and_stay')</button>
                        <a class="btn btn-default btn-cancelar" href="{{route('profiles::backend::backend.profiles.custom_fields.index')}}">@lang('profiles::backend.common.cancel')</a>
                    </div>

                </form>
            </div>
        </div>

    </div>



@endsection

