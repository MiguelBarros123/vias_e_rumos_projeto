<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:25
 */

namespace Brainy\Cms\Models;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Text extends Model {

    use Translatable;

    public $fillable = ['panel_name'];

    public $translatedAttributes = ['title', 'subtitle', 'description','hasPResources'];

    public $translationModel = TextTranslation::class;
}
