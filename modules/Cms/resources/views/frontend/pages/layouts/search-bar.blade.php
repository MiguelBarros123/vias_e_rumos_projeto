<div id="search-bar" class="hidden-xs hidden-sm">
	<div class="search-list-wrap">
		<ul class="search-list">
			@if(count($sub_type_of_goods))
				@foreach($sub_type_of_goods as $sub_type)
					<li>
						<a href="{{route('cms::frontend::model.filter_by_sub_types', [$sub_type->id, $sub_type->type])}}">{{$sub_type->name}}</a>
					</li>
				@endforeach
			@endif
		</ul>
	</div>
	<div class="search-list-wrap bot-list">
		<ul class="search-list">
			<li class="search-input">
				<input placeholder="PESQUISA" name="search"/>

			</li>
			<li><span class="search-button"><span>Pesquisar</span><span class="top-b-search-icon"><img src="{{ asset('front/imgs/icons/search.svg') }}"/></span></span></li>
			<li><a href="{{ route('cms::frontend::model.avancada')}}">Avançada</a></li>
			<!-- <li><a href="mapa.php">ver no mapa</a></li> -->
		</ul>
	</div>
</div>
