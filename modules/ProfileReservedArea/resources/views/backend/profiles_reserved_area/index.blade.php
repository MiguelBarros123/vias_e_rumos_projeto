@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/rent/backend.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
@endsection

@section('module-scripts')
    <!-- <script src="{{asset('js/rent/backend.js')}}"></script> -->
    <script src="{{asset('js/cms/backend.js')}}"></script>
    <script src="{{asset('back/js/topmenuselects.js')}}"></script>

    <!---->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>

    <script src="{{ asset('back/js/jstree/dist/jstree.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/directives/ng-js-tree/ngJsTree.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('angular/modules/brainy.module.js') }}" type="text/javascript"></script>
    <script src="{{asset('js/cms/backend.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/rowreorder/1.1.0/js/dataTables.rowReorder.min.js"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
    <script src="{{asset('back/js/spectrum.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ asset('angular/directives/toggler/toggler.directive.js') }}"></script>

    <script>
        $(document).ready(function () {

            var options = {
                rota: "{{ route('profilereservedarea::backend::profilereservedarea.index_ajax')}}",
                rowReorder: {
                    selector: 'i.icon-icon-peq-img-size'
                },
                colunas: [
                    // {data: 'order', name: 'order'},
                    {data: 'email', name:'email'},
                    {data: 'name', name:'name'},
                    {data: 'aproved', name:'aproved'},
                    {data: 'date', name:'date'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    initComplete: function (settings, json) {
                        $(function () {
                            $.logicDatatable(options);

                        });
                    },
                     fnCreatedRow: function (nRow, aData, iDataIndex) {
                        $(nRow).attr('data-rota-see', aData['rota_final']);
                        var tr2 = $(nRow).closest('tr');
                        tr2.find("td:first").addClass('details-control');
                        tr2.find("td:not(:first, :last, :nth-last-child(2))").addClass('clickable_url');
                    },
                    'fnDrawCallback': function () {

                      $('*[id^="toggle-one"]').bootstrapToggle();

                        $('*[id^="toggle-one"]').on('change', function(){
                            line = this.getAttribute('data-row');
                                var url = '{{ route("profilereservedarea::backend::profilereservedarea.change_profile_state", ":id") }}';
                                url = url.replace(':id', line);
                                $.ajax({
                                    url: url,
                                    type: 'post',
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                    },
                                    success:function(data)
                                    {
                                      state = !$('#toggle-one'+line).parent().hasClass('off');
                                      $('#info-hidden-composed-' + line).css('visibility', state == '1' ? 'hidden' : 'visible');
                                      // $row = list_tables[$id].datatable.row($('#info-hidden-composed-' + line).parent()).index();
                                    }
                                });

                        });

                    }

                },
                searchableElementId: 'search'
            };


            $('.default-table').mydatatable(options);

            // $('.default-table').on('click', 'td.clickable_url', function () {
            //     var tr = $(this).closest('tr');
            //     window.location.href = tr.attr('data-rota-see');

            // });

            // options.datatable.on('row-reorder', function (e, diff, edit) {
            //     var elem = options.datatable.row(edit.triggerRow.selector.rows).data();
            //     var chosen = diff.find(function(element) { return options.datatable.row(element.node).data() === elem });
            //     var oldPosition = elem.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');

            //     for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
            //         if(diff[i].oldPosition === chosen.newPosition){
            //             var rowData = options.datatable.row( diff[i].node ).data();
            //             var newPosition = rowData.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');
            //             $.ajax({
            //                 type: 'get',
            //                 url: "{{route('profilereservedarea::backend::profilereservedarea.reorder')}}",
            //                 headers: {
            //                     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //                 },
            //                 data: {
            //                     id: elem.id,
            //                     fromPosition: oldPosition,
            //                     toPosition: newPosition
            //                 },
            //                 success: function (data) {
            //                     options.datatable.ajax.reload(null, false);
            //                 }
            //             });
            //         }
            //     }
            // });
        });
    </script>

@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 ">

                    <a class="pull-right icon_eliminar icon-adicionar pading_right icon_delete_inside_view" href="{{ route('profilereservedarea::backend::profilereservedarea.create')}}"
                       >Criar Perfil</a>
                        @include('profilereservedarea::backend.layouts.top_menu')
                   


                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="search" class="search search_icon" type="text"
                                   placeholder="@lang('profiles::backend.common.search')">
                        </div>
                    </div>
                    @include('layouts.backend.messages')
                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('profiles.index') !!}
                    <!-- {!! DaveJamesMiller\Breadcrumbs\Facade::render('rent.auctions.index') !!} -->


                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                           <table id="unpaid_table" class="table default-table table-hover">
                                <thead>
                                <tr>
                                    <!-- <th></th> -->
                                    <th>@lang('profilereservedarea::backend.common.email')</th>
                                    <th>@lang('profilereservedarea::backend.common.name')</th>
                                    <th>@lang('profilereservedarea::backend.common.state')</th>
                                    <th>@lang('profilereservedarea::backend.common.date')</th>
                                    <th class="multiple-options">
                                        @include('layouts.backend.partials.delete_datatables',['id'=>'unpaid_table','route'=>'profilereservedarea::backend::profilereservedarea.delete'])
                                    </th>
                                    <th>
                                        <label class="checkbox-default">
                                            <input type='checkbox' id="sel_all_perm_unpaid_table">
                                            <span></span>
                                        </label>
                                    </th> 
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    @include('layouts.backend.partials.delete_form',['delete_message'=>'cms::backend.common.sure_delete'])
@endsection
