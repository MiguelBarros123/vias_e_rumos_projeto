<?php

use Illuminate\Database\Seeder;

use Laracasts\TestDummy\Factory as TestDummy;

class TypeRentItemTableSeeder extends Seeder {

    public function run()
    {

        $coins = \Brainy\Rent\Models\GlobalCoin::all();

        $iban = \Brainy\Reservations\Models\Iban::create([
            'iban' => 'PT50003520460000629603032'
        ]);

        $rentDefinitions = \Brainy\DefinitionsRent\Models\RentDefinition::create([
            'type_value' => 'value',
            'min_days' => 7
        ]);

        for($x = 0; $x < $coins->count(); $x++)
        {
            $pricesArray = [
                'price' => 0,
                'price_w_tax' => 0,
                'coin_id' => $coins[$x]->id
            ];
            $p_table = new \Brainy\Rent\Models\GlobalPriceTable($pricesArray);
            $rentDefinitions->global_price_tables()->save($p_table);
        }

        $rentDefinitions->save();
        
        /*TIPOLOGY 1*/
        $type_rent_item = \Brainy\Rent\Models\TypeRentItem::create([
            'reference' => 1,
            'category_id' => 1,
            'order' => 1,
            'pt' => ['name' => 'Tipologia 1'],
            'en' => ['name' => 'Tipologia 1'],
            'fr' => ['name' => 'Tipologia 1']
        ]);

        $segment_1 = \Brainy\Rent\Models\Essence::findOrFail(1);

        $type_rent_item->essences()->attach($segment_1);

        $rent_item = $type_rent_item->rent_items()->create([
            'reference' => 1,
            'adapted_unit' => 0,
            'pt' => ['name' => '1'],
            'en' => ['name' => '1'],
            'fr' => ['name' => '1']
        ]);

        $caracteristic_1 = \Brainy\Rent\Models\Caracteristic::findOrFail(1);
        $caracteristic_2 = \Brainy\Rent\Models\Caracteristic::findOrFail(2);
        $caracteristic_3 = \Brainy\Rent\Models\Caracteristic::findOrFail(3);
        $caracteristic_4 = \Brainy\Rent\Models\Caracteristic::findOrFail(4);
        $caracteristic_5 = \Brainy\Rent\Models\Caracteristic::findOrFail(5);

        $type_rent_item->caracteristics()->attach($caracteristic_1->id, ['visibility' => true, 'value' => 5]);
        $type_rent_item->caracteristics()->attach($caracteristic_2->id, ['visibility' => true, 'value' => 5]);
        $type_rent_item->caracteristics()->attach($caracteristic_3->id, ['visibility' => true, 'value' => 5]);
        $type_rent_item->caracteristics()->attach($caracteristic_4->id, ['visibility' => true, 'caracteristic_option_id' => 1]);
        $type_rent_item->caracteristics()->attach($caracteristic_5->id, ['visibility' => true, 'caracteristic_option_id' => 2]);

        $rent_item->extras()->attach(1, ['extra_type_id' => 1]);

        $resource = new \Brainy\Rent\Models\RentItemResource(['identifier_token' => '5980561dd0fd97.74474297.jpeg', 'order'=>1]);
        $rent_item->rent_item_resources()->save($resource);
        $resource->save();
        $rent_item->save();

        $l = \Brainy\Rent\Models\RentLocation::findOrFail(1);
        $resource->rent_locations()->save($l,['order' => 1]);


        $r = $type_rent_item->type_rent_item_rules()->create(['min_days' => 0, 'max_days' => 7]);
        $r->save();


        $p_table = new \Brainy\Rent\Models\GlobalPriceTable();
        $p_table->coin_id = 1;
        $p_table->price = 10;
        $p_table->price_w_tax = 12.3;

        $prices[] = 12.3;

        $p_table->save();

        $r->global_price_tables()->save($p_table);

        $r->save();

        $type_rent_item->save();

        $lowest_price = min($prices);

        $job = new \App\Jobs\TypologiesBaseCachePrices($type_rent_item, $lowest_price);

        /*TIPOLOGY 1*/

        /*TIPOLOGY 2*/
        $type_rent_item = \Brainy\Rent\Models\TypeRentItem::create([
            'reference' => 2,
            'category_id' => 1,
            'order' => 2,
            'pt' => ['name' => 'Tipologia 2'],
            'en' => ['name' => 'Tipologia 2'],
            'fr' => ['name' => 'Tipologia 2']
        ]);

        $segment_1 = \Brainy\Rent\Models\Essence::findOrFail(1);

        $type_rent_item->essences()->attach($segment_1);

        $rent_item = $type_rent_item->rent_items()->create([
            'reference' => 2,
            'adapted_unit' => 0,
            'pt' => ['name' => '2'],
            'en' => ['name' => '2'],
            'fr' => ['name' => '2']
        ]);

        $caracteristic_1 = \Brainy\Rent\Models\Caracteristic::findOrFail(1);
        $caracteristic_2 = \Brainy\Rent\Models\Caracteristic::findOrFail(2);
        $caracteristic_3 = \Brainy\Rent\Models\Caracteristic::findOrFail(3);
        $caracteristic_4 = \Brainy\Rent\Models\Caracteristic::findOrFail(4);

        $type_rent_item->caracteristics()->attach($caracteristic_1->id, ['visibility' => true, 'value' => 5]);
        $type_rent_item->caracteristics()->attach($caracteristic_2->id, ['visibility' => true, 'value' => 5]);
        $type_rent_item->caracteristics()->attach($caracteristic_3->id, ['visibility' => true, 'caracteristic_option_id' => 2]);
        $type_rent_item->caracteristics()->attach($caracteristic_4->id, ['visibility' => true, 'caracteristic_option_id' => 1]);

        $rent_item->extras()->attach(1, ['extra_type_id' => 1]);

        $resource = new \Brainy\Rent\Models\RentItemResource(['identifier_token' => '5980561dd0fd97.74474297.jpeg', 'order'=>1]);
        $rent_item->rent_item_resources()->save($resource);
        $resource->save();
        $rent_item->save();

        $l = \Brainy\Rent\Models\RentLocation::findOrFail(1);
        $resource->rent_locations()->save($l,['order' => 1]);

        $r = $type_rent_item->type_rent_item_rules()->create(['min_days' => 0, 'max_days' => 7]);
        $r->save();


        $p_table = new \Brainy\Rent\Models\GlobalPriceTable();
        $p_table->coin_id = 1;
        $p_table->price = 10;
        $p_table->price_w_tax = 12.3;

        $prices[] = 12.3;

        $p_table->save();

        $r->global_price_tables()->save($p_table);

        $r->save();

        $type_rent_item->save();

        $lowest_price = min($prices);

        $job = new \App\Jobs\TypologiesBaseCachePrices($type_rent_item, $lowest_price);

        /*TIPOLOGY 2*/

    }
}