/**
 * Created by User on 09-03-2017.
 */

//-----------------------------------------------------------setting the names for atributes
var id=0;
var ultimo= $('.item_for_condition[data-id=""]');
ultimo.attr('data-id',id);

if($('.item_for_condition').length > 1){
    var ordenados=$('.item_for_condition').sort(function (a, b) {

        var contentA =parseInt( $(a).attr('data-id'));
        var contentB =parseInt( $(b).attr('data-id'));
        return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
    });
    var prev=ordenados.last();

    id=parseInt(prev.attr('data-id'))+1;
    ultimo.attr('data-id',id);
}

var block_id=ultimo.parent().parent().attr('id');

ultimo.find('.name_for_first_option').attr('name','item['+block_id+'][email]['+id+'][option][]');
ultimo.find('.magicsuggest-emails').attr('name','item['+block_id+'][email]['+id+'][term][]');
ultimo.find('.filter_field_id').attr('name','item['+block_id+'][email]['+id+'][filter_field_id][]');

//----------------------------------------------------------end setting the names for atributes

$('.magicsuggest-emails').magicSuggest({
    placeholder:"",
    method: 'get',
    data: rota,
    allowDuplicates: false,
    valueField: 'name',
    allowFreeEntries:false,
    required: true,
});
//# sourceMappingURL=emails.js.map
