@extends('layouts.frontend.app')

@inject('page', 'Brainy\Cms\Models\Page')

@php
$seo = $page->find(1);
@endphp

@section('title', $seo->title)
@section('description', $seo->meta_description)

@section('image', asset('front/imgs/lotes/placeholder.jpg'))


@inject('inject', 'Brainy\Cms\Models\ServicePages')

@php
$content = $inject->getPageContent(1)
@endphp

<!--
divisao_ficheiro-->

@section('content')
<?php $slider=$content['composeds']->where('panel_name','cms::backend.model.pages.index.home_slider')->first(); ?>
<div id="hero" class="cover">
    <div class="hero-slider">
        @if(!count($slider->c_hilights()->where('visible', 1)->get()))
            <div class="hero-slide" style="background-image: url('{{ asset('front/imgs/hero.jpg') }}')">
            </div>
            <div class="hero-slide" style="background-image: url('{{ asset('front/imgs/hero.jpg') }}')">
            </div>
        @else
            @foreach($slider->c_hilights()->where('visible', 1)->get() as $slide)
                @if($slide->photo)
                    <div class="hero-slide" style="background-image: url({{ route('gallery::frontend::storage_image', $slide->photo) }})">
                    </div>
                @endif
            @endforeach        
        @endif    
    </div>
</div>
@include('cms::frontend.pages.layouts.search-bar')


<div class="row index-body">
	<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
		<div class="title-label">
			Calendário de Eventos
		</div>
		<div id="calendar">
		</div>
		<div class="title-label">
			Próximos Eventos
		</div>
		<div class="next-events">	
		</div>
		<div class="next-events-options">
			<div class="go-prev disabled_arr">
				<img src="{{ asset('front/imgs/icons/seta-cima.svg') }}"/>
			</div>
			<div class="go-next">
				<img src="{{ asset('front/imgs/icons/seta-baixo.svg') }}"/>
			</div>
		</div>
	</div>
	<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
		<div class="home-prods">
            @foreach($auctions as $auction)
			<div class="home-prod">
				<div class="row">
					<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
						<a href="{{ route('cms::frontend::model.leilao', $auction->id)}}">
                            @if($auction->auctions_resources()->first())
							<div class="cover prod-img" style="background-image: url('{{ route('gallery::frontend::storage_image_p',[$auction->auctions_resources()->first()->identifier_token,295,200])  }}');">
								<div class="more-prod-bg">

								</div>
								<div class="more-prod-text">
										Saber mais
								</div>
							</div>
                            @endif
						</a>
					</div>
					<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 height-200">
						<div class="top-prod">
							<div class="top-title-share">
								<div class="prod-title">
									<h3>{{ Brainy\DefinitionsRent\Models\Category::where('id', $auction->category_id)->first()->name}}</h3>
								</div>
								<div class="prod-share hidden-sm hidden-xs">
									<div class="share-word">
										Partilha
									</div>
									<div class="share-icons">
										<ul class="icons-list">
                                            
                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::model.leilao', $auction->id)}}" target="_blank"><span class="icon-facebook"></span></a></li>
                                            <li><a href="https://plus.google.com/share?url={{ route('cms::frontend::model.leilao', $auction->id)}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span class="icon-google-plus"></span></a></li>
                                            <li><a href="http://twitter.com/share?text={{$auction->name}}&url={{ route('cms::frontend::model.leilao', $auction->id)}}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a></li>
                                            <li><a href="mailto:?subject={{$auction->title}}&body={{$auction->description}}Link: {{ route('cms::frontend::model.leilao', $auction->id)}} "><span class="icon-mail"></span></a></li>


											
										</ul>
									</div>
								</div>
							</div>
							<div class="top-name">
								<h4>{{$auction->title}}</h4>
								<h4 class="l300">{{$auction->local}}</h4>
							</div>
						</div>
						<div class="bot-prod">
                            @if($auction->category_id == '1')
                            <div class="prod-line">
                                <div class="half">

                                    <div class="info-title">
                                        Receção
                                    </div>
                                    <div class="info-value">
                                        {{$auction->date_limit}}
                                    </div>
                                </div>
                                <div class="half">
                                    <div class="info-title">
                                        Referência
                                    </div>
                                    <div class="info-value">
                                        {{$auction->ref_intern}}
                                    </div>
                                </div>
                            </div>
                            <div class="prod-line mar-top-15">
                                <div class="half">
                                    <div class="info-title">
                                        Nº Lotes
                                    </div>
                                    <div class="info-value">
                                        {{count($auction->lots())}}
                                    </div>
                                </div>
                            </div>    
                            @elseif($auction->category_id == '2')
                            <div class="prod-line">
                                <div class="half">

                                    <div class="info-title">
                                        Receção
                                    </div>
                                    <div class="info-value">
                                        {{$auction->date_limit}}
                                    </div>
                                </div>
                                <div class="half">
                                    <div class="info-title">
                                        Referência
                                    </div>
                                    <div class="info-value">
                                        {{$auction->ref_intern}}
                                    </div>
                                </div>
                            </div>
                            <div class="prod-line mar-top-15">
                                <div class="half">
                                    <div class="info-title">
                                        Abertura
                                    </div>
                                    <div class="info-value">
                                        {{$auction->date}}
                                    </div>
                                </div>
                                <div class="half">
                                    <div class="info-title">
                                        Nº Lotes
                                    </div>
                                    <div class="info-value">
                                        {{count($auction->lots())}}
                                    </div>
                                </div>
                            </div>

                            @elseif($auction->category_id == '3')
                                <div class="prod-line">
                                <div class="half">
                                    <div class="info-title">
                                        Referência
                                    </div>
                                    <div class="info-value">
                                        {{$auction->ref_intern}}
                                    </div>
                                </div>
                            </div>
                            <div class="prod-line mar-top-15">
                                <div class="half">
                                    <div class="info-title">
                                        Nº Lotes
                                    </div>
                                    <div class="info-value">
                                        {{count($auction->lots())}}
                                    </div>
                                </div>
                            </div>

                            @elseif($auction->category_id == '4')
                                    <div class="prod-line">
                                <div class="half">

                                    <div class="info-title">
                                        inicia
                                    </div>
                                    <div class="info-value">
                                        {{$auction->date}}
                                    </div>
                                </div>
                                <div class="half">
                                    <div class="info-title">
                                        Referência
                                    </div>
                                    <div class="info-value">
                                        {{$auction->ref_intern}}
                                    </div>
                                </div>
                            </div>
                            <div class="prod-line mar-top-15">
                                <div class="half">
                                    <div class="info-title">
                                        Termina
                                    </div>
                                    <div class="info-value">
                                        {{$auction->date}}
                                    </div>
                                </div>
                                <div class="half">
                                    <div class="info-title">
                                        Nº Lotes
                                    </div>
                                    <div class="info-value">
                                        {{count($auction->lots())}}
                                    </div>
                                </div>
                            </div>
                            @endif
							
						</div>
					</div>
				</div>
			</div>
            @endforeach
		</div>
	</div>
</div>

@endsection

@section('module-scripts')
<script>
	var calendar = $('#calendar');

$.fn.datepicker.dates['pt'] = {
        days: ["Domingo", "Segunda", "Terça", "Quarta", "Quinta", "Sexta", "Sábado"],
        daysShort: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
        daysMin: ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sab"],
        months: ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro"],
        monthsShort: ["Jan", "Fev", "Mar", "Abr", "Mai", "Jun", "Jul", "Ago", "Set", "Out", "Nov", "Dez"],
        today: "Hoje",
        clear: "Clear",
        format: "dd/mm/yyyy",
        titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
        weekStart: 0};



        $(document).ready(function(){

            // function checkIfValidBidding(previousBidding, bidding, increasedPrice)
            // {
            //     console.log(bidding > (previousBidding + increasedPrice) ? true : false); 
            // }

            // checkIfValidBidding(10.23, 30.22, 20);

            $('.hero-slider').slick({
                dots: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
            });

            var new_events_aux = "{{ $events }}";
            var new_events = JSON.parse(new_events_aux.replace(/&quot;/g,'"'));


            var separator = "<div class='separator'></div>";
            var last_event=0;
            var limite;
            var ended=false;
            var current_el = 0;
            var event_amount = 4;
            function showMoreEvents(){
                if(ended == false){
                    limite = last_event+event_amount;
                    if(limite>new_events.length){
                        limite = new_events.length;
                    }
                    for(i=last_event; i<limite; i++){
                        
                        var route = "{{route('cms::frontend::model.leilao',[':id'])}}";
                        route = route.replace(':id',new_events[i]['id']);
                        if(new_events[i]['date'] != 'Sem Data'){
                            var date = new Date(new_events[i]['date']);
                            var event_day = date.getDate(); 
                            var event_month = parseInt(date.getMonth()) + 1;
                            var div= "<a href='"+route+"' class='event-link'><div class='one-event'><div class='top-event'><div class='tipo-event'>"+new_events[i]['tipo']+"</div><div class='date-event'>"+event_day+"/"+event_month+"</div></div><div class='name-event'>"+new_events[i]['nome']+"</div><div class='hour-event'>"+new_events[i]['local']+" / "+new_events[i]['hora']+"</div></div></a>";
                        }else{
                            var date = 'Sem Data';
                            var event_day = 'Sem Data';
                            var event_month = '';
                            var div= "<a href='"+route+"' class='event-link'><div class='one-event'><div class='top-event'><div class='tipo-event'>"+new_events[i]['tipo']+"</div><div class='date-event'>"+event_day+"</div></div><div class='name-event'>"+new_events[i]['nome']+"</div><div class='hour-event'>"+new_events[i]['local']+"</div></div></a>";
                        }
                        
                        $('.next-events').append(div);

                    /*  if(i != new_events.length-1){
                        }*/
                            $('.next-events').append(separator);

                        if(i == new_events.length ){
                            ended = true;
                            return true;
                        }
                    
                    }

                    last_event =i;
                }
                
            }

            showMoreEvents();

            $('.next-events').height(Math.ceil($('.next-events').height())); 

            function scrollElDown(){

                var go_to = current_el+event_amount;
                
                if(go_to>new_events.length-event_amount){
                    go_to = new_events.length-event_amount;
                }
                
                $(".next-events").animate({
                  scrollTop: $('.next-events').scrollTop() + $('.event-link:eq('+go_to+')').position().top
                }, 500);
                current_el = go_to;
                
                if(current_el==0){
                    $('.go-prev').addClass('disabled_arr');
                }else{
                    $('.go-prev').removeClass('disabled_arr');
                }

                if(current_el>=new_events.length-event_amount){
                    $('.go-next').addClass('disabled_arr');
                }else{
                    $('.go-next').removeClass('disabled_arr');
                }
            }

            function scrollElUp(){

                var go_to = current_el-event_amount;
                if(go_to<0){
                    go_to = 0;
                }
                $(".next-events").animate({
                  scrollTop: $('.next-events').scrollTop() + $('.event-link:eq('+go_to+')').position().top
                }, 500);

                current_el = go_to;

                
                if(current_el==0){
                    $('.go-prev').addClass('disabled_arr');
                }else{
                    $('.go-prev').removeClass('disabled_arr');
                }

                if(current_el>=new_events.length-event_amount){
                    $('.go-next').addClass('disabled_arr');
                }else{
                    $('.go-next').removeClass('disabled_arr');
                }
            }

            $('body').on('click', '.go-next', function(){
                if(ended==false){
                    showMoreEvents();               
                }

                scrollElDown();
            });

            $('body').on('click', '.go-prev', function(){
                scrollElUp();
                
            });

            $('body').on('click', '.click_date', function(e){
                e.preventDefault();
                window.location.replace($(this).attr('href'));

            })

            
            var array = [];

            var events = [];
            var date = JSON.parse('{!! $dates !!}');
            //dates = date.replace(/&quot;/g,'"');
            var aux;
            for(var i = 0; i < date.length; i++)
            {
                aux = new Date(date[i]);
                events.push(aux.getTime());
            }

            
            $('#calendar').datepicker({
                language: 'pt',
                daysOfWeekHighlighted: '0,6',
                weekStart: '1',
                templates:{ 
                    leftArrow: '<img src="{{ asset("front/imgs/icons/seta-esquerda.svg") }}"/>',
                    rightArrow: '<img src="{{ asset("front/imgs/icons/seta-direita.svg") }}"/>',
                },

                beforeShowDay: function(date){

                    if ($.inArray(date.getTime(), events) > -1){
                        return {

                            classes: "click_date date_"+date.getDate()+date.getMonth()+date.getFullYear(),
                            content: "<a>"+date.getDate()+"</a>"
                         }
                    }else{
                        return {
                            enabled: false,
                        }
                    }
                    
                }
            }).on('changeDate', function(e) {
                var route = "{{ route('cms::frontend::model.data-search', [':dia',':mes',':ano'])}}";
                var url = route.replace(':dia',e.date.getDate()).replace(':mes',parseInt(e.date.getMonth()+1)).replace(':ano',e.date.getFullYear());
                window.location.href = url;
                // "pesquisa/q="+e.date.getDate()+"/"+parseInt(e.date.getMonth()+1)+"/"+e.date.getFullYear();
            });    
        });
</script>
@endsection
