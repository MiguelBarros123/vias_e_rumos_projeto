@extends('layouts.backend.app')

@inject('folders', 'Brainy\Gallery\Models\MediaFolder')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/cms/backend.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
@endsection

@section('module-scripts')
    <script src="{{asset('js/cms/backend.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-sanitize.js"></script>
    <script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="{{ asset('angular/directives/toggler/toggler.directive.js') }}"></script>
    <script src="{{asset('angular/directives/image_picker/multiple_image_picker.directive.js')}}"></script>
    <script src="{{asset('js/cms/fileupload/jquery.ui.widget.js')}}"></script>
    <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
    <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
    <script src="{{asset('js/cms/fileupload/jquery.iframe-transport.js')}}"></script>
    <script src="{{asset('js/cms/fileupload/jquery.fileupload.js')}}"></script>
    <script src="{{asset('js/cms/fileupload/jquery.fileupload-process.js')}}"></script>
    <script src="{{asset('js/cms/fileupload/jquery.fileupload-image.js')}}"></script>
    <script src="{{asset('js/cms/fileupload/jquery.fileupload-audio.js')}}"></script>
    <script src="{{asset('js/cms/fileupload/jquery.fileupload-video.js')}}"></script>
    <script src="{{asset('js/cms/fileupload/jquery.fileupload-validate.js')}}"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

    <script src="{{route('framework::backend::brainy', 'gallery')}}"></script>

    <script>

        var save_asset_url = "{{route('gallery::backend::save_asset')}}";
        var fetch_new_files = "{{route('gallery::backend::fetch_new_files')}}";

        var folders='{!! $folders::all()->toJson() !!}';
        var inicial='4';


        $(document).ready(function () {

            $('#date').daterangepicker({
                singleDatePicker: true,
                showDropdowns: true,
                startDate: moment()

            });


            $('.lang_tabs li:first-child').tab('show');
            $('.lang_content').children().first().addClass('active');

            $('.embed_lang_tabs li:first-child').tab('show');
            $('.embed_lang_content').children().first().addClass('active');

            if (typeof angular != 'undefined') {
                angular.bootstrap('body', ['BootstrapToggler']);
            }

            $('#toggler-faq-visibility').attr('name', 'visible');

            var $injector =angular.injector(['ng','ImagePicker']);
            var html = document.getElementsByTagName('image-picker');
            $injector.invoke(function($compile, $rootScope){
                var $scope = $rootScope.$new();
                var result= $compile(html)($scope);
                var cScope = result.scope(); // This is scope in directive controller. :  )
                var myBtn = document.getElementById('ulpload-directive');
                myBtn.addEventListener('click', cScope.fetchNewFiles);
            });

            $('#btn_upload_image').on('click', function () {

                var selected = $('div.item_galery.ui-selected');
                if(selected.length){

                    document.getElementById('image').value = selected.parent().attr('data-media-item-id');
                    $('#upload_resource').modal('hide');

                    var bgUrl = selected.children().first().css('background-image');
                    bgUrl = /^url\((['"]?)(.*)\1\)$/.exec(bgUrl);
                    bgUrl = bgUrl ? bgUrl[2] : '';

                    $('.image_highlight').attr('src', bgUrl);

                } else {
                    alert('Selecione uma imagem');
                }

            });

        });
    </script>
    <script src="{{asset('back/js/upload_files.js')}}"></script>

    @foreach($locales as $loc)
        <script>
            CKEDITOR.replace('editor_{{ $loc }}');
            CKEDITOR.replace('editor_embed_d_{{ $loc }}');
        </script>
    @endforeach

    <script src="{{asset('back/js/onload.js')}}"></script>

@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row tabs-back top_menu_page">
                        <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">
                            <ul class="list-inline fonte_12_lato">
                                <li>
                                    <a class="icon_voltar"
                                       href="{{route('cms::backend::pages.edit_page',array($page_id))}}">
                                        <i class="icon-voltar pading_right"></i>@lang('cms::backend.model.common.back')
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @include('layouts.backend.messages')
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                            <ul class="breadcrumb">
                                <li>@lang('cms::backend.model.menu.breadcrumb.site')</li>
                                <li>@lang('cms::backend.model.menu.breadcrumb.pages')</li>
                                <li>@lang('cms::backend.model.menu.breadcrumb.company')</li>
                            </ul>
                        </div>
                    </div>
                    <!-- highlight section -->
                    <form id="form-highlight-new" action="{{ route('cms::backend::composed.store', $composed_id) }}"
                          method="POST">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="page_id" value="{{ $page_id }}">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel">
                                    <div class="panel-titulo">
                                        <span class="titulo">
                                            <i class="icon-alterar-permissoes-1"></i>@lang('cms::backend.model.common.create')
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <!-- Nav tabs -->
                                        <ul class="tabs_brainy lang_tabs" role="tablist">
                                            @foreach($locales as $loc)
                                                <li role="presentation" class=""><a class="text-uppercase"
                                                                                    href="#{{ $loc }}_h" role="tab"
                                                                                    data-toggle="tab">{{$loc}}</a></li>
                                            @endforeach
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content lang_content">
                                            @foreach($locales as $loc)
                                                <div role="tabpanel" class="tab-pane fade in  margem_20"
                                                     id="{{ $loc }}_h">
                                                    <div class="form-group fom-style-hidden3 @if($errors->first($loc.'.title')) has-error @endif">
                                                        <label for="exampleInputEmail1">@lang('cms::backend.model.common.title')</label>
                                                        <input type="text" name="{{ $loc }}[title]" class="form-control"
                                                               value="{{ old('title') }}">
                                                        {!! $errors->first($loc.'.title','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                    <div class="form-group fom-style-hidden3 @if($errors->first('subtitle')) has-error @endif">
                                                        <label for="exampleInputEmail1">@lang('cms::backend.model.common.subtitle')</label>
                                                        <input type="text" name="{{ $loc }}[subtitle]"
                                                               class="form-control" value="{{ old('subtitle') }}">
                                                        {!! $errors->first('subtitle','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                    <div class="form-group fom-style-hidden3 @if($errors->first('description')) has-error @endif">
                                                        <label for="exampleInputEmail1">@lang('cms::backend.model.common.description')</label>
                                                        <textarea name="{{ $loc }}[description]" id="editor_{{ $loc }}"
                                                                  rows="10" cols="80">
                                                                        @lang('cms::backend.model.common.ckeditor-placeholder')
                                                                </textarea>
                                                        {!! $errors->first('description','<span class="validator_errors">:message</span>')!!}
                                                    </div>

                                                    <div class="form-group fom-style-hidden3 @if($errors->first('name_link')) has-error @endif">
                                                        <label for="exampleInputEmail1">@lang('cms::backend.model.common.link-name')</label>
                                                        <input type="text" name="{{ $loc }}[name_link]"
                                                               class="form-control" value="{{ old('name_link') }}">
                                                        {!! $errors->first('link','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                    <div class="form-group fom-style-hidden3 @if($errors->first('link') ) has-error @endif">
                                                        <label for="exampleInputEmail1">@lang('cms::backend.model.common.link') </label>
                                                        <input type="text" name="{{ $loc }}[link]" class="form-control"
                                                               value="{{ old('link') }}">
                                                        {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                            @endforeach
                                            <div class="form-group fom-style-hidden3 @if($errors->first('date')) has-error @endif">
                                                <label for="exampleInputDate">@lang('cms::backend.model.common.date')</label>
                                                <input id="date" class="form-control" type="text" name="date" value="01/01/2015" required/>
                                                {!! $errors->first('date','<span class="validator_errors">:message</span>')!!}
                                            </div>
                                            <div class="form-group fom-style-hidden2 @if($errors->first('name') ) has-error @endif">
                                                <label for="exampleInputEmail1">@lang('cms::backend.model.common.image-background')</label>
                                                <p>
                                                    <img src="" class="image_highlight">
                                                    <a class="btn btn-composed-image btn-default btn-cancelar" data-target="#upload_resource" data-toggle="modal">@lang('cms::backend.model.common.image-upload')</a>
                                                </p>
                                                {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                            </div>
                                            <input id="image" type="hidden" name="image_id" value="">
                                            <div class="form-group fom-style-hidden2 no_padding">
                                                <div class="col-lg-3 form-group fom-style-hidden2 no_padding">
                                                    <label for="faq-visibility">@lang('faqs::backend.faqs.edit.visibility')</label>
                                                    <bootstrap-toggler toggler-id="toggler-faq-visibility" state="true" toggler-name="visible"
                                                                       callback-success="toggleVisibility()"
                                                                       classes-state-0="icon-bloqueado icon20"
                                                                       classes-state-1="icon-desbloquear icon15">
                                                    </bootstrap-toggler>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END highlight section -->
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin_btn_pages">
                                <button id="submit-new-highlight" type="submit" name="submit" value="save"
                                        class="btn btn-default btn-yellow">@lang('cms::backend.model.pages.create.submit')</button>
                                <button id="save-new-highlight" type="submit" name="submit" value="stay"
                                        class="btn btn-default btn-stay btn-yellow">@lang('cms::backend.model.pages.create.submit-and-stay')</button>
                                <a href="{{route('cms::backend::pages.edit_page',array($page_id))}}" class="btn btn-default btn-cancelar">@lang('cms::backend.model.common.cancel')</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @include('cms::backend.pages.template.elements.upload_files')
@endsection
