@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    {{--<link rel="stylesheet" type="text/css" href="{{asset('css/seo/backend.css')}}">--}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/catalog/backend.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
@endsection

@section('module-scripts')
    {{--<script src="{{asset('js/seo/backend.js')}}"></script>--}}
    <script src="{{asset('js/catalog/backend.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
    <script src="{{asset('back/js/onload.js')}}"></script>
    <script>

        $(document).ready(function() {

            var options = {
                rota: "{{route('seo::backend::seo.ajax_all_pages')}}",
                processing: true,
                colunas: [
                    {
                        "orderable":      false,
                        "data":           null,
                        "defaultContent": ''
                    },
                    { "data": "pages" },
                    { "data": "images" }
                ],
                config: {
                    initComplete: function (settings, json) {
                        $(function () {
                            $.logicDatatable(options);
                        });
                    },
                    fnCreatedRow: function (nRow, aData, iDataIndex) {

                        //passar aqui o id do item
                        $(nRow).attr('id', aData['id']);
                        var tr2 = $(nRow).closest('tr');
                        tr2.find("td:first").addClass('details-control');
                    },
                    'fnDrawCallback': function () {



                    }


                },
                searchableElementId: 'search'
            };

            $('.default-table2').mydatatable(options);





            // Add event listener for opening and closing details
            $('#seo_table tbody').on('click', 'td.details-control', function () {
                var tr = $(this).closest('tr');
                var row = options.datatable.row( tr );

                if ( row.child.isShown() ) {
                    // This row is already open - close it
                    row.child.hide();
                    tr.removeClass('shown');
                }
                else {
                    // Open this row
                    var page_id = tr.attr('id');
                    var rotaForSeo="{{route('seo::backend::seo.ajax_my_description',['%page_id%'])}}";
                    var urlTo = rotaForSeo.replace('%page_id%', page_id);

                    $.get(urlTo, function (data) {
                        row.child(data,'new_row_variant_datatable').show();
                    });

                    tr.addClass('shown');
                }
            } );

        } );
    </script>
@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('seo::backend.layouts.top_menu')



                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="search" class="search search_icon" type="text"
                                   placeholder="@lang('seo::backend.common.search')">
                        </div>
                    </div>

                    @include('layouts.backend.messages')

                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('cms.seo.show') !!}

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                            <table id="seo_table" class="table default-table2 explandable_table table-hover">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>@lang('seo::backend.common.pages')</th>
                                    <th>@lang('seo::backend.common.images')</th>
                                </tr>
                                </thead>

                            </table>
                        </div>
                    </div>


                </div>
            </div>

        </div>
    </div>
@endsection
