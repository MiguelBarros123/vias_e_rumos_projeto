@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/cms/backend.css')}}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    <style>
        div.files {
            padding: 24px 15px;
        }

        .file-upload {
            display: inline-block;
            position: relative;
            min-width: 170px;
            -webkit-box-shadow: 7px 7px 30px -7px rgba(0, 0, 0, 0.62);
            -moz-box-shadow: 7px 7px 30px -7px rgba(0, 0, 0, 0.62);
            box-shadow: 7px 7px 30px -7px rgba(0, 0, 0, 0.62);
            margin: 0 20px 0 20px;
        }

        .file-info {
            background-color: white;
        }

        .file-info p {
            margin-bottom: 0 !important;
            padding: 5px 12px;
        }

        .file-info p:first-child {
            font-weight: 700;
        }
    </style>
@endsection

@section('module-scripts')
    <script src="{{asset('js/cms/backend.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(document).ready(function() {

          $('.product-select').select2({
            placeholder: ''
          });

          $('.product-select').on('change', function(){
            var url = '{{ route("cms::backend::products.variant", ":id") }}';
                url = url.replace(':id', $(this).val());

                $.ajax({
                    url         : url,
                    type        : 'get',
                    dataType    : 'json',
                    success:function(data)
                    {

                      $('.show-variants').show();

                      $('.variant-select').empty();
                      $('.variant-select').append('<option></option>');
                        $('.variant-select').append(data.result);
//                      $.each(data.variants,function(index,el){
//                          console.log(el);
//                          $('.variant-select').append('<option value="'+el.id+'">'+el.reference+'</option>');
//                      });

                    }
                });

          });

          $('.variant-select').on('change', function(){
            var url = '{{ route("cms::backend::variants.resource", [":prod_id", ":var_id"]) }}';
                url = url.replace(':prod_id', $('.product-select').val());
                url = url.replace(':var_id', $(this).val());

                $.ajax({
                    url         : url,
                    type        : 'get',
                    dataType    : 'json',
                    success:function(data)
                    {

                      $('.show-resources').show();

                      $('.show-resources').empty();

                      $.each(data.resources, function(index, el){
                        var token = '{{route('gallery::backend::storage_image',[":identifier_token"])}}';
                            token = token.replace(':identifier_token', el.identifier_token);

                        // $('.show-resources').append('' +
                        //   '<div class="col-md-2">' +
                        //   '<label class="resource-image">' +
                        //   '<input type="radio" name="resource" value="'+el.id+'"/>' +
                        //   '<img src="'+token+'" class="resource_preview">' +
                        //   '<label>' +
                        //   '</div>');

                        $('.show-resources').append('' +
                          '<div class="col-md-2">' +
                          '<label class="resource-image">' +
                          '<input type="radio" name="resource" value="'+el.id+'"/>' +
                          '<div class="resource_preview" style="background-image:url('+token+');"></div>' +
                          '<label>' +
                          '</div>');
                      });

                    }
                });

          });

          $('.variant-select').select2({
            placeholder: ''
          });

        });
    </script>

    <script src="{{asset('back/js/onload.js')}}"></script>

@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu ">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="row tabs-back top_menu_page">
                    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">
                        <ul class="list-inline fonte_12_lato">
                            <li>
                                <a class="icon_voltar"
                                   href="{{route('cms::backend::pages.edit_page',array($page_id))}}">
                                    <i class="icon-voltar pading_right"></i>@lang('cms::backend.model.common.back')
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                @include('layouts.backend.messages')
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                        <ul class="breadcrumb">
                            <li>@lang('cms::backend.model.menu.breadcrumb.site')</li>
                            <li>@lang('cms::backend.model.menu.breadcrumb.pages')</li>
                            <li>@lang('cms::backend.model.menu.breadcrumb.company')</li>
                        </ul>
                    </div>
                </div>
                <!-- highlight section -->
                <form id="form-highlight-new" action="{{ route('cms::backend::resource.store', $location) }}"
                      method="POST">
                    <input type="hidden" name="_method" value="PUT">
                    {!! csrf_field() !!}
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="panel">
                                <div class="panel-titulo">
                                    <span class="titulo">
                                        <i class="icon-alterar-permissoes-1"></i>@lang('cms::backend.model.common.main-spotlight')
                                    </span>
                                </div>
                                <div class="panel-body">
                                  <div class="form-group">
                                      <label for="product">@lang('cms::backend.model.common.product')</label>
                                      <select class="product-select" name="product">
                                        <option></option>
                                        @foreach($products as $product)
                                          <option value="{{$product->id}}">{{ $product->name }}</option>
                                        @endforeach
                                      </select>
                                  </div>
                                  <div class="form-group show-variants">
                                      <label for="variant">@lang('cms::backend.model.common.variant')</label>
                                      <select class="variant-select" name="variant">
                                        <option></option>
                                      </select>
                                  </div>
                                  <div class="row show-resources"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END highlight section -->
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin_btn_pages">
                            <button id="submit-new-highlight" type="submit" value="save"
                                    class="btn btn-default btn-yellow">@lang('cms::backend.model.pages.create.submit')</button>
                            <button id="save-new-highlight" type="submit" value="stay"
                                    class="btn btn-default btn-stay btn-yellow">@lang('cms::backend.model.pages.create.submit-and-stay')</button>
                            <a href="{{route('cms::backend::pages.edit_page',array($page_id))}}" class="btn btn-default btn-cancelar">@lang('cms::backend.model.common.cancel')</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
