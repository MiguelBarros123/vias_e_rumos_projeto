<?php 

namespace Brainy\ProfileReservedArea\Controllers\Backend;

use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Mail;
use Yajra\Datatables\Facades\Datatables;
use Brainy\Profiles\Models\GlobalCountry;
use Brainy\Cms\Models\ProfileReservedArea;
use Brainy\Cms\Requests\LoginAreaReservadaRequest;
use Brainy\Framework\Controllers\BackendController;
use App\Http\Requests\EditarPerfilAreaReservadaRequest;

class ProfileReservedAreaBackendController extends BackendController
{
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        return view('profilereservedarea::backend.profiles_reserved_area.index');
    }

    public function index_ajax()
    {
        $profiles = ProfileReservedArea::all();

        return Datatables::of($profiles)
            ->addColumn('action', function($profile){
                $rota = route('profilereservedarea::backend::profilereservedarea.delete');
                $edit_route = route('profilereservedarea::backend::profilereservedarea.edit', $profile->id);

                $string = '
                    <div class="btn-group pull-right">
                          <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a href="' . $edit_route . '" class=""><i class="icon-editar"></i>'. trans("profilereservedarea::backend.common.see_edit") .'</a></li>
                            <li><a data-impossible-delete="0" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $profile->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("cms::backend.model.common.delete") . ' </a></li>
                          </ul>
                        </div>
                ';

                return $string;
            })
            ->editColumn('order', function ($profile) {
                return '<i class="icon-icon-peq-img-size"></i>';
            })
            ->addColumn('email', function($profile){
                return $profile->email;
            })
            ->addColumn('name', function($profile){
                return $profile->firstname . $profile->lastname;
            })
            ->addColumn('aproved', function($profile){
                $user = $profile->user()->first();

                if($user->confirmed == 1)
                {
                    return '<span>'.trans("profilereservedarea::backend.common.inactive").'</span><input id="toggle-one'.$user->id.'" data-row="'.$user->id.'" type="checkbox" '.($user->confirmed_by_admin ? 'checked' : '').' data-on=" " data-off=" " name="apply" data-style="ios new_toggle"> <span class="block-icon">'.trans("profilereservedarea::backend.common.active").'</span>';
                }
                else
                {
                    return 'Conta por Confirmar';
                }
                
            })
            ->addColumn('date', function($profile){
                return Carbon::parse($profile->created_at)->format('d/m/Y');
            })
            ->make(true);
    }

    public function reorder()
    {
        if($request->fromPosition < $request->toPosition) {
            ProfileReservedArea::where('order_column','>',$request->fromPosition)->where('order_column','<=',$request->toPosition)->decrement('order_column');
        }

        if($request['fromPosition'] > $request['toPosition']) {
            ProfileReservedArea::where('order_column','>=',$request->toPosition)->where('order_column','<',$request->fromPosition)->increment('order_column');
        }

        ProfileReservedArea::where('id', $request->id)->update(array('order_column'=>$request->toPosition));
    }

    public function changeProfileState($id)
    {
        $user = User::find($id);
        $user->confirmed_by_admin = !$user->confirmed_by_admin;

        $array = [
            'user' => $user
        ];

        if($user->changed_first_time == 0 && $user->confirmed_by_admin == 0)
        {
            Mail::send('profilereservedarea::backend.emails.conta_ativada',$array, function($message) use($user){
                $message->from('mail@thesilverfactory.pt');
                $message->to($user->email)
                    ->subject('Verifique o seu email');
            });
            $user->changed_first_time = 1;
        }

        $user->save();

        $msg = [
            'userId' => $id,
            'state' => $user->confirmed_by_admin,
            'successMsg' => trans('permissions::backend.messages.success.edit.change_user_state')
       ];

       return $msg;
    }

    public function create()
    {
        $countries = GlobalCountry::all();
        return view('profilereservedarea::backend.profiles_reserved_area.create', compact('countries'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function store(LoginAreaReservadaRequest $request)
    {
        $confirmation_code = str_random(30);

        $user = User::create([
            'name' => $request->input('firstname'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'confirmation_code' => $confirmation_code
        ]);

        $user->profileReservedArea()->create([
            'email' => $request->input('email'),
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'nif' => $request->input('nif'),
            'cartao_cidadao' => $request->input('cartao_cidadao'),
            'data_validade' => Carbon::createFromFormat('d/m/Y',$request->input('data_validade'))->format('Y-m-d H:i'),
            'telefone' => $request->input('telefone'),
            'nome_fiscal' => $request->input('nome_fiscal'),
            'nipc' => $request->input('nipc'),
            'country' => $request->input('country'),
            'address' => $request->input('address'),
            'cod_postal' => $request->input('cod_postal'),
            'local' => $request->input('local')
        ]);

        $array = [
            'confirmation_code' => $confirmation_code
        ];

        Mail::send('cms::frontend.emails.verifica_conta',$array, function($message) use($request){
            $message->from('mail@thesilverfactory.pt');
            $message->to($request->input('email'))
                ->subject('Verifique o seu email');
        });

        return redirect()->route('profilereservedarea::backend::profilereservedarea.index');
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  
    * @return Response
    */
    public function edit($id)
    {
        $profile = ProfileReservedArea::findOrFail($id);
        $countries = GlobalCountry::all();
        dd($profile->bidding_systems->first());
        return view('profilereservedarea::backend.profiles_reserved_area.edit', compact('profile','countries'));
    }

    /**
    * Update a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function update(EditarPerfilAreaReservadaRequest $request, $id)
    {
        $profile = ProfileReservedArea::find($id);

        $profile->update([
            'nome_fiscal' => $request->input('nome_fiscal'),
            'nipc' => $request->input('nipc'),
            'country' => $request->input('country'),
            'address' => $request->input('address'),
            'cod_postal' => $request->input('cod_postal'),
            'local' => $request->input('local')
        ]);

        return redirect()->route('profilereservedarea::backend::profilereservedarea.index');
    }

    public function delete()
    {
        $rows = explode(',', request('deleted_rows'));
        foreach($rows as $id)
        {
            $profile = ProfileReservedArea::find($id);
            $profile->user()->delete();
            $profile->delete();
        }

        return redirect()->route('profilereservedarea::backend::profilereservedarea.index');
    }
}
