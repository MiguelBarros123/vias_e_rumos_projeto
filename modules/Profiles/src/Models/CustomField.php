<?php

namespace Brainy\Profiles\Models;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model
{
    protected $fillable=['name','type'];


    public function custom_options(){
        return $this->hasMany(CustomOption::class);
    }
}
