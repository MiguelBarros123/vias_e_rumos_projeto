<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 06-05-2016
 * Time: 10:11
 */

namespace Brainy\Gallery\Requests;
use App\Http\Requests\Request;

class CreateFolderRequest extends Request{


    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'=>'required|unique:media_folders',
        ];
    }

}