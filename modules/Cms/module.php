<?php

return [
    // The label is used to represent the module's name
    'label' => 'cms::backend.module-label',

    // Permissions are organized by groups
    // The level 1 key denotes the group name (label)
    // The level 1 value is an associative array
    // The level 2 key denotes the permission name (label)
    // The level 2 value is an array with the routes that requires the permission
    'permissions' => [

        'cms::backend.permission.manage' => [
            'cms::backend.permission.manage' => [
                'cms::backend::representatives',
                'cms::backend::model.change_composed_visibility',
                'cms::backend::pages.ajax_table',
                'cms::backend::pages.ajax_section_table',
                'cms::backend::pages.create',
                'cms::backend::resource.create',
                'cms::backend::variants.resource',
                'cms::backend::pages.edit_page',
                'cms::backend::pages.edit-representative',
                'cms::backend::pages.save_representative',
                'cms::backend::pages.edit',
                'cms::backend::contacts.update',
                'cms::backend::pages.save_page',
                'cms::backend::pages.save_highlight',
                'cms::backend::composed.reorder',
                'cms::backend::resource.reorder',
                'cms::backend::representatives.reorder',
                'cms::backend::representative.update',
                'cms::backend::representative.store',
                'cms::backend::resource.store',
                'cms::backend::composed.store',
                'cms::backend::upload.image',
                'cms::backend::save_asset',
                'cms::backend::products.variant',
                'cms::backend::representative.destroy',
                'cms::backend::resource.destroy',
                'cms::backend::pages.delete_multiple_composeds',
                'cms::backend::pages.delete_multiple_resources',
                'cms::backend::pages.contacts_delete_multiple',
            ],
        ],

    ],

    // Menu items denote the menus that this module renders
    'menu-items' => [
        [
            // Label for the menu item (mandatory)
            'label' => 'cms::backend.model.menu.list',
            // Route for the action (mandatory if the menu has no childs)
            'route' => 'cms::backend::pages.edit_page',
            // Css class can be used to render the icon
            'css-class' => 'icon-icon-lateral-paginas icon15menu',
            // Unique html id for the item
            'css-id' => null,
            // list of submenus
            'sub-menus' => [],
        ],
    ],
    // List of required providers for the module to work
    'providers' => [
        Brainy\Cms\Providers\CmsServiceProvider::class,
    ],

    // List of facades used by the module
    'aliases' => [
        // 'Recaptcha' => Greggilbert\Recaptcha\Facades\Recaptcha::class,
    ],

    // List of route middleware to register
    'middleware' => [
        //'acl' => \Brainy\Cms\Middleware\SomeMiddleware::class,
    ],

    'commands' => [
        // \Brainy\Cms\Commands\SomeCommand::class,
    ],

];
