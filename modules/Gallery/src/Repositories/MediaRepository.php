<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22-04-2016
 * Time: 12:06
 */

namespace Brainy\Gallery\Repositories;

use Brainy\Framework\Facades\Brainy;
use Brainy\Gallery\Models\MediaFolder;
use Brainy\Gallery\Models\MediaItem;
use Brainy\Gallery\Models\MediaItemTranslation;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class MediaRepository implements MediaRepositoryInterface
{


    private $medias;
    private $fileTypes;

    public function __construct(MediaFolder $medias)
    {
        $this->medias = $medias;
    }

    public function getAllMediaFromFolder($folder,$filtro)
    {

        $contents = $this->scanFolderContents($folder,$filtro);
        return $contents;
    }

    public function scanFolderContents($folderId, $filter = '')
    {
        $result = [
            'files' => [],
            'folders' => [],
        ];

        $result['files'][] = $this->getFilesFromFolder($folderId, $filter);
        $result['folders'][] = $this->getFoldersFromFolder($folderId, $filter);

        return $result;
    }

    public function getFilesFromFolder($folder_id,$filtro)
    {
        setlocale(LC_TIME, 'portuguese');

        if($filtro!= null){
            switch ($filtro[0]['filtro']){
                case "format":
                    $files = $this->medias->find($folder_id)->media_items()->whereIn('format',$filtro[0]['items'])->get();
                    break;
                case "search":
                    $files = $this->medias->find($folder_id)->media_items()->withfilter($filtro[0]['items'])->get();
                    break;
            }
        }else{
            $files = $this->medias->find($folder_id)->media_items()->get();
        }



        foreach ($files as $f) {
            $f['last_update'] = $f['updated_at']->formatLocalized('%d de %B, %Y');
            if ($f->type == 'imagem') {
                $f['imagem'] = route('gallery::backend::storage_image', array($f->identifier_token,231,250));
            }
            if ($f->type == 'documento' || $f->type == 'video' || $f->type == 'audio'|| $f->type == 'link') {
                $f['imagem'] = $f['thumbnail'];
            }

            $f['rota'] = route('gallery::backend::backend.gallery.media-item.edit', array($f->id));
            $f['rota_eliminar'] = route('gallery::backend::backend.gallery.media-item.destroy', array($f->id));

        }

        return $files;
    }

    public function getFoldersFromFolder($folder_id,$filtro)
    {

        if($filtro!=null){
            switch ($filtro[0]['filtro']){
                case "format":
                    $folders = $this->folderHaveFilesOfType($folder_id,$filtro[0]['items']);
                    break;
                case "search":
                    $folders = $this->medias->where('title', 'LIKE', '%'.$filtro[0]['items'].'%')->get();
                    break;
            }
        }else{
            $folders = $this->medias->where('parent_folder_id', $folder_id)->get();
        }

        $final_folders=collect();

        foreach ($folders as $f) {
            $f['last_update'] = utf8_encode($f['updated_at']->formatLocalized('%d de %B, %Y'));
            $f['rota'] = route('gallery::backend::galery', array($f['id'], $f['title']));
            $f['n_folders']=$f->numberOfFolders();
            $f['n_files']=$f->numberOfFiles();
            $f['size']=$this->getFolderSize($f['id']);


            foreach(Brainy::containers() as $cont){

                if($cont->name==$f->container_name){
                    $final_folders->push($f);
                }
            }


        }


        return $final_folders;
    }

    private function getFolderSize($id)
    {
        $bytes=0;
        $f1=MediaFolder::withTrashed()->find($id);

        foreach($f1->media_items as $file){
            if($file->type!='link'){
                if (Storage::disk('galeria')->has($file->identifier_token)) {
                    $bytes+=Storage::disk('galeria')->size($file->identifier_token);
                }
            }

        }

        $subfolders = collect();

        $this->getallsubfolders($id, $subfolders);


        foreach ($subfolders as $f) {
            foreach($f->media_items as $item){
                if($item->type!='link'){
                if (Storage::disk('galeria')->has($item->identifier_token)) {
                    $bytes+=Storage::disk('galeria')->size($item->identifier_token);
                }
                }
            }
        }



        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 0) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 0) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 0) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }

    public function folderHaveFilesOfType($folder_id,$items){



        $finalFolders=collect();
        $allfolders=$this->medias->where('parent_folder_id', $folder_id)->get();


        foreach ($allfolders as $f) {

            if($f->hasFilesOfType($items) > 0){
                $finalFolders->push($f);
            }else{

                $subfolders = collect();
                $this->getallsubfolders($folder_id, $subfolders);


                foreach($subfolders as $item){
                    if($item->hasFilesOfType($items) > 0){
                        $finalFolders->push($f);
                        break;
                    }
                }


            }


        }

        return $finalFolders;


    }


    public function getImageThumbnail($path)
    {

        $file = Storage::disk('galeria')->get($path);
        $type = Storage::disk('galeria')->mimeType($path);
        return response($file, 200, ['Content-Type' => $type]);
    }

    public function buildBreadcrumb($id)
    {
        $breadcrumb = collect();
        $pai = $this->medias->findOrFail($id);
        $breadcrumb->push($pai);

        while ($pai->parent_folder_id != 0) {
            $pai = $this->medias->find($pai->parent_folder_id);
            $breadcrumb->push($pai);
        }

        $reversed = $breadcrumb->reverse();

        return $reversed->values()->all();


    }


    public function deleteItems($items)
    {

        $cont_not_deleted = 0;

        if ($items['deleted_files'] != '') {

            $ff = explode(',', $items['deleted_files']);


            foreach ($ff as $new_f) {

                if (MediaItem::findOrFail($new_f)->isBeeingUsed()) {
                    $cont_not_deleted++;
                } else {


                    $ficheiro=MediaItem::find($new_f);

                    $lixo=MediaFolder::where('garbage',1)->where('parent_folder_id',1)->where('container_name',$ficheiro->media_folder->container_name)->first();
                    $ficheiro->media_folder_id=$lixo->id;
                    $ficheiro->save();

                    $ficheiro->delete();
                }

            }

        }

        if ($items['deleted_folders'] != '') {

            $folders = explode(',', $items['deleted_folders']);


            foreach ($folders as $f) {
                $tot= $this->folderHasUndeletedFiles($f);
                if($tot==0){
                    $pasta=MediaFolder::find($f);
                    $lixo=MediaFolder::where('garbage',1)->where('parent_folder_id',1)->where('container_name',$pasta->container_name)->first();
                    $pasta->parent_folder_id=$lixo->id;
                    $pasta->save();
                    $pasta->delete();
                }else{
                    $cont_not_deleted+=$tot;
                }

            }


        }

        return $cont_not_deleted;
    }

    public function destroyItems($items)
    {



        if ($items['deleted_files'] != '') {

            $ff = explode(',', $items['deleted_files']);
            foreach ($ff as $new_f) {
                    MediaItem::withTrashed()->find(intval($new_f))->forceDelete();

            }

        }




        if ($items['deleted_folders'] != '') {

            $folders = explode(',', $items['deleted_folders']);
            foreach ($folders as $f) {

                MediaFolder::withTrashed()->find($f)->media_items()->forceDelete();
                MediaFolder::withTrashed()->find($f)->forceDelete();


            }


        }

    }

    public function moveItems($items,$final_folder)
    {



        if ($items['deleted_files'] != '') {

            $ff = explode(',', $items['deleted_files']);


            foreach ($ff as $new_f) {


                $d=MediaItem::find($new_f);
                $d->media_folder_id=$final_folder;
                $d->save();


            }

        }


        if ($items['deleted_folders'] != '') {

            $folders = explode(',', $items['deleted_folders']);
            foreach ($folders as $f) {

                MediaFolder::find($f)->update(array('parent_folder_id',$final_folder));


            }


        }

    }


 public function recoverItems($items)
    {

     

        if ($items['deleted_files'] != '') {

            $ff = explode(',', $items['deleted_files']);
            foreach ($ff as $new_f) {
                    MediaItem::withTrashed()->find(intval($new_f))->restore();
            }

        }



        if ($items['deleted_folders'] != '') {

            $folders = explode(',', $items['deleted_folders']);
            foreach ($folders as $f) {
                MediaFolder::withTrashed()->find($f)->restore();
            }

        }

    }





    public function folderHasUndeletedFiles($folder)
    {

        $subfolders = collect();

        $this->getallsubfolders($folder, $subfolders);

        $total_nao_eliminados=0;

        foreach ($subfolders as $f) {
            foreach($f->media_items as $item){
                if($item->isBeeingUsed()){
                    $total_nao_eliminados++;
                }
            }
        }


        return $total_nao_eliminados;

    }






    public function getallsubfolders($folder,&$subfolders)
    {


        $subs = $this->medias->where('parent_folder_id', $folder)->get();

        foreach ($subs as $f2) {
            $subfolders->push($f2);
            $this->getallsubfolders($f2->id, $subfolders);
        }


    }


    public function copyItems($items, $folder)
    {
        $errors=collect();


        DB::transaction(function () use($items,$folder){

        //for the files
        if(array_key_exists('files',$items)){
            $this->copyMediaItem($items['files'],$folder);
        }

        //for the folders
        if(array_key_exists('folders',$items)) {
            $this->copyFolderItem($items['folders'],$folder);
        }


        });

        return $errors;

    }


    public function copyFolderItem($subfolders, $folder_id){




        $folder=MediaFolder::find($folder_id);



        foreach ($subfolders as $f) {

            $folder_a_copiar=MediaFolder::find($f);

            $more_subs=MediaFolder::where('parent_folder_id',$folder_a_copiar->id)->get()->toArray();



            $novo_folder=MediaFolder::create([
                'parent_folder_id'=>$folder->id,
                'localization'=>$folder->localization,
                'title'=>$folder_a_copiar->title.'- Cópia',
                'module_id'=>$folder_a_copiar->module_id,
                'size'=>$folder_a_copiar->size
                ]);




            $files = $this->medias->find($folder->id)->media_items()->select('id')->get()->toArray();

            $files_to_copy=collect();
            foreach($files as $x){
                $files_to_copy->push($x['id']);

            }

            $this->copyMediaItem($files_to_copy->toArray(),$novo_folder);


            $more_subs_to_copy=collect();
            foreach($more_subs as $z){
                $more_subs_to_copy->push($z['id']);
            }


            $this->copyFolderItem($more_subs_to_copy->toArray(),$novo_folder->id);


        }


    }


    public function copyMediaItem($items, $folder){

        $parent_folder=MediaFolder::find($folder);
        foreach ($items as $file){

            $original=MediaItem::findorFail($file);
            $my_tags=$original->tags()->select('id')->get();

            $original_translations=$original->translations->toArray();

            $new_token=$this->generateFileToken($original->format);

            $new_item=new MediaItem([
                'type'=> $original->type,
                'thumbnail'=>$original->thumbnail,
                'size'=>$original->size,
                'resolution'=>$original->resolution,
                'md5'=>$original->md5,
                'identifier_token'=>$new_token,
                'format'=>$original->format,
                'used'=>0,

            ]);

            $parent_folder->media_items()->save($new_item);



            foreach($my_tags as $tag){
                $new_item->tags()->attach($tag->id);
            }


            if($original->hasTranslation()){
                foreach($original_translations as $old_trans){
                    $new_item->translations()->save(new MediaItemTranslation(['title'=>$old_trans['title'].'- Cópia','description'=>$old_trans['description'],'locale'=>$old_trans['locale']]));
                }

            }

            //do copy
            Storage::disk('galeria')->copy($original->identifier_token, $new_item->identifier_token);

        }

    }


    private function generateFileToken($format){

        $exist='start';
        while($exist!=null){
            $token=$this->generateToken();
            $exist=MediaItem::where('identifier_token',$token)->first();
        }

        return  $token.'.'.$format;

    }

    private function generateToken(){
        return uniqid('',true);
    }

    public function getAllArchivedAssets($filtro)
    {

        $result = [
            'files' => [],
            'folders' => []
        ];


        if(MediaItem::onlyTrashed()->count() != 0){

            if($filtro!=null){
                switch ($filtro[0]['filtro']){

                    case "format":
                        $files = MediaItem::onlyTrashed()->whereIn('format',$filtro[0]['items'])->get();
                        break;
                    case "search":
                        $files = MediaItem::onlyTrashed()->withfilter($filtro[0]['items'])->get();

                        break;
                }
            }else{

                $files = MediaItem::onlyTrashed()->get();
            }


            foreach ($files as $f) {
                $f['last_update'] = $f['updated_at']->formatLocalized('%d de %B, %Y');
                if ($f->type == 'imagem') {
                    $f['imagem'] = route('gallery::backend::storage_image', array($f->identifier_token));
                }
                if ($f->type == 'documento' || $f->type == 'video' || $f->type == 'audio'|| $f->type == 'link') {
                    $f['imagem'] = $f['thumbnail'];
                }

                $f['rota'] = route('gallery::backend::backend.gallery.media-item.edit', array($f->id));
                $f['rota_eliminar'] = route('gallery::backend::backend.gallery.media-item.destroy', array($f->id));

            }
            $result['files'][]=$files;

        }else{
            $result['files'][]=[];
        }

        if($this->medias->onlyTrashed()->count() != 0){

            if($filtro!=null){
                switch ($filtro[0]['filtro']){

                    case "format":
                        $folders = [];
                        break;

                    case "search":
                        $folders = $this->medias->onlyTrashed()->where('title', 'LIKE', '%'.$filtro[0]['items'].'%')->get();
                        break;
                }
            }else{


                $folders=$this->medias->onlyTrashed()->get();
            }



            $final_folders=collect();
            foreach ($folders as $f) {
                $f['last_update'] = $f['updated_at']->formatLocalized('%d de %B, %Y');
                $f['rota'] = route('gallery::backend::galery', array($f['id'], $f['title']));
                $f['n_folders']=$f->numberOfFolders();
                $f['n_files']=$f->numberOfFiles();
                $f['size']=$this->getFolderSize($f['id']);
                /*  if(auth()->user()->details->hasPermitionToSeeFolderInThisModule($f['module_id'])){*/
                $final_folders->push($f);
                /*}*/

            }

            $result['folders'][] = $folders;
        }else{
            $result['folders'][]=[];
        }




        return $result;

    }


    public function zipFilesAndFolders($files, $folders)
    {


        $zip = new \ZipArchive;
        $filename='\gallery.zip';
        $nova_dir=$this->tempdir();


        if ($zip->open($nova_dir.$filename,\ZipArchive::OVERWRITE) == TRUE) {


            foreach($files as $f){
                $x=MediaItem::find($f);
                $zip->addFile(Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$x->identifier_token, $x->title.'.'.$x->format);
                $x->increment('n_of_downloads');
            }

            foreach($folders as $fol){
                $z=MediaFolder::find($fol);
                $zip->addEmptyDir($z->title);

                foreach($z->media_items as $item2){
                    $zip->addFile(Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$item2->identifier_token, $z->title.'/'.$item2->title.'.'.$item2->format);
                    $item2->increment('n_of_downloads');
                }


                $subfolders = collect();
                $this->getallsubfolders($fol, $subfolders);

                foreach ($subfolders as $subs) {
                    $ax=explode('/',$subs->localization);
                    $key=array_search($subs->title, $ax);
                    $output = array_slice($ax,$key);


                    $zip->addEmptyDir($z->title.'/'.implode('/',$output));


                    foreach($subs->media_items as $item){
                        $zip->addFile(Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$item->identifier_token, $z->title.'/'.implode('/',$output).'/'.$item->title.'.'.$item->format);
                        $item->increment('n_of_downloads');
                    }
                }
            }


            $zip->close();

        }

        return $nova_dir.$filename;
    }
    private function  tempdir() {

        $tempfile=tempnam(sys_get_temp_dir(),'gallery');
        if (file_exists($tempfile)) { unlink($tempfile); }
        mkdir($tempfile);
        if (is_dir($tempfile)) { return $tempfile; }

    }
}