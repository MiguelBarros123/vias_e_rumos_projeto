<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubTypeGoodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_type_goods', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('type_good_id');
            $table->string('name');
            $table->boolean('list_principal')->default(0);
            $table->boolean('list_advanced')->default(0);
            $table->timestamps();

            $table->foreign('type_good_id')->references('id')->on('type_goods')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_type_goods');
    }
}
