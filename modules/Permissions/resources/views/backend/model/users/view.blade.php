@extends('layouts.backend.app')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/profiles/backend.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/backend.css') }}">
@endsection

@section('module-scripts')



@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <a class="pull-right icon_eliminar icon-editar pading_right icon_delete_inside_view_show"
                       href="{{ route('permissions::backend::model.edit', array($user->id)) }}">
                        Editar
                    </a>
                    <ul class="list-inline">
                        <li><a href="{{route('permissions::backend::model.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.users.show',$user) !!}
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo"><i class="icon-editar-utilizador icon20"></i>@lang('profiles::backend.common.contact_sheet')</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <div id="user-image" class="col-lg-2 text-center">
                                    @if($user->photo!= null)
                                        <img id="preview_file" class="pic-circle-corner2" src="{{route('gallery::frontend::storage_image_p',[$user->photo,90,90])}}"  >
                                    @else
                                        <div class="pic-circle-corner text-center"><i class="icon-avatar-person icon80 "></i></div>
                                    @endif
                                </div>
                                <input id="fileupload" type="file" name="profile_image">
                                <div class="col-lg-4 profile_divider" >
                                    <div class="row">
                                        <div class="col-lg-12 profile_name">

                                            <div class="profile_name_18">{{$user->user->name}} {{$user->surname}}</div>
                                            @if($user->job_position)
                                                <div><span class="profile_input">Cargo</span> <b>{{ $user->job_position }}</b></div>
                                            @endif

                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group fom-style-hidden3 @if($errors->first('surname') ) has-error @endif">
                                                <label for="surname"><i class="icon-mail"></i> @lang('profiles::backend.common.emails')</label>

                                                <div class="profile_input"><i class="icon-avatar-person"></i> {{$user->user->email}}</div>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <div class="col-lg-5 col-lg-offset-1">

                                    <div class="row padding-top-20">

                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                            <label class="profile_input"><i class="icon-ultimo-login icon15"></i> @lang('permissions::backend.user.edit.last-login')</label>
                                        </div>
                                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                            <div>{{ isset($user->last_login) ? $user->last_login : trans('permissions::backend.user.edit.no-login-record') }}</div>
                                        </div>

                                    </div>

                                    @if($user->group)
                                        <div class="row">

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <label class="profile_input"><i class="icon-grupo icon15"></i> {{ ucfirst(trans_choice('permissions::backend.groups', 1)) }}</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <div>{{ $user->group->name }}</div>
                                            </div>

                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>



        </div>
    </div>

@endsection