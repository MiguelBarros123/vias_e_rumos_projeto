<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22-04-2016
 * Time: 12:22
 */

namespace Brainy\Cms\Models;

use Brainy\Framework\Models\Tag;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class MediaItem extends Model{

    use Translatable;
    use SoftDeletes;

    protected $fillable = ['type','media_folder_id','thumbnail','size','resolution','md5','identifier_token','format'];
    protected $hidden = ['pivot'];

    public $translatedAttributes = ['title','description'];
    public $translationModel = MediaItemTranslation::class;


    protected $dates = ['deleted_at'];

    public function media_folder(){
        return $this->belongsTo(MediaFolder::class);
    }

    public function tags(){
        return $this->morphToMany(Tag::class,'taggable');
    }

    public function mytagsnames(){
        return $this->tags()->select('id','name')->get();
    }

    public function hasTag($tag_id){


        if(!$this->tags->isEmpty()){
            return $this->tags->contains('id',$tag_id);
        }
        return false;
    }

    public function isBeeingUsed(){
        return $this->used;
    }

    public function mygaleryImage(){

        if($this->type == 'imagem'){
            return route('gallery::backend::storage_image', array($this->identifier_token));
        }
        return $this->thumbnail;
    }

    public function scopeWithfilter($query,$keyword){

        $final_query=$query->whereHas('translations', function($q) use($keyword){
            $q->Where('title', 'LIKE', '%'.$keyword.'%');
        })
            ->orwhereHas('translations', function($q) use($keyword){
            $q->Where('description', 'LIKE', '%'.$keyword.'%');
        })
            ->orwhereHas('tags', function($q) use($keyword){
                $q->Where('name', 'LIKE', '%'.$keyword.'%');
            });

        return $final_query;
    }

    public function getFileSize(){

        if($this->type =='link'){
            return 0;
        }

        $bytes= Storage::disk('galeria')->size($this->identifier_token);

        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;



    }

}