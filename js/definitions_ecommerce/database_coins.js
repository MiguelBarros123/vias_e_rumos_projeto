/**
 * Created by User on 27-09-2016.
 */

$(document).ready(function() {
    var options = {
        rota: rota,
        colunas: [
            {data: 'iso', name: 'iso'},
            {data: 'symbol', name: 'symbol'},
            {data: 'rate', name: 'rate'},
            {data: 'state', name: 'state'}
        ],
        config: {
            initComplete: function (settings, json) {
                $(function () {



                });
            },
            fnCreatedRow: function (nRow, aData, iDataIndex) {

            },
            'fnDrawCallback': function () {

                $('*[id^="toggle-one"]').bootstrapToggle();

                $('*[id^="toggle-one"]').on('change', function(){
                    var line = this.getAttribute('data-row');
                    console.log(line);

                    var url = changeVisibility;
                    url = url.replace(':id', line);

                    $.ajax({
                        url: url,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        dataType: 'json',
                        success:function(data)
                        {


                        }
                    });

                });

            }


        },
        searchableElementId: 'search'
    };

    $('.default-table4').mydatatable(options);

});

//# sourceMappingURL=database_coins.js.map
