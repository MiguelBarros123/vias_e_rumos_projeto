@extends('layouts.frontend.app')

@section('title', 'Vias e Rumos - Recuperar Password')


@section('content')
<form action="{{ url('pt/changePassword')}}" method="POST">
    {{ csrf_field() }}
    <div class="empresa-body cred-body login-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2>Recuperar Password</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <!-- <div class="inp_label">Email</div> -->
                        <div class="inps_wrap_cred">
                            <input class="inp_av" placeholder="" name="email" type="hidden" value="{{ $email or old('email') }}">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="inp_label">Nova Password</div>
                        <div class="inps_wrap_cred">
                            <input class="inp_av" placeholder="" name="password" type="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="inp_label">Nova Password Confirmação</div>
                        <div class="inps_wrap_cred">
                            <input class="inp_av" placeholder="" name="password_confirmation" type="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <span class="btn-line">
                            <button type="submit" name="" id="login" class="cred-btn">Alterar</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection
