<?php

namespace Brainy\Seo\Requests;

use App\Http\Requests\Request;

class SeoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'route_url_pt' => 'required'
        ];
    }

}