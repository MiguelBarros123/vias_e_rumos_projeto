
<div class="row tabs-back top_menu_page">
    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6 no_padding">


        <ul class="list-inline fonte_12_lato">

            <li class="@activePageTopMenu('seo::backend::model.index')">
                <a class="icon_voltar" href="{{route('seo::backend::model.index')}}">@lang('seo::backend.model.menu.seo')</a>
            </li>
            <li class="@activePageTopMenu('seo::backend::model.index_favicon')">
                <a class="icon_voltar" href="{{route('seo::backend::model.index_favicon')}}">@lang('seo::backend.model.menu.favicon')</a>
            </li>

        </ul>
    </div>



    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-6 text-right tabs-back no_padding">
        <ul class="list-inline ">

            @if(\Illuminate\Support\Facades\Request::route()->getName()=='catalog::backend::products.index')
                <li>
                    <a class="icon_eliminar" data-toggle="modal" data-target="#myModalCreateProduct"><i class="icon-adicionar pading_right"></i>@lang('catalog::backend.menu.create_new')</a>
                </li>
            @endif
            @if(\Illuminate\Support\Facades\Request::route()->getName()=='catalog::backend::attributes.index')<li><a data-target="#my_atributes" data-toggle="modal" class="icon_eliminar"><i class="icon-adicionar pading_right"></i>@lang('catalog::backend.menu.create_new_atribute')</a></li>@endif

        </ul>
    </div>

</div>