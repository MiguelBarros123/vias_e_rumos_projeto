<?php

namespace Brainy\Profiles\Models;

use Illuminate\Database\Eloquent\Model;

class CustomOption extends Model
{
    protected $fillable=['name'];

    public function profiles()
    {
        return $this->belongsToMany(Profile::class, 'profile_custom_options', 'custom_option_id', 'profile_id')->withPivot('value');
    }

}
