<div ng-app="brainy.categories.backend" ng-controller="CategoriesBackendController" >

@include($vista_parcial)

<!-- --------------------------------------------------- MODAL SELECT RECORD ---------------------------------------------------- -->
<div class="modal modal-aux" id="select_resource" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg modal-dialog-aux" role="document">      
            <div class="modal-content">
                <div class="panel-titulo"><span class="titulo"> {{$items}} </span></div>
                <div class="modal-body no_padding">
                   <div class="row">
                       <div  class="col-lg-12 " >

                           <div
                                ng-init="fetch('{{ $route }}')" >

                               <div class="col-lg-12 treeSearch no_padding">
                                   <input class="search search_icon" type="text" id="searchbox"
                                          placeholder="@lang('faqs::backend.faqs.index.search')"
                                          ng-model="search" ng-keyup="find()">
                               </div>

                               <div class=" grey3 col-lg-12" >
                                   <div class="js-tree-usage-content">
                                            <span class="js-tree-usage-info-icon">
                                                <i class="icon-info-extra-1 icon15 pading_right"></i>
                                            </span>
                                       <span class="js-tree-usage-info-text">
                                                {{$info}}
                                            </span>
                                   </div>


                               </div>

                               <div class="col-lg-12 jstree-faqs" js-tree="faqsTree.config" tree="faqsTree.instance" should-apply="true" tree-events-obj="faqsTree.events" style="height: 500px; overflow: auto;">

                               </div>
                           </div>
                       </div>
                   </div>
                </div>
                <div class="modal-footer modal-eliminar2 padding-bottom-30">
                    <button type="button" class="btn btn-default btn-yellow " ng-click="save_selection()">@lang("cms::backend.model.common.save")</button>
                    <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("cms::backend.model.common.cancel") </button>
                </div>
            </div>

    </div>
</div>
<!-- -------------------------------------------------MODAL SELECT RECORD  --------------------------------------------------- -->


</div>