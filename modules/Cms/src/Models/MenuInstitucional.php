<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class MenuInstitucional extends Model
{
    protected $guarded = [];
    public $timestamps = false;
}
