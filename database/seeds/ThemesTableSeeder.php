<?php

use Illuminate\Database\Seeder;
use Brainy\Faqs\Models\Theme;
use Brainy\Framework\Models\Tag;

class ThemesTableSeeder extends Seeder
{
    private function createThemes($langs, $faker, $isSubTheme = false, $count = 70) {
        for ($i = 0; $i < $count; $i++) {
            $theme = new Theme;

            foreach ($langs as $lang) {
                $theme->translateOrNew($lang)->name = $faker->sentence;
            }

            if ($isSubTheme) {
                $theme->parent()->associate(Theme::all()->shuffle()->first());
            }

            $theme->save();
        }
    }


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*$faker = \Faker\Factory::create();

        $langs = config('translatable.locales');

        $this->createThemes($langs, $faker);
        $this->createThemes($langs, $faker, true, 2);*/
    }
}