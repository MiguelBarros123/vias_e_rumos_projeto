@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    {{--<link rel="stylesheet" type="text/css" href="{{asset('css/seo/backend.css')}}">--}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/cms/backend.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/catalog/backend.css')}}">
@endsection

@section('module-scripts')
    {{--<script src="{{asset('js/seo/backend.js')}}"></script>--}}
    <script src="{{asset('js/catalog/backend.js')}}"></script>
    <script src="{{asset('back/js/onload.js')}}"></script>

@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('seo::backend.layouts.top_menu')

                    @include('layouts.backend.messages')

                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('cms.favicon.show') !!}

                    <div class="row">
                        <div class="col-lg-12">

                            <form method="POST" action="{{ route('seo::backend::favicon.save_favicon') }}" enctype="multipart/form-data">
                                <input type="hidden" name="_method" value="PUT">
                                {!! csrf_field() !!}


                                <div class="panel ">
                                    <div class="panel-titulo">
                                    <span class="titulo">
                                        <i class="icon-alterar-permissoes-1 "></i> @lang('seo::backend.common.upload_favicon')
                                    </span>
                                    </div>
                                    <div class="panel-body">

                                        <div class="form-group fom-style-hidden2 @if($errors->first('name') ) has-error @endif">
                                            <label for="exampleInputEmail1">@lang('seo::backend.common.add_zip')</label>
                                            <p>
                                                <img src="{{asset('mstile-150x150.png')}}" class="favicon_270 image_favicon">
                                                <img src="{{asset('apple-touch-icon.png')}}" class="favicon_152 image_favicon">
                                                <img src="{{asset('favicon-32x32.png')}}" class="favicon_32 image_favicon">
                                                <img src="{{asset('favicon.ico')}}" class="favicon_32 image_favicon">
                                                <img src="{{asset('favicon-16x16.png')}}" class="favicon_16 image_favicon">
                                            </p>
                                            {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                        <input id="zip_button" type="file" name="file" accept=".zip">

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin_btn_pages">
                                        <button type="submit" class="btn btn-default btn-yellow">@lang('cms::backend.model.common.save')</button>
                                    </div>
                                </div>


                            </form>

                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>

@endsection
