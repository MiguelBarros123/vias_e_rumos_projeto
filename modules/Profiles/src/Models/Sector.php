<?php

namespace Brainy\Profiles\Models;

use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Sector extends Model
{

    use Translatable;

    public $translationModel = SectorTranslation::class;
    public $translatedAttributes = [
        'name'
    ];

}
