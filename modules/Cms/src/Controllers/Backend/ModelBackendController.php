<?php

namespace Brainy\BiblicalCms\Controllers\Backend;

use Brainy\Framework\Controllers\BackendController;

class ModelBackendController extends BackendController
{
    public function index()
    {
        return view('biblicalcms::backend.model.index');
    }

    public function create()
    {
        return view('biblicalcms::backend.model.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
        ]);
    }

    public function show($model)
    {
        return view('biblicalcms::backend.model.show', compact('model'));
    }

    public function edit($model)
    {
        return view('biblicalcms::backend.model.edit', compact('model'));
    }

    public function update(Request $request, $model)
    {
        $this->validate($request, [
        ]);
    }

    public function delete($model)
    {
        return back();
    }
}
