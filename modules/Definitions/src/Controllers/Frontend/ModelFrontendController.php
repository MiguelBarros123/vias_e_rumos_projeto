<?php

namespace Brainy\DefinitionsRent\Controllers\Frontend;

use Brainy\Framework\Controllers\FrontendController;

class ModelFrontendController extends FrontendController
{
    public function index()
    {
        return view('definitionsrent::frontend.model.index');
    }
}
