@extends('layouts.backend.app')

@section('title', ucfirst(trans_choice('permissions::backend.users', 2)))

@section('module-styles')
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/backend.css') }}">
    <link rel="stylesheet" href="{{asset('back/css/select2.min.css')}}">
@endsection

@section('module-scripts')
    <script src="{{asset('js/permissions/backend.js')}}"></script>
    <script src="{{asset('back/js/topmenuselects.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>

    <!-- AngularJS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- ngBootstrapToggle -->
    <script src="{{ asset('angular/directives/toggler/toggler.directive.js') }}"></script>

    <script>

        $(document).ready(function(){
            $("#grupo option:selected").removeAttr("selected");

            renderInfoUserBlocked = function (data) {
                $('#info-lock-user-' + data.userId).css('visibility', data.state == '1' ? 'hidden' : 'visible');
                $row = options.datatable.row($('#info-lock-user-' + data.userId).parent()).index();
                options.datatable.cell($row, $col).data(data.state).draw();
            };

            var options= {
                rota: "{{ route('permissions::backend::model.users_ajax') }}",
                colunas: [
                { data: 'photo', name: 'photo' },
                { data: 'job_position', name: 'job_position' },
                { data: 'email', name: 'email' },
                { data: 'group', name: 'group' },
                { data: 'stateMessage', name: 'stateMessage', orderable: false, searchable: false},
                { data: 'state', name: 'state' },
                { data: 'action', name: 'action', orderable: false, searchable: false},
                { data: null, defaultContent: '', orderable: false, searchable: false },
                { data: 'userState', name: 'userState', visible: false }
                ],
                config: {
                    initComplete: function(settings, json) {
                        $(function(){
                            $.logicDatatable(options);
                        });
                    },
                        fnCreatedRow: function (nRow, aData, iDataIndex) {
                            //for clicable rows
                            $(nRow).attr('data-rota-see', aData['rota_final']);
                            var tr2 = $(nRow).closest('tr');
                            tr2.find("td:first").addClass('details-control');
                            tr2.find("td:not(:last, :nth-last-child(2), :nth-last-child(3))").addClass('clickable_url');
                        },
                    fnDrawCallback: function() {
                        $('*[id^="toggle_"]').bootstrapToggle();

                        $('*[id^="toggle_"]').each(function () {
                            line = this.getAttribute('data-row');
                            state = !$('#toggle_'+line).parent().hasClass('off');
                            $('#toggle_'+line).parent().addClass('btn btn-xs ios toggle-parent');
                            $('#toggle_'+line).parent().children('.toggle-group').children('.toggle-on').addClass('toggle-label-on');
                            if(!$('#toggle_'+line).parent().parent().children('i').length){
                                $('#toggle_'+line).parent().parent().append('<i class="info-unlock-'+line+' info-unlock icon-desbloquear icon15"></i>');
                                $('#toggle_'+line).parent().parent().prepend('<i class="info-lock-'+line+' info-lock icon-bloqueado icon20"></i>');
                            }
                            if(state){
                                $('.info-unlock-'+line).addClass('active');
                            } else {
                                $('.info-lock-'+line).addClass('active');
                            }
                        });

                        $('*[id^="toggle_"]').on('change', function(){
                            line = this.getAttribute('data-row');
                            state = !$('#toggle_'+line).parent().hasClass('off');
                            $('#info-lock-user-' + line).css('visibility', state == '1' ? 'hidden' : 'visible');
                            $row = options.datatable.row($('#info-lock-user-' + line).parent()).index();
                            if(state){
                                $('.info-unlock-'+line).addClass('active');
                                $('.info-lock-'+line).removeClass('active');
                            } else {
                                $('.info-lock-'+line).addClass('active');
                                $('.info-unlock-'+line).removeClass('active');
                            }
                            options.datatable.cell($row, $col).data(state);
                        });
                    }
                },
                searchableElementId: 'users_search'
            };

            var datatable = $('.default-table').mydatatable(options);

            $('.default-table').on('click', 'td.clickable_url', function () {
                var tr = $(this).closest('tr');
                window.location.href = tr.attr('data-rota-see');

            } );

            datatable.on('click', '.toggle-group' ,function () {
                var selected = $(this);
                var id = selected.parent().children('input').attr('data-row');
                var isChecked = selected.is(':checked');
                var url = "{{ route('permissions::backend::model.change_user_state', ':id') }}";
                url = url.replace(':id', id);

                $.ajax({
                    url         : url,
                    type        : 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data        : {'id':id},
                    success:function(data)
                    {
                        if (isChecked && data !== '') {
                            selected.attr('checked', false);
                        }
                    }
                });

            });

            var $col = options.datatable.column('userState:name').index();
            var $row;

            $("#estado option:selected").removeAttr("selected");

            $('#estado').multiselect({
                buttonText: function(options, select) {
                    return 'estado';
                },
                onChange: function(option, checked, select) {
                    var selected=[];
                    var selectedOptions = $('#estado option:selected');
                    $(selectedOptions).each(function(index, brand){
                        selected.push($(this).val());
                    });
                    
                    if(selected.length > 0) {
                    
                        if(selected.length == 2) {
                            console.log('both');
                            options.datatable.columns().search('').draw();
                        } else if(selected == 0) {
                            console.log('block');
                            options.datatable.columns(8).search('false').draw();
                        } else if(selected == 1) {
                            console.log('unblock');
                            options.datatable.columns(8).search('true').draw();
                        } 

                    } else {
                        options.datatable.columns().search('').draw();
                    }

                }
            });

            $('.multiselect-container li a').on('click', function(e){
                e.keyup(13);
            });

            $('#user-image').on('click', function (e) {
                e.preventDefault();
                $('#fileupload').click();
            });

            $("#fileupload").on('change', function(){
                readURL(this);
            });


        });

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                document.getElementById('user-image').style.backgroundImage = 'url(' + e.target.result +')';
                $('#user-image i').hide();
            };

            reader.readAsDataURL(input.files[0]);
        }
    }

    </script>


    @if(\Illuminate\Support\Facades\Session::has('errors'))
        <script>
            $('#modalCreateUser').modal();
        </script>
    @endif



@endsection

@section('content')
    <div class="conteudo_central">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <ul id="lista_menu" class="list-inline lista_menu">
                                <li>
                                    <select id="estado" multiple="multiple">
                                        <option value="0">@lang('permissions::backend.block')</option>
                                        <option value="1">@lang('permissions::backend.unblock')</option>
                                    </select>
                                </li>
                            </ul>
                        </div>
                    </div>

                    @include('permissions::backend.model.layouts.top_menu')

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="users_search" class="search search_icon" type="text"  placeholder="@lang('permissions::backend.user.index.search')">
                        </div>
                    </div>

                    @include('layouts.backend.messages')

                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.users') !!}

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                            <table id="users_table" class="table default-table" >
                                <thead>
                                <tr>
                                    <th>{{ ucfirst(trans_choice('permissions::backend.users', 1)) }}</th>
                                    <th>{{ ucfirst(trans('permissions::backend.user.model.position')) }}</th>
                                    <th>{{ ucfirst(trans('permissions::backend.user.model.email')) }}</th>
                                    <th>{{ ucfirst(trans('permissions::backend.user.model.group')) }}</th>
                                    <th class="user-state-info"></th>
                                    <th>{{ ucwords(trans('permissions::backend.block-unblock')) }}</th>
                                    <th class="multiple-options">
                                        @include('layouts.backend.partials.delete_datatables',['id'=>'users_table','route'=>'permissions::backend::users.delete_multiple'])
                                    </th>
                                    <th>
                                        <label class="checkbox-default">
                                            <input type='checkbox' id="sel_all_perm_users_table">
                                            <span></span>
                                        </label>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    
   <div class="modal modal-aux" id="modalCreateUser" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux modal-dialog-create-user" role="document">
            <form class="form_elimina_user" method="POST" action="{{route('permissions::backend::model.store')}}" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-novo-utilizador-1 icon30"></i>@lang('permissions::backend.user.index.create_new')</span></div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-3">
                                <div class="row">
                                    <div class="col-lg-12 ">
                                        <div id="user-image" class="pic-circle-corner-create-user" >
                                            <i class="icon-avatar-person icon50 icon-create-user-color-padding" ></i>
                                        </div>
                                        <input id="fileupload" type="file" name="profile_image">
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-9">
                                <div class="row">
                                    <div class="form-group fom-style-hidden ">
                                        <div class="col-lg-12">
                                            <label class="fonte_15_lato" style="padding-top: 6px">@lang('permissions::backend.user.index.name')</label>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control form-control-back-gray fonte_12_lato_light_cinza_model_new_user"  name="name" value="{{old('name')}}" >
                                            {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                        <div class="col-lg-12">
                                            <label class="fonte_15_lato" style="padding-top: 6px">@lang('permissions::backend.user.index.surname')</label>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control form-control-back-gray fonte_12_lato_light_cinza_model_new_user"  name="surname" value="{{old('surname')}}" >
                                            {!! $errors->first('surname','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                        <div class="col-lg-12">
                                            <label style="padding-top: 6px">@lang('permissions::backend.user.index.position')</label>
                                        </div>
                                        <div class="col-lg-12">
                                            <input type="text" class="form-control form-control-back-gray " name="job_position" value="{{old('job_position')}}">
                                            {!! $errors->first('job_position','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row padding-top-20">
                            <div class="form-group fom-style-hidden ">
                                <div class="col-lg-6">
                                    <label style="padding-top: 6px">@lang('permissions::backend.user.index.email')</label>
                                    <input type="email" class="form-control form-control-back-gray" name="email" value="{{old('email')}}">
                                    {!! $errors->first('email','<span class="validator_errors">:message</span>')!!}
                                </div>
                                <div class="col-lg-6">
                                    <label style="padding-top: 6px">@lang('profiles::backend.common.password')</label>
                                    <input type="text" name="password" class="form-control form-control-back-gray" value="{{str_random()}}">
                                    {!! $errors->first('password','<span class="validator_errors">:message</span>')!!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="row padding-top-35 fonte_12_lato_regular">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 list-groups-in-user-profile">

                                        <div class="row">
                                            <div  class="col-lg-6">
                                                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                                    <label>@lang('profiles::backend.common.group_front')</label>
                                                    <div>
                                                        @include('layouts.backend.partials.select_object_input', ['array'=>$front_groups, 'name'=>'front_group_id', 'selected'=> '', 'class'=> 'back_groups'])
                                                    </div>
                                                </div>
                                            </div>
                                            <div  class="col-lg-6">
                                                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                                    <label>@lang('profiles::backend.common.group_admin')</label>
                                                    <div>
                                                        @include('layouts.backend.partials.select_object_input', ['array'=>$back_groups, 'name'=>'back_group_id', 'selected'=> '', 'class'=> 'back_groups'])
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30" style="text-align: left">
                        <button type="submit" class="btn btn-default btn-yellow">@lang('permissions::backend.save')</button>
                        <button type="button" class="btn btn-cancelar" data-dismiss="modal"> @lang('permissions::backend.cancel') </button>
                    </div>
                </div>
            </form>
        </div>
    </div>



    @include('layouts.backend.partials.delete_form',['delete_message'=>'cms::backend.common.sure_delete'])

@endsection