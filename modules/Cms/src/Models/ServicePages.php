<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:19
 */
namespace Brainy\Cms\Models;

use Brainy\Catalog\Models\Categorie;
use Brainy\Catalog\Models\Product;
use Brainy\Catalog\Models\State;
use Brainy\Catalog\Models\Location;
use Brainy\DefinitionsEcommerce\Models\Coin;
use Brainy\Rent\Models\GlobalCoin;
use Illuminate\Support\Facades\Session;

class ServicePages {

    public function getPageContent($id){

        $highlights = Highlight::where("page_id", preg_replace('/[^A-Za-z0-9\-]/', '', $id))->get();
        $composeds = Composed::where("page_id", preg_replace('/[^A-Za-z0-9\-]/', '', $id))->get();
        $texts = Text::where("page_id", preg_replace('/[^A-Za-z0-9\-]/', '', $id))->get();
        $texts_simple = TextSimple::where('page_id', preg_replace('/[^A-Za-z0-9\-]/', '', $id))->get();
        // $coins = GlobalCoin::where('state', 1)->get();
        // $coin = GlobalCoin::where('iso', Session::get('coin'))->first();
        //$location = Location::where('resource_location', 'cms::backend.model.pages.index.third_section')->first();
        $location=false;
        $resources = '';
        if($location){
            $resources = $location->product_resources()->orderBy('order')->get();
        }

        $data = collect([
            'highlights' => $highlights,
            'composeds' => $composeds,
            'texts' => $texts,
            'texts_simple' => $texts_simple,
            // 'coins' => $coins,
            // 'coin' => $coin,
            'resources' => $resources
        ]);

        return $data;

    }

}
