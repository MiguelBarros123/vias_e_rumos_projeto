@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    {{--<link rel="stylesheet" type="text/css" href="{{ asset('css/cms/cms/backend/index.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{asset('css/cms/backend.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
@endsection

@section('module-scripts')
    <script src="{{asset('js/cms/backend.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-sanitize.js"></script>
    <script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>
    {{--<script src="{{asset('angular/js/ckeditor/ckeditor.js')}}"></script>--}}
    {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG73D9eYy8255L7G3mU37CPdu3oSOeLLE&callback=initMap" type="text/javascript" async defer></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCG73D9eYy8255L7G3mU37CPdu3oSOeLLE&callback=initMap" async defer></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/rowreorder/1.1.2/js/dataTables.rowReorder.min.js"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
    <script src="{{asset('back/js/onload.js')}}"></script>

    <script>
        var map;
        var marker = false;

        function markerLocation() {
            //Get location.
            var currentLocation = marker.getPosition();
            document.getElementById('lat').value = currentLocation.lat(); //latitude
            document.getElementById('lng').value = currentLocation.lng(); //longitude
        }

        function setMarker(map) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(parseFloat("{{$contacts_info->lat}}"),parseFloat("{{$contacts_info->long}}")),
                map: map,
                draggable: true
            });
        }

        function initMap() {
            var options = {
                center: new google.maps.LatLng(parseFloat("{{$contacts_info->lat}}"),parseFloat("{{$contacts_info->long}}")),
                zoom: 7,
                zoomControl: true
            };

            map = new google.maps.Map(document.getElementById('map'), options);

            setMarker(map);

            google.maps.event.addListener(map, 'click', function(event) {

                var clickedLocation = event.latLng;

                if(marker === false){
                    //Create the marker.
                    marker = new google.maps.Marker({
                        position: clickedLocation,
                        map: map,
                        draggable: true
                    });
                    //Listen for drag events!
                    google.maps.event.addListener(marker, 'dragend', function(event){
                        markerLocation();
                    });
                } else {
                    marker.setPosition(clickedLocation);
                }
                markerLocation();
            });
        }
    </script>


    <script>
        $(function () {
            CKEDITOR.replace('editor1');

            var options = {
                rota: "{{ route('cms::backend::representatives') }}",
                rowReorder: {
                    selector: 'i.icon-icon-peq-img-size'
                },
                colunas: [
                    { data: 'position', name: 'position', searchable: false },
                    { data: 'name', name: 'name', searchable: true },
                    { data: 'emails', name: 'emails', searchable: true },
                    { data: 'contacts', name: 'contacts', searchable: true },
                    { data: 'job_position', name: 'job_position', searchable: true },
                    { data: 'action', name: 'action', orderable: false, searchable: false },
                    { data: null, defaultContent: '', orderable: false, searchable: false }
                ],
                config: {
                    initComplete: function (settings, json) {
                        $(function () {
                            $.logicDatatable(options);
                        });
                    },
                    fnCreatedRow: function (nRow, aData, iDataIndex) {

                    },
                    'fnDrawCallback': function () {

                    }

                },
                searchableElementId: 'search-in-representatives'
            };

            $('#representatives-table').mydatatable(options);

            options.datatable.on('row-reorder', function (e, diff, edit) {
                var elem = options.datatable.row(edit.triggerRow.selector.rows).data();
                var chosen = diff.find(function(element) { return options.datatable.row(element.node).data() === elem });
                var oldPosition = elem.position.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');

                for ( var i=0, ien=diff.length ; i<ien ; i++ ) {

                    if(diff[i].oldPosition === chosen.newPosition){
                        var rowData = options.datatable.row( diff[i].node ).data();
                        var newPosition = rowData.position.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');
                        $.ajax({
                            type: 'PUT',
                            url: '{{ route('cms::backend::representatives.reorder') }}',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                id: elem.id,
                                fromPosition: oldPosition,
                                toPosition: newPosition
                            },
                            success: function (data) {
                                options.datatable.ajax.reload(null, false);
                            }
                        });
                    }
                }

            });

            {{--options.datatable.on('row-reorder', function (e, diff, edit) {--}}
                {{--var elem = options.datatable.row(edit.triggerRow.selector.rows).data();--}}
                {{--var chosen = diff.find(function(element) { return options.datatable.row(element.node).data() === elem });--}}
                {{--var oldPosition = elem.position.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');--}}

                {{--for ( var i=0, ien=diff.length ; i<ien ; i++ ) {--}}

                    {{--if(diff[i].oldPosition === chosen.newPosition){--}}
                        {{--var rowData = options.datatable.row( diff[i].node ).data();--}}
                        {{--var newPosition = rowData.position.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');--}}
                        {{--$.ajax({--}}
                            {{--type: 'PUT',--}}
                            {{--url: '{{ route('cms::backend::representatives.reorder') }}',--}}
                            {{--headers: {--}}
                                {{--'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')--}}
                            {{--},--}}
                            {{--data: {--}}
                                {{--id: elem.id,--}}
                                {{--fromPosition: oldPosition,--}}
                                {{--toPosition: newPosition--}}
                            {{--},--}}
                            {{--success: function (data) {--}}
                                {{--options.datatable.ajax.reload(null, false);--}}
                            {{--}--}}
                        {{--});--}}
                    {{--}--}}
                {{--}--}}

            {{--});--}}

        });
    </script>

    <script>
        $(function() {
            'use strict';

            var $inputNewContact = $('#add-new-contact');
            var $inputNewEmail = $('#add-new-email');

            var $contacts = $('#new-contacts');
            var $emails = $('#new-emails');

            function addToList(key, $elem) {
                var len = this.children().length;
                //this.append('<li id="' + key + '-' + len + '">' + $elem.val() + '</li>');
                this.append('<li id="' + key + '-' + len + '"><input type="hidden" name="' + key + 's[' + len + ']" value="' + $elem.val() + '">' + $elem.val() + '</li>');
                $elem.val('');
                $('#btn-add-'+key).prop('disabled', true);
            }

            function submitRequest() {
                var $form = $('#form-representative-new');

                // logic ...

                /*$form.submit(function () {
                    /*data: {
                        form['contacts'] = $contacts;
                      return true;
                    };
                });*/

            }

            $('#btn-add-contact').prop('disabled', true);
            $('#btn-add-email').prop('disabled', true);

            $inputNewContact.bind('input', function(){
                if($inputNewContact.val() != null){
                    $('#btn-add-contact').prop('disabled', false);
                }
            });

            $inputNewEmail.bind('input', function(){
                if($inputNewEmail.val() != null){
                    $('#btn-add-email').prop('disabled', false);
                }
            });

            $('#btn-add-contact').click(addToList.bind($contacts, 'contact', $inputNewContact));
            $('#btn-add-email').click(addToList.bind($emails, 'email', $inputNewEmail));

            $('#submit-new-representative').click(submitRequest);

        });

        $(document).ready(function(){
            $(document).on('click', '.open_edit_representative_modal', function(){
                var route = $(this).data('route');
                $('#form-representative-update').attr('action', route);

                var attributes = $.parseJSON(atob($(this).attr('data-attributes')));

                $('#form-representative-update input[name="name"]')[0].value = attributes.name;
                $('#form-representative-update input[name="order"]')[0].value = attributes.position;
                $('#form-representative-update input[name="job_position"]')[0].value = attributes.job_position;

                $('#modal-representative-update').modal();
            });
        });
    </script>

    @if($errors->first('name'))
        <script>
            $('#modal-representative-new').modal('show');
        </script>
    @endif

@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('cms::backend.pages.layouts.top_menu')
                    @include('layouts.backend.messages')
                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('cms.pages.show',$page) !!}
                    <form method="POST" action="{{ route('cms::backend::contacts.update') }}">
                        <input type="hidden" name="_method" value="PUT">
                        {!! csrf_field() !!}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="panel">
                                    <div class="panel-titulo">
                                        <span class="titulo">
                                            <i class="icon-alterar-permissoes-1 "></i>@lang('cms::backend.model.pages.contacts.contacts')
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('address') ) has-error @endif">
                                            <label for="exampleInputEmail1">@lang('cms::backend.model.pages.contacts.address')</label>
                                                <textarea name="address" id="editor1" rows="10" cols="80">
                                                    {!! $contacts_info->address !!}
                                                </textarea>
                                            {!! $errors->first('address','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                        <label class="margem_20">@lang('cms::backend.model.pages.contacts.geolocation')</label>
                                        <div class="row">
                                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                               <div class="form-group fom-style-hidden3 @if($errors->first('latitude') ) has-error @endif">
                                                   <label for="exampleInputEmail1">@lang('cms::backend.model.pages.contacts.latitude')</label>

                                                   <input type="text" class="form-control" id="lat" value="{{$contacts_info->lat}}" name="latitude">
                                                   {!! $errors->first('latitude','<span class="validator_errors">:message</span>')!!}
                                               </div>

                                               <div class="form-group fom-style-hidden3 @if($errors->first('longitude') ) has-error @endif">
                                                   <label for="exampleInputEmail1">@lang('cms::backend.model.pages.contacts.longitude')</label>

                                                   <input type="text" class="form-control" id="lng" value="{{ $contacts_info->long }}" name="longitude">
                                                   {!! $errors->first('longitude','<span class="validator_errors">:message</span>')!!}
                                               </div>
                                           </div>
                                           <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                                <div id="map"></div>
                                           </div>
                                        </div>


                                        <!-- CONTACTS SECTION -->
                                        <div class="row">
                                            <div class="divider_line margem_20"></div>
                                        @foreach($inputs->where('section','contacts') as $input)
                                            @include('cms::backend.pages.template.elements.input_field', $input)
                                        @endforeach
                                        </div>
                                        <!-- END CONTACTS SECTION -->

                                        <!-- FORM SECTION -->
                                        <div class="row">
                                            <div class="divider_line margem_20"></div>
                                            @foreach($inputs->where('section','form') as $input)
                                                @include('cms::backend.pages.template.elements.input_field', $input)
                                            @endforeach
                                        </div>
                                        <!-- END FORM SECTION -->


                                        <!-- SOCIIAL SECTION -->
                                        <div class="row">
                                            <div class="divider_line margem_20"></div>
                                            @foreach($inputs->where('section','social') as $input)
                                                @include('cms::backend.pages.template.elements.input_field', $input)
                                            @endforeach
                                        </div>
                                        <!-- END SOCIAL SECTION -->
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{--<div class="panel-titulo">--}}
                            {{--<span class="titulo">--}}
                                {{--<i class="icon-alterar-permissoes-1"></i>REPRESENTANTES--}}
                            {{--</span>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-12">--}}
                                {{--<input id="search-in-representatives" class="search search_icon" type="text" id="searchbox"--}}
                                        {{--placeholder="@lang('cms::backend.model.common.search')"--}}
                                        {{--ng-model="search">--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="row">--}}
                            {{--<div class="col-lg-12">--}}
                                {{--<a class="add_btn" data-toggle="modal" data-target="#modal-representative-new">--}}
                                    {{--<i class="icon-adicionar icon10"></i>--}}
                                    {{--@lang('cms::backend.model.pages.representatives.new')--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<table id="representatives-table" class="table default-table table-representatives">--}}
                            {{--<thead>--}}
                                {{--<tr>--}}
                                    {{--<th>@lang('cms::backend.model.pages.contacts.position')</th>--}}
                                    {{--<th>@lang('cms::backend.model.pages.contacts.name')</th>--}}
                                    {{--<th>@lang('cms::backend.model.pages.contacts.emails')</th>--}}
                                    {{--<th>@lang('cms::backend.model.pages.contacts.contacts')</th>--}}
                                    {{--<th>@lang('cms::backend.model.pages.contacts.job-position')</th>--}}
                                    {{--<th class="multiple-options">--}}
                                        {{--@include('layouts.backend.partials.delete_datatables',['id'=>'representatives-table','route'=>'cms::backend::pages.contacts_delete_multiple'])--}}
                                    {{--</th>--}}
                                    {{--<th>--}}
                                        {{--<label class="checkbox-default">--}}
                                            {{--<input type='checkbox' id="sel_all_perm_representatives-table">--}}
                                            {{--<span></span>--}}
                                        {{--</label>--}}
                                    {{--</th>--}}
                                {{--</tr>--}}
                            {{--</thead>--}}
                        {{--</table>--}}

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margin_btn_pages">
                                <button type="submit" class="btn btn-default btn-yellow">@lang('cms::backend.model.common.save')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @include('cms::backend.pages.modals.representatives.new')
    @include('cms::backend.pages.modals.representatives.edit')

    @include('layouts.backend.partials.delete_form',['delete_message'=>'cms::backend.common.sure_delete'])

@endsection
