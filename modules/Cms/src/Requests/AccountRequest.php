<?php

namespace Brainy\Cms\Requests;

use App\Http\Requests\Request;

class AccountRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'nif' => 'required'
        ];
    }

}