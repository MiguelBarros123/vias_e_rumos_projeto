@extends('reservations::backend.emails.main')
@section('content')



<container>

    <row>
        <columns>

            <spacer size="30"></spacer>

            <center>
                <img class="logo_principal" src="{{ asset('icons/cms/logo_sarafauto_color.svg') }}" alt="sarafauto" style="width: 250px !important">
            </center>

            <spacer size="30"></spacer>

        </columns>
    </row>

    <row>
        <p class="firsttitle title text-center small-text-center" style="text-align:center;"><b class="title-bold">@lang('cms::frontend.messages.msg_email_automatico', [], $lang)</b></p>
    </row>

    <row>
        <columns large="12">
            <p class="content_paragraph">{{$user->getResponse($lang)}}, {{$user->name}}!</p>
        </columns>
    </row>

    <row>
        <columns large="12">
            <p class="content_paragraph">@lang('cms::frontend.messages.msg_email_automatico2', [], $lang)</p>
        </columns>
    </row>

    <row>
        <columns large="12">
            <p class="content_paragraph">@lang('cms::frontend.messages.msg_email_automatico3', [], $lang)</p>
        </columns>
    </row>

    <row>
        <columns large="12">
            <p class="content_paragraph"><b>@lang('profiles::backend.messages.email.msg_account_email',[],$lang)</b> {{ $email }}</p>
        </columns>
    </row>

    <row>
        <columns large="12">
            <p class="content_paragraph"><b>@lang('profiles::backend.messages.email.msg_account_password',[],$lang)</b> {{ $password }}</p>
        </columns>
    </row>

    <row>
        <columns large="12">
            <p class="content_paragraph">@lang('cms::frontend.messages.msg_email_automatico11', [], $lang)</p>
        </columns>
    </row>

    <spacer size="15"></spacer>

    <spacer size="0" class="divider_line"></spacer>

    <row>
        <columns small="12" large="12">
            <p class="text-center small-text-center">@lang('cms::frontend.messages.msg_email_automatico12')</p>
        </columns>
    </row>

</container>


@stop