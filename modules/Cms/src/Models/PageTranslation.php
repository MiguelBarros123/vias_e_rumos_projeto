<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:50
 */

namespace Brainy\Cms\Models;
use Illuminate\Database\Eloquent\Model;

class PageTranslation extends Model {
    
    public $timestamps = false;
    
    protected $fillable = ['meta_description', 'route_name','route_url', 'title'];
}