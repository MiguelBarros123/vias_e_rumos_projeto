<?php

    //the image to be watermarked
    $image = $_GET['img'];
    $ref = $_GET['ref'];

    /*
     * set the watermark font size
     * possible values 1,2,3,4, and 5
     * where 5 is the biggest
     */
    $fontSize = 20;

    //set the watermark text
    $text = $ref;

    /*
     * position the watermark
     * 10 pixels from the left
     * and 10 pixels from the top
     */
    $xPosition = 10;
    $yPosition = 10;

    //create a new image
    $newImg = imagecreatefromjpeg($image);

    //set the watermark font color to red
    $fontColor = imagecolorallocate($newImg, 255, 255, 255);

    //write the watermark on the created image
    imagestring($newImg, $fontSize, $xPosition, $yPosition, $text, $fontColor);

    //output the new image with a watermark to a file
    header("Content-Type: image/jpeg");

    imagejpeg($newImg);
    /*
     * PNG file appeared to have
     * a better quality than the JPG
     */
    //imagepng($newImg,"add-a-text-watermark-to-an-image-with-php_01.png");
    //echo($newImg);

    /*
     * destroy the image to free
     * any memory associated with it
     */
    
    //echo $newImg;
    imagedestroy($newImg);

    ?>