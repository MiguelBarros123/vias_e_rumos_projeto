(function() {
    'use strict';

    function CategoriesBackendController($scope, $http ,$location) {




        $scope.search = '';
        $scope.no_categories_message = false;
        $scope.selected_colections = [];
        $scope.urlFaqsThemes = '';
        $scope.shouldMakeHttpRequest = true;
        $scope.selectedNode = {};
        $scope.publicUrl = 'http://localhost/brainy-web/public/';

        $scope.isEmptySelection = function () {
            return $.isEmptyObject($scope.selectedNode);
        };



        $scope.isInCreationMode = function () {
            return $scope.create_parent;
        };

        $scope.setInCreationMode = function () {
            $scope.create_parent=false;
        };

        $scope.clearSelection = function () {
            $scope.selectedNode = {};
        };

        $scope.escapeHtml = function (str) {
            return $(str).text();
        };

        function httpSuccess(response) {
            $scope.locales = response.locales;
            $scope.locale = response.locale || 'en'; // case undefined -> fallback en

            createCategoriesNodes(response);
        }

        function httpError(response) {

        }

        $scope.fetch = function(urlFaqsThemes) {
            $scope.urlFaqsThemes = urlFaqsThemes;

            $http({ method: 'GET', url: $scope.urlFaqsThemes }).success(httpSuccess).error(httpError);
        };

        $scope.find = function() {
            $scope.faqsTree.instance.jstree().search($scope.search);
        };

        $scope.setLocale = function(locale) {
            $scope.locale = locale;
            renderFaqsTree();
        };

        function getTextPropertyByNodeType(type) {
            return type === 'categorie' ? 'name' : type;
        }

        function getAllNodes(flat) {
            return angular.fromJson($scope.faqsTree.instance.jstree().get_json('#', { flat: flat }));
        }

        function renderFaqsTree() {
            var nodes = getAllNodes(true);

            $scope.shouldMakeHttpRequest = false;

            for (var i = 0; i < nodes.length; ++i) {
                var node = nodes[i];
                var data = $scope.faqsTree.mapper.node2data[node.id];
                var newText = getTextByLocale(data, getTextPropertyByNodeType(node.type));
                $scope.faqsTree.instance.jstree().rename_node(node, newText);
            }

            $scope.shouldMakeHttpRequest = true;
            //$scope.faqsTree.instance.jstree().redraw(true);
        }

        function getSelectedNodes() {
            return $scope.faqsTree.instance.jstree().get_selected(true);
        }

        function hasLocale(locale) {
            return function(element) {
                return element.locale === locale;
            };
        }

        function getTextByLocale(object, property) {
            if (!object.translations || !object.translations.length) {
                return object[property];
            }

            var result = object.translations.findIndex(hasLocale($scope.locale));

            if (result === -1) {
                return object[property];
            }

            return object.translations[result][property];
        }

        function setPropertyByLocale(object, property, node) {
            if ($scope.locale === undefined || (!object.translations || !object.translations.length)) {
                object[property] = node.text;
            }

            var result = object.translations.findIndex(hasLocale($scope.locale));

            if (result == -1) {
                object[property] = node.text;
            }

            object.translations[result][property] = node.text;
        }

        function handlerRenameNode(event, obj) {
            var data = $scope.faqsTree.mapper.node2data[obj.node.id];
            var property = getTextPropertyByNodeType(obj.node.type);

            if (data !== undefined) {
                setPropertyByLocale(data, property, obj.node);
            }

            if ($scope.shouldMakeHttpRequest) {
                var request = {};
                var i, locale;
                if (property == 'name') {
                    for (i = 0; i < $scope.locales.length; ++i) {
                        locale = $scope.locales[i];
                        request['name_' + locale] = data.translations[data.translations.findIndex(hasLocale(locale))].name;
                    }


                    var rota=window.Brainy.module.routes['catalog::backend::categories.rename'];
                    var urlTo = rota.replace('{id}', data.id);

                    $http.put($scope.publicUrl+urlTo, request);
                } else {
                    for (i = 0; i < $scope.locales.length; ++i) {
                        locale = $scope.locales[i];
                        request['question_' + locale] = data.translations[data.translations.findIndex(hasLocale(locale))].question;
                        request['answer_' + locale] = data.translations[data.translations.findIndex(hasLocale(locale))].answer;
                    }

                    $http.put('http://localhost/brainy-web/public/backend/faqs/faq/' + data.id + '/qa', request);
                }
            }
        }

        function httpSuccessDelete(response) {

            var node = $scope.faqsTree.mapper.data2nodes[response];
            $scope.faqsTree.instance.jstree().delete_node(node);


            /*var dirty = $scope.faqsTree.mapper.data2nodes[response];

             for (var i = 0; i < dirty.length; ++i) {
             delete $scope.faqsTree.mapper.node2data[dirty[i].id];
             }

             delete $scope.faqsTree.mapper.data2nodes[response];*/
        }

        function httpErrorDelete(response) {
            $('#modal-no-delete').modal();
        }

        function handlerDeleteNode(event, obj) {

            var data = $scope.faqsTree.mapper.node2data[obj.node.id];


        }

        function handlerChangedSelection(event, obj) {

        }

        function handlerSelectNode(event, obj) {

            $scope.selected_colections=[];

            for (var i = 0; i < obj.selected.length; ++i) {
                var node = obj.selected[i];
                var data = $scope.faqsTree.mapper.node2data[node];
                var no = $scope.faqsTree.mapper.data2nodes[node];
                data['posicao_final']=no[0].last_pos;
                data['principal']=no[0].main_categorie;
                data['max_order']=no[0].max_order;

                $scope.selected_colections.push(data);

            }


            $scope.$apply();



        }



        function handlerRedraw(event, nodes) {}

        function httpSuccessChangedPosition(response) {
            var nodes = $scope.faqsTree.mapper.data2nodes[response.id];
            if (nodes !== undefined && nodes.length) {
                $scope.faqsTree.mapper.node2data[nodes[0].id] = response;
            }
        }

        function httpErrorChangedPosition(response) {
            console.log('error at changing the position');
        }

        function handlerMoveNode(event, obj) {
            var data = $scope.faqsTree.mapper.node2data[obj.node.id];



            if (data !== undefined) {

                var pai;

                if (obj.parent !== '#') {
                    pai = $scope.faqsTree.mapper.node2data[obj.parent].id;
                }else{
                    pai = 0;
                }
                var rota=window.Brainy.module.routes['catalog::backend::brands.updatePosition'];
                var urlTo = rota.replace('{id}', data.id);



                if ($scope.shouldMakeHttpRequest) {
                    $http.put($scope.publicUrl+urlTo,{pai: pai})
                        .success(httpSuccessChangedPosition)
                        .error(httpErrorChangedPosition);
                }
            }
        }

        function faqsTreeError(error) {
            console.log('[ERROR] :: REASON => ' + error.reason);
        }

        function isNotNodeDraggable(node) {
            return node.type === 'answer';
        }

        function isDraggale(nodes, event) {
            return !nodes.find(isNotNodeDraggable);
        }

        function httpPostSuccess(callbackCreate) {
            return function(response) {
                callbackCreate([response]);
            };
        }

        function contextMenuItems (node) {

            function createThemeRoot() {

            }

            function createFAQRoot() {

            }

            function createThemeNode() {
                var theme = {
                    id: 'new-theme-' + Date.now(),
                    name: {
                        translations: {}
                    },
                    theme_id: $scope.faqsTree.mapper.node2data[node.id].id,
                    visible: true,
                    link: null,
                    tags: []
                };

                for (var i = 0; i < $scope.locales.length; ++i) {
                    theme.name.translations[$scope.locales[i]] = '[substitua este texto pelo título do tema]';
                }

                $http.post('http://localhost/brainy-web/public/backend/faqs/theme', theme)
                    .success(httpPostSuccess(createThemeNodes));
            }

            function createCategoriesNode() {

                $scope.themeType = 'subcategorie';
                $scope.create_parent = true;
                openModal('#modal-categorie');
            }

            function openModal(modalType) {
                var data = $scope.faqsTree.mapper.node2data[node.id];

                if (data !== undefined) {
                    $scope.selectedNode = data;
                }

                $(modalType).modal('show');
            }

            function editFAQNode() {
                openModal('#modal-categorie');
            }

            function editCategorieNode() {
                var data = $scope.faqsTree.mapper.node2data[node.id];
                $scope.themeType = (data.parent_id == 0)? 'categorie' : 'subcategorie';
                openModal('#modal-categorie');
            }

            function renameNode() {
                $scope.faqsTree.instance.jstree().edit(node);
            }

            function deleteThemeNode() {
                $scope.faqsTree.instance.jstree().delete_node(node);
            }

            function deleteCategorieNode() {
                var data = $scope.faqsTree.mapper.node2data[node.id];
                var nodes = $scope.faqsTree.mapper.data2nodes[data.id];


                var rota=window.Brainy.module.routes['catalog::backend::brands.destroy'];
                var rota_final=rota.replace('{id}',data.id);

                $http.delete($scope.publicUrl+rota_final)
                    .success(httpSuccessDelete)
                    .error(httpErrorDelete);


            }


            var actions = {
                createFAQRoot: {
                    label: 'Nova FAQ',
                    icon: '',
                    action: createFAQRoot
                },
                editCategorieNode: {
                    label: 'Editar',
                    icon: 'icon-editar icon-10',
                    action: editCategorieNode,
                    separator_after: true
                },
                createCategorieNode: {
                    label: 'Criar',
                    icon: 'icon-adicionar icon-10',
                    action: createCategoriesNode,
                    separator_after: true
                },
                createThemeNode: {
                    label: 'Subtema',
                    icon: '',
                    action: createThemeNode
                },
                renameNode: {
                    label: 'Mudar o nome',
                    icon: 'icon-mudar-nome icon-10',
                    action: renameNode
                },
                editThemeNode: {
                    label: 'Editar',
                    icon: '',
                    action: editCategorieNode,
                    separator_before: true,
                    separator_after: true
                },
                editFAQNode: {
                    label: 'Editar',
                    icon: '',
                    action: editFAQNode,
                    separator_before: true,
                    separator_after: true
                },
                deleteThemeNode: {
                    label: 'Eliminar',
                    icon: '',
                    action: deleteThemeNode
                },
                deleteCategorieNode: {
                    label: 'Eliminar',
                    icon: 'icon-opcoes-eliminar icon-10',
                    action: deleteCategorieNode
                }
            };

            switch (node.type) {
                case 'theme':
                    return {
                        create: {
                            label: 'Criar',
                            submenu: {
                                theme: actions.createThemeNode,
                                faq: actions.createFAQNode
                            }//,
                            //separator_after: true
                        },
                        edit: actions.editThemeNode,
                        rename: actions.renameNode,
                        delete: actions.deleteThemeNode
                    };
                case 'question':
                case 'answer':
                    return {
                        edit: actions.editFAQNode,
                        rename: actions.renameNode,
                        delete: actions.deleteFAQNode
                    };

                case 'categorie':
                    return {
                        create:actions.createCategorieNode,
                        edit: actions.editCategorieNode,
                        rename: actions.renameNode,
                        delete: actions.deleteCategorieNode
                    };
                default:
                    return {
                        createThemeRoot: actions.createThemeRoot,
                        createFAQRoot: actions.createFAQRoot
                    };
            }
        }

        $scope.faqsTree = {
            nodeModel: {
                id: '',
                text: '',
                parent: '#',
                type: 'default',
                state: {
                    'opened': true
                }
            },
            config: {
                core: {
                    check_callback: true,
                    multiple: true,
                    animation: 170,
                    error: faqsTreeError,
                    strings: {
                        'Loading ...': 'A carregar ...'
                    }
                },
                checkbox : {
                    "keep_selected_style" : true,
                    cascade: "",
                    three_state: false
                },
                search: {
                    fuzzy: true,
                    show_only_matches: true
                },
                types: {
                    '#': {
                        valid_children: [
                            'categorie'
                        ]
                    },
                    /* default is question (question) */
                    categorie: {
                        icon: false,
                        valid_children: [
                            'categorie'
                        ]
                    }
                },
                plugins: [
                    'checkbox',
                    'types',
                    'search',
                    'changed',
                    'wholerow'
                ],
                version: 1
            },
            instance: {},
            events: {
                'rename_node': handlerRenameNode,
                'move_node': handlerMoveNode,
                'delete_node': handlerDeleteNode,
                'changed': handlerChangedSelection,
                'redraw': handlerRedraw,
                'select_node': handlerSelectNode,
                'deselect_node': handlerSelectNode
            },
            mapper: {
                node2data: [],
                data2nodes: []
            },
            incrementer: 0
        };

        function getNodeParentId(data) {
            //return data.parent_id && $scope.faqsTree.mapper.data2nodes[data.parent_id] ? $scope.faqsTree.mapper.data2nodes[data.parent_id][0] : '#';
            return data.parent_id == 0 ? '#': data.parent_id ;
        }

        function createCategoriesNodes(total_data) {

            var finals=[];

            var categories=total_data.categories;
            var selected_id=total_data.selected_id;
            if(categories.length==0){
               $scope.no_categories_message=true;
            }
            while(categories.length !=0){

                for (var i = 0; i < categories.length; ++i) {
                    var faq = categories[i];

                    var nodecategorie = $.extend({}, $scope.faqsTree.nodeModel);
                    //nodecategorie.id = ++$scope.faqsTree.incrementer;
                    nodecategorie.id = faq.id;
                    var themeText = getTextByLocale(faq, 'name');
                    nodecategorie.text = themeText;
                    nodecategorie.type = 'categorie';
                    nodecategorie.parent_id = faq.parent_id;
                    nodecategorie.main_categorie = total_data.main_categorie;
                    var produtos_filtrados=faq.products.filter(function (item) {
                        return (item.id==selected_id);
                    });



                    if(produtos_filtrados.length > 0){
                        nodecategorie.state ={selected:true,opened:true};
                        nodecategorie.last_pos =produtos_filtrados[0].pivot.order;
                        nodecategorie.max_order =faq.last_product_order;


                    }else{
                        nodecategorie.state ={selected:false,opened:true};
                        nodecategorie.last_pos =faq.last_product_order+1;
                        nodecategorie.max_order =faq.last_product_order+1;
                    }



                    $scope.faqsTree.mapper.node2data[nodecategorie.id] = faq;
                    $scope.faqsTree.mapper.data2nodes[faq.id] = [nodecategorie];


                    var result=$scope.faqsTree.instance.jstree('create_node', getNodeParentId(faq), nodecategorie);
                    if(result != false){
                        categories.splice(i, 1);
                    }



                }


            }



            var selecionados=$scope.faqsTree.instance.jstree().get_selected(false);
            $scope.selected_colections=[];

            for (var i = 0; i < selecionados.length; ++i) {
                var node = selecionados[i];
                var data = $scope.faqsTree.mapper.node2data[node];
                var no = $scope.faqsTree.mapper.data2nodes[node];
                data['posicao_final']=no[0].last_pos;
                data['principal']=no[0].main_categorie;
                data['max_order']=no[0].max_order;
                $scope.selected_colections.push(data);

            }


            $scope.faqsTree.instance.jstree().redraw(true);



        }


        $scope.onlyNumbers = function (number_inp_pos) {
          /*  console.log($scope.number_inp_pos);*/
          /*  var sanitized = $(this).val().replace(/[^1-9]/g, '');
            $(this).val(sanitized);*/
        };

       /* $scope.$watch('number_inp_pos', function(newvalue,oldvalue) {
            console.log('ola');
        });*/



        /* -------------------------------- CHILDREN EVENTS  -------------------------------- */
        function handlerNewCategorie(event, faq) {
            createCategoriesNodes([faq]);
        }

        function handlerNewTheme(event, theme) {
            createThemeNodes([theme]);
        }

        function handlerEditFaq(event, faq) {

            var node = $scope.faqsTree.mapper.data2nodes[faq.id][0];
            var dirty = $scope.faqsTree.mapper.node2data[node.id];
            $scope.faqsTree.mapper.node2data[node.id] = faq;

            $scope.shouldMakeHttpRequest = false;

            $scope.faqsTree.instance.jstree().rename_node(node, getTextByLocale(faq, 'name'));


            if (dirty.parent_id !== faq.parent_id) {
                $scope.faqsTree.instance.jstree().move_node(node.id, getNodeParentId(faq));
            }

            $scope.shouldMakeHttpRequest = true;
        }

        function handlerEditTheme(event, theme) {
            var node = $scope.faqsTree.mapper.data2nodes[theme.id][0];

            var dirty = $scope.faqsTree.mapper.node2data[node.id];

            $scope.faqsTree.mapper.node2data[node.id] = theme;

            $scope.shouldMakeHttpRequest = false;
            $scope.faqsTree.instance.jstree().rename_node(node, getTextByLocale(theme, 'name'));

            if (dirty.theme_id !== theme.theme_id) {
                $scope.faqsTree.instance.jstree().move_node(node.id, getNodeParentId(theme));
            }

            $scope.shouldMakeHttpRequest = true;
        }

        $scope.$on('new categorie', handlerNewCategorie);
        $scope.$on('new theme', handlerNewTheme);

        $scope.$on('edit faq', handlerEditFaq);
        $scope.$on('edit theme', handlerEditTheme);
        /* ------------------------------ END CHILDREN EVENTS ------------------------------ */



        /* -------------------------------- TAGS  -------------------------------- */
        var tags = [];

        function httpTagsSuccess(response) {
            tags = response;
        }

        function httpTagsError(response) {
            tags = [];
        }

        $http({ method: 'GET', url: 'http://localhost/brainy-web/public/framework/tags' })
            .success(httpTagsSuccess)
            .error(httpTagsError);

        $scope.getTags = function () {
            return tags;
        };
        /* ------------------------------ END TAGS ------------------------------ */

        $scope.dismiss = function (id) {
            $(id).modal('hide');
        };
    }

    function allowedParents() {
        function hasValueInObj(key, value) {
            return function (obj) {
                return obj[key] === value;
            };
        }

        function hasParent(element, idKey, parentKey, arrayData, id) {
            if (! element[parentKey]) {
                return false;
            }

            if (element[parentKey] === id) {
                return true;
            }

            var next = arrayData.find(hasValueInObj(idKey, element[parentKey]));

            return hasParent(next, idKey, parentKey, arrayData, id);
        }

        return function (input, id, idKey, parentKey) {
            var output = [];

            angular.forEach(input, function(element) {
                if (element[idKey] !== id && ! hasParent(element, idKey, parentKey, input, id)) {
                    output.push(element);
                }
            });

            return output;
        };
    }

    angular.module('brainy.categories.backend')
        .controller('CategoriesBackendController', ['$scope', '$http','$location', CategoriesBackendController])
        .filter('allowedParents', allowedParents);
})();
