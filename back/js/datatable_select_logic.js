(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

$.selectDatatableLogic = function (options) {

    // console.log('options', options.id_of_table);

    var id_of_table = options.id_of_table;

    var start = 'myTemplate_' + id_of_table;

    var table_list = [];

    draw_list();

    $('#save_selection_' + id_of_table).click(function () {

        $('#empty_list_' + id_of_table).hide();
        draw_list();
        $('#select_resource_' + id_of_table).modal('hide');
    });

    $(document).on('click', '#delete_selected_item_datatable_list_' + id_of_table, function () {
        $('#empty_list_' + id_of_table).hide();
        var id = $(this).attr('data-id');

        //remove from datatable
        options.datatable.row($('#sel_row_' + id)).deselect();
        //remove from view
        $(this).parent().parent().remove();

        //remove from list
        var hasMatch2 = table_list.filter(function (a) {
            return a.id == id;
        })[0];
        table_list.splice(table_list.indexOf(hasMatch2), 1);

        if (table_list.length == 0) {
            $('#empty_list_' + id_of_table).fadeIn();
        }
    });

    function draw_list() {

        var datatable = options.datatable;
        var dataRows = datatable.rows({ selected: true }).data();

        console.log('dataRows', dataRows);

        //for datatable rows
        $.each(dataRows, function (index, item) {

            //add item lo list and render it
            var hasMatch = table_list.some(function (sel) {
                return sel.id == item.id;
            });

            if (!hasMatch) {
                var quant = table_list.push(item);
                replacing(quant - 1);
                start += ";";

                var result = eval(start);
                $('#initial_table_' + id_of_table).append(result);
                start = 'myTemplate_' + id_of_table;
            }
        });

        //delete from list items that aren´t on datatable
        var keys_to_del = [];
        $.each(table_list, function (index, item) {

            var hasMatch2 = dataRows.filter(function (a) {
                return a.id == item.id;
            })[0];

            if (typeof hasMatch2 === 'undefined') {
                console.log(index);
                keys_to_del.push(index);
                $('#sel_item_list2_' + id_of_table + '_' + item.id + '').remove();
            }
        });

        keys_to_del.sort(function (a, b) {
            return b - a;
        }).forEach(function (index) {
            table_list.splice(index, 1);
        });

        //if empty
        if (table_list.length == 0) {
            $('#empty_list_' + id_of_table).fadeIn();
        }
    }

    function replacing(index) {

        //best method
        //find items to replace in template
        //check if table_list as item
        //add item to replace

        $.each(table_list[index], function (key, value) {

            if ((typeof value === 'undefined' ? 'undefined' : _typeof(value)) == 'object' && value != null) {

                $.each(value[0], function (key2, value2) {

                    start += ".replace(/\\${" + key + "." + key2 + "}/g,'" + value2 + "')";
                });
            } else {
                start += ".replace(/\\${" + key + "}/g,'" + value + "')";
            }
        });
    }

    return {
        redraw: function redraw() {
            table_list = [];
            $('#initial_table_' + id_of_table).empty();
            draw_list();
        }
    };
};

},{}]},{},[1]);

//# sourceMappingURL=datatable_select_logic.js.map
