<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComposedHighlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('c_highlights', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->boolean('visible');
            $table->integer('order');
            $table->integer('composed_id')->unsigned();
            $table->text('photo');
            $table->dateTime('date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('c_highlights');
    }
}
