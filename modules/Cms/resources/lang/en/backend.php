<?php

return [
    'model' => [
        'common' => [
            'search' => 'pesquisar',
            'new' => 'Adicionar novo',
            'site' => 'O meu site',
            'pages' => 'Páginas',
            'title' => 'Título',
            'identifier' => 'Identificador',
            'subtitle' => 'Subtítulo',
            'description' => 'Descrição',
            'photo' => 'Foto',
            'link'=>'Link',
            'link-name' => 'Nome do Link',
            'state' => 'Invisível / Visível',
            'subtitle' => 'Subtitulo',
            'image' => 'Imagem',
            'image-upload' => 'Upload de imagem',
            'image-edit' => 'Editar imagem',
            'image-background' => 'Imagem de fundo',
            'save' => 'Guardar',
            'cancel' => 'Cancelar',
            'back' => 'Voltar',
            'gallery' => 'Galeria',
            'upload' => 'Upload',
            'main-spotlight' => 'DESTAQUE PRINCIPAL',
            'ckeditor-placeholder' => 'Isto é um texto de exemplo para ser substituído.',
            'product' => 'Produto',
            'variant' => 'Variante',
            'sure_delete_composed' => 'Tem a certeza que pretende eliminar os composeds selecionadas?',
            'delete' => 'Eliminar',
            'edit' => 'Editar',
        ],
        'menu' => [
            'list' => 'Páginas',
            'breadcrumb' => [
                'site' => 'O meu site',
                'pages' => 'Páginas',
                'company' => 'Empresa'
            ],
        ],
        'upload' => [
            'title' => 'Upload',
            'dnd' => 'DRAG and DROP',
            'drag-or-click-to-upload' => '<u>Arraste</u> o/s ficheiro/s para a caixa ou <u>clique</u> para fazer upload.',
            'allowed-files' => [
                'info' => 'É permitido o upload de: ',
                'img' => 'Imagens',
            ],
            'progress' => 'Upload em progresso - ',
            'embed' => 'Embed Link',
        ],
        'pages' => [
            'index' => [
                'home' => 'Home',
                'home_detail' => 'Primeira Secção',
                'first_section' => 'Primeira Secção',
                'second_section' => 'Segunda Secção',
                'third_section' => 'Terceira Secção',
                'slider' => 'Slideshow Coleções',
                'last_section' => 'Última Secção',
                'terms' => 'Termos',
                'condition' => 'Condições',
                'cookies-policy' => 'Política de Cookies',
                'what-are-cookies' => 'O que são Cookies',
                'cookies-goal' => 'Finalidade das Cookies',
                'cookies-types' => 'Tipo de Cookies'
            ],
            'create' => [
                'submit' => 'Criar',
            ],
            'edit' => [
                'visibility' => 'Invisível / Visível',
            ],
            'contacts' => [
                'address'=>'Morada',
                'latitude'=>'Latitude',
                'longitude'=>'Longitude',
                'phone'=>'telefone',
                'mobile-phone'=>'telemovel',
                'fax'=>'fax',
                'email-form'=>'email do formulário',
                'geolocation' => 'Geolocalização no google maps',
                'position' => 'Posição',
                'name' => 'Nome',
                'emails' => 'Emails',
                'contacts' => 'Contactos',
                'job-position' => 'Cargo',
                'actions' => 'Ações',
                'order' => 'Ordem',
                'job_position' => 'Cargo',
            ],
            'representatives' => [
                'new' => 'Adicionar novo representante',
                'edit' => 'Editar representante',
                'del' => 'Remover representante',
                'common' => [
                    'edit' => 'Editar',
                ],
            ],
        ],
    ],
    'hidden' => 'invisível | invisíveis',
    'visible' => 'visível | visíveis',
    'composed' => [
        'index' => [
            'info-hidden' => 'este item não está visível!',
        ],
        'common' => [
            'delete' => 'Eliminar',
        ],
    ],
    'messages' => [
        'success' => [
            'added' => 'Item adicionado com sucesso.',
            'update_contacts' => 'Dados alterados com sucesso.',
            'update_pages' => 'Dados alterados com sucesso',
            'deleted_group' => 'Elemento(s) removido(s) com sucesso',
            'deleted_representatives' => 'Representante(s) removido(s) com sucesso',
            'delete' => [
                'representative' => 'Representante com o id \':id\' eliminado com sucesso.',
            ]
        ],
        'error' => [
            'delete' => [
                'representative' => 'Não foi possível eliminar o representante com o id \':id\'.',
            ],
        ],
    ],
];
