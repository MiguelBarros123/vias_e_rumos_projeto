<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaFolderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_folders', function (Blueprint $table) {

            $table->engine='InnoDB';

            $table->increments('id');
            $table->string('title');
            $table->string('localization');
            $table->integer('parent_folder_id');
            $table->string('container_name');
            $table->double('size');
            $table->boolean('garbage')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('media_folders');
    }
}
