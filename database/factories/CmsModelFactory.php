<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Brainy\Cms\Models as Cms;

$factory->defineAs(Cms\Representative::class, 'representatives', function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'job_position' => $faker->jobTitle,
        'position' => $faker->numberBetween(0, 50),
    ];
});

$factory->defineAs(Cms\RepresentativeContact::class, 'representative_contacts', function (Faker\Generator $faker) {
	return [
		'representative_id' => Cms\Representative::all()->shuffle()->first()->id,
		'contact' => $faker->phoneNumber,
	];
});

$factory->defineAs(Cms\RepresentativeEmail::class, 'representative_emails', function (Faker\Generator $faker) {
	return [
		'representative_id' => Cms\Representative::all()->shuffle()->first()->id,
		'email' => $faker->email,
	];
});