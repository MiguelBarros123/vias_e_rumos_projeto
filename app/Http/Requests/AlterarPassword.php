<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AlterarPassword extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required|email|min:10|max:255',
            'password' => 'required|min:6',
            'password_confirmation' => 'required' 
        ];
    }

    public function messages()
    {
        return [
            'password.required' => 'Este campo é obrigatório',
            'password.min' => 'Este campo não tem o mínimo de caracteres',
            'password_confirmation.required' => 'Este campo é obrigatório',
            'password_confirmation.same' => 'Passwords tem de ser iguais'
        ];
    }
}
