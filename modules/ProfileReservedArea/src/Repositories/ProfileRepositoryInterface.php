<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 24/05/2016
 * Time: 18:13
 */

namespace Brainy\Profiles\Repositories;


interface ProfileRepositoryInterface
{
    public function edit($id);
    public function create();
    public function store($request);
    public function addProfile($request);
    public function update($request, $id);
    public function getMonths();
    public function getLanguages();
}