@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/gallery/backend.css')}}">
@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropper/2.3.0/cropper.min.js"></script>
    <script>
        $(document).ready(function(){
            var $previews = $('.preview');

            $('#image_crop').cropper({

                viewMode:0,

                build: function (e) {
                    var $clone = $(this).clone();
                    $clone.css({
                        display: 'block',
                        width: '100%',
                        minWidth: 0,
                        minHeight: 0,
                        maxWidth: 'none',
                        maxHeight: 'none'
                    });

                    $previews.css({
                        width: '100%',
                        overflow: 'hidden'
                    }).html($clone);
                },

                crop: function(e) {
                    // For preview
                    var imageData = $(this).cropper('getImageData');
                    var previewAspectRatio = e.width / e.height;

                    $previews.each(function () {
                        var $preview = $(this);
                        var previewWidth = $preview.width();
                        var previewHeight = previewWidth / previewAspectRatio;
                        var imageScaledRatio = e.width / previewWidth;

                        $preview.height(previewHeight).find('img').css({
                            width: imageData.naturalWidth / imageScaledRatio,
                            height: imageData.naturalHeight / imageScaledRatio,
                            marginLeft: -e.x / imageScaledRatio,
                            marginTop: -e.y / imageScaledRatio
                        });
                    });
                }
            });

            $('.docs-buttons button').click(function(){
                $('#image_crop').cropper($(this).attr('data-method'),$(this).attr('data-option'),$(this).attr('data-second-option'));
            });

            $('.buttons_aspect_ratio label').click(function(){
                var as=$(this).find('input').val();
                console.log($(this).find('input').val());
                $('#image_crop').cropper('destroy');

                $('#image_crop').cropper({

                    aspectRatio: as,
                    viewMode:0,

                    build: function (e) {
                        var $clone = $(this).clone();
                        $clone.css({
                            display: 'block',
                            width: '100%',
                            minWidth: 0,
                            minHeight: 0,
                            maxWidth: 'none',
                            maxHeight: 'none'
                        });

                        $previews.css({
                            width: '100%',
                            overflow: 'hidden'
                        }).html($clone);
                    },

                    crop: function(e) {
                        // For preview
                        var imageData = $(this).cropper('getImageData');
                        var previewAspectRatio = e.width / e.height;

                        $previews.each(function () {
                            var $preview = $(this);
                            var previewWidth = $preview.width();
                            var previewHeight = previewWidth / previewAspectRatio;
                            var imageScaledRatio = e.width / previewWidth;

                            $preview.height(previewHeight).find('img').css({
                                width: imageData.naturalWidth / imageScaledRatio,
                                height: imageData.naturalHeight / imageScaledRatio,
                                marginLeft: -e.x / imageScaledRatio,
                                marginTop: -e.y / imageScaledRatio
                            });
                        });
                    }
                });

            });



            $('#store_blob').submit(function(e){

                e.preventDefault();

                var formData = new FormData();
                $('#image_crop').cropper('getCroppedCanvas').toBlob(function (blob2) {


                    formData.append('croppedImage', blob2);
                    var rota="{{route('gallery::backend::store_image_asset',array($item->id))}}";



                    $.ajax(rota, {
                        method: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (data) {

                            $('.err_mesages').empty();
                            $('.msg_sucesso_ajax').hide();
                            $('.msg_erro_ajax').hide();

                            if(data.errors.length != 0){
                                $('.msg_erro_ajax').fadeIn();

                                $.each(data.errors,function( index , el) {
                                    $('.err_mesages').append('<p>'+el+'</p>');
                                });
                            }else{
                                $('.msg_sucesso_ajax').fadeIn();
                            }


                        },
                        error: function (data) {

                        }
                    });
                });

                $(this).append('<input type="hidden" name="field_name" value="'+formData+'" /> ');
                return true;
            });

        });
    </script>

@endsection



@section('content')
    <div class="conteudo_central2">
        <div class="container-fluid">

            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{route('gallery::backend::backend.gallery.media-item.edit',array($item->id))}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>@lang('gallery::backend.model.edit_image.go_back')</a></li>
                    </ul>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <ul class="breadcrumb">
                        <li>@lang('gallery::backend.model.edit_image.gallery')</li>
                        <li>{{$item->title}}</li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  msgs_gallery">



                        <div class="alert alert-sucess alert-dismissible  msg_sucesso_ajax" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <i class="icon-permissoes icon9 pading_right" ></i>@lang('gallery::backend.messages.success.cuted_image')
                        </div>



                        <div class="alert alert-erro alert-dismissible msg_erro_ajax" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <i class="icon-opcoes-eliminar icon9  pading_right"></i>
                            <span class="err_mesages"></span>
                        </div>


                </div>
            </div>


            <form id="store_blob" method="POST" action="" class="fontes_form" autocomplete="off" enctype="multipart/form-data">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="col-lg-8">


                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <div class="">

                                    @if($item->type=='imagem')
                                        <img id="image_crop" src="{{route('gallery::backend::storage_image', array($item->identifier_token))}}">
                                    @endif
                                </div>

                            </div>

                        </div>

                    </div>
                    <div class="col-lg-4">
                        <div>
                            <p class="fonte_12_lato">@lang('gallery::backend.model.edit_image.preview')</p>
                        </div>
                        <div class="preview"></div>

                    </div>

                    <div class="col-lg-12 margem_20">
                        <div class="docs-buttons">
                            <div class="btn-group">
                                <button title="Arrastar" data-option="move" data-method="setDragMode"  class="btn btn-image-change" type="button">
                                    <span class="icon-mover"></span>
                                </button>
                                <button title="Crop" data-option="crop" data-method="setDragMode"  class="btn btn-image-change" type="button">
                                    <span class="icon-copiar"></span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button title="Zoom In" data-option="0.1" data-method="zoom" class="btn btn-image-change" type="button">
                                    <span class="icon-zoom-in"></span>
                                </button>
                                <button title="Zoom Out" data-option="-0.1" data-method="zoom" class="btn btn-image-change" type="button">
                                    <span class="icon-zoom-out"></span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button title="Move Left" data-second-option="0" data-option="-10" data-method="move" class="btn btn-image-change" type="button">
                                    <span class="icon-esquerda"></span>
                                </button>
                                <button title="Move Right" data-second-option="0" data-option="10" data-method="move" class="btn btn-image-change" type="button">
                                    <span class="icon-direita"></span>
                                </button>
                                <button title="Move Up" data-second-option="-10" data-option="0" data-method="move" class="btn btn-image-change" type="button">
                                    <span class="icon-cima"></span>
                                </button>
                                <button title="Move Down" data-second-option="10" data-option="0" data-method="move" class="btn btn-image-change" type="button">
                                    <span class="icon-baixo"></span>
                                </button>
                            </div>

                            <div class="btn-group">

                                <button title="Rotate Left" data-option="-45" data-method="rotate" class="btn btn-image-change" type="button">
                                    <span class="icon-rodar-esquerda"></span>
                                </button>
                                <button title="Rotate Right" data-option="45" data-method="rotate" class="btn btn-image-change" type="button">
                                    <span class="icon-rodar-direira"></span>
                                </button>
                            </div>

                            <div class="btn-group">
                                <button title="Flip Horizontal" data-option="-1" data-method="scaleX" class="btn btn-image-change" type="button">
                                    <span class="icon-flip-vertical"></span>
                                </button>
                                <button title="Flip Vertical" data-option="-1" data-method="scaleY" class="btn btn-image-change" type="button">
                                    <span class="icon-flip-horizontal"></span>
                                </button>
                            </div>


                            <div class="btn-group">
                                <button title="Reset" data-method="reset" class="btn btn-image-change" type="button">
                                    <span class="icon-recuperar"></span>
                                </button>
                            </div>





                            <div class="btn-group">
                                <button data-option="0" data-method="moveTo" class="btn btn-image-change" type="button">
                                <span title="" data-toggle="tooltip" class="docs-tooltip" data-original-title="cropper.moveTo(0)">
                                  0,0
                                </span>
                                </button>
                                <button data-option="1" data-method="zoomTo" class="btn btn-image-change" type="button">
                                <span title="" data-toggle="tooltip" class="docs-tooltip" data-original-title="cropper.zoomTo(1)">
                                  100%
                                </span>
                                </button>
                                <button data-option="180" data-method="rotateTo" class="btn btn-image-change" type="button">
                                <span title="" data-toggle="tooltip" class="docs-tooltip" data-original-title="cropper.rotateTo(180)">
                                  180°
                                </span>
                                </button>
                            </div>

                        </div>

                    </div>



                    <div class="col-lg-4  buttons_aspect_ratio">
                        <div data-toggle="buttons" class="btn-group btn-group-justified">

                            <label class="btn btn-image-change active">
                                <input type="radio" value="1.7777777777777777" name="aspectRatio" id="aspectRatio0" class="sr-only">
                                <span title="" data-toggle="tooltip" class="docs-tooltip" data-original-title="aspectRatio: 16 / 9">
                                  16:9
                                </span>
                            </label>
                            <label class="btn btn-image-change">
                                <input type="radio" value="1.3333333333333333" name="aspectRatio" id="aspectRatio1" class="sr-only">
                                <span title="" data-toggle="tooltip" class="docs-tooltip" data-original-title="aspectRatio: 4 / 3">
                                  4:3
                                </span>
                            </label>
                            <label class="btn btn-image-change">
                                <input type="radio" value="1" name="aspectRatio" id="aspectRatio2" class="sr-only">
                                <span title="" data-toggle="tooltip" class="docs-tooltip" data-original-title="aspectRatio: 1 / 1">
                                  1:1
                                </span>
                            </label>
                            <label class="btn btn-image-change">
                                <input type="radio" value="0.6666666666666666" name="aspectRatio" id="aspectRatio3" class="sr-only">
                                <span title="" data-toggle="tooltip" class="docs-tooltip" data-original-title="aspectRatio: 2 / 3">
                                  2:3
                                </span>
                            </label>
                            <label class="btn btn-image-change">
                                <input type="radio" value="NaN" name="aspectRatio" id="aspectRatio4" class="sr-only">
                                    <span title="" data-toggle="tooltip" class="docs-tooltip" data-original-title="aspectRatio: NaN">
                                      @lang('gallery::backend.model.edit_image.free')
                                    </span>
                            </label>
                        </div>

                    </div>

                    <div class="col-lg-12 margem_20 no_padding">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 ">

                                    <button type="submit" class="btn btn-default btn-yellow btn_upload_files btn_save_image_blob">@lang('gallery::backend.model.edit_image.save')</button>
                                    {{--<button class="btn btn-default btn-cancelar btn_upload_files">@lang('gallery::backend.model.edit_image.cancel')</button>--}}
                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </form>


        </div>
    </div>
@endsection




