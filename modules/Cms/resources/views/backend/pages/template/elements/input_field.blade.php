<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
	<div class="form-group fom-style-hidden3 @if($errors->first($input->key)) has-error @endif">
	    <label for="exampleInputEmail1">{{ $input->label }}</label>
	    <input type="text" class="form-control" name="{{ $input->key }}" value="{{ $input->value }}">
	    {!! $errors->first('.$input->key.','<span class="validator_errors">:message</span>')!!}
	</div>
</div>