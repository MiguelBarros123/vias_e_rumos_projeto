<?php

Route::group(['namespace' => 'Brainy\Permissions\Controllers\Backend', 'middleware' => \App\Http\Middleware\BackofficeLanguage::class], function () {

    Route::get('model', 'UserBackendController@index')->name('model.index');
    Route::get('model/create', 'UserBackendController@create')->name('model.create');
    Route::post('model', 'UserBackendController@store')->name('model.store');
    Route::get('model/{model}', 'UserBackendController@show')->name('model.show');
    Route::get('model/{model}/edit', 'UserBackendController@edit')->name('model.edit');
    Route::put('model/{model}', 'UserBackendController@update')->name('model.update');
    Route::delete('model/', 'UserBackendController@delete')->name('model.destroy');
    Route::delete('delete-multiple-users', 'UserBackendController@deleteMultipleUsers')->name('users.delete_multiple');
    Route::get('model/view/{model}', 'UserBackendController@showArchived')->name('model.show_archived');

    Route::get('/get-all-users-ajax', 'UserBackendController@allUsers')->name('model.users_ajax');
    Route::post('/change-user-state/{id}', 'UserBackendController@changeUserState')->name('model.change_user_state');


    Route::get('register/confirm/{token}', 'UserBackendController@confirmRegister')->name('model.registerConfirm');


    Route::get('/change-user-group-view/{id}', 'UserBackendController@changeUserGroup')->name('group.index_change_user_group');
    Route::post('/update-user-change-users-group-view/{id}', 'UserBackendController@updateUsersGroup')->name('group.update_users_group');
    Route::post('/recover-user-password', 'UserBackendController@recoverPassword')->name('model.recoverUserPassword');






    Route::get('group', 'GroupBackendController@index')->name('group.index');
    Route::get('group/create', 'GroupBackendController@create')->name('group.create');
    Route::post('group', 'GroupBackendController@store')->name('group.store');
    Route::get('group/{group}', 'GroupBackendController@show')->name('group.show');
    Route::get('group/{group}/edit', 'GroupBackendController@edit')->name('group.edit');
    Route::put('group/{group}', 'GroupBackendController@update')->name('group.update');
    Route::delete('group/{group}', 'GroupBackendController@delete')->name('group.destroy');
    Route::delete('delete-multiple-groups', 'GroupBackendController@deleteMultiple')->name('group.delete_multiple');
    Route::get('/group-change-users-group-view/{id?}/{group?}', 'GroupBackendController@changeUsersGroupView')->name('group_change_users_group_view');
    Route::post('/update-group-change-users-group-view/{id}', 'GroupBackendController@updateUsersGroupView')->name('update_users_group_view');
    Route::post('/update-new-users-for-group/{id}', 'GroupBackendController@updateNewUsersForGroup')->name('update_new_users_for_group');
    Route::get('/add-user-to-group/{id}', 'GroupBackendController@showAddUser')->name('add_user_to_group');
    Route::get('/add-new-user-to-group/{id}', 'GroupBackendController@addNewUserGroup')->name('add_new_user_to_group');
    Route::get('/get-all-users-except-group/{id}', 'GroupBackendController@getUsersForGroup')->name('group_not_users_ajax');



    Route::get('/get-all-grupos-ajax', array('as'=>'grupos_ajax','uses'=>'GroupBackendController@allGrupos'));
    Route::get('/get-all-users-grupo-ajax/{id}', array('as'=>'users_grupo_ajax','uses'=>'GroupBackendController@allUsersGrupo'));
    Route::get('/get-users-all-groups-ajax/{id}', array('as'=>'users_all_groups_ajax','uses'=>'GroupBackendController@usersAllGroupsAjax'));
    Route::get('/edit-permissions-grupo/{id}', array('as'=>'edit_permissions','uses'=>'GroupBackendController@allPermissionsGrupo'));
    Route::get('/group-change-user-group-view/{idUser}', array('as'=>'group_change_user_group_view','uses'=>'GroupBackendController@changeUserGroupView'));
    Route::put('/update-permissions-grupo/{id}', array('as'=>'update_permissions','uses'=>'GroupBackendController@updatePermissionsGrupo'));


    Route::get('/archived-users', array('as'=>'archived_users_index','uses'=>'UserBackendController@indexArchived'));
    Route::get('/get-all-archived-users-ajax', array('as'=>'users_archived_ajax','uses'=>'UserBackendController@allArchivedUsers'));
    Route::delete('/restore-archived-user', array('as'=>'restore_archived_users','uses'=>'UserBackendController@restoreArchivedUser'));


});
