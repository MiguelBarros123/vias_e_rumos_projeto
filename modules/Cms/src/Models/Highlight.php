<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:20
 */

namespace Brainy\Cms\Models;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class Highlight extends Model {

    use Translatable;

    public $fillable = [
    	'image',
    	'panel_name',
        'link',
        'hasPResources'
    ];

    public $translatedAttributes = [
    	'title',
    	'description',
        'subtitle',
        'name_link',
        'image_alt'
    ];

    public $translationModel = HighlightTranslation::class;

    public function links()
    {
        return $this->hasMany(HighlightLink::class);
    }
}
