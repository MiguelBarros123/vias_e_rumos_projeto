<?php

Route::group(['namespace' => 'Brainy\Profiles\Controllers\Backend'], function () {

    Route::get('profiles', 'ProfileBackendController@index')->name('profiles.index');
    Route::get('ajax-profiles', 'ProfileBackendController@index_ajax')->name('clients.index_ajax');


    Route::get('companies', 'ProfileBackendController@indexCompany')->name('companies.index');
    Route::get('ajax-copmpanies', 'ProfileBackendController@index_companies_ajax')->name('companies.index_ajax');

    Route::get('profile/create/{type}', 'ProfileBackendController@create')->name('profiles.create');
    Route::get('profile/view/{id}', 'ProfileBackendController@view')->name('profiles.view');
    Route::get('profile/edit/{id}/{type}', 'ProfileBackendController@edit')->name('profiles.edit');
    Route::post('profile/store', 'ProfileBackendController@store')->name('profiles.store');
    Route::put('profile/update/{id}', 'ProfileBackendController@update')->name('profiles.update');
    Route::get('get-persons-no-company/{id?}', 'ProfileBackendController@getPersonsNoCompany')->name('profiles.ajax.no.company');
    Route::get('get-profiles/{id?}', 'ProfileBackendController@getProfiles')->name('profiles.ajax.profiles');
    Route::get('get-companies', 'ProfileBackendController@getCompanies')->name('profiles.ajax.companies');
    Route::delete('delete-multiple-clients', 'ProfileBackendController@deleteMultipleClients')->name('profiles.delete_multiple_clients');
    Route::delete('delete-multiple-companies', 'ProfileBackendController@deleteMultipleCompanies')->name('profiles.delete_multiple_companies');
    Route::get('profile/get-group/{id}', 'ProfileBackendController@getGroup')->name('profiles.get_group');


    Route::get('profiles-groups', 'ProfileGroupBackendController@index')->name('profiles.group.index');
    Route::get('profile/group/view/{id}', 'ProfileGroupBackendController@viewGroup')->name('profiles.group.view');
    Route::get('ajax-profile-groups', 'ProfileGroupBackendController@index_ajax')->name('profiles.group.index_ajax');
    Route::get('profile/group/create', 'ProfileGroupBackendController@create')->name('profiles.group.create');
    Route::post('profile/group/store', 'ProfileGroupBackendController@store')->name('profiles.group.store');
    Route::get('profile/group/{id}', 'ProfileGroupBackendController@edit')->name('profiles.group.edit');
    Route::put('profile/group/update/{id}', 'ProfileGroupBackendController@update')->name('profiles.group.update');
    Route::get('get-persons-no-group/{id?}', 'ProfileGroupBackendController@getPersonsNoGroup')->name('profiles.ajax.no.group');
    Route::delete('delete-multiple-groups', 'ProfileGroupBackendController@deleteMultipleGroups')->name('profiles.delete_multiple_groups');

    Route::post('profiles-add-new-profile', 'ProfileGroupBackendController@newProfile')->name('profiles.group.add_new_profile');



    Route::delete('custom_fields/delete', 'CustomFieldController@delete')->name('backend.profiles.custom_fields.delete');
    Route::get('lists/custom_fields/ajax', 'CustomFieldController@index_ajax')->name('profiles.custom_fields.index_ajax');
    Route::get('lists/custom_fields/ajax-all/{id?}', 'CustomFieldController@index_custom_fields_ajax')->name('profiles.custom_fields.index_custom_fields_ajax');
    Route::resource("custom_fields","CustomFieldController");




});
