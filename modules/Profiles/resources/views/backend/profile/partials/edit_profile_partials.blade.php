<div class="row">
    <div  class="col-lg-12 margem_20">
        <label>@lang('profiles::backend.common.emails')</label>
    </div>

    <div  class="col-lg-6">

        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
            <label>@lang('profiles::backend.common.personal_emails')</label>
            <div>
                @php
                $personalEmail = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'personal_email')->get();
                @endphp

                @if($personalEmail->count())
                    @foreach($personalEmail as $email)
                        <div class="input_container margem_5">
                            <div class="input-group">

                                <input type="text" name="personal_emails[]" class="form-control " value="{{$email->contact}}" placeholder="@lang('profiles::backend.common.new_personal_emai')">
                                <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="input_container margem_5">
                        <div class="input-group">

                            <input type="text" name="personal_emails[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_personal_emai')">
                            <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                            {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                        </div>
                    </div>
                @endif

            </div>

            <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
        </div>
    </div>

    <div  class="col-lg-6">
        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
            <label>@lang('profiles::backend.common.company_emails')</label>
            <div>
                @php
                $companyEmail = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'company_email')->get();
                @endphp

                @if($companyEmail->count())
                    @foreach($companyEmail as $email)
                        <div class="input_container margem_5">
                            <div class="input-group">
                                <input type="text" name="company_emails[]" class="form-control " value="{{$email->contact}}" placeholder="@lang('profiles::backend.common.new_company_emails')">
                                <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="input_container margem_5">
                        <div class="input-group">
                            <input type="text" name="company_emails[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_company_emails')">
                            <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                            {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                        </div>
                    </div>
                @endif


            </div>
            <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
        </div>
    </div>
</div>
<div class="divider_line2"></div>

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="row">
            <div  class="col-lg-12 margem_20">
                <label>@lang('profiles::backend.common.phones')</label>
            </div>

            <div  class="col-lg-6">
                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                    <label>@lang('profiles::backend.common.personal_phone')</label>
                    <div>
                        @php
                        $personalPhone = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'personal_phone')->get();
                        @endphp

                        @if($personalPhone->count())
                            @foreach($personalPhone as $phone)
                                <div class="input_container margem_5">
                                    <div class="input-group">
                                        <input type="text" name="personal_phones[]" class="form-control " value="{{$phone->contact}}" placeholder="@lang('profiles::backend.common.new_personal_phone')">
                                        <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                        {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="input_container margem_5">
                                <div class="input-group">
                                    <input type="text" name="personal_phones[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_personal_phone')">
                                    <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                    {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                </div>
                            </div>
                        @endif

                    </div>
                    <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                </div>
            </div>

            <div  class="col-lg-6">
                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                    <label>@lang('profiles::backend.common.company_phone')</label>
                    <div>
                        @php
                        $companyPhone = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'company_phone')->get();
                        @endphp

                        @if($companyPhone->count())
                            @foreach($companyPhone as $phone)
                                <div class="input_container margem_5">
                                    <div class="input-group">
                                        <input type="text" name="company_phones[]" class="form-control " value="{{$phone->contact}}" placeholder="@lang('profiles::backend.common.new_company_phone')">
                                        <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                        {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="input_container margem_5">
                                <div class="input-group">
                                    <input type="text" name="company_phones[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_company_phone')">
                                    <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                    {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                </div>
                            </div>
                        @endif


                    </div>
                    <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
        <div class="row">
            <div  class="col-lg-12 margem_20">
                <label>@lang('profiles::backend.common.mobile_phones')</label>
            </div>

            <div  class="col-lg-6">
                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                    <label>@lang('profiles::backend.common.personal_phone')</label>
                    <div>
                        @php
                        $personalMobile = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'personal_mobile')->get();
                        @endphp

                        @if($personalMobile->count())
                            @foreach($personalMobile as $mobile)
                                <div class="input_container margem_5">
                                    <div class="input-group">
                                        <input type="text" name="personal_mobile[]" class="form-control " value="{{$mobile->contact}}" placeholder="@lang('profiles::backend.common.new_personal_mobile')">
                                        <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                        {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="input_container margem_5">
                                <div class="input-group">
                                    <input type="text" name="personal_mobile[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_personal_mobile')">
                                    <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                    {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                </div>
                            </div>
                        @endif

                    </div>
                    <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                </div>
            </div>

            <div  class="col-lg-6">
                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                    <label>@lang('profiles::backend.common.company_phone')</label>
                    <div>
                        @php
                        $companyMobile = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'company_mobile')->get();
                        @endphp

                        @if($companyMobile->count())
                            @foreach($companyMobile as $mobile)
                                <div class="input_container margem_5">
                                    <div class="input-group">
                                        <input type="text" name="company_mobile[]" class="form-control " value="{{$mobile->contact}}" placeholder="@lang('profiles::backend.common.new_company_mobile')">
                                        <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                        {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="input_container margem_5">
                                <div class="input-group">
                                    <input type="text" name="company_mobile[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_company_mobile')">
                                    <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                    {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                </div>
                            </div>
                        @endif


                    </div>
                    <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="divider_line2"></div>


<div class="row">
    <div  class="col-lg-12 margem_20">
        <label>@lang('profiles::backend.common.faxes')</label>
    </div>

    <div  class="col-lg-6">
        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
            <label>@lang('profiles::backend.common.personal_fax')</label>
            <div>
                @php
                $personalFax = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'personal_fax')->get();
                @endphp

                @if($personalFax->count())
                    @foreach($personalFax as $fax)
                        <div class="input_container margem_5">
                            <div class="input-group">
                                <input type="text" name="personal_faxes[]" class="form-control " value="{{$fax->contact}}" placeholder="@lang('profiles::backend.common.new_personal_fax')">
                                <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="input_container margem_5">
                        <div class="input-group">
                            <input type="text" name="personal_faxes[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_personal_fax')">
                            <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                            {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                        </div>
                    </div>
                @endif


            </div>
            <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
        </div>
    </div>

    <div  class="col-lg-6">
        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
            <label>@lang('profiles::backend.common.company_fax')</label>
            <div>
                @php
                $companyFax = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'company_fax')->get();
                @endphp

                @if($companyFax->count())
                    @foreach($companyFax as $fax)
                        <div class="input_container margem_5">
                            <div class="input-group">
                                <input type="text" name="company_faxes[]" class="form-control " value="{{$fax->contact}}" placeholder="@lang('profiles::backend.common.new_company_fax')">
                                <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="input_container margem_5">
                        <div class="input-group">
                            <input type="text" name="company_faxes[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_company_fax')">
                            <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                            {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                        </div>
                    </div>
                @endif

            </div>
            <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
        </div>
    </div>
</div>
<div class="divider_line2"></div>