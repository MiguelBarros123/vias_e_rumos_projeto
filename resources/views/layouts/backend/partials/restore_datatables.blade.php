<div class="btn-group pull-right">
    <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="icon-roda-dentada icon10"></i>
    </button>
    <ul class="dropdown-menu delete_modal_record_drop">
        <li><a data-target="#delete_resource" data-toggle="modal" data-table-id="{{$id}}" data-route="{{route($route)}}" class="row_options delete_modal_record"><i class="icon-opcoes-eliminar"></i> Restaurar selecionados</a></li>
    </ul>
</div>
<input type="hidden" name="records_to_delete" id="delete_records_{{$id}}"/>
<input type="hidden" name="impossible_records_to_delete" id="impossible_records_to_delete{{$id}}"/>
