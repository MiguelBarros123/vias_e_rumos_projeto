(function() {
	'use strict';

	angular.module('brainy.categories.backend', ['brainy.categories', 'ngJsTree', 'BootstrapToggler', 'ckeditor']);
})();