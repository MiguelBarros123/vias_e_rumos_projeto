@extends('layouts.frontend.app')


@section('title', $auction->title )
@if($auction->auctions_resources->first())
    @section('image', route('gallery::frontend::storage_image',[$auction->auctions_resources->first()->identifier_token]))
@else
    @section('image', asset('front/imgs/lotes/placeholder.jpg'))
@endif

@section('description', $auction->executed )

@section('content')
@include('cms::frontend.pages.layouts.search-bar')

<?php
function formatMoney($number, $fractional=false) { 
    if ($fractional) { 
        $number = sprintf('%.2f', $number); 
    } 
    while (true) { 
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number); 
        if ($replaced != $number) { 
            $number = $replaced; 
        } else { 
            break; 
        } 
    } 
    return $number; 
} 
?>

<div class="vendas-prods">
    
    <div class="top-leilao-list">
        <div class="prod-title">
            <h3>{{ $auction->executed}}</h3>
            <div class="ref-top">
                Referência {{$auction->ref_intern}}
            </div>
        </div>
        <div class="prod-top-wrap">
            <div class="prod-data">
                <div class="prod-dad">
                    <div class="info-title">
                        Data:
                    </div>
                    <div class="info-value">
                        @if($auction->date)
                            {{str_replace(':','h',substr(\Carbon\Carbon::parse($auction->date)->toDateTimeString(),0,-3))}}
                        @else
                            Sem Data
                        @endif         
                    </div>
                </div>
                
                <div class="prod-dad">
                    <div class="info-title">
                        Local:
                    </div>
                    <div class="info-value">
                        {{$auction->local}}
                    </div>
                </div>

            </div>
            <div class="prod-files">
                <div class="files-share">
                    <ul class="files-share-list">
                        <!-- PODE NAO TER MAS POE SE TIVER -->
                        @if($auction->advertisement)
                        <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->advertisement])  }}"><span class="icon-anuncio file-icon "></span><span>Anúncio</span></a></li>
                        @endif

                        @if($auction->minuta)
                        <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->minuta])  }}"><span class="icon-minuta file-icon"></span><span>Minuta</span></a></li>
                        @endif

                        @if($auction->conditions)
                        <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->conditions])  }}"><span class="icon-condicoes file-icon"></span><span>Condições</span></a></li>
                        @endif

                        @if($auction->catalog)
                        <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->catalog])  }}"><span class="icon-catalog file-icon"></span><span>Catálogo</span></a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </div>

    </div>
    
    @if(count($auction->lots()) > 0)
    @foreach($auction->lots()->get() as $key => $auction_resource)

    <div class="each_prod">
        <div class="row prod-row">
            <div class="top-prod-list">
                <div class="prod-title">
                    <h3>{{$auction_resource->designacao}}</h3>
                </div>
            </div>
            <div class="participar">
                <a href="{{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                    <button>
                        @if(auth()->user() && $auction->id == 4)
                            Participar
                        @else
                            Ver +
                        @endif
                    </button>
                </a>
                <div class="prod-share hidden-md hidden-lg">
                    <div class="share-word">
                        partilha
                    </div>
                    <div class="share-icons">
                        <ul class="icons-list">
                            <li>
                                <a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                                    <span class="icon-facebook"></span>
                                </a>
                            </li>
                            <li>
                                <a href="https://plus.google.com/share?url={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span class="icon-google-plus"></span></a>
                            </li>
                            <li>
                                <a href="http://twitter.com/share?text={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a>
                            </li>
                            <li>
                                <a href="mailto:?subject={{$auction_resource->name}} - {{$auction_resource->designacao}} &body={{$auction_resource->description}} Link: {{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}"><span class="icon-mail"></span></a>
                                </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="displ-flex">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 flex-col">
                    <div class="row hidden-md hidden-lg">
                        <div class="col-xs-12 col-sm-12 text-area-wrap img-wrap">
                            @if($auction_resource->lots_resources()->first())
                                <div class="cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image_p',[$auction_resource->lots_resources()->first()->identifier_token,280,140])  }}')">
                                     <a href="{{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                                        <div class="more-prod-bg">
                
                                        </div>
                                        <div class="more-prod-text">
                                            Saber mais
                                        </div>
                                    </a>
                                </div>
                            @else
                                <div class="cover prod-img-list"  style="background-image: url('{{asset('front/imgs/lotes/placeholder.jpg')}}')">
                                     <a href="{{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                                        <div class="more-prod-bg">
                
                                        </div>
                                        <div class="more-prod-text">
                                            Saber mais
                                        </div>
                                    </a>
                                </div>    
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">
                                    <span class="mobile-title hidden-md hidden-lg">Descrição:</span>
                                    <!-- <div class="text-prod">Nome do lote</div>  -->


                                    </div>
                                    <div class="text-prod text-left">
                                        @if(strlen($auction_resource->description)>100)
                                         {{ mb_substr(html_entity_decode($auction_resource->description, null, 'ISO-8859-1'),0,100)}}...

                                        @else
                                            {{$auction_resource->description}}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <span class="mobile-title hidden-md hidden-lg">Local:</span>
                                    <div class="text-prod">
                                        <a href="http://www.google.com/maps/place/{{$auction_resource->latitude}},{{$auction_resource->longitude}}" target="_blank">{{ $auction_resource->local}}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                            <div class="text-area valor-area">
                                <div class="text-wrap">
                                    <div class="text-prod">
                                        

                                        @if($auction_resource->price)
                                            <div class="val_div">
                                                <b>Valor Base:</b> {{ formatMoney( $auction_resource->price/100, true)}}
                                            </div>
                                        @endif    
                                        @if($auction_resource->min_price)
                                            <div class="val_div">
                                                <?php setlocale(LC_MONETARY, 'en_US.UTF-8') ?>
                                                <b>Valor Mínimo:</b> {{ formatMoney( $auction_resource->min_price/100, true)}}
                                            </div>
                                        @endif
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row displ-flex bot-prod-row">
                        <div class="col-lg-8 col-md-8 pad-5">
                            <div class="infos-wrap">
                                <div class="info">
                                    {{ $auction_resource->comments}}
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 pad-5 hidden-xs hidden-sm">
                            <div class="files-share">
                                <ul class="files-share-list">
                                    <li class="share-li">
                                        <div class="prod-share">
                                            <div class="share-word">
                                                partilha
                                            </div>
                                            <div class="share-icons">
                                                <ul class="icons-list">
                                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}"><span class="icon-facebook"></span></a></li>
                                                       <li><a href="https://plus.google.com/share?url={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span class="icon-google-plus"></span></a></li>
                                                      <li><a href="http://twitter.com/share?text={{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a></li>

                                                    <li><a href="mailto:?subject={{$auction_resource->name}} - {{$auction_resource->designacao}} &body={{$auction_resource->description}} Link: {{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}" ><span class="icon-mail"></span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-area-wrap img-wrap hidden-xs hidden-sm">
                    <!-- <div class=" cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image',[$auction_resource->first()->identifier_token])  }}');"> -->
                    @if($auction_resource->lots_resources()->first())
                        <div class=" cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image_p',[$auction_resource->lots_resources()->first()->identifier_token,280,140])  }}');">
                             <a href="{{ route('cms::frontend::model.lotes_detalhes', $auction_resource->id) }}">
                                <div class="more-prod-bg">

                                </div>
                                <div class="more-prod-text">
                                    Saber mais
                                </div>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="line-separator">
    </div>
    @endforeach
    @endif
</div>  
@endsection

@section('module-scripts')
    <script>

        function contadorleilao(intervalo, tag, leilao_fim =false) {

            var distance = intervalo;

            if (distance > 0)
            {
              //$('#'+tag+'_terminado').css('display', 'none');
              $('#'+tag+'_time').css('display', 'block');
              
            }

            // Update contdown
            var timer = setInterval(function() {

                // Data e hora currente

                distance--;

                var dias = Math.floor(distance / (3600 * 24));
                var horas = Math.floor((distance - (dias * (3600 * 24)))/3600);
                var minutos = Math.floor((distance - (dias * (3600 * 24)) - (horas * 3600)) / 60);
                var segundos = Math.floor(distance - (dias * (3600 * 24)) - (horas * 3600) - (minutos * 60));

                if(horas<10){
                    horas="0"+horas;
                }
                if(minutos<10){
                    minutos="0"+minutos;
                }
                if(segundos<10){
                    segundos="0"+segundos;

                }


                //escreve no respetivo elemento
                $('#'+tag+'_dias').text(dias);
                $('#'+tag+'_thetime').text(horas + ":"+minutos+":"+segundos);

                if(dias == 0)
                {

                    $('#'+tag+'_dias_wrap').css('display','none');
                  
                }
               
            }, 1000);
        }

         contadorleilao("{{\Carbon\Carbon::now()->diffInSeconds(\Carbon\Carbon::parse($auction->date))}}", "{{$auction->id}}", false);
    </script>
@endsection
