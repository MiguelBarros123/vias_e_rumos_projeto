@extends('layouts.frontend.app')

@section('content')
<div class="mar-top-5">
    @include('cms::frontend.pages.layouts.search-bar')
</div>
<div class="empresa-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		
		<h2>POLÍTICAS DE UTILIZAÇÃO E SEGURANÇA</h2>

		<h2>1.	GERAL</h2>

		<p>Mediante a utilização do website www.viaserumos.pt, o utilizador declara, sem necessidade posterior de qualquer ato ou consentimento, que compreende e aceita as condições descritas nas Políticas de Utilização e Segurança. Estes termos e condições podem ser alterados a qualquer momento pela Vias & Rumos, considerando-se que as mesmas entram em vigor a partir da data de publicação no website.</p>

<p>O acesso, e subsequente utilização do website, é considerado sinal inequívoco de que o utilizador leu, compreendeu e aceitou as condições aqui descritas.</p>

<p>Estas Políticas de Utilização e Segurança são condições adicionais às específicas de cada venda promovida no website.</p>

<p>O utilizador compromete-se a:</p>
<p>2.1.	cumprir estas Políticas de Utilização e Segurança;</p>
<p>2.2.	manter seguros o dispositivo físico de acesso ao website e os dados de acesso às contas pessoais detidas;</p>
<p>2.3.	não partilhar os dados de acesso às contas pessoais com terceiros;</p>
<p>2.4.	responsabilizar-se por qualquer utilização indevida da sua conta na participação das vendas promovidas no website quando tal utilização foi permitida por violação dos pontos anteriores;</p>
<p>2.5.	não perturbar o acesso ao website www.viaserumos.pt, nem impedir ou limitar o acesso dos restantes utilizadores ao website e suas funcionalidades, quaisquer que sejam os meios aplicados;</p>
<p>2.6.	não infringir o direito à privacidade, marca registada, segredo comercial, direito de autor ou direitos de propriedade intelectual da Vias & Rumos, dos seus parceiros ou qualquer visado pelos processos judiciais atribuídos e publicitados neste website;</p>
<p>2.7.	não assumir a identidade de qualquer pessoa ou entidade incluindo, mas não limitado a um representante da Vias & Rumos, ou dos seus parceiros, um outro utilizador ou cliente, ou proferir informações falsas ou de outra forma mentir sobre a sua ligação a uma pessoa ou entidade;</p>

<p>O não cumprimento destas obrigações confere à Vias & Rumos o direito bloquear o acesso ao seu website sem direito a indemnização e sem prejuízo de posteriores ações legais cíveis contra o infrator.</p>

<p>De acordo com as limitações impostas pela legislação e jurisdição aplicável, a Vias & Rumos não poderá ser responsabilizada por quaisquer prejuízos ou danos em sede de responsabilidade civil, incluindo, mas não limitado a, danos emergentes, lucros cessantes e danos morais, causados de forma direta ou indireta, em consequência da utilização correta ou incorreta do website, das suas funcionalidades e conteúdos, pelo utilizador, do acesso ao computador e sistema informático do utilizador por terceiros, vírus, trojans, spyware, malware, etc.</p>

<p>É expressamente proibida a utilização do website, das suas funcionalidades e conteúdo, para quaisquer outros fins que possam ser considerados, ilegais, indignos ou prejudiciais da imagem da Vias & Rumos. Qualquer acesso suspeito de ilícito será comunicado às autoridades competentes, não sendo excluídas outras ações legais disponíveis à Vias & Rumos.</p>

<h2>3.	INFORMAÇÃO DISPONÍVEL</h2>

<P>A Vias & Rumos empenha todos os seus esforços na segurança do site e na disponibilização de correta informação, utilizando os meios técnicos, físicos que garantam proteção adequada. No entanto, face aos inúmeros vetores de acesso e elementos não controlados diretamente pela Vias & Rumos, não é possível certificar, não obstante os meios empregues, que os ficheiros disponíveis para download no site estejam livres de vírus, worms, trojans, scripts e/ou qualquer outro código ou programa de computador que contenha ações destrutivas e/ou prejudiciais.</P>

<p>Mesmo tendo sido feitos todos os esforços para que a informação contida no website esteja correta, a mesma poderá ficar desatualizada, conter erros de descrição e de tipografia; sendo que a Vias & Rumos reserva o direito de alterar ou corrigir a informação a qualquer momento, sem que para isso seja obrigada notificar previamente os utilizadores do website.</p>

<h2>4.	DIREITOS DE AUTOR</h2>

<p>Os conteúdos apresentados neste website estão protegidos por Direitos de Autor e Direitos Conexos, Direitos da Propriedade Industrial, e restante legislação aplicável. São detidos na totalidade pela Vias & Rumos sendo expressamente interdita qualquer cópia, reprodução, difusão, transmissão, venda, publicação, modificação, distribuição ou qualquer outro uso, total ou parcial, quaisquer que sejam os meios utilizados, sem que tal tenha sido autorizado pela Vias & Rumos. </p>

<p>Exceptua-se da interdição infa os usos devidamente autorizados por lei, especificamente o direito a citação quando claramente identificada a sua origem.</p>

<p>Pela área de atuação da Vias & Rumos, ou dos seus parceiros, o website poderá conter imagens, textos ou demais informações cujos direitos são detidos por terceiros (sejam exemplo marcas de veículos, nomes de empresas, fotografias ou logótipos). Estes dados são usados em estrito cumprimento dos propósitos do website na promoção das vendas disponíveis e em conformidade com a legislação aplicável. Usos indevidos poderão ser comunicados à Vias & Rumos, pessoalmente ou através dos contactos disponíveis no website.</p>

<h2>5.	POLÍTICA DE PRIVACIDADE</h2>

<p>O acesso ao site e a subscrição das suas funcionalidades requer o registo de informações pessoais dos seus utilizadores. São obrigatórias apenas as informações estritamente necessárias para a prestação dos serviços e funcionalidades do site.<p>

<p>A Vias & Rumos, está fortemente empenhada no respeito pela privacidade dos seus utilizadores e clientes não partilhando, vendendo ou alugando as informações pessoais a terceiros, e empregando medidas de controlo e validação nos casos onde o acesso aos dados dos utilizadores possa implicar acesso a dados pessoais identificadores.</p>

<p>5.1.	INFORMAÇÕES PESSOAIS RECOLHIDAS </p>

<p>São recolhidas informações pessoais que permitam a validação de identidade, possibilitando a segurança a todos os envolvidos nas vendas promovidas pelo site, informações essas que serão usadas para efeitos de contabilidade no caso de participação em vendas. São registados, nomeadamente, e consoante o tipo de utilizador: o nome ou designação social; NIF ou NIPC; morada; endereço de correio eletrónico; telefone e/ou telemóvel. </p>

<p>Automaticamente, e para garantir o acesso a algumas funcionalidades, são registados cookies que poderão guardar informação sobre o seu navegador de Internet e sobre o seu dispositivo de acesso.</p>

<p>5.2.	CONSERVAÇÃO E ACESSO À INFORMAÇÃO PESSOAL</p>

<p>Nos termos do previsto na Lei n.º 67/98, de 26 de Outubro, é permitido aos titulares dos dados pessoais o direito ao acesso livre, não restrito, de modo a que possam confirmar, retificar, apagar ou bloquear os dados que tenham facultado, podendo exercer esse direito pessoalmente ou por escrito através da morada da Vias & Rumos disponível neste website, em qualquer altura e sem qualquer encargo.</p>

<p>O website permite também, através de autenticação e área pessoal, a edição de alguns dos dados pessoais registados.</p>



        </div>
    </div>
</div>
@endsection
