@extends('layouts.frontend.app')

@section('title', 'frontend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/gallery/frontend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/gallery/frontend.js')}}"></script>
@endsection

@section('content')
<p>@lang('gallery::frontend.model.index.welcome')</p>
@endsection
