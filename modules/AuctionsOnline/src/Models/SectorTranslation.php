<?php

namespace Brainy\Profiles\Models;

use Illuminate\Database\Eloquent\Model;

class SectorTranslation extends Model
{
    protected $fillable = ['name'];
}
