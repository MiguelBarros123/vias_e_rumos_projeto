<div id="menu-squares" class="hidden-xs hidden-sm">
    <div class="squares-wrap">
        <div class="cover square-el menu-short" style="background-image: url('{{ asset('front/imgs/img1.jpg') }}')">
            <a href="{{ route('cms::frontend::model.empresa') }}" class="link_sq">
            </a>
            <div class="overlay-full">
            </div>
            <div class="square-text">

                <div class="sec-title">
                    INSTITUCIONAL
                </div>
            
            </div>
        </div>
        <div class="cover square-el menu-short" style="background-image: url('{{ asset('front/imgs/img2.jpg') }}')">
            <a href="{{ route('cms::frontend::model.vendas') }}" class="link_sq">
            </a>
            <div class="overlay-full">
            </div>
            <div class="square-text">

                <div class="sec-title">
                    VENDAS
                </div>
            
            </div>
        </div>
        <div class="cover square-el menu-short" style="background-image: url('{{ asset('front/imgs/img3.jpg') }}')">
            <a href="{{ route('cms::frontend::model.biblioteca') }}" class="link_sq">
            </a>
            <div class="overlay-full">
            </div>
            <div class="square-text">

                <div class="sec-title">
                    BIBLIOTECA
                </div>
            
            </div>
        </div>
        <div class="cover square-el menu-short" style="background-image: url('{{ asset('front/imgs/img4.jpg') }}')">
            <a href="{{ route('cms::frontend::model.contacts')}}" class="link_sq">
            </a>
            <div class="overlay-full">
            </div>
            <div class="square-text">

                <div class="sec-title">
                    CONTACTOS
                </div>
            
            </div>
        </div>
    </div>

    <div class="squares-wrap submenu">
        @foreach(\Brainy\DefinitionsRent\Models\Category::all() as $key => $category)
        <?php $key_i = $key +1 ?>
        <div class="cover square-el" style="background-image: url('{{ asset('front/imgs/'.$key_i.'.jpg') }}')">
            <a href="{{route('cms::frontend::model.vendas', $category->id)}}" class="link_sq">
            </a>
            <div class="square-text">
                <div class="sec-title">
                    {{ $category->name }}
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
