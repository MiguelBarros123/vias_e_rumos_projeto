(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol ? "symbol" : typeof obj; };

/**
 * Created by User on 13-05-2016.
 */
(function ($) {
    "use strict";

    var CoustomDatatables = function CoustomDatatables(element, options) {
        this.$el = $(element);
        this.options = options || {};
        this.init();
    };

    CoustomDatatables.prototype.init = function () {

        var id = this.$el.attr('id');
        var rota = this.options.rota;
        var colunas = this.options.colunas;
        var dom = this.options.dom;
        var rowReorder = this.options.rowReorder;
        if(typeof(rowReorder)!='undefined'){
            var reorder = this.options.rowReorder.selector;
        }
        var pagingType = this.options.pagingType;
        var search = this.options.searchLabel;


        if(typeof(dom)=='undefined'){
            dom = '<<t>ilp>r';
        }

        var metodo = this.options.metodo;
        var dados = this.options.dados;

        var dataRows = [];
        var users = [];


        this.options.config = this.options.config || {};

        this.options.config.stateSave = true;
        this.options.config.processing = true;
        this.options.config.select = {
            style: 'multi',
            selector: 'td:last-child'
        };

        this.options.config.responsive = true;
        this.options.config.dom = dom;
        this.options.config.language = {
            "sProcessing": "A processar...",
            "sLengthMenu": "_MENU_ entradas",
            "sZeroRecords": "Não foram encontrados resultados",
            "sInfo": "_START_ a _END_ de _TOTAL_ resultados",
            "sInfoEmpty": "0 a 0 de 0",
            "sInfoFiltered": "(filtrado de _MAX_ entradas no total)",
            "sSearch": "Procurar:",
            "oPaginate": {
                "sFirst": "Primeiro",
                "sPrevious": "Anterior",
                "sNext": "Seguinte",
                "sLast": "Último"
            },
            select: {
                rows: ""
            }
        };

        this.options.config.columns = colunas;
        this.options.id_of_table = id;

        if(typeof(reorder)!='undefined'){
            this.options.rowReorder = {
                selector: reorder
            };
        }

        // if(typeof(metodo)!='undefined'){
        //     this.options.config.ajax.method = metodo;
        // }
        //
        // if(typeof(dados)!='undefined'){
        //     this.options.config.ajax.data = function (d) {
        //         dados
        //     };
        // }

        if(typeof(metodo)!='undefined' && typeof(dados)!='undefined') {
            this.options.config.ajax = {
                "url": rota,
                "method": metodo,
                "data": dados,
                "headers": {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            };
            this.options.config.serverSide = true;
        } else {
            this.options.config.ajax = rota;
        }

        if(typeof(pagingType)!='undefined'){
            this.options.config.pagingType = pagingType;
        }

        if(typeof(search)!='undefined'){
            // this.options.config.language = {
            //     "sSearch": ""+search+""
            // };

            this.options.config.language.sSearch = search;
        }




        this.options.datatable = $('#' + id).DataTable(this.options.config);

        var datatable = this.options.datatable;

            $('#sel_all_perm_'+id).prop('checked', false);



            this.options.searchableElementId = this.options.searchableElementId || 'search_icon';


            var search = this.options.searchableElementId[0] == '#' ? this.options.searchableElementId : '#' + this.options.searchableElementId;



            $(search).on("keyup search input paste cut", function () {
                datatable.search(this.value).draw();
            });
    };

    // -------------------PLUGIN DEFINITION-----------------------------------
    var old = $.fn.mydatatable;

    $.fn.mydatatable = function (options) {
        var args = arguments;

        return this.each(function () {
            var $this = $(this);
            var data = $this.data('oc.mydatatable');
            $this.data('oc.mydatatable', data = new CoustomDatatables(this, options));
        });
    };

    $.fn.mydatatable.Constructor = CoustomDatatables;

    $.fn.mydatatable.noConflict = function () {
        $.fn.mydatatable = old;
        return this;
    };
})(window.jQuery);

},{}]},{},[1]);

//# sourceMappingURL=datatables.js.map