<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaItemTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_item_translations', function (Blueprint $table) {

            $table->engine='InnoDB';

            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('media_item_id')->unsigned();

            $table->string('title');
            $table->text('description');
            $table->string('slug');

            $table->unique(['media_item_id','locale']);
            $table->foreign('media_item_id')->references('id')->on('media_items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('media_item_translations');
    }
}
