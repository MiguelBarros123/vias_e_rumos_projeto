$('.accommodation-slider').slick({
    'prevArrow': '<span class="font-icon-setarecta accommodation-prev"></span>',
    'nextArrow': '<span class="font-icon-setarecta accommodation-next"></span>'
});

$(document).ready(function(){

    $(window).on('scroll', function(){
        var top = $(window).scrollTop();

        if(top > 0){
            $('.header-menu').addClass('sections-menu');
            $('.header-menu .top_menu .menu-logo-home').addClass('menu-hidden');
            $('.header-menu .top_menu .menu-logo').removeClass('menu-hidden');
        } else {
            $('.header-menu').removeClass('sections-menu');
            $('.header-menu .top_menu .menu-logo-home').removeClass('menu-hidden');
            $('.header-menu .top_menu .menu-logo').addClass('menu-hidden');
        }
    });

    $('#reservations_modal').on('click', function(){
        $('.modal-reservations').fadeIn();
    });

    $('#close-modal-reservations').on('click', function(){
        $('.modal-reservations').fadeOut();
    });

});
//# sourceMappingURL=frontend_accommodation.js.map
