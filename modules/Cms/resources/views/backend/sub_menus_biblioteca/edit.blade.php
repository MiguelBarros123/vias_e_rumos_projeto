@extends('layouts.backend.app')

@section('title','Criacao Submenu Biblioteca')

@section('module-styles')
<link rel="stylesheet" href="{{asset('css/rent/backend.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('css/gallery/backend.css')}}">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
@endsection

@section('module-scripts')
<script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>
<script>
    CKEDITOR.replace( 'description');
</script>
@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">

                        <li><a href="{{route('cms::backend::backend.cms.sub_menus_biblioteca.index')}}" class="icon_voltar"><i
                                        class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
        @include('layouts.backend.messages')
        {!! DaveJamesMiller\Breadcrumbs\Facade::render('cms.sub_menus_biblioteca.create') !!}
        <div class="row">
            <form method="POST" action="{{route('cms::backend::backend.cms.sub_menus_biblioteca.update', $sub_menu->id) }}" autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('PUT')}}

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20"></i>Sub Menu Biblioteca
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">
                                    <div class="form-group @if($errors->has('title')) has-error @endif">
                                        <label for="title-field">Nome</label>
                                        <input type="text" name="title" id="title" class="form-control" value="{{ $sub_menu->name }}">
                                        @if($errors->has("title"))
                                            <span class="help-block">{{ $errors->first('title') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">
                                    <div class="form-group @if($errors->has('description')) has-error @endif">
                                        <label for="description-field">@lang('rent::backend.common.description')</label>
                                        <textarea class="form-control" id="description-field" rows="3" name="description">{!! $sub_menu->description !!}</textarea>
                                        @if($errors->has("description"))
                                            <span class="help-block">{{ $errors->first("description") }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">
                                        <div class="form-group">
                                            <label for="category-field">Menu Biblioteca</label>
                                            <div id="menu_id">
                                                @include('layouts.backend.partials.select_object_input_menus', ['array'=> $menus, 'name'=>'menu_id', 'selected'=> $sub_menu->menu_biblioteca_id, 'class'=> 'menu_biblioteca', 'id' => 0])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                        <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('cms::backend.model.common.save')</button>
                        <button type="submit" name="submit" value="stay" class="btn btn-default btn-yellow btn-stay btn-submit-all">@lang('cms::backend.model.common.save_and_stay')</button>
                        <a class="btn btn-default btn-cancelar"
                           href="{{route('cms::backend::backend.cms.sub_menus_biblioteca.index')}}">@lang('cms::backend.model.common.cancel')</a>
                    </div>
            </form>
        </div>
    </div>
</div>
@endsection
