@extends('layouts.backend.app')
@inject('folders', 'Brainy\Gallery\Models\MediaFolder')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" href="{{asset('css/profiles/backend.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.css">
@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.js"></script>

@endsection
@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">

            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <a class="pull-right icon_eliminar icon-editar pading_right icon_delete_inside_view_show" href="{{ route('profiles::backend::backend.profiles.custom_fields.edit', $custom_field->id) }}"> Editar</a>
                    <ul class="list-inline">

                        <li><a href="{{route('profiles::backend::backend.profiles.custom_fields.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>

                </div>

            </div>


            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.custom_fields.show',$custom_field) !!}
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo">@lang('profiles::backend.menu.personalized_fields')</span>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div  class="col-lg-3 margem_20">

                                    <div class="form-group">
                                        <label for="name">@lang('profiles::backend.common.name')</label>
                                        <p class="form-control-static">{{$custom_field->name}}</p>
                                    </div>

                                </div>
                                <div  class="col-lg-3 margem_20">


                                    <div class="form-group">
                                        <label for="type">@lang('profiles::backend.common.type')</label>
                                        @if($custom_field->type=='single')<p class="form-control-static">@lang('profiles::backend.common.text_field')</p>@endif
                                        @if($custom_field->type=='multiple')<p class="form-control-static">@lang('profiles::backend.common.multiple')</p>@endif
                                    </div>

                                </div>
                                <div  class="col-lg-6 margem_20">

                                    <div class="form-group">
                                        <div><label for="type">@lang('profiles::backend.common.options')</label></div>
                                        @if($custom_field->type=='single')
                                            <div>N/D</div>
                                            @else
                                        @foreach($custom_field->custom_options as $option)
                                            <span class="item_yellow">{{$option->name}}</span>
                                        @endforeach
                                            @endif
                                    </div>



                                </div>

                            </div>


                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>



@endsection
