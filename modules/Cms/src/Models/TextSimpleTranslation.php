<?php

namespace Brainy\Cms\Models;
use Illuminate\Database\Eloquent\Model;

class TextSimpleTranslation extends Model {
    public $timestamps = false;
    
    protected $table = 'text_simple_translations';

    protected $fillable = ['description'];
}
