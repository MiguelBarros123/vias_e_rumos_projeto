<div class="row tabs-back top_menu_page">
    <div class="col-lg-6 col-md-8 col-sm-6 col-xs-6 no_padding">


        <ul class="list-inline fonte_12_lato">

            <li class="@activePageTopMenu('profiles::backend::profiles.index')">
                <a class="icon_voltar" href="{{route('profiles::backend::profiles.index')}}" > @lang('profiles::backend.menu.clients') </a>
            </li>
            <li class="@activePageTopMenu('profiles::backend::companies.index')">
                <a class="icon_voltar" href="{{route('profiles::backend::companies.index')}}" > @lang('profiles::backend.menu.companys') </a>
            </li>

            <li class="@activePageTopMenu('profiles::backend::profiles.group.index')">
                <a class="icon_voltar" href="{{route('profiles::backend::profiles.group.index')}}" > @lang('profiles::backend.menu.groups') </a>
            </li>

            <li class="@activePageTopMenu('profiles::backend::backend.profiles.custom_fields.index')">
                <a class="icon_voltar" href="{{route('profiles::backend::backend.profiles.custom_fields.index')}}" > @lang('profiles::backend.menu.personalized_fields') </a>
            </li>

        </ul>
    </div>





    <div class="col-lg-6 col-md-4 col-sm-6 col-xs-6 text-right tabs-back no_padding">
        <ul class="list-inline ">

            @if(Route::is('profiles::backend::profiles.index'))
                <li>
                    <a href="{{route('profiles::backend::profiles.create',['person'])}}" class="icon_eliminar" ><i class="icon-adicionar pading_right"></i>@lang('profiles::backend.menu.create_new_person')</a>
                </li>
            @endif
            @if(Route::is('profiles::backend::companies.index'))
                    <li><a class="icon_eliminar" href="{{route('profiles::backend::profiles.create',['company'])}}"><i class="icon-adicionar pading_right"></i>@lang('profiles::backend.menu.create_new_company')</a></li>
                @endif

                @if(Route::is('profiles::backend::profiles.group.index'))
                    <li><a class="icon_eliminar" href="{{route('profiles::backend::profiles.group.create')}}"><i class="icon-adicionar pading_right"></i>@lang('profiles::backend.menu.create_new_group')</a></li>
                @endif
        </ul>
    </div>

</div>