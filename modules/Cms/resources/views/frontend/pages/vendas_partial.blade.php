@foreach($auctions as $auction)
            <script>
                contadorleilao("{{\Carbon\Carbon::now()->diffInSeconds(\Carbon\Carbon::parse($auction->date))}}", "{{$auction->id}}", false);
            </script>
    <div class="each_prod">

        <div class="row prod-row">
            <div class="top-prod-list">
                <div class="prod-title">
                    <h3>{{ \Brainy\DefinitionsRent\Models\Category::where('id', $auction->category_id)->first()->name}}</h3>
                </div>
                <div class="time-left {{$auction->id}}_time">
                    @if($auction->date)
                        <span id="{{$auction->id}}_dias_wrap" class="">
                            <span id="{{$auction->id}}_dias" class="">{{ \Carbon\Carbon::now()->diffInDays(\Carbon\Carbon::parse($auction->date))}}</span> dias
                        </span>
                        <span id="{{$auction->id}}_thetime" class="">{{ gmdate('H:i:s',\Carbon\Carbon::now()->diffInSeconds(\Carbon\Carbon::parse($auction->date)))}}</span>
                    @endif            
                </div>
            </div>

            <div class="participar">
                <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                    <button>
                        Ver +
                    </button>
                </a>
                <div class="prod-share hidden-md hidden-lg">
                    <div class="share-word">
                        partilha
                    </div>
                    <div class="share-icons">
                        <ul class="icons-list">
                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::model.leilao', $auction->id)}}" target="_blank"><span class="icon-facebook"></span></a></li>
                               <li><a href="https://plus.google.com/share?url={{ route('cms::frontend::model.leilao', $auction->id)}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span class="icon-google-plus"></span></a></li>
                                <li><a href="http://twitter.com/share?text={{$auction->name}}&url={{route('cms::frontend::model.leilao', $auction->id)}}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a></li>
                            <li><a href="mailto:?subject={{$auction->title}}&body={{$auction->description}}Link: {{ route('cms::frontend::model.leilao', $auction->id)}} "><img src="{{ asset('front/imgs/icons/mail.svg') }}"/></a></li>
                            
                        </ul>
                    </div>
                </div>
            </div>
            <div class="displ-flex">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 flex-col">
                    <div class="row hidden-md hidden-lg">
                        <div class="col-xs-12 col-sm-12 text-area-wrap img-wrap">
                            @if($auction->auctions_resources()->first())
                            <div class="cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image_p',[$auction->auctions_resources()->first()->identifier_token,280,140])  }}');">
                                  <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                                <div class="more-prod-bg">

                                </div>
                                <div class="more-prod-text">
                                    Saber mais
                                </div>
                            </a>
                            </div>
                            @else
                            <div class="cover prod-img-list"  style="background-image: url('{{ asset('front/imgs/lotes/placehoder.jpg') }}');">
                                  <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                                <div class="more-prod-bg">

                                </div>
                                <div class="more-prod-text">
                                    Saber mais
                                </div>
                            </a>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="row hidden-md hidden-lg files-mob">
                        <div class="col-xs-12 col-sm-12">
                            <div class="files-share">
                                <ul class="files-share-list">
                                    @if($auction->advertisement)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->advertisement])  }}"><span class="icon-anuncio file-icon "></span><span>Anúncio</span></a></li>
                                    @endif

                                    @if($auction->minuta)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->minuta])  }}"><span class="icon-minuta file-icon"></span><span>Minuta</span></a></li>
                                    @endif

                                    @if($auction->conditions)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->conditions])  }}"><span class="icon-condicoes file-icon"></span><span>Condições</span></a></li>
                                    @endif

                                    @if($auction->catalog)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->catalog])  }}"><span class="icon-catalog file-icon"></span><span>Catálogo</span></a></li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    @if($auction->date)
                                        <div class="text-prod">{{ \Carbon\Carbon::parse($auction->date)->format('d/m/Y') }}</div>
                                        <div class="text-prod">{{ str_replace(':','h',\Carbon\Carbon::parse($auction->date)->format('H:i')) }}</div>
                                    @else
                                        <div class="text-prod">Sem Data</div>
                                        <div class="text-prod"></div>
                                    @endif    
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    <div class="text-prod">{{ $auction->executed }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 text-center text-area-wrap">
                            <a href="http://www.google.com/maps/place/{{$auction->latitude}},{{$auction->longitude}}" target="_blank">
                                <div class="text-area">
                                    <div class="text-wrap">
                                        <div class="text-prod">{{ $auction->local }}</div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4 col-md-4 text-center text-area-wrap">
                            <div class="text-area">
                                <div class="text-wrap">
                                    
                                    <div class="text-prod text-prod_ text-left">
                                        @php
                                            $no_tags = strip_tags($auction->comments)
                                        @endphp
                                        @if(strlen($no_tags)>100)
                                        
                                            {{ utf8_encode(mb_substr(html_entity_decode($no_tags, null, 'ISO-8859-1'),0,100))}}...
                                        @else
                                            {{ $no_tags }}
                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row displ-flex bot-prod-row">
                        <div class="col-lg-10 col-lg-offset-2 col-md-12 pad-5 hidden-xs hidden-sm">
                            <div class="files-share files-share-vendas">
                                <ul class="files-share-list">
                                    @if($auction->advertisement)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->advertisement])  }}"><span class="icon-anuncio file-icon "></span><span>Anúncio</span></a></li>
                                    @endif

                                    @if($auction->minuta)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->minuta])  }}"><span class="icon-minuta file-icon"></span><span>Minuta</span></a></li>
                                    @endif

                                    @if($auction->conditions)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->conditions])  }}"><span class="icon-condicoes file-icon"></span><span>Condições</span></a></li>
                                    @endif

                                    @if($auction->catalog)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->catalog])  }}"><span class="icon-catalog file-icon"></span><span>Catálogo</span></a></li>
                                    @endif
                                </ul>
                                <div class="share-li">
                                        <div class="share-word">
                                            partilha
                                        </div>
                                    <div class="prod-share">
                                        <div class="share-icons">
                                            <ul class="icons-list">
                                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::model.leilao', $auction->id)}}" target="_blank"><span class="icon-facebook"></span></a></li>
                                                 <li><a href="https://plus.google.com/share?url={{ route('cms::frontend::model.leilao', $auction->id)}}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span class="icon-google-plus"></span></a></li>
                                                  <li><a href="http://twitter.com/share?text={{$auction->name}}&url={{route('cms::frontend::model.leilao', $auction->id)}}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a></li>
                                                <li><a href="mailto:?subject={{$auction->title}}&body={{$auction->description}}Link: {{ route('cms::frontend::model.leilao', $auction->id)}} "><span class="icon-mail"></span></a></li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-3 hidden-xs hidden-sm text-area-wrap img-wrap image_auction" data-auction-href="{{route('cms::frontend::model.leilao', $auction->id)}}">
                    @if($auction->auctions_resources()->first())
                        <div class=" cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image_p',[$auction->auctions_resources()->first()->identifier_token,280,140])  }}');">
                              <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                            <div class="more-prod-bg">

                            </div>
                            <div class="more-prod-text">
                                Saber mais
                            </div>
                        </a>
                        </div>
                    @else
                        <div class=" cover prod-img-list"  style="background-image: url('{{ asset('front/imgs/lotes/placehoder.jpg') }}')">
                            <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                        <div class="more-prod-bg">

                        </div>
                        <div class="more-prod-text">
                            Saber mais
                        </div>
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endforeach
