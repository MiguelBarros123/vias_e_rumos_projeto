<?php

namespace Brainy\Profiles\Requests;

use App\Http\Requests\Request;

class PersonalizedFieldRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'fields' => 'required_if:subscript,on',
        ];
    }

}