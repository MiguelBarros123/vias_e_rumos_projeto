<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMediaItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('media_items', function (Blueprint $table) {
            $table->engine='InnoDB';

            $table->increments('id');
            $table->integer('media_folder_id');
            $table->string('type');
            $table->string('thumbnail');
            $table->string('size');
            $table->string('resolution')->nullable();
            $table->string('md5');
            $table->string('identifier_token');
            $table->string('format');
            $table->boolean('used');
            $table->integer('n_of_downloads')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('media_items');
    }
}
