<?php

namespace Brainy\Permissions\Controllers\Frontend;

use Brainy\Framework\Controllers\FrontendController;

class ModelFrontendController extends FrontEndController
{
	public function index()
    {
        return view('permissions::frontend.model.index');
    }
}
