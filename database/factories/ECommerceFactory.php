<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(\Brainy\ECommerce\Models\Order::class, function (Faker\Generator $faker) {
    return [
        'client_name' => $faker->name,
        'client_email' => $faker->email,
        'payment_state' => 0,
        'process_state' => 0,
        'user_id' => 1,
        'currency' => 'EUR',
        'total' => '100',
    ];
});

$factory->define(\Brainy\ECommerce\Models\OrderAddresse::class, function (Faker\Generator $faker) {
    return [
        'street' => $faker->name,
        'city' => $faker->name,
        'country' => $faker->name,
        'zip_code' => 0,
        'phone' => 22342341,
        'email' => $faker->email,
    ];
});

$factory->define(\Brainy\ECommerce\Models\OrderProduct::class, function (Faker\Generator $faker) {
    return [
        'quantity' => 5,
        'price' => 22,
        'product_photo' => 0,
        'product_name' => $faker->name,
        'product_reference' => 'dasd23',
    ];
});