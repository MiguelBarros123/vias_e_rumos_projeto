<?php

namespace Brainy\Permissions\Controllers\Backend;

use App\User;
use Brainy\Framework\Controllers\BackendController;
use Brainy\Framework\Facades\Brainy;
use Brainy\Framework\Models\Group;
use Brainy\Framework\Models\ModuleInfo;
use Brainy\Framework\Models\UserDetail;
use Brainy\Permissions\Repositories\GroupRepositoryInterface;
use Brainy\Permissions\Requests\CreateGroupRequest;
use Brainy\Permissions\Requests\UpdateGroupRequest;
use Brainy\Permissions\Requests\UpdatePermitionsRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Facades\Datatables;


class GroupBackendController extends BackendController
{

    protected $groupRepo;

    public function __construct(GroupRepositoryInterface $groupRepo)
    {
        parent::__construct();
        $this->groupRepo = $groupRepo;
    }

    public function index()
    {
        return view('permissions::backend.model.groups.index');
    }

    public function edit($id)
    {
        $grupo = Group::findOrFail($id);
        $groups = Group::all();
        $modulos=Brainy::containers();

        return view('permissions::backend.model.groups.edit', [
                'grupo' => $grupo,
                'groups' => $groups,
                'modulos' => $modulos
            ]);
    }

    public function create()
    {
        return view('permissions::backend.model.groups.create', [
            'modulos' => Brainy::containers(),
        ]);
    }


    public function allGrupos()
    {
        return Datatables::of(Group::query())
            ->addColumn('action', function ($user) {
                $rota = route('permissions::backend::group.destroy', array($user->id));
                $view_route = route('permissions::backend::group.show',[$user->id]);
                $route_del = route('permissions::backend::group.delete_multiple');

                $used = 0;

                if($user->users()->count() > 0){
                    $used = 1;
                }

                $string = '
                        <div class="btn-group pull-right">
                          <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a  class="open_edit_atrribute_modal" href="'.$view_route.'" ><i class="icon-visivel"></i>  '.trans("profiles::backend.common.view").'</a></li>
                            <li><a href="' . route('permissions::backend::group.edit', array($user->id)) . '" class=""><i class="icon-editar"></i> Editar</a></li>
                            <li><a data-impossible-delete="'.$used.'"
                                    data-target="#delete_resource"
                                    data-toggle="modal"
                                    class="delete_item_table"
                                    data-id="' . $user->id . '"
                                    data-route="'.$route_del.'">
                                <i class="icon-opcoes-eliminar"></i> Eliminar </a>
                            </li>
                          </ul>
                        </div>

                       ';
                return $string;
            })
            ->editColumn('created_at', function ($grupo) {
                return '<p>' . $grupo->created_at . '</p>';
            })
            ->editColumn('updated_at', function ($grupo) {
                return '<p>' . $grupo->updated_by . '</p>';
            })
            ->editColumn('nome', function ($grupo) {
                $users=$grupo->users()->regularUsers()->take(4)->get();
                $fotos='';
                foreach ($users as $user){
                    if($user->photo === ''){
                        $fotos .= '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
                    } else {
                        $fotos .= '<img class="img-circle img-responsive imagem_user_mini" src="'.route('gallery::frontend::storage_image_p',[$user->photo,50,50]).'">';
                    }
                }

                $f = '<div class="container_images">' . $fotos . '</div>';
                return $f . '<div style="display: inline-block;">' . $grupo->name . '</div>';
            })
            ->addColumn('has_users', function ($user) {

                return $user->users()->count();
            })
            ->addColumn('rota_final', function ($user) {
                return route('permissions::backend::group.show',[$user->id]);
            })
            ->make(true);
    }


    public function allUsersGrupo($id)
    {

        $users=UserDetail::regularUsers()->where('group_id', $id)->get();

        return Datatables::of($users)

            ->editColumn('name',function($user){

                if($user->photo === ''){
                    $user_photo = '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
                } else {
                    $user_photo = '<img class="img-circle img-responsive imagem_user_mini" src="'.route('gallery::frontend::storage_image_p',[$user->photo,50,50]).'">';
                }

                return $user_photo.' '.$user->user->name.' '.$user->surname;
            })

            ->editColumn('email',function($user){

                return '<i class="icon-email icon20"></i>'.$user->user->email;
            })
            ->editColumn('grupo',function($user){

                return $user->group->name;
            })

            ->addColumn('action', function ($user) {


                $change_group = route('permissions::backend::group_change_users_group_view',array($user->id,$user->group->id));

                $string = '
                        <div class="btn-group pull-right">
                          <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a target="_blank" href="'.route('permissions::backend::model.edit',array($user->id)).'" class=""><i class="icon-opcoes-editar icon15"></i> Editar</a></li>
                            <li class="divider"></li>
                            <li><a href="'.$change_group.'" class="change-group"><i class="icon-trocar-grupos icon15"></i> Mudar de grupo</a></li>

                          </ul>
                        </div>';


                return $string;
            })
            ->make(true);
    }

    public function usersAllGroupsAjax($id)
    {
        $users=UserDetail::regularUsers()->where('group_id', '!=', $id)->get();

        return Datatables::of($users)

            ->editColumn('name',function($user){

                return $user->user->name.' '.$user->surname;
            })

            ->editColumn('email',function($user){

                return '<i class="icon-email icon20"></i>'.$user->user->email;
            })
            ->editColumn('grupo',function($user){

                return $user->group->name;
            })
            ->make(true);
    }


    public function allPermissionsGrupo($id)
    {
        $grupo=Group::findOrFail($id);
        $modulos=Brainy::containers();

        return view('permissions::backend.model.groups.permissions')
            ->with('grupo', $grupo)
            ->with('modulos', $modulos);
    }

    public function changeUsersGroupView(Request $request,$id=0,$group=0){


        $users = collect();
        if($id!=0 && $group!=0){
            $users->push(UserDetail::findOrFail($id));
            $old_group = Group::findOrFail($group);
        }else{
            $aux = $request->input('deleted_orders');
            $old_group = Group::findOrFail($request->input('group_id'));
            $aux_users = explode(",", $aux);
            foreach ($aux_users as $usr) {
                $users->push(UserDetail::findOrFail($usr));
            }
        }

        $groups = Group::where('id','!=',$old_group->id)->get();


        return view('permissions::backend.model.groups.change')
            ->with('groups',$groups)
            ->with('old_group',$old_group)
            ->with('users',$users);

    }

    public function updateUsersGroupView(Request $request, $id)
    {

        $aux = $request->input('deleted_orders');
        $new_group = $request->input('group_id');
        $old_group = $id;
        $aux_users = explode(",", $aux);

        $this->groupRepo->changeGroupForUsers($new_group,$aux_users,$id);

        $grupo = Group::find($old_group);

        $user_name=" ";

        foreach ($aux_users as $usr) {
            $user = UserDetail::find($usr);
            $user_name .=$user->user->name." ".$user->apelido.", ";
        }

        $aux_username = substr($user_name, 0, -2);

        return redirect()->route('permissions::backend::group.edit',array($old_group))
            ->with('grupo', $grupo)
            ->with('sucess-message',
                trans('permissions::backend.group.messages.success.users_changed_sucess',
                    ['user'=>$aux_username, 'group'=>Group::find($new_group)->name]));

    }

    public function updateNewUsersForGroup(Request $request,$id)
    {

        $new_group=Group::findOrFail($id);
        $users = $request->input('deleted_orders');
        $all_users = explode(",", $users);

        $user_name=" ";


        DB::transaction(function () use($user_name,$all_users,$id,$new_group) {
            foreach ($all_users as $usr) {
                $user=UserDetail::findOrFail($usr);
                $user->group()->update(['updated_by'=>auth()->user()->email]);
                $new_group->users()->save($user);
                $user_name .=$user->user->name." ".$user->surname.", ";
            }
            Group::findOrFail($id)->update(['updated_by'=>auth()->user()->email]);

        });


        $aux_username = substr($user_name, 0, -2);


        return redirect()->route('permissions::backend::group.edit',array($new_group->id))
            ->with('grupo', $new_group)
            ->with('sucess-message',
                trans('permissions::backend.group.messages.success.users_changed_sucess',
                    ['user'=>$aux_username, 'group'=>$new_group->name]));

    }


    public function delete(Request $request,$id)
    {

        $aux_groups = [$id];
        $messages = $this->groupRepo->destroyGroups($aux_groups);

        return redirect()->route('permissions::backend::group.index')->with('error-message',$messages['error'])->with('sucess-message',$messages['success']);
    }

    public function deleteMultiple(Request $request)
    {

        $rows=explode(',',$request->deleted_rows);

        foreach($rows as $id){
            $group = Group::findOrFail($id);

            if($group->users()->count() > 0){
                return redirect()
                    ->route('permissions::backend::group.index')
                    ->with('error-message', trans('permissions::backend.group.messages.error.delete_group', ['name' => $group->name]));
            }

            $group->delete();
        }

        return redirect()->route('permissions::backend::group.index')->with('sucess-message', trans('permissions::backend.group.messages.success.deleted_groups'));

    }


    public function store(CreateGroupRequest $request)
    {
        $this->groupRepo->storeGroup($request->input());

        return redirect()
            ->route('permissions::backend::group.index')
            ->with('sucess-message', trans('permissions::backend.group.messages.success.created_group'));

    }

    public function updatePermissionsGrupo(UpdatePermitionsRequest $request,$id){

        $grupo=Group::findOrFail($id);

        DB::transaction(function () use($grupo,$request) {

        $grupo->permissions()->delete();
        foreach($request->permissoes as $perm){
            $grupo->permissions()->create(['type'=>$perm,'permission'=>$perm]);
        }
        $grupo->update(['updated_by'=>auth()->user()->email]);
        });

        return redirect()
            ->back()
            ->with('sucess-message', trans('permissions::backend.group.messages.success.update_permissions'));
    }

    public function update(UpdateGroupRequest $request,$id)
    {

        $grupo=Group::findOrFail($id);
        $grupo->update(['name'=>$request->name,'updated_by'=>auth()->user()->email]);

//        $group_users = UserDetail::regularUsers()->where('group_id', $grupo->id)->get();
//
//        foreach ($group_users as $g_user){
//            $g_user->group_id = null;
//            $g_user->save();
//        }
//
//        if(isset($request->ids)){
//
//            foreach ($request->ids as $user_id){
//                $user = UserDetail::find($user_id);
//
//                if($user){
//                    $grupo->users()->save($user);
//                }
//
//            }
//        }

        DB::transaction(function () use($grupo,$request) {

            $grupo->permissions()->delete();
            foreach($request->permissoes as $perm){
                $grupo->permissions()->create(['type'=>$perm,'permission'=>$perm]);
            }
            $grupo->update(['updated_by'=>auth()->user()->email]);
        });

        return redirect()
            ->route('permissions::backend::group.index')
            ->with('sucess-message', trans('permissions::backend.group.messages.success.update_group'));
    }


        public function showAddUser($id){
            $group=Group::findOrFail($id);

            $users_group=$group->users;
            $users=UserDetail::all();
            $filtered = $users->reject(function ($item) use($users_group) {
                return $users_group->contains($item->id);
            });

        return view('permissions::backend.model.groups.add_users')->with('group', $group)->with('total_users', $filtered->count());
        }


    public function getUsersForGroup(Request $request,$id){


        $users_group=Group::findOrFail($id)->users;
        $users=UserDetail::regularUsers()->get();

        $filtered = $users->reject(function ($item) use($users_group) {
            return $users_group->contains($item->id);
        });


        return Datatables::of($filtered)
            ->editColumn('name',function($user){

                $user_photo = '<img src="'.asset($user->photo).'" class="img-circle img_user_list" style="margin-right:10px">';

                return $user_photo.' '.$user->user->name.' '.$user->surname;
            })
            ->editColumn('group_id',function($user){

                if($user->group){
                    return $user->group->name;
                } else {
                    return '';
                }

            })

            ->editColumn('email',function($user){

                return '<i class="icon-email icon20"></i>'.$user->user->email;
            })
            ->editColumn('grupo',function($user){

                if($user->group){
                    return $user->group->name;
                } else {
                    return '';
                }

            })
            ->addColumn('msgEstado', function ($user) {


                return '<div id="info-lock-user-' . $user->id . '"
                            class="user-locked-info"
                            style="visibility: ' . ($user->isActive ? 'hidden' : 'visible') . ';">
                                <i class="icon-bloqueado icon20"></i>
                                <span>' . trans('permissions::backend.user.index.info-blocked') . '</span>
                        </div>';

            })

            ->addColumn('estado', function ($user) {


                $routeState = route('permissions::backend::model.change_user_state', [$user->id]);

                return '<bootstrap-toggler
                            toggler-id="toggler-for-user-' . $user->id . '"
                            state="' . (string) $user->getIsActiveAttribute() . '"
                            url="' . $routeState . '"
                            window-callback-success="renderInfoUserBlocked"
                            classes-state-0="info-lock icon-bloqueado icon20"
                            classes-state-1="info-unlock icon-desbloquear icon15">
                        </bootstrap-toggler>';

            })



            ->make(true);
    }

    public function show($id){

        $grupo=Group::findOrFail($id);
        $modulos=Brainy::containers();

        return view('permissions::backend.model.groups.view',compact('grupo'))->with('modulos', $modulos);
    }

    public function addNewUserGroup(Request $request, $id)
    {

//        dd($request->except('_token'));

        if($request->input('users_id') != ""){
            $users = $request->input('users_id');
            $all_users = explode(",", $users);
            $group = Group::findOrFail($id);

            foreach ($all_users as $user){
                $u = UserDetail::findOrFail($user);

                $group->users()->save($u);

            }
        }

    }


}
