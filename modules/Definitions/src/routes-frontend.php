<?php

Route::group(['namespace' => 'Brainy\DefinitionsRent\Controllers\Frontend'], function () {

    Route::get('/', 'ModelFrontendController@index')->name('model.index');

});
