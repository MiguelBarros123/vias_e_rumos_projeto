function removeModalGallerySelection() {
    $('div.item_galery.ui-selected').removeClass('ui-selected');
}

$(function () {
    'use strict';

    var $uploadForm = $('div#files div.cont_inicial');

    $("[data-toggle=popover]").popover({
        html : true,
        template:'<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content gallery_popover"></div></div>'
    });

    var filelist = [];

    $('#fileupload').fileupload({
        url: save_asset_url,
        autoUpload: false,
        maxChunkSize: 524288,
        singleFileUploads: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
        previewMaxWidth: 170,
        previewMaxHeight: 170,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {

        for(var i = 0; i < data.files.length; i++){
            filelist.push(data.files[i]);
        }

        console.log('list', filelist);

        $('.cont_inicial').hide();
        $(".btn_upload_files").off('click').on('click', function () {
//                        data.formData = {pasta_final: $('#dest_f_inp').val(), files: [filelist]};
            data.files = filelist;
            data.submit();
        });


        $.each(data.files, function (index, file) {

            var $fileUpload = $('<div class="file-upload"><div/>');

            var context =  data.context = $fileUpload.appendTo('#files');

            var node = $("<div class=\"file\"></div>")
                .append('<i class=\"file-cancel icon-opcoes-eliminar"></i><div class="file-info"><p>' + file.name + '</p><p>' + file.size + ' KB</p></div>');
            node.appendTo(data.context);
        });

        $(".file-cancel").on("click",function(e){
            e.stopPropagation();
            console.log('element', $(this).parents('.file-upload').index());
            if($(this).parents('.file-upload').index() != -1){
                filelist.splice($(this).parents('.file-upload').index() - 1, 1);
                console.log('new list', filelist);
            }
            $(this).parents('.file-upload').remove();
            if (! $('i.file-cancel').length) {
                $uploadForm.css('display', 'block');
            }
        });

    }).on('fileuploadsubmit', function (e, data) {

        $('.upload_progress').fadeIn();
        $('.btn_upload_files').hide();
        $('.cancel_btn').show();
        data.formData = {pasta_final: $('#dest_f_inp').val()};

    }).on('fileuploadchunksend', function (e, data) {
        if((data.uploadedBytes + data.chunkSize ) == data.total){
            data.data.append('last_chunk', 1);
        }else{
            data.data.append('last_chunk', 0);
        }

    }).on('fileuploadprocessalways', function (e, data) {

        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);

        console.log('files length', data.files.length);
        var reference = $('.file-upload').length - data.files.length + data.index;

        var element = $('.file-upload:eq('+reference+')');

        if (file.preview) {
            element
                .prepend(file.preview);
        } else {
            element.prepend('<i class="icon-icon-upload-doc texto_amarelo icon100"></i>');
        }
        if (file.error) {
            element
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }

    }).on('fileuploadprogressall', function (e, data) {

        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('.tot_prog').text(progress);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );

    }).on('fileuploaddone', function (e, data) {
        var resul=data.result;

        $('.btn_upload_files').show();
        $('.cancel_btn').hide();

        $('.err_mesages').empty();
        $('.msg_sucesso_ajax').hide();
        $('.msg_erro_ajax').hide();


        if(resul['errors']){
            $('.msg_erro_ajax').fadeIn();
            $.each(resul['errors'],function( index , el) {
                $('.err_mesages').append('<p>'+el+'</p>');
            });
        }else{
            $('.msg_sucesso_ajax').fadeIn();
            $('.file-upload').remove();
            filelist = [];
            $uploadForm.show();

            $.each(data.result.files, function (index, file) {
                if (file.save_asset_url) {
                    var link = $('<a>')
                        .attr('target', '_blank')
                        .prop('href', file.save_asset_url);
                    $(data.context.children()[index])
                        .wrap(link);
                } else if (file.error) {
                    var error = $('<span class="text-danger"/>').text(file.error);
                    $(data.context.children()[index])
                        .append(error);
                }

            });

        }

        setTimeout(function(){$('.upload_progress').fadeOut()},1000);

    }).on('fileuploadfail', function (e, data) {
        $('.btn_upload_files').show();
        $('.cancel_btn').hide();

        $('.err_mesages').empty();
        $('.msg_sucesso_ajax').hide();
        $('.msg_erro_ajax').hide();


        $('.msg_erro_ajax').fadeIn();
        $.each(data.jqXHR.responseJSON.errors,function( index , el) {
            $('.err_mesages').append('<p>'+el+'</p>');
        });
        setTimeout(function(){$('.upload_progress').fadeOut()},1000);

    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});

$(document).ready(function(){
    $('#files').click(function(){
        $('#fileupload').click();
    });

    var token=$('meta[name="csrf-token"]').attr('content');

    $('.listagem_pastas').selectslidemenu({ dados: folders,inicial:inicial,token: token});


    $('#open_select_folder').click(function(){
        $('.listagem_pastas').toggle();
    });
    $('.fecha_list_pastas').click(function(){
        $('.listagem_pastas').hide();
    });

    $('.lang_tabs li:first-child').tab('show');
    $('.lang_content').children().first().addClass('active');

    $('.embed_lang_tabs li:first-child').tab('show');
    $('.embed_lang_content').children().first().addClass('active');

    $('#tab-upload-images').on('click', function(){
        document.getElementById('upload-link').appendChild(
            document.getElementById('upload-folder')
        );
        $('#upload-folder').addClass('no_padding');
        document.getElementById('dest_f_inp').setAttribute('name','folder_id');
    });

    $('#tab-embed-link').on('click', function(){
        document.getElementById('embed-folder').appendChild(
            document.getElementById('upload-folder')
        );
        $('#upload-folder').removeClass('no_padding');
        document.getElementById('dest_f_inp').setAttribute('name','media_folder_id');
    });

});

