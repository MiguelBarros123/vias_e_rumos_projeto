<?php

use Illuminate\Database\Seeder;

// composer require laracasts/testdummy
use Laracasts\TestDummy\Factory as TestDummy;

class ExtraTableSeeder extends Seeder {

    public function run()
    {
        // TestDummy::times(20)->create('App\Post');

        $type = \Brainy\Rent\Models\ExtraType::create(['pt' => ['name' => 'Serviços adicionais recomendados']]);
        $type = \Brainy\Rent\Models\ExtraType::create(['pt' => ['name' => 'Extras recomendados']]);
        $type = \Brainy\Rent\Models\ExtraType::create(['pt' => ['name' => 'Extras adicionais']]);
        
        $extra = \Brainy\Rent\Models\Extra::create([
            'has_qty' => 1,
            'has_value' => 1,
            'pay_by_day' => 1,
            'min_quantity' => 0,
            'max_quantity' => 5,
            'iva' => 23,
            'image' => '5980561dd0fd97.74474297.jpeg',
            'order' => 1,
            'pt' => ['name' => 'Idade', 'value_text' => 'Idade ou Peso'],
            'en' => ['name' => 'Idade', 'value_text' => 'Idade ou Peso'],
            'fr' => ['name' => 'Idade', 'value_text' => 'Idade ou Peso']
        ]);

        $p_table= \Brainy\Rent\Models\GlobalPriceTable::create([
            'price_w_tax' => 15,
            'price' => 10,
            'coin_id' => 1
        ]);
        $extra->global_price_tables()->save($p_table);

    }

}