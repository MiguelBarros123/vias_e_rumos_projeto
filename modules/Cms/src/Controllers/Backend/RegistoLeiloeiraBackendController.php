<?php

namespace Brainy\Cms\Controllers\Backend;

use Brainy\Cms\Models\RegistoLeiloeira;
use Illuminate\Support\Facades\Storage;
use Brainy\Framework\Controllers\BackendController;

class RegistoLeiloeiraBackendController extends BackendController
{
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $registo_leiloeira = RegistoLeiloeira::first();

        if($registo_leiloeira){
            return view('cms::backend.registo_leiloeira.edit', compact('registo_leiloeira'));
        }

        return view('cms::backend.registo_leiloeira.create');
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function store()
    {
        $registo_leiloeira = new RegistoLeiloeira;
        $registo_leiloeira_file = request()->file('registo_leiloeira');

        if($registo_leiloeira_file){
            $uniqueFileName = uniqid() . $registo_leiloeira_file->getClientOriginalName();
            Storage::disk('pdf_files')->put($uniqueFileName, file_get_contents($registo_leiloeira_file));
            $registo_leiloeira->registo_leiloeira = $uniqueFileName;
        }

        $registo_leiloeira->save();

        return redirect()->route('cms::backend::backend.cms.registo_leiloeira.index')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }

    /**
    * Update a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function update($id)
    {
        $registo_leiloeira = RegistoLeiloeira::find($id);
        $registo_leiloeira_file = request()->file('registo_leiloeira');

        if($registo_leiloeira_file){
            $uniqueFileName = uniqid() . $registo_leiloeira_file->getClientOriginalName();
            Storage::disk('pdf_files')->put($uniqueFileName, file_get_contents($registo_leiloeira_file));
            $registo_leiloeira->registo_leiloeira = $uniqueFileName;
        }

        $registo_leiloeira->save();

        return redirect()->route('cms::backend::backend.cms.registo_leiloeira.index')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }
}
