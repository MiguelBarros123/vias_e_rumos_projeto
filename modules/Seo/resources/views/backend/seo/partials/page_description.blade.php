<script>
    $(function(){
        var item="{{$page->id}}";
        $('.new_tabs'+item+' li:first-child').tab('show');
        $('.content_lang'+item+'').children().first().addClass('active');
    });
</script>

@foreach($locales as $loc)
    <script>
        {{--$('#route_url_{{$page->id}}_{{ $loc }}').on('change keyup',function() {--}}

            {{--var sanitized = $(this).val().replace(/\//g , '');--}}
            {{--$(this).val(sanitized);--}}
            {{--if($(this).val()!='' ){--}}
                {{--document.getElementById("page_submit_{{$page->id}}").disabled = false;--}}
            {{--}else{--}}
                {{--document.getElementById("page_submit_{{$page->id}}").disabled = true;--}}
            {{--}--}}
        {{--});--}}

        {{--$('#page_submit_{{$page->id}}').on('click', function(){--}}
            {{--if($('#route_url_{{$page->id}}_{{ $loc }}').val() === ''){--}}
                {{--alert('trans()')--}}
            {{--}--}}
        {{--});--}}

        function validateForm()
        {
            var route_url = $('#route_url_{{$page->id}}_{{ $loc }}');

            console.log(route_url);

            if(route_url == ''){
                alert('O campo Friendly Url tem de ser preenchido para todas as línguas!');
                return false;
            }
        }

        $( document ).ready(function() {
            $.validator.setDefaults({ ignore: ":hidden:not(select)" });
            $('#first_form').validate({
                success: function(label) {
                    label.addClass("valid").text("")
                },
                ignore : [],
                invalidHandler : function(label) {

                    $('#error_tabs1').fadeIn();

                }
            });
        });

    </script>
@endforeach

<form id="first_form" method="post" action="{{route('seo::backend::seo.update_description',[$page->id])}}" onsubmit="return validateForm()" autocomplete="off">
    <input type="hidden" name="_method" value="put">
    {!! csrf_field() !!}
    <table class="variant_table table">
        <tbody>
        <tr>

            <td >

                <div id="error_tabs1" class="alert alert-erro alert-dismissible error_tabs" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <i class="icon-opcoes-eliminar"></i>@lang('catalog::backend.messages.error.tabs')
                </div>

                <!-- Nav tabs -->
                <ul class="tabs_brainy lang_tabs new_tabs{{$page->id}}" role="tablist">
                    @foreach($locales as $loc)
                        <li role="presentation" class=""><a href="#{{$loc}}{{$page->id}}"  role="tab" data-toggle="tab">{{$loc}}</a></li>
                    @endforeach
                </ul>

                <div class="tab-content  lang_content content_lang{{$page->id}}">

                    @foreach($locales as $loc)
                        <div role="tabpanel" class="tab-pane fade in  margem_20" id="{{$loc}}{{$page->id}}">
                            <div class="form-group fom-style-hidden2 @if($errors->first('route_url_'.$loc) ) has-error @endif">
                                <label>@lang('seo::backend.common.route_url')</label>
                                <input type="text" name="route_url_{{$loc}}" id="route_url_{{$page->id}}_{{ $loc }}" class="form-control" value="{{$page->translateOrNew($loc)->route_url}}" required="">

                                {!! $errors->first('route_url_'.$loc,'<span class="validator_errors">:message</span>')!!}
                            </div>

                            <div class="form-group fom-style-hidden2 @if($errors->first('title_'.$loc) ) has-error @endif">
                                <label>@lang('seo::backend.common.title')</label>
                                <input type="text" name="title_{{$loc}}" class="form-control" value="{{$page->translateOrNew($loc)->title}}">

                                {!! $errors->first('title_'.$loc,'<span class="validator_errors">:message</span>')!!}
                            </div>

                            <div class="form-group fom-style-hidden2 @if($errors->first('meta_description_'.$loc) ) has-error @endif">
                                <label for="exampleInputEmail1">@lang('seo::backend.common.meta_description')</label>
                                <textarea name="meta_description_{{$loc}}" class="form-control">{{$page->translateOrNew($loc)->meta_description}}</textarea>

                                {!! $errors->first('meta_description_'.$loc,'<span class="validator_errors">:message</span>')!!}
                            </div>

                            @foreach($highlights as $high)
                                @if($high->image != '')
                                    <div class="form-group row">
                                        <div class="col-md-1">
                                            <img src="{{ route('gallery::backend::storage_image', [$high->image,100,100]) }}">
                                        </div>
                                        <div class="col-md-11">
                                            <div class="form-group fom-style-hidden2 @if($errors->first('image_alt_'.$loc) ) has-error @endif">
                                                <label for="exampleInputEmail1">@lang('seo::backend.common.image_alt')</label>
                                                <input type="text" name="highlight[{{ $high->id }}][{{ $loc }}][image_alt]" class="form-control" value="{{ $high->translateOrNew($loc)->image_alt }}">

                                                {!! $errors->first('image_alt_'.$loc,'<span class="validator_errors">:message</span>')!!}
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                            @foreach($composeds as $composed)
                                @if($composed->c_hilights)
                                    @foreach($composed->c_hilights as $c_high)
                                        @if($c_high->photo != '')
                                            <div class="row">
                                                <div class="col-md-1">
                                                    <img src="{{ route('gallery::backend::storage_image', [$c_high->photo,100,100]) }}" class="image_highlight">
                                                </div>
                                                <div class="col-md-11">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('image_alt_'.$loc) ) has-error @endif">
                                                        <label for="exampleInputEmail1">@lang('seo::backend.common.image_alt')</label>
                                                        <input type="text" name="c_highlight[{{ $c_high->id }}][{{ $loc }}][image_alt]" class="form-control" value="{{ $c_high->translateOrNew($loc)->image_alt }}">

                                                        {!! $errors->first('image_alt_'.$loc,'<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach
                                @endif
                                {{--@foreach($composed->c_highlight as $c_high)--}}
                                    {{--{{$c_high->image_alt}}--}}
                                    {{--<div class="col-md-1">--}}

                                    {{--</div>--}}
                                    {{--<div class="col-md-11">--}}
                                        {{--<div class="form-group fom-style-hidden2 @if($errors->first('image_alt_'.$loc) ) has-error @endif">--}}
                                            {{--<label for="exampleInputEmail1">@lang('seo::backend.common.meta_description')</label>--}}
                                            {{--<input type="text" name="c_highlight[{{ $c_high->id }}][{{ $loc }}][image_alt]" class="form-control" value="{{ $c_high->translateOrNew($loc)->image_alt }}">--}}

                                            {{--{!! $errors->first('image_alt_'.$loc,'<span class="validator_errors">:message</span>')!!}--}}
                                        {{--</div>--}}
{{--                                        {{ $c_high->translateOrNew($loc)->image_alt }}--}}
                                    {{--</div>--}}
                                {{--@endforeach--}}
                            @endforeach

                        </div>
                    @endforeach

                </div>
            </td>


        </tr>
        <tr>
            <td>
                <button id="page_submit_{{$page->id}}" type="submit" class="btn btn-default btn-yellow">@lang('catalog::backend.common.save')</button>
            </td>


        </tr>

        </tbody>
    </table>
</form>