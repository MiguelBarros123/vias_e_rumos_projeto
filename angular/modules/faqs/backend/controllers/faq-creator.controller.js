(function () {
	'use strict';

	function FaqCreatorController($scope, $http) {
		var faqModel = {
			question: {
				translations: {}
			},
			answer: {
				translations: {}
			},
			theme_id: null,
			visible: true,
			link: null,
			tags: []
		};

		$scope.faq = $.extend({}, faqModel);

		$scope.option = {
            language: $scope.locale,
            allowedContent: true,
            entities: false,
        };

        // Called when the editor is completely ready.
        $scope.onReady = function() {
            //
        };

		$scope.toggleVisibility = function () {
			$scope.faq.visible = !$scope.faq.visible;
			console.log('visibily toggled', $scope.faq.visible);
		};

		function httpPostSuccess(event) {
			return function (response) {
				$scope.$emit(event, response);
				$scope.dismiss('#modal-faq');
			};
		}

		function httpFaqCreationError(response) {
			console.log(response);
		}

		$scope.updateOrNewFaqNode = function () {
			var event = 'edit faq';

			if ($scope.isEmptySelection()) {
				$scope.faq.id = 'new-faq-' + Date.now();
				event = 'new faq';
			}

			$http.post('http://localhost/brainy-web/public/backend/faqs/faq', $scope.faq)
				.success(httpPostSuccess(event))
				.error(httpFaqCreationError);
		};

		$(function() {
			$scope.ms = $('#magicsuggest-faq').magicSuggest({
                data: $scope.getTags,
                allowDuplicates: false,
                valueField: 'name'
            });

            $($scope.ms).on('selectionchange', function() {
                var tags = angular.fromJson(this.getSelection());

                for (var i = 0; i < tags.length; ++i) {
                	var tag = tags[i];
                	if (tag.id === undefined) {
                		tag.id = 'new-tag-' + Date.now();
                	}
                }

                $scope.faq.tags = tags;
                console.log($scope.faq.tags);
            });

            $('#modal-faq').on('hidden.bs.modal', function () {
    			$scope.faq = angular.copy(faqModel);
                $scope.clearSelection();
                $scope.ms.clear();
			});

			$('#modal-faq').on('shown.bs.modal', function () {
                if (! $scope.isEmptySelection()) {
                	$scope.faq.id = $scope.selectedNode.id;
                	$scope.faq.link = $scope.selectedNode.link;
                    $scope.faq.visible = $scope.selectedNode.visible;
                    $scope.faq.theme_id = $scope.selectedNode.theme_id;

                    for (var i = 0; i < $scope.selectedNode.translations.length; ++i) {
                        var locale = $scope.selectedNode.translations[i].locale;
                        $scope.faq.question.translations[locale] = $scope.selectedNode.translations[i].question; 
                        $scope.faq.answer.translations[locale] = $scope.selectedNode.translations[i].answer;
                    }

                    $scope.faq.tags = $scope.selectedNode.tags;
                    if ($scope.faq.tags && $scope.faq.tags.length) {
                        $scope.ms.setSelection($scope.faq.tags);
                    }
                    
                } else {
                    $scope.faq = angular.copy(faqModel);
                }

                if (! $scope.$$phase) {
                    $scope.$apply();
                }
            });
        });
	}

	angular.module('brainy.faqs.backend').controller('FaqCreatorController', ['$scope', '$http', FaqCreatorController]);
})();