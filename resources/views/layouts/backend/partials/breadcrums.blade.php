<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
        <ul class="breadcrumb">
            @if ($breadcrumbs)
                    @foreach ($breadcrumbs as $breadcrumb)
                        @if ($breadcrumb->url && !$breadcrumb->last)
                            <li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
                        @else
                            <li class="active">{{ $breadcrumb->title }}</li>
                        @endif
                    @endforeach
            @endif

        </ul>
    </div>
</div>