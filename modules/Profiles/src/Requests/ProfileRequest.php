<?php

namespace Brainy\Profiles\Requests;

use App\Http\Requests\Request;

class ProfileRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'email' => 'required_if:is_user,on|unique:users,email',
            'password' => 'required_if:is_user,on|min:8'
        ];
    }

}