<?php

return [
    'model' => [
        'index' => [
            //@lang('permissions::frontend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view frontend page',
        ],
        'edit' => [
            'recover-password' => [
                'title' => 'Recover password',
                'confirm-question' => 'Have you sure you want to recover the password for user',
                'confirm-true' => 'recover',
                'confirm-false' => 'cancel'
            ],
            'archive-user' => [
                'title' => 'archive user',
                'confirm-question' => 'Have you sure you pretend to archive the user(s)',
                'confirm-true' => 'archive',
                'confirm-false' => 'cancel'
            ],
            'change-group' => [
                'title' => 'Change Grup',
            ],
            'save' => 'save',
            'cancel' => 'cancel',
        ],
    ],
    'messages' => [
        //@lang('permissions::frontend.messages.notifications.new-task')
        'notifications' => [
            'new-task' => 'You have a new pending task',
        ],
    ],
];
