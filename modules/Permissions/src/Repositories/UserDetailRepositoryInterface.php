<?php

namespace Brainy\Permissions\Repositories;

use Illuminate\Http\Request;

interface UserDetailRepositoryInterface
{

    public function storeUser($user);
    public function changeUserState($user);
    public function destroyUsers($users);
    public function recoverPassword($user_email);



}










