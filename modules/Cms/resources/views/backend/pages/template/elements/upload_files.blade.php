<script>
    var url = "{{url('/')}}";
</script>
<div class="modal modal-aux" id="upload_resource" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <input type="hidden" id="variant_id" value=""/>
    <input type="hidden" id="image_store_clicked" value=""/>
    <div class="modal-dialog modal-lg modal-dialog-aux" role="document">
        <input type="text" name="groups_id" id="groups_id" value="" class="groups_id" hidden>
        <div class="modal-content">
            <div class="panel-titulo">
                    <span class="titulo">
                        <i class="icon-upload pading_right icon20"></i>@lang('cms::backend.model.common.image_upload')
                    </span>
            </div>
            <div class="modal-body">


                <!-- Nav tabs -->
                <ul class="tabs_brainy" role="tablist">
                    <li role="presentation" class="active">
                        <a id="ulpload-directive" class="text-uppercase " href="#select_images"  role="tab" data-toggle="tab">
                            @lang('cms::backend.model.common.gallery')
                        </a>
                    </li>
                    <li role="presentation" class="">
                        <a id="tab-upload-images" class="text-uppercase" href="#upload_images" onClick="removeModalGallerySelection()" role="tab" data-toggle="tab">
                            @lang('cms::backend.model.upload.title')
                        </a>
                    </li>
                    <li role="presentation" class="">
                    <a id="tab-embed-link" class="text-uppercase" href="#embed_link" onClick="removeModalGallerySelection()" role="tab" data-toggle="tab">
                    @lang('cms::backend.model.upload.embed')
                    </a>
                    </li>
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div ng-app="ImagePicker" ng-init="getPublicPath(url)" role="tabpanel" class="tab-pane fade in  margem_20 active" id="select_images">
                        <image-picker json-gallery-items="{{ \Brainy\Gallery\Models\MediaFolder::with('media_items')->get()->toJson()}}" root-folder="1"></image-picker>
                        <div class="modal-footer modal-eliminar padding-bottom-30">
                            <button id="btn_upload_image" type="button" class="btn btn-default btn-yellow">
                                @lang('cms::backend.model.common.save')
                            </button>
                            <button type="button" class="btn btn-cancelar" data-dismiss="modal">
                                @lang('cms::backend.model.common.cancel')
                            </button>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in margem_20" id="embed_link">
                    <div class="container-fluid">
                    <div class="row">
                    <div class="col-lg-12">
                    <form class="form_elimina_media" method="POST" action="{{route('gallery::backend::store_embed_link')}}" >
                    {{--<input  name="media_folder_id" type="hidden" value="{{\Brainy\Gallery\Models\MediaFolder::findOrFail(1)->id}}">--}}
                    {!! csrf_field() !!}
                    <div class="row">
                    <div class="col-lg-12">
                    <!-- Nav tabs -->
                    <ul class="tabs_brainy embed_lang_tabs" role="tablist">
                    @foreach($locales as $loc)
                    <li role="presentation" class=""><a href="#{{$loc}}"  role="tab" data-toggle="tab">{{$loc}}</a></li>
                    @endforeach
                    </ul>

                        <div class="row">
                            <div id="embed-folder">

                            </div>
                        </div>

                    <!-- Tab panes -->
                    <div class="tab-content embed_lang_content">


                    @foreach($locales as $loc)
                    <div role="tabpanel" class="tab-pane fade in  margem_20" id="{{$loc}}">


                    <div class="form-group fom-style-hidden2 @if($errors->first('titulo_'.$loc) ) has-error @endif">
                    <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.title')</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="titulo_{{$loc}}" value="{{old('titulo_'.$loc)}}">

                    {!! $errors->first('titulo_'.$loc,'<span class="validator_errors">:message</span>')!!}
                    </div>

                    <div class="form-group fom-style-hidden @if($errors->first('descricao_'.$loc) ) has-error @endif">
                    <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.description')</label>
                    <textarea class="form-control" rows="3" name="descricao_{{$loc}}" id="editor_embed_d_{{ $loc }}">{{old('descricao_'.$loc)}}</textarea>
                    {!! $errors->first('descricao_'.$loc,'<span class="validator_errors">:message</span>')!!}
                    </div>

                    </div>
                    @endforeach
                    </div>




                    <div class="form-group fom-style-hidden2 @if($errors->first('identifier_token') ) has-error @endif">
                    <label for="exampleInputEmail1">@lang('gallery::backend.model.upload.add_link')</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" name="identifier_token" value="{{old('identifier_token')}}">
                    {!! $errors->first('identifier_token','<span class="validator_errors">:message</span>')!!}
                    </div>


                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30">
                    <button type="submit" class="btn btn-default btn-yellow">@lang('gallery::backend.model.upload.add')</button>
                    <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal">
                    @lang('cms::backend.model.common.cancel')
                    </button>
                    </div>
                    </div>
                    </form>
                    </div>
                    </div>
                    </div>
                    </div>
                    <div role="tabpanel" class="tab-pane fade in margem_20" id="upload_images">
                        <div class="conteudo_central margin-top-no-menu">
                            <div class="container-fluid">


                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  msgs_gallery">


                                        <div class="alert alert-sucess alert-dismissible  msg_sucesso_ajax" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <i class="icon-permissoes icon9 pading_right" ></i>@lang('gallery::backend.messages.success.uploaded_image')
                                        </div>



                                        <div class="alert alert-erro alert-dismissible msg_erro_ajax" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <i class="icon-opcoes-eliminar icon9  pading_right"></i>
                                            <span class="err_mesages"></span>
                                        </div>


                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">

                                        <div class="row">
                                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                                                <div class="titulo2" ><i class="icon-icon-peq-upload"></i>@lang('gallery::backend.model.upload.upload')</div>
                                            </div>
                                            <div id="upload-link">
                                                <div id="upload-folder" class="col-lg-10 col-md-10 col-sm-10 col-xs-10 upload-folder no_padding">

                                                    <div class="container-fluid">
                                                        <div class="row">

                                                            <div class="col-lg-4 pos_relative no_padding">
                                                                <a id="open_select_folder">@lang('gallery::backend.model.upload.destiny')</a> <span ><i class="icon-icon-peq-pasta icon15 margem_icon"></i><span id="dest_f" ></span></span> <input id="dest_f_inp" value="{{\Brainy\Gallery\Models\MediaFolder::findOrFail(1)->id}}" type="hidden" name="folder_id">
                                                                <div class="listagem_pastas">
                                                                    <div class="container-fluid">
                                                                        <div class="row">
                                                                            <div class="col-lg-12 no_padding">
                                                                                <div class="row-eq-height-center">
                                                                                    <div class="col-lg-2 back_destiny"><i class="icon-voltar"></i></div>
                                                                                    <div class="col-lg-8 pasta_atual">@lang('gallery::backend.model.upload.gallery')</div>
                                                                                    <div class="col-lg-10 nova_pasta_escond">

                                                                                        <div class="input-group">
                                                                                            <span  class="input-group-addon btn_cancel_new_folder"><i class="icon-voltar"></i></span>
                                                                                            <input  type="text" class="form-control input_pasta" name="title" value="nova pasta">
                                                                                            <input id="pasta_atual_pai"  type="hidden"  name="parent_folder_id" value="">
                                                                                            <a  class="input-group-addon btn_save_new_folder" data-href="{{route('gallery::backend::save_folder_upload')}}"><i class="icon-permissoes icon15"></i></a>
                                                                                        </div>


                                                                                    </div>
                                                                                    <div class="col-lg-2 fecha_list_pastas"><i class="icon-opcoes-eliminar"></i></div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12 sec_pastas_centro">
                                                                                <div class="new_folder_selected"></div>
                                                                                <div class="row">
                                                                                    <div class="col-lg-12 no_padding">
                                                                                        <ul id="selectable" class="destinys">

                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-lg-12 foot_destiny no_padding">
                                                                                <div class="innactive_btns"></div>
                                                                                <div class="container-fluid">

                                                                                    <div class="row row-eq-height-center">
                                                                                        <div class="col-lg-8 no_padding">
                                                                                            <a class="btn-yellow btn_save_folder">@lang('gallery::backend.model.upload.save_here')</a>
                                                                                            <div class="new_folder_selected"></div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>

                                                            </div>

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="panel-body uploadfilesgalery">

                                            <input id="fileupload" type="file" name="files[]" multiple>
                                            <!-- The container for the uploaded files -->
                                            <div id="files" class="files row">
                                                <div class="cont_inicial">
                                                    <p class="fonte_15_lato">@lang('gallery::backend.model.upload.drag_and_drop')</p>
                                                    <p class="fonte_12_lato_regular">@lang('gallery::backend.model.upload.drag_and_click')</p>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-lg-offset-3 fonte_12_lato_light_cinza documentos_perm">
                                                            <div class="row">
                                                                <div class="col-lg-15"><i class="icon-icon-upload-doc texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.documents')</p></div>
                                                                <div class="col-lg-15"><i class="icon-icon-upload-zip texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.zipped_folders')</p></div>
                                                                <div class="col-lg-15"><i class="icon-icon-upload-img texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.images')</p> </div>
                                                                <div class="col-lg-15"><i class="icon-icon-upload-video texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.videos')</p></div>
                                                                <div class="col-lg-15"><i class="icon-icon-upload-som texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.audio')</p></div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>

                                            <div>
                                                <p class="fonte_12_lato_light allowed_upload">

                                                    @lang('gallery::backend.model.upload.allowed_upload')
                                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="
                                                            <div>PDF</div>
                                                            <div>DOC</div>
                                                            <div>DOCX</div>
                                                            <div>XLS</div>
                                                            <div>PPT</div>
                                                            "
                                                    > <i class="icon-icon-upload-doc icon15 pading_right"></i>@lang('gallery::backend.model.upload.documents'); <i class="icon-info-extra-1 icon-cinza"></i></a>
                                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus"  data-content="
                                                            <div>JPEG</div>
                                                            <div>PNG</div>
                                                            <div>GIF</div>
                                                            <div>SVG</div>
                                                            "
                                                    ><i class="icon-icon-upload-img icon15 pading_right"></i>@lang('gallery::backend.model.upload.images'); <i class="icon-info-extra-1 icon-cinza"></i></a>
                                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus"  data-content="
                                                           <div>MKV</div>
                                                            <div>AVI</div>
                                                            <div>MP4</div>
                                                            <div>H.264</div>
                                                            "><i class="icon-icon-upload-video icon15 pading_right"></i>@lang('gallery::backend.model.upload.videos'); <i class="icon-info-extra-1 icon-cinza"></i></a>
                                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus"  data-content="
                                                            <div>Zip</div>
                                                            "><i class="icon-icon-upload-zip icon15 pading_right"></i>@lang('gallery::backend.model.upload.zipped_folders'); <i class="icon-info-extra-1 icon-cinza"></i></a>
                                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus"  data-content="
                                                            <div>WAV</div>
                                                            <div>MP3</div>
                                                            <div>RAW</div>
                                                            "><i class="icon-icon-upload-som icon15 pading_right"></i> @lang('gallery::backend.model.upload.audio'). <i class="icon-info-extra-1 icon-cinza"></i></a>
                                                </p>
                                            </div>


                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-lg-3 upload_progress">

                                                    <p class="fonte_10_lato_regular">@lang('gallery::backend.model.upload.progress_upload') <span class="tot_prog"></span>%</p>
                                                    <div id="progress" class="progress">
                                                        <div class="progress-bar progress-bar-success"></div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 upload-file-create">
                                                    <button class="btn btn-default btn-yellow btn_upload_files"> @lang('gallery::backend.model.upload.upload')</button>
                                                    <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal">
                                                        @lang('cms::backend.model.common.cancel')
                                                    </button>
                                                </div>
                                            </div>


                                        </div>
                                    </div>




                                </div>
                            </div>

                            <div class="row">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--</form>-->
    </div>
</div>
