var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.styles([
            'backend-styles.css'
        ], '../../public/css/profiles/backend.css');
});