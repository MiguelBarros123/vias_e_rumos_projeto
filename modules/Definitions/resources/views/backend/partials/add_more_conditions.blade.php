<div class="conditions">
    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
        <div class="fom-style-hidden2" data-id="{{isset($bidding) ? $bidding->id: 0}}">
            <label>Lotes com valor entre</label>
            <div class="definitions-value" style="display: inline-block;">
                <div class="form-line-group">
                    <div class="input-group">
                        <div class="input-group-addon EUR">€</div>
                        <input type="text" name="licitacoes[{{isset($bidding) ? $bidding->id : 0}}][min_licitacao]" class="form-control no_inva_input min_licitacao" value="{{ isset($bidding) ? $bidding->min_bidding/100 : ''}}">
                    </div>
                </div>
            </div>
            <label>e entre</label>
            <div class="definitions-value" style="display: inline-block;">
                <div class="form-line-group">
                    <div class="input-group">
                        <div class="input-group-addon EUR">€</div>
                        <input type="text" name="licitacoes[{{isset($key) ? $bidding->id : 0}}][max_licitacao]" class="form-control no_inva_input max_licitacao" value="{{ isset($bidding) ? $bidding->max_bidding/100 : ''}}">
                    </div>
                </div>
            </div>
            <label>as licitações tem de ser um valor acrescido de pelo menos</label>
            <div class="definitions-value" style="display: inline-block;">
                <div class="form-line-group">
                    <div class="input-group">
                        <div class="input-group-addon EUR">€</div>
                        <input type="text" name="licitacoes[{{isset($key) ? $bidding->id : 0}}][proxima_licitacao]" class="form-control no_inva_input proxima_licitacao" value="{{ isset($bidding) ? $bidding->next_bidding/100 : ''}}">
                    </div>
                </div>
            </div>
            <hr>
        </div>
    </div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
        <div>
            <div class="input-group">
                <i class="icon-opcoes-eliminar eliminar-rule line-eliminar"></i>
            </div>
        </div>
    </div>
</div>
