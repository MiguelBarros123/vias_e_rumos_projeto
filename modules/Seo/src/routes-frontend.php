<?php

Route::group(['namespace' => 'Brainy\Seo\Controllers\Frontend'], function () {

    Route::get('/', 'ModelFrontendController@index')->name('model.index');

});
