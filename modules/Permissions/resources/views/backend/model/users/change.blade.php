@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
@endsection

@section('module-scripts')

@endsection

@section('content')

    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{route('permissions::backend::model.edit', [$user_id])}}" class="icon_voltar"><i class="icon-voltar"></i> Voltar</a></li>
                    </ul>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <ul class="breadcrumb">
                        <li>Gestão de permissões</li>
                        <li>Gestão de Utilizadores</li>
                        <li>Mudar de grupo</li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo"><span class="titulo"><i class="icon-trocar-grupos icon20"></i> MUDAR DE GRUPO</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <div class="col-lg-12">
                                    @foreach($users as $user)
                                        <div class="inline-images text-center">
                                            <img class="avatar_user " src="{{asset($user->photo)}}" >
                                            <p class="font_12_lato_regular padding-top-10 ">{{$user->user->name}}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <form class="form_elimina_user" method="POST" action="{{route('permissions::backend::group.update_users_group',array($old_group))}}" >
                {!! csrf_field() !!}

                <input type="text" name="deleted_orders"  value="{{$users->implode('id',',')}}" hidden>
                <input type="text" name="old_group_id" value="{{$old_group}}" hidden>
                <input type="text" name="user_id" value="{{$user->id}}" hidden>




                <div class="row">
                    <div class="col-lg-12">
                        <p class="padding-left-15 font_12_lato_regular">Mudar do grupo</p>

                        <div class="panel ">
                            <div class="panel-body">
                                <div class="row">
                                    <a href="{{route('permissions::backend::group.edit',array($old_group))}}" target="_blank">
                                        <div class="col-lg-12">
                                            <label class="radio-default" style="margin-bottom: -2px!important;">
                                                <input type="radio" name="group_id" value="{{$old_group}}" checked>
                                                <span class="span-radio"></span>
                                            </label>
                                            <div class="container_images"> <img class="img-circle img-responsive imagem_user_mini" src="{{asset($user->photo)}}"></div>
                                            <label class="label-group-list fonte_12_lato_light"> {{ ($user->group) ? $user->group->name : ''}} </label>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                            <p class="padding-left-15 font_12_lato_regular">para o grupo:</p>
                        @if($groups->count()==0)
                            <div>Não existem grupos no sistema. Crie um grupo.</div>
                        @endif

                        @foreach($groups as $group)
                            @if($group->id!=$old_group)
                                <div class="panel" style="margin-bottom: 5px!important;">
                                    <div class="panel-body">
                                        <div class="row">
                                            <a href="{{route('permissions::backend::group.edit',array($group->id))}}" target="_blank">
                                                <div class="col-lg-12">
                                                    <label class="radio-default" style="margin-bottom: -2px!important;">
                                                        <input type="radio" name="group_id" value="{{$group->id}}">
                                                        <span class="span-radio"></span>
                                                    </label>

                                                    <div class="container_images">

                                                        @foreach ($group->users()->take(4)->get() as $user_image)
                                                        <img class='img-circle img-responsive imagem_user_mini' src='{{asset($user_image->photo)}}'>
                                                        @endforeach
                                                    </div>
                                                    <label class="label-group-list fonte_12_lato_light">{{$group->name}}
                                                        <div class="accordion-arrow"></div>
                                                    </label>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>


                <div class="row padding-top-35">
                    @if($groups->count()!=0)<div class="col-lg-12">
                        <button type="submit" class="btn btn-default btn-save-user ">MUDAR DE GRUPO</button>
                        <a href="{{route('permissions::backend::model.edit',array($user_id))}}"  class="btn btn-default btn-cancelar ">CANCELAR</a>
                    </div>
                        @endif
                </div>


            </form>
        </div>
    </div>




@endsection






