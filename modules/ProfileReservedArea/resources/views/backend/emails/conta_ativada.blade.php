<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<body>
    <p>Caro {{ $user->profileReservedArea()->first()->firstname}} {{$user->profileReservedArea()->first()->lastname}},</p>

    <p>O seu registo foi validado com sucesso.</p>

    <p>A partir deste momento pode participar nos nossos leilões.</p>

    <p>A nossa equipa deseja que faça bons negócios.</p>

    <p>Atenciosamente,</p>
    <p>Vias e Rumos, Lda.</p>
</body>
</html>
