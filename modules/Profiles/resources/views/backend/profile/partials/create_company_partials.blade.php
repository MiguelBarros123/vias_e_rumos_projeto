<div class="row">
    <div  class="col-lg-6 margem_20">
        <label>@lang('profiles::backend.common.emails')</label>
        <div class="row">
            <div  class="col-lg-12">
                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                    <div>
                        <div class="input_container margem_5">
                            <div class="input-group">
                                <input type="text" name="company_emails[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_emails')">
                                <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div  class="col-lg-6 margem_20">
        <label>@lang('profiles::backend.common.phones')</label>
        <div class="row">
            <div  class="col-lg-12">
                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                    <div>
                        <div class="input_container margem_5">
                            <div class="input-group">
                                <input type="text" name="company_phones[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_phone')">
                                <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="divider_line2"></div>

<div class="row">
    <div  class="col-lg-6 margem_20">
        <label>@lang('profiles::backend.common.mobile_phones')</label>
        <div class="row">
            <div  class="col-lg-12">
                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                    <div>
                        <div class="input_container margem_5">
                            <div class="input-group">
                                <input type="text" name="company_mobile[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_mobile')">
                                <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                </div>
            </div>
        </div>
    </div>

    <div  class="col-lg-6 margem_20">
        <label>@lang('profiles::backend.common.faxes')</label>
        <div class="row">
            <div  class="col-lg-12">
                <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                    <div>
                        <div class="input_container margem_5">
                            <div class="input-group">
                                <input type="text" name="company_faxes[]" class="form-control " value="{{old('subbrand')}}" placeholder="@lang('profiles::backend.common.new_fax')">
                                <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                </div>
            </div>
        </div>
    </div>

</div>
<div class="divider_line2"></div>