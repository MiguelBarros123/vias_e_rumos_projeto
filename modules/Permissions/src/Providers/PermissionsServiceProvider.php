<?php

namespace Brainy\Permissions\Providers;

use App\User;
use Brainy\Framework\Models\Group;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as LaravelProvider;

class PermissionsServiceProvider extends LaravelProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        Blade::directive('activePageTopMenu', function ($menu) {
            return "<?= (Route::currentRouteName() == $menu) ? 'ativo' : '' ?>";
        });

        Group::deleting(function ($group) {
            $group->permissions()->delete();
        });

        User::deleting(function ($user) {
            $user->details()->delete();
        });
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->bind('Brainy\Permissions\Repositories\UserDetailRepositoryInterface','Brainy\Permissions\Repositories\UserDetailRepository');
        $this->app->bind('Brainy\Permissions\Repositories\GroupRepositoryInterface','Brainy\Permissions\Repositories\GroupRepository');

    }
}
