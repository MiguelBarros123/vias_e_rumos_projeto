@extends('layouts.backend.app')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/backend.css') }}">
    <link rel="stylesheet" href="{{asset('css/profiles/backend.css')}}">
    <link rel="stylesheet" href="{{asset('back/css/select2.min.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-sanitize.js"></script>

    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
    <script src="{{asset('back/js/datatable_select_logic.js')}}"></script>
    <script src="{{asset('back/js/onload.js')}}"></script>

    <script src="{{asset('back/js/select2.min.js')}}"></script>



    <script>

        var myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                "<td> ${email} </td> " +
                "<td> ${reserved_area} </td> " +
                "<td> ${type} </td> " +
                "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                "</tr>";


        var myTemplate_unpaid_table_personalized = "<tr id='sel_item_list2_unpaid_table_personalized_${id}'>" +
                "<td> ${name} <input type='hidden' name='personalized_ids[]' value='${id}' /></td> " +
                "<td> ${template}</td>" +
                "<td class='text-center'><a id='delete_selected_item_datatable_list_unpaid_table_personalized' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                "</tr>";

        $('#toggle-user').bootstrapToggle();
        $('#toggle-profile-user').bootstrapToggle();
        $('#toggle-type').bootstrapToggle();
        $('#toggle-reserved-area').bootstrapToggle();

        if($('#toggle-user').attr('checked')){
            $('.user_definitions_profile').show();
        }

        $('#toggle-profile-user').change(function() {

            if($('#toggle-profile-user').prop('checked')){
                $('.user_definitions_profile').show();
                $('#front_groups').show();
            } else {
                $('.user_definitions_profile').hide();
                $('#front_groups').hide();
            }

        });

        if($('#toggle-reserved-area').prop('checked') == true){
            myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                    "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                    "<td> ${email} </td> " +
                    "<td><span>Não</span><input id='toggle-profile_ra_${id}' type='checkbox' data-row='${id}' data-on=' ' data-off=' ' name='profile_ra[${id}]' data-style='ios new_toggle' ><span>Sim</span> <input type='hidden' id='reserved_area_${id}' value='${reserved_area}' /></td>"+
                    "<td> ${type} </td> " +
                    "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                    "</tr>";
        } else {
            myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                    "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                    "<td> ${email} </td> " +
                    "<td> ${reserved_area} </td> " +
                    "<td> ${type} </td> " +
                    "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                    "</tr>";
        }

        $('#toggle-user').change(function() {
            $('.user_definitions_profile').fadeToggle();
        });

        $('#toggle-profile-user').prop('checked', false);
        $('#profile_type').hide();
        $('#front_groups').show();
        $('#form_messages').hide();

        $(document).ready(function() {

            $(document).on('submit', '.form_new_profile', function(event) {
                event.preventDefault();
                var urlTo=$(this).attr('action');
                var data=$(this).serialize();

                $.ajax({
                    type:'post',
                    url:urlTo,
                    data:data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    beforeSend:function() {
                        $('#new_profile_submit').button('loading');
                    },
                    success:function (data) {

                        if(data.profile){
                            console.log('data', data.profile);

                            var id = data.profile.id,
                                    name = data.profile.name,
                                    surname = data.profile.surname,
                                    defaultemail = data.profile.defaultemail,
                                    reserved_area = data.profile.reserved_area,
                                    type = data.profile.type;

                            options.datatable.rows.add([{
                                'id': id,
                                'name': '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>'+ name +' '+ surname,
                                'email': defaultemail,
                                'type': type,
                                'reserved_area': reserved_area,
                                'already_selected': 0
                            }]).draw(false);
                        }


                        $('#new_profile_submit').button('reset');

                        if(data.type === 'success'){
                            $('.alert-dismissible').addClass('alert-success');
                            $('.alert-dismissible').removeClass('alert-erro');
                        } else {
                            $('.alert-dismissible').addClass('alert-erro');
                            $('.alert-dismissible').removeClass('alert-success');
                        }

                        $('.alert-dismissible .data_message').html(data.message);

                        $('#form_messages').show();

                    }
                });
            });

            $(document).on('change', '#toggle-type', function () {
                $('#profile_type').fadeToggle();
                $('#toggle-profile-user').prop('checked', $(this).prop('checked')).change();
            });

            $(".company_sector").select2();
            $(".front_groups").select2({
                placeholder: "{{ trans('profiles::backend.common.select_group') }}",
                allowClear: true
            });
            $(".back_groups").select2();

            $('.more_button').click(function(){
                var pai=$(this).prev();
                if(pai.children().length < 5) {
                    var cloned = pai.children().first().clone();
                    cloned.find('input').val('');
                    cloned.appendTo(pai);
                }
            });

            var total_addresses="{{$addresses->count()}}";
            $('.more_button_address').click(function(){
                var pai=$('.ac_children');
                var cloned=pai.children().first().clone();
                cloned.find('input').val('');
                cloned.find('.ac_anchor').attr('href','#collapseOne'+total_addresses);
                cloned.find('.ac_anchor_child').attr('id','collapseOne'+total_addresses);
                cloned.find('.send_address').attr('name', 'moradas['+total_addresses+'][moradas_send_address]');
                cloned.find('.send_zip_code').attr('name', 'moradas['+total_addresses+'][moradas_send_zip_code]');
                cloned.find('.send_company').attr('name', 'moradas['+total_addresses+'][moradas_send_company]');
                cloned.find('.send_locality').attr('name', 'moradas['+total_addresses+'][moradas_send_locality]');
                cloned.find('.send_phone').attr('name', 'moradas['+total_addresses+'][moradas_send_phone]');
                cloned.find('.send_city').attr('name', 'moradas['+total_addresses+'][moradas_send_city]');
                cloned.find('.send_country').attr('name', 'moradas['+total_addresses+'][moradas_send_country]');
                cloned.appendTo(pai);
                total_addresses++;
            });

            var profile_type = '{{ $profile->type }}';
            $('#front_groups').hide();
            $('#back_groups').hide();

            if(profile_type === 'login_user' || profile_type === 'reserved_area_user'){
                $('#back_admin_user').show();
                $('#front_groups').show();
            } else if(profile_type === 'back_user'){
                $('#back_admin_user').show();
                $('#back_groups').show();
            }


            $('#type_of_user').change(function(){
                $('#back_admin_user').hide();
                $('#front_groups').hide();
                $('#back_groups').hide();
                if($(this).val()==='reserved_area_user' || $(this).val()==='login_user'){
                    $('#back_admin_user').show();
                    $('#front_groups').show();
                }

                if($(this).val()==='back_user'){
                    $('#back_admin_user').show();
                    $('#back_groups').show();
                }
            });

            $(document).on('click','.input-group-addon',function(){
                if($(this).parent().parent().parent().children().length > 1){
                    $(this).parent().parent().remove();
                }

            });
            $(document).on('click','.inline-block-delete',function(){
                if($(this).parent().parent().parent().parent().children().length > 1){
                    $(this).parent().parent().parent().remove();
                }

            });

            var sugest=$('#magicsuggest-faq').magicSuggest({
                placeholder: "",
                method: 'get',
                data: "{{route('framework::framework.tags.index')}}",
                allowDuplicates: false,
                valueField: 'name'
            });

            sugest.setSelection(jQuery.parseJSON('{!! $profile->mytagsnames()->toJson() !!}'));

            var sugest_company = $('#magicsuggest-company').magicSuggest({
                placeholder: "",
                method: 'get',
                data: "{{ route('profiles::backend::profiles.ajax.companies') }}",
                allowDuplicates: false,
                valueField: 'id',
                maxSelection: 1
            });

            sugest_company.setSelection(jQuery.parseJSON('{!! $profile->mycompany()->toJson() !!}'));

            var selectLogic = '';

            var options = {
                rota: "{{ route('profiles::backend::profiles.ajax.profiles', [$profile->id])}}",
                colunas: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'type', name: 'type'},
                    {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                        //for already selected rows
                        $(nRow).attr('id', 'sel_row_'+aData['id']);

                        if( aData['already_selected']==1){
                            options.datatable.row(nRow).select();
                        }

                    },
                    initComplete: function(settings, json) {
                        $(function() {
                            $.logicDatatable(options);
                            selectLogic = $.selectDatatableLogic(options);

                            $("[id*='toggle-profile_ra_']").bootstrapToggle();

                            changeState();

                        })
                    }



                },
                searchableElementId: 'ordered_search'
            };


            var options2 = {
                rota: "{{ route('profiles::backend::profiles.custom_fields.index_custom_fields_ajax',[$profile->id])}}",
                colunas: [
                    {data: 'name', name: 'name'},
                    {data: 'type', name: 'type'},
                    {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                        //for already selected rows
                        $(nRow).attr('id', 'sel_row_'+aData['id']);
                        if( aData['already_selected']==1){
                            options2.datatable.row(nRow).select();
                        }

                    },

                    initComplete: function(settings, json) {
                        $(function() {
                            $.logicDatatable(options2);
                            selectLogic = $.selectDatatableLogic(options2);

                        })
                    }



                },
                searchableElementId: 'ordered_search2'
            };
            $('#unpaid_table_personalized').mydatatable(options2);
            $('#unpaid_table').mydatatable(options);

            $('#select_resource_unpaid_table_personalized').on('shown.bs.modal', function (e) {

                options2.datatable.columns.adjust().draw();

            });


            $('#select_resource_unpaid_table').on('shown.bs.modal', function (e) {

                options.datatable.columns.adjust().draw();

            });

            $('#select_resource_unpaid_table').on('hidden.bs.modal', function (e) {

                $("[id*='toggle-profile_ra_']").bootstrapToggle();

                changeState();

            });

            $('#toggle-reserved-area').on('change', function () {

                if($(this).prop('checked') == true){
                    myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                            "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                            "<td> ${email} </td> " +
                            "<td><span>Não</span><input id='toggle-profile_ra_${id}' type='checkbox' data-row='${id}' data-on=' ' data-off=' ' name='profile_ra[${id}]' data-style='ios new_toggle' ><span>Sim</span> <input type='hidden' id='reserved_area_${id}' value='${reserved_area}' /></td>"+
                            "<td> ${type} </td> " +
                            "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                            "</tr>";
                } else {
                    myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                            "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                            "<td> ${email} </td> " +
                            "<td> ${reserved_area} </td> " +
                            "<td> ${type} </td> " +
                            "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                            "</tr>";
                }

                selectLogic.redraw();
                $("[id*='toggle-profile_ra_']").bootstrapToggle();

                changeState();

            });

            var type = '{{ $type }}';

            $('#user-image').on('click', function (e) {
                e.preventDefault();
                $('#input_image').click();
            });

            $("#input_image").on('change', function(){
                readURL(this);
            });

            var company_group = $('.form_company_group .front_groups').val();

            if(company_group != ''){
                var rotaForGroup="{{route('profiles::backend::profiles.get_group',['%group_id%'])}}";
                var urlTo = rotaForGroup.replace('%group_id%', company_group);

                $.get(urlTo, function (data) {

                    $('.group_message').html('');
                    $('.group_message').html(data.message);

                    if(data.reserved_area == true){
                        myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                                "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                                "<td> ${email} </td> " +
                                "<td><span>Não</span><input id='toggle-profile_ra_${id}' type='checkbox' data-row='${id}' data-on=' ' data-off=' ' name='profile_ra[${id}]' data-style='ios new_toggle' ><span>Sim</span> <input type='hidden' id='reserved_area_${id}' value='${reserved_area}' /></td>"+
                                "<td> ${type} </td> " +
                                "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                                "</tr>";
                    } else {
                        myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                                "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                                "<td> ${email} </td> " +
                                "<td> ${reserved_area} </td> " +
                                "<td> ${type} </td> " +
                                "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                                "</tr>";
                    }

                    $("[id*='toggle-profile_ra_']").bootstrapToggle();

                });
            }

            $('.form_company_group .front_groups').on('change', function(){
                var group_value = $(this).val();

                if(group_value != ''){
                    console.log('group', group_value);

                    var rotaForGroup="{{route('profiles::backend::profiles.get_group',['%group_id%'])}}";
                    var urlTo = rotaForGroup.replace('%group_id%', group_value);

                    $.get(urlTo, function (data) {

                        $('.group_message').html('');
                        $('.group_message').html(data.message);

                        if(data.reserved_area == true){
                            myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                                    "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                                    "<td> ${email} </td> " +
                                    "<td><span>Não</span><input id='toggle-profile_ra_${id}' type='checkbox' data-row='${id}' data-on=' ' data-off=' ' name='profile_ra[${id}]' data-style='ios new_toggle' ><span>Sim</span> <input type='hidden' id='reserved_area_${id}' value='${reserved_area}' /></td>"+
                                    "<td> ${type} </td> " +
                                    "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                                    "</tr>";
                        } else {
                            myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                                    "<td> ${name} <input type='hidden' name='profile_ids[]' value='${id}' /></td> " +
                                    "<td> ${email} </td> " +
                                    "<td> ${reserved_area} </td> " +
                                    "<td> ${type} </td> " +
                                    "<td class='text-right'><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                                    "</tr>";
                        }

                        selectLogic.redraw();
                        $("[id*='toggle-profile_ra_']").bootstrapToggle();

                    });

                }

            });

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    document.getElementById('image_user_picked').src = e.target.result;
                    $('.profile_no_pic').hide();
                    $('.image_user_picked').css('display', 'inline-block');
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        function changeState() {
            $.each($("[id*='reserved_area_']"), function (value) {
                if($(this).val() == 1){
                    $(this).siblings('div.toggle').children('input').bootstrapToggle('on');
                }
            });
        }

    </script>

@endsection

@section('content')

    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">
                        @if($type=='person')
                            <li><a href="{{route('profiles::backend::profiles.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                        @else
                            <li><a href="{{route('profiles::backend::companies.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                        @endif
                    </ul>
                </div>

            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.profiles.edit',[$profile,$type]) !!}

            <div class="row">
                <form  method="POST" action="{{route('profiles::backend::profiles.update',[$profile->id])}}" autocomplete="off" enctype="multipart/form-data">
                    <input  type="hidden" name="_method" value="put"/>
                    <input type="hidden" name="is_company" value="{{$profile->type}}">
                    {!! csrf_field() !!}
                    <div class="col-lg-12">
                        <div class="panel ">
                            <div class="panel-titulo">
                                <span class="titulo"><i class="icon-editar-utilizador icon20"></i>@lang('profiles::backend.common.edit_profile')</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div  class="col-lg-4">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('name') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.name')</label>
                                            <input type="text" name="name" class="form-control " value="{{$profile->name}}">
                                            {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                    </div>
                                    @if($type === 'person')
                                        <div  class="col-lg-4">
                                            <div class="form-group fom-style-hidden2 @if($errors->first('surname') ) has-error @endif">
                                                <label>@lang('profiles::backend.common.surname')</label>
                                                <input type="text" name="surname" class="form-control " value="{{$profile->surname}}">
                                                {!! $errors->first('surname','<span class="validator_errors">:message</span>')!!}
                                            </div>
                                        </div>
                                    @else
                                        <div  class="col-lg-4">
                                            <div class="form-group fom-style-hidden2 @if($errors->first('url') ) has-error @endif">
                                                <label>@lang('profiles::backend.common.url')</label>
                                                <input type="text" name="url" class="form-control " value="{{ $profile->url }}">
                                                {!! $errors->first('url','<span class="validator_errors">:message</span>')!!}
                                            </div>
                                        </div>
                                    @endif
                                    <div  class="col-lg-4">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">

                                            <label>@lang('profiles::backend.common.add_image')</label>
                                            <div>
                                                @if($profile->thumbnail!='')
                                                    <div class="pic-circle-corner2"><img id="image_user_picked" class="image_user_picked2" src="{{route('gallery::frontend::storage_image_p',[$profile->thumbnail,50,50])}}" /></div>
                                                @else
                                                    <div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>
                                                    <div class="pic-circle-corner2 image_user_picked "><img id="image_user_picked" class="image_user_picked2" src="{{route('gallery::frontend::storage_image_p',[$profile->thumbnail,50,50])}}" /></div>
                                                @endif
                                                <button id="user-image" type="button" class="btn btn-default btn-cancelar">UPLOAD</button>
                                                <input type="file" id="input_image" name="image_id" />
                                            </div>

                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div  class="col-lg-4">
                                        <label>@lang('profiles::backend.common.birth_date')</label>
                                        <div class="form-inline">
                                            @php
                                                $date = explode("-", date("Y-n-j"));
                                                if($profile->birth_at != null){
                                                    $date = explode("-", $profile->birth_at);
                                                }
                                            @endphp
                                            <div class="form-group fom-style-hidden2 @if($errors->first('birth_at') ) has-error @endif">

                                                <select class="form-control " name="birth_day">
                                                    @for($i=1; $i <= 31 ; $i++)
                                                        <option value="{{$i}}" {{ ($date[2] == $i) ? 'selected': ''}}>{{$i}}</option>
                                                    @endfor
                                                </select>
                                                {!! $errors->first('birth_at','<span class="validator_errors">:message</span>')!!}

                                                @include('layouts.backend.partials.select_input', ['array'=>$months, 'name'=>'birth_month', 'selected'=> $date[1]])

                                                {!! $errors->first('birth_at','<span class="validator_errors">:message</span>')!!}

                                                @php
                                                $dt      = Carbon\Carbon::now();
                                                $start= Carbon\Carbon::now()->subYears(100);

                                                @endphp
                                                <select class="form-control " name="birth_year">
                                                    @for($i=$dt->year; $i >= $start->year ; $i--)
                                                        <option value="{{$i}}" {{ ($date[0] == $i) ? 'selected': ''}}>{{$i}}</option>
                                                    @endfor
                                                </select>
                                                {!! $errors->first('birth_at','<span class="validator_errors">:message</span>')!!}
                                            </div>
                                        </div>

                                    </div>



                                    @if($type=='person')
                                    <div  class="col-lg-4">
                                        <label>@lang('profiles::backend.common.gender')</label>
                                        <div class="form-inline">

                                            <div class="form-group fom-style-hidden2 margin-right-20 @if($errors->first('subbrand') ) has-error @endif">
                                                <label> <input type="radio" name="gender" value="male" {{($profile->gender === 'male') ? 'checked': ''}}> Masculino </label>
                                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                            </div>
                                            <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                                <label><input type="radio" name="gender" value="female" {{($profile->gender === 'female') ? 'checked': ''}}> Feminino</label>
                                                {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                            </div>
                                        </div>

                                    </div>
                                    @endif

                                    <div  class="col-lg-4">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.language')</label>

                                            @include('layouts.backend.partials.select_input', ['array'=>$languages, 'name'=>'language', 'selected'=> $profile->language])

                                            {!! $errors->first('subbrand','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div  class="col-lg-4">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('nif') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.nif')</label>
                                            <input type="text" name="nif" class="form-control " value="{{$profile->nif}}">
                                            {!! $errors->first('nif','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                    </div>
                                </div>
                                <div class="divider_line2"></div>



                                @if($type=='person')
                                <div class="row">
                                    <div  class="col-lg-12 margem_20">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.is_user')</label>
                                            <div>
                                                <span>Não</span><input id="toggle-user" type="checkbox" {{ ($profile->type !== 'no_login_user') ? 'checked': ''}} data-on=" " data-off=" " name="is_user" data-style="ios new_toggle"><span>Sim</span>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="user_definitions_profile">
                                        <div class="row no_margin_row">
                                            <div class="col-lg-4">
                                                <div class="form-group fom-style-hidden2">
                                                    <label for="exampleInputEmail1">@lang('profiles::backend.common.type')</label>
                                                    <select class="form-control " id="type_of_user" name="type">
                                                        <optgroup label="FrontOffice">
                                                            <option {{ ($profile->type === 'login_user') ? 'selected="selected"' : ''}} value="login_user">Conta de utilizador FrontOffice</option>
                                                        </optgroup>
                                                        @if($back_groups->count() >0)
                                                            <optgroup label="BackOffice">
                                                                <option {{ ($profile->type === 'back_user') ? 'selected="selected"' : ''}} value="back_user">Conta de utilizador BackOffice</option>
                                                            </optgroup>
                                                        @endif

                                                    </select>
                                                </div>
                                            </div>

                                        </div>

                                        @if($profile->type  == 'no_login_user')
                                            <div class="row no_margin_row">
                                                <div  class="col-lg-4 ">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('email') ) has-error @endif">
                                                        <label>@lang('profiles::backend.common.email')</label>
                                                        <input type="text" name="email" class="form-control " value="{{old('email')}}">
                                                        {!! $errors->first('email','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                                <div  class="col-lg-4 ">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('password') ) has-error @endif">
                                                        <label>@lang('profiles::backend.common.password')</label>
                                                        <input type="text" name="password" class="form-control " value="{{str_random()}}">
                                                        {!! $errors->first('password','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        <div id="front_groups">
                                            <div class="row no_margin_row">
                                                <div  class="col-lg-4">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                                        <label>@lang('profiles::backend.common.group_front')</label>
                                                        <div>
                                                            @if($profile->type=='login_user')
                                                                @include('layouts.backend.partials.select_object_placeholder', ['array'=>$front_groups, 'name'=>'front_group_id', 'selected'=> $profile->profile_group_id, 'class'=> 'front_groups'])
                                                            @else
                                                                @include('layouts.backend.partials.select_object_placeholder', ['array'=>$front_groups, 'name'=>'front_group_id', 'selected'=> '', 'class'=> 'front_groups'])
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div  id="back_groups">
                                            <div class="row no_margin_row">
                                                <div  class="col-lg-4">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                                        <label>@lang('profiles::backend.common.group_front')</label>
                                                        <div>
                                                            @if($profile->type=='back_user')
                                                                @include('layouts.backend.partials.select_object_placeholder', ['array'=>$front_groups, 'name'=>'front_group_id', 'selected'=> $profile->profile_group_id, 'class'=> 'back_groups'])
                                                            @else
                                                                @include('layouts.backend.partials.select_object_placeholder', ['array'=>$front_groups, 'name'=>'front_group_id', 'selected'=> '', 'class'=> 'back_groups'])
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div  class="col-lg-4">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                                        <label>@lang('profiles::backend.common.group_admin')</label>
                                                        <div>
                                                            @if($profile->type=='back_user')
                                                                @include('layouts.backend.partials.select_object_input', ['array'=>$back_groups, 'name'=>'back_group_id', 'selected'=> $profile->user->details->group_id, 'class'=> 'back_groups'])
                                                            @else
                                                                @include('layouts.backend.partials.select_object_input', ['array'=>$back_groups, 'name'=>'back_group_id', 'selected'=> '', 'class'=> 'back_groups'])
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </div>







                                </div>
                                <div class="divider_line2"></div>
                                @endif


                                <div class="row">
                                    <div  class="col-lg-12 margem_20">
                                        <label>@lang('profiles::backend.menu.personalized_fields')</label>
                                    </div>
                                    <div  class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                                                <a class="add_btn" data-target="#select_resource_unpaid_table_personalized" data-toggle="modal">
                                                    <i class="icon-adicionar icon10"></i>
                                                    @lang('profiles::backend.common.select_personalized_fields')
                                                </a>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-lg-12 no_padding">
                                                <table id="dhl_collection" class="table default-table_2 dataTable min_padding_table">
                                                    <thead>
                                                    <tr>
                                                        <th>@lang('profiles::backend.common.name')</th>
                                                        <th>@lang('profiles::backend.common.value')</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="initial_table_unpaid_table_personalized">

                                                    </tbody>
                                                </table>
                                                <div class="empty_list" id="empty_list_unpaid_table_personalized">@lang('profiles::backend.common.empty_personalized')</div>

                                            </div>

                                        </div>



                                    </div>

                                </div>

                                <div class="divider_line2"></div>

                                @if($type=='person')
                                <div class="row">
                                    <div  class="col-lg-12 margem_20">
                                        <label>@lang('profiles::backend.common.personal_info')</label>
                                    </div>
                                    <div  class="col-lg-6">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('company_id') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.company')</label>
                                            <input class="form-control" id="magicsuggest-company" name="company_id">
                                            {!! $errors->first('company_id','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                    </div>
                                    <div  class="col-lg-6">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('job') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.work')</label>
                                            <input type="text" name="job" class="form-control " value="{{$profile->job}}">
                                            {!! $errors->first('job','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                    </div>

                                </div>
                                @else
                                    <div class="row">
                                        <div  class="col-lg-12 margem_20">
                                            <label>@lang('profiles::backend.common.info')</label>
                                        </div>
                                        <div  class="col-lg-6">
                                            <div class="form-group fom-style-hidden2 @if($errors->first('job') ) has-error @endif">
                                                <label>@lang('profiles::backend.common.sector')</label>
                                                <input type="text" name="setor" class="form-control" value="{{$profile->setor}}">

                                                {!! $errors->first('job','<span class="validator_errors">:message</span>')!!}
                                            </div>
                                        </div>
                                    </div>
                                    @endif
                                <div class="divider_line2"></div>


                                @if($type=='company')
                                    @include('profiles::backend.profile.partials.edit_company_partials')
                                @else
                                    @include('profiles::backend.profile.partials.edit_profile_partials')
                                @endif



                                <div class="row">
                                    <div  class="col-lg-12 margem_20">
                                        <label for="exampleInputEmail1">@lang('profiles::backend.common.addresses')</label>
                                        <div class="panel-group margem_20" id="accordion" role="tablist" aria-multiselectable="true">
                                            <div class="ac_children">

                                                @if($addresses->count())
                                                    @foreach($addresses as $key => $address)
                                                        <div class="pannel_address">
                                                            <div class="panel-heading" role="tab">
                                                                <h4 class="panel-title">
                                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" class="ac_anchor" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                        Nova Morada
                                                                    </a>
                                                                    <a class="pull-right inline-block-delete">
                                                                        <i class="icon-opcoes-eliminar"></i>
                                                                    </a>
                                                                </h4>
                                                            </div>
                                                            <div id="collapseOne" class="panel-collapse collapse in ac_anchor_child" role="tabpanel" aria-labelledby="headingOne">
                                                                <div class="panel-body">

                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group fom-style-hidden3 @if($errors->first('send_address')) has-error @endif">
                                                                                <label for="send_address">@lang('cms::frontend.model.common.address')</label>
                                                                                <input type="text" name="moradas[{{$key}}][moradas_send_address]" class="form-control send_address"
                                                                                       value="{{ $address->address }}">
                                                                                {!! $errors->first('send_address','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group fom-style-hidden3 @if($errors->first('send_zip_code')) has-error @endif">
                                                                                <label for="send_zip_code">@lang('cms::frontend.model.common.zipcode')</label>
                                                                                <input type="text" name="moradas[{{$key}}][moradas_send_zip_code]" class="form-control send_zip_code"
                                                                                       value="{{ $address->zipcode }}">
                                                                                {!! $errors->first('send_zip_code','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group fom-style-hidden3 @if($errors->first('send_company')) has-error @endif">
                                                                                <label for="send_company">@lang('cms::frontend.model.common.company')</label>
                                                                                <input type="text" name="moradas[{{$key}}][moradas_send_company]" class="form-control send_company"
                                                                                       value="{{ $address->company }}">
                                                                                {!! $errors->first('send_company','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group fom-style-hidden3 @if($errors->first('send_locality')) has-error @endif">
                                                                                <label for="send_locality">@lang('cms::frontend.model.common.town')</label>
                                                                                <input type="text" name="moradas[{{$key}}][moradas_send_locality]" class="form-control send_locality"
                                                                                       value="{{ $address->town }}">
                                                                                {!! $errors->first('send_locality','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group fom-style-hidden3 @if($errors->first('send_phone')) has-error @endif">
                                                                                <label for="send_phone">@lang('cms::frontend.model.common.phone')</label>
                                                                                <input type="text" name="moradas[{{$key}}][moradas_send_phone]" class="form-control send_phone"
                                                                                       value="{{ $address->phone }}">
                                                                                {!! $errors->first('send_phone','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group fom-style-hidden3 @if($errors->first('send_city')) has-error @endif">
                                                                                <label for="send_city">@lang('cms::frontend.model.common.city')</label>
                                                                                <input type="text" name="moradas[{{$key}}][moradas_send_city]" class="form-control send_city"
                                                                                       value="{{ $address->city }}">
                                                                                {!! $errors->first('send_city','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group fom-style-hidden3 @if($errors->first('send_country')) has-error @endif">
                                                                                <label for="send_country">@lang('cms::frontend.model.common.country')</label>
                                                                                <select name="moradas[{{$key}}][moradas_send_country]" class="form-control send_country">
                                                                                    <option></option>
                                                                                    @foreach($countries as $country)
                                                                                        <option value="{{ $country->index }}" {{ ($address->country === $country->value) ? 'selected': ''}}>{{ $country->value }}</option>
                                                                                    @endforeach
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>



                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                @else
                                                    <div class="pannel_address">
                                                        <div class="panel-heading" role="tab">
                                                            <h4 class="panel-title">
                                                                <a role="button" data-toggle="collapse" data-parent="#accordion" class="ac_anchor" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                                    Nova Morada
                                                                </a>
                                                                <a class="pull-right inline-block-delete">
                                                                    <i class="icon-opcoes-eliminar"></i>
                                                                </a>
                                                            </h4>
                                                        </div>
                                                        <div id="collapseOne" class="panel-collapse collapse in ac_anchor_child" role="tabpanel" aria-labelledby="headingOne">
                                                            <div class="panel-body">

                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group fom-style-hidden3 @if($errors->first('send_address')) has-error @endif">
                                                                            <label for="send_address">@lang('cms::frontend.model.common.address')</label>
                                                                            <input type="text" name="moradas[0][moradas_send_address]" class="form-control send_address"
                                                                                   value="{{ old('send_address') }}">
                                                                            {!! $errors->first('send_address','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group fom-style-hidden3 @if($errors->first('send_zip_code')) has-error @endif">
                                                                            <label for="send_zip_code">@lang('cms::frontend.model.common.zipcode')</label>
                                                                            <input type="text" name="moradas[0][moradas_send_zip_code]" class="form-control send_zip_code"
                                                                                   value="{{ old('send_zip_code') }}">
                                                                            {!! $errors->first('send_zip_code','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group fom-style-hidden3 @if($errors->first('send_company')) has-error @endif">
                                                                            <label for="send_company">@lang('cms::frontend.model.common.company')</label>
                                                                            <input type="text" name="moradas[0][moradas_send_company]" class="form-control"
                                                                                   value="{{ old('send_company') }}">
                                                                            {!! $errors->first('send_company','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group fom-style-hidden3 @if($errors->first('send_locality')) has-error @endif">
                                                                            <label for="send_locality">@lang('cms::frontend.model.common.town')</label>
                                                                            <input type="text" name="moradas[0][moradas_send_locality]" class="form-control send_locality"
                                                                                   value="{{ old('send_locality') }}">
                                                                            {!! $errors->first('send_locality','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group fom-style-hidden3 @if($errors->first('send_phone')) has-error @endif">
                                                                            <label for="send_phone">@lang('cms::frontend.model.common.phone')</label>
                                                                            <input type="text" name="moradas[0][moradas_send_phone]" class="form-control send_phone"
                                                                                   value="{{ old('send_phone') }}">
                                                                            {!! $errors->first('send_phone','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group fom-style-hidden3 @if($errors->first('send_city')) has-error @endif">
                                                                            <label for="send_city">@lang('cms::frontend.model.common.city')</label>
                                                                            <input type="text" name="moradas[0][moradas_send_city]" class="form-control send_city"
                                                                                   value="{{ old('send_city') }}">
                                                                            {!! $errors->first('send_city','<span class="validator_errors">'.trans("cms::frontend.messages.required.field").'</span>')!!}
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group fom-style-hidden3 @if($errors->first('send_country')) has-error @endif">
                                                                            <label for="send_country">@lang('cms::frontend.model.common.country')</label>
                                                                            <select name="moradas[0][moradas_send_country]" class="form-control send_country">
                                                                                <option></option>
                                                                                @foreach($countries as $country)
                                                                                    <option value="{{ $country->index }}">{{ $country->value }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>



                                                            </div>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>

                                            <button class="btn btn-default btn-yellow-icon margem_5 more_button_address" type="button"><i class="icon-adicionar"></i></button>
                                        </div>
                                    </div>

                                </div>
                                <div class="divider_line2"></div>


                                <div class="row">
                                    <div  class="col-lg-12 margem_20">
                                        <div class="form-group fom-style-hidden2">
                                            <label for="exampleInputEmail1">Tags</label>
                                            <input class="form-control" id="magicsuggest-faq" name="tags">
                                        </div>
                                    </div>

                                </div>
                                <div class="divider_line2"></div>

                                <div class="row">
                                    <div  class="col-lg-12 margem_20">
                                        <label for="exampleInputEmail1">@lang('profiles::backend.common.social_networks')</label>
                                    </div>

                                    <div  class="col-lg-3 ">
                                        <div class="form-group fom-style-hidden2">
                                            <label for="exampleInputEmail1">Facebook</label>
                                            @php
                                                $facebook = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'facebook')->first();
                                            @endphp

                                            @if($facebook)
                                                <input type="text" name="social_facebook" class="form-control " value="{{$facebook->contact}}">
                                            @else
                                                <input type="text" name="social_facebook" class="form-control " value="{{old('subbrand')}}">
                                            @endif

                                        </div>
                                    </div>

                                    <div  class="col-lg-3 ">
                                        <div class="form-group fom-style-hidden2">
                                            <label for="exampleInputEmail1">Instagram</label>
                                            @php
                                                $instagram = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'instagram')->first();
                                            @endphp

                                            @if($instagram)
                                                <input type="text" name="social_instagram" class="form-control " value="{{$instagram->contact}}">
                                            @else
                                                <input type="text" name="social_instagram" class="form-control " value="{{old('subbrand')}}">
                                            @endif

                                        </div>
                                    </div>

                                    <div  class="col-lg-3 ">
                                        <div class="form-group fom-style-hidden2">
                                            <label for="exampleInputEmail1">Google +</label>
                                            @php
                                                $google = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'google')->first();
                                            @endphp

                                            @if($google)
                                                <input type="text" name="social_google" class="form-control " value="{{$google->contact}}">
                                            @else
                                                <input type="text" name="social_google" class="form-control " value="{{old('subbrand')}}">
                                            @endif

                                        </div>
                                    </div>

                                    <div  class="col-lg-3 ">
                                        <div class="form-group fom-style-hidden2">
                                            <label for="exampleInputEmail1">Youtube</label>
                                            @php
                                                $youtube = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'youtube')->first();
                                            @endphp

                                            @if($youtube)
                                                <input type="text" name="social_youtube" class="form-control " value="{{$youtube->contact}}">
                                            @else
                                                <input type="text" name="social_youtube" class="form-control " value="{{old('subbrand')}}">
                                            @endif

                                        </div>
                                    </div>

                                    <div class="col-lg-12">
                                        <div>
                                            <label for="exampleInputEmail1">@lang('profiles::backend.common.other')</label>
                                        </div>
                                        <div >
                                            @php
                                                $other = \Brainy\Profiles\Models\ProfileContact::where('profile_id', $profile->id)->where('type', 'other')->get();
                                            @endphp

                                            @if($other->count())
                                                @foreach($other as $o)
                                                    <div class="input_container margem_5">
                                                        <div class="input-group">
                                                            <input type="text" name="other[]" class="form-control " value="{{$o->contact}}" placeholder="@lang('profiles::backend.common.other')">
                                                            <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                                            {!! $errors->first('other','<span class="validator_errors">:message</span>')!!}
                                                        </div>
                                                    </div>
                                                @endforeach
                                            @else
                                                <div class="input_container margem_5">
                                                    <div class="input-group">
                                                        <input type="text" name="other[]" class="form-control " value="{{old('other')}}" placeholder="@lang('profiles::backend.common.other')">
                                                        <div class="input-group-addon"><i class="icon-opcoes-eliminar"></i></div>
                                                        {!! $errors->first('other','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                            @endif


                                        </div>
                                        <button class="btn btn-default btn-yellow-icon margem_5 more_button" type="button"><i class="icon-adicionar"></i></button>
                                    </div>
                                </div>



                            </div>
                        </div>
                        </div>

                        @if($type=='company')
                            <div class="col-lg-12">
                                <div class="panel ">
                                    <div class="panel-titulo">
                                        <span class="titulo"><i class="icon-editar-utilizador icon20"></i>@lang('profiles::backend.common.functionaries')</span>
                                    </div>
                                    <div class="panel-body">

                                        <div class="row">
                                            <div  class="col-lg-4">
                                                <div class="form-group fom-style-hidden2 form_company_group @if($errors->first('subbrand') ) has-error @endif">
                                                    <label>@lang('profiles::backend.common.group_front')</label>
                                                    <div>
                                                        @include('layouts.backend.partials.select_object_placeholder', ['array'=>$front_groups, 'name'=>'front_group_id', 'selected'=> $profile->profile_group_id, 'class'=> 'front_groups'])
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel_groups">
                                                    <div class="group_message">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6 no_padding_left">
                                                <a class="add_btn" data-target="#select_resource_unpaid_table" data-toggle="modal">
                                                    <i class="icon-adicionar icon10"></i>

                                                    @lang('profiles::backend.common.select_profiles')
                                                </a>
                                            </div>
                                            <div class="col-lg-6 no_padding_right">
                                                <a class="add_btn" data-target="#new_profile" data-toggle="modal">
                                                    <i class="icon-adicionar icon10"></i>
                                                    @lang('profiles::backend.common.add_new_profile')
                                                </a>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-lg-12 no_padding">
                                                <table id="dhl_collection" class="table default-table_2 dataTable min_padding_table">
                                                    <thead>
                                                    <tr>
                                                        <th>@lang('profiles::backend.common.name')</th>
                                                        <th>@lang('profiles::backend.common.email')</th>
                                                        <th>@lang('profiles::backend.common.reserved_area')</th>
                                                        <th>@lang('profiles::backend.common.type')</th>
                                                        <th></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="initial_table_unpaid_table">

                                                    </tbody>
                                                </table>
                                                <div class="empty_list" id="empty_list_unpaid_table">@lang('profiles::backend.common.empty_profiles')</div>

                                            </div>

                                        </div>


                                    </div>
                                </div>


                            </div>
                        @endif

                    <div class="col-lg-12">
                        <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('profiles::backend.common.save')</button>
                        <button type="submit" name="submit" value="stay" class="btn btn-default btn-yellow btn-stay btn-submit-all">@lang('profiles::backend.common.save_and_stay')</button>
                    @if($type === "person")
                            <a href="{{route('profiles::backend::profiles.index')}}" class="btn btn-default btn-cancelar">@lang('profiles::backend.common.cancel')</a>
                        @else
                            <a href="{{route('profiles::backend::companies.index')}}" class="btn btn-default btn-cancelar">@lang('profiles::backend.common.cancel')</a>
                        @endif
                    </div>


                </form>
            </div>
        </div>

    </div>

    @include('profiles::backend.profile.partials.add_new_profile')

    <!-- --------------------------------------------------- MODAL SELECT RECORD ---------------------------------------------------- -->
    <div class="modal modal-aux" id="select_resource_unpaid_table" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg modal-dialog-aux" role="document">      
            <div class="modal-content">
                <div class="panel-titulo"><span class="titulo"> @lang('profiles::backend.common.profiles') </span></div>
                <div class="modal-body no_padding">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 ">
                            <input id="ordered_search" class="search search_icon" type="text"
                                   placeholder="@lang('profiles::backend.common.search') ">
                        </div>
                    </div>
                    <div class="row">
                        <div  class="col-lg-12 " >
                            <table id="unpaid_table" class="table default-table explandable_table">
                                <thead>
                                <tr>
                                    <th>@lang('profiles::backend.common.name')</th>
                                    <th>@lang('profiles::backend.common.email')</th>
                                    <th>@lang('profiles::backend.common.type')</th>
                                    <th>
                                        <label class="checkbox-default">
                                            <input type='checkbox' id="sel_all_perm_unpaid_table">
                                            <span></span>
                                        </label>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-eliminar2 padding-bottom-30">
                    <button type="button" class="btn btn-default btn-yellow " id="save_selection_unpaid_table">@lang("cms::backend.model.common.save")</button>
                    <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("cms::backend.model.common.cancel") </button>
                </div>
            </div>

        </div>
    </div>
    <!-- -------------------------------------------------MODAL SELECT RECORD  --------------------------------------------------- -->


    <!-- --------------------------------------------------- MODAL SELECT PERSONALIZED FIELD ---------------------------------------------------- -->
    <div class="modal modal-aux" id="select_resource_unpaid_table_personalized" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg modal-dialog-aux" role="document">      
            <div class="modal-content">
                <div class="panel-titulo"><span class="titulo"> @lang('profiles::backend.menu.personalized_fields') </span></div>
                <div class="modal-body no_padding">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 ">
                            <input id="ordered_search2" class="search search_icon" type="text"
                                   placeholder="@lang('profiles::backend.common.search') ">
                        </div>
                    </div>
                    <div class="row">
                        <div  class="col-lg-12 " >
                            <table id="unpaid_table_personalized" class="table default-table explandable_table">
                                <thead>
                                <tr>
                                    <th>@lang('profiles::backend.common.name')</th>
                                    <th>@lang('profiles::backend.common.type')</th>
                                    <th>
                                        <label class="checkbox-default">
                                            <input type='checkbox' id="sel_all_perm_unpaid_table_personalized">
                                            <span></span>
                                        </label>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-eliminar2 padding-bottom-30">
                    <button type="button" class="btn btn-default btn-yellow " id="save_selection_unpaid_table_personalized">@lang("cms::backend.model.common.save")</button>
                    <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("cms::backend.model.common.cancel") </button>
                </div>
            </div>

        </div>
    </div>
    <!-- -------------------------------------------------MODAL SELECT PERSONALIZED FIELD  --------------------------------------------------- -->

@endsection



