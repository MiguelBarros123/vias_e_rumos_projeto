@extends('layouts.frontend.app')

@section('title', 'Vias e Rumos - ' . $sub_menu->name)

@section('content')
@include('cms::frontend.pages.layouts.biblio-menu')

<div class="empresa-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 an-article">
            <h2>{{$sub_menu->name}}</h2>
            
            {!! $sub_menu->description !!}

        </div>
    </div>
</div>
@endsection
