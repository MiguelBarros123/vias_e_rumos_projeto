/**
 * Created by User on 09-03-2017.
 */

//-----------------------------------------------------------setting the names for atributes
var id=0;
var ultimo= $('.item_for_condition[data-id=""]');
ultimo.attr('data-id',id);

if($('.item_for_condition').length > 1){
    var ordenados=$('.item_for_condition').sort(function (a, b) {

        var contentA =parseInt( $(a).attr('data-id'));
        var contentB =parseInt( $(b).attr('data-id'));
        return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
    });
    var prev=ordenados.last();

    id=parseInt(prev.attr('data-id'))+1;
    ultimo.attr('data-id',id);
}

var block_id=ultimo.parent().parent().attr('id');

ultimo.find('.name_for_first_option').attr('name','item['+block_id+'][custom_field]['+id+'][option][]');
ultimo.find('.terms_for_custom_field').attr('name','item['+block_id+'][custom_field]['+id+'][term_all][]');
ultimo.find('.select_custom_field').attr('name','item['+block_id+'][custom_field]['+id+'][term][]');
ultimo.find('.filter_field_id').attr('name','item['+block_id+'][custom_field]['+id+'][filter_field_id][]');

$('.terms_for_custom_field').on('change', function(){
    var field_id =$(this).val();
    var urlOptions = rota.replace(':id', field_id);

    var element_id = $(this).closest('.group_of_conditions').attr('id');

    var element_content = $(this).parent().next().find('.custom_options_content');

    $.get(urlOptions, function(data){
        element_content.html(data);
        element_content.find('.select_custom_field').attr('name','item['+element_id+'][custom_field]['+id+'][term][]');
    });
});

//----------------------------------------------------------end setting the names for atributes
//# sourceMappingURL=custom_field.js.map
