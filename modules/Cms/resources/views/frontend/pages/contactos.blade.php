@extends('layouts.frontend.app')

@inject('page', 'Brainy\Cms\Models\Page')

@php
$seo = $page->find(3);
@endphp

@section('title', $seo->title)
@section('description', $seo->meta_description)

@inject('inject', 'Brainy\Cms\Models\ServicePages')

@php
$content = $inject->getPageContent(3)
@endphp

<!--
divisao_ficheiro-->

@section('content')


<div class="row contacts-wrap">
	<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12 dspl_spc_btwn">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="contact-text-wrap">
                    <h3>Contactos</h3>
                </div>
            </div>
        </div>

         <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="contact-text-wrap">
                    <h4>Escritório de Leiria</h4>
                    <p>Rua Francisco Pereira da Silva, 28</p>
                    <p>2410-105 Leiria</p>
                    <p>(+351) 244 828 092</p>
                    <p>geral@viaserumos.pt</p>
                    <p><a class="move_map" lat="39.7379081" lng="-8.8085806">Ver no mapa</a></p>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="contact-text-wrap">
                    <h4>Escritório de Lisboa</h4>
                    <p>Rua Campos Júnior, 7A</p>
                    <p>1070-306 Lisboa</p>
                    <p><a class="move_map" lat="38.7284019" lng="-9.1667482">Ver no mapa</a></p>

                </div>
            </div>
        </div>
        
        <div class="form-wrap">
    		<h3 class="">Fale Connosco</h3>
            <div class="inputs-wrap">
    		<form method="POST" action="">
                {{ csrf_field() }}
                <div class="inputs">
    			<input class="c_inp contact-input" id="nome" name="nome" placeholder="Nome*" type="text"/>
    			<input class="c_inp contact-input" id="email" name="email" placeholder="E-mail*" type="email"/>
    			<input class="c_inp contact-input" id="assunto" name="assunto" placeholder="Assunto*" type="text"/>
    			<input class="c_inp contact-input" id="mensagem" name="mensagem" placeholder="Mensagem*" type="text"/>
                <div class="error-message">Preencha todos os campos</div>
                <div class="form_btn send_msg">
                    Enviar
                </div>
            </div>
    		</form>
             <div class="success-message">
                    <h2>A sua mensagem</h2><h2> foi enviada com sucesso!</h2>
                </div>
        </div>
        </div>
	</div>
	<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
		<div id="mapC">
		</div>
	</div>

</div>


@endsection

@section('module-scripts')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq6_UUr8WI2eiiKz_Ty3QWDUIJVLPVT-4"></script>

<script>

    function isEmail(email) {
      var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      return regex.test(email);
    }

    $(document).ready(function() {

        $('body').on('click', '.move_map', function(){
            var this_lat = $(this).attr('lat');
            var this_lng = $(this).attr('lng');

             map.setCenter(new google.maps.LatLng(this_lat, this_lng));


        })
        var count = 0;
        $('body').on('click','.send_msg', function(){
            count = 0;
            $('.contact-input').each(function() {
              if($(this).val() == '') {
                count = count + 1;
                $('.error-message').text('Preencha todos os campos');
                return false
              }else if(!$(this).val() == '' && $(this).attr('type') == 'email'){
               

                if(!isEmail($(this).val())){
                     count = count + 1;
                     $('.error-message').text('Introduza um endereço de email válido.');
                     return false

                }
            }
            }).promise().done(function() {
                  if( count > 0){
                    $('.error-message').css('opacity', '1');
                  }else{
                    $.ajax({
                        type: 'post',
                        url: "{{route('cms::frontend::model.send_email')}}",
                        data: {
                            nome : $('#nome').val(),
                            email : $('#email').val(),
                            assunto : $('#assunto').val(),
                            mensagem : $('#mensagem').val()
                        },
                        headers: {
                        'X-CSRF-TOKEN': "{{csrf_token()}}"
                        },
                        success: function()
                        {
                            $('.inputs').css('opacity', '0');
                        }
                    });
                   setTimeout(function() {
                        $('.success-message').css('opacity', '1');
                   }, 500);
                  }
                });

        })
    });


	       var locations = [
      ['Escritório de Leiria', 39.7379081, -8.8085806, "Rua Francisco Pereira da Silva, 28"],
      ['Escritório de Lisboa', 38.7284019, -9.1667482, "Rua Campos Júnior, 7A"]
    ];


    var lat = locations[0][1];
    var lng = locations[0][2];
    var latlng = new google.maps.LatLng(lat,lng); // Want to pass an address here
    var myOptions = {
    zoom: 15,
    scrollwheel: false,
    center: latlng,
    streetViewControl: false,
    mapTypeControl: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP,

    styles: [
                {
                    "featureType": "water",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#b5cbe4"
                        }
                    ]
                },
                {
                    "featureType": "landscape",
                    "stylers": [
                        {
                            "color": "#efefef"
                        }
                    ]
                },
                {
                    "featureType": "road.highway",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#83a5b0"
                        }
                    ]
                },
                {
                    "featureType": "road.arterial",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#bdcdd3"
                        }
                    ]
                },
                {
                    "featureType": "road.local",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#ffffff"
                        }
                    ]
                },
                {
                    "featureType": "poi.park",
                    "elementType": "geometry",
                    "stylers": [
                        {
                            "color": "#e3eed3"
                        }
                    ]
                },
                {
                    "featureType": "administrative",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": 33
                        }
                    ]
                },
                {
                    "featureType": "road"
                },
                {
                    "featureType": "poi.park",
                    "elementType": "labels",
                    "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": 20
                        }
                    ]
                },
                {},
                {
                    "featureType": "road",
                    "stylers": [
                        {
                            "lightness": 20
                        }
                    ]
                }
            ]
    };

    var map = new google.maps.Map(document.getElementById("mapC"),
            myOptions);

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });
      var link = "leilao.php";
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent("<div class='map_title'>"+locations[i][0]+"</div>"+"<div>"+locations[i][3]+"</div>");
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
</script>
@endsection
