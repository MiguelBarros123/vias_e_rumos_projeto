<!-- highlight section -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-titulo">
                <span class="titulo">
                    <i class="icon-alterar-permissoes-1 "></i> @lang($highlight->panel_name)
                </span>
            </div>
            <div class="panel-body">
                    <!-- Nav tabs -->
                    <ul class="tabs_brainy lang_tabs_h_{{ $highlight->id }}" role="tablist">
                        @foreach($locales as $loc)
                            <li role="presentation" class=""><a class="text-uppercase" href="#{{ $loc }}_h_{{ $highlight->id }}"  role="tab" data-toggle="tab">{{$loc}}</a></li>
                        @endforeach
                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content lang_content_h_{{ $highlight->id }}">
                        @foreach($locales as $loc)
                            <div role="tabpanel" class="tab-pane fade in  margem_20" id="{{ $loc }}_h_{{ $highlight->id }}">
                                <div class="form-group fom-style-hidden3 @if($errors->first('title')) has-error @endif">
                                    <label for="exampleInputEmail1">@lang('cms::backend.model.common.title')</label>
                                    <input type="text" name="highlight[{{ $highlight->id }}][{{ $loc }}][title]" class="form-control" value="{{ $highlight->translateOrNew($loc)->title }}">
                                    {!! $errors->first('title','<span class="validator_errors">:message</span>')!!}
                                </div>
                                <div class="form-group fom-style-hidden3 @if($errors->first('description')) has-error @endif">
                                    <label for="exampleInputEmail1">@lang('cms::backend.model.common.description')</label>
                                        <textarea name="highlight[{{ $highlight->id }}][{{ $loc }}][description]" id="editor_{{$highlight->id.$loc}}" rows="10" cols="80">
                                            {{ $highlight->translateOrNew($loc)->description }}
                                        </textarea>
                                    {!! $errors->first('description','<span class="validator_errors">:message</span>')!!}
                                </div>

                                @foreach($highlight->links as $key => $link)
                                    <div class="form-group fom-style-hidden3 @if($errors->first('name_link')) has-error @endif">
                                        <label for="exampleInputEmail1">@lang('cms::backend.model.common.link-name') {{$key + 1}}</label>
                                        <input type="text" name="highlight[{{ $highlight->id }}][{{ $loc }}][name_link][{{ $link->id }}]" class="form-control" value="{{ $link->translateOrNew($loc)->name_link }}">
                                        {!! $errors->first('link','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                    <div class="form-group fom-style-hidden3 @if($errors->first('link') ) has-error @endif">
                                        <label for="exampleInputEmail1">@lang('cms::backend.model.common.link') {{ $key + 1 }}</label>
                                        <input type="text" name="highlight[{{ $highlight->id }}][{{ $loc }}][link][{{ $link->id }}]" class="form-control" value="{{ $link->translateOrNew($loc)->link }}">
                                        {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                @endforeach

                            </div>
                        @endforeach

                        <div class="form-group fom-style-hidden2 @if($errors->first('name') ) has-error @endif">
                            <label for="exampleInputEmail1">@lang('cms::backend.model.common.image-background')</label>
                            <p>
                                <img src="{{ ($highlight->image) ? route('gallery::backend::storage_image', [$highlight->image,100,100]) : '' }}" class="image_highlight image_highlight_{{ $highlight->id }}">
                                <a  class="btn btn-highpight-image btn-default btn-cancelar" data-variant-id="{{ $highlight->id }}" data-target="#upload_resource" data-toggle="modal">@lang('cms::backend.model.common.image-upload')</a>
                            </p>
                            {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                        </div>
                        <input id="image_{{ $highlight->id }}" type="hidden" name="highlight[{{ $highlight->id }}][image_id]" value="">

                    </div>
            </div>
        </div>
    </div>
</div>
<!-- END highlight section -->
