<?php

Route::group(['namespace' => 'Brainy\Definitions\Controllers\Backend'], function () {
    Route::get('definitions','DefinitionsBackendController@index')->name('definitions.index');
    Route::post('definitions','DefinitionsBackendController@store')->name('definitions.store');
    Route::get('definitions/more-conditions','DefinitionsBackendController@addMoreConditions')->name('definitions.add_more_conditions');
});
