@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
@endsection

@section('module-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <!-- AngularJS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- ngBootstrapToggle -->
    <script src="{{ asset('angular/directives/toggler/toggler.directive.js') }}"></script>

    <script>

        $(document).ready(function(){


            renderInfoUserBlocked = function (data) {
                $('#info-lock-user-' + data.userId).css('visibility', data.state == '1' ? 'hidden' : 'visible');
            };

            var rotaAjax = "{{ route('permissions::backend::group_not_users_ajax', [$group->id]) }}";
            var columns = [
                {data: 'name', name: 'name'},
                {data: 'group_id', name: 'group_id'},
                {data: 'email', name: 'email'},
                {data: 'msgEstado', name: 'msgEstado', orderable: false, searchable: false},
                {data: 'estado', name: 'estado'},
                {data: null, defaultContent: '', orderable: false, searchable: false}
            ];

            $('.default-table').mydatatable({rota: rotaAjax, colunas: columns});



        });


    </script>



@endsection

@section('content')

    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">

            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{route('permissions::backend::group.edit',[$group->id])}}" class="icon_voltar"><i class="icon-voltar"></i>{{ ucfirst(trans('permissions::backend.return')) }}</a></li>
                    </ul>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @if(session('sucess-message'))
                        <div class="alert alert-sucess alert-dismissible " role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <i class="icon-permissoes icon10" ></i>{{ session('sucess-message') }}
                        </div>
                    @endif

                    {!! $errors->first('error-message','
                    <div class="alert alert-erro alert-dismissible " role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                        <i class="icon-opcoes-eliminar" style="padding-bottom: 1px;"></i> :message
                    </div>
                    ')!!}
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <ul class="breadcrumb">
                        <li>@lang('permissions::backend.menu.breadcrumb.permissions')</li>
                        <li>@lang('permissions::backend.menu.breadcrumb.groups')</li>
                        <li>{{$group->name}}</li>
                        <li>@lang('permissions::backend.menu.breadcrumb.add_to_group')</li>


                    </ul>
                </div>
            </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel painel">
                            <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20"></i>
                                {{ trans_choice('permissions::backend.users', 2) }}
                            </span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                        <input class="search search_icon" type="text" id="searchbox" placeholder="@lang('permissions::backend.user.index.search')">
                                        <table id="table_groups" class="table default-table">
                                            <thead>
                                            <tr>
                                                <th>{{ ucfirst(trans('permissions::backend.name')) }}</th>
                                                <th>{{ ucfirst(trans('permissions::backend.user.model.group')) }}</th>
                                                <th>{{ ucfirst(trans('permissions::backend.user.model.email')) }}</th>
                                                <th></th>
                                                <th>{{ ucwords(trans('permissions::backend.block-unblock')) }}</th>
                                                <th>
                                                    <label class="checkbox-default">
                                                        <input type='checkbox' id="sel_all_perm">
                                                        <span></span>
                                                    </label>
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                       @if($total_users!=0) <a data-toggle="modal" data-target="#delete_order" class="btn btn-default btn-yellow">@lang('permissions::backend.save')</a>@endif

                    </div>
                </div>
        </div>
    </div>


    <!-- --------------------------------------------------- MODAL DELETE RECORD ---------------------------------------------------- -->
    <div class="modal modal-aux" id="delete_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux" role="document">
            <form class="form_delete_order" method="POST" action="{{route('permissions::backend::update_new_users_for_group',[$group->id])}}" >
                <input id="deleted_orders" name="deleted_orders" type="hidden" value="">

                                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-trocar-grupos"></i> @lang("permissions::backend.group.edit.change")</span></div>
                    <div class="modal-body">
                        <div class="col-md-12 padding-top-10"><p class="text-center"><i class="icon-eliminar-imagem icon40"></i></p></div>
                        <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">@lang("permissions::backend.group.edit.are_you_sure")</p></div>

                        <div class="col-md-12 padding-bottom-20 oders_and_dev_to_delete"></div>
                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30">
                        <button type="submit" class="btn btn-default btn-yellow">@lang("permissions::backend.change")</button>
                        <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("permissions::backend.cancel") </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- -------------------------------------------------MODAL DELETE RECORD  --------------------------------------------------- -->



@endsection




