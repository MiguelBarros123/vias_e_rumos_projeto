<?php

Route::group(['namespace' => 'Brainy\Cms\Controllers\Frontend'], function () {



    Route::get('logout', 'ModelFrontendController@logout')->name('model.logout');
    Route::put('send-email', 'ModelFrontendController@sendEmail')->name('contact.send-email');
    Route::post('authenticate', 'ModelFrontendController@authenticate')->name('model.authenticate');
    Route::put('new-account', 'ModelFrontendController@newAccount')->name('model.new-account');
    Route::put('update-account', 'ModelFrontendController@updateAccount')->name('model.update_account');
    Route::put('save-address', 'ModelFrontendController@saveAddress')->name('model.save_address');
    Route::put('save-invoice-address', 'ModelFrontendController@saveInvoiceAddress')->name('model.save_invoice_address');
    Route::put('save-company-address', 'ModelFrontendController@saveCompanyAddress')->name('model.save_company_address');
    Route::get('active-address/{id}', 'ModelFrontendController@activeAddress')->name('model.active_address');
    Route::get('remove-address/{id}', 'ModelFrontendController@removeAddress')->name('model.remove_address');
    Route::get('remove-company-address/{id}', 'ModelFrontendController@removeCompanyAddress')->name('model.remove_company_address');
    Route::put('edit-address/{id}', 'ModelFrontendController@editAddress')->name('model.edit_address');
    Route::put('edit-company-address/{id}', 'ModelFrontendController@editCompanyAddress')->name('model.edit_company_address');
    Route::get('set-cookies', 'ModelFrontendController@setCookies')->name('model.set-cookies');
    Route::post('authenticate-checkout', 'ModelFrontendController@authenticateCheckout')->name('model.authenticate-checkout');
    Route::get('search-products/{string}', 'ModelFrontendController@searchProducts')->name('model.search_products');
    Route::put('change-language', 'ModelFrontendController@changeLanguage')->name('model.change-language');
    Route::get('get-all-orders-ajax', 'ModelFrontendController@allOrdersAjax')->name('model.ajax_all_orders');
    Route::get('rewards-ajax', 'ModelFrontendController@ajaxRewards')->name('model.ajax_rewards');
    Route::get('type-reservations', 'ModelFrontendController@typeReservations')->name('model.type-reservations');
    Route::get('extras-reservations', 'ModelFrontendController@extrasReservations')->name('model.extras-reservations');
    Route::get('data-reservations', 'ModelFrontendController@dataReservations')->name('model.data-reservations');
    Route::get('total-reservations', 'ModelFrontendController@totalReservations')->name('model.total-reservations');
    Route::get('summary-reservations', 'ModelFrontendController@summaryReservations')->name('model.summary-reservations');
    Route::get('check-promo', 'ModelFrontendController@checkPromo')->name('model.check-promo');

    Route::get('counties', 'ModelFrontendController@getCounties')->name('model.get_counties');
    Route::get('parishs', 'ModelFrontendController@getParishs')->name('model.get_parishs');
        


});
