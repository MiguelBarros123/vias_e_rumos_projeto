var myTemplate_products_table = "<tr id='sel_item_list2_products_table_${id}'>" +
    "<td>${id}</td>" +
    "<td> ${name} <input type='hidden' name='products_ids[]' value='${id}' /></td> " +
    "<td> ${price}</td>" +
    "<td><input class='form-control' type='number' name='variants[${id}][min_qty]' value='1' min='1'/></td>" +
    "<td class='text-right'><a id='delete_selected_item_datatable_list_products_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
    "</tr>";

var myTemplate_packs_table = "<tr id='sel_item_list2_packs_table_${id}'>" +
    "<td class='details-control'></td>"+
    "<td>${id}</td>" +
    "<td> ${name} <input type='hidden' name='packs_ids[]' value='${id}' /></td> " +
    "<td>${visibility}</td>" +
    "<td>${discount}</td>" +
    "<td><input class='form-control' type='number' name='packs[${id}][min_qty]' value='1' min='1'/></td>" +
    "<td class='text-right'><a id='delete_selected_item_datatable_list_packs_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
    "</tr>";

console.log('brainy', window.Brainy.module.routes['ecommerce::backend::sales.btb.ajax_addresses']);



var rotaSummary = window.Brainy.module.routes['ecommerce::backend::sales.btb.ajax_summary'];
var rotaProfiles = window.Brainy.module.routes['ecommerce::backend::sales.btb.ajax_profiles'];

$(document).on('click', '#delete_selected_item_datatable_list_packs_table', function(){
    var pack_id = $(this).data('id');

    $('.new_row_variant_datatable_'+pack_id).remove();

});

var optionsPacks,
    optionsProducts,
    init_datatable = 0,
    init_ckeditor = 0;
init_summary = 0;

var form = $("#new-order");
form.validate({
    errorPlacement: function errorPlacement(error, element) {
        element.before(error);
    },
    rules: {
        'send_address': {required:true},
        'invoice_address': {required:true}
    },
    messages:{

    }
});

var end = window.Brainy.module.translations.sales.common.finish_order;
var next = window.Brainy.module.translations.sales.common.next;
var prev = window.Brainy.module.translations.sales.common.previous;

$("#order-steps").steps({
    headerTag: "h3",
    bodyTag: "section",
    autoFocus: true,
    labels: {
        finish: "<p class='btn btn-default btn-yellow btn-submit-all'>"+ end +"</p>",
        next: "<p class='btn btn-default btn-yellow btn-submit-all'>"+ next +"</p>",
        previous: "<p class='btn btn-default btn-cancelar'>"+ prev +"</p>"
    },
    onStepChanging: function (event, currentIndex, newIndex) {
        var user_id = $('input[name="company_users[]"]').val();

        if(user_id == undefined){
            $('.order_no_user').show();
            return false;
        } else {
            $('.order_no_user').hide();
        }

        if(newIndex == 1 && init_datatable == 0) {
            init_datatable = 1;


            productsTable();
            packsTable();

        }

        if(newIndex == 3 && init_ckeditor == 0){
            init_ckeditor = 1;
            CKEDITOR.replace('order_description_back');
            CKEDITOR.replace('order_description_front');

            $('#toggle-email').bootstrapToggle();

        }

        if(newIndex == 4){

            if(init_summary != 0){
                optionsSummary.datatable.destroy();
            }

            init_summary = 1;
            summaryTable();
            orderDetails();
            orderAddresses();
            sendData();
        }

        return form.valid();
    },
    onFinishing: function (event, currentIndex) {
        return form.valid();
    },
    onFinished: function (event, currentIndex) {
        $('#new-order').submit();
    }
});

$(document).ready(function() {


    var sugest2=$('#magicsuggest-profile').magicSuggest({
        placeholder: "Pesquisar",
        method: 'get',
        allowFreeEntries: false,
        data: rotaProfiles,
        allowDuplicates: false,
        valueField: 'id',
        selectionPosition: 'bottom',
        selectionStacked: true,
        maxSelection: 1,
        renderer: function(data){
            return data.name + ' (' + data.company.name + ')';
        },
        selectionRenderer: function(data){

            var img = '';

            if(data.thumbnail === "") {
                img = '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
            } else {
                img = '{{route("gallery::frontend::storage_image_p",[":thumb",50,50])}}';
                img = '<img class="image_user_picked2" src="'+img.replace(':thumb', data.thumbnail)+'" />';
            }


            return '<div class="row">' +
                '<div class="col-md-1">'+img+'</div>' +
                '<div class="col-md-3 padding-divs">'+data.name+'</div>' +
                '<div class="col-md-3 padding-divs">'+data.defaultemail+'</div>' +
                '<div class="col-md-3 padding-divs">'+data.company.name+'</div>' +
                '<div class="col-md-2 padding-divs">'+data.type+'</div>' +
                '</div>';
        }
    });

    $(sugest2).on('selectionchange', function(e,m){

        var profile_id = this.getValue()[0];
        var urlTo = rotaForAjaxAddresses.replace('%profile_id%', profile_id);

        if(profile_id != undefined){

            $.get(urlTo, function (data) {
                console.log('addresses', data);
                $('.send_order_addresses').html('');
                $('.send_order_addresses').html(data.send);
                $('.invoice_order_addresses').html('');
                $('.invoice_order_addresses').html(data.invoice);

                $('#toggle-not-send_address').bootstrapToggle();
                $('#toggle-not-invoice_address').bootstrapToggle();

                $('#toggle-not-send_address').on('change', function(){
                    $('.send_address_not').fadeToggle();
                    $('.send_address_company').fadeToggle();
                });

                $('#toggle-not-invoice_address').on('change', function(){
                    $('.invoice_address_not').fadeToggle();
                    $('.invoice_address_company').fadeToggle();
                });

            });

            if(init_datatable){
                optionsPacks.datatable.destroy();
                packsTable();
            }

        }

    });

    $('#select_resource_products_table').on('shown.bs.modal', function (e) {

        optionsProducts.datatable.columns.adjust().draw();

    });

    $('#select_resource_packs_table').on('shown.bs.modal', function (e) {

        optionsPacks.datatable.columns.adjust().draw();

    });

    $('#create_address').on('click', function(){

        var profile_id =$('input[name="company_users[]"]').val();
        var urlAddress = rotaForAddresses.replace('%profile_id%', profile_id);

        $.ajax({
            type:'post',
            url:urlAddress,
            data: {
                'address': $('input[name="new_address"]').val(),
                'zipcode': $('input[name="new_zip_code"]').val(),
                'company': $('input[name="new_company"]').val(),
                'locality': $('input[name="new_locality"]').val(),
                'phone': $('input[name="new_phone"]').val(),
                'city': $('input[name="new_city"]').val(),
                'country': $('select[name="new_country"]').val()
            },
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success:function (data) {

                console.log('new_addresses', data);

                $('.send_order_addresses').html('');
                $('.send_order_addresses').html(data.send);
                $('.invoice_order_addresses').html('');
                $('.invoice_order_addresses').html(data.invoice);

                $('#new_address').modal('toggle');

                $('#toggle-not-send_address').bootstrapToggle();
                $('#toggle-not-invoice_address').bootstrapToggle();

                $('#toggle-not-send_address').on('change', function(){
                    $('.send_address_not').fadeToggle();
                    $('.send_address_company').fadeToggle();
                });

                $('#toggle-not-invoice_address').on('change', function(){
                    $('.invoice_address_not').fadeToggle();
                    $('.invoice_address_company').fadeToggle();
                });

            }
        });

    });

    $('.order_coin select').on('change', function(){

        if(init_datatable){
            optionsProducts.datatable.destroy();
            productsTable();
        }

    });


    $('#range').daterangepicker({
        "locale": {
            "format": 'DD/MM/YYYY'
        },
        "minDate": "{{\Carbon\Carbon::now()->format('d/m/Y')}}",
        "startDate":  "{{\Carbon\Carbon::now()->format('d/m/Y')}}",
        "endDate":  "{{\Carbon\Carbon::now()->format('d/m/Y')}}"
    });


});

function productsTable() {
    var coin = $('select[name="coin"]').val();
    var url = rotaProducts.replace(':coin', ++coin);

    optionsProducts = {
        rota: url,
        colunas: [
            {
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'price', name: 'price'},
            {data: null, defaultContent: '', orderable: false, searchable: false}
        ],
        config: {
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                //for already selected rows
                $(nRow).attr('id', 'sel_row_'+aData['id']);

            },

            initComplete: function(settings, json) {
                $(function() {
                    $.logicDatatable(optionsProducts);
                    $.selectDatatableLogic(optionsProducts);
                })
            }



        },
        searchableElementId: 'ordered_search'
    };

    $('#products_table').mydatatable(optionsProducts);
}

function packsTable() {
    var user_id = $('input[name="company_users[]"]').val();
    url = rota.replace(':user_id', user_id);

    optionsPacks = {
        rota: url,
        colunas: [
            {
                "orderable":      false,
                "data":           null,
                "defaultContent": ''
            },
            {data: 'id', name: 'id'},
            {data: 'name', name: 'name'},
            {data: 'visibility', name: 'visibility'},
            {data: 'discount', name: 'discount'},
            {data: null, defaultContent: '', orderable: false, searchable: false}
        ],
        config: {
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                //for already selected rows
                $(nRow).attr('id', 'sel_row_'+aData['id']);

                //passar aqui o id do item
                var tr2 = $(nRow).closest('tr');
                if(aData['has_variants'] > 0){
                    tr2.find("td:first").addClass('details-control');
                }

                tr2.find("td:not(:first, :last, :nth-last-child(2))").addClass('clickable_url');

            },

            initComplete: function(settings, json) {
                $(function() {
                    $.logicDatatable(optionsPacks);
                    $.selectDatatableLogic(optionsPacks);

                })
            }



        },
        searchableElementId: 'ordered_search'
    };

    $('#packs_table').mydatatable(optionsPacks);

    // Add event listener for opening and closing details
    $('#packs_table tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = optionsPacks.datatable.row( tr );

        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            var pack_id = tr.attr('id').split('sel_row_')[1];
            var urlTo = rotaForComercialVariants.replace('%pack_id%', pack_id);

            $.get(urlTo, function (data) {
                row.child(data,'new_row_variant_datatable').show();
            });

            tr.addClass('shown');
        }
    });

    // Add event listener for opening and closing details
    $('#dhl_collection tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var pack_id = tr.attr('id').split('sel_item_list2_packs_table_')[1];

        if(tr.hasClass('shown')){
            tr.removeClass('shown');
            tr.parent().children('.new_row_variant_datatable_'+pack_id).remove();
        } else {

            tr.addClass('shown');

            // Open this row
            var urlTo = rotaForVariants.replace('%pack_id%', pack_id);

            $.get(urlTo, function (data) {
                tr.parent().append(tr, '<tr class="new_row_variant_datatable_'+pack_id+'"><td class="new_row_variant_datatable" colspan="7">' + data + '</td></tr>');
            });
        }

    });
}

function summaryTable(){

    var variants = getVariants();
    var pack_variants = getPackVariants();
    var coin = $('select[name="coin"]').val();
    var coin = ++coin;

    optionsSummary = {
        rota: rotaSummary,
        metodo: 'POST',
        dados: function(d){
            d.coin = coin;
            d.variants = variants;
            d.pack_variants = pack_variants;
        },
        processing: true,
        colunas: [
            {data: 'variant_photo', name: 'variant_photo'},
            {data: 'variant_reference', name: 'variant_reference'},
            {data: 'variant_name', name: 'variant_name'},
            {data: 'price', name: 'price'},
            {data: 'qty', name: 'qty'},
            {data: 'total', name: 'total'}
        ],
        config: {
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                //for already selected rows
                $(nRow).attr('id', 'sel_row_'+aData['id']);

            },

            initComplete: function(settings, json) {
                $(function() {
                    $.logicDatatable(optionsSummary);
                    $.selectDatatableLogic(optionsSummary);

                })
            }



        },
        searchableElementId: 'search'
    };

    $('#summary_table').mydatatable(optionsSummary);

}

function getVariants(){

    var variants = [];
    var qty = '';
    var pr_id = '';
    var reference = '';

    for(var i = 0; i < $('input[name="products_ids[]"]').length; i++){
        reference = i + 1;
        pr_id = $('input[name="products_ids[]"]')[i].value;
        qty = $('input[name="variants['+ pr_id +'][min_qty]"]').val();

        variants = variants.concat([
            {
                'variant': pr_id,
                'qty': qty
            }
        ]);
    }

    return variants;

}

function getPackVariants(){

    var pack_variants = [];
    var pk_id = '';
    var pv_id = '';
    var pv_reference = '';
    var qty = '';
    var reference = '';

    for(var j = 0; j < $('input[name="packs_ids[]"]').length; j++){
        reference = j + 1;
        pk_id = $('input[name="packs_ids[]"]')[j].value;

        for(var x = 0; x < $('input[name="pv_ids['+ pk_id +'][]"]').length; x++){
            pv_reference = x + 1;
            pv_id = $('input[name="pv_ids['+ pk_id +'][]"]')[x].value;

            qty = $('input[name="pack_variants['+ pk_id +']['+ pv_id +'][min_qty]"]').val() * $('input[name="packs['+ pk_id +'][min_qty]"]').val();;

            pack_variants = pack_variants.concat([
                {
                    'pack': pk_id,
                    'variant': pv_id,
                    'qty': qty
                }
            ]);

        }

    }

    return pack_variants;

}

function orderDetails(){

    var user_id = $('input[name="company_users[]"]').val();
    var urlTotal = rotaForTotal.replace('%user_id%', user_id);
    var coin = $('select[name="coin"]').val();
    var coin = ++coin;
    var manual_discount = $('input[name="manual_discount"]').val();

    $.ajax({
        type:'post',
        url:urlTotal,
        data: {
            'coin': coin,
            'manual_discount': manual_discount,
            'variants': getVariants(),
            'pack_variants': getPackVariants()
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function (data) {

            var content = $('.wrapper_table_order_details');

            content.html('');
            content.html(data);

        }
    });

}

function orderAddresses(){

    var user_id = $('input[name="company_users[]"]').val();
    var urlSummary = summary.replace('%user_id%', user_id);

    $.ajax({
        type:'post',
        url:urlSummary,
        data: {
            'send': $('#toggle-not-send_address').prop('checked'),
            'invoice': $('#toggle-not-invoice_address').prop('checked'),
            'id_send': $('input[name="send_address"]:checked').val(),
            'id_invoice': $('input[name="invoice_address"]:checked').val(),
            'send_address': $('input[name="send_address_new_address"]').val(),
            'send_zip_code': $('input[name="send_address_new_zip_code"]').val(),
            'send_company': $('input[name="send_address_new_company"]').val(),
            'send_locality': $('input[name="send_address_new_locality"]').val(),
            'send_phone': $('input[name="send_address_new_phone"]').val(),
            'send_city': $('input[name="send_address_new_city"]').val(),
            'send_country': $('select[name="send_address_new_country"]').val(),
            'invoice_address': $('input[name="invoice_address_new_address"]').val(),
            'invoice_zip_code': $('input[name="invoice_address_new_zip_code"]').val(),
            'invoice_company': $('input[name="invoice_address_new_company"]').val(),
            'invoice_locality': $('input[name="invoice_address_new_locality"]').val(),
            'invoice_phone': $('input[name="invoice_address_new_phone"]').val(),
            'invoice_city': $('input[name="invoice_address_new_city"]').val(),
            'invoice_country': $('select[name="invoice_address_new_country"]').val()
        },
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success:function (data) {

            var content = $('.address_information');

            content.html('');
            content.html(data);

        }
    });

}

function sendData(){
    var back = CKEDITOR.instances.order_description_back.getData();
    var front = CKEDITOR.instances.order_description_front.getData();
    var range = $('#range').val();
    var send = 'Não';

    if($('#toggle-email').prop('checked')){
        send = 'Sim'
    }

    var d_back = $('.description_back');
    var d_front = $('.description_front');
    var daterange = $('.date_range');
    var emailsend = $('.email_send');

    d_back.html('');
    d_back.html(back);

    d_front.html('');
    d_front.html(front);

    daterange.html('');
    daterange.html(range);

    emailsend.html('');
    emailsend.html(send);

}
//# sourceMappingURL=create_order.js.map
