@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/biblicalcms/backend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/biblicalcms/backend.js')}}"></script>
@endsection

@section('content')
<p>@lang('biblicalcms::backend.model.index.welcome')</p>
@endsection
