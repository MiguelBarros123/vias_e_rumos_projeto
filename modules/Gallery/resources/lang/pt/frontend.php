<?php

return [
    'model' => [
        'index' => [
            //@lang('gallery::frontend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view frontend page',
        ],
    ],
    'messages' => [
        //@lang('gallery::frontend.messages.notifications.new-task')
        'notifications' => [
            'new-task' => 'You have a new pending task',
        ],
    ],
];
