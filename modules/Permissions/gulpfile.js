var elixir = require('laravel-elixir');

elixir(function(mix) {
     mix.styles([
         'backend-styles.css',
         'jquery-ui.min.css'
     ], '../../public/css/permissions/backend.css')

         .scripts([
             'datatables.js',
             'jquery-ui.min.js',
             'permissions.js',
         ], '../../public/js/permissions/backend.js')
    //     .scripts([
    //         'frontend-app.js'
    //     ], 'public/js/frontend.js');
});
