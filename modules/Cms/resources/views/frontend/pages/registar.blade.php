@extends('layouts.frontend.app')

@section('title', 'Vias e Rumos - Novo Registo')
@section('content')
<form action="{{route('cms::frontend::model.create_user')}}" method="POST">
    {{ csrf_field() }}
    <div class="empresa-body cred-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2>Novo Registo</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h3>Dados de Utilizador</h3>
          </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inp_label">E-mail * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="email" type="email" value="{{ old('email') }}">
                    @if($errors->has("email"))
                        <span class="help-block">{{ $errors->first('email') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inp_label">Confirmar E-mail * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="email_confirmation" type="email" value="{{ old('email_confirmation') }}">
                    @if($errors->has("email_confirmation"))
                        <span class="help-block">{{ $errors->first('email_confirmation') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inp_label">Password * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="password" type="password" value="{{ old('password') }}">
                    @if($errors->has("password"))
                        <span class="help-block">{{ $errors->first('password') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inp_label">Confirmar Password * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="password_confirmation" type="password" value="{{ old('password_confirmation') }}">
                    @if($errors->has("password_confirmation"))
                        <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <h3>Dados pessoais</h3>
          </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="inp_label">Nome * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="firstname" value="{{ old('firstname') }}">
                    @if($errors->has("lastname"))
                        <span class="help-block">{{ $errors->first('lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="inp_label">Apelido * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="lastname" value="{{ old('lastname') }}">
                    @if($errors->has("lastname"))
                        <span class="help-block">{{ $errors->first('lastname') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="inp_label">Nif * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="nif" value="{{ old('nif') }}">
                    @if($errors->has("nif"))
                        <span class="help-block">{{ $errors->first('nif') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="inp_label">Cartão de Cidadão * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="cartao_cidadao" value="{{ old('cartao_cidadao') }}">
                    @if($errors->has("cartao_cidadao"))
                        <span class="help-block">{{ $errors->first('cartao_cidadao') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="inp_label">Data de Validade * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="data_validade" value="{{ old('data_validade') }}">
                    @if($errors->has("data_validade"))
                        <span class="help-block">{{ $errors->first('data_validade') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="inp_label">Telefone / Telemóvel * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="telefone" value="{{ old('telefone') }}">
                    @if($errors->has("telefone"))
                        <span class="help-block">{{ $errors->first('telefone') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h3>Dados para faturação</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inp_label">Nome Fiscal * </div>
                <div class="inps_wrap_cred">
                   <input class="inp_av" placeholder="" name="nome_fiscal" value="{{ old('nome_fiscal') }}">
                   @if($errors->has("nome_fiscal"))
                        <span class="help-block">{{ $errors->first('nome_fiscal') }}</span>
                    @endif
               </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inp_label">NIF/NIPC *  </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="nipc" value="{{ old('nipc') }}">
                    @if($errors->has("nipc"))
                        <span class="help-block">{{ $errors->first('nipc') }}</span>
                    @endif
               </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inp_label">País * </div>
                <div class="inps_wrap_cred">
                    <select class="select_av" name="country">
                        @foreach($countries as $country)
                            <option value="{{$country->id}}" {{(($country->id) == old('country')) ? 'selected': ''}}>{{$country->value}}</option>
                        @endforeach
                    </select>
                    @if($errors->has("country"))
                        <span class="help-block">{{ $errors->first('country') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <div class="inp_label">Morada * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="address" value="{{ old('address') }}">
                    @if($errors->has("address"))
                        <span class="help-block">{{ $errors->first('address') }}</span>
                    @endif
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="inp_label">Código Postal * </div>
                <div class="inps_wrap_cred">
                   <input class="inp_av" placeholder="" name="cod_postal" value="{{ old('cod_postal') }}">
                   @if($errors->has("cod_postal"))
                        <span class="help-block">{{ $errors->first('cod_postal') }}</span>
                    @endif
               </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="inp_label">Localidade * </div>
                <div class="inps_wrap_cred">
                    <input class="inp_av" placeholder="" name="local" value="{{ old('local') }}">
                    @if($errors->has("local"))
                        <span class="help-block">{{ $errors->first('local') }}</span>
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="info_label">
                    * Campos de preenchimento obrigatório
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <span class="btn-line">
                    <button type="submit" name="" id="registar" class="cred-btn">Registar</button>
                </span>
            </div>
        </div>
    </div>
</form>

@endsection
