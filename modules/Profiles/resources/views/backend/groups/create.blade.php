@extends('layouts.backend.app')
@inject('folders', 'Brainy\Gallery\Models\MediaFolder')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" href="{{asset('css/profiles/backend.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('css/catalog/backend.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.css">
@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.js"></script>
    <script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('back/js/onload.js')}}"></script>



    <script>

        $(document).ready(function() {


            var sugest2=$('#magicsuggest-functionaries').magicSuggest({
                placeholder: "Pesquisar",
                method: 'get',
                allowFreeEntries: false,
                data: "{{route('profiles::backend::profiles.ajax.no.group')}}",
                allowDuplicates: false,
                valueField: 'id',
                selectionPosition: 'bottom',
                selectionStacked: true,
                renderer: function(data){
                    return data.name + ' (' + data.type + ')';
                },
                selectionRenderer: function(data){

                    var img = '';

                    if(data.thumbnail === "") {
                        img = '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
                    } else {
                        img = '{{route("gallery::frontend::storage_image_p",[":thumb",50,50])}}';
                        img = '<img class="image_user_picked2" src="'+img.replace(':thumb', data.thumbnail)+'" />';
                    }

                    {{--<img class="image_user_picked2" src="{{route('gallery::frontend::storage_image_p',[$profile->thumbnail,50,50])}}" />--}}

                    {{--var url = '{{ route("cms::backend::model.change_composed_visibility", ":id") }}';--}}
                    {{--url = url.replace(':id', line);--}}


                    return '<div class="row">' +
                            '<div class="col-md-1">'+img+'</div>' +
                            '<div class="col-md-4 padding-divs">'+data.name+'</div>' +
                            '<div class="col-md-4 padding-divs">'+data.defaultemail+'</div>' +
                            '<div class="col-md-3 padding-divs">'+data.type+'</div>' +
                            '</div>';

//                    return '<table class="table profile_render_table">' +
//                            '<tr>' +
//                            '<td>'+data.thumbnail+'</td>' +
//                            '<td>'+data.name+'</td>' +
//                            '<td>'+data.defaultemail+'</td>' +
//                            '<td>'+data.type+'</td>' +
//                            '</tr>' +
//                            '</table> ' ;
                }
            });

            CKEDITOR.replace('description');

        });
    </script>


@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">

                            <li><a href="{{route('profiles::backend::profiles.group.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.groups.create') !!}

            <div class="row">
                <form  method="POST" action="{{route('profiles::backend::profiles.group.store')}}" autocomplete="off">
                    {!! csrf_field() !!}
                    <div class="col-lg-12">
                        <div class="panel ">
                            <div class="panel-titulo">
                                <span class="titulo"><i class="icon-editar-utilizador icon10"></i>@lang('profiles::backend.common.create_group')</span>
                            </div>
                            <div class="panel-body">
                                <div class="row">
                                    <div  class="col-lg-12">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('name') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.name')</label>
                                            <input type="text" name="name" class="form-control " value="{{old('name')}}">
                                            {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                        </div>
                                    </div>

                                    <div  class="col-lg-12">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('description') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.description')</label>
                                            <textarea name="description" id="description" rows="10" cols="80"></textarea>
                                        </div>
                                    </div>

                                </div>



                                </div>
                            </div>


                        </div>

                    <div class="col-lg-12">
                        <div class="panel ">
                            <div class="panel-titulo">
                                <span class="titulo"><i class="icon-editar-utilizador icon10"></i>@lang('profiles::backend.common.profiles')</span>
                            </div>
                            <div class="panel-body">

                                <div class="row">
                                    <div  class="col-lg-12 margem_20">
                                        <div class="list_magicsuggest form-group fom-style-hidden2">
                                            <label for="exampleInputEmail1">@lang('profiles::backend.common.add_profiles')</label>
                                            <input class="form-control" id="magicsuggest-functionaries" name="company_users">
                                        </div>
                                    </div>

                                </div>


                            </div>
                        </div>


                    </div>


                    <div class="col-lg-12">
                        <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('profiles::backend.common.save')</button>
                        <button type="submit" name="submit" value="stay" class="btn btn-default btn-yellow btn-stay btn-submit-all">@lang('profiles::backend.common.save_and_stay')</button>
                        <a href="{{route('profiles::backend::profiles.group.index')}}" class="btn btn-default btn-cancelar">@lang('profiles::backend.common.cancel')</a>
                    </div>

                </form>
            </div>
                    </div>

            </div>



@endsection



