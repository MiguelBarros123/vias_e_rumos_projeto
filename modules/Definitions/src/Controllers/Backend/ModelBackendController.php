<?php

namespace Brainy\DefinitionsRent\Controllers\Backend;

use Brainy\DefinitionsRent\Models\RentDefinition;
use Brainy\Framework\Controllers\BackendController;
use Brainy\Rent\Models\GlobalCoin;
use Illuminate\Http\Request;

class ModelBackendController extends BackendController
{
    public function index()
    {

        $definitions = RentDefinition::first();
        $coins=GlobalCoin::all();

        return view('definitionsrent::backend.model.index')->with('definitions', $definitions)->with('coins', $coins);
    }

    public function create()
    {
        return view('definitionsrent::backend.model.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
        ]);
    }

    public function show($model)
    {
        return view('definitionsrent::backend.model.show', compact('model'));
    }

    public function edit($model)
    {
        return view('definitionsrent::backend.model.edit', compact('model'));
    }

    public function update(Request $request)
    {

        dd($request->except('_token'));

        $definitions = RentDefinition::first();

        $definitions->update([
            'min_days' => $request->min_days
        ]);

        return redirect()->back()->with('sucess-message', trans('definitionscampaigns::backend.messages.success.update_company'));
    }

    public function delete($model)
    {
        return back();
    }
}
