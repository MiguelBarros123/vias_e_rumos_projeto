<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:19
 */
namespace Brainy\Cms\Models;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

class Page extends Model {

    use Translatable;

    protected $table = 'pages';

    protected $fillable = ['name', 'is_contacts'];

    public $translatedAttributes = ['meta_description', 'route_name','route_url', 'title'];

    public $translationModel = PageTranslation::class;

    public function texts_simple()
    {
        return $this->hasMany(TextSimple::class);
    }

    public function texts()
    {
        return $this->hasMany(Text::class);
    }

    public function highlights()
    {
        return $this->hasMany(Highlight::class);
    }

    public function links()
    {
        return $this->hasMany(HighlightLink::class);
    }

    public function composeds()
    {
        return $this->hasMany(Composed::class);
    }

    public function pageTranslation()
    {
        return trans('cms::backend.model.pages.index.'. str_slug($this->name, '-'));
    }

    public static function convertUrl($route_name,$final_lang){


        if(Schema::hasTable('pages')){
            $page = Page::whereTranslation('locale',$final_lang)->whereTranslation('route_name',$route_name)->get();
            if($page->count()==1){
                return $page->first()->translate($final_lang)->route_url;
            }
        }

        return false;
    }




}
