<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class ContactsInput extends Model {

    protected $fillable = [
    	'label',
    	'section',
    	'key',
    	'value'
    ];
}