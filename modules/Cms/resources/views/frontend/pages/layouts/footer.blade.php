<div id="footer">
	<div class="container">
		<div class="row displ-flex">
			<div class="col-lg-3 col-md-3 col-sm-12 hidden-sm hidden-xs">
				<div class="logo-footer">
					<img src="{{ asset('front/imgs/logo_vias.svg') }}"/>
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mid-footer">
				<ul class="footer-list">
					<li><a href="{{ route('cms::frontend::model.cookies') }}">Política de Cookies</a></li>
					<li><a href="{{ route('cms::frontend::model.litigio') }}">Lítigios de Consumo</a></li>
					<!-- <li><a>Condições gerais de venda</a></li> -->
					<li><a href="{{ route('cms::frontend::model.privacidade') }}">Politica de Privacidade</a></li>
					@if($registo_leiloeira)
						<li><a href="{{ route('cms::frontend::storage_pdf',[$registo_leiloeira->registo_leiloeira])  }}">Registo Leiloeira</a></li>
					@endif
				</ul>
			</div>
			<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 copy-wrap">
				<div class="footer-social text-right">
					<ul class="footer-social-list">
						<li><a class="footer-icon" href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::index') }}" target="_blank"><span class="icon-facebook"></span></a></li>
						<li><a class="footer-icon" href="https://plus.google.com/share?url={{ route('cms::frontend::index') }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" target="_blank"><span class="icon-google-plus"></span></a></li>
						<li><a class="footer-icon" href="http://twitter.com/share?text=Vias%20e%20Rumos&url={{ route('cms::frontend::index')}}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a></li>
						<li class="go-up hidden-sm hidden-xs"><img src="{{ asset('front/imgs/icons/seta-cima.svg') }}"/></li>
					</ul>
				</div>
				<div class="copyright">
					<span>Copyright</span><span><img src="{{ asset('front/imgs/icons/copy.svg') }}"/></span><span>2018 Vias & Rumos</span>
				</div>
			</div>
		</div>
	</div>

</div>
