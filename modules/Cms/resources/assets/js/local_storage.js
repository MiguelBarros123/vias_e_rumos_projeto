(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

/**
 * Created by User on 20-05-2016.
 */

$(function () {
    var StorageLib = function StorageLib() {
        this.init();
    };

    StorageLib.prototype.init = function () {
        if (typeof Storage == "undefined") {

            console.log('no-support');
        }
    };

    $.setStorageObject = function (key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    };

    $.getStorageObject = function (key) {
        return JSON.parse(localStorage.getItem(key));
    };

    $.clearStorageObjects = function (key) {
        localStorage.removeItem(key);
    };

    $.storagelib = function () {
        return new StorageLib();
    };
});

},{}]},{},[1]);

//# sourceMappingURL=local_storage.js.map
