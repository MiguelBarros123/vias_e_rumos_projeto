<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class ChangeLanguage
{
    public function handle($request, Closure $next)
    {
        if(!Session::has('locale'))
        {
            Session::put('locale', 'pt');
        }
        app()->setLocale(Session::get('locale'));


        return $next($request);
    }
}
