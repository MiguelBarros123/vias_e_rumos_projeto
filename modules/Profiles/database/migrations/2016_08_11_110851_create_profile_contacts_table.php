<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_contacts', function (Blueprint $table) {
            $table->engine='InnoDB';
            $table->increments('id');
            $table->text('contact');
            $table->enum('type',[
                'personal_email',
                'company_email',
                'personal_phone',
                'company_phone',
                'personal_mobile',
                'company_mobile',
                'personal_fax',
                'company_fax',
                'facebook',
                'instagram',
                'google',
                'youtube',
                'other'
            ]);

            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_contacts');
    }
}
