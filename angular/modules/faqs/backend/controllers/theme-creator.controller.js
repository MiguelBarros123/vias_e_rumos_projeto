(function() {
    'use strict';

    function ThemeCreatorController($scope, $http) {
        var themeModel = {
            name: {
                translations: {}
            },
            theme_id: null,
            visible: true,
            link: null,
            tags: []
        };

        $scope.theme = $.extend({}, themeModel);

        $scope.option = {
            language: $scope.locale,
            allowedContent: true,
            entities: false,
        };

        // Called when the editor is completely ready.
        $scope.onReady = function() {
            //
        };

        function httpPostSuccess(event) {
            return function (response) {
                $scope.$emit(event, response);
                $scope.dismiss('#modal-theme');
            };
        }

        function httpThemeError(response) {
            console.log(response);
            // do something...?
        }

        $scope.updateOrNewThemeNode = function() {
            var event = 'edit theme';

            if ($scope.isEmptySelection()) {
                $scope.theme.id = 'new-theme-' + Date.now();
                event = 'new theme';
            }
            
            $http.post('http://localhost/brainy-web/public/backend/faqs/theme', $scope.theme)
                .success(httpPostSuccess(event))
                .error(httpThemeError);
        };

        $(function() {
            $scope.ms = $('#magicsuggest-theme').magicSuggest({
                data: $scope.getTags,
                allowDuplicates: false,
                valueField: 'name'
            });

            $($scope.ms).on('selectionchange', function() {
                var tags = angular.fromJson(this.getSelection());

                for (var i = 0; i < tags.length; ++i) {
                    var tag = tags[i];
                    if (tag.id === undefined) {
                        tag.id = 'new-tag-' + Date.now();
                    }
                }

                $scope.theme.tags = tags;

                console.log($scope.theme.tags);
            });

            $('#modal-theme').on('hidden.bs.modal', function () {
                $scope.theme = angular.copy(themeModel);
                $scope.clearSelection();
                $scope.ms.clear();
            });

            $('#modal-theme').on('shown.bs.modal', function () {
                if (! $scope.isEmptySelection()) {
                    $scope.theme.id = $scope.selectedNode.id;
                    $scope.theme.link = $scope.selectedNode.link;
                    $scope.theme.visible = $scope.selectedNode.visible;
                    $scope.theme.theme_id = $scope.selectedNode.theme_id;

                    for (var i = 0; i < $scope.selectedNode.translations.length; ++i) {
                        var locale = $scope.selectedNode.translations[i].locale;
                        $scope.theme.name.translations[locale] = $scope.selectedNode.translations[i].name;   
                    }

                    $scope.theme.tags = $scope.selectedNode.tags;
                    if ($scope.theme.tags && $scope.theme.tags.length) {
                        $scope.ms.setSelection($scope.theme.tags);
                    }

                } else {
                    $scope.theme = angular.copy(themeModel);
                }

                if (! $scope.$$phase) {
                    $scope.$apply();
                }
            });
        });
    }
    
	angular.module('brainy.faqs.backend').controller('ThemeCreatorController', ['$scope', '$http', ThemeCreatorController]);
})();
