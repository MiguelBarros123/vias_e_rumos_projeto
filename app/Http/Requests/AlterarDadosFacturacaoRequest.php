<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AlterarDadosFacturacaoRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nome_fiscal' => 'required',
            'nipc' => 'required',
            'country' => 'required',
            'address' => 'required',
            'cod_postal' => 'required',
            'local' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'nome_fiscal.required' => 'Este campo é obrigatório',
            'nipc.required' => 'Este campo é obrigatório',
            'country.required' => 'Este campo é obrigatório',
            'address.required' => 'Este campo é obrigatório',
            'cod_postal.required' => 'Este campo é obrigatório',
            'local.required' => 'Este campo é obrigatório',
        ];
    }
}
