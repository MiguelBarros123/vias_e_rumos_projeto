<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTypegoodstableTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('typegoodstables', function (Blueprint $table) {
            $table->engine='InnoDB';
            $table->increments('id');
            $table->integer('sub_type_goods_id')->unsigned();
            $table->integer('typegoodstable_id')->unsigned();
            $table->string('typegoodstable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('typegoodstables');
    }
}
