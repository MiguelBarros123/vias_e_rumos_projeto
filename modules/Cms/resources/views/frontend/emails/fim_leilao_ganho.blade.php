<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Parabéns <!-- Em Falta Firstname do utilizador e lastname -->!</p>
        <div>
            <p>A sua licitação para o leilão <!-- numero do leilao --> foi a mais elevada.</p>
            <p>De modo a finalizarmos o processo, iremos entrar em contacto consigo.</p>
            <p>Caso tenha alguma dúvida, nao hesite em contactar a nossa equipa através do e-mail <a href="mailto:apoio@viaserumos.pt">apoio@viaserumos.pt</a>.</p>
            <p>Atenciosamente,</p>
            <p>A equipa Vias & Rumos</p>
        </div>
    </body>
</html>

