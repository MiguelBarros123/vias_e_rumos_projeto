@extends('layouts.frontend.app')

@section('title', 'frontend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/cms/frontend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/cms/frontend.js')}}"></script>
@endsection

@section('content')
<p>@lang('cms::frontend.model.index.welcome')</p>
@endsection
