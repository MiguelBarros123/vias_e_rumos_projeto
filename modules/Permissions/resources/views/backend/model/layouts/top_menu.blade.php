@inject('groups', 'Brainy\Framework\Models\Group')
@inject('users', 'Brainy\Framework\Models\UserDetail')

<div class="row tabs-back top_menu_page">
    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">

        <ul class="list-inline fonte_12_lato">
            <!-- routes to GROUPS and ARCHIVED USERS removed from here.....add later-->
            <li class="@activePageTopMenu('permissions::backend::group.index')">
              <a class="icon_voltar" href="{{route('permissions::backend::group.index')}}">
                {{ ucfirst(trans_choice('permissions::backend.groups', 2)) . ' (' . $groups->all()->count() . ')' }}
              </a>
            </li>
            <li class="@activePageTopMenu('permissions::backend::model.index')">
                <a class="icon_voltar" href="{{route('permissions::backend::model.index')}}">
                {{ ucfirst(trans_choice('permissions::backend.users', 2)) .' (' . $users->regularUsers()->count() . ')'  }}
                </a>
            </li>
            <li class="@activePageTopMenu('permissions::backend::archived_users_index')">
                <a class="icon_voltar" href="{{route('permissions::backend::archived_users_index')}}">
                {{ trans('permissions::backend.user.archived.title') . ' (' . $users->onlyTrashed()->count() . ')' }}
                </a>
            </li>
        </ul>
    </div>


    @if(\Illuminate\Support\Facades\Request::route()->getName()!='archived_users_index')
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 text-right tabs-back no_padding">
            <ul class="list-inline ">
                @if(\Illuminate\Support\Facades\Request::route()->getName()=='permissions::backend::model.index') <li><a data-toggle="modal" data-target="#modalCreateUser"  class="create-user icon_eliminar"><i class="icon-novo-utilizador-1 pading_right"></i>
                    @lang('permissions::backend.user.index.actions.new')
                </a></li> @endif
                @if(\Illuminate\Support\Facades\Request::route()->getName()=='permissions::backend::group.index')
                        <li><a class="icon_eliminar" href="{{route('permissions::backend::group.create')}}"><i class="icon-group pading_right"></i>
                        @lang('permissions::backend.group.index.actions.new')
                        </a></li>
                        @endif

            </ul>
        </div>



    @endif
</div>

