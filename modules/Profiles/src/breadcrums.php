<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-01-2017
 * Time: 09:46
 */



DaveJamesMiller\Breadcrumbs\Facade::register('admin', function($breadcrumbs)
{

    $breadcrumbs->push('Administração', route('gallery::backend::model.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.profiles', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Perfis', route('profiles::backend::profiles.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.people', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.profiles');
    $breadcrumbs->push('Pessoas', route('profiles::backend::profiles.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.companys', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.profiles');
    $breadcrumbs->push('Empresas', route('profiles::backend::companies.index'));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.groups', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.profiles');
    $breadcrumbs->push('Grupos', route('profiles::backend::profiles.group.index'));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.custom_fields', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.profiles');
    $breadcrumbs->push('Campos Personalizados', route('profiles::backend::backend.profiles.custom_fields.index'));
});





DaveJamesMiller\Breadcrumbs\Facade::register('admin.custom_fields.show', function($breadcrumbs, $custom_field)
{
    $breadcrumbs->parent('admin.custom_fields');
    $breadcrumbs->push($custom_field->name, route('profiles::backend::backend.profiles.custom_fields.show', $custom_field->id));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.custom_fields.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.custom_fields');
    $breadcrumbs->push('Novo Campo Personalizado', route('profiles::backend::backend.profiles.custom_fields.create'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.custom_fields.edit', function($breadcrumbs,$custom_field)
{
    $breadcrumbs->parent('admin.custom_fields.show',$custom_field);
    $breadcrumbs->push('Editar Campo Personalizado', route('profiles::backend::backend.profiles.custom_fields.update',$custom_field->id));
});




DaveJamesMiller\Breadcrumbs\Facade::register('admin.groups.show', function($breadcrumbs, $group)
{
    $breadcrumbs->parent('admin.groups');
    $breadcrumbs->push($group->name, route('profiles::backend::profiles.group.view', $group->id));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.groups.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.groups');
    $breadcrumbs->push('Novo grupo', route('profiles::backend::profiles.group.create'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.groups.edit', function($breadcrumbs,$comercial_condition)
{
    $breadcrumbs->parent('admin.groups.show',$comercial_condition);
    $breadcrumbs->push('Editar grupo', route('profiles::backend::profiles.group.update',$comercial_condition->id));
});




DaveJamesMiller\Breadcrumbs\Facade::register('admin.companies.show', function($breadcrumbs, $profile)
{
    $breadcrumbs->parent('admin.companys');
    $breadcrumbs->push($profile->name, route('profiles::backend::profiles.view', $profile->id));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.profiles.show', function($breadcrumbs, $profile)
{
    $breadcrumbs->parent('admin.people');
    $breadcrumbs->push($profile->name, route('profiles::backend::profiles.view', $profile->id));
});






DaveJamesMiller\Breadcrumbs\Facade::register('admin.profiles.edit', function($breadcrumbs,$profile)
{

    if($profile[1]=='person'){
        $breadcrumbs->parent('admin.profiles.show',$profile[0]);

    }else{
        $breadcrumbs->parent('admin.companies.show',$profile[0]);
    }



    $breadcrumbs->push('Editar perfil', route('profiles::backend::profiles.edit',[$profile[0]->id,$profile[1]]));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.profiles.create', function($breadcrumbs, $type)
{

    if($type=='person'){
        $breadcrumbs->parent('admin.people');

    }else{
        $breadcrumbs->parent('admin.companys');
    }

    $breadcrumbs->push('Novo perfil', route('profiles::backend::profiles.create',[$type]));
});