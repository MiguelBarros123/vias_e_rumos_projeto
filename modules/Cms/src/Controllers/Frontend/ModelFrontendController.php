<?php

namespace Brainy\Cms\Controllers\Frontend;

use App\User;
use Carbon\Carbon;
use Brainy\Rent\Models\Lot;
use App\BiddingsDefinitions;
use App\Events\UserHasBidding;
use Brainy\Rent\Models\Auction;
use Brainy\Rent\Models\Concelho;
use Brainy\Rent\Models\Distrito;
use Illuminate\Support\Facades\Mail;
use Brainy\Cms\Models\MenuBiblioteca;
use App\Http\Requests\AlterarPassword;
use Brainy\Rent\Models\FreguesiaActual;
use Illuminate\Support\Facades\Storage;
use Brainy\Cms\Models\MenuInstitucional;
use Brainy\Cms\Models\SubMenuBiblioteca;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Response;
use Brainy\Profiles\Models\GlobalCountry;
use Brainy\Cms\Models\ProfileReservedArea;
use Brainy\DefinitionsRent\Models\Category;
use Brainy\DefinitionsRent\Models\TypeGoods;
use Brainy\DefinitionsRent\Models\SubTypeGoods;
use App\Http\Requests\AlterarDadosPessoaisRequest;
use Brainy\Cms\Requests\LoginAreaReservadaRequest;
use App\Http\Requests\AlterarDadosFacturacaoRequest;
use Brainy\Framework\Controllers\FrontendController;

class ModelFrontendController extends FrontendController
{
    protected $auctions_per_page  = 10;

    public function index()
    {
        return view('cms::frontend.model.index');
    }

    //TODO criar os metodos com as páginas existentes

    public function __constructor()
    {

    }

    public function homePage()
    {
        // $auctions = Auction::where('featured', 1)->where('active',1)->where('date', '>', Carbon::now()->format('Y-m-d H:i'))->take(3)->get();
        $auctions = Auction::withoutGlobalScope('date')->where('featured',1)->where('active',1)->orderBy('created_at','desc')->take(3)->get();

        $next_events = Auction::withoutGlobalScope('other_date')->where('active',1)->orderBy('date','asc')->get(); 
        $next_events_aux = Auction::withoutGlobalScopes(['date','other_date'])->where('date', null)->get();
        $next_events = $next_events->merge($next_events_aux);    

        $array_events = [];
        $next_events_date = [];

        foreach($next_events as $key => $event)
        {
            if($event->date != null)
            {
                $date = explode(" ", $event->date);
                array_push($next_events_date, Carbon::parse($event->date)->format('m/d/Y'));
                // $date_aux = str_replace("-","/",substr($date[0],5));
                $date_2 = str_replace(':','h',substr($date[1],0,-3));
                $array_events[$key]['date'] = $event->date;
                $array_events[$key]['hora'] = $date_2;
            }else{
                $array_events[$key]['hora'] = '';
                $array_events[$key]['date'] = 'Sem Data';
            } 
            
            $array_events[$key]['tipo'] = Category::find($event->category_id)->name; 
            $array_events[$key]['nome'] = $event->title; 
            $array_events[$key]['local'] = $event->local; 
            $array_events[$key]['id'] = $event->id;
        }

        $dates = json_encode($next_events_date);

        $events = json_encode($array_events);

        return view('cms::frontend.pages.home', compact('auctions','sub_type_of_goods','events','dates'));
    }

    public function general_conditions()
    {
        return view('cms::frontend.pages.general-conditions');
    }

    public function vendas($id = null)
    {
        $search = urldecode(request()->query('search_query'));
        if($search)
        {
            $auctions = collect();
            $auction_aux = Auction::withoutGlobalScope('other_date');
            $auctions->push(Auction::withoutGlobalScope('other_date')->categoria($search)->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->process($search)->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->executed($search)->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->local($search)->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->distrito($search)->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->concelho($search)->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->freguesia($search)->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->whereIn('id', Lot::local($search)->pluck('auction_id'))->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->whereIn('id', Lot::distrito($search)->pluck('auction_id'))->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->whereIn('id', Lot::concelho($search)->pluck('auction_id'))->get());
            $auctions->push(Auction::withoutGlobalScope('other_date')->whereIn('id', Lot::freguesia($search)->pluck('auction_id'))->get());

            $lots = Lot::all();
            $sub_type_goods_id = SubTypeGoods::where('name','LIKE','%'.$search.'%')->pluck('id');
            foreach($lots as $lot)
            {
                $lots_exist = $lot->type_goods()->whereIn('sub_type_goods_id',$sub_type_goods_id);
                if($lots_exist->count() > 0)
                {
                    $auctions->push(Auction::withoutGlobalScope('other_date')->whereIn('id',Lot::whereIn('id',$lots_exist->pluck('typegoodstable_id'))->pluck('auction_id'))->get());
                }
            }

            $type_goods_id = TypeGoods::where('name','LIKE','%'.$search.'%')->pluck('id');
            foreach($lots as $lot)
            {
                $lots_exist = $lot->type_goods()->whereIn('sub_type_goods_id', SubTypeGoods::whereIn('type_good_id', $type_goods_id)->pluck('id'));
                if($lots_exist->count() > 0)
                {
                    $auctions->push(Auction::withoutGlobalScope('other_date')->whereIn('id',Lot::whereIn('id',$lots_exist->pluck('typegoodstable_id'))->pluck('auction_id'))->get());
                }
            }
            $ids = [];

            foreach($auctions as $collection_auction)
            {
                foreach($collection_auction as $auction_getted)
                {
                    array_push($ids,$auction_getted->id);
                }
            }

            $auctions = Auction::withoutGlobalScope('other_date')->whereIn('id',$ids)->where('active',1)->orderBy('date','desc')->paginate($this->auctions_per_page);    

        }
        else{
            if($id == null)
            {
                $auctions = Auction::withoutGlobalScope('other_date')->where('active',1)->orderBy('date','desc')->paginate($this->auctions_per_page);
            }
            else{
                $auctions = Auction::withoutGlobalScope('other_date')->where('category_id', $id)->where('active', 1)->orderBy('date','desc')->paginate($this->auctions_per_page);
            }
        }
        

        if(request()->ajax()) {
            if(count($auctions) > 0)
            {
                return [
                    'auctions' => view('cms::frontend.pages.vendas_partial')->with(compact('auctions'))->render(),
                    'next_page' => $auctions->nextPageUrl()
                ];
            }
            else
            {
                 return [
                    'auctions' => view('cms::frontend.pages.vendas_partial')->with(compact('auctions'))->render(),
                    'next_page' => ''
                 ]   ;
            }
        }

        if($auctions){
            $all_auctions = $auctions->pluck('id');
        }else{
            $all_auctions = [];
        }

        // $comments = $auctions->pluck('comments');    
        return view('cms::frontend.pages.vendas', compact('auctions','all_auctions'));
    }

    public function contacts()
    {
        return view('cms::frontend.pages.contactos');
    }

    public function avancada()
    {
        $data = [
            'type_goods' => TypeGoods::all(),
            // 'sub_type_of_goods' => SubTypeGoods::all(),
            'districts' => Distrito::all()
        ];

        return view('cms::frontend.pages.avancada', $data);
    }

    public function mapa()
    {
        return view('cms::frontend.pages.mapa');
    }

    public function leilao($id)
    {
        $auction = Auction::withoutGlobalScopes(['date','other_date'])->find($id);
        
        // $sub_type_of_goods = SubTypeGoods::all();
        return view('cms::frontend.pages.leilao',compact('auction'));
    }

    public function getCounties()
    {
        $concelhos = Concelho::where('CodigoDistrito', request()->input('id'))->get();

        return view('layouts.backend.partials.concelhos', ['array'=> $concelhos, 'name'=>'county', 'selected'=> '', 'class'=> 'county', 'id' => 0]);
    }

    public function getParishs()
    {
        $freguesias = FreguesiaActual::where('IdConcelho', request()->input('id'))->get();

        return view('layouts.backend.partials.concelhos', ['array'=> $freguesias, 'name'=>'county', 'selected'=> '', 'class'=> 'county', 'id' => 0]);
    }

    public function subtipos()
    {
        $sub_type_of_goods =  SubTypeGoods::where('type_good_id', request('id'))->get();

        return view('layouts.backend.partials.select_object_input_sub_tipos', ['array'=> $sub_type_of_goods, 'name'=>'subtipos', 'selected'=> '', 'class'=> '','id' => 0]);
    }

    public function lotes_detalhes($id)    
    {
        $lot = Lot::find($id);
        $auction = Auction::withoutGlobalScopes(['date','other_date'])->find($lot->auction_id);
        // $sub_type_of_goods = SubTypeGoods::all();
        return view('cms::frontend.pages.leilao-lotes', compact('lot','auction'));   
    }    

    public function empresa()
    {
        $menu_institucional = MenuInstitucional::first();
        return view('cms::frontend.pages.empresa', compact('menu_institucional'));
    } 

    public function biblioteca()
    {
        $menus = MenuBiblioteca::all();
        // $sub_type_of_goods = SubTypeGoods::all();
        $auctions = Auction::withoutGlobalScopes(['date','other_date'])->where('date', '<', Carbon::now()->format('Y-m-d H:i:s'))->where('active', 0)->orderBy('date')->get();
        $auction_collect = collect();
        foreach($auctions as $auction)
        {
            if(Carbon::now()->diffInSeconds(Carbon::parse($auction->date), false) < 0)
            {
                $auction_collect->push($auction);
            }
        }

        $auctions = $auction_collect;
        return view('cms::frontend.pages.biblioteca', compact('menus','auctions'));
    }      

    public function insolvencia($name1,$name2)
    {
        // $sub_type_of_goods = SubTypeGoods::all();
        $menus = MenuBiblioteca::all();
        $menu_biblioteca = MenuBiblioteca::whereName($name1)->first();
        $sub_menu = SubMenuBiblioteca::whereName($name2)->where('menu_biblioteca_id',$menu_biblioteca->id)->first();
        return view('cms::frontend.pages.insolvencia', compact('menus','sub_menu'));
    }   

    public function filter_by_sub_types($id, $type)
    {
        $auctions = collect();
        $auctions_ids = [];

        if($type == 'Tipo de bens')
        {
            foreach(Lot::all() as $lot)
            {
                foreach($lot->type_goods as $type_good)
                {
                    if($type_good->type_good_id == $id)
                    {
                        if(!in_array($lot->auction_id, $auctions_ids))
                        {
                            $auctions_aux = $lot->auction_id;
                            $auction = Auction::where('id',$auctions_aux);
                            $auctions->push($auction);
                            array_push($auctions_ids, $auctions_aux);
                        }
                    }
                }
            }
        }else{
            foreach(Lot::all() as $lot)
            {
                foreach($lot->type_goods as $type_good)
                {
                    if($type_good->id == $id)
                    {
                        if(!in_array($lot->auction_id, $auctions_ids))
                        {
                            $auctions_aux = $lot->auction_id;
                            $auction = Auction::where('id',$auctions_aux);
                            $auctions->push($auction);
                            array_push($auctions_ids, $auctions_aux);
                        }
                    }
                }
            }
        }

        $auctions = Auction::withoutGlobalScope('other_date')->where('active',1)->whereIn('id',$auctions_ids)->paginate($this->auctions_per_page);

        if($auctions){
            $all_auctions = $auctions->pluck('id');
        }else{
            $all_auctions = [];
        }
        // $sub_type_of_goods = SubTypeGoods::all();
        // $auctions = $auctions->paginate($this->auctions_per_page);
        if(request()->ajax()) {
            return [
                'auctions' => view('cms::frontend.pages.vendas_partial')->with(compact('auctions'))->render(),
                'next_page' => $auctions->nextPageUrl()
            ];
        }

        return view('cms::frontend.pages.vendas', compact('auctions','all_auctions')); 
    }

    public function cookies()
    {
        // $sub_type_of_goods = SubTypeGoods::all();
        return view('cms::frontend.pages.cookies');
    }  

    public function litigio()
    {
        // $sub_type_of_goods = SubTypeGoods::all();
        return view('cms::frontend.pages.litigio');
    }  

    public function privacidade()
    {
        // $sub_type_of_goods = SubTypeGoods::all();
        return view('cms::frontend.pages.privacidade');
    }

    public function getStoragePdf($id,$with=null,$height=null)    
    {
        $file = Storage::disk('pdf_files')->get($id);
        $caminho=Storage::disk('pdf_files')->getDriver()->getAdapter()->getPathPrefix().$id;
        $type = Storage::disk('pdf_files')->mimeType($id);

        if($type === 'application/pdf'){
            return Response::make(file_get_contents($caminho), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => 'inline; filename="'.$id.'"'
            ]);
        }
    }

    public function pesquisa()
    {
        
        $lots = collect();
        
        //check for distrito
        $lots->merge(Lot::distrito(request('search')));
    }        

    public function pesquisaAvancada()
    {
        if(request()->ajax()){
            $auctions = Auction::withoutGlobalScope('other_date')->whereIn('id', request('auctions'))->paginate($this->auctions_per_page);
            return [
                'auctions' => view('cms::frontend.pages.vendas_partial')->with(compact('auctions'))->render(),
                'next_page' => $auctions->nextPageUrl()
            ];
        }

        $lots = null;
        if(request('distrito') != "all")
        {
            $lots = Lot::distrito(Distrito::find(request('distrito'))->Designacao);
        }

        if(request('concelho') != "all")
        {
            $lots = $lots->concelho(Concelho::find(request('concelho'))->Designacao);
        }
        
        if(request('freguesia') != "all")
        {
            $lots = $lots->freguesia(FreguesiaActual::find(request('freguesia'))->Designacao);
        }

        $array = $this->checkForTypePrices();
        foreach($array as $type)
        {
            if($this->filterBasePrice($type, $lots) != false){
                $lots = $this->filterBasePrice($type, $lots);
            }
        }

        if(request('tipo') == 'all' && request('subtipo') == 'all')
        {

        }else if(request('tipo') == 'all' && request('subtipo') != 'all'){
            if($lots){
                $lots_aux = $lots;
                $lots = collect();
                foreach($lots_aux as $lot)
                {
                    $lots_aux_2 = $lot->type_goods()->where('sub_type_goods_id', request('subtipo'))->get();
                    if(count($lots_aux_2) > 0){
                        $lots->push($lot);
                    }
                }
            }else{
                $lots = collect();
                foreach(Lot::all() as $lot){

                    $lots_aux = $lot->type_goods()->where('sub_type_goods_id', request('subtipo'))->get();
                    if(count($lots_aux) > 0){
                        $lots->push($lot);
                    }
                }
            }
        }else if(request('tipo') != 'all' && request('subtipo') == 'all'){
            $sub_type_goods_id = SubTypeGoods::where('type_good_id', request('tipo'))->pluck('id');
            if($lots){
                $lots_aux = $lots;
                $lots = collect();
                foreach($lots_aux as $lot)
                {
                    $lots_aux_2 = $lot->type_goods()->whereIn('sub_type_goods_id', $sub_type_goods_id)->get();
                    if(count($lots_aux_2) > 0){
                        $lots->push($lot);
                    }
                }
            }else{
                $lots = collect();
                foreach(Lot::all() as $lot){

                    $lots_aux = $lot->type_goods()->whereIn('sub_type_goods_id', $sub_type_goods_id)->get();
                    if(count($lots_aux) > 0){
                        $lots->push($lot);
                    }
                }
            }
        }else{
            if($lots){
                $lots_aux = $lots;
                $lots = collect();
                foreach($lots_aux as $lot)
                {
                    $lots_aux_2 = $lot->type_goods()->where('sub_type_goods_id', request('subtipo'))->get();
                    if(count($lots_aux_2) > 0){
                        $lots->push($lot);
                    }
                }
            }else{
                $lots = collect();
                foreach(Lot::all() as $lot){

                    $lots_aux = $lot->type_goods()->where('sub_type_goods_id', request('subtipo'))->get();
                    if(count($lots_aux) > 0){
                        $lots->push($lot);
                    }
                }
            }
        }

        if($lots){
            $auctions_aux = $lots->pluck('auction_id');
            $auctions = Auction::withoutGlobalScope('other_date')->whereIn('id',$auctions_aux);

        }else{
            $auctions = null;
        }

        //Filtrar por categoria
        if(request('categoria') != "all"){
            if($auctions){
                $auctions->categoria(request('categoria'));
            }else{
                $auctions = Auction::withoutGlobalScope('other_date')->categoria(request('categoria'));                
            }
        }

        if(request('processo') != "")
        {
            if($auctions){
                $auctions->process(request('number_process'));
            }else{
                $auctions = Auction::withoutGlobalScope('other_date')->process(request('number_process'));
            }
        }

        if(request('referencia') != "")
        {
            if(auctions){
                $auctions->referencia(request('referencia'));
            }else{
                $auctions = Auction::withoutGlobalScope('other_date')->referencia(request('referencia'));
            }
        }

        if(request('designacao') != "")
        {
            if($auctions){
                $auctions->executed(request('designacao'));
            }else{
                $auctions = Auction::withoutGlobalScope('other_date')->executed(request('designacao'));
            }
        }

        if($auctions){
            $all_auctions = $auctions->pluck('id');
        }else{
            $all_auctions = [];
        }

        if($auctions){
            $auctions = $auctions->where('active', 1)->orderBy('date','desc')->paginate($this->auctions_per_page);
            //$auctions = $auctions->where('date', '>', Carbon::now()->format('Y-m-d H:i:s'))->orderBy('date')->get();
        }else{
            $auctions = Auction::withoutGlobalScope('other_date')->where('active', 1)->orderBy('date','desc')->paginate($this->auctions_per_page);
        }

        return view('cms::frontend.pages.vendas', compact('auctions', 'all_auctions'));
    } 

    public function pesquisaDatas($dia, $mes, $ano)
    {
        $data_aux = $ano . '/'. sprintf("%02d",$mes) . '/' .sprintf("%02d",$dia);
        $date_beg = Carbon::parse($data_aux)->setTime(0,0,0);
        $date_fin = Carbon::parse($data_aux)->setTime(23,59,59);
        $auctions = Auction::withoutGlobalScopes(['date','other_date'])->where('date','>=', $date_beg)->where('date','<=', $date_fin)->get();

        $comments = $auctions->pluck('comments');    

        return view('cms::frontend.pages.vendas', compact('auctions','comments'));
    }

    public function sendEmail()
    {
        $dados = [
            'nome' => request('nome'),
            'email' => request('email'),
            'assunto' => request('assunto'),    
            'mensagem' => request('mensagem')
        ];

        Mail::send('cms::frontend.pages.emails.email', $dados , function($message){
            $message->from('mail@thesilverfactory.pt');
            $message->to('miguel.barros@thesilverfactory.pt')->subject('Pedido de contacto');
        });
    }

    public function checkForTypePrices()
    {
        $types = [];
        if(request('valor_base_min') != '' || request('valor_base_max') != '')
        {
            array_push($types, 'valor_base');
        }

        if(request('valor_minimo_min') != '' || request('valor_minimo_max') != "")
        {
            array_push($types, 'valor_minimo');
        }

        return $types;
    }

    public function filterBasePrice($type, $lots)
    {
        if($type == 'valor_base')
        {
            $valor_base_min = request('valor_base_min');
            $valor_base_max = request('valor_base_max');

            if($valor_base_min != "" && $valor_base_max == "")
            {
                $valor_base_min = str_replace(',','',$valor_base_min);
                $valorBaseMin = intval($valor_base_min * 100);
                if($lots == null){
                    $lots = Lot::singleBasePrice('min', $valorBaseMin);
                }else{
                    $lots = $lots->singleBasePrice('min', $valorBaseMin);
                }
            }
            else if($valor_base_min != "" &&  $valor_base_max != "")
            {
                $valor_base_min = str_replace(',','',$valor_base_min);
                $valor_base_max = str_replace(',','',$valor_base_max);
                $valorBaseMin = intval($valor_base_min * 100);
                $valorBaseMax = intval($valor_base_max * 100);

                if($lots == null){
                    $lots = Lot::basePrice($valorBaseMin,$valorBaseMax);
                }else{
                    $lots = $lots->basePrice($valorBaseMin,$valorBaseMax);
                }
            }
            else if($valor_base_min == "" && $valor_base_max != "")
            {
                $valor_base_max = str_replace(',','',$valor_base_max);
                $valorBaseMax = intval($valor_base_max * 100);

                if($lots == null){
                    $lots = Lot::singleBasePrice('max', $valorBaseMax);
                }else{
                    $lots = $lots->singleBasePrice('max', $valorBaseMax);
                }
            }
            else
            {
                return false;
            }

            return $lots;
        }
        else if($type == 'valor_abertura')
        {
            $valor_abertura_min = request('valor_abertura_min');
            $valor_abertura_max = request('valor_abertura_max');

            if($valor_abertura_min != "" && $valor_abertura_max == "")
            {
                $valor_abertura_min = str_replace(',','',$valor_abertura_min);
                $valorAberturaMin = intval($valor_abertura_min * 100);

                if($lots == null){
                    $lots = Lot::singleOpeningPrice('min', $valorAberturaMin);
                }else{
                    $lots = $lots->singleOpeningPrice('min', $valorAberturaMin);
                }
            }
            else if($valor_abertura_min != "" &&  $valor_abertura_max != "")
            {
                $valor_abertura_min = str_replace(',','',$valor_abertura_min);
                $valor_abertura_max = str_replace(',','',$valor_abertura_max);
                $valorAberturaMin = intval($valor_abertura_min * 100);
                $valorAberturaMax = intval($valor_abertura_max * 100);

                if($lots == null){
                    $lots = Lot::openingPrice($valorAberturaMin,$valorAberturaMax);
                }else{
                    $lots = $lots->openingPrice($valorAberturaMin,$valorAberturaMax);
                }
            }
            else if($valor_abertura_min == "" && $valor_abertura_max != "")
            {
                $valor_abertura_max = str_replace(',','',$valor_abertura_max);
                $valorAberturaMax = intval($valor_abertura_max * 100);

                if($lots == null){
                    $lots = Lot::singleOpeningPrice('max', $valorAberturaMax);
                }else{
                    $lots = $lots->singleOpeningPrice('max', $valorAberturaMax);
                }
            }
            else
            {
                return false;
            }

            return $lots;
        }
        else if($type == 'valor_minimo')
        {
            $valor_minimo_min = request('valor_minimo_min');
            $valor_minimo_max = request('valor_minimo_max');

            if($valor_minimo_min != "" && $valor_minimo_max == "")
            {
                $valor_minimo_min = str_replace(',','',$valor_minimo_min);
                $valorMinimoMin = intval($valor_minimo_min * 100);


                if($lots == null){
                    $lots = Lot::singleMinPrice('min', $valorMinimoMin);
                }else{
                    $lots = $lots->singleMinPrice('min', $valorMinimoMin);
                }
            }
            else if($valor_minimo_min != "" &&  $valor_minimo_max != "")
            {
                $valor_minimo_min = str_replace(',','',$valor_minimo_min);
                $valor_minimo_max = str_replace(',','',$valor_minimo_max);
                $valorMinimoMin = intval($valor_minimo_min * 100);
                $valorMinimoMax = intval($valor_minimo_max * 100);

                if($lots == null){
                    $lots = Lot::minPrice($valorMinimoMin,$valorMinimoMax);
                }else{
                    $lots = $lots->minPrice($valorMinimoMin,$valorMinimoMax);
                }
            }
            else if($valor_minimo_min == "" && $valor_minimo_max != "")
            {
                $valor_minimo_max = str_replace(',','',$valor_minimo_max);
                $valorMinimoMax = intval($valor_minimo_max * 100);

                if($lots == null){
                    $lots = Lot::singleMinPrice('max', $valorMinimoMax);
                }else{
                    $lots = $lots->singleMinPrice('max', $valorMinimoMax);
                }
            }
            else
            {
                return false;
            }
            return $lots;
        }
    }

    public function registar()
    {
        $countries = GlobalCountry::all();
        return view('cms::frontend.pages.registar', compact('countries'));
    }

    public function entrar()
    {
        if($user = auth()->user()){
            if($user->is_backend_user()){
                return redirect('/backend/cms/edit-page');
            }
            return redirect()->route('cms::frontend::model.area-reservada');
        }
        return view('cms::frontend.pages.entrar');
    }

    public function areaReservada()
    {
        if(!auth()->check())
            return redirect()->route('cms::frontend::index');

        if(request()->session()->has('tab'))
        {
            $tab = 'dados_de_utilizador';
        }
        else
        {
            $tab = 'tab';
        }

        request()->session()->forget('tab');

        $countries = GlobalCountry::all();
        $user = auth()->user();
        $profile = $user->profileReservedArea()->first();
        $auctions = Auction::withoutGlobalScope('other_date')->latest()->take(5)->get();
        return view('cms::frontend.pages.area-reservada', compact('countries','auctions','user','profile','tab'));
    }

    public function createUserAreaReservada(LoginAreaReservadaRequest $request)
    {
        $confirmation_code = str_random(30);

        $user = User::create([
            'name' => $request->input('firstname'),
            'email' => $request->input('email'),
            'password' => bcrypt($request->input('password')),
            'confirmation_code' => $confirmation_code
        ]);

        $user->profileReservedArea()->create([
            'email' => $request->input('email'),
            'firstname' => $request->input('firstname'),
            'lastname' => $request->input('lastname'),
            'nif' => $request->input('nif'),
            'cartao_cidadao' => $request->input('cartao_cidadao'),
            'data_validade' => Carbon::parse($request->input('data_validade'))->format('Y-m-d H:i'),
            'telefone' => $request->input('telefone'),
            'nome_fiscal' => $request->input('nome_fiscal'),
            'nipc' => $request->input('nipc'),
            'country' => $request->input('country'),
            'address' => $request->input('address'),
            'cod_postal' => $request->input('cod_postal'),
            'local' => $request->input('local')
        ]);

        $array = [
            'confirmation_code' => $confirmation_code
        ];

        Mail::send('cms::frontend.emails.verifica_conta',$array, function($message) use($request){
            $message->from('mail@thesilverfactory.pt');
            $message->to($request->input('email'))
                ->subject('Verifique o seu email');
        });

        return redirect()->route('cms::frontend::index');
    }

    public function confirmarConta($confirmation_code)
    {
        if( ! $confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }

        $user = User::whereConfirmationCode($confirmation_code)->first();

        if(! $user){
            throw new InvalidConfirmationCodeException;
        }

        $user->confirmed = 1;
        $user->confirmation_code = null;
        $user->save();

        Mail::send('cms::frontend.emails.activacao_successo',$array, function($message) use($request){
            $message->from('mail@thesilverfactory.pt');
            $message->to($request->input('email'))
                ->subject('Verifique o seu email');
        });

        return redirect()->route('cms::frontend::model.entrar');
    }

    public function login()
    {
        $credentials = [
            'email' => request('email'),
            'password' => request('password'),
            'confirmed' => 1,
            'confirmed_by_admin' => 1
        ];

        if(!auth()->attempt($credentials))
            return redirect()->back()->withInput();

        $user = auth()->user();

        return redirect()->route('cms::frontend::model.area-reservada');    
    }

    public function changeInvoiceInformation(AlterarDadosFacturacaoRequest $request, $id)
    {
        $profile = ProfileReservedArea::findOrFail($id);
        $profile->update([
            'nome_fiscal' => $request->input('nome_fiscal'),
            'nipc' => $request->input('nipc'),
            'country' => $request->input('country'),
            'address' => $request->input('address'),
            'cod_postal' => $request->input('cod_postal'),
            'local' => $request->input('local')
        ]);

        request()->session()->forget('tab');
        request()->session()->put('tab','dados_de_utilizador');

        return redirect()->route('cms::frontend::model.area-reservada');
    }

    public function recoveryPassword()
    {
        return view('cms::frontend.pages.recover_password');
    }

    public function changePassword(AlterarPassword $request)
    {
        $user = User::where('email', $request->email)->firstOrFail();
        $user->update([
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('cms::frontend::model.entrar');
    }

    public function changePersonalInformation(AlterarDadosPessoaisRequest $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'password' => bcrypt($request->input('new_password'))
        ]);

        request()->session()->forget('tab');
        request()->session()->put('tab','dados_de_utilizador');

        return redirect()->route('cms::frontend::model.area-reservada');
    }

    public function broadcastEvent()
    {
        $price = request('price');
        $lot = request('lot_id');
        $user_id = auth()->id();
        event(new UserHasBidding($price, $lot, $user_id));
    }

    public function checkIfPriceValid()
    {
        $price = str_replace(',','', request('price'));
        $price = intval(str_replace('.','', $price)*100);
        $lot = Lot::find(request('lot_id'));
        if(count($lot->pivot) > 0){
            $lot->pivot->latest()->first();
        }else{
            $last_price = $lot->opening_price;
        }
        $opening_price = $lot->opening_price;
        $definition = BiddingsDefinitions::where('min_bidding', '<=', $opening_price)->where('max_bidding','>=', $opening_price)->first();
        $next_bidding = $definition->next_definition;
        $min_bidding = $last_price + $next_bidding;
        if($price >= $last_price){
            return "true";
        }
        return "false";
    }
}

