<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18-05-2016
 * Time: 11:14
 */

namespace Brainy\Gallery\Repositories;


interface MediaItemRepositoryInterface {

    public function updateTags($id,$tags);

    public function updateSlug($id,$data);

    public function updateTitleAndDescription($id,$data);

    public function moveTo($id,$nova_localizacao);

    public function storeItems($request,$id);

    public function storeEmbedLink($request);

    public function traslateItem($data,$item);

    public function checkifSlugExists($slug,$id);

    public function build_final_file($filename, $file, $id);


}