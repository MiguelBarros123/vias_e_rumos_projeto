<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class MenuBiblioteca extends Model
{
    protected $guarded = [];
    protected $with = ['sub_menus_biblioteca'];

    public function sub_menus_biblioteca()
    {
        return $this->hasMany(SubMenuBiblioteca::class);
    }

    public function getRouteKeyName()
    {
        return 'name';
    }
}
