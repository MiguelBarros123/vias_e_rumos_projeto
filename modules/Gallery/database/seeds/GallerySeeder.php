<?php

use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('media_items')->delete();
        \Illuminate\Support\Facades\DB::table('media_folders')->delete();

        \Brainy\Gallery\Models\MediaFolder::create([
            'title'=>'galeria',
            'localization'=>'galeria',
            'parent_folder_id'=>'0',
            'container_name'=>'null',
            'size'=>'0MB',
        ]);







    }
}
