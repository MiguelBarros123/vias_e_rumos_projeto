@extends('layouts.app')

@section('content')
<div class="container login-form">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-titulo">
                    <span class="titulo">
                        Dashboard
                    </span>
                </div>

                <div class="panel-body login-body">
                    <p>You are logged in!</p>
                    <a href="{{route('gallery::backend::model.index')}}">see gallery</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
