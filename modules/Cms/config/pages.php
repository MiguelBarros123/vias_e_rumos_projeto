<?php
return [
    // Menu items denote the menus of the pages that this module renders
    'menu-items' => [
        [
            // Label for the menu item (mandatory)
            'label' => 'Home',
            'is_contacts' => false,
            'route' => 'cms::frontend::index',
            'route_url' => 'home',
            'page-content' => [
                [
                    //an element of this type has an image, title, description and a visible option
                    'type'=>'text-simple',
                    'panelName'=>'cms::backend.model.pages.index.company',
                    'hasPResources'=> false,
                ],
            ],

        ],
        [
            'label' => 'Contactos',
            'is_contacts' => true,
            'route' => 'cms::frontend::model.contacts',
            'route_url' => 'contacts',
            'page-content' => [
                [
                    'type'=>'input_field',
                    'label'=>'telemovel',
                    'section'=>'contacts',
                    'key'=>'mobile',
                    'value'=>'',
                ],
                [
                    'type'=>'input_field',
                    'label'=>'telefone',
                    'section'=>'contacts',
                    'key'=>'phone',
                    'value'=>'',
                ],

                [
                    'type'=>'input_field',
                    'label'=>'email',
                    'section'=>'contacts',
                    'key'=>'email',
                    'value'=>'',
                ],

                [
                    'type'=>'input_field',
                    'label'=>'Email do Formulario',
                    'section'=>'form',
                    'key'=>'form_email',
                    'value'=>'geral@thesilverfactory.pt',
                ],
            ],
        ],

    ],
];
