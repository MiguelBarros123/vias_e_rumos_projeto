/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    config.toolbarGroups = [
        { name: 'styles', groups: [ 'styles' ] },
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        { name: 'insert', groups: [ 'insert' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        { name: 'forms', groups: [ 'forms' ] },
        { name: 'colors', groups: [ 'colors' ] },
        '/',
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        { name: 'links', groups: [ 'links' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        '/',
        { name: 'tools', groups: [ 'tools' ] },
        { name: 'others', groups: [ 'others' ] },
        { name: 'about', groups: [ 'about' ] },
        { name: 'holders', groups: ['placeholder_select']}
    ];

    config.justifyClasses = [ 'text-left', 'text-center', 'text-right', 'rtejustify' ];

    config.placeholder_select={
        placeholders:['nome','apelido','email','nome_completo','data_nascimento','nome_empresa','data_atual','unsubscribe','subscribe','morada_empresa'],
        format:'{{ %placeholder% }}'
    };
    config.removeButtons = 'Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,CreateDiv,JustifyBlock,BidiLtr,BidiRtl,Language,Image,Flash,Table,PageBreak,Iframe,Smiley,ShowBlocks,NewPage,Styles,About,Indent,Outdent,Blockquote';

    config.extraPlugins = 'placeholder_select';
};