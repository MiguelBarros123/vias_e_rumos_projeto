<?php

namespace Brainy\Gallery\Controllers\Backend;

use Brainy\Framework\Controllers\BackendController;
use Brainy\Gallery\Models\MediaFolder;
use Brainy\Gallery\Models\MediaItem;
use Brainy\Gallery\Models\Tag;
use Brainy\Gallery\Models\UploadHandler;
use Brainy\Gallery\Repositories\MediaItemRepositoryInterface;
use Brainy\Gallery\Requests\CreateEmbedRequest;
use Brainy\Gallery\Requests\UpdateMediaItemRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Storage;


class MediaItemController extends BackendController
{
    protected $media;

    public function __construct(MediaItemRepositoryInterface $media)
    {
        parent::__construct();
        $this->middleware('auth');
        $this->media = $media;
    }


    public function edit($id)
    {

        $item = MediaItem::findOrFail($id);

        if($item->type != 'link'){
            if (! Storage::disk('galeria')->has($item->identifier_token)) {
                return abort(404, 'File \'' . $item->identifier_token . '\' not found');
            }
        }




        $locales=config('translatable.locales');
        $tags=$item->mytagsnames();




        return view('gallery::backend.model.edit', [
            'item' => $item,
            'pasta' => $item->media_folder,
            'all_folders' => MediaFolder::all(),
            'tags' => $tags->toJson(),
            'locales' => $locales,
        ]);
    }

    public function update(UpdateMediaItemRequest $request,$id)
    {
        $it=MediaItem::findOrFail($id);
        if($it->type == 'link'){
            if($request->link != $it->indentifier_token){
                $it->update(array('indentifier_token'=>$request->link));
            }

        }else{
            $errors=$this->media->updateSlug($id,array_except($request->input(),array('tags','_method','_token','parent_folder_id','folder_id','title')));
            if($errors->count() > 0){
                return redirect()->back()->with('error-message',Lang::get('messages.core.galeria.edit.slugs_exist',['slugs'=>implode(',',$errors->toArray())]));

            }
        }


        $this->media->updateTags($id,$request->tags);
        $this->media->updateTitleAndDescription($id,array_except($request->input(),array('tags','_method','_token','parent_folder_id','folder_id','title')));
        $this->media->moveTo($id,$request->folder_id);
        return redirect()->back()->with('sucess-message',trans('gallery::backend.messages.success.update'));

    }

    public function editImage($id)
    {
        return view('gallery::backend.model.edit_image', [
            'item' => MediaItem::findOrFail($id),
        ]);
    }

    public function storeImage(Request $request,$id)
    {
        $folder_id=MediaItem::find($id)->media_folder->id;
        $errors=$this->media->storeItems($request->allFiles(),$folder_id);

        return response()->json(['errors' => $errors]);
    }

    public function saveAsset(Request $request)
    {

        if (isset($request->last_chunk)) {
            $file = $request->allFiles()['files'][0];
            $filename = $request->header('Content-Disposition');


            preg_match('/filename=\"(.*)\"/', $filename, $filename);
            $filename = $filename[1];

            $this->media->writeToTempFile($filename, $file);

            if ($request->last_chunk == 1) {

                $tempFile = $this->media->getTempFile($filename);


                if ($tempFile != null) {

                    $errors = $this->media->storeItems([[$tempFile]],$request->pasta_final);
                    $this->media->deleteTempFile($filename);

                    if(!$errors->isEmpty()) {
                        return response()->json(['errors' => $errors], 412);
                    }
                } else {
                    $errors['chunked-file'] = 'Sorry, an internal error occurred. Could not save uploaded file.';
                    return response()->json(['errors' => $errors], 412);
                }

                return response()->json(['ok'=>'ok']);



            }

        } else {


            $errors = $this->media->storeItems($request->allFiles(),$request->pasta_final);
            if ($errors->isEmpty()) {
                return response()->json(array('ok'=>'ok'));
            } else {
                return response()->json(array('errors'=>$errors),412);
            }
        }
    }

    public function uploadAssetHandler(Request $request)
    {

    }

    public function storeEmbedLink(CreateEmbedRequest $request)
    {
        $arr=array_except($request->input(),array('_token'));
        $this->media->storeEmbedLink($arr);
        return redirect()->back()->with('sucess-message', trans('gallery::backend.messages.success.added'));
    }

    public function downloadItem($way)
    {
        $unique=MediaItem::where('identifier_token',$way)->first();
        $unique->increment('n_of_downloads');
        return response()->download(Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$unique->identifier_token,$unique->title);
    }

    public function fetchNewFiles()
    {
        return response()->json(MediaFolder::with('media_items')->get());
    }
}