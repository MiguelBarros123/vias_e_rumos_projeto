<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22-04-2016
 * Time: 12:20
 */

namespace Brainy\Gallery\Repositories;


interface MediaRepositoryInterface {

    public function getAllMediaFromFolder($folder,$filtro);

    public function scanFolderContents($folder_id,$filtro);

    public function getFilesFromFolder($folder_id,$filtro);

    public function getImageThumbnail($path);

    public function getFoldersFromFolder($folder_id,$filtro);

    public function buildBreadcrumb($id);

    public function deleteItems($items);

    public function destroyItems($items);

    public function moveItems($items,$final_folder);

    public function recoverItems($items);

    public function getallsubfolders($folder,&$subfolders);

    public function copyItems($items,$folder);

    public function copyFolderItem($items,$folder);

    public function copyMediaItem($items, $folder);

    public function folderHasUndeletedFiles($folder);

    public function getAllArchivedAssets($filtro);

    public function folderHaveFilesOfType($folder_id,$items);


    public function zipFilesAndFolders($files,$folders);







}