<?php

namespace Brainy\Gallery\Controllers\Backend;

use Brainy\Framework\Controllers\BackendController;
use Brainy\Framework\Facades\Settings;
use Brainy\Gallery\Models\MediaFolder;
use Brainy\Gallery\Models\MediaItem;
use Brainy\Gallery\Repositories\MediaRepositoryInterface;
use Carbon\Carbon;
use Faker\Provider\File;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Http\Response as IlluminateResponse;

class ModelBackendController extends BackendController
{
    protected $medias;

    use AuthorizesRequests;
    
    public function __construct(MediaRepositoryInterface $medias)
    {
        parent::__construct();
        $this->media = $medias;
    }

    public function index()
    {

       $folder=MediaFolder::where('parent_folder_id',0)->firstOrFail();
        return redirect()->route('gallery::backend::galery',[$folder->id,'all']);

    }

    public function create()
    {
        return view('gallery::backend.model.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [

        ]);
    }

    public function show($model)
    {
        return view('gallery::backend.model.show', compact('model'));
    }


    public function edit($model)
    {

    }

    public function showUploadAsset($model)
    {
        return view('gallery::backend.model.upload', [
            'pasta' => MediaFolder::findOrFail($model),
            'all_folders' => MediaFolder::all(),
            'locales' => config('translatable.locales'),
        ]);
    }

    public function update(Request $request, $model)
    {
        $this->validate($request, [

        ]);
    }

    public function delete($model)
    {
        return back();
    }

    public function getGalery($id,$nome)
    {
        $settings=Settings::get("gallery.list-mode",0,'user','gallery::backend::galery');


        return view('gallery::backend.model.index', [
            'id_pasta' => $id,
            'list_mode' => $settings,
            'folder' => MediaFolder::findOrFail($id),
            'all_folders' => MediaFolder::all(),
            'localization' => $this->media->buildBreadcrumb($id),
        ]);
    }

    public function getAssets(Request $request,$id)
    {

        $filtro=$request->d;
        $content=$this->media->getAllMediaFromFolder($id,$filtro);
        //dd($content['folders'][0]->toArray());
        return response()->json($content, 200);
    }

    public function getImageThumbnail($id,$with=null,$height=null)
    {
        $imagem = MediaItem::withTrashed()->where('identifier_token',$id);
        $file = Storage::disk('galeria')->get($id);
        $type = Storage::disk('galeria')->mimeType($id);
        if(strcmp($type,".svg")) {

        }
        else {

            if ($with != null && $height != null) {
                $caminho = Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix() . $id;

                $img = Image::cache(function ($image) use ($caminho, $with, $height) {
                    return $image->make($caminho)->fit($with, $height);
                }, 43200);

                return $this->buildResponse($img);

            }
        }

        return response($file,200,['Content-Type'=>$type]);
    }

    public function changeGalleryMode($mode)
    {
        auth()->user()->details->user_preferences()
            ->where('preference','show_list_gallery')->first()
            ->update(array('action'=>$mode));

        return redirect()->back();

    }

    public function changeGalleryAssetMode($mode)
    {
        auth()->user()->user_info->user_preferences()
            ->where('preference','show_list_gallery_archive')->first()
            ->update(array('action'=>$mode));

        return redirect()->back();

    }

    public function changeItemsLocation(Request $request)
    {
        if(array_key_exists('files',$request->dados)){
            foreach ($request->dados['files'] as $file) {
                MediaItem::find($file['id'])->update(array('media_folder_id' => $request->pai));
            }
        }

        if(array_key_exists('folders',$request->dados)){
            foreach ($request->dados['folders'] as $folder ){
                MediaFolder::find($folder['id'])->update(array('parent_folder_id'=>$request->pai));
            }
        }

        return redirect()->back();
    }

    public function deleteItems(Request $request)
    {
        $not_deleted=$this->media->deleteItems($request->input());

        if($not_deleted == 0){
            return redirect()->back()->with('sucess-message',trans('gallery::backend.messages.success.delete'));
        }else{
            return redirect()->back()->with('error-message', trans('gallery::backend.messages.error.delete',['total'=>$not_deleted]));
        }
    }

    public function destroyItems(Request $request)
    {
        $this->media->destroyItems($request->input());
        return redirect()->back()->with('sucess-message', trans('gallery::backend.messages.success.delete'));
    }

    public function moveItems(Request $request)
    {
        $this->media->moveItems($request->input(),$request->final_folder);
        return redirect()->back()->with('sucess-message',trans('gallery::backend.messages.success.moved'));
    }

    public function recoverItems(Request $request)
    {
        $this->media->recoverItems($request->input());
        return redirect()->back()->with('sucess-message',trans('gallery::backend.messages.success.recover'));
    }

    public function copyMediaAsset(Request $request,$id)
    {
        $errors = $this->media->copyItems($request->input(), $id);
        return response()->json($errors->isEmpty() ? ['ok' => 'ok'] : ['errors' => $errors]);
    }

    public function showArchive()
    {
         $settings=Settings::get("gallery.archive.list-mode",0,'user','gallery::backend::galery_archive');
        return view('gallery::backend.model.archive')->with('list_mode',$settings);
    }

    public function getArchivedAssets(Request $request)
    {
        $filtro=$request->d;
        $content=$this->media->getAllArchivedAssets($filtro);

        return response()->json($content, 200);
    }


    public function downloadItems(Request $request)
    {
        if ($request->input('folders') != "") {
            $folders=explode(',',$request->input('folders'));
        } else {
            $folders = [];
        }

        if ($request->input('files') != ""){
            $files = explode(',', $request->input('files'));
        } else {
            $files = [];
        }

        //if only files and
        if( count($folders) == 0 && count($files) == 1) {
            $unique = MediaItem::find($files[0]);
            $unique->increment('n_of_downloads');
            return response()->download(Storage::disk('galeria')
                    ->getDriver()->getAdapter()
                    ->getPathPrefix().$unique->identifier_token, $unique->title);
        }

        $location = $this->media->zipFilesAndFolders($files, $folders);
        $now = Carbon::now()->toDateTimeString();

        return response()->download($location, "galery_$now.zip");
    }

    function getBreadcrumb($pathId)
    {
        return $this->media->buildBreadcrumb($pathId);
    }

    function scanFolderContents($pathId)
    {
        return $this->media->scanFolderContents($pathId);
    }

    public function buildResponse($img) {
        // define mime type
        $mime = finfo_buffer(finfo_open(FILEINFO_MIME_TYPE), $img);

        // return http response
        return new IlluminateResponse($img, 200, array(
            'Content-Type' => $mime,
            'Cache-Control' => 'max-age='.(config('imagecache.lifetime')*60).', public',
            'Etag' => md5($img)
        ));
    }
    
}
