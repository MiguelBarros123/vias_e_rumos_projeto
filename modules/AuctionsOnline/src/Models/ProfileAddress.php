<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22-07-2016
 * Time: 18:03
 */

namespace Brainy\Profiles\Models;

use Illuminate\Database\Eloquent\Model;

class ProfileAddress extends Model
{
    protected $fillable = ['name', 'last_name', 'address', 'zipcode', 'company', 'town', 'phone', 'city', 'country', 'active_address'];
}
