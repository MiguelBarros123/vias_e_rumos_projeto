@extends('layouts.frontend.app')

@section('title', 'Vias e Rumos - Novo Registo')

@section('module-scripts')
    <script>
        $(document).on('click','#go_registo', function(e){
            e.preventDefault();
            console.log($(this).parent().attr('href'));
            window.location.href = $(this).parent().attr('href');
        });

        $(document).on('click', '#recover_password', function(e){
            e.preventDefault();
            window.location.href = $(this).parent().attr('href');
        });
    </script>
@endsection

@section('content')
<form action="{{ route('cms::frontend::model.login')}}" method="POST">
    {{ csrf_field() }}
    <div class="empresa-body cred-body login-body">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2>Login</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="inp_label">Email * </div>
                        <div class="inps_wrap_cred">
                            <input class="inp_av" placeholder="" name="email" type="email">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="inp_label">Password * </div>
                        <div class="inps_wrap_cred">
                            <input class="inp_av" placeholder="" name="password" type="password">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                       <span class="btn-line">
                            <button type="submit" name="" id="login" class="cred-btn">Entrar</button>
                        </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-lg-offset-2 col-md-offset-2 col-sm-12 col-sm-offset-0 col-xs-12 col-xs-offset-0">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="login-text">Se ainda não tem uma conta de utilizador faça o seu registo agora.</div>
                        
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="btn-line">
                            <a href="{{ route('cms::frontend::model.registar')}}">
                                <button name="" id="go_registo" class="cred-btn cred-btn-t">Registar</button>
                            </a>
                        </span>
                    </div>
                </div>
    
                <div class="row m-t-20">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="login-text">Se esqueceu a sua password pode proceder à sua recuperação.</div>
                        
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <span class="btn-line">
                            <a href="{{ route('cms::frontend::model.recovery_password') }}"><button name="" id="recover_password" class="cred-btn cred-btn-t">Recuperar Password</button></a>
                        </span>
                    </div>
                </div>
    
            </div>
        </div>
    </div>
</form>
@endsection
