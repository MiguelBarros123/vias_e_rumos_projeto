<div class="row tabs-back top_menu_page">
    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">
        <ul class="list-inline fonte_12_lato">
            <li class="@activePageTopMenu('profilereservedarea::backend.profilereservedarea.index')">
                <a class="icon_voltar" href="{{route('profilereservedarea::backend::profilereservedarea.index')}}">@lang('profilereservedarea::backend.menu.profiles')</a>
            </li>
        </ul>
    </div>
</div>
