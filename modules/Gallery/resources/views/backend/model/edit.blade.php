@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.css">
    <link rel="stylesheet" type="text/css" href="{{asset('css/gallery/backend.css')}}">

@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>
    <script src="{{asset('js/gallery/tagsinput.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.js"></script>
    <script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>

    @foreach($locales as $loc)
        <script>
            CKEDITOR.replace('editor_image_{{ $loc }}');
        </script>
    @endforeach

    <script>
        $(document).ready(function(){

            var sugest=$('#magicsuggest-faq').magicSuggest({
                method: 'get',
                data: "{{route('framework::framework.tags.index')}}",
                allowDuplicates: false,
                valueField: 'name'
            });
            sugest.setSelection(jQuery.parseJSON('{!! $tags !!}'));


            $('#files').click(function(){
                $('#fileupload').click();
            });

            var folders='{!! $all_folders !!}';
            var inicial="{{$pasta->id}}";

            var token=$('meta[name="csrf-token"]').attr('content');

            $('.listagem_pastas').selectslidemenu({ dados: folders,inicial:inicial,token: token});


            $('#open_select_folder').click(function(){
                $('.listagem_pastas').toggle();
            });
            $('.fecha_list_pastas').click(function(){
                $('.listagem_pastas').hide();
            });


            $('.lang_tabs li:first-child').tab('show');
            $('.lang_content').children().first().addClass('active');

        });
    </script>
@endsection



@section('content')
    <div class="conteudo_central2">
        <div class="container-fluid">

            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{route('gallery::backend::galery',array($pasta->id,$pasta->title))}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>@lang('gallery::backend.model.edit.go_back')</a></li>
                    </ul>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <ul class="breadcrumb">
                        <li>@lang('gallery::backend.model.edit.gallery')</li>
                        <li>{{$item->title}}</li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  msgs_gallery">



                    @if(\Illuminate\Support\Facades\Session::get('sucess-message'))
                        <div class="alert alert-sucess alert-dismissible " role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <i class="icon-permissoes icon9 pading_right" ></i>{{\Illuminate\Support\Facades\Session::get('sucess-message')}}
                        </div>
                    @endif

                    @if(\Illuminate\Support\Facades\Session::get('error-message'))
                        <div class="alert alert-erro alert-dismissible " role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <i class="icon-opcoes-eliminar icon9  pading_right"></i>{!!\Illuminate\Support\Facades\Session::get('error-message')!!}
                        </div>
                    @endif


                </div>
            </div>


            <form method="POST" action="{{route('gallery::backend::backend.gallery.media-item.update',array($item->id))}}" class="fontes_form" autocomplete="off">
                <input name="_method" type="hidden" value="PUT">
                {!! csrf_field() !!}

                <div class="row">
                    <div class="col-lg-12">


                        <div class="row">
                            <div class="col-lg-7 col-md-12 col-sm-12 col-xs-12 ">
                                <div class="sec_imagem_edit">

                                    @if($item->type=='imagem')
                                        <img class="image1" src="{{asset($item->mygaleryImage())}}">
                                    @else
                                        <i class="{{$item->mygaleryImage()}} icon100"></i>
                                    @endif

                                </div>

                            </div>
                            <div class="col-lg-5 col-md-12 col-sm-12 col-xs-12 no_padding">


                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-lg-12" >

                                            @if(\Illuminate\Support\Facades\Session::get('error-message')!= "")
                                                     <div class="alert alert-erro alert-dismissible " role="alert">
                                                              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                         <span aria-hidden="true">&times;</span></button>
                                                    <p><i class="icon-opcoes-eliminar"></i>{!!\Illuminate\Support\Facades\Session::get('error-message')!!}</p>
                                                              
                                                     </div>
                                                @endif

                                                        <!-- Nav tabs -->
                                                <ul class="tabs_brainy lang_tabs" role="tablist">
                                                    @foreach($locales as $loc)
                                                    <li role="presentation" class=""><a href="#{{$loc}}"  role="tab" data-toggle="tab">{{$loc}}</a></li>
                                                    @endforeach
                                                </ul>



                                                <!-- Tab panes -->
                                                <div class="tab-content  lang_content">

                                                    @foreach($locales as $loc)
                                                    <div role="tabpanel" class="tab-pane fade in  margem_20" id="{{$loc}}">


                                                        <div class="form-group fom-style-hidden2 @if($errors->first('titulo_'.$loc) ) has-error @endif">
                                                            <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.title')</label>
                                                            <input type="text" class="form-control" id="exampleInputEmail1" name="titulo_{{$loc}}" value="{{$item->translateOrNew($loc)->title}}">

                                                            {!! $errors->first('titulo_'.$loc,'<span class="validator_errors">:message</span>')!!}
                                                        </div>

                                                        <div class="form-group fom-style-hidden @if($errors->first('descricao_'.$loc) ) has-error @endif">
                                                            <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.description')</label>
                                                            <textarea class="form-control" rows="3" name="descricao_{{$loc}}" id="editor_image_{{ $loc }}">{{$item->translateOrNew($loc)->description}}</textarea>
                                                            {!! $errors->first('descricao_'.$loc,'<span class="validator_errors">:message</span>')!!}
                                                        </div>




                                                    </div>
                                                    @endforeach

                                                </div>

                                            @if($item->type=='link')
                                                <div class="form-group fom-style-hidden @if($errors->first('link') ) has-error @endif">
                                                    <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.embedLink')</label>
                                                    <textarea class="form-control  " rows="3" name="link">{{$item->identifier_token}}</textarea>
                                                    {!! $errors->first('link','<span class="validator_errors">:message</span>')!!}
                                                </div>
                                                @endif



                                        </div>

                                        <div class="col-lg-12">
                                            <div class="divider_line"></div>

                                            <div class="container-fluid margem_20">
                                                <div class="row">

                                                    <div class="col-lg-6 pos_relative no_padding">
                                                        <a id="open_select_folder">@lang('gallery::backend.model.edit.move')</a> <span ><i class="icon-icon-peq-pasta icon15 margem_icon"></i><span id="dest_f" ></span></span> <input id="dest_f_inp" value="{{$pasta->id}}" type="hidden" name="folder_id">
                                                        <div class="listagem_pastas">
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="col-lg-12 no_padding">
                                                                        <div class="row-eq-height-center">
                                                                            <div class="col-lg-2 back_destiny"><i class="icon-voltar"></i></div>
                                                                            <div class="col-lg-8 pasta_atual">@lang('gallery::backend.model.edit.gallery')</div>
                                                                            <div class="col-lg-10 nova_pasta_escond">


                                                                                <div class="input-group">
                                                                                    <span  class="input-group-addon btn_cancel_new_folder"><i class="icon-voltar"></i></span>
                                                                                    <input  type="text" class="form-control input_pasta" name="title" value="nova pasta">
                                                                                    <input id="pasta_atual_pai"  type="hidden"  name="parent_folder_id" value="">
                                                                                    <a  class="input-group-addon btn_save_new_folder" data-href="{{route('gallery::backend::save_folder_upload')}}"><i class="icon-permissoes icon15"></i></a>
                                                                                </div>



                                                                            </div>
                                                                            <div class="col-lg-2 fecha_list_pastas"><i class="icon-opcoes-eliminar"></i></div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 sec_pastas_centro">
                                                                        <div class="new_folder_selected"></div>
                                                                        <div class="row">
                                                                            <div class="col-lg-12 no_padding">
                                                                                <ul id="selectable" class="destinys">

                                                                                </ul>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-lg-12 foot_destiny no_padding">
                                                                        <div class="container-fluid">
                                                                            <div class="row row-eq-height-center">
                                                                                <div class="col-lg-8 no_padding">
                                                                                    <a class="btn-yellow btn_save_folder">@lang('gallery::backend.model.edit.save_here')</a>
                                                                                    <div class="new_folder_selected"></div>
                                                                                </div>
                                                                                <div class="col-lg-4">
                                                                                    <div class="new_folder_selected"></div>
                                                                                    <i class="icon-criar-pasta icon15 new_folder_here" ></i>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-right">
                                                        @if($item->type!='link')<div class="form-group fom-style-hidden">
                                                            <a href="{{route('gallery::backend::download_item_gallery',array($item->identifier_token))}}" class="btn btn-default  btn_upload_files btn-cancelar"> <i class="icon-opcoes-download pading_right"></i>Download {{$item->n_of_downloads}}x</a>
                                                        </div>
                                                            @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-lg-12">
                                            <div class="divider_line"></div>

                                            <div class="container-fluid margem_20">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <label for="exampleInputEmail1">Tags</label>

                                                            <input id="magicsuggest-faq" name="tags">

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>




                                        <div class="col-lg-12">
                                            <div class="divider_line"></div>

                                            <div class="container-fluid margem_20">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group fom-style-hidden">
                                                            <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.added_in')</label>
                                                            <p>{{$item->created_at}}</p>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        @if($item->type!='link')
                                                            <div class="form-group fom-style-hidden">
                                                            <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.size')</label>
                                                            <p>{{$item->getFileSize()}}</p>
                                                            </div>
                                                            @endif
                                                    </div>
                                                </div>
                                            </div>

                                        </div>






                                    </div>

                                </div>
                            </div>
                        </div>



                    </div>

                    <div class="col-lg-12 margem_20">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8 ">

                                    <button type="submit" class="btn btn-default btn-yellow">@lang('gallery::backend.model.edit.save')</button>
                                    @if($item->type== 'imagem')<a href="{{route('gallery::backend::edit_image_asset',array($item->id))}}" class="btn btn-default btn-cancelar btn_upload_files">@lang('gallery::backend.model.edit.edit_picture')</a>@endif

                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4 text-right">

                                    <button class="btn btn-default btn-cancelar btn_upload_files">@lang('gallery::backend.model.edit.delete')</button>

                                </div>

                            </div>


                        </div>
                    </div>





                </div>
            </form>
        </div>




    </div>
@endsection






