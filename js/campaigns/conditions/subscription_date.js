/**
 * Created by User on 09-03-2017.
 */

//-----------------------------------------------------------setting the names for atributes
var id=0;
var ultimo= $('.item_for_condition[data-id=""]');
ultimo.attr('data-id',id);

if($('.item_for_condition').length > 1){
    var ordenados=$('.item_for_condition').sort(function (a, b) {

        var contentA =parseInt( $(a).attr('data-id'));
        var contentB =parseInt( $(b).attr('data-id'));
        return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
    });
    var prev=ordenados.last();

    id=parseInt(prev.attr('data-id'))+1;
    ultimo.attr('data-id',id);
}

var block_id=ultimo.parent().parent().attr('id');

ultimo.find('.option_subscription_date').attr('name','item['+block_id+'][daterange]['+id+'][option][]');
ultimo.find('.range_date').attr('name','item['+block_id+'][daterange]['+id+'][term][]');
ultimo.find('.filter_field_id').attr('name','item['+block_id+'][daterange]['+id+'][filter_field_id][]');


//----------------------------------------------------------end setting the names for atributes

$(document).ready(function () {

    $('.option_subscription_date').change(function () {

        $(this).parent().next().find('.range_date').val('');

        //se o valor for entre
        if($(this).val()=='7'){

            $(this).parent().next().find('.range_date').daterangepicker({
                "timePicker": true,
                "locale": {
                    "format": 'DD/MM/YYYY'
                },
            });

        }else{
            $(this).parent().next().find('.range_date').daterangepicker({
                singleDatePicker: true,
                "locale": {
                    "format": 'DD/MM/YYYY'
                }
            });
        }
    });

    $('.range_date').daterangepicker({
        singleDatePicker: true,
        "locale": {
            "format": 'DD/MM/YYYY'
        },
    }, function(start, end, label) {



    });
});


//# sourceMappingURL=subscription_date.js.map
