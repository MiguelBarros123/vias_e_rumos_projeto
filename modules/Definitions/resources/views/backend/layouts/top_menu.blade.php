<div class="row tabs-back top_menu_page">
    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">
        <ul class="list-inline fonte_12_lato">

            <li class="@activePageTopMenu('definitionsrent::backend::rent_definitions.edit')">
                <a class="icon_voltar" href="{{route('definitionsrent::backend::rent_definitions.edit')}}">@lang('definitionsrent::backend.menu.campaigns')</a>
            </li>

        </ul>
    </div>
</div>