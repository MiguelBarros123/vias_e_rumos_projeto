<?php

return [
    // The label is used to represent the module's name
    'label' => 'permissions::backend.module-label',

    // Permissions are organized by groups
    // The level 1 key denotes the group name (label)
    // The level 1 value is an associative array
    // The level 2 key denotes the permission name (label)
    // The level 2 value is an array with the routes that requires the permission
    'permissions' => [
        'permissions::backend.permissions.see_users' => [
            'permissions::backend.permissions.see_users' =>
                [
                    'permissions::backend::model.index',
                    'permissions::backend::model.show',
                    'permissions::backend::model.users_ajax',
                    'permissions::backend::model.change_user_state',
                ],
        ],
        'permissions::backend.permissions.create_users' => [
            'permissions::backend.permissions.create_users' =>
                [
                    'permissions::backend::model.create',
                    'permissions::backend::model.store',
                    'permissions::backend::model.edit',
                    'permissions::backend::model.update',
                    'permissions::backend::group.index_change_user_group',
                    'permissions::backend::model.recoverUserPassword',
                    'permissions::backend::group.update_users_group',
                    'permissions::backend::model.registerConfirm'
                ],


        ],

        'permissions::backend.permissions.delete_users' => [
            'permissions::backend.permissions.delete_users' =>
                [
                    'permissions::backend::model.destroy',
                    'permissions::backend::users.delete_multiple'

                ],


        ],

        'permissions::backend.permissions.see_archived_users' => [
            'permissions::backend.permissions.see_archived_users' =>
                [
                    'permissions::backend::archived_users_index',
                    'permissions::backend::model.show_archived',
                    'permissions::backend::users_archived_ajax',

                ],
        ],

        'permissions::backend.permissions.restore_archived_users' => [
            'permissions::backend.permissions.restore_archived_users' =>
                [
                    'permissions::backend::restore_archived_users',

                ],


        ],




        'permissions::backend.permissions.see_groups' => [
            'permissions::backend.permissions.see_groups' =>
                [
                    'permissions::backend::group.index',
                    'permissions::backend::grupos_ajax',
                    'permissions::backend::group.show',
                    'permissions::backend::users_grupo_ajax',
                    'permissions::backend::users_all_groups_ajax',

                ],

        ],
        'permissions::backend.permissions.create_groups' => [

            'permissions::backend.permissions.create_groups' =>
                [
                    'permissions::backend::group.create',
                    'permissions::backend::group.edit',
                    'permissions::backend::group.store',
                    'permissions::backend::group.update',
                    'permissions::backend::users_grupo_ajax',
                    'permissions::backend::edit_permissions',
                    'permissions::backend::group_change_users_group_view',
                    'permissions::backend::update_users_group_view',
                    'permissions::backend::add_user_to_group',
                    'permissions::backend::add_new_user_to_group',
                    'permissions::backend::group_not_users_ajax',
                    'permissions::backend::update_new_users_for_group',
                    'permissions::backend::update_permissions',
                    'permissions::backend::group.index_change_user_group',
                     'permissions::backend::group.update_users_group'



                ],

        ],


        'permissions::backend.permissions.delete_groups' => [

            'permissions::backend.permissions.delete_groups' =>
                [
                    'permissions::backend::group.destroy',
                    'permissions::backend::group.delete_multiple'

                ],
        ]

],

    // Menu items denote the menus that this module renders
    'menu-items' => [
    [
        // Label for the menu item (mandatory)
        'label' => 'permissions::backend.menu.breadcrumb.permissions',
        // Route for the action (mandatory if the menu has no childs)
        'route' => 'permissions::backend::model.index',
        // Css class can be used to render the icon
        'css-class' => 'icon-icon-lateral-gestao-permissoes icon15menu',
        // Unique html id for the item
        'css-id' => null,
        // list of submenus
        'sub-menus' => [],
    ],
    /*[
        'label' => 'permissions::menu.model.create',
        'sub-menus' => [
            [
                'label' => 'permissions::menu.model.create-empty',
                'route' => 'permissions::backend::model.create',
            ],
            [
                'label' => 'permissions::menu.model.create-lorem',
                'route' => 'permissions::backend::model.create',
            ],
        ],
    ],*/
],
    // List of required providers for the module to work
    'providers' => [
    Brainy\Permissions\Providers\PermissionsServiceProvider::class,
],

    // List of facades used by the module
    'aliases' => [
    // 'Recaptcha' => Greggilbert\Recaptcha\Facades\Recaptcha::class,
],

    // List of route middleware to register
    'middleware' => [
    //'acl' => \Brainy\Permissions\Middleware\SomeMiddleware::class,
],

    'commands' => [
    // \Brainy\Permissions\Commands\SomeCommand::class,
],

];
