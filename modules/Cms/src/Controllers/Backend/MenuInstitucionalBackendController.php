<?php

namespace Brainy\Cms\Controllers\Backend;

use Brainy\Framework\Controllers\BackendController;
use Brainy\Cms\Models\MenuInstitucional;


class MenuInstitucionalBackendController extends BackendController

{

    /**

    * Display a listing of the resource.

    *

    * @return Response

    */

    public function index()
    {   
        $menu_institucional = MenuInstitucional::first();

        if(!$menu_institucional){
            $menu_institucional = null;
        }

        return view('cms::backend.menu_institucional.index', compact('menu_institucional'));
    }



    /**

    * Store a newly created resource in storage.

    *

    * @param Request 

    * @return Response

    */

    public function store()

    {
       $menu_institucional =  MenuInstitucional::create([
            'description' => request('description')
        ]);

        return redirect()->route('cms::backend::backend.cms.menu_institucional.index')->with('menu_institucional')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }

    public function update()
    {
       $menu_institucional =  MenuInstitucional::first();
       $menu_institucional->update([
            'description' => request('description')
        ]);

        return redirect()->route('cms::backend::backend.cms.menu_institucional.index')->with('menu_institucional')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }

}

