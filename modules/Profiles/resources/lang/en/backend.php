<?php

return [
    'model' => [
        'index' => [
            //@lang('profiles::backend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view backend page',
        ],
    ],
    'messages' => [
        //@lang('profiles::backend.messages.notifications.new-task')
        'notifications' => [
            'new-task' => 'You have a new pending task',
        ],
        'success' => [
            'create_profile' => 'Perfil criado com sucesso!',
            'update_profile' => 'Perfil editado com sucesso!',
            'create_group' => 'Grupo criado com sucesso!',
            'update_group' => 'Grupo atualizado com sucesso!',
            'delete_groups' => 'Grupo(s) apagado(s) com sucesso.',
        ],
    ],
];
