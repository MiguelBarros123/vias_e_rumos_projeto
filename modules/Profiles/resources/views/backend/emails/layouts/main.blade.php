<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
</head>
<style>
    body{  font-family: Titillium Web, Helvetica Neue,Helvetica,Arial,sans-serif!important;  font-size: 12pt !important;  }
    hr{  border:none;  border-bottom:1px solid #6D6E71;  }
    .intoPInline p{  display: inline-block;  }
    p{  margin: 0!important; color: #000000!important;  }
    .menu_topo_email{  text-align: center; width: 100%;  }
    .div_final span{  color: #8c8e91!important;  }
    .div_final {  text-align: center;  }

    .email-field span { font-weight: 600 }

    @media only screen and (max-device-width: 480px) {
        .menu_topo_email {width: 480px !important; display: block!important;}
    }
</style>
<body style="font-size: 12pt !important; ">
</br>
@yield('mail')
</body>
</html>