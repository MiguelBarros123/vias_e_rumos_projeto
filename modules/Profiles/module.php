<?php

return [
    // The label is used to represent the module's name
    'label' => 'profiles::backend.module-label',

    // Permissions are organized by groups
    // The level 1 key denotes the group name (label)
    // The level 1 value is an associative array
    // The level 2 key denotes the permission name (label)
    // The level 2 value is an array with the routes that requires the permission
    'permissions' => [
        'profiles::backend.permission.profiles.see' => [
            'profiles::backend.permission.profiles.see' => [
                'profiles::backend::profiles.index',
                'profiles::backend::clients.index_ajax',
                'profiles::backend::companies.index',
                'profiles::backend::companies.index_ajax',
                'profiles::backend::profiles.view',
                'profiles::backend::profiles.ajax.no.company',
                'profiles::backend::profiles.ajax.profiles',
                'profiles::backend::profiles.group.index',
                'profiles::backend::profiles.group.index_ajax',
                'profiles::backend::profiles.group.view',
                'profiles::backend::profiles.ajax.no.group',
                'profiles::backend::profiles.get_group',
                'profiles::backend::profiles.custom_fields.index_custom_fields_ajax',
            ],


        ],
        'profiles::backend.permission.profiles.create' => [
            'profiles::backend.permission.profiles.create' => [
                'profiles::backend::profiles.create',
                'profiles::backend::profiles.edit',
                'profiles::backend::profiles.store',
                'profiles::backend::profiles.update',
                'profiles::backend::profiles.group.create',
                'profiles::backend::profiles.group.store',
                'profiles::backend::profiles.group.edit',
                'profiles::backend::profiles.group.update',
                'profiles::backend::profiles.group.add_new_profile',
            ],


        ],
        'profiles::backend.permission.profiles.delete' => [
            'profiles::backend.permission.profiles.delete' => [
                'profiles::backend::profiles.delete_multiple_clients',
                'profiles::backend::profiles.delete_multiple_companies',
                'profiles::backend::profiles.delete_multiple_groups',
            ],


        ],

        'profiles::backend.permission.personalized_fields.see' => [
            'profiles::backend.permission.personalized_fields.see' => [
                'profiles::backend::backend.profiles.custom_fields.index',
                'profiles::backend::profiles.custom_fields.index_ajax',
                'profiles::backend::profiles.custom_fields.index_custom_fields_ajax',

            ],


        ],
        'profiles::backend.permission.personalized_fields.create' => [
            'profiles::backend.permission.personalized_fields.create' => [
                'profiles::backend::backend.profiles.custom_fields.create',
                'profiles::backend::backend.profiles.custom_fields.edit',
                'profiles::backend::backend.profiles.custom_fields.store',
                'profiles::backend::backend.profiles.custom_fields.update',



            ],


        ],
        'profiles::backend.permission.personalized_fields.delete' => [
            'profiles::backend.permission.personalized_fields.delete' => [
                'profiles::backend::backend.profiles.custom_fields.delete',
            ],


        ],
    ],

    // Menu items denote the menus that this module renders
    'menu-items' => [
        [
            // Label for the menu item (mandatory)
            'label' => 'profiles::backend.menu.profiles',
            // Route for the action (mandatory if the menu has no childs)
            'route' => 'profiles::backend::profiles.index',
            // Css class can be used to render the icon
            'css-class' => 'icon-perfis-teste-1 icon15menu',
            // Unique html id for the item
            'css-id' => null,
            // list of submenus
            'sub-menus' => [],
        ]
    ],
    // List of required providers for the module to work
    'providers' => [
        Brainy\Profiles\Providers\ProfilesServiceProvider::class,
    ],

    // List of facades used by the module
    'aliases' => [
        // 'Recaptcha' => Greggilbert\Recaptcha\Facades\Recaptcha::class,
    ],

    // List of route middleware to register
    'middleware' => [
        //'acl' => \Brainy\Profiles\Middleware\SomeMiddleware::class,
    ],

    'commands' => [
        // \Brainy\Profiles\Commands\SomeCommand::class,
    ],

];
