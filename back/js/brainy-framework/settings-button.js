(function() {
    'use strict';

    $('.btn-settings').click(function(){
        var $btn = $(this);
        var group = $btn.data('btn-group');
        var state = $btn.data('btn-state');
        var value = $btn.data('btn-value');

        $.get('/framework/update-state', {'state': state}).success(function() {
            $('[data-btn-group="' + group + '"]').removeClass('active');
            $btn.addClass('active');
            var event = jQuery.Event('settings.' + group);
            event.value = value;
            event.key = group;
            $btn.trigger(event);
        });
    });
})();
