<?php

use Illuminate\Database\Seeder;

use Laracasts\TestDummy\Factory as TestDummy;

class SegmentTableSeeder extends Seeder {

    public function run()
    {
        $segment = \Brainy\Rent\Models\Essence::create([
            'order'=> 1,
            'image'=>'598055079d4947.77988428.jpeg',
            'pt' => ['name' => 'Automáticos'],
            'en' => ['name' => 'Automáticos'],
            'fr' => ['name' => 'Automáticos']
        ]);
        $segment = \Brainy\Rent\Models\Essence::create([
            'order'=> 2,
            'image'=>'5980561dd0fd97.74474297.jpeg',
            'pt' => ['name' => 'Citadinos'],
            'en' => ['name' => 'Citadinos'],
            'fr' => ['name' => 'Citadinos']
        ]);
    }

}