<div class="row">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-titulo">
                <span class="titulo">
                    <i class="icon-alterar-permissoes-1 "></i>
                    @lang('cms::backend.model.common.slideshow')
                </span>
            </div>
            <div class="panel-body no_padding">
                <input class="search search_icon" type="text" id="searchbox_section_{{ $p_section->id }}" placeholder="@lang('cms::backend.model.common.search')">
                <a class="add_btn" href="{{route('cms::backend::resource.create',[ $p_section->id ])}}">
                    <i class="icon-adicionar icon10"></i>
                    @lang('cms::backend.model.common.new')
                </a>
                <table id="resources-table_{{ $p_section->id }}" class="table default-table resources-table_{{ $p_section->id }}">
                    <thead>
                        <tr>
                            <th></th>
                            <th>@lang('cms::backend.model.common.photo')</th>
                            <th>@lang('cms::backend.model.common.identifier')</th>
                            <th class="multiple-options">
                                @include('layouts.backend.partials.delete_datatables',['id'=>'resources-table_'.$p_section->id,'route'=>'cms::backend::pages.delete_multiple_resources'])
                            </th>
                            <th>
                                <label class="checkbox-default">
                                    <input type='checkbox' id="sel_all_perm_resources-table_{{ $p_section->id }}">
                                    <span></span>
                                </label>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
