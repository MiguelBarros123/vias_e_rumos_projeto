<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 17-05-2016
 * Time: 15:57
 */

namespace Brainy\Gallery\Requests;
use App\Http\Requests\Request;

class UpdateMediaItemRequest extends Request{


    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'titulo_pt'=>'required',
            'titulo_en'=>'required',
            'titulo_fr'=>'required',
            'descricao_pt'=>'required',
            'descricao_en'=>'required',
            'descricao_fr'=>'required',
            'link'=>'sometimes|required'
        ];
    }
}