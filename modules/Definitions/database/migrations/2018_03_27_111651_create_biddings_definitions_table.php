<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBiddingsDefinitionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biddings_definitions', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('min_bidding')->nullable();
            $table->unsignedInteger('max_bidding')->nullable();
            $table->unsignedInteger('next_bidding')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('biddings_definitions');
    }
}
