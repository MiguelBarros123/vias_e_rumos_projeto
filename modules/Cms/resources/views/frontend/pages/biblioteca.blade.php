@extends('layouts.frontend.app')

@section('title','Vias e Rumos - Histórico')
@section('content')
<div class="biblio_m">
@include('cms::frontend.pages.layouts.biblio-menu')
</div>
<div class="vendas-prods history">
    <div class="top-bar hidden-sm hidden-xs">
        <div class="row prod-row">
            <div class="col-lg-9 col-md-9">
                <div class="row">
                    <div class="col-lg-2 col-md-2 text-center text-area-wrap">
                        data
                    </div>
                    <div class="col-lg-3 col-md-3 text-center text-area-wrap">
                        designação
                    </div>
                    <div class="col-lg-3 col-md-3 text-center text-area-wrap">
                        local
                    </div>
                    <div class="col-lg-4 col-md-4 text-center text-area-wrap">
                        descrição
                    </div>
                </div>
            </div>  
        
        </div>
    </div>
    @if(count($auctions) == 0)
    <div class="no-data-here">
        Não existe histórico de leilões.
    </div>
    @else
        @foreach($auctions as $auction)
        <div class="each_prod">
            <div class="row prod-row">
                <div class="top-prod-list">
                    <div class="prod-title">
                        <h3>{{ \Brainy\DefinitionsRent\Models\Category::where('id', $auction->category_id)->first()->name}}</h3>
                    </div>
                    
                </div>

                <div class="displ-flex">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 flex-col">
                        <div class="row hidden-md hidden-lg">
                            <div class="col-xs-12 col-sm-12 text-area-wrap img-wrap">
                                @if($auction->auctions_resources()->first())
                                <div class="cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image',[$auction->auctions_resources()->first()->identifier_token])  }}');">
                                    <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                                        <div class="more-prod-bg">

                                        </div>
                                        <div class="more-prod-text">
                                            Saber mais
                                        </div>
                                    </a>
                                </div>
                                @else
                                    <div class="cover prod-img-list"  style="background-image: url('{{asset('front/imgs/lotes/placeholder.jpg')}}');">
                                    <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                                        <div class="more-prod-bg">

                                        </div>
                                        <div class="more-prod-text">
                                            Saber mais
                                        </div>
                                    </a>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 text-center text-area-wrap">
                                <div class="text-area">
                                    <div class="text-wrap">
                                        <div class="text-prod">{{\Carbon\Carbon::parse($auction->date)->format('d/m/Y')}}</div>
                                        <div class="text-prod">{{str_replace(':','h',\Carbon\Carbon::parse($auction->date)->format('H:i'))}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center text-area-wrap">
                                <div class="text-area">
                                    <div class="text-wrap">
                                        <div class="text-prod">{{$auction->executed}}</div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-center text-area-wrap">
                                <div class="text-area">
                                    <div class="text-wrap">
                                        <div class="text-prod">{{$auction->address}} {{$auction->county}}, {{$auction->district}}</div>
                                    </div>
                                    
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 text-center text-area-wrap">
                                <div class="text-area">
                                    <div class="text-wrap">

                                        <div class="text-prod text-prod_ text-left">
                                            @php
                                                $no_tags = strip_tags($auction->comments)
                                            @endphp
                                            @if(strlen($no_tags)>100)
                                            
                                                {{ utf8_encode(mb_substr(html_entity_decode($no_tags, null, 'ISO-8859-1'),0,100))}}...
                                            @else
                                                {{ $no_tags }}
                                            @endif

                                        </div>

                                     
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    
                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 text-area-wrap img-wrap hidden-xs hidden-sm">
                        @if($auction->auctions_resources()->first())
                            <div class=" cover prod-img-list"  style="background-image: url('{{ route('gallery::frontend::storage_image',[$auction->auctions_resources()->first()->identifier_token])  }}');">
                                <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                                <div class="more-prod-bg">

                                </div>
                                <div class="more-prod-text">
                                    Saber mais
                                </div>
                            </a>
                            </div>
                        @else
                            <div class=" cover prod-img-list"  style="background-image: url('{{asset('front/imgs/lotes/placeholder.jpg')}}');">
                                <a href="{{ route('cms::frontend::model.leilao', [$auction->id])}}">
                                <div class="more-prod-bg">

                                </div>
                                <div class="more-prod-text">
                                    Saber mais
                                </div>
                            </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    @endif
</div>
@endsection
