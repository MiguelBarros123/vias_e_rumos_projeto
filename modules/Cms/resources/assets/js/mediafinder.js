/**
 * Created by User on 22-04-2016.
 *
 * Description: load resources for gallery
 */
(function ($) { "use strict";
    var MediaFinder = function (element, options) {
        this.$el = $(element)
        this.options = options || {}
        this.$el.empty()
        this.init()

    }



    var req;

    MediaFinder.prototype.init = function() {

        var elemento=this.$el;
        var modo_lista=this.options.modo_lista;
        var arquivado=this.options.arquivado;
        var mediafinder=this;
        var token=this.options.token_laravel;
        var caminho_muda_items =this.options.caminho_muda_items;
        var items_atuais=this.options.items;

        var filtro=this.options.onfiltertype;
        var dados=[];
        if(filtro!= null){
            dados.push({'filtro':filtro,'items':this.options.filterObjects});

        }

        $('#loading_folders').fadeIn();

        if(req){
            req.abort();
        }

        req=$.ajax({
            type: "get",
            url: this.options.caminho,
            data:{d:dados},
            headers: {
                'X-CSRF-TOKEN': this.options.token_laravel
            },
            success: function (data) {


                if(data['folders'][0].length == 0 && data['files'][0].length == 0){
                    $('#empty_folder_gallery').fadeIn();
                    $('#loading_folders').fadeOut();
                }else{
                    items_atuais=data;
                    $('#empty_folder_gallery').hide();
                    $('#loading_folders').fadeOut();
                    if(modo_lista==1){

                        mediafinder.adiciona_lista(data,elemento);
                    }else{
                        mediafinder.adiciona_grelha(data,elemento);
                    }
                }




                if(arquivado == 0){
                    $('.item_galery').dblclick(function() {

                        window.location =$(this).attr('data-href');
                        return false;

                    });
                }






                //para arrastar items e mudar a sua localização

                var selected = $([]), offset = {top:0, left:0};

                $( "#selectable_cont .item_galery" ).draggable({
                    opacity: 0.7, helper: "clone",
                    start: function(ev, ui) {
                        if (!$(this).hasClass("ui-selected")){
                            selected = $([]);
                            $("#selectable_cont .item_galery").removeClass("ui-selected");
                            $(this).addClass("ui-selected");
                        }
                    },
                    drag: function(ev, ui) {
                        /*$(".tooltip").show().text($(".ui-selected, .ui-draggable-dragging").length);*/
                    }
                });

                $( "#selectable_cont" ).selectable({
                    filter: ".item_galery",
                    cancel: "a"
                });

                var prev = -1;
                $( "#selectable_cont .item_galery").click(function(e){

                    if (e.ctrlKey){
                        e.metaKey = e.ctrlKey;
                    }


                    if (e.metaKey == false) {

                        var curr = $(this).parent().parent().index();
                        console.log(curr);

                        if(e.shiftKey && prev > -1){
                            $('.modo').slice(Math.min(prev, curr), 1 + Math.max(prev, curr)).children().children().addClass("ui-selecting");
                            prev = -1; // and reset prev

                        }else {
                            prev = curr;
                            $("#selectable_cont .item_galery").removeClass("ui-selected");
                            $(this).addClass("ui-selecting");
                        }
                    }
                    else {
                        prev = curr;
                        if ($(this).hasClass("ui-selected")) {
                            $(this).removeClass("ui-selected");
                        }
                        else {
                            $(this).addClass("ui-selecting");
                        }


                    }
                    $( "#selectable_cont" ).data("ui-selectable")._mouseStop(null);
                });

                $( ".item_galery").contextmenu(function(e){

                    if (!$(this).hasClass("ui-selected")) {
                        $( ".item_galery" ).removeClass("ui-selected");
                        $(this).addClass("ui-selecting");

                    }
                    $( "#selectable_cont" ).data("ui-selectable")._mouseStop(null);
                });

                $( ".pasta_accept" ).droppable({
                    accept: ".ui-selected, .ui-draggable-dragging",
                    activeClass: "ui-state-hover",
                    hoverClass: "ui-state-active selecionado",
                    drop: function( event, ui ) {
                        var $this = $(this);
                        $this.addClass( "ui-state-highlight" );
                        var items={};
                        items.folders=[{}];
                        items.files=[{}];
                        var pasta_final=$(this).attr('data-id');
                        console.log(pasta_final);


                        $(".item_galery.ui-selected").each(function(){

                            if(!$(this).hasClass('ui-draggable-dragging')){
                                if($(this).hasClass('pasta_accept'))
                                {
                                    items.folders.push({'id' : $(this).attr('data-id')});
                                }else{
                                    items.files.push({'id' : $(this).attr('data-id')});
                                }
                            }

                        });

                        $('.menu_options_hidden').hide();
                        $(".ui-selected").parent().parent().remove();


                        $this.removeClass( "ui-state-highlight" );
                        selected = $([]);

                        $.ajax({
                            type: "post",
                            data: {'dados': items, 'pai': pasta_final},
                            url: caminho_muda_items,
                            headers: {
                                'X-CSRF-TOKEN': token
                            },
                            success: function (data) {
                                $('.location_message').fadeIn();
                            },
                            error: function (data) {
                                $('.location_message2').fadeIn();
                                setTimeout(function () {
                                    location.reload();
                                },2000);

                            }
                        });



                    }
                });

            }
        });

    }





    MediaFinder.prototype.adiciona_lista = function(data,elemento) {

        $(data['folders'][0]).each(function(indice,elem){

            elemento.append('' +
                '<div class="col-lg-12 modo"> ' +

                '<div class="container-fluid">' +

                '<div class="item_galery folder_ref pasta_accept row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                '<table class="tabela_list" >'+
                '<tr>'+
                '<td class="folder_icon2 col-lg-1">'+
                '<div class="  text-center "><i class="icon-icon-pasta icon30 "></i></div>' +
                '</td>'+

                '<td class="second_column_folder   fonte_12_lato_light_cinza col-lg-2">'+
                '<p class="title_gallery_list"><i class="icon-pasta pading_right"></i>'+elem.title+'</p>' +
                '<p>'+elem.n_folders+' pastas</p>'+
                '<p>'+elem.n_folders+' ficheiros</p>'+
                '</td>'+

                '<td class="second_column_folder col-lg-3">'+
                '<div class="  fonte_12_lato_light_cinza"><p class="title_gallery_list">Último update</p><p>'+elem.last_update+'</p></div>' +
                '</td>'+



                '<td class="col-lg-3">'+
                '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">tipo</p><p>Pasta de ficheiros</p></div>' +
                '</td>'+


                '<td class="col-lg-3">'+
                '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">Tamanho</p><p></p>'+elem.size+'</div>' +
                '</td>'+


                '</tr>'+
                '</table>'+


                '</div>'+

                '</div>'+
                '</div>');
        });

        $(data['files'][0]).each(function(indice,elem){


            switch(elem.type){

                case 'imagem':

                    elemento.append('<div class="col-lg-12 modo"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'"  data-id="'+elem.id+'">' +

                        '<table class="tabela_list" >'+
                        '<tr>'+
                        '<td class="folder_icon2 image_td col-lg-1">'+
                        '<div style="background: url('+elem.imagem+')no-repeat;" class=" image_galery3"></div>' +
                        '</td>'+

                        '<td class="second_column_folder   fonte_12_lato_light_cinza col-lg-2">'+
                        '<p class="title_gallery_list"><i class="icon-pasta pading_right"></i>'+elem.title+'</p>' +
                        '<p>'+elem.resolution+'px</p>'+
                        '</td>'+

                        '<td class="second_column_folder col-lg-3">'+
                        '<div class="  fonte_12_lato_light_cinza"><p class="title_gallery_list">Último update</p><p>'+elem.last_update+'</p></div>' +
                        '</td>'+

                        '<td class="col-lg-3">'+
                        '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">tipo</p><p>'+elem.format+'</p></div>' +
                        '</td>'+
                        '<td class="col-lg-3">'+
                        '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">Tamanho</p><p></p>'+elem.size+'</div>' +
                        '</td>'+

                        '</tr>'+



                        '</table>'+



                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                default :


                    elemento.append('<div class="col-lg-12 modo"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                        '<table class="tabela_list" >'+
                        '<tr>'+
                        '<td class="col-lg-1">'+
                        '<div class="  text-center "><i class="'+elem.imagem+' icon30 "></i></div>' +
                        '</td>'+


                        '<td class="second_column_folder   fonte_12_lato_light_cinza col-lg-2">'+
                        '<p class="title_gallery_list"><i class="icon-pasta pading_right"></i>'+elem.title+'</p>' +
                        '</td>'+

                        '<td class="second_column_folder col-lg-3">'+
                        '<div class="  fonte_12_lato_light_cinza"><p class="title_gallery_list">Último update</p><p>'+elem.last_update+'</p></div>' +
                        '</td>'+

                        '<td class="col-lg-3">'+
                        '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">tipo</p><p>'+elem.format+'</p></div>' +
                        '</td>'+
                        '<td class="col-lg-3">'+
                        '<div class=" fonte_12_lato_light_cinza"><p class="title_gallery_list">Tamanho</p><p></p>'+elem.size+'</div>' +
                        '</td>'+

                        '</tr>'+



                        '</table>'+

                        '</div>'+

                        '</div>'+
                        '</div>');

                    break;

            }


        });


    }

    MediaFinder.prototype.adiciona_grelha = function(data,elemento) {
        $(data['folders'][0]).each(function(indice,elem){

            elemento.append('' +
                '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +



                '<div class="container-fluid">' +




                '<div class="item_galery folder_ref row pasta_accept"  data-id="'+elem.id+'" data-href="'+elem.rota+'">' +


                '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center parent">' +
                '<i class="icon-icon-pasta icon80 child"></i>'+
                '</div>' +

                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"><i class="icon-icon-peq-pasta icon10"></i>'+elem.title+' </div>' +
                '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right fonte_12_lato_light_cinza">'+elem.size+'</div>' +


                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fonte_12_lato_light_cinza no_padding">'+
                '<div class="separacao"></div>' +
                '<div class="text-center foot_gallery_icon fonte_12_lato_light_cinza">'+elem.n_folders+' pastas</div>' +
                '<div class="text-center foot_gallery_icon2 fonte_12_lato_light_cinza">'+elem.n_files+' ficheiros</div>' +
                '</div>' +


                '</div>'+

                '</div>'+
                '</div>');
        });

        $(data['files'][0]).each(function(indice,elem){

console.log('type - '+ elem.type);
            switch(elem.type){

                case 'imagem':

                    elemento.append(
                        '' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div style="background: url('+elem.imagem+')no-repeat;" class=" part1 image_galery col-lg-12"></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"><i class="icon-icon-upload-img icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  text-right fonte_12_lato_light_cinza">'+elem.size+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size"><i class="icon-icon-peq-img-size pading_right"></i>'+elem.resolution+' px</div>' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                case 'documento':


                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +



                        '<div class="item_galery  row" data-href="'+elem.rota+'" data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"> <i class="icon-icon-upload-doc icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12   fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.size+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'video':

                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fonte_12_lato first_text_gallery"><i class="icon-icon-peq-video icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12   fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.size+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'audio':

                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"><i class="icon-icon-som icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12   fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.size+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'link':

                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato first_text_gallery"><i class="icon-embed-upload icon10"></i>'+elem.title+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12   fonte_12_lato_light_cinza"><i class="icon-icon-peq-upload icon10"></i>'+elem.last_update+'</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                default :
                    elemento.append('' +
                        '<div class="col-lg-15 col-lg-3 col-md-4 modo grill"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon_document2 fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

            }


        });
    }



    MediaFinder.prototype.adiciona_lista_archived = function(data,elemento) {

        $(data['folders'][0]).each(function(indice,elem){

            elemento.append('' +
                '<div class="col-lg-12 modo"> ' +

                '<div class="container-fluid">' +

                '<div class="item_galery folder_ref pasta_accept row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                '<div class="folder_icon2 col-lg-1 text-center parent"><i class="icon-icon-pasta icon40 child"></i></div>' +

                '<div class="col-lg-2 second_column_folder  fonte_12_lato parent">' +
                '<div class="child">' +
                '<p class="icon-pasta">'+elem.title+'</p>' +
                '<p>'+elem.n_folders+' pastas</p>'+
                '<p>'+elem.n_folders+' ficheiros</p>'+
                '</div>'+
                '</div>' +

                '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>Pasta de ficheiros</p></div>' +

                '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.size+' mb</div>' +
                '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                '<div class="menu_options_hidden">' +
                '<div class="text-right ">' +
                '<div class="dropdown">'+
                ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                '<i class="icon-roda-dentada icon20"></i>'+
                ' </a>'+
                ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                ' </ul>'+
                ' </div>'+
                ' </div>'+
                '</div>' +
                '</div>' +


                '</div>'+

                '</div>'+
                '</div>');
        });

        $(data['files'][0]).each(function(indice,elem){


            switch(elem.tipo){

                case 'imagem':

                    elemento.append('<div class="col-lg-12 modo"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'"  data-id="'+elem.id+'">' +

                        '<div style="background: url('+elem.imagem+')no-repeat;" class=" image_galery3 col-lg-1"></div>' +


                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '<p>'+elem.resolucao+' px</p>'+
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +

                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +


                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                        '' +
                        '<div class="menu_options_hidden">' +
                        '<div class="text-right ">' +
                        '<div class="dropdown">'+
                        ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<i class="icon-roda-dentada icon20"></i>'+
                        ' </a>'+
                        ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                        ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                        ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                        ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                        ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                        ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                        ' </ul>'+
                        ' </div>'+
                        ' </div>'+
                        '</div>' +
                        '' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                case 'documento':



                    elemento.append('<div class="col-lg-12"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                        '<div class="folder_icon2 col-lg-1 text-center parent"><i class="'+elem.imagem+' icon40 child"></i></div>' +


                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +

                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                        '' +
                        '<div class="menu_options_hidden">' +
                        '<div class="text-right ">' +
                        '<div class="dropdown">'+
                        ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<i class="icon-roda-dentada icon20"></i>'+
                        ' </a>'+
                        ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                        ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                        ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                        ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                        ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                        ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                        ' </ul>'+
                        ' </div>'+
                        ' </div>'+
                        '</div>' +
                        '' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                case 'video':

                    elemento.append('<div class="col-lg-12"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                        '<div class="folder_icon2 col-lg-1 text-center parent"><i class="'+elem.imagem+' icon40 child"></i></div>' +



                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +

                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                        '' +
                        '<div class="menu_options_hidden">' +
                        '<div class="text-right ">' +
                        '<div class="dropdown">'+
                        ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<i class="icon-roda-dentada icon20"></i>'+
                        ' </a>'+
                        ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                        ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                        ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                        ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                        ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                        ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                        ' </ul>'+
                        ' </div>'+
                        ' </div>'+
                        '</div>' +
                        '' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'audio':

                    elemento.append('<div class="col-lg-12"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'"  data-id="'+elem.id+'">' +

                        '<div class="folder_icon2 col-lg-1 text-center parent"><i class="'+elem.imagem+' icon40 child"></i></div>' +



                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +

                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery">' +
                        '' +
                        '<div class="menu_options_hidden">' +
                        '<div class="text-right ">' +
                        '<div class="dropdown">'+
                        ' <a class="roda_open_list_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+
                        '<i class="icon-roda-dentada icon20"></i>'+
                        ' </a>'+
                        ' <ul class="dropdown-menu " aria-labelledby="dLabel">'+
                        ' <li><a><i class="icon-opcoes-editar icon15"></i>Editar</a></li>'+
                        ' <li><a><i class="icon-opcoes-eliminar icon15"></i>Eliminar</a></li>'+
                        ' <li><a><i class="icon-opcoes-copiar icon15"></i>Copiar</a></li>'+
                        ' <li><a><i class="icon-opcoes-mudar-pasta icon15"></i>Mover para...</a></li>'+
                        ' <li><a><i class="icon-opcoes-download icon15"></i>Download</a></li>'+
                        ' </ul>'+
                        ' </div>'+
                        ' </div>'+
                        '</div>' +
                        '' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                default :
                    elemento.append('<div class="col-lg-12"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery row row-eq-height-center" data-href="'+elem.rota+'" data-id="'+elem.id+'">' +

                        '<div class="folder_icon2 col-lg-1 text-center parent"><i class="'+elem.imagem+' icon40 child"></i></div>' +




                        '<div class="col-lg-2 second_column_folder  fonte_12_lato parent ">' +
                        '<div class="child">' +
                        '<p>'+elem.titulo+'</p>' +
                        '</div>'+
                        '</div>' +

                        '<div class="col-lg-2  fonte_12_lato_light_cinza"><p>Último update</p><p>'+elem.last_update+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>tipo</p><p>'+elem.format+'</p></div>' +
                        '<div class="col-lg-2 fonte_12_lato_light_cinza"><p>Tamanho</p><p></p>'+elem.tamanho+' mb</div>' +

                        '<div class="col-lg-3 fonte_12_lato_light_cinza last_col_gallery"><i class="icon-roda-dentada icon20"></i></div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

            }


        });


    }

    MediaFinder.prototype.adiciona_grelha_archived = function(data,elemento) {
        $(data['folders'][0]).each(function(indice,elem){

            elemento.append('' +
                '<div class="col-lg-15 col-md-4 modo "> ' +



                '<div class="container-fluid">' +




                '<div class="item_galery folder_ref row ">' +


                '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12  text-center parent">' +
                '<i class="icon-icon-pasta icon80 child"></i>'+
                '</div>' +

                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-peq-pasta fonte_12_lato first_text_gallery">'+elem.title+'</div>' +
                '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 text-right fonte_12_lato_light_cinza">'+elem.size+' mb</div>' +


                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 fonte_12_lato_light_cinza no_padding">'+
                '<div class="separacao"></div>' +
                '<div class="text-center foot_gallery_icon fonte_12_lato_light_cinza">'+elem.n_folders+' pastas</div>' +
                '<div class="text-center foot_gallery_icon2 fonte_12_lato_light_cinza">'+elem.n_folders+' ficheiros</div>' +
                '</div>' +


                '</div>'+

                '</div>'+
                '</div>');
        });

        $(data['files'][0]).each(function(indice,elem){

            console.log('type - '+ elem.tipo);
            switch(elem.tipo){

                case 'imagem':

                    elemento.append(
                        '' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div style="background: url('+elem.imagem+')no-repeat;" class=" part1 image_galery col-lg-12"></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-upload-img fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  text-right fonte_12_lato_light_cinza">'+elem.tamanho+' mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">'+elem.resolucao+' px</div>' +
                        '</div>' +

                        '</div>'+

                        '</div>'+
                        '</div>');




                    break;

                case 'documento':


                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo "> ' +

                        '<div class="container-fluid">' +



                        '<div class="item_galery  row" data-href="'+elem.rota+'" data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-upload-doc fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'video':

                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +


                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-peq-video fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'audio':

                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-som fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                case 'link':

                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row" data-href="'+elem.rota+'"  data-id="'+elem.id+'" data-delete_route="'+elem.rota_eliminar+'">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon-icon-peq-upload fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

                default :
                    elemento.append('' +
                        '<div class="col-lg-15 col-md-4 modo"> ' +

                        '<div class="container-fluid">' +

                        '<div class="item_galery  row">' +

                        '<div class="folder_icon col-lg-12 col-md-12 col-sm-12 col-xs-12   text-center parent"><i class="'+elem.imagem+' icon80 child"></i></div>' +

                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 icon_document2 fonte_12_lato first_text_gallery">'+elem.titulo+'</div>' +
                        '<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12  data_icon fonte_12_lato_light_cinza">'+elem.last_update+'</div>' +
                        '<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12  fonte_12_lato_light_cinza text-right">'+elem.tamanho+'mb</div>' +


                        '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  fonte_12_lato_light_cinza no_padding">'+
                        '<div class="separacao"></div>' +
                        '<div class=" fonte_12_lato_light_cinza foot_gallery_icon4"></div>' +
                        '</div>' +


                        '</div>'+

                        '</div>'+
                        '</div>');
                    break;

            }


        });
    }


    MediaFinder.prototype.onfiltermediaItems = function (format,selected) {




        console.log(format);
        var items_atuais=this.options.items;
        console.log(items_atuais);

        var filtered = $(items_atuais).filter(function( el ) {
            console.log(el);
            return el;
        });

    }

    $.fn.filterMediaFinder = function (format,selected) {


        MediaFinder.prototype.onfiltermediaItems = function (format,selected) {




            console.log(format);
            var items_atuais=this.options.items;
            console.log(items_atuais);

            var filtered = $(items_atuais).filter(function( el ) {
                console.log(el);
                return el;
            });

        }



    }

    // -------------------PLUGIN DEFINITION-----------------------------------
    MediaFinder.DEFAULTS = {
        caminho: null,
        token_laravel: null,
        modo_lista:0,
        arquivado:0,
        caminho_muda_items:null,
        items:null,
        onfiltertype:null,
        filterObjects:null
    }

    $.fn.mediaFinder = function (option) {
        var args = arguments;


        return this.each(function () {
            var $this   = $(this)
            var options =  (!option)? MediaFinder.DEFAULTS: option
            new MediaFinder(this, options)

        })
    }








}(window.jQuery));