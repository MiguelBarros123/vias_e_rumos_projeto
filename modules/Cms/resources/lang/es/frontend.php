<?php

return [
    'model' => [
        'index' => [
            //@lang('cms::frontend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view frontend page',
        ],
        'common' => [
            'name' => 'Nombre',
            'full_name' => 'Nombre completo',
            'last_name' => 'Apodo',
            'phone' => 'Teléfono',
            'email' => 'Correo Electrónico',
            'subject' => 'Mensaje',
            'submit' => 'Enviar',
            'password' => 'Contraseña',
            'forgot_password' => 'Ha olvidado su contraseña?',
            'new_password' => 'Nueva Contraseña',
            'confirm_password' => 'Repetir Contraseña',
            'repeat_password' => 'Repetir Contraseña',
            'enter' => 'Entrar',
            'create' => 'Crear',
            'nif' => 'Nif',
            'address' => 'Vivienda',
            'zipcode' => 'Código Postal',
            'town' => 'Localidad',
            'city' => 'Ciudad',
            'country' => 'País',
            'company' => 'Negocios',
            'change_email' => 'El cambio de correo electrónico',
            'change_password' => 'Cambio de contraseña',
            'save' => 'Guardar',
            'gender' => 'Género',
            'male' => 'Masculino',
            'female' => 'Femenino',
            'same_address' => 'Guardar como dirección de facturación',
            'additional_addresses' => 'Moradas Añadidos',
            'edit_address' => 'Editar dirección',
            'coin' => 'Moneda',
            'language' => 'Idioma',
            'concept' => 'Concepto',
            'social' => 'Social',
            'cookies_info' => 'Utilizamos cookies para ofrecer la mejor experiencia. Si continúa navegando, vamos a suponer que usted acepta recibirlos. si es así
                            No lo hace, se puede limitar la configuración de cookies en su explorador.',
            'cookies_know_more' => 'sepa mas',
            'cookies_ok_btn' => 'ok',
            'no-search-results' => 'No hay productos en esta categoría.',
            'unity' => 'unidad(es)',
            'pair' => 'pareja',
            'guest-user' => 'Seguir como invitado',
            'no_stock' => 'Valores no disponible',
            'check_stock' => 'Los siguientes productos no tienen la cantidad deseada:',
            'check_cart1' => 'Revisar su ',
            'check_cart2' => ' antes de continuar',
            'to_continue' => 'Para realizar su compra sin registrarse siga como invitado.',
            'close' => 'Cerrar',
            'ask' => 'Solicitar',
            'packs' => 'Paquetes',
        ],
        'menu' => [
            'list' => 'Páginas',
            'breadcrumb' => [
                'site' => 'Mi sitio',
                'pages' => 'Páginas',
                'company' => 'Negocios'
            ],
        ],
        'upload' => [
            'title' => 'Subir',
            'dnd' => 'Arrastrar y soltar',
            'drag-or-click-to-upload' => '<u>Arrastre</u> o/s archivo/s  de la caja o <u>clic</u> para cargar.',
            'allowed-files' => [
                'info' => 'Se permite la carga: ',
                'img' => 'Imágenes',
            ],
            'progress' => 'Sube el progreso - ',
            'embed' => 'Embed Enlace',
        ],
    ],
    'menus' => [
        'cart' => [
            'empty' => 'La cesta está vacía.',
            'checkout' => 'Tramitar Pedido',
            'see_cart' => 'Ver cesta',
            'search' => 'Búsqueda',
        ],
        'footer' => [
            'terms-and-conditions' => 'Términos legales',
            'contacts' => 'Contactos',
            'reserved-area' => 'Área reservada',
            'cookies-policy' => 'Política de cookies',
            'privacy-policy' => 'Política de Privacidad',
            'consumption' => 'Litigios de consumo',
            'free-shipping' => 'Envío Gratuito',
        ]
    ],
    'pages' => [
        'home' => [
            'scroll' => 'Scroll Down',
        ],
        'concept' => [
            'our-logo' => 'Identidad Visual',
            'stitches' => 'Puntos y Colores',
            'text-stitches' => 'Los dos puntos representan la dualidad conceptual de la marca, las entrecruzadas identidades espiritual y sexual en nosotros.',
        ],
        'contacts' => [
            'contact-us' => 'Contáctenos',
        ],
        'terms' => [
            'terms-and-conditions' => 'Términos y Condiciones',
        ],
        'login' => [
            'login' => 'Iniciar sesión',
            'register' => 'Registro',
            'logout' => 'Cerrar sesión',
            'new-client' => 'Nuevo usuario',
            'new-client-activation' => 'Activación de la cuenta Biblical Lust',
            'ask_password' => 'Solicitar una nueva contraseña',
        ],
        'reserved-area' => [
            'account' => 'Mi cuenta',
            'data' => 'Configuraciones de la cuenta',
            'send-address' => 'Dirección de envío',
            'invoice-address' => 'Dirección biling',
            'address' => 'Dirección de la Empresa',
            'orders' => 'Pedidos',
            'product' => 'Producto',
            'order_number' => 'Número de Predido',
            'description' => 'Descripción',
            'date' => 'Día',
            'state' => 'Estado',
            'quantity' => 'Cantidad',
            'value' => 'Valor',
            'unpaid' => 'No Pagado',
            'processing' => 'En Procesamiento',
            'send' => 'Enviado',
            'unknown' => 'Indefinido',
            'user_data' => 'Datos del cliente',
            'products' => 'Productos',
            'articles' => 'Artículos',
            'no_items' => 'No se encontraron artículos para este orden.',
        ],
        'cookies' => [
            'title_cookies' => 'Política de cookies',
            'title_privacy' => 'Política de privacidad',
            'first_subtitle_cookies' => 'O que são Cookies?',
            'first_text_cookies' => '<p>Os cookies são pequenos ficheiros de texto gravados no seu equipamento ao aceder aos websites. Estes permitem o armazenamento de um conjunto de informações relacionadas com o utilizador, em que umas são usadas para reconhecer o utilizador em futuras visitas ao mesmo endereço web, e outras são usadas para garantir as funcionalidades disponibilizadas e a melhorar o desempenho e a experiência do utilizador.</p>
                                    <p>O tipo de informação que pode ser analisada utilizando cookies é o seguinte: nome de utilizador, endereço de correio eletrónico e palavra-passe, o endereço de protocolo de internet (IP) que é utilizado para ligar á internet, o tipo e a versão de navegador, o sistema operativo e a plataforma utilizada para a ligação, os espaços dos nossos serviços online que tenha visualizado ou pesquisado, os endereços web (URLs) desde os quais ou através dos quais tenha acedido e deixado os nossos serviços online, a data e a hora da referida atividade, entre outros.</p>
                                    <p>Os cookies dos websites poderão ser, a qualquer momento, desactivados ou geridos. Para saber como visite a seção de “Ajuda” do seu navegador de internet.</p>
                                    <p>Os cookies dos websites poderão ser, a qualquer momento, desativados ou geridos. Para saber como visite a secção de “Ajuda” do seu navegador de internet.</p>
                                    <p>Alertamos para o facto de algumas áreas do website poderem não funcionar corretamente após a desativação dos cookies.</p>',
            'second_subtitle_cookies' => 'Qual a finalidade dos cookies?',
            'second_text_cookies' => '<p>A utilização de cookies otimiza a navegação do utilizador, permitindo que os dados armazenados melhorem a sua experiência no website, aumentando também a eficiência de resposta do endereço web. Assim, permitem eliminar algumas ações repetitivas no website, bem como auxiliar na apresentação de mensagens publicitárias contidas no endereço web, para ir, o mais possível, de encontro aos seus interesses.</p>',
            'third_subtitle_cookies' => 'Tipo de cookies de utilizados',
            'third_text_cookies' => '<p>Os cookies são segmentados de acordo com a sua finalidade e data de validade.</p>
                                    <ul>
                                    <li>Cookies de sessão - São temporários, os cookies são gerados e estão disponíveis até encerrar a sessão. Da próxima vez que o utilizador aceder ao seu navegador de internet (browser) os cookies já não estarão armazenados. A informação obtida permite gerir as sessões, identificar problemas e fornecer uma melhor experiência de navegação.</li>
                                    <li>Cookies de confirmação - Este cookie serve apenas para garantir que o utilizador confirma a existência de cookies no site que está a visitar e que pretende continuar a navegar no site. Este cookie será gravado quando o utilizador clicar no botão "OK" na barra inferior de aceitação de cookies.</li>
                                    <li>Cookies de Segurança - Utilizamos cookies de segurança para autenticar utilizadores, evitar a utilização fraudulenta de credenciais de início de sessão e proteger dados de pessoas autorizadas. Estes cookies permitem-nos bloquear muitos tipos de ataque, como tentativas de roubo do conteúdo de formulários que se preenche em páginas Web.</li>
                                    <li>Pode consultar informação útil acerca dos cookies e de como gerir ou desativá-los no seguinte endereço: <a href="http://www.allaboutcookies.org" target="_blank" style="color: #000000!important;">www.allaboutcookies.org</a></li>
                                    </ul>',
        ],
        'litigation' => [
            'title' => 'Resolutión alternativa de litigios de consumo',
            'first_text' => ' ',
            'second_text' => '<div><b>Resolutión de litigios en línea</b></div>
                            <div>Web: <a href="https://webgate.ec.europa.eu/odr/main/index.cfm" target="_blank">https://webgate.ec.europa.eu/odr/main/index.cfm</a></div>',
            'third_text' => ' ',
        ],
        'details' => [
            'order' => 'Encomenda',
            'no_products' => 'No hay productos presentes para este fin'
        ],
        'subscribe' => [
            'add_user' => 'Suscripción de usuario',
        ],
        'unsubscribe' => [
            'remove_user' => 'Eliminación de suscripciones de usuarios',
        ],
    ],

    'hidden' => 'invisível | invisíveis',
    'visible' => 'visível | visíveis',
    'composed' => [
        'index' => [
            'info-hidden' => 'este artículo no es visible!',
        ],
        'common' => [
            'delete' => 'Eliminar',
        ],
    ],
    'messages' => [
        'msg_email_automatico'=>'Bienvenido',
        'msg_email_automatico2'=>'Bienvenido al sitio Biblical Lust.',
        'msg_email_automatico3'=>'Para empezar la sessión, al visitar nuestro sitio, introduzca los siguientes datos:',
        'msg_email_automatico4'=>'En su área privada tendrá accesso a:',
        'msg_email_automatico5'=>'El proceso de finalización del pedido más rápido',
        'msg_email_automatico6'=>'Comprobar el estado de sus pedidos',
        'msg_email_automatico7'=>'Ver historial de pedidos',
        'msg_email_automatico8'=>'Realizar cambios en su información de cuenta',
        'msg_email_automatico9'=>'Cambiar la contraseña',
        'msg_email_automatico10'=>'Guardar múltiples direcciones',
        'msg_email_automatico11'=>'Gracias por elegier Blblical Lust.',
        'msg_email_automatico12'=>'Continua a visitarnos en nuestro sitio web',
        'msg_email_contact' => 'Hola! Hemos recebido tu mesaje. Le contactaremos lo antes posible.',
        'msg_email_contact2' => 'Gracias por tu preferencia!',
        'msg_reset_password' => 'Haga clic en el enlace para restablecer su contraseña:',
        'msg_password_subject' => 'Solicitud de reposición',
        'msg_subscricao_automatico' => 'Te han añadido a nuestra lista de correo y ahora estarán entre los primeros en enterarse de las nuevas llegadas, grandes eventos y ofertas especiales.',
        'msg_cancelar_subscricao_automatico' => 'Su solicitud de baja en el Biblical Lust se llevó a cabo con éxito y su correo electrónico se ha eliminado correctamente y ya no recibir nuestro boletín.',
        'bom_dia' => 'Buen día',
        'boa_tarde' => 'Buenas tardes',
        'boa_noite' => 'Buenas noches',
        'subject' => [
            'msg_email_contacts' => 'Contacto mensaje enviado a través del sitio Biblical Lust',
        ],
        'required' => [
            'field' => 'Es obligatorio indicar un valor para este campo.',
        ],
        'success' => [
            'added' => 'Agregado con éxito artículo.',
            'update_contacts' => 'Cambiado correctamente los datos.',
            'update_pages' => 'Cambiado correctamente los datos',
            'deleted_group' => 'Elemento(s) eliminado(s) con éxito',
            'deleted_representatives' => 'Representante(s) eliminado (s) con éxito',
            'email-send' => 'Gracias por tu mensaje. Le contactaremos lo antes posible.',
            'update_account' => 'Sus datos se ha actualizado correctamente.',
            'address_saved' => 'Su dirección se ha añadido con éxito.',
            'user-created' => 'Usuario creado con éxito.',
            'password-email' => 'Un correo electrónico fue enviado con el fin de proceder a cambiar su contraseña.',
            'delete' => [
                'representative' => 'Representante con el id \': id\' eliminado correctamente.',
            ]
        ],
        'error' => [
            'delete' => [
                'representative' => 'No se pudo eliminar el representante con el id \': id\'.',
            ],
            'email-error' => 'No se pudo enviar tu correo electrónico. Vuelve a intentarlo más tarde.',
            'error_password' => 'La contraseña inicial es incorrecta!',
            'login-error' => 'El correo electrónico o la contraseña no son válidos!',
            'login-email' => 'Este correo electrónico ya está registrado!',
            'no-user-error' => 'El usuario no está registrado!',
            'user-already-exists' => 'El usuario ya está registrado!',
            'password-error' => 'No se pudo enviar un mensaje de confirmación al correo electrónico dado.',
            'page' => [
                'page_not_found' => 'OOOPS! PÁGINA NO ENCONTRADA',
                'return' => 'VOLVER A LA PÁGINA DE INICIO',
            ],
        ],
    ],
];
