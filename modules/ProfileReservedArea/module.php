<?php

return [
    // The label is used to represent the module's name
    'label' => 'profiles::backend.module-label',

    // Permissions are organized by groups
    // The level 1 key denotes the group name (label)
    // The level 1 value is an associative array
    // The level 2 key denotes the permission name (label)
    // The level 2 value is an array with the routes that requires the permission
    'permissions' => [
        'profilereservedarea::backend.permission.profilereservedarea.see' => [
            'profilereservedarea::backend.permission.profilereservedarea.see' => [
                'profilereservedarea::backend::profilereservedarea.index',
                'profilereservedarea::backend::profilereservedarea.index_ajax',
                'profilereservedarea::backend::profilereservedarea.reorder',
                'profilereservedarea::backend::profilereservedarea.create',
                'profilereservedarea::backend::profilereservedarea.edit',
            ],
        ],
        'profilereservedarea::backend.permission.profilereservedarea.create' => [
            'profilereservedarea::backend.permission.profilereservedarea.create' => [
                'profilereservedarea::backend::profilereservedarea.update',
                'profilereservedarea::backend::profilereservedarea.change_profile_state',
            ],
        ],
        'profilereservedarea::backend.permission.profilereservedarea.delete' => [
            'profilereservedarea::backend.permission.profilereservedarea.delete' => [
                'profilereservedarea::backend::profilereservedarea.delete',
            ],
        ],
    ],

    // Menu items denote the menus that this module renders
    'menu-items' => [
        [
            // Label for the menu item (mandatory)
            'label' => 'profilereservedarea::backend.menu.profiles',
            // Route for the action (mandatory if the menu has no childs)
            'route' => 'profilereservedarea::backend::profilereservedarea.index',
            // Css class can be used to render the icon
            'css-class' => 'icon-perfis-teste-1 icon15menu',
            // Unique html id for the item
            'css-id' => null,
            // list of submenus
            'sub-menus' => [],
        ]
    ],
    // List of required providers for the module to work
    'providers' => [
        Brainy\ProfileReservedArea\Providers\ProfileReservedAreaServiceProvider::class,
    ],

    // List of facades used by the module
    'aliases' => [
        // 'Recaptcha' => Greggilbert\Recaptcha\Facades\Recaptcha::class,
    ],

    // List of route middleware to register
    'middleware' => [
        //'acl' => \Brainy\Profiles\Middleware\SomeMiddleware::class,
    ],

    'commands' => [
        // \Brainy\Profiles\Commands\SomeCommand::class,
    ],

];
