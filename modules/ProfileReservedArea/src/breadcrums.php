<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-01-2017
 * Time: 09:46
 */

DaveJamesMiller\Breadcrumbs\Facade::register('auctions_online', function($breadcrumbs)
{
    $breadcrumbs->push('Vendas Online', route('auctionsonline::backend::auctions_online.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('profiles.index', function($breadcrumbs){
    $breadcrumbs->parent('auctions_online');
    $breadcrumbs->push('Perfis', route('profilereservedarea::backend::profilereservedarea.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('profiles.create', function($breadcrumbs){
    $breadcrumbs->parent('profiles.index');
    $breadcrumbs->push('Criar Perfil', route('profilereservedarea::backend::profilereservedarea.create'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('profiles.edit', function($breadcrumbs){
    $breadcrumbs->parent('profiles.index');
    $breadcrumbs->push('Editar Perfil', route('profilereservedarea::backend::profilereservedarea.edit'));
});
