<?php

return [
    'module-label' => 'Definições',
    'menu'=>[
        'definitions'=>'Definições',
        'campaigns'=>'Campanhas',
        'conditions'=>'Condições'
    ],
    'common' => [
        'definitions_rent' => 'Definições de Reservas',
        'name' => 'Nome',
        'price_table' => 'Tabela',
        'group' => 'Grupo',
        'conditions' => 'Condições',
        'definitions' => 'Definições',
        'create_condition' => 'Criar nova condição',
        'groups' => 'Grupos',
        'list_groups' => 'Lista com os grupos associados à condição',
        'select_groups' => 'Selecionar grupos',
        'empty_groups' => 'Sem grupos selecionados',
        'id' => 'Id',
        'search' => 'Pesquisar',
        'save' => 'Guardar',
        'cancel' => 'Cancelar',
        'condition-tables' => 'Tabelas de condições',
    ],
    'model' => [
        'index' => [
            //@lang('definitionsrent::backend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view backend page',
        ],
    ],
    'messages' => [
        //@lang('definitionsrent::backend.messages.notifications.new-task')
        'success' => [
            'global_create' => 'Gravado com sucesso!',
            'global_delete' => 'Eliminado com sucesso!',
            'global_update' => 'Editado com sucesso!',
            'global_duplicate' => 'Copiado com sucesso!',
        ],
        'notifications' => [
            'new-task' => 'You have a new pending task',
        ],
    ],
    'permissions' => [
        'definitions' => [
            'see' => 'Ver Definições',
            'create' => 'Criar/Editar Definições'
        ],
    ],
];
