<?php

namespace Brainy\Cms\Requests;

use App\Http\Requests\Request;

class SendAddressRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'send_name' => 'required',
            'send_address' => 'required',
            'send_nif' => 'required',
            'send_zip_code' => 'required',
            'send_phone' => 'required'
        ];
    }

}