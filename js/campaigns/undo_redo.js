(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
    "use strict";

    /**
     * Created by User on 20-05-2016.
     */

    $(function () {
        var StorageLib = function StorageLib() {
            this.init();
        };

        StorageLib.prototype.init = function () {
            if (typeof Storage == "undefined") {

                console.log('no-support');
            }
        };

        var stackSize = stackSize || 10;

        var stUndo = [];
        var stRedo = [];
        var lastSav = {};

        var L = localStorage;

        $.saveState = function () {
            var mod = {l:1};
            var html = $('#email_container').html();
            var style = $('.email_parent').attr('style') + '&--' +$('#email_container').attr('style');
            var email = JSON.stringify(html);
            // var css = JSON.stringify(css);
            var css = JSON.stringify(style);

            if(lastSav.hasOwnProperty('html') && lastSav.hasOwnProperty('style')){
                if(JSON.stringify(lastSav['html']) == email && JSON.stringify(lastSav['style']) == css) {
                    return;
                }
            }

            // if( (lastSav.hasOwnProperty('html') && JSON.stringify(lastSav['html']) == email) || (lastSav['style'] && JSON.stringify(lastSav['style']) == css) ) return;

            if(!jQuery.isEmptyObject(lastSav)) {
                stUndo.push(btoa(JSON.stringify(lastSav)));
                if(stUndo.length > stackSize) {stUndo.shift(); mod.u = 1;}
            }

            if(stRedo.length > 0) {stRedo.length = 0; mod.r = 1;}

            lastSav['html'] = JSON.parse(email);
            lastSav['style'] = JSON.parse(css);
            syncLS(mod);

        };

        $.undo = function () {
            // this.saveState();
            if(stUndo.length > 0){
                stRedo.push(btoa(JSON.stringify(lastSav)));
                lastSav = jQuery.parseJSON(atob(stUndo.pop()));
                $('#email_container').html(lastSav['html']);
                var styles = lastSav['style'].split('&--');
                $('.email_parent').attr('style', styles[0]);
                $('#email_container').attr('style', styles[1]);
                syncLS();
            }
        };

        $.redo = function () {
            // this.saveState();
            if(stRedo.length > 0){
                stUndo.push(btoa(JSON.stringify(lastSav)));
                lastSav = jQuery.parseJSON(atob(stRedo.pop()));
                $('#email_container').html(lastSav['html']);
                var styles = lastSav['style'].split('&--');
                $('.email_parent').attr('style', styles[0]);
                $('#email_container').attr('style', styles[1]);
                syncLS();
            }
        };

        $.clear = function () {
            return new StorageLib();
        };

        if(L.getItem('lastSav')) {
            lastSav=JSON.parse(L.getItem('lastSav'));
            // extend(html,lastSav);  //restoring the last saved state
            if(L.getItem('stUndo')) stUndo=JSON.parse(L.getItem('stUndo'));  //restoring undo stack
            if(L.getItem('stRedo')) stRedo=JSON.parse(L.getItem('stRedo'));  //restoring redo stack
        }

        // function extend (target, source) {
        //     target = target || {};
        //     if(target.length+"" != "undefined"){ //if it's an array
        //         while(target.length > 0) {  //empty the array
        //             target.pop();
        //         }
        //     }
        //     for (var prop in source) {  //do a deep copy of the object recursively
        //         if (typeof source[prop] === 'object') {
        //             target[prop] = extend(target[prop], source[prop]);
        //         } else {
        //             target[prop] = source[prop];
        //         }
        //     }
        //     return target;
        // }

        function syncLS(what){
            what= what || {u:1,l:1,r:1};  //U=undo, L=lastSav, R=redo
            if(what.u) L.setItem('stUndo', JSON.stringify(stUndo));
            if(what.r) L.setItem('stRedo', JSON.stringify(stRedo));
            if(what.l) L.setItem('lastSav', JSON.stringify(lastSav));
        }

        $.storagelib = function () {
            return new StorageLib();
        };

    });

},{}]},{},[1]);
//# sourceMappingURL=undo_redo.js.map
