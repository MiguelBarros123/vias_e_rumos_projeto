<?php

namespace Brainy\Cms\Requests;

use App\Http\Requests\Request;

class NewUserRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required',
            'password' => 'required|min:8',
            'repeat_password' => 'required|min:8|same:password'
        ];
    }

}