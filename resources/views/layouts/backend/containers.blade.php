<div class="conteudo_central_modulos">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 no_padding">
                <ul class="list-inline menu_modulos fonte_15 texto_preto">
                @foreach(Brainy::containers() as $container)
                    @ifAllowContainer($container)
                    <li class="@activeContainer($container)">
                        <a href="{{route($container->route)}}">@lang($container->label)</a>
                    </li>
                    @endifAllowContainer
                @endforeach
                </ul>
            </div>
        </div>
    </div>
</div>