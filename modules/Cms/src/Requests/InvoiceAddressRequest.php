<?php

namespace Brainy\Cms\Requests;

use App\Http\Requests\Request;

class InvoiceAddressRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'invoice_name' => 'required',
            'invoice_address' => 'required',
            'invoice_nif' => 'required',
            'invoice_zip_code' => 'required',
            'invoice_phone' => 'required'
        ];
    }

}