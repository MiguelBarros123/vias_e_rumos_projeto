<div id="menu-squares" class="hidden-xs hidden-sm">
	<div class="squares-wrap">
		<div class="cover square-el" style="background-image: url('{{ asset('front/imgs/img1.jpg') }}')">
			<a href="{{ route('cms::frontend::model.empresa') }}" class="link_sq">
			</a>
			<div class="overlay-square">
			</div>
			<div class="square-text">
				<div class="sec-title">
					INSTITUCIONAL
				</div>
				<div class="know-more">
					Saber mais
				</div>
			</div>
		</div>
		<div class="cover square-el" style="background-image: url('{{ asset('front/imgs/img2.jpg') }}')">
			<a href="{{ route('cms::frontend::model.vendas') }}" class="link_sq">
			</a>
			<div class="overlay-square">
			</div>
			<div class="square-text">
				<div class="sec-title">
					vendas
				</div>
				<div class="know-more">
					Saber mais
				</div>
			</div>
			
		</div>
		<div class="cover square-el" style="background-image: url('{{ asset('front/imgs/img3.jpg') }}')">
			<a href="{{ route('cms::frontend::model.biblioteca') }}" class="link_sq">
			</a>
			<div class="overlay-square">
			</div>
			<div class="square-text">
				<div class="sec-title">
					biblioteca
				</div>
				<div class="know-more">
					Saber mais
				</div>
			</div>
		</div>
		<div class="cover square-el" style="background-image: url('{{ asset('front/imgs/img4.jpg') }}')">
			<a href="{{ route('cms::frontend::model.contacts')}}" class="link_sq">
			</a>
			<div class="overlay-square">
			</div>
			<div class="square-text">
				<div class="sec-title">
					contactos
				</div>
				<div class="know-more">
					Saber mais
				</div>
			</div>
		</div>
	</div>
</div>
