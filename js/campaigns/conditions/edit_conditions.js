/**
 * Created by User on 15-03-2017.
 */
var email_domain=$('.magicsuggest-email-domain').magicSuggest({
    placeholder:"",
    data: ['hotmail.com', 'gmail.com'],
    allowDuplicates: false,
    valueField: 'name',
    allowFreeEntries:true,
    required: true,
});

email_domain.setSelection(jQuery.parseJSON('{!! $group->profiles->toJson() !!}'));

$('.magicsuggest-tags').magicSuggest({
    placeholder:"",
    method: 'get',
    data: rota_tags,
    allowDuplicates: false,
    valueField: 'name',
    allowFreeEntries:false,
    required: true,
});


$('.magicsuggest-country').magicSuggest({
    placeholder:"",
    method: 'get',
    data: rota_country,
    allowDuplicates: false,
    valueField: 'name',
    allowFreeEntries:false,
    required: true,
});



$('.magicsuggest-emails').magicSuggest({
    placeholder:"",
    method: 'get',
    data: rota_emails,
    allowDuplicates: false,
    valueField: 'name',
    allowFreeEntries:false,
    required: true,
});


$('.magicsuggest-group').magicSuggest({
    placeholder:"",
    method: 'get',
    data: rota_group,
    allowDuplicates: false,
    valueField: 'name',
    allowFreeEntries:false,
    required: true,
});


$('.age_input').change(function () {
    //if value is between
    if($(this).val()=='7'){
        $(this).parent().next().find('.section_others').hide();
        $(this).parent().next().find('.section_between').fadeIn();
    }else{
        $(this).parent().next().find('.section_others').fadeIn();
        $(this).parent().next().find('.section_between').hide();
    }
});


$('.option_subscription_date').change(function () {

    $(this).parent().next().find('.range_date').val('');

    //se o valor for entre
    if($(this).val()=='7'){

        $(this).parent().next().find('.range_date').daterangepicker({
            "timePicker": true,
            "locale": {
                "format": 'DD/MM/YYYY'
            },
        });

    }else{
        $(this).parent().next().find('.range_date').daterangepicker({
            singleDatePicker: true,
            "locale": {
                "format": 'DD/MM/YYYY'
            }
        });
    }
});

$('.range_date').daterangepicker({
    singleDatePicker: true,
    "locale": {
        "format": 'DD/MM/YYYY'
    },
}, function(start, end, label) {



});



$('.magicsuggest-language').magicSuggest({
    placeholder:"",
    data:[{"id":"AF","name":"Afrikanns"},
        {"id":"SQ","name":"Albanian"},
        {"id":"AR","name":"Arabic"},
        {"id":"HY","name":"Armenian"},
        {"id":"EU","name":"Basque"},
        {"id":"BN","name":"Bengali"},
        {"id":"BG","name":"Bulgarian"},
        {"id":"CA","name":"Catalan"},
        {"id":"KM","name":"Cambodian"},
        {"id":"ZH","name":"Chinese (Mandarin)"},
        {"id":"HR","name":"Croation"},
        {"id":"CS","name":"Czech"},
        {"id":"DA","name":"Danish"},
        {"id":"NL","name":"Dutch"},
        {"id":"EN","name":"English"},
        {"id":"ET","name":"Estonian"},
        {"id":"FJ","name":"Fiji"},
        {"id":"FI","name":"Finnish"},
        {"id":"FR","name":"French"},
        {"id":"KA","name":"Georgian"},
        {"id":"DE","name":"German"},
        {"id":"EL","name":"Greek"},
        {"id":"GU","name":"Gujarati"},
        {"id":"HE","name":"Hebrew"},
        {"id":"HI","name":"Hindi"},
        {"id":"HU","name":"Hungarian"},
        {"id":"IS","name":"Icelandic"},
        {"id":"ID","name":"Indonesian"},
        {"id":"GA","name":"Irish"},
        {"id":"IT","name":"Italian"},
        {"id":"JA","name":"Japanese"},
        {"id":"JW","name":"Javanese"},
        {"id":"KO","name":"Korean"},
        {"id":"LA","name":"Latin"},
        {"id":"LV","name":"Latvian"},
        {"id":"LT","name":"Lithuanian"},
        {"id":"MK","name":"Macedonian"},
        {"id":"MS","name":"Malay"},
        {"id":"ML","name":"Malayalam"},
        {"id":"MT","name":"Maltese"},
        {"id":"MI","name":"Maori"},
        {"id":"MR","name":"Marathi"},
        {"id":"MN","name":"Mongolian"},
        {"id":"NE","name":"Nepali"},
        {"id":"NO","name":"Norwegian"},
        {"id":"FA","name":"Persian"},
        {"id":"PL","name":"Polish"},
        {"id":"PT","name":"Portuguese"},
        {"id":"PA","name":"Punjabi"},
        {"id":"QU","name":"Quechua"},
        {"id":"RO","name":"Romanian"},
        {"id":"RU","name":"Russian"},
        {"id":"SM","name":"Samoan"},
        {"id":"SR","name":"Serbian"},
        {"id":"SK","name":"Slovak"},
        {"id":"SL","name":"Slovenian"},
        {"id":"ES","name":"Spanish"},
        {"id":"SW","name":"Swahili"},
        {"id":"SV","name":"Swedish "},
        {"id":"TA","name":"Tamil"},
        {"id":"TT","name":"Tatar"},
        {"id":"TE","name":"Telugu"},
        {"id":"TH","name":"Thai"},
        {"id":"BO","name":"Tibetan"},
        {"id":"TO","name":"Tonga"},
        {"id":"TR","name":"Turkish"},
        {"id":"UK","name":"Ukranian"},
        {"id":"UR","name":"Urdu"},
        {"id":"UZ","name":"Uzbek"},
        {"id":"VI","name":"Vietnamese"},
        {"id":"CY","name":"Welsh"},
        {"id":"XH","name":"Xhosa"},
    ],
    allowDuplicates: false,
    valueField: 'name',
    allowFreeEntries:false,
    required: true,
});
//# sourceMappingURL=edit_conditions.js.map
