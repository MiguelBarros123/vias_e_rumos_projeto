<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class BackofficeLanguage
{
    public function handle($request, Closure $next)
    {

        Session::put('locale', 'pt');

        app()->setLocale(Session::get('locale'));


        return $next($request);
    }
}
