<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Caro <!-- Em Falta Firstname do utilizador e lastname -->,</p>
        <div>
            <p>Infelizmente, a sua licitação de <!-- Valor em falta --> ao lote <!-- valor em falta --> foi superada.</p>
            <p>Caso tenha alguma dúvida, nao hesite em contactar a nossa equipa através do e-mail <a href="mailto:apoio@viaserumos.pt">apoio@viaserumos.pt</a>.</p>
            <p>Atenciosamente,</p>
            <p>A equipa Vias & Rumos</p>
        </div>
    </body>
</html>

