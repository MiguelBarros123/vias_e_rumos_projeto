<?php

Route::group(['namespace' => 'Brainy\Seo\Controllers\Backend', 'middleware' => \App\Http\Middleware\BackofficeLanguage::class], function () {

    Route::get('model', 'SeoBackendController@index')->name('model.index');
    Route::get('favicon', 'SeoBackendController@favicon')->name('model.index_favicon');
    Route::put('save-favicon', 'SeoBackendController@save_favicon')->name('favicon.save_favicon');
    Route::get('get-all-pages-ajax', 'SeoBackendController@allPagesAjax')->name('seo.ajax_all_pages');
    Route::get('get-seo-for-pages-ajax/{id}', 'SeoBackendController@ajax_my_description')->name('seo.ajax_my_description');
    Route::put('store-seo-description/{id}', 'SeoBackendController@UpdateDescription')->name('seo.update_description');

});
