(function () {
	'use strict';

	function CategoriesCreatorController($scope, $http) {


		$scope.getPublicPath = function (url) {
			$scope.publicUrl = url+'/';
		};


		var faqModel = {
			question: {
				translations: {}
			},
			visible: true,
			parent_id: null,
			tags: []
		};

		$scope.faq = $.extend({}, faqModel);

		$scope.option = {
            language: $scope.locale,
            allowedContent: true,
            entities: false
        };

        // Called when the editor is completely ready.
        $scope.onReady = function() {
            //
        };

		$scope.toggleVisibility = function () {
			$scope.faq.visible = !$scope.faq.visible;
		};


		function httpPostSuccess(event) {
			return function (response) {
				$scope.$emit(event, response);
				$scope.dismiss('#modal-categorie');
			};
		}

		function httpFaqCreationError(response) {
			$('#error_tabs1').fadeIn();
		}

		$scope.updateOrNewFaqNode = function () {
			var event = 'edit faq';

			if ($scope.isEmptySelection()) {
				$scope.faq.id = 'new-faq-' + Date.now();
				event = 'new categorie';
			}

			var rota=window.Brainy.module.routes['catalog::backend::brands.save'];

			console.log($scope.publicUrl);

			$http.post($scope.publicUrl+rota, $scope.faq)
				.success(httpPostSuccess(event))
				.error(httpFaqCreationError);
		};

		$(function() {
			$scope.ms = $('#magicsuggest-faq').magicSuggest({
                data: $scope.getTags,
                allowDuplicates: false,
                valueField: 'name'
            });

            $($scope.ms).on('selectionchange', function() {
                var tags = angular.fromJson(this.getSelection());

                for (var i = 0; i < tags.length; ++i) {
                	var tag = tags[i];
                	if (tag.id === undefined) {
                		tag.id = 'new-tag-' + Date.now();
                	}
                }

                $scope.faq.tags = tags;
            });

            $('#modal-categorie').on('hidden.bs.modal', function () {
    			$scope.faq = angular.copy(faqModel);
                $scope.clearSelection();
                $scope.ms.clear();
			});

			$('#modal-categorie').on('shown.bs.modal', function () {

                if (! $scope.isEmptySelection()) {
                	$scope.faq.id = $scope.selectedNode.id;
                    $scope.faq.visible = $scope.selectedNode.visible;
                    $scope.faq.parent_id = $scope.selectedNode.parent_id;


                    for (var i = 0; i < $scope.selectedNode.translations.length; ++i) {
                        var locale = $scope.selectedNode.translations[i].locale;
						$scope.faq.question.translations[locale] = $scope.selectedNode.translations[i].name;

					}

                    $scope.faq.tags = $scope.selectedNode.tags;
                    if ($scope.faq.tags && $scope.faq.tags.length) {
                        $scope.ms.setSelection($scope.faq.tags);
                    }
                    
                } else {
                    $scope.faq = angular.copy(faqModel);
                }
				if($scope.isInCreationMode()==true){
					$scope.faq.parent_id = $scope.selectedNode.id;
					$scope.faq.question.translations={};
					$scope.setInCreationMode()
				}

                if (! $scope.$$phase) {
                    $scope.$apply();
                }
            });
        });
	}

	angular.module('brainy.categories.backend').controller('CategoriesCreatorController', ['$scope', '$http', CategoriesCreatorController]);
})();