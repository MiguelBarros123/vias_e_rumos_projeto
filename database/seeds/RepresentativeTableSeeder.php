<?php

use Illuminate\Database\Seeder;

use Brainy\Cms\Models as Cms;

class RepresentativeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {       
        factory(Cms\Representative::class, 'representatives', 3)->create();
        factory(Cms\RepresentativeContact::class, 'representative_contacts', 2)->create();
        factory(Cms\RepresentativeEmail::class, 'representative_emails', 2)->create();
    }
}
