(function() {
	'use strict';

	function notNullOrUndefined() {
        function isNotNullOrUndefined(property) {
            return function (element) {
                return !angular.isUndefined(element) && element !== null && !angular.isUndefined(element[property]) && element[property] !== null;
            };
        }

        return function (input, property) {
            return input.filter(isNotNullOrUndefined(property));
        };
    }

	angular.module('brainy', []).filter('notNullOrUndefined', [notNullOrUndefined]);
})();