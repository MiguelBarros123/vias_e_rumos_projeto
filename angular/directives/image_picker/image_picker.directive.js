(function() {
    'use strict';

    function isFolder (item) {
        return true;
    }

    function isPicture(item) {
        return true;
    }

    function ImagePickerController($scope, $http, $sce) {
        $scope.rootFolder = $scope.rootFolder || 1;
        $scope.currentFolderId = $scope.rootFolder;

        function isRootFolder (id) {
        	return id == $scope.rootFolder;
        }

        $scope.redraw = function (event) {


            var $selected = $(event.target).parents('div[data-media-item-type^="f"]');

            var $child = $selected.children().first();

            if ($child.hasClass('ui-selected')) {
                $child.removeClass('ui-selected');
            } else {
                $('div.media_item.ui-selected').removeClass('ui-selected');
                $child.addClass('ui-selected');
            }

            var type = $selected.attr('data-media-item-type');
            var id = $selected.attr('data-media-item-id');

            if (type == 'folder') {
                $scope.setCurrentFolder(id);
            } else {

            }

            $scope.renderGallery();
        };


        $scope.navToFolder = function(event) {
            var id = $(event.target).attr('data-folder-id');
            if (! isRootFolder(id)) {
                $scope.currentFolderId = id;
                $scope.buildBreadcrumb(id);
                $scope.renderGallery();
            }
        };

        $scope.setCurrentFolder = function(id) {
            if (! isRootFolder(id)) {
        	   $scope.currentFolderId = id;
                $scope.buildBreadcrumb(id);
            }
        };

        $scope.breadcrumb = [];

        $scope.buildBreadcrumb = function (id) {
            return $http.get(
                '/' + id)
                .then(function(response, textStatus, xhr) {

                    response.data = response.data.filter(function(elem) {
                        return elem.id != $scope.currentFolderId; //&& elem.id >= $scope.rootFolder;
                    });

                    response.data.sort(function(a, b) {
                        return a.id > b.id;
                    });

                    $scope.breadcrumb = response.data;
                }, function(response) {
                    $scope.breadcrumb = [];
                });
        };

        function findFolderById (id) {
            for (var i = 0; i < $scope.galleryItems.length; ++i) {
                if (id == $scope.galleryItems[i].id) {
                    return $scope.galleryItems[i];
                }
            }
            return null;
        }

        $scope.renderGallery = function() {
            $scope.gallery = '<div class="row" style="background-color:#f1f2f2; height: 425px; overflow-y: auto">';

            var folder = findFolderById($scope.currentFolderId);

           if (folder !== null) {
                for (var i = 0; i < folder.media_items.length; ++i) {
                    var element = folder.media_items[i];
                    switch(element.type){

                        case 'imagem':
                            $scope.gallery +=
                            '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery row" style="margin: 10px; padding: 0 10px;">' +
                                    '<div style="background: url(http://localhost/brainy-web/public/backend/gallery/get-storage-image/' + element.identifier_token + ')no-repeat;"' +
                                        'class="part1 image_galery col-lg-12">' +
                                    '</div>' +
                                    '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                        '<i class="icon-icon-upload-img icon10"></i>' + element.title +
                                    '</div>' +
                                    '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                        '<i class="icon-icon-peq-upload icon10">' + element.created_at + '</i>' +
                                    '</div>' +
                                    '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                    '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                        '<div class="separacao"></div>' +
                                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                            '<i class="icon-icon-peq-img-size pading_right"></i>200x300 px' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                        break;
                        case 'documento':
                            $scope.gallery +=
                            '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery row" style="margin: 10px; padding: 0 10px;">' +
                                    '<div class="folder_icon col-lg-12 text-center parent">' +
                                        '<i class="icon-icon-upload-doc icon80 child"></i>' +
                                    '</div>' +
                                    '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                        '<i class="icon-icon-upload-doc icon10"></i>' + element.title +
                                    '</div>' +
                                    '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                        '<i class="icon-icon-peq-upload icon10">' + element.created_at + '</i>' +
                                    '</div>' +
                                    '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                    '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                        '<div class="separacao"></div>' +
                                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                        break;
                        case 'video':
                            $scope.gallery +=
                            '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery row" style="margin: 10px; padding: 0 10px;">' +
                                    '<div class="folder_icon col-lg-12 text-center parent">' +
                                        '<i class="icon-icon-video icon80 child"></i>' +
                                    '</div>' +
                                    '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                        '<i class="icon-icon-video icon10"></i>' + element.title +
                                    '</div>' +
                                    '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                        '<i class="icon-icon-peq-upload icon10">' + element.created_at + '</i>' +
                                    '</div>' +
                                    '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                    '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                        '<div class="separacao"></div>' +
                                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                        break;
                        case 'audio':
                            $scope.gallery +=
                            '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery row" style="margin: 10px; padding: 0 10px;">' +
                                    '<div class="folder_icon col-lg-12 text-center parent">' +
                                        '<i class="icon-icon-som icon80 child"></i>' +
                                    '</div>' +
                                    '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                        '<i class="icon-icon-som icon10"></i>' + element.title +
                                    '</div>' +
                                    '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                        '<i class="icon-icon-peq-upload icon10">' + element.created_at + '</i>' +
                                    '</div>' +
                                    '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                    '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                        '<div class="separacao"></div>' +
                                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                        break;
                        case 'link':
                            $scope.gallery +=
                                '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery row" style="margin: 10px; padding: 0 10px;">' +
                                '<div class="folder_icon col-lg-12 text-center parent">' +
                                '<i class="icon-icon-peq-embed icon80 child"></i>' +
                                '</div>' +
                                '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                '<i class="icon-icon-peq-embed icon10"></i>' + element.title +
                                '</div>' +
                                '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                '<i class="icon-icon-peq-upload icon10">' + element.created_at + '</i>' +
                                '</div>' +
                                '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                '<div class="separacao"></div>' +
                                '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                '</div>' +
                                '</div>' +
                                '</div>' +
                                '</div>';
                            break;
                        default:
                            $scope.gallery +=
                            '<div class="col-lg-4" data-media-item-type="file" data-media-item-id="' + element.id + '">' +
                                '<div class="item_galery row" style="margin: 10px; padding: 0 10px;">' +
                                    '<div class="part1 image_galery col-lg-12">' +
                                    '</div>' +
                                    '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                        '<i class="icon-icon-upload-img icon10"></i>' + element.title +
                                    '</div>' +
                                    '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                        '<i class="icon-icon-peq-upload icon10">' + element.created_at + '</i>' +
                                    '</div>' +
                                    '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">2MB</div>' +
                                    '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                        '<div class="separacao"></div>' +
                                        '<div class="foot_gallery_icon3 fonte_12_lato_light_cinza img_size">' +
                                        '</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                        break;
                    }

                }

                for (var j = 0; j < $scope.galleryItems.length; ++j) {
                    var otherFolder = $scope.galleryItems[j];

                    if (otherFolder.parent_folder_id === folder.id) {
                        $scope.gallery += '<div class="col-lg-4" data-media-item-type="folder" data-media-item-id="' + otherFolder.id + '">' +
                                '<div class="item_galery folder_ref row" style="margin: 10px; padding: 0 10px;">' +
                                    '<div class="folder_icon col-lg-12 text-center parent">' +
                                        '<i class="icon-icon-pasta icon80 child"></i>' +
                                    '</div>' +
                                    '<div class="col-lg-12 fonte_12_lato first_text_gallery">' +
                                        '<i class="icon-icon-peq-pasta icon10"></i>' + otherFolder.title +
                                    '</div>' +
                                    '<div class="col-lg-8 col-xs-12 fonte_12_lato_light_cinza">' +
                                        '<i class="icon-icon-peq-upload icon10">' + otherFolder.created_at + '</i>' +
                                    '</div>' +
                                    '<div class="col-lg-4 col-xs-12 text-right fonte_12_lato_light_cinza">' + (otherFolder.size || '0 bytes') + '</div>' +
                                    '<div class="col-lg-12 fonte_12_lato_light_cinza no_padding">' +
                                        '<div class="separacao"></div>' +
                                        '<div class="text-center foot_gallery_icon fonte_12_lato_light_cinza">' + $scope.galleryItems.filter(function(e) { return e.parent_folder_id === otherFolder.id }).length + ' pastas</div>' +
                                        '<div class="text-center foot_gallery_icon2 fonte_12_lato_light_cinza">' + otherFolder.media_items.length + ' ficheiros</div>' +
                                    '</div>' +
                                '</div>' +
                            '</div>';
                    }
                }
           }
            $scope.gallery += '</div>';
            $scope.gallery = $sce.trustAsHtml($scope.gallery);
        };

        $(function() {
            $scope.buildBreadcrumb($scope.rootFolder);
            $scope.renderGallery();
        });
    }

    function link($scope, elements, attributes) {
    	$scope.galleryItems = angular.fromJson(attributes.jsonGalleryItems);
        $scope.rootFolder = attributes.rootFolder;
    }

    function picker() {
        return {
            controller: ['$scope', '$http', '$sce', ImagePickerController],
            link: link,
            restrict: 'EA',
            template: '<div class="image_picker">'+
            				'<div class="row">' +
                        		'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">' +
                            		'<ul class="breadcrumb">' +
                            			'<li data-folder-id="{{folder.id}}" ng-repeat="folder in breadcrumb" ng-click="navToFolder($event)">{{ folder.title }}</li>' +
                            		'</ul>' +
                       			'</div>' +
                    		'</div>' +
            				'<div class="col-lg-12" ng-bind-html="gallery" ng-dblclick="redraw($event)">' +
                            '</div>' +
            				'</div>' +
            		  '</div>'
        };
    }

    function filterFolderOrPicture () {
        return function(input, id) {
            var out = [];

            for (var i = 0; i < input.length; ++i) {
            	var item = input[i];
                if ( item.id >= id && (isFolder(item) || isPicture(item)) ) {
                    out.push(item);
                }
            }

            return out;
        };
    }

    angular.module('ImagePicker', ['ngSanitize'])
    		.directive('imagePicker', picker)
    		.filter('folderOrPicture', filterFolderOrPicture);
})();
