<?php

Route::group(['namespace' => 'Brainy\Profiles\Controllers\Frontend'], function () {

    Route::get('/', 'ModelFrontendController@index')->name('model.index');

});
