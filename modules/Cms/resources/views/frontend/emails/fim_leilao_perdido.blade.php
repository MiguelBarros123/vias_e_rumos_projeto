<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Caro <!-- Em Falta Firstname do utilizador e lastname -->,</p>
        <div>
            <p>A equipa da Vias & Rumos vem informar que o prazo de licitações para o Leilão <!-- numero do leilao --> terminou.</p>
            <p>Caso tenha alguma dúvida, nao hesite em contactar a nossa equipa através do e-mail <a href="mailto:apoio@viaserumos.pt">apoio@viaserumos.pt</a>.</p>
            <p>Atenciosamente,</p>
            <p>A equipa Vias & Rumos</p>
        </div>
    </body>
</html>

