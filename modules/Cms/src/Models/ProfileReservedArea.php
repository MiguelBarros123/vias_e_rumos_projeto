<?php

namespace Brainy\Cms\Models;

use App\User;
use Brainy\Rent\Models\Lot;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileReservedArea extends Model
{
    protected $guarded = [];

    use SoftDeletes;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function bidding_systems()
    {
        return $this->belongsToMany(Lot::class,'bidding_systems')->withPivot('price')->withTimestamps();
    }
}
