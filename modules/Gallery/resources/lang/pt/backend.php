<?php

return [
    'module_name'=>'Administração',
    'module-label'=>'Galeria',
    'cancel' => 'CANCELAR',
    'model' => [
        'menu' => [
            'list' => 'Galeria',
        ],
        'index' => [
            //@lang('gallery::backend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view backend page',
            'edit' => 'Editar',
            'delete' => 'Eliminar',
            'copy' => 'Copiar',
            'move' => 'Mover para...',
            'download' => 'Download',

            'gallery' => 'Galeria',
            'gallery_archive' => 'Arquivos da galeria',
            'new_folder' => 'Nova pasta',
            'upload' => 'Upload',
            'seach_gallery' => 'pesquisar por titulo, descriçao ou tags',
            'create' => 'CRIAR',
            'cancel' => 'CANCELAR',
            'remove_selected' => 'Tem a certeza que pretende apagar os items selecionados?',
            'folders' => 'pastas',
            'no_results' => 'Não foram encontrados resultados.',
            'paste' => 'Colar',
            'recover' => 'Recuperar',


        ],
        'upload' => [
            'go_back' => 'Voltar',
            'gallery' => 'Galeria',
            'upload' => 'Upload',
            'destiny' => 'Destino',
            'save_here' => 'Guardar Aqui',
            'add' => 'Adicionar',
            'drag_and_drop' => 'DRAG and DROP',
            'drag_and_click' => '<u>Arraste</u> o/s ficheiro/s para a caixa ou <u>clique</u> para fazer upload.',
            'documents' => 'Documentos',
            'zipped_folders' => 'Pastas “zipadas”',
            'images' => 'Imagens',
            'videos' => 'Vídeos',
            'audio' => 'Áudio',
            'allowed_upload' => 'É permitido upload de:',
            'title' => 'Titulo*',
            'description' => 'Descrição*',
            'add_link' => 'Adicionar link*',
            'cancel' => 'CANCELAR',
            'progress_upload' => 'Upload em progresso - ',


        ],
        'edit' => [
            'go_back' => 'Voltar',
            'gallery' => 'Galeria',
            'upload' => 'Upload',
            'destiny' => 'Destino',
            'save_here' => 'Guardar Aqui',
            'save' => 'Guardar',
            'add' => 'Adicionar',
            'title' => 'Titulo*',
            'description' => 'Descrição*',
            'cancel' => 'CANCELAR',
            'move' => 'Mover para...',
            'added_in' => 'Adicionado em',
            'size' => 'Tamanho',
            'edit_picture' => 'EDITAR IMAGEM',
            'delete' => 'Eliminar',
            'embedLink' => 'Embed Link',

        ],
        'edit_image' => [
            'go_back' => 'Voltar',
            'gallery' => 'Galeria',
            'preview' => 'Pre-visualização',
            'free' => 'Livre',
            'cancel' => 'CANCELAR',
            'save' => 'Guardar',

        ],
        'archive' => [
            'gallery' => 'Galeria',
            'gallery_archive' => 'Arquivos da galeria',
            'cancel' => 'CANCELAR',
            'remove_selected' => 'Tem a certeza que pretende apagar os items selecionados?',
            'delete' => 'Eliminar',
            'recover' => 'RECUPERAR',
            'recover_selected' => 'Tem a certeza que pretende recuperar os items selecionados?',


        ],

    ],
    'messages' => [
        'success' => [
            'delete' => 'Item(s) eliminado(s) com sucesso.',
            'update' => 'Item atualizado com sucesso.',
            'location' => 'Localização alterada com sucesso.',
            'added' => 'Embed Link adicionado com sucesso.',
            'cuted_image' => 'Nova imagem criada com sucesso.',
            'uploaded_image' => 'Upload efetuado com sucesso.',
            'recover' => 'Item recuperado com sucesso.',
            'moved' => 'Item(s) movido(s) com sucesso.',

        ],
        'error' => [
            'delete' => 'Foi impossivel eliminar :total ficheiro(s) uma vez que este(s) está(m) a ser usado(s) pela plataforma.',
            'location' => 'Erro ao alterar localização.',
        ],
    ],
    'permissions' => [

        'see' => 'ver galeria',
        'create' => 'criar/editar items',
        'delete' => 'eliminar items',

    ],
];
