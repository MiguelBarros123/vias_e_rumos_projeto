@extends('core.back.layouts.scaffold')
@section('content')
    @include('core.back.layouts.top_menu')
    @include('core.back.layouts.side_menu')
    @include('core.back.layouts.modulos_menu')
    <div class="conteudo_central">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{route('grupos.edit',array($user->group_id))}}" class="icon_voltar"><i class="icon-voltar"></i> Voltar</a></li>
                    </ul>
                </div>

            </div>

            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.groups.edit',$grupo) !!}

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo"><span class="titulo"><i class="icon-trocar-grupos icon20"></i> MUDAR DE GRUPO</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <div class="col-lg-12">
                                    <div class="">
                                        @if($user->photo === '')
                                            <div class="pic-circle-corner2 text-center profile_no_pic">
                                                <i class="icon-avatar-person icon10 "></i>
                                            </div>
                                        @else
                                            <img class="img-circle img-responsive imagem_user_mini" src="{{route('gallery::frontend::storage_image_p',[$user->photo,50,50])}}">
                                        @endif
                                        {{--<img class=" pic-circle-corner-75" src="{{asset($user->foto)}}">--}}
                                        <p class="font_12_lato_regular padding-top-10">{{$user->user->name}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form class="form_elimina_user" method="POST" action="{{route('group_change_user_group',array($user->id))}}" >
                {!! csrf_field() !!}
            <div class="row">
                <div class="col-lg-12">
                    <p class="padding-left-15 font_12_lato_regular">Mudar do</p>

                    <div class="panel ">
                        <div class="panel-body">
                            <div class="row">
                                <a href="{{route('grupos.edit',array($user->group_id))}}" target="_blank">
                                    <div class="col-lg-12">
                                        <label class="radio-default" style="margin-bottom: -2px!important;">
                                            <input type="radio" name="group_id" value="{{$user->group_id}}" checked>
                                            <span class="span-radio"></span>
                                        </label>

                                        <div class="container_images"> {!!$user->group->photosUsersForGroup() !!} </div>
                                        <label class="label-group-list fonte_12_lato_light"> {{$user->group->nome}} </label>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <p class="padding-left-15 font_12_lato_regular">para:</p>
                    @foreach($groups as $group)
                        @if($user->group_id!=$group->id)
                            <div class="panel" style="margin-bottom: 5px!important;">
                                <div class="panel-body">
                                    <div class="row">
                                        <a href="{{route('grupos.edit',array($group->id))}}" target="_blank">
                                            <div class="col-lg-12">
                                                <label class="radio-default" style="margin-bottom: -2px!important;">
                                                    <input type="radio" name="group_id" value="{{$group->id}}">
                                                    <span class="span-radio"></span>
                                                </label>

                                                <div class="container_images"> {!!$group->photosUsersForGroup() !!} </div>
                                                <label class="label-group-list fonte_12_lato_light">{{$group->nome}}
                                                    <div class="accordion-arrow"></div>
                                                </label>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="row padding-top-35">
                <div class="col-lg-12">
                    <button type="submit" class="btn btn-default btn-save-user ">MUDAR DE GRUPO</button>
                    <span class="margin-right-20"></span>
                    <a href="{{route('grupos.edit',array($user->group_id))}}"  class="btn btn-default btn-cancelar ">CANCELAR</a>
                </div>
            </div>
            </form>
        </div>
    </div>



    <div class="action_multiple">

        <div class="wrap_action_multiple text-right">
            <a> <i class="icon-opcoes-eliminar"></i>Eliminar</a>
        </div>
    </div>







@endsection

@include('core.back.layouts.footer')

