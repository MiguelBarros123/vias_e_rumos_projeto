<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 24/05/2016
 * Time: 18:13
 */

namespace Brainy\Permissions\Repositories;


interface GroupRepositoryInterface
{
    public function destroyGroups($groups);
    public function changeGroupForUsers($group, $users,$id);
    public function updateUserGroup($group, $user);
    public function storeGroup($group);
}