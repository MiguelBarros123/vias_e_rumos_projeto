<?php

namespace Brainy\ProfileReservedArea\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as LaravelProvider;

class ProfileReservedAreaServiceProvider extends LaravelProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        Blade::directive('activePageTopMenu', function ($menu) {
            return "<?= (Route::currentRouteName() == $menu) ? 'ativo' : '' ?>";
        });
    }

    /**
     * Register the application services.
     */
    public function register()
    {
    }
}
