/**
 * Created by User on 09-03-2017.
 */

//-----------------------------------------------------------setting the names for atributes
var id=0;
var ultimo= $('.item_for_condition[data-id=""]');
ultimo.attr('data-id',id);

if($('.item_for_condition').length > 1){
    var ordenados=$('.item_for_condition').sort(function (a, b) {

        var contentA =parseInt( $(a).attr('data-id'));
        var contentB =parseInt( $(b).attr('data-id'));
        return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
    });
    var prev=ordenados.last();

    id=parseInt(prev.attr('data-id'))+1;
    ultimo.attr('data-id',id);
}

var block_id=ultimo.parent().parent().attr('id');

ultimo.find('.name_for_first_option').attr('name','item['+block_id+'][age]['+id+'][option][]');
ultimo.find('.age_for_user').attr('name','item['+block_id+'][age]['+id+'][term][0]');
ultimo.find('.age_for_user_start').attr('name','item['+block_id+'][age]['+id+'][term][1]');
ultimo.find('.age_for_user_end').attr('name','item['+block_id+'][age]['+id+'][term][2]');
ultimo.find('.filter_field_id').attr('name','item['+block_id+'][age]['+id+'][filter_field_id][]');

//----------------------------------------------------------end setting the names for atributes

$('.age_input').change(function () {
    //if value is between
    if($(this).val()=='7'){
        $(this).parent().next().find('.section_others').hide();
        $(this).parent().next().find('.section_between').fadeIn();
    }else{
        $(this).parent().next().find('.section_others').fadeIn();
        $(this).parent().next().find('.section_between').hide();
    }
});
//# sourceMappingURL=age.js.map
