$(document).ready(function () {

    if (typeof angular != 'undefined') {
        angular.bootstrap('body', ['BootstrapToggler']);
    }
    
    $('.lang_tabs li:first').tab('show');
    $('.lang_content div:first').addClass('active');
    $('.pay_by_day').bootstrapToggle();
    $('.has_quantities').bootstrapToggle();

    var $injector =angular.injector(['ng','ImagePicker']);
    var html = document.getElementsByTagName('image-picker');
    $injector.invoke(function($compile, $rootScope){
        var $scope = $rootScope.$new();
        var result= $compile(html)($scope);
        var cScope = result.scope(); // This is scope in directive controller. :  )
        var myBtn = document.getElementById('ulpload-directive');
        myBtn.addEventListener('click', cScope.fetchNewFiles);
    });

    $('#btn_upload_image').on('click', function () {

        var selected = $('div.item_galery.ui-selected');
        if(selected.length){

            document.getElementById('image').value = selected.parent().attr('data-media-item-id');
            $('#upload_resource').modal('hide');

            var bgUrl = selected.children().first().css('background-image');
            bgUrl = /^url\((['"]?)(.*)\1\)$/.exec(bgUrl);
            bgUrl = bgUrl ? bgUrl[2] : '';

            $('.image_highlight').attr('src', bgUrl);

        } else {
            alert('Selecione uma imagem');
        }

    });

    //-----------------------------------------------------------------prices
    $('.no_iva_input').on('change keyup',function () {
        var sanitized = $(this).val().replace(/[^0-9\.\/]/g, '');
        $(this).val(sanitized);

        if($(this).val()!='' ){
            var iva=$('#selected_tax').val();
            var inicial=parseFloat($(this).val());
            var tot_iva=(inicial*iva/100);
            $(this).parent().parent().next().find('.w_iva_input').val((inicial+tot_iva).toFixed(2));
        }else{
            $(this).parent().parent().next().find('.w_iva_input').val(null)
        }
    });

    $('.w_iva_input').on('change keyup',function() {

        var sanitized = $(this).val().replace(/[^0-9\.\/]/g, '');
        $(this).val(sanitized);

        if($(this).val()!='') {
            var iva = $('#selected_tax').val();
            var inicial = parseFloat($(this).val());
            var tot_iva = parseFloat(1 + (iva / 100));
            console.log(tot_iva);
            $(this).parent().parent().prev().find('.no_iva_input').val((inicial / tot_iva).toFixed(2));
        }else{
            $(this).parent().parent().prev().find('.no_iva_input').val(null);
        }
    });
    //-----------------------------------------------------------------end prices

});
//# sourceMappingURL=extras.js.map
