<?php

return [
    'button_template' => '<a href="#" data-btn-state="{state}" data-btn-group="{group}" data-btn-value="{value}" class="btn-settings {active}"><i class="{cssClasses}"></i>{coinIcon}</a>',
];
