<div class="collapse navbar-collapse navbar-ex1-collapse menu_lateral">
    <div class="nav navbar-nav side-nav">
@foreach(Brainy::containerModules() as $module)
    <ul class="module @activeModule($module)">
        @include('layouts.backend.menu', ['menu_items' => $module->menu_items])
    </ul>
@endforeach
</div>
</div>
