<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuctionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auctions', function(Blueprint $table){
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->unsignedInteger('category_id');
            $table->string('title');
            $table->text('comments');
            $table->boolean('featured')->default(false);
            $table->boolean('active')->default(false);
            $table->datetime('date_limit')->nullable();
            $table->datetime('date');
            $table->string('district');
            $table->string('parish');
            $table->string('cod_postal');
            $table->string('county');
            $table->string('local');
            $table->string('address');
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            $table->string('ref_intern');
            $table->unsignedInteger('number_process');
            $table->string('catalog')->nullable();
            $table->string('advertisement')->nullable();
            $table->string('admin_insolvency')->nullable();
            $table->string('minuta')->nullable();
            $table->string('conditions')->nullable();
            $table->string('tribunal');
            $table->string('executed');
            $table->string('visits');

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('auctions');
    }
}
