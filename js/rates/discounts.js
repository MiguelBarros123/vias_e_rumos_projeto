var myTemplate_types_table = "<tr id='sel_item_list2_types_table_${id}'>" +
    "<td> ${name} <input type='hidden' name='types[]' value='${id}' /></td> " +
    "<td class='text-right'><a id='delete_selected_item_datatable_list_types_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
    "</tr>";

var myTemplate_offers_table = "<tr id='sel_item_list2_offers_table_${id}'>" +
    "<td>${name} <input type='hidden' name='offers[]' value='${id}' /></td>" +
    "<td>${start_date}</td>" +
    "<td>${end_date}</td>" +
    "<td class='text-right'><a id='delete_selected_item_datatable_list_offers_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
    "</tr>";

$(document).ready(function () {

    if (typeof angular != 'undefined') {
        angular.bootstrap('body', ['BootstrapToggler']);
    }

    $('.visible_home').bootstrapToggle();
    $('.visible_offers').bootstrapToggle();

    var $injector =angular.injector(['ng','ImagePicker']);
    var html = document.getElementsByTagName('image-picker');
    $injector.invoke(function($compile, $rootScope){
        var $scope = $rootScope.$new();
        var result= $compile(html)($scope);
        var cScope = result.scope(); // This is scope in directive controller. :  )
        var myBtn = document.getElementById('ulpload-directive');
        myBtn.addEventListener('click', cScope.fetchNewFiles);
    });

    $('#btn_upload_image').on('click', function () {

        var selected = $('div.item_galery.ui-selected');
        if(selected.length){

            document.getElementById('image').value = selected.parent().attr('data-media-item-id');
            $('#upload_resource').modal('hide');

            var bgUrl = selected.children().first().css('background-image');
            bgUrl = /^url\((['"]?)(.*)\1\)$/.exec(bgUrl);
            bgUrl = bgUrl ? bgUrl[2] : '';

            $('.image_highlight').attr('src', bgUrl);

        } else {
            alert('Selecione uma imagem');
        }

    });
    
    if(start_range != '' && end_range != ''){
        var start_date = start_range;
        var end_date = end_range;
    } else {
        var start_date = moment();
        var end_date = moment().add(5, 'days');   
    }

    $('#date_range').daterangepicker({
        "locale": {
            "format": 'DD/MM/YYYY'
        },
        "minDate": start_date,
        "startDate": start_date,
        "endDate": end_date
    });

    //---------------------------------------------------------------start types
    var optionsTypes = {
        rota: selecting_types,
        colunas: [
            {data: 'name', name: 'name'},
            {data: null, defaultContent: '', orderable: false, searchable: false}
        ],
        config: {
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                //for already selected rows
                $(nRow).attr('id', 'sel_row_'+aData['id']);
                if(aData['already_selected']==1){
                    optionsTypes.datatable.row(nRow).select();
                }

            },

            initComplete: function(settings, json) {
                $(function() {
                    $.logicDatatable(optionsTypes);
                    $.selectDatatableLogic(optionsTypes);
                    $('.types_check').bootstrapToggle();


                })
            }



        },
        searchableElementId: 'types_search'
    };

    $('#types_table').mydatatable(optionsTypes);

    $('#select_types_table').on('shown.bs.modal', function (e) {

        optionsTypes.datatable.columns.adjust().draw();

    });

    $('#select_types_table').on('hide.bs.modal', function (e) {

        $('.types_check').bootstrapToggle();

    });

    $('#save_selection_types_table').on('click', function(){
        $('#select_types_table').modal('toggle');
    });

    //---------------------------------------------------------------end types

    //---------------------------------------------------------------start offers

    var init_url = selecting_offers;

    var optionsOffers = {
        rota: init_url.replace(':range', btoa($('#date_range').val())),
        colunas: [
            {data: 'name', name: 'name'},
            {data: 'start_date', name: 'start_date'},
            {data: 'end_date', name: 'end_date'},
            {data: null, defaultContent: '', orderable: false, searchable: false}
        ],
        config: {
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                //for already selected rows
                $(nRow).attr('id', 'sel_row_'+aData['id']);
                if(aData['already_selected']==1){
                    optionsOffers.datatable.row(nRow).select();
                }

            },

            initComplete: function(settings, json) {
                $(function() {
                    $.logicDatatable(optionsOffers);
                    $.selectDatatableLogic(optionsOffers);
                    $('.offers_check').bootstrapToggle();


                })
            }



        },
        searchableElementId: 'offers_search'
    };

    $('#offers_table').mydatatable(optionsOffers);

    $('#select_offers_table').on('shown.bs.modal', function (e) {

        var reload_url = selecting_offers;

        optionsOffers.datatable.ajax.url(reload_url.replace(':range', btoa($('#date_range').val()))).load();

    });

    $('#select_offers_table').on('hide.bs.modal', function (e) {

        $('.offers_check').bootstrapToggle();

    });

    $('#save_selection_offers_table').on('click', function(){
        $('#select_offers_table').modal('toggle');
    });

    //---------------------------------------------------------------end offers

});
//# sourceMappingURL=discounts.js.map
