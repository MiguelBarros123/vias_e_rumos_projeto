<?php

return [
    // The label is used to represent the module's name
    'label' => 'auctionsonline::backend.module-label',

    // Permissions are organized by groups
    // The level 1 key denotes the group name (label)
    // The level 1 value is an associative array
    // The level 2 key denotes the permission name (label)
    // The level 2 value is an array with the routes that requires the permission
    'permissions' => [
        'auctionsonline::backend.permission.auctions_online.see' => [
            'auctionsonline::backend.permission.auctions_online.see' => [
                'auctionsonline::backend::auctions_online.index',
                'auctionsonline::backend::auctions_online.index_ajax',
                'auctionsonline::backend::auctions_online.index_ajax_lots',
                'auctionsonline::backend::auctions_online.edit',
                'auctionsonline::backend::auctions_online_lots.edit',
            ]
        ],
        'auctionsonline::backend.permission.auctions_online.create' => [
            'auctionsonline::backend.permission.auctions_online.create' => [
            ],
        ], 
        'auctionsonline::backend.permission.auctions_online.delete' => [
            'auctionsonline::backend.permission.auctions_online.delete' => [
                'auctionsonline::backend::auctions_online.delete',
            ],
        ],       
    ],

    // Menu items denote the menus that this module renders
    'menu-items' => [
        [
            // Label for the menu item (mandatory)
            'label' => 'auctionsonline::backend.menu.auctions_online',
            // Route for the action (mandatory if the menu has no childs)
            'route' => 'auctionsonline::backend::auctions_online.index',
            // Css class can be used to render the icon
            'css-class' => 'icon-perfis-teste-1 icon15menu',
            // Unique html id for the item
            'css-id' => null,
            // list of submenus
            'sub-menus' => [],
        ]
    ],
    // List of required providers for the module to work
    'providers' => [
        Brainy\AuctionsOnline\Providers\AuctionsOnlineServiceProvider::class,
    ],

    // List of facades used by the module
    'aliases' => [
        // 'Recaptcha' => Greggilbert\Recaptcha\Facades\Recaptcha::class,
    ],

    // List of route middleware to register
    'middleware' => [
        //'acl' => \Brainy\Profiles\Middleware\SomeMiddleware::class,
    ],

    'commands' => [
        // \Brainy\Profiles\Commands\SomeCommand::class,
    ],

];
