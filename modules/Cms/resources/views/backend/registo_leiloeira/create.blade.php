@extends('layouts.backend.app')

@section('title', 'Registo Leiloeira')

@section('module-styles')

@endsection

@section('module-scripts')
<script>
    $('#reset_registo_leiloeira').on('click', function(){
        var registo_leiloeira = $('#registo_leiloeira');
        registo_leiloeira.val("");
    });
</script>
@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
    <div class="container-fluid">
        <div class="row tabs-back">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                @include('cms::backend.pages.layouts.top_menu')
            </div>
        </div>
        @include('layouts.backend.messages')
        <div class="row">
            <form method="POST" action="{{ route('cms::backend::backend.cms.registo_leiloeira.store')}}" autocomplete="off" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20">
                                </i>
                                Registo Leiloeira
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">



                                    <div class="row">
                                        <div class="col-lg-3">
                                            <input type="file" id="registo_leiloeira" name="registo_leiloeira" accept="application/pdf"/>
                                            <button type="button" name="reset_registo_leiloeira" id="reset_registo_leiloeira">Registo Leiloeira</button>
                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('rent::backend.common.save')</button>
                    <a class="btn btn-default btn-cancelar" href="{{route('cms::backend::backend.cms.registo_leiloeira.index')}}">@lang('rent::backend.common.cancel')</a>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection



