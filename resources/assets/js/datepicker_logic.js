$(function() {
    $('#skedule').datepicker(
        {
            minDate: 0,
            dateFormat: 'yy-mm-dd',
            closeText: 'ok',
            timeInput: false,

            secondText: 'Segundo',
            currentText: 'Agora',
            showOn: "button",
            buttonImageOnly: false,
            buttonText: "<i class='icon-icon-lateral-calendario icon15'></i>",
            onSelect: function (dateText, obj) {
                if (dateText) {
                    $(this).next().css('background-color', '#FFD200');
                }

            }
        }
    );

});
