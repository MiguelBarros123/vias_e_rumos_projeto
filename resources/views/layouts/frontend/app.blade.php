<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="p:domain_verify" content="750af47b0cf050f25a6fc1ec35b39b9e"/>
    <title>@yield('title')</title>

    <meta name="description" content="@yield('description')" />
    <meta name="keywords" content="@yield('keywords')" />

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="google" value="notranslate">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">

    <!-- Facebook share -->
    <meta property="fb:app_id" content="966242223397117" />
    <meta property="og:title" content="@yield('title')">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{Request::url()}}">
    <meta property="og:image" content="@yield('image')">
    <meta property="og:site_name" content="@yield('title')">
    <meta property="og:description" content="@yield('description')">

    <!-- Twitter share -->
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="@yield('title')">
    <meta name="twitter:description" content="@yield('description')">
    <meta name="twitter:image" content="@yield('image')">
    <meta name="twitter:domain" content="{{Request::url()}}">

    <meta name="p:domain_verify" content="750af47b0cf050f25a6fc1ec35b39b9e"/>

    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Didact+Gothic|Palanquin+Dark:400,600|Palanquin:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('front/css/slick.css')}}">

    <link rel="stylesheet" href="{{asset('front/css/bootstrap.min.css')}}">
     <link rel="stylesheet" href="{{asset('front/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/comfortaa.css')}}">

    <link rel="stylesheet" href="{{asset('front/css/front.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/style.css')}}">
    <link rel="stylesheet" href="{{asset('front/css/styles.css')}}" >

    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <meta name="theme-color" content="#ffffff">
    
    @yield('module-styles')

    <!-- Facebook Pixel Code -->
        <!-- <script>
            !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
                n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
                    document,'script','https://connect.facebook.net/en_US/fbevents.js');

            fbq('init', '1191797094227787');
            fbq('track', "ViewContent");</script>
        <noscript>
            <img height="1" width="1" style="display:none"
                       src="https://www.facebook.com/tr?id=1191797094227787&ev=PageView&noscript=1"
            />
        </noscript> -->
        <!-- End Facebook Pixel Code -->
        <!-- Global site tag (gtag.js) - Google Analytics -->


        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-114617902-1"></script>

        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-114617902-1');
      </script>


  </head>
    
    <!-- <script>
        var title = "@yield('title')";
        var language = "{{session('locale')}}";
        fbq('track', 'ViewContent', {
            content_name: title,
            language: language
        });

    </script>
    <script>fbq('track', 'Search');</script> -->

    <body>


        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="{{asset('front/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('front/js/jquery.watermark.min.js')}}"></script>
        <script src="{{asset('front/js/slick.min.js')}}"></script>
         <script src="{{asset('front/js/bootstrap-datepicker.min.js')}}"></script>
        <div class="container">
            @include('cms::frontend.pages.layouts.top-menu')
            @if(\Request::route()->getName() === 'cms::frontend::model.vendas' || \Request::route()->getName() === 'cms::frontend::model.leilao')
                @include('cms::frontend.pages.layouts.vendas-menu')
                @yield('content')
            @else
                
                @include('cms::frontend.pages.layouts.menu')
                @yield('content')
            @endif
            
        </div>
        @include('cms::frontend.pages.layouts.footer')


        <script>
           

        $( document ).ready(function() {
            $(document).on('click','.search-button',function(){
                var url = "{{ route('cms::frontend::model.vendas')}}";
                var search_val = encodeURIComponent($('[name="search"]').val());
                url = url + "?search_query=" +search_val;
                window.location.href = url;
            });

            setTimeout(function() {

            }, 1000);
        });

        function enableScroll(){
            $('body').removeClass('no-scroll');
        }

        function disableScroll(){
            $('body').addClass('no-scroll');
        }

        function openSearch(event){
            event.preventDefault();
            closeMenu(event);
            disableScroll();
            $('.open-search').find('img').attr('src',"{{asset('front/imgs/icons/close.svg')}}");
            $('.open-search').addClass('close-search');
            $('.search-opened').addClass('set-search');
        }

        function closeSearch(event){
            event.preventDefault();
            enableScroll();
            $('.open-search').find('img').attr('src',"{{asset('front/imgs/icons/search.svg')}}");
            $('.open-search').removeClass('close-search');
            $('.search-opened').removeClass('set-search');

        }

        function openMenu(event){
            event.preventDefault();
            closeSearch(event);
            disableScroll();
            $('.open-menu').find('img').attr('src',"{{asset('front/imgs/icons/close.svg')}}");
            $('.open-menu').addClass('close-menu');
            $('.menu-opened').addClass('set-menu');
            scale_o=3;
/*          $('.open-menu').addClass('open');
            $('.menu-opened').addClass('o-menu');
            $('.open-menu').addClass('open-r');*/


        }

        function closeMenu(event){
            event.preventDefault();
            enableScroll();
            $('.open-menu').find('img').attr('src',"{{asset('front/imgs/icons/menu.svg')}}");
            $('.open-menu').removeClass('close-menu');
            $('.menu-opened').removeClass('set-menu');
        }

        $('body').on('click', '.open-menu', function(event){
            if($(this).hasClass('close-menu')){
                closeMenu(event);
            }else{          
                openMenu(event);
            }   
        });


        $('body').on('click', '.open-search', function(event){
            if($(this).hasClass('close-search')){
                closeSearch(event);
            }else{          
                openSearch(event);

            }
        });

        $('body').on('click', '.close-search_trigger', function(event){ 
            closeSearch(event);
        });


        $('.dropdown').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(300);
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(300);
        });

        $("body").on('click','.go-up', function() { 
            $("html").animate({ 
                scrollTop: 0
            }, 600); 
        });
    
        var calendar = $('#calendar');


        $('body').on('click', function(){
            $('.links-list li').removeClass('active');
            $('.links-menu .tab-pane').removeClass('active');
        });

        $('body').on('click', '.links-menu .tab-pane' ,function(e){
            e.preventDefault();
            e.stopPropagation();
        });

</script>
@yield('module-scripts')
</body>
</html>
