<?php

namespace Brainy\Permissions\Controllers\Backend;

use App\User;
use Brainy\ECommerce\Models\Emails;
use Brainy\Framework\Controllers\BackendController;
use Brainy\Framework\Models\Group;
use Brainy\Framework\Models\UserDetail;
use Brainy\Gallery\Models\MediaItem;
use Brainy\Permissions\Repositories\UserDetailRepositoryInterface;
use Brainy\Permissions\Requests\CreateNewUserRequest;
use Brainy\Profiles\Models\Profile;
use Brainy\Profiles\Models\ProfileGroup;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Facades\Storage;
use Yajra\Datatables\Facades\Datatables;

class UserBackendController extends BackendController
{

    protected $userRepo;

    public function __construct(UserDetailRepositoryInterface $userRepo, Guard $auth, PasswordBroker $passwords)
    {
        parent::__construct();
        $this->userRepo = $userRepo;
    }

    public function index()
    {
        $locales=config('translatable.locales');
        $back_groups=Group::all();
        $front_groups=ProfileGroup::all();

        $data = [
            'locales' => $locales,
            'back_groups' => $back_groups,
            'front_groups' => $front_groups
        ];

        return view('permissions::backend.model.users.index')->with($data);
    }

    public function create()
    {
        return view('permissions::backend.model.create');
    }

    public function store(CreateNewUserRequest $request)
    {

        if(array_key_exists('email',$request->input())){
            $user = User::where('email', $request->email)->first();
            if($user){
                return redirect()->back()->with('error-message', trans('profiles::backend.messages.error.existing_user'));
            }
        }

        $thumbnail='';

        if(isset($request->profile_image)){
            $file = $request->profile_image;
            $thumbnail = $file->getClientOriginalName();
            $file->move(Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix(), $file->getClientOriginalName());
            $request->request->add(['profile_image' => Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$file->getClientOriginalName()]);
        }

        $type = 'back_user';

        $group = null;

        if(array_key_exists('front_group_id',$request->input())){
            $group = $request->front_group_id;
        }

        /* Save Profile */

        $profile = Profile::create([
            'name'=>$request->name,
            'surname'=>$request->surname,
            'thumbnail'=>$thumbnail,
            'type'=>$type,
            'job'=>$request->job_position,
            'profile_group_id'=>$group
        ]);

        /* Save User */

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        $data = [
            'user' => $user,
            'email' => $request->email,
            'password' => $request->password,
            'lang' => session('locale')
        ];

        $profile->user_id = $user->id;

        $profile->save();

        /* Save User Details */

        $back_group = null;

        if(array_key_exists('back_group_id',$request->input())){
            $back_group = $request->back_group_id;
        }

        $details = [
            'photo' => $thumbnail,
            'surname' => $request->surname,
            'job_position' => $request->job_position
        ];

        $userDetails = factory(\Brainy\Framework\Models\UserDetail::class)->make($details);
        $userDetails->is_active = true;
        $userDetails->is_super_admin = false;
        $userDetails->group_id = $back_group;
        $user->details()->save($userDetails);

        return redirect()->route('permissions::backend::model.index', [
            'sucess-message' => trans('permissions::backend.messages.success.index.create_user_sucess', ['utilizador' => $user->name])
        ]);
    }


    public function confirmRegister($token)
    {
        if(!$token)
        {
            dd("erro token");
        }
        return view('core.back.gestao_permissoes.utilizadores.activate_account', [
            'token' => $token,
        ]);
    }

    public function show($model)
    {
        $user=UserDetail::findOrFail($model);

        return view('permissions::backend.model.users.view',compact('user'));
    }

    public function showArchived($model)
    {

        $user=UserDetail::withTrashed()->findOrFail($model);

        return view('permissions::backend.model.users.archived.view',compact('user'));
    }

    public function edit($model)
    {
        $locales=config('translatable.locales');
        $user = User::findOrFail($model);
        $back_groups=Group::all();
        $front_groups=ProfileGroup::all();

        $data = [
            'locales' => $locales,
            'user' => $user,
            'back_groups' => $back_groups,
            'front_groups' => $front_groups
        ];

        return view('permissions::backend.model.users.edit')->with($data);
    }

    public function update(Request $request, $model)
    {
        $user = User::findOrFail($model);

        $this->validate($request, [
            'email' => 'required|email|unique:users,email,'.$user->id
        ]);

        $thumbnail='';

        if(isset($request->profile_image)){
            $file = $request->profile_image;
            $thumbnail = $file->getClientOriginalName();
            $file->move(Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix(), $file->getClientOriginalName());
            $request->request->add(['profile_image' => Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$file->getClientOriginalName()]);
        }

        $group = null;

        if(array_key_exists('front_group_id',$request->input())){
            $group = $request->front_group_id;
        }

        /* Save Profile */

        $profile = Profile::findOrFail($user->profile->id);

        $profile->update([
            'name'=>$request->name,
            'surname'=>$request->surname,
            'thumbnail'=>$thumbnail,
            'job'=>$request->job_position,
            'profile_group_id'=>$group
        ]);

        /* Save User */

        $user->update([
            'name' => $request->name,
            'email' => $request->email
        ]);

        /* Save User Details */

        $back_group = null;

        if(array_key_exists('back_group_id',$request->input())){
            $back_group = $request->back_group_id;
        }

        $userDetails = UserDetail::findOrFail($user->id);

        $userDetails->update([
            'photo' => $thumbnail,
            'surname' => $request->surname,
            'job_position' => $request->job_position,
            'group_id' => $back_group
        ]);

        return redirect()->route('permissions::backend::model.index')->with('sucess-message',trans('permissions::backend.user.messages.success.update', ['name' => $user->name]));

    }

    public function changeUserGroup($id){

        $user=UserDetail::findOrFail($id);
        $users = collect();
        $users->push($user);
        $old_group=$user->group->id;
        $groups = Group::where('id','!=',$old_group)->get();

        return view('permissions::backend.model.users.change')
            ->with('groups',$groups)
            ->with('old_group',$old_group)
            ->with('users',$users)
            ->with('user_id',$user->id);
    }

    public function updateUsersGroup(Request $request,$id){


        $user = $request->input('deleted_orders');
        $new_group = $request->input('group_id');


        DB::transaction(function () use($user,$new_group,$id) {

            $user=UserDetail::findOrFail($user);
            $group=Group::findOrFail($new_group);
            $group->users()->save($user);
            $group->update(['updated_by'=>auth()->user()->email]);
            Group::findOrFail($id)->update(['updated_by'=>auth()->user()->email]);


        });

        return redirect()->route('permissions::backend::model.edit',array($request->user_id))->with('sucess-message', trans('permissions::backend.user.messages.success.edit_group'));
    }

    public function recoverPassword(Request $request)
    {
//        dd("recoverPassword");

        $response = $this->userRepo->recoverPassword($request->input('email'));
        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()
                    ->with('sucess-message',
                        trans('permissions::backend.user.messages.success.recovery_password'));
            //->with('status', trans($response));
            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);
        }

    }



    public function delete($model)
    {
        return back();
    }


    public function indexArchived()
    {
        return view('permissions::backend.model.users.archived.index', ['groups', Group::all()]);
    }

    public function allUsers()
    {

        $users = UserDetail::regularUsers()->select('photo', 'job_position', 'group_id', 'id', 'flags', 'surname', 'flags');


        return Datatables::of($users)

            ->editColumn('photo',function($user){

                if($user->photo === ''){
                    $data = '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
                } else {
                    $data = '<img class="image_user_picked2" src="'.route('gallery::frontend::storage_image_p',[$user->photo,50,50]).'">';
                }

                return $data.' '.$user->user->name.' '.$user->surname;
            })

            ->editColumn('email',function($user){

                return '<i class="icon-email icon15 padding-top-5" style="padding-right: 3px"></i>' .$user->user->email;
            })
            ->editColumn('group',function($user){

                if($user->group){
                    return $user->group->name;
                } else {
                    return '';
                }


            })
            ->addColumn('stateMessage', function ($user) {
                return '<div id="info-lock-user-' . $user->id . '" 
                            class="user-locked-info"
                            style="visibility: ' . ($user->isActive ? 'hidden' : 'visible') . ';">
                                <i class="icon-bloqueado icon20"></i>
                                <span>' . trans('permissions::backend.user.index.info-blocked') . '</span>
                        </div>';
            })
            ->addColumn('state', function ($user) {

                $routeState = route('permissions::backend::model.change_user_state', [$user->id]);

                return '<input id="toggle_'.$user->id.'" data-row="'.$user->id.'" type="checkbox" '.($user->getIsActiveAttribute() ? 'checked' : '').' data-on=" " data-off=" " data-toggle="toggle">';
            })
            ->addColumn('action', function ($user) {

                $class2 = $user->deleted_at != null ? "fa fa-recycle" : "fa fa-trash";
                $view_route = route('permissions::backend::model.show',[$user->id]);

                $route_del = route('permissions::backend::users.delete_multiple');

                $used = 0;

                $string = '
                        <div class="btn-group pull-right">
                          <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a  class="open_edit_atrribute_modal" href="'.$view_route.'" ><i class="icon-visivel"></i>  '.trans("profiles::backend.common.view").'</a></li>
                            <li><a href="'.route('permissions::backend::model.edit',array($user->id)).'" class=""><i class="icon-editar"></i> Editar</a></li>
                            <li><a data-impossible-delete="'.$used.'"
                                    data-target="#delete_resource"
                                    data-toggle="modal"
                                    class="delete_item_table"
                                    data-id="' . $user->id . '"
                                    data-route="'.$route_del.'">
                                <i class="icon-opcoes-eliminar"></i> Eliminar </a>
                            </li>
                          </ul>
                        </div>';

                return $string;
            })

            ->addColumn('userState', function ($user) {
                return $user->getIsActiveAttribute();
            })

            ->addColumn('rota_final', function ($user) {
                return route('permissions::backend::model.show',[$user->id]);
            })
            ->make(true);
    }

    public function changeUserState($id)
    {
        try {
            $state = $this->userRepo->changeUserState($id);
            $msg = [
                'userId' => $id,
                'state' => $state,
                'successMsg' => trans('permissions::backend.messages.success.edit.change_user_state'),
            ];
        } catch (\Exception $e) {
            $msg = [
                'userId' => $id,
                'errorMsg' => trans('permissions::backend.messages.error.edit.change_user_state'),
            ];
        }

        return json_encode($msg);
    }



    public function allArchivedUsers()
    {

        $user_logged = Auth::user()->id;

        $users = UserDetail::onlyTrashed();


        /*User::join('grupo_bases',)->allusers()*/
        return Datatables::of($users)

            ->editColumn('foto',function($user){

                if($user->photo === ''){
                    $data = '<div class="pic-circle-corner2 text-center profile_no_pic"> <i class="icon-avatar-person icon10 "></i></div>';
                } else {
                    $data = '<img class="image_user_picked2" src="'.route('gallery::frontend::storage_image_p',[$user->photo,50,50]).'">';
                }

                return $data.' '.$user->user->name.' '.$user->surname;
            })

            ->editColumn('eliminado',function($user){

                return $user->deleted_at;
            })
            ->editColumn('cargo',function($user){

                return $user->job_position;
            })
            ->editColumn('email',function($user){

                return '<i class="icon-email icon20"></i>'.$user->user->email;
            })
            ->editColumn('grupo',function($user){

                if($user->group){
                    return $user->group->name;
                } else {
                    return '';
                }
            })

            ->addColumn('action', function ($user) {

                ($user->deleted_at!= null)? $class2 = "fa fa-recycle" : $class2 = "fa fa-trash";

                $view_route = route('permissions::backend::model.show_archived',[$user->id]);
                $rotaRestore=route('permissions::backend::restore_archived_users');


                $string = '
                        <div class="btn-group pull-right">
                          <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                             <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a  class="open_edit_atrribute_modal" href="'.$view_route.'" ><i class="icon-visivel"></i>  '.trans("profiles::backend.common.view").'</a></li>
                            <li><a data-impossible-delete="0"
                                    data-target="#delete_resource"
                                    data-toggle="modal"
                                    class="restore_user delete_item_table"
                                    data-id="' . $user->id . '"
                                    data-route="'.$rotaRestore.'">
                                <i class="icon-recuperar"></i> Recuperar</a>
                            </li>
                          </ul>
                        </div>';

                return $string;
            })
            ->addColumn('rota_final', function ($user) {
                return route('permissions::backend::model.show_archived',[$user->id]);
            })

            ->make(true);
    }



    public function deleteMultiple(Request $request)
    {

        $groups = $request->input('deleted_orders');
        $all_groups = explode(",", $groups);
        $messages = $this->userRepo->destroyUsers($all_groups);

        return redirect()->route('permissions::backend::model.index')->with('error-message',$messages['error'])->with('sucess-message',$messages['success']);
    }

    public function deleteMultipleUsers(Request $request)
    {

        foreach(explode(',',$request->deleted_rows) as $company){

            $user = UserDetail::findOrFail($company);

            $profiles = Profile::where('user_id', $company)->get();

            foreach ($profiles as $profile){
                $profile->delete();
            }

            $user->delete();

        }

//        return redirect()->back()->with('sucess-message', trans('profiles::backend.messages.success.delete_users'));
        return redirect()->route('permissions::backend::model.index')->with('sucess-message', trans('profiles::backend.messages.success.delete_users'));
    }

    public function restoreArchivedUser(Request $request)
    {
        foreach(explode(',',$request->deleted_rows) as $company){

            $user = UserDetail::withTrashed()->where('id', $company)->first();

            $profiles = Profile::withTrashed()->where('user_id', $company)->get();

            foreach ($profiles as $profile){
                $profile->restore();
            }

            $user->restore();

        }

        return redirect()->back()->with('sucess-message', trans('profiles::backend.messages.success.restore_users'));
    }

}
