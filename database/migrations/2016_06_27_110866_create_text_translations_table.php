<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTextTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('text_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('text_id')->unsigned();

            $table->string('title');
            $table->string('subtitle');
            $table->text('description');


            $table->unique(['text_id','locale']);
            $table->foreign('text_id')->references('id')->on('texts')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('text_translations');
    }
}
