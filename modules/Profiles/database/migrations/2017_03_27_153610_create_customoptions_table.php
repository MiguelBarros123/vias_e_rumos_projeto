<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomOptionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('custom_options', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('custom_field_id')->unsigned();
            $table->foreign('custom_field_id')->references('id')->on('custom_fields');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('custom_options');
	}

}
