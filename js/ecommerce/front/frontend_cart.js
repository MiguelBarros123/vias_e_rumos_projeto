/**
 * Created by User on 30-09-2016.
 */
$(function () {
    $('.footable').footable();
});
$(document).on('change','.cart-select',function(){
    var id = $(this).attr('data-id-product');
    var item_id = $(this).attr('data-id');
    var row = $(this).attr('data-id-cart-row');



    var $attributes = [];

    $('select[name="attribute['+item_id+'][]"]').each(function() {
        $attributes.push($(this).val());
    });

    $('.loding_cart').fadeIn();

    var url = rota_update;
    url = url.replace(':id_cart', row);
    url = url.replace(':attributes', $attributes);
    url = url.replace(':id', id);


    $.ajax({
        url         : url,
        type        : 'get',
        success:function(data)
        {

            $('.cart_section').empty();
            $('.cart_section').append(data);
            $('.loding_cart').fadeOut();

            $('[data-toggle="tooltip"]').tooltip();


        }
    });
});

$(document).on('click','.remove_cart_item',function(){

    var item_id = $(this).attr('data-id');
    var row = $(this).attr('data-id-cart-row');


    var url = rota_delete;
    url = url.replace(':id_cart', row);
    url = url.replace(':id', item_id);
    $('.loding_cart').fadeIn();

    $.ajax({
        url         : url,
        type        : 'get',
        success:function(data)
        {

            $('.cart_section').empty();
            $('.cart_section').append(data);
            $('.loding_cart').fadeOut();

            $('[data-toggle="tooltip"]').tooltip();

        }
    });
});

$(document).on('click','.remove_pack_item',function(){

    var item_id = $(this).attr('data-id');
    var row = $(this).attr('data-id-cart-row');


    var url = rota_delete_pack;
    url = url.replace(':id_cart', row);
    url = url.replace(':id', item_id);
    $('.loding_cart').fadeIn();

    $.ajax({
        url         : url,
        type        : 'get',
        success:function(data)
        {

            $('.cart_section').empty();
            $('.cart_section').append(data);
            $('.loding_cart').fadeOut();

            $('[data-toggle="tooltip"]').tooltip();

        }
    });
});

var request2;
$(document).on('change keyup','.qtd_final',function(){

    if (request2) {
        request2.abort();
    }
    var qty = $(this).val();
    var row = $(this).attr('data-id-cart-row');

    var sanitized = $(this).val().replace(/[^0-9]/g, '');
    if(sanitized == 0){
        $(this).val('');
        sanitized = '';
    }
    $(this).val(sanitized);
    if (sanitized != "") {
        var item_id = $(this).attr('data-id');
        var row = $(this).attr('data-id-cart-row');
        var url = rota_update_qty;
        url = url.replace(':id_cart', row);
        url = url.replace(':qty', qty);
        $('.loding_cart').fadeIn();
        request2 = $.ajax({
            url         : url,
            type        : 'get',
            success:function(data)
            {

                $('.cart_section').empty();
                $('.cart_section').append(data);
                $('.loding_cart').fadeOut();

                $('[data-toggle="tooltip"]').tooltip();

            }
        });
    }
});

var request3;
$(document).on('change keyup','.qtd_pack_final',function(){

    if (request3) {
        request3.abort();
    }
    var qty = $(this).val();
    var row = $(this).attr('data-id-cart-row');

    var sanitized = $(this).val().replace(/[^0-9]/g, '');
    if(sanitized == 0){
        $(this).val('');
        sanitized = '';
    }
    $(this).val(sanitized);
    if (sanitized != "") {
        var item_id = $(this).attr('data-id');
        var row = $(this).attr('data-id-cart-row');
        var url = rota_update_pack_qty;
        url = url.replace(':id_cart', row);
        url = url.replace(':qty', qty);
        $('.loding_cart').fadeIn();
        request3 = $.ajax({
            url         : url,
            type        : 'get',
            success:function(data)
            {

                $('.cart_section').empty();
                $('.cart_section').append(data);
                $('.loding_cart').fadeOut();

                $('[data-toggle="tooltip"]').tooltip();

            }
        });
    }
});

var request4;
$(document).on('change keyup','.qtd_pack_variant_final',function(){

    if (request4) {
        request4.abort();
    }
    var qty = $(this).val();
    var row = $(this).attr('data-id-cart-row');

    var sanitized = $(this).val().replace(/[^0-9]/g, '');
    if(sanitized == 0){
        $(this).val('');
        sanitized = '';
    }
    $(this).val(sanitized);
    if (sanitized != "") {
        var item_id = $(this).attr('data-id');
        var row = $(this).attr('data-id-cart-row');
        var url = rota_update_pack_variant_qty;
        url = url.replace(':id_cart', row);
        url = url.replace(':id_variant', item_id);
        url = url.replace(':qty', qty);
        $('.loding_cart').fadeIn();
        request4 = $.ajax({
            url         : url,
            type        : 'get',
            success:function(data)
            {

                $('.cart_section').empty();
                $('.cart_section').append(data);
                $('.loding_cart').fadeOut();

                $('[data-toggle="tooltip"]').tooltip();

            }
        });
    }
});


$(document).on('click','.mais',function(){
    var inicial = parseInt($(this).parent().prev().val());
    var final = inicial + 1;
    $(this).parent().prev().val(final).trigger('change');
});

$(document).on('click','.menos',function(){
    var inicial = parseInt($(this).parent().prev().val());
    var min = parseInt($(this).parent().prev().attr('min'));
    var final = 1;
    if (inicial > min) {
        final = inicial - 1;
        $(this).parent().prev().val(final).trigger('change');
    }
});
//# sourceMappingURL=frontend_cart.js.map
