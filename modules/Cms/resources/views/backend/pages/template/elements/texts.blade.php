<div class="row">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-titulo">
                <span class="titulo">
                    <i class="icon-alterar-permissoes-1 "></i> @lang($text->panel_name)
                </span>
            </div>
            <div class="panel-body">
                <!-- Nav tabs -->
                <ul class="tabs_brainy lang_tabs_t_{{$text->id}}" role="tablist">
                    @foreach($locales as $loc)
                        <li role="presentation" class=""><a class="text-uppercase" href="#{{$loc}}_t_{{$text->id}}"  role="tab" data-toggle="tab">{{$loc}}</a></li>
                    @endforeach
                </ul>
                <!-- Tab panes -->
                <div class="tab-content  lang_content_t_{{$text->id}}">
                    @foreach($locales as $loc)
                        <div role="tabpanel" class="tab-pane fade in  margem_20" id="{{$loc}}_t_{{$text->id}}">
                            <div class="form-group fom-style-hidden3 @if($errors->first('title') ) has-error @endif">
                                <label for="exampleInputEmail1">@lang('cms::backend.model.common.title')</label>
                                <input type="text" name="text[{{ $text->id }}][{{ $loc }}][title]" class="form-control " value="{{ $text->translateOrNew($loc)->title }}">
                                {!! $errors->first('title','<span class="validator_errors">:message</span>')!!}
                            </div>
                            <div class="form-group fom-style-hidden3 @if($errors->first('subtitle') ) has-error @endif">
                                <label for="exampleInputEmail1">@lang('cms::backend.model.common.subtitle')</label>
                                <input type="text" name="text[{{ $text->id }}][{{ $loc }}][subtitle]" class="form-control " value="{{ $text->translateOrNew($loc)->subtitle }}">
                                {!! $errors->first('subtitle','<span class="validator_errors">:message</span>')!!}
                            </div>
                            <div class="form-group fom-style-hidden3 @if($errors->first('description') ) has-error @endif">
                                <label for="exampleInputEmail1">@lang('cms::backend.model.common.description')</label>
                                <textarea name="text[{{ $text->id }}][{{ $loc }}][description]" id="text_editor_{{$text->id.$loc}}" rows="10" cols="80">
                                    @if(empty($text->translateOrNew($loc)->description))
                                        @lang('cms::backend.model.common.ckeditor-placeholder')
                                    @endif
                                    {{ $text->translateOrNew($loc)->description }}
                                </textarea>
                                {!! $errors->first('description','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
