<?php 

DaveJamesMiller\Breadcrumbs\Facade::register('auction_online', function($breadcrumbs)
{
    $breadcrumbs->push('Vendas Online', route('auctionsonline::backend::auction_online.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('auctions_online.index', function($breadcrumbs){
    $breadcrumbs->parent('auctions_online');
    $breadcrumbs->push('Vendas Online', route('auctionsonline::backend::auctions_online.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('auctions_online.edit', function($breadcrumbs, $auction_online){
    $breadcrumbs->parent('auctions_online.index');
    $breadcrumbs->push('Ver Dados Venda Online', route('auctionsonline::backend::auctions_online.edit', $auction_online->id));
});

DaveJamesMiller\Breadcrumbs\Facade::register('auctions_online_lots.edit', function($breadcrumbs, $lot){
    $breadcrumbs->parent('auctions_online.edit');
    $breadcrumbs->push('Ver Dados Lote', route('auctionsonline::backend::auctions_online_lots.edit', $lot->id));
});


// DaveJamesMiller\Breadcrumbs\Facade::register('auctions_online.edit', function($breadcrumbs){
//     $breadcrumbs->parent('profiles.index');
//     $breadcrumbs->push('Editar Perfil', route('auctionsonline::backend::auctions_online.edit'));
// });
