@extends('layouts.frontend.app')

@section('title', $lot->name )
@if($auction->auctions_resources->first())
@section('image', route('gallery::frontend::storage_image',[$auction->auctions_resources->first()->identifier_token]))
@endif
@section('description', $lot->description )

@section('module-scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDq6_UUr8WI2eiiKz_Ty3QWDUIJVLPVT-4"></script>
<script src="https://js.pusher.com/2.2/pusher.min.js"></script>
<script>
    var countDownDate = new Date("{{$auction->date}}").getTime();
    var addMinutes = "{{Carbon::parse($auction->date)}}";
   
    var x = setInterval(function(){
    }, 1000);
    (function(){
        var pusher = new Pusher('8851a16eee27dbc16f28',{
            cluster: 'eu',
            encrypted: false
        });
        var channel = pusher.subscribe('test');
        channel.bind('App\\Events\\UserHasBidding', function(data){
            $('#licitar').val(data['price']);
            $('#licitar_hidden:hidden').val(data['next_price']);
        });
    })();

    var is_authenticated = "{{auth()->check()}}";
    if(is_authenticated == "true")
    {
        $('#bidding').on('click', function(){
            var price_valid;
            $.ajax({
                type: 'post',
                url: "{{ route('cms::frontend::model.check_if_valid')}}",
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: {
                    lot_id: "{{$lot->id}}",
                    price: $('#licitar').val()
                },
                success: function(data){
                    price_valid = ""+data;
                }
            }).done(add_new_bidding);


            function add_new_bidding(){
                if(price_valid == "true"){
                    $.ajax({
                        type: 'post',
                        url: "{{route('cms::frontend::model.broadcasting-event')}}",
                        headers: {
                            'X-CSRF-TOKEN': "{{ csrf_token() }}"
                        },
                        data: {
                            lot_id: "{{$lot->id}}",
                            price: $('#licitar').val()
                        },
                        success:function(data){

                        }
                    });
                }else{
                    alert(price_valid);
                }
            }
        });
    }
    var lat = "{{$lot->latitude}}";
    var lng = "{{$lot->longitude}}";
    if(lat>-90 && lat<90 && lng>-180 && lng<180){
                var latlng = new google.maps.LatLng(lat,lng); // Want to pass an address here
                var myOptions = {
                    zoom: 15,
                    scrollwheel: false,
                    center: latlng,
                    streetViewControl: false,
                    mapTypeControl: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,

                    styles: [
                    {
                        "featureType": "water",
                        "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "color": "#b5cbe4"
                        }
                        ]
                    },
                    {
                        "featureType": "landscape",
                        "stylers": [
                        {
                            "color": "#efefef"
                        }
                        ]
                    },
                    {
                        "featureType": "road.highway",
                        "elementType": "geometry",
                        "stylers": [
                        {
                            "color": "#83a5b0"
                        }
                        ]
                    },
                    {
                        "featureType": "road.arterial",
                        "elementType": "geometry",
                        "stylers": [
                        {
                            "color": "#bdcdd3"
                        }
                        ]
                    },
                    {
                        "featureType": "road.local",
                        "elementType": "geometry",
                        "stylers": [
                        {
                            "color": "#ffffff"
                        }
                        ]
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "geometry",
                        "stylers": [
                        {
                            "color": "#e3eed3"
                        }
                        ]
                    },
                    {
                        "featureType": "administrative",
                        "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": 33
                        }
                        ]
                    },
                    {
                        "featureType": "road"
                    },
                    {
                        "featureType": "poi.park",
                        "elementType": "labels",
                        "stylers": [
                        {
                            "visibility": "on"
                        },
                        {
                            "lightness": 20
                        }
                        ]
                    },
                    {},
                    {
                        "featureType": "road",
                        "stylers": [
                        {
                            "lightness": 20
                        }
                        ]
                    }
                    ]
                };
                var map = new google.maps.Map(document.getElementById("map"),
                    myOptions);

                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(lat, lng),
                    draggable: false,
                    optimized: false,
                    map: map
                });
            }else{
                $('#map').remove();
                $('.leilao-visitas').css('margin-top', 0);
            }

            var slider =  $('.prod-slider');
            if(slider.length > 0){       
/*        var inputImages = [];
            var ref = "{{ $auction->ref_intern}}";
            $('.prod-slide').each(function(){
                var bg = $(this).css('background-image');
                bg = bg.replace('url(','').replace(')','').replace(/\"/gi, "");

                inputImages.push(bg);

            });*/

            function initSlick(){
                slider.slick({
                    dots: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    prevArrow: '<div class="c_slide prev_slide"><</div>',
                    nextArrow:'<div class="c_slide next_slide">></div>',
                });
                slider.css('height', 'auto');
            }


            
            initSlick();


            
          /*  var outputImages = [];

            var defer = $.Deferred();
            $.when(defer).done(function () {


                for(i=0;i <= $('.prod-slide').length;i++){
                    $(".prod-slide:eq("+i+")").css('background-image', "url("+outputImages[i]+")");
                    //$(".slick-cloned:eq("+i+")").css('background-image', "url("+outputImages[i]+")");
                }
                //console.log(outputImages);
                initSlick();
                
            });

            $.each(inputImages, function (i, v) {
                $('<img>', {
                    src: v,
                   
                }).watermark({
                     text: ref,
                     gravity: 'c',
                     margin: 20,
                     textSize: 24,
                     textBg: 'rgba(0, 0, 0, 0.0)',
                     outputHeight: '500',
                     outputWidth: '650',        
                    done: function (imgURL) {

                        outputImages[i] = imgURL;
                        if (i + 1 === inputImages.length) {
                            defer.resolve();
                        }

                        
                    },
                    fail: function(){
                        //console.log("input: "+inputImages[i]);
                        //console.log("output: "+outputImages[i]);
                    }
                });
            });*/


        }

    </script>
    @endsection

    @section('content')

    <?php
    function formatMoney($number, $fractional=false) { 
        if ($fractional) { 
            $number = sprintf('%.2f', $number); 
        } 
        while (true) { 
            $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1,$2', $number); 
            if ($replaced != $number) { 
                $number = $replaced; 
            } else { 
                break; 
            } 
        } 
        return $number; 
    } 
    ?>

    <div class="mar-top-5">
        @include('cms::frontend.pages.layouts.search-bar')
    </div>

    <div class="leilao-body">
        <div class="leilao-top">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="prod-title">
                        <h3>
                            @if($lot->name == '')
                            N/A
                            @else
                            {{$lot->name}}
                            @endif
                        </h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 flex-col height-500">
                    <div class="leilao-top-top">
                        <div class="row_line top_r">
                            <span>{{ \Brainy\DefinitionsRent\Models\Category::find($auction->category_id)->name }}</span>
                            <span>Referência {{$auction->ref_intern}}</span>
                        </div>
                        <div class="row_line default_r">
                            <span>Valor Base</span>
                            <span>
                                @if($lot->price == '')
                                N/A
                                @else
                                {{ formatMoney( $lot->price/100, true)}}€
                                @endif

                            </span>
                        </div>
                        <div class="row_line default_r">
                            <span>Valor Abertura</span>
                            <span>
                                @if($lot->opening_price == '')
                                N/A
                                @else
                                {{ formatMoney( $lot->opening_price/100, true)}}€
                                @endif
                            </span>
                        </div>
                        <div class="row_line default_r">
                            <span>Valor Mínimo</span>
                            <span>
                                @if($lot->min_price == '')
                                N/A
                                @else
                                {{ formatMoney( $lot->min_price/100, true)}}€
                                @endif
                            </span>
                        </div>
                        <div class="row_line lance_r">
                            <span>Lance Atual</span>
                            <span>N/A</span>
                        </div>
                        @if(!auth()->check())
                        <div class="login-area">
                            Faça login para poder licitar.
                        </div>
                        @else
                        @if(\Carbon\Carbon::now()->gt(\Carbon\Carbon::parse($auction->date)->format('d-m-y H:i')))
                        <div class="login-area">
                            <input type="text" name="licitar" id="licitar" value="{{ formatMoney( $lot->opening_price/100, true)}}">
                            <button id="bidding" >Licitar</button>
                        </div>
                        @endif
                        @endif
                    </div>
                    <div class="leilao-top-bot">
                        <div class="row_line top_r">
                            <span>Tempo restante</span>
                            <span class="leilao-time">

                            </span>
                        </div>
                        <div class="row_line default_r">
                            <span>Relógio do Servidor</span>
                            
                                @if($auction->category_id == "4")
                                    <span id="relogio_leilao">
                                        
                                    </span>    
                                @else
                                    <span id="relogio_leilao">N/A</span>
                                @endif
                            </span>
                        </div>
                        <div class="row_line default_r">
                            <span>Atualização de Página</span>
                            <span>N/A</span>
                        </div>
                        <div class="row_line default_r">
                            <span>Início do Leilão</span>
                            @if($auction->date == '')
                            <span>{{$auction->date}}</span>
                            @else
                            <span>N/A</span>
                            @endif
                        </div>
                        <div class="row_line default_r">
                            <span>Fim do Leilão</span>
                            <span>N/A</span>
                        </div>
                    </div> 
                    <div class="leilao-top-bot">
                        <div class="row_line top_r">
                            <span>Data</span>
                            <span class="leilao-time">
                                @if($auction->date == '')
                                Sem Data
                                @else
                                {{ $auction->date }}
                                @endif
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                    @if($lot->lots_resources()->count() > 0)
                    <div class="prod-slider">

                        @foreach($lot->lots_resources()->cursor() as $lot_resource)
                        <?php
                        $startlink =asset('image_water.blade.php'); 
                        $url = $startlink."?img=".route('gallery::frontend::storage_image',[$lot_resource->identifier_token])."&ref=".$auction->ref_intern;
                        ?>   
                        <div class="cover prod-slide" style="background-image: url('{{ $url }}');">
                        </div>
                        @endforeach
                    </div>
                    @else
                    <div class="cover prod-slide" style="background-image: url('{{asset('front/imgs/lotes/placeholder.jpg')}}')">
                    </div>    
                    @endif
                    <!-- <div class="cover prod-slide" style="background-image: url('{{asset('front/imgs/prod/a2.jpeg')}}')">
                    </div>
                    <div class="cover prod-slide" style="background-image: url('{{asset('front/imgs/prod/a3.jpeg')}}')">
                    </div> -->



                </div>
            </div>
        </div>
        <div class="leilao-bot">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                    <div class="leilao-tipo">
                        <div class="row leilao-tipo-row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 half_div">
                                <div class="row_line top_r leilao-area new_style">
                                    <span class="b">Tipo:</span>
                                    <?php $sub_type_goods_id = $lot->type_goods()->pluck('sub_type_goods_id');
                                    $type_goods_id = \Brainy\DefinitionsRent\Models\SubTypeGoods::whereIn('id', $sub_type_goods_id)->pluck('type_good_id');

                                    ?>
                                    @foreach(\Brainy\DefinitionsRent\Models\TypeGoods::whereIn('id', $type_goods_id)->get() as $type_good)
                                    <span class="t_sp">

                                        {{ $type_good->name}}
                                        
      <!--                                   @foreach($lot->type_goods() as $type_good_aux)
                                            {{dd($type_good_aux) }}
                                            {{\Brainy\DefinitionsRent\Models\TypeGoods::where('id',$type_good_aux->type_goods_id)->first()->name}}
                                            
                                            @endforeach -->
                                        </span>
                                        @endforeach    
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 half_div">
                                    <div class="row_line top_r leilao-area new_style">
                                        <span class="b">Subtipo:</span>
                                        @foreach($lot->type_goods()->get() as $sub_type_good)
                                        <span class="t_sp">{{ $sub_type_good->name }}</span>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row leilao-tipo-row">
                            <div class="col-lg-4 half_div">
                                <div class="leilao-area">
                                    <div class="leilao-title type">
                                        Tipologia
                                    </div>
                                    <div class="leilao-text">
                                        <p>
                                            @if($lot->tipologia == '')
                                            N/A
                                            @else
                                            {{$lot->tipologia}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 half_div">
                                <div class="leilao-area">
                                    <div class="leilao-title type">
                                        Área privativa
                                    </div>
                                    <div class="leilao-text">
                                        <p>
                                            @if($lot->area_priv == '')
                                            N/A
                                            @else
                                            {{$lot->area_priv}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 half_div">
                                <div class="leilao-area">
                                    <div class="leilao-title type">
                                        Área Dependente
                                    </div>
                                    <div class="leilao-text">
                                        <p>
                                            @if($lot->area_dep == '')
                                            N/A
                                            @else
                                            {{$lot->area_dep}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4 half_div">
                                <div class="leilao-area">
                                    <div class="leilao-title type">
                                        Área Total
                                    </div>
                                    <div class="leilao-text">
                                        <p>
                                            @if($lot->area_total == '')
                                            N/A
                                            @else
                                            {{$lot->area_total}}
                                            @endif
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="leilao-area">
                            <div class="leilao-title">
                                descrição
                            </div>
                            <div class="leilao-text">
                                <p>
                                    @if($lot->description == '')
                                    N/A
                                    @else
                                    {{$lot->description}}
                                    @endif
                                </p>
                            </div>
                        </div>
                        <div class="leilao-area">
                            <div class="leilao-title">
                                Observações
                            </div>
                            <div class="leilao-text">
                                <p>@if($lot->comments == '')
                                    N/A
                                    @else
                                    {{$lot->comments}}
                                @endif</p>
                            </div>
                        </div>
                        <div class="leilao-area">
                            <div class="leilao-title">
                                Localização
                            </div>
                            <div class="leilao-text">
                                <p><b>Morada:</b> {{ $lot->address}}</p>
                                <p><b>Código Postal:</b> {{ $lot->cod_postal}}</p>
                                <p>
                                    <b>Distrito:</b> 
                                    {{ $lot->district }}
                                    <b>Concelho:</b>   
                                    {{$lot->county}}
                                    <b>Freguesia:</b>   
                                    {{$lot->parish}}
                                </p>    
                                <p><b>GPS Latitude:</b> {{$lot->latitude}}</p>
                                <p><b>GPS Longitude:</b> {{$lot->longitude}}</p>
                            </div>
                        </div>
                        <div class="leilao-area">
                            <div class="leilao-title">
                                Dados do processo
                            </div>
                            <div class="leilao-text">
                                <p><b>Designação:</b> {{$auction->executed}}  </p>
                                <p><b>Processo:</b> {{$auction->number_process}}</p>
                                <p><b>Tribunal:</b> {{$auction->tribunal}}</p>
                            </div>
                        </div>
                        @if($lot->field_extra)
                        <div class="leilao-area">
                            <div class="leilao-title">
                                {{ $lot->field_extra}}
                            </div>
                            <div class="leilao-text">
                                <p>{{$lot->another_field_extra}}</p>
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="leilao-sidebar">
                            <div id="map">
                                Sem localização.
                            </div>
                            <div class="leilao-visitas">
                                <div class="row_line lance_r">
                                    <span class="visitas">Visitas</span>
                                </div>
                                <div class="login-area">
                                    <!-- Faça login para consultar as suas visitas -->

                                    @if($lot->visits == '')
                                    Não existem horários de visita definidos
                                    @else
                                    {{$lot->visits}}
                                    @endif

                                </div>
                            </div>
                            <div class="leilao-icons">
                                <div class="row_line lance_r">
                                    <span class="visitas">Documentos</span>
                                </div>
                                <ul class="leilao-icons-list">

                                    @if($auction->advertisement)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->advertisement])  }}"><span class="icon-anuncio file-icon "></span><span>Anúncio</span></a></li>
                                    @endif

                                    @if($auction->minuta)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->minuta])  }}"><span class="icon-minuta file-icon"></span><span>Minuta</span></a></li>
                                    @endif

                                    @if($auction->conditions)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->conditions])  }}"><span class="icon-condicoes file-icon"></span><span>Condições</span></a></li>
                                    @endif

                                    @if($auction->catalog)
                                    <li><a target="_blank" href="{{ route('cms::frontend::storage_pdf',[$auction->catalog])  }}"><span class="icon-catalog file-icon"></span><span>Catálogo</span></a></li>
                                    @endif

                                </ul>
                            </div>
                            <div class="leilao-share">
                                <div class="prod-share">
                                    <div class="share-word">
                                        partilha
                                    </div>
                                    <div class="share-icons">
                                        <ul class="icons-list">
                                            <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ route('cms::frontend::model.lotes_detalhes', $lot->id) }}" target="_blank"><span class="icon-facebook"></span></a></li>
                                            <li><a href="https://plus.google.com/share?url={{ route('cms::frontend::model.lotes_detalhes', $lot->id) }}" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><span class="icon-google-plus"></span></a></li>
                                            <li><a href="http://twitter.com/share?text={{$auction->name}}&url={{ route('cms::frontend::model.lotes_detalhes', $lot->id) }}&hashtags=leilao,vias,rumos" target="_blank"><span class="icon-twitter"></span></a></li>
                                            <li><a href="mailto:?subject={{$auction->title}}&body={{$lot->description}}Link: {{ route('cms::frontend::model.lotes_detalhes', $lot->id) }} "><span class="icon-mail"></span></a></li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        @endsection

        
