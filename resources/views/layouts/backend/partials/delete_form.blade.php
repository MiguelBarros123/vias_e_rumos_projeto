<!-- --------------------------------------------------- MODAL DELETE RECORD ---------------------------------------------------- -->
<div class="modal modal-aux" id="delete_resource" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-aux" role="document">
        <form class="form_delete_order" method="POST" action="" >
              {!! csrf_field() !!}
            <input type="hidden" name="_method" value="DELETE">
            <input id="deleted_rows" name="deleted_rows" type="hidden" value="">            
            <div class="modal-content">
                <div class="panel-titulo"><span class="titulo"><i class="icon-opcoes-eliminar"></i> @lang("profiles::backend.common.delete")</span></div>
                <div class="modal-body">
                    <div class="col-md-12 padding-top-10"><p class="text-center"><i class="icon-eliminar-imagem icon40"></i></p></div>
                    <div class="col-md-12 possible_delete_message padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">@lang($delete_message)</p></div>
                    <div class="col-md-12 impossible_delete_message padding-top-20 padding-bottom-20 fonte_15_lato" style="display: none"><p class="text-center">@lang("profiles::backend.sales.common.impossible_delete")</p></div>
                </div>
                <div class="modal-footer modal-eliminar padding-bottom-30">
                    <button type="submit" class="btn btn-default btn-yellow">@lang("profiles::backend.common.delete")</button>
                    <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("profiles::backend.common.cancel") </button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- -------------------------------------------------MODAL DELETE RECORD  --------------------------------------------------- -->
