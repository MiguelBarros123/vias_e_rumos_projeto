@extends('layouts.frontend.app')

@section('title', 'frontend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/profiles/frontend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/profiles/frontend.js')}}"></script>
@endsection

@section('content')
<p>@lang('profiles::frontend.model.index.welcome')</p>
@endsection
