<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComposedHighlightTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('composed_highlight_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('c_highlight_id')->unsigned();

            $table->string('title');
            $table->string('subtitle');
            $table->text('description');
            $table->string('name_link');
            $table->string('link');
            $table->text('image_alt');


            $table->unique(['c_highlight_id','locale']);
            $table->foreign('c_highlight_id')->references('id')->on('c_highlights')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('composed_highlight_translations');
    }
}
