@extends('cms::frontend.emails.layouts.main')
@section('mail')

    <?php

    $hr = date("H");
    if($hr >= 12 && $hr<20) {
        $resp = trans('ecommerce::frontend.messages.boa_tarde',[],$lang);}
    else if ($hr >= 0 && $hr <12 ){
        $resp = trans('ecommerce::frontend.messages.bom_dia',[],$lang);}
    else {
        $resp = trans('ecommerce::frontend.messages.boa_noite',[],$lang);
    }

    ?>

    <div class="menu_topo_email">
        <table align="center">
            <tbody>
            <tr>
                <td>
                    <img src="{{URL::to(asset('back/icons/logo.png'))}}" alt="biblicallust"><br><br>
                </td>
            </tr>
            </tbody>
        </table>


        <table>
            <tbody>
            <tr>
                <td >
                    <p><b style="color: #000;">{{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico',[],$lang)}}</b></p>
                </td>
            </tr>
            </tbody>
        </table>

        <br>



        <table>
            <tbody>
            <tr>
                <td >
                    <p>{{$resp}}, {{$user->name}}!  {{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico2',[],$lang)}}</p>
                </td>
            </tr>
            </tbody>
        </table>


        <table>
            <tbody>
            <tr>
                <td >
                    <p>{{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico2',[],$lang)}}</p>
                </td>
            </tr>
            </tbody>
        </table>


        <table>
            <tbody>
            <tr>
                <td >
                    <p><b style="color: #000;">{{\Illuminate\Support\Facades\Lang::get('profiles::backend.messages.email.msg_account_email',[],$lang)}}</b> {{ $email }}</p>
                </td>
            </tr>
            </tbody>
        </table>


        <table>
            <tbody>
            <tr>
                <td >
                    <p><b style="color: #000;">{{\Illuminate\Support\Facades\Lang::get('profiles::backend.messages.email.msg_account_password',[],$lang)}}</b> {{ $password }}</p>
                </td>
            </tr>
            </tbody>
        </table>



    </div>

@stop