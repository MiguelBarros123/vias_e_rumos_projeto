<?php

return [
    // The label is used to represent the module's name
    'label' => 'seo::backend.module-label',

    // Permissions are organized by groups
    // The level 1 key denotes the group name (label)
    // The level 1 value is an associative array
    // The level 2 key denotes the permission name (label)
    // The level 2 value is an array with the routes that requires the permission
    'permissions' => [

        'seo::backend.permission.seo.manage' => [
            'seo::backend.permission.seo.manage' => [
                'seo::backend::model.index',
                'seo::backend::seo.ajax_all_pages',
                'seo::backend::seo.ajax_my_description',
                'seo::backend::seo.update_description'
            ],
        ],


        'seo::backend.permission.favicon.manage' => [
            'seo::backend.permission.favicon.manage' => [
                'seo::backend::model.index_favicon',
                'seo::backend::favicon.save_favicon'
            ],
        ],
    ],

    // Menu items denote the menus that this module renders
    'menu-items' => [
        [
            // Label for the menu item (mandatory)
            'label' => 'seo::backend.model.menu.seo',
            // Route for the action (mandatory if the menu has no childs)
            'route' => 'seo::backend::model.index',
            // Css class can be used to render the icon
            'css-class' => 'icon-icon-lateral-seo icon20',
            // Unique html id for the item
            'css-id' => null,
            // list of submenus
            'sub-menus' => [],
        ],
    ],
    // List of required providers for the module to work
    'providers' => [
        Brainy\Seo\Providers\SeoServiceProvider::class,
    ],

    // List of facades used by the module
    'aliases' => [
        // 'Recaptcha' => Greggilbert\Recaptcha\Facades\Recaptcha::class,
    ],

    // List of route middleware to register
    'middleware' => [
        //'acl' => \Brainy\Seo\Middleware\SomeMiddleware::class,
    ],

    'commands' => [
        // \Brainy\Seo\Commands\SomeCommand::class,
    ],

];
