@extends('layouts.frontend.app')

@section('content')
<div class="mar-top-5">
    @include('cms::frontend.pages.layouts.search-bar')
</div>
<div class="empresa-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <h2>O QUE SÃO COOKIES?</h2>
           

<p>Os cookies são pequenos ficheiros de texto gravados no seu equipamento ao aceder aos websites. Estes permitem o armazenamento de um conjunto de informações relacionadas com o utilizador, em que umas são usadas para reconhecer o utilizador em futuras visitas ao mesmo endereço web, e outras são usadas para garantir as funcionalidades disponibilizadas e a melhorar o desempenho e a experiência do utilizador.</p>

<p>O tipo de informação que pode ser analisada utilizando cookies é o seguinte: nome de utilizador, endereço de correio eletrónico e palavra-passe, o endereço de protocolo de internet (IP) que é utilizado para ligar á internet, o tipo e a versão de navegador, o sistema operativo e a plataforma utilizada para a ligação, os espaços dos nossos serviços online que tenha visualizado ou pesquisado, os endereços web (URLs) desde os quais ou através dos quais tenha acedido e deixado os nossos serviços online, a data e a hora da referida atividade, entre outros.</p>

<p>Os cookies dos websites poderão ser, a qualquer momento, desactivados ou geridos. Para saber como visite a seção de “Ajuda” do seu navegador de internet.</p>

<p>Alertamos para o facto de algumas áreas do website poderem não funcionar corretamente após a desativação dos cookies.</p>

 

<h2>QUAL A FINALIDADE DOS COOKIES?</h2>

<p>A utilização de cookies otimiza a navegação do utilizador, permitindo que os dados armazenados melhorem a sua experiência no website, aumentando também a eficiência de resposta do endereço web. Assim, permitem eliminar algumas ações repetitivas no website, bem como auxiliar na apresentação de mensagens publicitárias contidas no endereço web, para ir, o mais possível, de encontro aos seus interesses.</p>

 

<h2>TIPO DE COOKIES UTILIZADOS</h2>

<p>Os cookies são segmentados de acordo com a sua finalidade e data de validade.<p>

<p>Cookies de sessão - São temporários, os cookies são gerados e estão disponíveis até encerrar a sessão. Da próxima vez que o utilizador aceder ao seu navegador de internet (browser) os cookies já não estarão armazenados. A informação obtida permite gerir as sessões, identificar problemas e fornecer uma melhor experiência de navegação.</p>
<p>Cookies de confirmação - Este cookie serve apenas para garantir que o utilizador confirma a existência de cookies no site que está a visitar e que pretende continuar a navegar no site. Este cookie será gravado quando o utilizador clicar no botão "OK" na barra inferior de aceitação de cookies.</p>
<p>Cookies de Segurança - Utilizamos cookies de segurança para autenticar utilizadores, evitar a utilização fraudulenta de credenciais de início de sessão e proteger dados de pessoas autorizadas. Estes cookies permitem-nos bloquear muitos tipos de ataque, como tentativas de roubo do conteúdo de formulários que se preenche em páginas Web.
Pode consultar informação útil acerca dos cookies e de como gerir ou desativá-los no seguinte endereço: <a href="http://www.allaboutcookies.org" target="_blank">www.allaboutcookies.org</a></p>


        </div>
    </div>
</div>
@endsection
