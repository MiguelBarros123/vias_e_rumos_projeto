<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 23-05-2016
 * Time: 18:03
 */

namespace Brainy\Gallery\Models;

use Illuminate\Database\Eloquent\Model;


class MediaItemTranslation extends Model {


    public $timestamps = false;
    protected $fillable = ['title','description','locale','slug'];


}