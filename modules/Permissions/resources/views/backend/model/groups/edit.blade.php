@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
@endsection

@section('module-scripts')
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
    <script src="{{asset('back/js/datatable_select_logic.js')}}"></script>
    <!-- AngularJS -->
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
    <!-- Bootstrap Toggle -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <!-- ngBootstrapToggle -->
    <script src="{{ asset('angular/directives/toggler/toggler.directive.js') }}"></script>

    <script src="{{asset('js/permissions/backend.js')}}"></script>

    <script>
        var myTemplate_unpaid_table = "<tr id='sel_item_list2_unpaid_table_${id}'>" +
                "<td> ${name} <input type='hidden' name='ids[]' value='${id}' /></td> " +
                "<td>${job_position}</td>" +
                "<td>${email}</td>" +
                "<td><a id='delete_selected_item_datatable_list_unpaid_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
                "</tr>";
    </script>

    <script>

        $(document).ready(function(){

            var options = {
                rota: "{{ route('permissions::backend::users_all_groups_ajax', [$grupo->id]) }}",
                colunas: [
                    {data: 'name', name: 'name'},
                    {data: 'email', name: 'email'},
                    {data: 'grupo', name: 'grupo'},
                    {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                        //for already selected rows
                        $(nRow).attr('id', 'sel_row_'+aData['id']);

                        if( aData['already_selected']==1){
                            options.datatable.row(nRow).select();
                        }


                    },
                    initComplete: function(settings, json) {
                        $(function() {
                            $.logicDatatable(options);
                            $.selectDatatableLogic(options);

                        })
                    }


                },
                searchableElementId: 'ordered_search'
            };

            $('#unpaid_table').mydatatable(options);

            var options_two = {
                rota: "{{ route('permissions::backend::users_grupo_ajax', [$grupo->id]) }}",
                colunas: [
                    {data: 'name', name: 'name'},
                    {data: 'job_position', name: 'job_position'},
                    {data: 'email', name: 'email'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                        //for already selected rows
                        $(nRow).attr('id', 'sel_row_'+aData['id']);

                        if( aData['already_selected']==1){
                            options_two.datatable.row(nRow).select();
                        }


                    },
                    initComplete: function(settings, json) {
                        $(function() {
                            $.logicDatatable(options_two);
                            $.selectDatatableLogic(options_two);

                        })
                    }


                },
                searchableElementId: 'searchbox'
            };

            $('#table_groups').mydatatable(options_two);

            $(document).on('submit', '.form_add_users', function(event){
                event.preventDefault();
                var urlTo=$(this).attr('action');
                var data=$(this).serialize();

                $.ajax({
                    type:'get',
                    url:urlTo,
                    data:data,
                    success:function (data) {
                        options.datatable.ajax.reload();
                        options_two.datatable.ajax.reload();
                    }
                });

            });

            $('#select_resource_unpaid_table').on('shown.bs.modal', function(){

                options.datatable.columns.adjust().draw();

            });


        });


    </script>



@endsection

@section('content')

    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">

            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{route('permissions::backend::group.index')}}" class="icon_voltar "><i class="icon-voltar pading_right"></i>{{ ucfirst(trans('permissions::backend.return')) }}</a></li>
                    </ul>
                </div>
            </div>

            @include('layouts.backend.messages')

            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.groups.edit',$grupo) !!}

            <form method="post" action="{{route('permissions::backend::group.update',array($grupo->id))}}" >
                <input type="hidden" name="_method" value="PUT">
                {!! csrf_field() !!}

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo"><span class="titulo"><i class="icon-group icon20"></i> @lang('permissions::backend.name')</span></div>
                        <div class="panel-body">
                            <div class="form-group fom-style-hidden @if($errors->first('name') ) has-error @endif">
                                <label for ="nomeGrupo">{{ ucfirst(trans('permissions::backend.title')) }}</label>
                                <input id="nomeGrupo" name="name" type="text"
                                        class="form-control"
                                        placeholder="{{ ucfirst(trans('permissions::backend.name')) }}"
                                        value="{{ $grupo->name }}">
                                {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-12">

                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-permissoes icon20"></i>
                                @lang('permissions::backend.group.permissions')
                            </span>
                        </div>
                        <div class="panel-body">
                            <ul class="list-group panel treeview_permissions">
                                @foreach($modulos as $modulo)



                                    <li>
                                        <label class="checkbox-default  checkbox_permissions">
                                            <input type='checkbox'
                                                   class="check_perm" @if($modulo->authorizesGroup($grupo, true))
                                                   checked @endif>

                                            <span class="check_span"></span>
                                            <a href="#modulo_{{$modulo->name}}" class="list-group-item collapsed"
                                               data-toggle="collapse">
                                                @lang($modulo->label)
                                                @if(count($modulo->modules)>0)<i
                                                        class="icon-baixo"></i>@endif
                                            </a>
                                        </label>


                                        <ul class="collapse list-group-submenu" id="modulo_{{$modulo->name}}">
                                            @foreach(\Brainy\Framework\Facades\Brainy::containerModules($modulo->name) as $assunto)



                                                <li>
                                                    <label class="checkbox-default  checkbox_permissions">
                                                        <input type='checkbox'
                                                               class="check_perm" @if($assunto->authorizesGroup($grupo,true)== true)
                                                               checked @endif >

                                                        <span class="check_span"></span>
                                                        <a href="#assunto_{{$assunto->name}}"
                                                           class="list-group-item collapsed" data-toggle="collapse"
                                                           data-parent="#assunto_{{$assunto->name}}">
                                                            @lang($assunto->label)
                                                            @if(count($assunto->permissions)>0)<i
                                                                    class="icon-baixo"></i>@endif
                                                        </a>
                                                    </label>


                                                    <ul class="collapse list-group-submenu"
                                                        id="assunto_{{$assunto->name}}">
                                                        @foreach($assunto->permission_types as $database_permition)

                                                            <li class="perm_spacer">
                                                                <label class="checkbox-default  checkbox_permissions">
                                                                    <input class="check_perm" name="permissoes[]"
                                                                           value="{{$database_permition}}"
                                                                           type='checkbox' @if($grupo->hasPermission($database_permition))
                                                                           checked @endif>
                                                       <span class=""
                                                             data-parent="#SubMenu1">@lang($database_permition)</span>
                                                                </label>
                                                            </li>
                                                        @endforeach
                                                    </ul>

                                                </li>

                                            @endforeach
                                        </ul>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="panel">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20"></i>
                                {{ trans_choice('permissions::backend.users', 2) }}
                            </span>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                    <input class="search search_icon" type="text" id="searchbox" placeholder="@lang('permissions::backend.user.index.search')">
                                    <a class="add_btn" data-target="#select_resource_unpaid_table" data-toggle="modal"><i class="icon-adicionar icon10"></i>@lang('permissions::backend.group.edit.add-user')</a>
                                    <table id="table_groups" class="table default-table">
                                        <thead>
                                        <tr>
                                            <th>{{ ucfirst(trans('permissions::backend.name')) }}</th>
                                            <th>{{ ucfirst(trans('permissions::backend.user.model.position')) }}</th>
                                            <th>{{ ucfirst(trans('permissions::backend.user.model.email')) }}</th>
                                            <th></th>
                                            <th>
                                                <label class="checkbox-default">
                                                    <input type='checkbox' id="sel_all_perm">
                                                    <span></span>
                                                </label>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody id="initial_table_unpaid_table">

                                        </tbody>
                                    </table>
                                </div>
                            </div>


                        </div>
                    </div>

                    <button type="submit" class="btn btn-default btn-yellow">@lang('permissions::backend.save')</button>
                    <a href="{{route('permissions::backend::group.index')}}" class="btn btn-default btn-cancelar ">@lang('permissions::backend.cancel')</a>

                </div>



            </div>

            </form>
        </div>
    </div>


    <!-- --------------------------------------------------- MODAL DELETE GROUP ---------------------------------------------------- -->
    <div class="modal modal-aux" id="delete_group" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux" role="document">
            <form class="form_delete_order" method="POST" action="{{route('permissions::backend::group.destroy',[$grupo->id])}}" >
                <input type="hidden" name="_method" value="DELETE">
                                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-opcoes-eliminar"></i> @lang("ecommerce::backend.sales.common.delete")</span></div>
                    <div class="modal-body">

                        <div class="row">
                        <div class="col-md-12 padding-top-10"><p class="text-center"><i class="icon-eliminar-imagem icon40"></i></p></div>

                        @if($grupo->users()->count() > 0)
                            <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">@lang("permissions::backend.group.edit.no_delete")</p></div>
                            @else
                            <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">@lang("permissions::backend.group.edit.are_you_sure_group")</p></div>
                        @endif

                        </div>



                    </div>
                    @if($grupo->users()->count() == 0)
                        <div class="modal-footer modal-eliminar padding-bottom-30">
                        <button type="submit" class="btn btn-default btn-yellow">@lang("ecommerce::backend.sales.common.delete")</button>
                        <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("ecommerce::backend.sales.common.cancel") </button>
                    </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
    <!-- -------------------------------------------------MODAL DELETE GROUP  --------------------------------------------------- -->


    <!-- --------------------------------------------------- MODAL DELETE RECORD ---------------------------------------------------- -->
    <div class="modal modal-aux" id="delete_order" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux" role="document">
            <form class="form_delete_order" method="GET" action="{{route('permissions::backend::group_change_users_group_view')}}" >
                <input id="deleted_orders" name="deleted_orders" type="hidden" value="">
                <input  name="group_id" type="hidden" value="{{$grupo->id}}">

                                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-trocar-grupos"></i> @lang("permissions::backend.group.edit.change")</span></div>
                    <div class="modal-body">
                        <div class="col-md-12 padding-top-10"><p class="text-center"><i class="icon-eliminar-imagem icon40"></i></p></div>
                        <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">@lang("permissions::backend.group.edit.are_you_sure")</p></div>

                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30">
                        <button type="submit" class="btn btn-default btn-yellow">@lang("permissions::backend.change")</button>
                        <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("permissions::backend.cancel") </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- -------------------------------------------------MODAL DELETE RECORD  --------------------------------------------------- -->


    <!-- --------------------------------------------------- MULTIPLE SELECTION ---------------------------------------------------- -->
    <div class="action_multiple">

        <div class="wrap_action_multiple text-left">
            <a data-target="#delete_order" data-toggle="modal"><i class="icon-trocar-grupos icon15 pading_right"></i>@lang('permissions::backend.group.edit.change')</a>
        </div>
    </div>
    <!-- --------------------------------------------------- END MULTIPLE SELECTION ---------------------------------------------------- -->


    <!-- --------------------------------------------------- MODAL SELECT RECORD ---------------------------------------------------- -->
    <div class="modal modal-aux" id="select_resource_unpaid_table" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-lg modal-dialog-aux" role="document">      
            <div class="modal-content">
                <div class="panel-titulo"><span class="titulo"> {{ trans_choice('permissions::backend.users', 2) }}  </span></div>
                <div class="modal-body no_padding">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 ">
                            <input id="ordered_search" class="search search_icon" type="text"
                                   placeholder="@lang('definitionsecommerce::backend.common.seach') ">
                        </div>
                    </div>
                    <div class="row">
                        <div  class="col-lg-12 " >
                            <table id="unpaid_table" class="table default-table unpaid_table">
                                <thead>
                                <tr>
                                    <th>{{ ucfirst(trans('permissions::backend.name')) }}</th>
                                    <th>{{ ucfirst(trans('permissions::backend.user.model.email')) }}</th>
                                    <th>{{ ucfirst(trans('permissions::backend.user.model.group')) }}</th>
                                    <th>
                                        <label class="checkbox-default">
                                            <input type='checkbox' id="sel_all_perm_unpaid_table">
                                            <span></span>
                                        </label>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer modal-eliminar2 padding-bottom-30">
                    <form class="form_add_users" method="GET" action="{{ route('permissions::backend::add_new_user_to_group', [$grupo->id]) }}" >
                        {!! csrf_field() !!}
                        <input name="users_id" type="hidden" id="delete_records_unpaid_table">
                        <button type="submit" class="btn btn-default btn-yellow " id="save_selection_unpaid_table">@lang("cms::backend.model.common.save")</button>
                        <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("cms::backend.model.common.cancel") </button>
                    </form>
                </div>
            </div>

        </div>
    </div>
    <!-- -------------------------------------------------MODAL SELECT RECORD  --------------------------------------------------- -->



    

@endsection




