<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AlterarDadosPessoaisRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => 'required|old_password:'.auth()->user()->password,
            'new_password' => 'required|min:6',
            'new_password_confirmation' => 'required|same:new_password' 
        ];
    }

    public function messages()
    {
        return [
            'old_password.old_password' => 'A password não corresponde á sua password',
            'old_password.required' => 'Este campo é obrigatório',
            'new_password.required' => 'Este campo é obrigatório',
            'new_password.min' => 'Este campo não tem o mínimo de caracteres',
            'new_password_confirmation.required' => 'Este campo é obrigatório',
            'new_password_confirmation.same' => 'Passwords tem de ser iguais'
        ];
    }
}
