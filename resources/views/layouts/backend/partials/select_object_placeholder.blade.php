<select class="form-control {{$class}}" name="{{$name}}">
    <option></option>
    @foreach($array as $key => $a)
        <option value="{{ $a->id }}" {{ (($a->id) == $selected)? 'selected': ''}}>{{$a->name}}</option>
    @endforeach
</select>