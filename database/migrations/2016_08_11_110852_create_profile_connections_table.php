<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileConnectionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_connections', function (Blueprint $table) {
            $table->engine='InnoDB';
            $table->increments('id');
            $table->enum('connection',['pai','filho(a)','irmão(a)','mãe']);
            $table->integer('connection_id')->unsigned();
            $table->integer('profile_id')->unsigned();
            $table->foreign('profile_id')->references('id')->on('profiles');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_connections');
    }
}
