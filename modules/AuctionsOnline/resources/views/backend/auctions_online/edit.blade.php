
@extends('layouts.backend.app')
@inject('folders', 'Brainy\Gallery\Models\MediaFolder')

@section('title', 'Leilao Presencial')

@section('module-styles')
<link rel="stylesheet" href="{{asset('css/rent/backend.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('css/gallery/backend.css')}}">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"/> -->
@endsection

@section('module-scripts')
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/rowreorder/1.1.0/js/dataTables.rowReorder.min.js"></script>
<script src="{{asset('back/js/datatablesselect.js')}}"></script>
<script src="{{asset('back/js/datatables.js')}}"></script>
<script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
<script src="{{asset('back/js/spectrum.js')}}"></script>
<script src="{{asset('js/cms/backend.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-sanitize.js"></script>
<script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{ asset('angular/directives/toggler/toggler.directive.js') }}"></script>
<script src="{{asset('angular/directives/image_picker/multiple_image_picker.directive.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.ui.widget.js')}}"></script>
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="{{asset('js/cms/fileupload/jquery.iframe-transport.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-process.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-image.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-audio.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-video.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-validate.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

<script src="{{route('framework::backend::brainy', 'gallery')}}"></script>

<script>
    $('.lang_tabs li:first-child').tab('show');
    $('.lang_content').children().first().addClass('active');
</script>

<script>
     var label_content = '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Escolha um ficheiro</span>';

    'use strict';

;( function( $, window, document, undefined )
{
    $( '.inputfile' ).each( function()
    {
        var $input   = $( this ),
            $label   = $input.next( 'label' ),
            labelVal = $label.html();

        $input.on( 'change', function( e )
        {
            var fileName = '';

            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else if( e.target.value )
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName ){
                $('#reset_'+ $(this).attr("id")).removeClass('disabled');
                $label.find( 'span' ).html( fileName );
            }
            else{
                $label.html( labelVal );
            }

            console.log($input.val())
            if($input.val().length === 0){
                $('#reset_'+ $(this).attr("id")).addClass('disabled');
            }
        });

        // Firefox bug fix
        $input
        .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
        .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });
})( jQuery, window, document );

    var save_asset_url = "{{route('gallery::backend::save_asset')}}";
    var fetch_new_files = "{{route('gallery::backend::fetch_new_files')}}";
    var folders='{!! $folders::all()->toJson() !!}';
    var inicial='4';

    var rotaTypeGoods = "{{route('rent::backend::backend.rent.auctions.sub_type_of_goods')}}";

    $(document).ready(function () {

        var options = {
                rota: "{{ route('auctionsonline::backend::auctions_online.index_ajax_lots', $auction->id)}}",
                // rowReorder: {
                //     selector: 'i.icon-icon-peq-img-size'
                // },
                colunas: [
                    {data: 'designacao', name: 'designacao'},
                    {data: 'local', name: 'local'},
                    {data: 'distrito', name: 'distrito'},
                    {data: 'concelho', name: 'concelho'},
                    {data: 'freguesia', name: 'freguesia'},
                    {data: 'price', name: 'price'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    initComplete: function (settings, json) {
                        $(function () {
                            $.logicDatatable(options);

                        });
                    },
                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                        //for clicable rows
                        $(nRow).attr('data-rota-see', aData['rota_final']);
                        var tr2 = $(nRow).closest('tr');
                        tr2.find("td:first").addClass('details-control');
                        tr2.find("td:not(:first, :last, :nth-last-child(2))").addClass('clickable_url');
                    },

                },
                searchableElementId: 'search'
            };


            $('.default-table').mydatatable(options);

            $('.default-table').on('click', 'td.clickable_url', function () {
                var tr = $(this).closest('tr');
                window.location.href = tr.attr('data-rota-see');

            });

            options.datatable.on('row-reorder', function (e, diff, edit) {
                var elem = options.datatable.row(edit.triggerRow.selector.rows).data();
                var chosen = diff.find(function(element) { return options.datatable.row(element.node).data() === elem });
                var oldPosition = elem.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');

                for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                    if(diff[i].oldPosition === chosen.newPosition){
                        var rowData = options.datatable.row( diff[i].node ).data();
                        var newPosition = rowData.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');
                        $.ajax({
                            type: 'get',
                            url: '{{route('rent::backend::auctions.reorder')}}',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                id: elem.id,
                                fromPosition: oldPosition,
                                toPosition: newPosition
                            },
                            success: function (data) {
                                options.datatable.ajax.reload(null, false);
                            }
                        });
                    }
                }

            });

        $('.active2').bootstrapToggle();
        var array = [];
        @foreach($auction->lots()->get() as $key => $lot)
            @if($lot->type_goods()->count())
                array[{{$key}}] = "{{ base64_encode($lot->type_goods()->get()->toJson()) }}";
            @else
                array[{{$key}}] = "";
            @endif
        @endforeach

        for(var i = 0; i < $('.lote_section').find('.lots').length; i++)
        {
            var suggest=$('#tipos_bens_'+i).magicSuggest({
                placeholder: "",
                method: 'get',
                data: rotaTypeGoods,
                allowDuplicates: false,
                valueField: 'id'
            });

            if(atob(array[i]) != '')
            {

                suggest.setSelection(JSON.parse(atob(array[i])));
            }
            
        }

        

        // if(atob(type_essences) != ''){

        //     sugest.setSelection(JSON.parse(atob(type_essences)));

        // }



        // $('#date').daterangepicker({

        //     singleDatePicker: true,

        //     showDropdowns: true,

        //     startDate: moment()

        // });



        $('.lang_tabs li:first-child').tab('show');
        $('.lang_content').children().first().addClass('active');

        $('.embed_lang_tabs li:first-child').tab('show');
        $('.embed_lang_content').children().first().addClass('active');



        if (typeof angular != 'undefined') {
            angular.bootstrap('body', ['BootstrapToggler']);
        }

        var $injector =angular.injector(['ng','ImagePicker']);
        var html = document.getElementsByTagName('image-picker');

        $injector.invoke(function($compile, $rootScope){
            var $scope = $rootScope.$new();
            var result= $compile(html)($scope);
                var cScope = result.scope(); // This is scope in directive controller. :  )
                var myBtn = document.getElementById('ulpload-directive');
                myBtn.addEventListener('click', cScope.fetchNewFiles);
            });



        $(document).on('click','.add_btn', function(){
            if($(this).hasClass("add_btn_auction")){
                $('#btn_upload_image').attr('is-lote',"no");
            }else{
                $('#btn_upload_image').attr('is-lote',"yes");
                $('#btn_upload_image').attr('data-lote-id',$(this).attr('data-id'));
            }
        });



        $('#btn_upload_image').on('click', function () {
            var self = $('#btn_upload_image');
            var selected = $('div.item_galery.multiple_seletees.ui-selected');
            // var allLocations = '';
            // console.log(atob(selected_locations));
            // console.log(allLocations);
            if(selected.length!=0){
                $.each(selected,function (index,elem) {
                    var id=$(elem).parent().attr('data-media-item-id');
                    var bgUrl = $(elem).children().first().css('background-image');
                    bgUrl = /^url\((['"]?)(.*)\1\)$/.exec(bgUrl);
                    bgUrl = bgUrl ? bgUrl[2] : '';
                    // $.each(JSON.parse(atob(selected_locations)), function(key, value){
                    //     var id_location = value['id'];
                    //     var v = value['resource_location'];
                    //     allLocations +=
                    //     '<div class="row"><div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 padding-top-10"><label class="checkbox-default"><input class="use_checkbox" type="checkbox" data-is-page-section="1" value="'+id_location+'" name="image_in_location['+id+'][resource][]"/><span>'+v+'</span></label></div> </div>';
                    // });

                    /*if($('#image_rent_'+id+'').length==0){*/
                        var next_index=1;
                        if(self.attr('is-lote') == "no")
                        {
                            if($('#images_for_type').children().length > 0){
                                next_index=parseInt($('#images_for_type').children().last().find('.index_image').html())+1;
                            }

                            $('#images_for_type').append('<tr>'+
                                '<td class="vertical_midle "><i class="icon-icon-peq-img-size icon12"></i> <span class="index_image">'+next_index+'</span></td>'+
                                '<td class="vertical_midle"><img src="'+bgUrl+'" class="image_rent" id="image_rent_'+id+'"> <input type="hidden" name="image_in_location['+id+'][media_item_id]" value="'+id+'"><input class="index_position" type="hidden" name="image_in_location['+id+'][position]" value="'+next_index+'"> <input  type="hidden" name="image_in_location['+id+'][identifier_token]" value="" /></td>'+
                                '<td class="vertical_midle">'+
                                '<a class="delete_image"><i class="icon-opcoes-eliminar pading_right fonte_10_lato_regular"></i></a>'+
                                '</td>'+
                                '</tr>');
                        }
                        else{
                            var id_lote = self.attr('data-lote-id');
                            var lots_image = $('#images_for_type_lots_'+id_lote);
                             if(lots_image.children().length > 0){
                                next_index=parseInt(lots_image.children().last().find('.index_image').html())+1;
                            } 

                            lots_image.append('<tr>'+
                                '<td class="vertical_midle "><i class="icon-icon-peq-img-size icon12"></i> <span class="index_image">'+next_index+'</span></td>'+
                                '<td class="vertical_midle"><img src="'+bgUrl+'" class="image_rent" id="image_rent_'+id+'"> <input type="hidden" name="lots['+id_lote+'][image_in_location]['+id+'][media_item_id]" value="'+id+'"><input class="index_position" type="hidden" name="lots['+id_lote+'][image_in_location]['+id+'][position]" value="'+next_index+'"> <input  type="hidden" name="lots['+id_lote+'][image_in_location]['+id+'][identifier_token]" value="" /></td>'+
                                '<td class="vertical_midle">'+
                                '<a class="delete_image"><i class="icon-opcoes-eliminar pading_right fonte_10_lato_regular"></i></a>'+
                                '</td>'+
                                '</tr>');  
                        }
                        
                        /* }*/
                    });

                $('#upload_resource').modal('hide');
            }
        });



        var fixHelperModified = function(e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function(index) {
                $(this).width($originals.eq(index).width())
            });
            return $helper;
        },
        updateIndex = function(e, ui) {
            console.log(ui.item.parent());
            $('.index_image', ui.item.parent()).each(function (i) {
                $(this).html(i + 1);
            });
            
            $('.index_position', ui.item.parent()).each(function (i) {
                $(this).val(i + 1);
            });
        };

        $("#images_for_type").sortable({
            helper: fixHelperModified,
            stop: updateIndex,
            cursor: "move",
            update: function() {
            }
        });

        $(document).on('click','.delete_image',function () {
            alert('1');
            $(this).parent().parent().remove();
            $('.index_image').each(function (i) {
                $(this).html(i + 1);
            });
        });

        $('[name=district]').on('change', function(){
            $.ajax({
                type: 'get',
                url: "{{ route('rent::backend::backend.rent.get_counties')}}",
                data: {
                    id: $(this).val()
                },
                success: function(data)
                {
                    $('#county').html(data);
                    $('#parish select').html('');
                }
            });
        });

        $(document).on('change','.district_lots', function(){
            var self = $(this);
            $.ajax({
                type: 'get',
                url: "{{ route('rent::backend::backend.rent.get_counties')}}",
                data: {
                    id: $(this).val()
                },
                success: function(data)
                {
                    var element = self.parent().parent().parent().next();
                    console.log(element);
                    element.children().children().next().children().html(data);
                    element.next().children().children().next().children().html('');
                }
            });
        });



        $(document).on('change','.county_lots', function(){
            var self = $(this);
            $.ajax({
                type: 'get',
                url: "{{ route('rent::backend::backend.rent.get_parishs')}}",
                data: {
                    id: $(this).val()
                },
                success: function(data)
                {
                    self.parent().parent().parent().next().children().children().next().children().html(data);
                }
            });
        });

        $('.more_lots').on('click', function(){
            $.ajax({
                type: 'get',
                url: "{{ route('rent::backend::backend.rent.extra_lot', $auction->category_id) }}",
                data: '',
                success: function(data){
                    $('.lote_section').append('<hr>');
                    var last_lot_aux = $('.lote_section').find('.lots').last();
                    var id_next_lot = parseInt($('.lote_section').find('.lots').last().attr('data-id-lots'))+ 1;
                    var last_lot_aux = $('.lote_section').find('.lots').last();
                    var last_lot_id_aux  = $('.lote_section').find('.lots').last().attr('data-id-lots');
                    var name = last_lot_aux.find('#name_lots_'+last_lot_id_aux).val();
                    var address = last_lot_aux.find('#address_lots_'+last_lot_id_aux).val();
                    var local = last_lot_aux.find('#locals_lots_'+last_lot_id_aux).val();
                    var latitude = last_lot_aux.find('#latitude_lots_'+last_lot_id_aux).val();
                    var longitude = last_lot_aux.find('#longitude_lots_'+last_lot_id_aux).val();
                    var visits = last_lot_aux.find('#visits_lots_'+last_lot_id_aux).val();
                    var cod_postal = last_lot_aux.find('#cod_postal_lots_'+last_lot_id_aux).val();
                    $('.lote_section').append(data);
                    var last_lot = $('.lote_section').find('.lots').last();
                    last_lot.attr('data-id-lots',id_next_lot);
                    last_lot.find('#id_lots_0').attr('id', 'id_lots_'+id_next_lot).attr('name', 'lots['+id_next_lot+'][id]');
                    last_lot.find('#name_lots_0').attr('id','name_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][name_lots]').val(name);
                    last_lot.find('#tipos_bens_0').attr('id','tipos_bens_'+id_next_lot).attr('name','lots['+id_next_lot+'][tipos_bens]');
                    last_lot.find('#price_lots_0').attr('id','price_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][price_lots]');
                    last_lot.find('#min_price_lots_0').attr('id','min_price_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][min_price_lots]');
                    last_lot.find('.district_lots').attr('name','lots['+id_next_lot+'][district_lots]');
                    last_lot.find('.county_lots').attr('name','lots['+id_next_lot+'][county_lots]');
                    last_lot.find('.parish_lots').attr('name','lots['+id_next_lot+'][parish_lots]');
                    last_lot.find('#address_lots_0').attr('id','address_lot_'+id_next_lot).attr('name','lots['+id_next_lot+'][address_lots]').val(address);
                    last_lot.find('#locals_lots_0').attr('id','locals_lot_'+id_next_lot).attr('name','lots['+id_next_lot+'][locals_lots]').val(local);
                    last_lot.find('#cod_postal_lots_0').attr('id','cod_postal_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][cod_postal_lots]').val(cod_postal);
                    last_lot.find('#latitude_lots_0').attr('id','latitude_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][latitude_lots]').val(latitude);
                    last_lot.find('#longitude_lots_0').attr('id','longitude_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][longitude_lots]').val(longitude);
                    last_lot.find('#tipologia_lots_0').attr('id','tipologia_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][tipologia_lots]');
                    last_lot.find('#area_priv_lots_0').attr('id','area_priv_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][area_priv_lots]');
                    last_lot.find('#area_dep_lots_0').attr('id','area_dep_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][area_dep_lots]');
                    last_lot.find('#area_total_lots_0').attr('id','area_total_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][area_total_lots]');
                    last_lot.find('#description_lots_0').attr('id','description_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][description_lots]');
                    last_lot.find('#comments_lots_0').attr('id','comments_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][comments_lots]');
                    last_lot.find('#field_extra_lots_0').attr('id','field_extra_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][field_extra_lots]');
                    last_lot.find('#another_field_extra_lots_0').attr('id','another_field_extra_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][another_field_extra_lots]');
                    last_lot.find('#visits_lots_0').attr('id','visits_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][visits_lots]').val(visits);
                    last_lot.find('.add_btn_lots').attr('data-id',id_next_lot);
                    last_lot.find('#images_for_type_lots_0').attr('id','images_for_type_lots_'+id_next_lot).attr('name','lots['+id_next_lot+'][images_for_type_lots]');

                    var sugest=$('#tipos_bens_'+id_next_lot).magicSuggest({
                        placeholder: "",
                        method: 'get',
                        data: rotaTypeGoods,
                        allowDuplicates: false,
                        valueField: 'id'
                    });
                }
            })
        });

        



        $(document).on('change','.county', function(){
            $.ajax({
                type: 'get',
                url: "{{ route('rent::backend::backend.rent.get_parishs')}}",
                data: {
                    id: $(this).val()
                },
                success: function(data)
                {
                    $('#parish').html(data);
                }
            });
        });


        $('#range').daterangepicker({
            "timePicker": true,
            "singleDatePicker": true,
            "showDropdowns": true,
            "sideBySide": true,
            "timePicker24Hour": true,
            autoUpdateInput: false,
            "locale": {
                "format": 'DD/MM/YYYY HH:mm',
                "cancelLabel": 'Limpar',
                "applyLabel": 'Aplicar'
            },
            "startDate": "{{\Carbon\Carbon::parse($auction->date)->format('d/m/Y H:i')}}",
        }, function (start, end, label) {
            $('#range span').html(start.format('DD/MM/YYYY HH:mm'));
        });

        $('#range_limit').daterangepicker({
            "timePicker": true,
            "singleDatePicker": true,
            "showDropdowns": true,
            "sideBySide": true,
            "timePicker24Hour": true,
            autoUpdateInput: false,
            "locale": {
                "format": 'DD/MM/YYYY HH:mm',
                "cancelLabel": 'Limpar',
                "applyLabel": 'Aplicar'
            },
            "startDate": "{{\Carbon\Carbon::parse($auction->date_limit)->format('d/m/Y H:i')}}",
        }, function (start, end, label) {
            $('#range span').html(start.format('DD/MM/YYYY HH:mm'));
        });    

        $('.reset_btn').on('click', function(){
            var label = $('label[for="'+$(this).attr('thefile')+'"]');
            $(this).addClass('disabled');
            label.html(label_content);
            var inp = $('#'+$(this).attr('thefile'));
            inp.val("");
            $(this).blur();
        });


        var date_value = "{{$auction->date}}";
        var date_limit_value = "{{$auction->date_limit}}";

        if(date_value){
            $('#range').val(date_value);
        }

        if(date_limit_value){
            $('#range_limit').val(date_limit_value);
        }

        $('#range').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY-MM-DD hh:mm:ss'));
        });

        $('#range').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
        });

        $('#range_limit').on('apply.daterangepicker', function(ev, picker) {
          $(this).val(picker.startDate.format('YYYY-MM-DD hh:mm:ss'));
        });

        $('#range_limit').on('cancel.daterangepicker', function(ev, picker) {
          $(this).val('');
        });

        

    });

</script>    

<script src="{{asset('back/js/upload_files.js')}}"></script>





<script>

    CKEDITOR.replace( 'description');

</script>


@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
    <div class="container-fluid">
        <div class="row tabs-back">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                <ul class="list-inline">
                    <li><a href="{{route('auctionsonline::backend::auctions_online.index')}}"
                     class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                 </ul>
             </div>
         </div>
         @include('layouts.backend.messages')
         {!! DaveJamesMiller\Breadcrumbs\Facade::render('auctions_online.edit', $auction) !!}
         <div class="row">
            <form method="POST" action="{{ route('rent::backend::backend.rent.auctions.update', $auction->id)}}" autocomplete="off" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                {{ method_field('PUT') }}
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20">
                                </i>
                                Leilão Online
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">
                                    <div class="form-group @if($errors->has('title')) has-error @endif">
                                        <label for="title-field">@lang('rent::backend.common.title')</label>
                                        <div>{{$auction->title}}</div>
                                    </div>
                                    <div class="form-group @if($errors->has('description')) has-error @endif">
                                        <label for="description-field">@lang('rent::backend.common.description')</label>
                                        <div>{!! $auction->description !!}</div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="category-field">@lang('rent::backend.common.district')</label>
                                                <div>{{$auction->district}}</div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="category-field">@lang('rent::backend.common.county')</label>
                                                <div>{{$auction->county}}</div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="category-field">@lang('rent::backend.common.parish')</label>
                                                <div>{{$auction->parish}}</div>
                                            </div>
                                        </div>

                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('local')) has-error @endif">
                                                <label for="local-field">@lang('rent::backend.common.local')</label>
                                                <div>{{ $auction->local }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('address')) has-error @endif">
                                                <label for="address-field">@lang('rent::backend.common.address')</label>
                                                <div>{{ $auction->address }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('cod_postal')) has-error @endif">
                                                <label for="cod_postal-field">@lang('rent::backend.common.cod_postal')</label>
                                                <div>{{ $auction->cod_postal }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('latitude')) has-error @endif">
                                                <label for="latitude-field">@lang('rent::backend.common.latitude')</label>
                                                <div>{{ $auction->latitude }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('longitude')) has-error @endif">
                                                <label for="longitude-field">@lang('rent::backend.common.longitude')</label>
                                                <div>{{ $auction->longitude }}</div>
                                            </div> 
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('ref_intern')) has-error @endif">
                                                <label for="ref_intern-field">@lang('rent::backend.common.ref_intern')</label>
                                                <div>{{ $auction->ref_intern }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('number_process')) has-error @endif">
                                                <label for="number_process-field">@lang('rent::backend.common.number_process')</label>
                                                <div>{{ $auction->number_process }}</div>
                                            </div> 
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="date-field">Data de Abertura: </label>
                                                <div>{{\Carbon\Carbon::parse($auction->date)->format('d/m/Y H:i')}}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <label for="date-field">Data de Rececção: </label>
                                                <div>{{\Carbon\Carbon::parse($auction->date_limit)->format('d/m/Y H:i')}}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group @if($errors->has('admin_insolv')) has-error @endif">
                                                <label for="admin_insolv-field">@lang('rent::backend.common.admin_insolv')</label>
                                                <div>{{ $auction->admin_insolvency }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('court')) has-error @endif">
                                                <label for="court-field">@lang('rent::backend.common.court')</label>
                                                <div>{{ $auction->tribunal }}</div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group @if($errors->has('executed')) has-error @endif">
                                                <label for="executed-field">@lang('rent::backend.common.executed')</label>
                                                <div>{{$auction->executed}}</div>
                                            </div>
                                        </div>
                                    </div>        
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group fom-style-hidden2 no_padding">
                                                <label for="active">@lang('rent::backend.common.featured')</label>
                                                <div class="form-group fom-style-hidden2">
                                                    <span>não</span>
                                                    <input class='active2' type='checkbox' data-on=' ' data-off=' ' name='featured' data-style='ios new_toggle' {{$auction->featured ? 'checked' : ''}} disabled>
                                                    <span>sim</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group fom-style-hidden2 no_padding">
                                                <label for="active">@lang('rent::backend.common.active')</label>
                                                <div class="form-group fom-style-hidden2">
                                                    <span>não</span>
                                                    <input class='active2' type='checkbox' data-on=' ' data-off=' ' name='active' data-style='ios new_toggle' {{$auction->active ? 'checked' : ''}} disabled>
                                                    <span>sim</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <span class="title_file">Ficheiro para o <b>@lang('rent::backend.common.catalog')</b>.</span>
                                            <div>{{$auction->catalog}}</div>
                                        </div>
                                        <div class="col-lg-3">
                                            <span class="title_file">Ficheiro para o <b>@lang('rent::backend.common.advertisement')</b>.</span>
                                            <div>{{$auction->advertisement}}</div>
                                        </div>
                                        <div class="col-lg-3">
                                            <span class="title_file">Ficheiro para a <b>@lang('rent::backend.common.minuta')</b>.</span>
                                            <div>{{$auction->minuta}}</div>
                                        </div>

                                        <div class="col-lg-3">
                                            <span class="title_file">Ficheiro para as <b>@lang('rent::backend.common.conditions')</b>.</span>
                                            <div>{{$auction->conditions}}</div>
                                        </div>

                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo">Listagem de Lotes</span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                    <table id="unpaid_table" class="table default-table table-hover">
                                        <thead>
                                            <tr>
                                                <!-- <th></th> -->
                                                <th>Designação</th>
                                                <th>Local</th>
                                                <th>Distrito</th>
                                                <th>Concelho</th>
                                                <th>Freguesia</th>
                                                <th>Price</th>
                                                <th class="multiple-options">
                                                    @include('layouts.backend.partials.delete_datatables',['id'=>'unpaid_table','route'=>'rent::backend::backend.rent.auctions.delete'])
                                                </th>
                                                <th>
                                                    <label class="checkbox-default">
                                                        <input type='checkbox' id="sel_all_perm_unpaid_table">
                                                        <span></span>
                                                    </label>
                                                </th> 
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-lg-12">
                <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('rent::backend.common.save')</button>
                <button type="submit" name="submit" value="stay" class="btn btn-default btn-yellow btn-stay btn-submit-all">@lang('rent::backend.common.save_and_stay')</button>
                <a class="btn btn-default btn-cancelar" href="{{route('auctionsonline::backend::auctions_online.index')}}">@lang('rent::backend.common.cancel')</a>
            </div>
        </form>
    </div>
</div>
</div>

@include('cms::backend.pages.template.elements.upload_files')

@endsection



