<?php

Route::group(['namespace' => 'Brainy\Permissions\Controllers\Frontend'], function () {

    Route::get('/', 'ModelFrontendController@index')->name('model.index');

});
