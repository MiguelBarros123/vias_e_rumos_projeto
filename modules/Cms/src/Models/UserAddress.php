<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:30
 */

namespace Brainy\Cms\Models;
use Illuminate\Database\Eloquent\Model;

class UserAddress extends Model
{
    protected $fillable = ['name', 'last_name', 'address', 'nif', 'zipcode', 'company', 'town', 'phone', 'city', 'country', 'invoice_address', 'active_address'];

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'id');
    }
}
