@extends('layouts.backend.app')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/profiles/backend.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/backend.css') }}">
@endsection

@section('module-scripts')



@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <a class="pull-right icon_eliminar icon-editar pading_right icon_delete_inside_view_show"
                       href="{{ route('profiles::backend::profiles.edit', [$profile->id, 'company']) }}">
                        Editar
                    </a>
                    <ul class="list-inline">
                        <li><a href="{{route('profiles::backend::companies.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.companies.show',$profile) !!}
            <div class="row">
                    <div class="col-lg-12">
                        <div class="panel ">
                            <div class="panel-titulo">
                                <span class="titulo"><i class="icon-ficha icon10"></i>@lang('profiles::backend.common.contact_sheet')</span>
                            </div>
                            <div class="panel-body">
                                <div class="row ">

                                    <div class="col-lg-6 profile_divider">
                                        <div class="row">
                                            <div id="user-image" class="col-lg-4 text-center">
                                                @if($profile->thumbnail!= null)
                                                    <img id="preview_file" class="pic-circle-corner2" src="{{route('gallery::frontend::storage_image_p',[$profile->thumbnail,90,90])}}"  >
                                                @else
                                                    <div class="pic-circle-corner text-center"><i class="icon-avatar-person icon80 "></i></div>
                                                @endif
                                            </div>
                                            <input id="fileupload" type="file" name="profile_image">
                                            <div class="col-lg-8" >
                                                <div class="row">
                                                    <div class="col-lg-12 profile_name">

                                                        <div class="profile_name_18">{{$profile->name}}, {{ ($profile->profile_addresses()->count()) ? $profile->profile_addresses()->first()->city : '' }}</div>

                                                    </div>
                                                    <div class="col-lg-12">
                                                        <div class="form-group fom-style-hidden3 profile_contacts @if($errors->first('surname') ) has-error @endif">

                                                            <div><span class="profile_input">@lang('profiles::backend.common.nif')</span> {{$profile->nif}}</div>

                                                            @if($email)
                                                                <div><i class="icon-mail profile_input"></i> {{$email->contact}}</div>
                                                            @endif

                                                            @if($phone)
                                                                <div><i class="icon-telefone profile_input"></i> {{$phone->contact}}</div>
                                                            @endif

                                                            @if($mobile)
                                                                <div><i class="icon-telefone profile_input"></i> {{$mobile->contact}}</div>
                                                            @endif

                                                            @if($fax)
                                                                <div><i class="icon-fax profile_input"></i> {{$fax->contact}}</div>
                                                            @endif

                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                                                <label class="profile_input"><i class="icon-redes-sociais"></i> @lang('profiles::backend.common.social_networks')</label>
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">

                                                @if($facebook)
                                                    <a href="{{ $facebook->contact }}" target="_blank">
                                                        <i class="icon-face social-profile social-facebook"></i>
                                                    </a>
                                                @endif

                                                @if($instagram)
                                                    <a href="{{ $instagram->contact }}" target="_blank">
                                                        <i class="icon-instagram-1 social-profile social-instagram"></i>
                                                    </a>
                                                @endif

                                                @if($google)
                                                    <a href="{{ $google->contact }}" target="_blank">
                                                        <i class="icon-google-plus social-profile social-google"></i>
                                                    </a>
                                                @endif

                                                @if($youtube)
                                                    <a href="{{ $youtube->contact }}" target="_blank">
                                                        <i class="icon-youtube-1 social-profile social-youtube"></i>
                                                    </a>
                                                @endif

                                            </div>
                                        </div>

                                        @if(count($others))

                                            <div class="row padding-top-20">

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label class="profile_input"><i class="icon-redes-sociais"></i> @lang('profiles::backend.common.other')</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                                                    @foreach($others as $other)
                                                        <a href="{{ $other->contact }}" target="_blank">
                                                            {{ $other->contact }}
                                                        </a>
                                                    @endforeach

                                                </div>

                                            </div>

                                        @endif

                                    </div>

                                    <div class="col-lg-5 col-lg-offset-1">

                                        @if($profile->profile_addresses()->count())
                                            <div class="row padding-top-20">

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label class="profile_input"><i class="icon-morada"></i> @lang('profiles::backend.common.address')</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <div>{{ ($profile->profile_addresses()->count()) ? $profile->profile_addresses()->first()->address : '' }}</div>
                                                </div>

                                            </div>
                                        @endif

                                        <div class="row padding-top-20">

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <label class="profile_input"><i class="icon-idioma"></i> @lang('profiles::backend.common.language')</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <div>{{ $language }}</div>
                                            </div>

                                        </div>

                                            <div class="row">

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label class="profile_input"><i class="icon-links"></i> @lang('profiles::backend.common.url')</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <div>{{ $profile->url }}</div>
                                                </div>

                                            </div>

                                            <div class="row">

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label class="profile_input"><i class="icon-novo-utilizador-1"></i> @lang('profiles::backend.common.created_on')</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <div>{{ date('d-m-Y', strtotime($profile->created_at)) }}</div>
                                                </div>

                                            </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo"><i class="icon-links icon10"></i>@lang('profiles::backend.common.employees')</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <table class="table dataTable default-table2">
                                    <thead>
                                    <tr>
                                        <th>@lang('profiles::backend.menu.clients')</th>
                                        <th>@lang('profiles::backend.common.contacts')</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($profile->users->count())
                                        @foreach($profile->users as $user)
                                            @php
                                                $email = $user->profile_contacts()->where('type', 'personal_email')->first();
                                                $phone = $user->profile_contacts()->where('type', 'personal_phone')->first();
                                            @endphp
                                            <tr>
                                                <td>
                                                    <div class="company_user_img">
                                                        @if($user->thumbnail === '')
                                                            <div class="pic-circle-corner2 text-center profile_no_pic">
                                                                <i class="icon-avatar-person icon10 "></i>
                                                            </div>
                                                        @else
                                                            <img class="image_user_picked2" src="{{route('gallery::frontend::storage_image_p',[$user->thumbnail,50,50])}}">
                                                        @endif
                                                    </div>
                                                    <div class="company_user_info">
                                                        <div>{{ $user->name }} {{ $user->surname }}</div>
                                                        <div class="profile_input">{{ $user->job }}</div>
                                                    </div>
                                                </td>
                                                <td>
                                                   @if($email)
                                                        <i class="icon-mail"></i> {{ $email->contact }}
                                                   @endif
                                                </td>
                                                <td>
                                                    @if($phone)
                                                        <i class="icon-telefone"></i> {{ $phone->contact }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="4">
                                                @lang('profiles::backend.common.no_results')
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                {{--<div class="col-lg-6">--}}
                    {{--<div class="panel ">--}}
                        {{--<div class="panel-titulo">--}}
                            {{--<span class="titulo"><i class="icon-timeline icon10"></i>@lang('profiles::backend.common.timeline')</span>--}}
                        {{--</div>--}}
                        {{--<div class="panel-body">--}}
                            {{--<div class="row ">--}}

                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>



        </div>
    </div>

@endsection



