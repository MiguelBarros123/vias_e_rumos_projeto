<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class PageContact extends Model
{
    protected $fillable = ['address', 'lat', 'long'];
}