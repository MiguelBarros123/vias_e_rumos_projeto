@extends('cms::frontend.emails.layouts.main')
@section('content')



<container>

    <row>
        <columns>

            <spacer size="30"></spacer>

            <center>
                <img src="{{ asset('back/icons/logo.png') }}" alt="biblicallust">
            </center>

            <spacer size="30"></spacer>

        </columns>
    </row>

    <row>
        <columns></columns>
        <columns>
            <p class="text-center small-text-center"><b style="color: #54075b;">{{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico',[],$lang)}}</b></p>
        </columns>
        <columns></columns>
    </row>

    <row>
        <columns>
            <p>{{$user->getResponse($lang)}}, {{$user->name}}!</p>
            <p>{{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico2',[],$lang)}}</p>
            <spacer size="15"></spacer>
            <p>{{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico4',[],$lang)}}</p>
            <spacer size="15"></spacer>
            <p>&#8226; {{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico5',[],$lang)}}</p>
            <p>&#8226; {{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico6',[],$lang)}}</p>
            <p>&#8226; {{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico7',[],$lang)}}</p>
            <p>&#8226; {{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico8',[],$lang)}}</p>
            <p>&#8226; {{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico9',[],$lang)}}</p>
            <p>&#8226; {{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico10',[],$lang)}}</p>
            <spacer size="15"></spacer>
            <p>{{\Illuminate\Support\Facades\Lang::get('cms::frontend.messages.msg_email_automatico11',[],$lang)}}</p>
        </columns>
    </row>

    <spacer size="0" class="divider_line"></spacer>

    <row>
        <columns large="12" small="12">
            <p class="text-center small-text-center">{{\Illuminate\Support\Facades\Lang::get('ecommerce::frontend.messages.msg_email_automatico8',[],$lang)}} <b>www.biblicallust.com</b></p>
        </columns>
    </row>

</container>


@stop