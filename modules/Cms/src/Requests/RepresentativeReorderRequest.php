<?php

namespace Brainy\Cms\Requests;

use App\Http\Requests\Request;

class RepresentativeReorderRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
    	return [
            'id' => 'required|exists:representatives,id',
    		'fromPosition' => 'required|integer|min:0',
            'toPosition' => 'required|integer|min:0',
    	];    
    }

}