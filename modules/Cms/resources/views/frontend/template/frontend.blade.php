
@extends('layouts.frontend.app')

@inject('page', 'Brainy\Cms\Models\Page')

@php
$seo = $page->find(1);
@endphp

@section('title', $seo->title)
@section('description', $seo->meta_description)

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/<%= lc_module_name %>/frontend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/'. $lc_module_name .'/frontend.js')}}"></script>
@endsection
//