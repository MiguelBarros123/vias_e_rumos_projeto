<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:50
 */

namespace Brainy\Cms\Models;
use Illuminate\Database\Eloquent\Model;

class TextTranslation extends Model {
    public $timestamps = false;
    
    protected $fillable = ['title', 'subtitle', 'description'];
}