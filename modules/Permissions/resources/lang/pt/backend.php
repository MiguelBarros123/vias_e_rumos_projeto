<?php

return [
    'module_name' => 'Administração',
    'module-label'=>'Gestão de permissões',

    'name' => 'nome',
    'users' => 'utilizador|utilizadores',
    'groups' => 'grupo|grupos',
    'block' => 'bloqueado|bloqueados',
    'unblock' => 'desbloquado|desbloqueados',
    'block-unblock' => 'bloquear/desbloquear',
    'back' => 'anterior',
    'next' => 'seguinte',
    'return' => 'Voltar',
    'first' => 'primeiro',
    'last' => 'último',
    'search' => 'pesquisar',
    'find' => 'procurar',
    'new' => 'novo',
    'add' => 'adicionar',
    'create' => 'criar',
    'edit' => 'editar',
    'save' => 'guardar',
    'cancel' => 'cancelar',
    'remove' => 'remover',
    'retrieve' => 'obter',
    'update' => 'atualizar',
    'delete' => 'eliminar',
    'restore' => 'restaurar',
    'recover' => 'recuperar',
    'archive' => 'arquivar',
    'title' => 'título',
    'processing' => 'a processar',
    'entries' => 'entradas',
    'n-entries' => '{0} sem entradas|[1] 1 entrada|[2,+inf] entries',
    'n2n-of-total-entries' => ':x a :y de :n entradas',
    'filter-n-entries' => 'filtrado de :n entradas',
    'no-records' => 'não foram encontrados resultados',
    'removed' => 'removido',
    'deleted' => 'eliminado',
    'common' => [
        'group' => 'Grupo',
        'users' => 'Utilizadores',
        'permissions' => 'Permissões',
        'email' => 'Email',
        'sure_delete_user' => 'Tem a certeza que pretende eliminar o(s) utilizador(es) selecionado(s)?',
    ],
    'menu' => [
        'breadcrumb' => [
            'permissions' => 'Gestão de Permissões',
            'users' => 'Gestão de Utilizadores',
            'groups' => 'Gestão de Grupos',
            'archived-users' => 'Gestão de Arquivo de Utilizadores',
        ],
        'archived' => [
            'users' => 'Utilizador arquivado|Utilizadores arquivados',
            'groups' => 'Grupo arquivado|Grupos arquivados',
        ],
    ],
    'timestamps' => [
        'date' => [
            'created-at' => 'Criado a :date',
            'updated-at' => 'Atualizado a :date',
            'deleted-at' => 'Removido a :date',
        ],
        'created-at' => 'Criado a',
        'updated-at' => 'Ultima atualização',
        'deleted-at' => 'Removido a',
    ],
    'placeholder' => [
        'email' => 'exemplo@email.com',
        'date' => 'dd-mm-yyyy',
        'time' => 'hh:mm',
        'time-secs' => 'hh:mm:ss',
        'date-time' => 'dd-mm-yyyy hh:mm',
        'date-time-secs' => 'dd-mm-yyy hh:mm:ss',
    ],
    'user' => [
        'model' => [
            'surname' => 'apelido',
            'email' => 'email',
            'position' => 'cargo',
            'group' => 'Grupo',
        ],
        'index' => [
            'search' => 'pesquisar utilizador',
            'info-blocked' => 'este utilizador está bloqueado!',
            'delete_user_sucess' => ':user  eliminado(s) com sucesso.',
            'actions' => [
                'new' => 'criar novo utilizador',
                'edit' => 'Editar utilizador \':name\'',
                'remove' => 'Remover utilizador \':name\'|Remover :n utilizadores',
            ],
            'name' => 'Nome',
            'surname' => 'Apelido',
            'email' => 'Email',
            'position' => 'Cargo',
            'create_new' => 'Novo Utilizador',
        ],
        'edit' => [
            'title' => 'Editar Utilizador',
            'no-login-record' => 'Sem registo de login',
            'last-login' => 'Último login',
            'info-blocked-account-n-tries' => 'conta bloqueada por excesso de tentativas.',
            'connect2profile' => 'Ligar a perfil',
            'edit_profile' => 'Editar perfil',
            'actions' => [
                'change-group' => 'alterar grupo',
                'recover-password' => 'Recuperar Password',
                'remove' => 'Remover utilizador',
            ],
        ],
        'archived' => [
            'title' => 'Arquivo de Utilizadores',
            'search' => 'pesquisar perfil',
        ],
        'ask' => [
            'recover-password' => 'Tem a certeza que pretende recuperar a password do utilizador \':user\'?',
            'restore' => 'Tem a certeza que pretende restaurar o utilizador \':user\'?|Tem a certeza que pretende restaurar estes :n utilizadores?',
            'delete' => 'Tem a certeza que pretende apagar o utilizador \':user\'?|Tem a certeza que pretende apagar estes :n utilizadores?',
            'delete-forever' => 'Tem a certeza que pretende eliminar de forma permanente o utilizador \':user\'?|Tem a certeza que pretende eliminar de forma permanente estes :n utilizadores?',
        ],
        'messages' => [
            'sure_restore_user' => 'Tem a certeza que pretende restaurar os utilizadores selecionados?',
            'success' => [
                'new' => 'Utilizador \':name\' criado com sucesso.',
                'update' => 'Utilizador \':name\' atualizado com sucesso.',
                'edit_group' => 'grupo editado com sucesso.',
                'edit' => 'Estado do utilizador \':user\' alterado com sucesso.| Os estados dos :n utilizadores foram alterados com sucesso.',
                'delete' => 'Utilizador \':user\' removido com sucesso!| :n utilizadores removidos!',
                'delete-forever' => 'Utilizador \':user\' eliminado de forma permanente!| :n utilizadores eliminados de forma permanente!',
                'recovery_password' => 'Um email foi enviado ao utilizador, para que este possa proceder à recuperação de password',
                'recovey_subject' => 'Recuperação password - Sarafauto',
            ],
            'error' => [
                'new' => 'Erro ao criar o utilizador \':name\'!',
                'edit' => 'Não foi possivel alterar o estado do utilizador \':name\'!|Não foi possivel alterar os estados dos :n utilizadores!',
                'delete' => 'Erro ao remover o utilizador \':user\'!|Erro ao remover os :n utilizadores!',
                'delete-forever' => 'Erro ao eliminar permanentemente o utilizador \':user\'!|Erro ao eliminar de forma permanente os :n utilizadores!',
            ],
        ],
    ],
    'group' => [
        'permissions' => 'Lista de atuais permissões',
        'rm-users' => 'Remover utilizador :name|Remover :n utilizadores',
        'index' => [
            'search' => 'pesquisar grupo',
            'updated-by' => 'Atualizado por',
            'actions' => [
                'new' => 'criar novo grupo',
                'edit' => 'Editar grupo :name',
                'remove' => 'Remover grupo :name|Remover :n grupos',
            ],
        ],
        'create' => [
            'new' => 'Novo grupo',
            'name' => 'nome',
            'current_permissions' => 'Lista de permissões',
        ],
        'edit' => [
            'administration' => 'Administração',
            'title' => 'Editar Grupo',
            'permissions' => 'Alterar permissões',
            'add-user' => 'Adicionar novo utilizador',
            'actions' => [
                'remove' => 'Remover grupo',
            ],
        ],
        'archived' => [
            'title' => 'Arquivo de Grupos',
            
        ],
        'ask' => [
            'restore' => 'Tem a certeza que pretende restaurar o grupo \':group\'?|Tem a certeza que pretende restaurar estes :n grupos?',
            'delete' => 'Tem a certeza que pretende apagar o grupo selecionado?|Tem a certeza que pretende apagar estes :n grupos?',
            'delete-forever' => 'Tem a certeza que pretende eliminar de forma permanente o grupo \':group\'?|Tem a certeza que pretende eliminar de forma permanente estes :n grupos?',
        ],
        'messages' => [
            'success' => [
                'new' => 'Grupo \':group\' criado com sucesso.',
                'edit' => 'Estado do grupo \':group\' alterado com sucesso.|Estados dos :n grupos alterados com sucesso.',
                'created_group' => 'Grupo criado com sucesso.',
                'delete' => 'Grupo \':group\' removido com sucesso!| :n grupos removidos!',
                'delete-forever' => 'Grupo \':group\' eliminado com sucesso!| :n grupos eliminados!',
                'update_permissions' => 'Permissões alteradas com sucesso!',
                'update_group' => 'Grupo atualizado com sucesso.',
                'deleted_groups' => 'Grupo(s) removido(s) com sucesso.',
            ],
            'error' => [
                'new' => 'Erro ao criar grupo \':group\'!',
                'edit' => 'Não foi possivel alterar o estado do grupo \':group\'!|Não foi possivel alterar os estados dos :n grupos!',
                'delete' => 'Erro ao remover o grupo \':group\'!|Erro ao remover os :n grupos!',
                'delete-forever' => 'Erro ao eliminar o grupo \':group\'!|Erro ao eliminar os :n grupos!',
                'delete_group' => 'Não é possivel eliminar o grupo :Name',
            ],
        ],
    ],
    'task' => [
        'messages' => [
            'view' => 'Ver tarefas em espera',
            'success' => [
                'new' => 'Existem novas tarefas à sua espera.',
                'edit' => 'Tarefa \':name\' editada com sucesso!',
                'done' => 'Tarefa \':name\' concluida!',
                'delete' => 'Tarefa eliminada!|Tarefas eliminadas!',
            ],
            'error' => [
                'new' => 'Não foi possível criar nova tarefa!',
                'edit' => 'Não foi possível editar tarefa \':name\'!',
                'done' => 'Não foi possível concluir a tarefa \':name\'!',
                'delete' => 'Não foi possivel eliminar a tarefa \':name\'!|Não foi possível eliminar :n tarefas!',
            ],
        ],
    ],
    'permissions' => [

            'see_users' => 'ver utilizadores',
            'create_users' => 'criar/editar utilizadores',
            'delete_users' => 'eliminar utilizadores',

            'see_groups' => 'ver grupos',
            'create_groups' => 'criar/editar grupos',

            'delete_groups' => 'eliminar grupos',
            'restore' => 'Restaurar',
            'see_archived_users' => 'ver utilizadores arquivados',
            'restore_archived_users' => 'recuperar utilizadores arquivados',

    ],
];
