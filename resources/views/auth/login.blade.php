@extends('layouts.app')

@section('content')
<div class="container login-form">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="panel">
                <div class="panel-titulo">
                    <span class="titulo">
                        <i class="icon-ultimo-login icon10"></i>
                        Login
                    </span>
                </div>
                <div class="panel-body login-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} login-input">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="email" class="col-md-12"><i class="icon-avatar-person icon10 icon-padding"></i>Email</label>
                                <div class="login-error">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="ex. brainy@tsf.pt">
                                    @if($errors->has('email'))
                                            <i class="icon-opcoes-eliminar"></i>
                                    @endif
                                </div>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email', 'Email e/ou password estão incorretos.') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} login-input">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="password" class="col-md-12"><i class="icon-bloquear-1 icon10 icon-padding"></i>Password</label>
                                <div class="login-error">
                                    <input id="password" type="password" class="form-control" name="password" placeholder="ex. omeubackofﬁce">
                                    @if($errors->has('password'))
                                            <i class="icon-opcoes-eliminar"></i>
                                    @endif
                                </div>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password', 'Email e/ou password estão incorretos.') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group login-remember">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="checkbox">
                                    <label class="checkbox-default checkbox_permissions">
                                        <input class="check_perm" type="checkbox" name="remember">
                                        <span>Guardar password</span>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="login-btn">
                                    <button type="submit" class="btn btn-default btn-yellow">
                                        Entrar
                                    </button>

                                    <a class="btn btn-link login-forgot" href="{{ url('/password/reset') }}"><span>Esqueceu-se da password ?</span></a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
