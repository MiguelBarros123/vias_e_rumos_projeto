<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 10:12
 */

namespace Brainy\Cms\Models;
use Illuminate\Database\Eloquent\Model;

class ComposedHighlightTranslation extends Model {

    public $timestamps = false;
    protected $fillable = ['title', 'description', 'name_link', 'link', 'image_alt'];
    
}