<div class="row">
    <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 new_message_popup">

        @if(session('sucess-message'))
            <div class="alert alert-sucess alert-dismissible fadeInDown initial_alert" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <i class="icon-permissoes icon15 pading_right"></i>{{session('sucess-message')}}
            </div>
        @endif

        @if(session('error-message'))
            <div class="alert alert-erro alert-dismissible fadeInDown initial_alert" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <i class="icon-opcoes-eliminar icon15 pading_right"></i>{!!session('error-message')!!}
            </div>
        @endif
    </div>
</div>
