@extends('layouts.frontend.app')



@section('content')

<div class="mar-top-5">

    @include('cms::frontend.pages.layouts.search-bar')

</div>

<div class="empresa-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            {!! $menu_institucional->description !!}

        </div>

    </div>

</div>

@endsection

