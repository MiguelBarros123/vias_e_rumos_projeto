<div class="modal modal-aux" id="new_profile" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-dialog-aux" role="document">
        <form class="form_new_profile" method="POST" action="{{route('profiles::backend::profiles.group.add_new_profile')}}" >
            {!! csrf_field() !!}
            <div class="modal-content">
                <div class="panel-titulo"><span class="titulo"> @lang('profiles::backend.common.profile')  </span></div>
                <div class="modal-body">

                    <div id="form_messages" class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="alert alert-dismissible " role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span></button>
                                    <i class="icon-permissoes icon15 pading_right"></i>
                                    <span class="data_message"></span>
                                </div>
                        </div>
                    </div>

                    <div class="row">
                        <div  class="col-lg-6">
                            <div class="form-group fom-style-hidden2 @if($errors->first('profile_name') ) has-error @endif">
                                <label>@lang('profiles::backend.common.name')</label>
                                <input type="text" name="profile_name" class="form-control " value="{{old('profile_name')}}">
                                {!! $errors->first('profile_name','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                        <div  class="col-lg-6">
                            <div class="form-group fom-style-hidden2 @if($errors->first('profile_surname') ) has-error @endif">
                                <label>@lang('profiles::backend.common.surname')</label>
                                <input type="text" name="profile_surname" class="form-control " value="{{old('profile_surname')}}">
                                {!! $errors->first('profile_surname','<span class="validator_errors">:message</span>')!!}
                            </div>
                        </div>
                    </div>

                    <div class="divider_line2"></div>

                    <div class="row">
                        <div  class="col-lg-12 margem_20">
                            <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                <label>@lang('profiles::backend.common.is_user')</label>
                                <div>
                                    <span>Não</span><input id="toggle-profile-user" type="checkbox" data-on=" " data-off=" " name="is_user" data-style="ios new_toggle" ><span>Sim</span>
                                </div>

                            </div>
                        </div>

                        <div class="user_definitions_profile">

                            <div class="row no_margin_row">
                                <div class="col-lg-6">
                                    <div class="form-group fom-style-hidden2">
                                        <label for="exampleInputEmail1">@lang('profiles::backend.common.type')</label>
                                        <select class="form-control " id="type_of_user" name="type">
                                            <optgroup label="FrontOffice">
                                                <option value="login_user">Conta de utilizador FrontOffice</option>
                                            </optgroup>
                                            @if($back_groups->count() >0)
                                                <optgroup label="BackOffice">
                                                    <option value="back_user">Conta de utilizador BackOffice</option>
                                                </optgroup>
                                            @endif

                                        </select>
                                    </div>
                                </div>

                            </div>

                            <div class="row no_margin_row">
                                <div  class="col-lg-6 ">
                                    <div class="form-group fom-style-hidden2 @if($errors->first('profile_email') ) has-error @endif">
                                        <label>@lang('profiles::backend.common.email')</label>
                                        <input type="text" name="profile_email" class="form-control " value="{{old('profile_email')}}">
                                        {!! $errors->first('profile_email','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                </div>
                                <div  class="col-lg-6 ">
                                    <div class="form-group fom-style-hidden2 @if($errors->first('profile_password') ) has-error @endif">
                                        <label>@lang('profiles::backend.common.password')</label>
                                        <input type="text" name="profile_password" class="form-control " value="{{str_random()}}">
                                        {!! $errors->first('profile_password','<span class="validator_errors">:message</span>')!!}
                                    </div>
                                </div>
                            </div>

                            <div id="front_groups">
                                <div class="row no_margin_row">
                                    <div  class="col-lg-6">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.group_front')</label>
                                            <div>
                                                @include('layouts.backend.partials.select_object_placeholder', ['array'=>$front_groups, 'name'=>'profile_f_front_group_id', 'selected'=> '', 'class'=> 'front_groups'])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div  id="back_groups">
                                <div class="row no_margin_row">
                                    <div  class="col-lg-6">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.group_front')</label>
                                            <div>
                                                @include('layouts.backend.partials.select_object_placeholder', ['array'=>$front_groups, 'name'=>'profile_b_front_group_id', 'selected'=> '', 'class'=> 'back_groups'])
                                            </div>
                                        </div>
                                    </div>
                                    <div  class="col-lg-6">
                                        <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                            <label>@lang('profiles::backend.common.group_admin')</label>
                                            <div>
                                                @include('layouts.backend.partials.select_object_input', ['array'=>$back_groups, 'name'=>'profile_back_group_id', 'selected'=> '', 'class'=> 'back_groups'])
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </div>

                    </div>

                    <div class="divider_line2"></div>

                </div>
                <div class="modal-footer modal-eliminar padding-bottom-30">
                    <button id="new_profile_submit" type="submit" data-loading-text="@lang('profiles::backend.common.loading')" class="btn btn-default btn-yellow">@lang("profiles::backend.common.save")</button>
                    <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("permissions::backend.cancel") </button>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>