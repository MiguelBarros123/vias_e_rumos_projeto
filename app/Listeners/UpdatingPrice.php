<?php

namespace App\Listeners;

use App\Events\UserHasBidding;
use Illuminate\Queue\InteractsWithQueue;
use Brainy\Cms\Models\ProfileReservedArea;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdatingPrice
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserHasBidding  $event
     * @return void
     */
    public function handle(UserHasBidding $event)
    {
        $profileReservedArea = ProfileReservedArea::where('user_id',$event->user_id)->first();
        $profileReservedArea->bidding_systems()->attach([
            'lot_id' => $event->lot_id,
            'price' => $event->price
        ]);

        $profileReservedArea->save();

        var_dump('Bidding was done successfully');
    }
}
