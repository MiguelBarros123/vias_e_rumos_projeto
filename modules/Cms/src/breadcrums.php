<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-01-2017
 * Time: 09:46
 */

DaveJamesMiller\Breadcrumbs\Facade::register('cms', function($breadcrumbs)
{
    $breadcrumbs->push('O meu site', route('cms::backend::pages.edit_page'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.pages', function($breadcrumbs)
{
    $breadcrumbs->parent('cms');
    $breadcrumbs->push('Páginas', route('cms::backend::pages.edit_page'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.pages.show', function($breadcrumbs, $page)
{
    $breadcrumbs->parent('cms.pages');
    $breadcrumbs->push($page->pageTranslation(), route('cms::backend::pages.edit_page', $page->id));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.members.index', function($breadcrumbs)
{
    $breadcrumbs->parent('cms');
    $breadcrumbs->push('Membros', route('cms::backend::backend.cms.members.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.members.create', function($breadcrumbs)
{
    $breadcrumbs->parent('cms.members.index');
    $breadcrumbs->push('Criar membro', route('cms::backend::backend.cms.members.create'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.members.show', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('cms.members.index');
    $breadcrumbs->push($product->name, route('cms::backend::backend.cms.members.show', [$product->id]));
});


DaveJamesMiller\Breadcrumbs\Facade::register('cms.members.edit', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('cms.members.show',$product);
    $breadcrumbs->push('Editar membro', route('cms::backend::backend.cms.members.edit', [$product->id]));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.menu_biblioteca.index', function($breadcrumbs)
{
    $breadcrumbs->parent('cms');
    $breadcrumbs->push('Menu Bliblioteca', route('cms::backend::backend.cms.menu_biblioteca.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.menu_biblioteca.create', function($breadcrumbs)
{
    $breadcrumbs->parent('cms.menu_biblioteca.index');
    $breadcrumbs->push('Criar Menu Bliblioteca', route('cms::backend::backend.cms.menu_biblioteca.create'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.menu_biblioteca.edit', function($breadcrumbs, $menu)
{
    $breadcrumbs->parent('cms.menu_biblioteca.show',$menu);
    $breadcrumbs->push('Editar membro', route('cms::backend::backend.cms.menu_biblioteca.edit', [$menu->id]));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.sub_menus_biblioteca.index', function($breadcrumbs)
{
    $breadcrumbs->parent('cms');
    $breadcrumbs->push('Menu Bliblioteca', route('cms::backend::backend.cms.sub_menus_biblioteca.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.sub_menus_biblioteca.create', function($breadcrumbs)
{
    $breadcrumbs->parent('cms.sub_menus_biblioteca.index');
    $breadcrumbs->push('Criar Menu Bliblioteca', route('cms::backend::backend.cms.sub_menus_biblioteca.create'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('cms.sub_menus_biblioteca.edit', function($breadcrumbs, $menu)
{
    $breadcrumbs->parent('cms.sub_menus_biblioteca.show',$menu);
    $breadcrumbs->push('Editar membro', route('cms::backend::backend.cms.sub_menus_biblioteca.edit', [$menu->id]));
});
