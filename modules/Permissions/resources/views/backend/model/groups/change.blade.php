@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
@endsection

@section('module-scripts')

@endsection

@section('content')

    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{route('permissions::backend::group.edit', [$old_group->id])}}" class="icon_voltar"><i class="icon-voltar"></i> Voltar</a></li>
                    </ul>
                </div>

            </div>

            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.group_change_users_group_view',$old_group) !!}

            <div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo"><span class="titulo"><i class="icon-trocar-grupos icon20"></i> MUDAR DE GRUPO</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <div class="col-lg-12">
                                    @foreach($users as $user)
                                        <div class="inline-images text-center">
                                            @if($user->photo === '')
                                                <div class="pic-circle-corner2 text-center profile_no_pic">
                                                    <i class="icon-avatar-person icon10 "></i>
                                                </div>
                                            @else
                                                <img class="pic-circle-corner-75" src="{{route('gallery::frontend::storage_image_p',[$user->photo,50,50])}}">
                                            @endif
                                            <p class="font_12_lato_regular padding-top-10 ">{{$user->user->name}}</p>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <form class="form_elimina_user" method="POST" action="{{route('permissions::backend::update_users_group_view',array($old_group->id))}}" >
                {!! csrf_field() !!}

                <input type="text" name="deleted_orders"  value="{{$users->implode('id',',')}}" hidden>
                <input type="text" name="old_group_id" value="{{$old_group->id}}" hidden>




                <div class="row">
                    <div class="col-lg-12">
                        <p class="padding-left-15 font_12_lato_regular">Mudar do grupo</p>

                        <div class="panel ">
                            <div class="panel-body">
                                <div class="row">
                                        <div class="col-lg-12">
                                            <label class="radio-default" style="margin-bottom: -2px!important;">
                                                <input type="radio" name="group_id" value="{{$old_group->id}}" checked>
                                                <span class="span-radio"></span>
                                            </label>
                                            <div class="container_images">
                                                @if($user->photo === '')
                                                    <div class="pic-circle-corner2 text-center profile_no_pic">
                                                        <i class="icon-avatar-person icon10 "></i>
                                                    </div>
                                                @else
                                                    <img class="img-circle img-responsive imagem_user_mini" src="{{route('gallery::frontend::storage_image_p',[$user->photo,50,50])}}">
                                                @endif
                                            </div>
                                            <label class="label-group-list fonte_12_lato_light"> {{$user->group->name}} </label>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">

                            <p class="padding-left-15 font_12_lato_regular">para o grupo:</p>
                        @if($groups->count()==0)
                            <div class="panel" style="margin-bottom: 5px!important;">
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-lg-12">Não existem grupos no sistema. Crie um grupo.</div>
                                    </div>
                                </div>
                            </div>
                        @endif

                        @foreach($groups as $group)
                            @if($group->id!=$old_group->id)
                                <div class="panel" style="margin-bottom: 5px!important;">
                                    <div class="panel-body">
                                        <div class="row">
                                                <div class="col-lg-12">
                                                    <label class="radio-default" style="margin-bottom: -2px!important;">
                                                        <input type="radio" name="group_id" value="{{$group->id}}">
                                                        <span class="span-radio"></span>
                                                    </label>

                                                    <div class="container_images">

                                                        @foreach ($group->users()->take(4)->get() as $user_image)
                                                            @if($user_image->photo === '')
                                                                <div class="pic-circle-corner2 text-center profile_no_pic">
                                                                    <i class="icon-avatar-person icon10 "></i>
                                                                </div>
                                                            @else
                                                                <img class="img-circle img-responsive imagem_user_mini" src="{{route('gallery::frontend::storage_image_p',[$user_image->photo,50,50])}}">
                                                            @endif
                                                        @endforeach
                                                    </div>
                                                    <label class="label-group-list fonte_12_lato_light">{{$group->name}}
                                                        <div class="accordion-arrow"></div>
                                                    </label>
                                                </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div>


                <div class="row padding-top-35">
                    @if($groups->count()!=0)<div class="col-lg-12">
                        <button type="submit" class="btn btn-default btn-save-user ">MUDAR DE GRUPO</button>
                        <a href="{{route('permissions::backend::group.edit',array($old_group->id))}}"  class="btn btn-default btn-cancelar ">CANCELAR</a>
                    </div>
                        @endif
                </div>


            </form>
        </div>
    </div>




@endsection






