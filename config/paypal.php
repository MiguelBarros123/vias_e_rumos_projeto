<?php
return array(
    // set your paypal credential
    // real credentials 'client_id' => 'ATWy7PjW39ruKKH7GMvkPvw5LJGyRw6VIsgFQo-QoSF_ADGdIDzoqqmZ8-sxNIk2y4h8uAkTji5erexX',
    // real credentials 'secret' => 'EIPAKd9P3k5OQM0der2J6862tP8T9ii9RpkSVs16MqByD4rgaFVBJutPfimuWwY46srXD2vt_KiC-SCv',

      'client_id' => 'AXpAzIVokiG8DCGSpNssRKvHaxTGinA38dSCQvH71t59qJLHjUIniKqT6rZvk5O8KIbhq74P2c3zXH1D',
         'secret' => 'EJod_lHN_2UpsAOKUSL6TCBoJBoZ-v1-J2MnNMUoxY5aCtDsUfDxWRb4fca7E4QVRWNxhJqJbsm3Dsqk',

    /**
     * SDK configuration
     */
    'settings' => array(
        /**
         * Available option 'sandbox' or 'live'
         */
        'mode' => 'sandbox',

        /**
         * Specify the max request time in seconds
         */
        'http.ConnectionTimeOut' => 30,

        /**
         * Whether want to log to a file
         */
        'log.LogEnabled' => true,

        /**
         * Specify the file that want to write on
         */
        'log.FileName' => storage_path() . '/logs/paypal.log',

        /**
         * Available option 'FINE', 'INFO', 'WARN' or 'ERROR'
         *
         * Logging is most verbose in the 'FINE' level and decreases as you
         * proceed towards ERROR
         */
        'log.LogLevel' => 'FINE'
    ),
);