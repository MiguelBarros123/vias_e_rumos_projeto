<?php

return [
    'module-label'=>'Vendas Online',
    'menu' => [
        'auctions_online' => 'Vendas Online'
    ],
    'common'=>[
       
    ],
    'messages' => [
        'success' => [
            
        ],
        'error' => [
            
        ],
        'email' => [
           
        ],
    ],

    'permission' => [
        'auctions_online' => [
            'see' => 'Ver Vendas Online',
            'create' => 'Criar Vendas Online',
            'delete' => 'Apagar Vendas Online',
        ],
        'personalized_fields'=>[
          
        ]
    ],
];
