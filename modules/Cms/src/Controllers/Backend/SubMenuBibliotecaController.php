<?php

namespace Brainy\Cms\Controllers\Backend;
use Brainy\Cms\Models\MenuBiblioteca;
use Brainy\Cms\Models\SubMenuBiblioteca;
use Yajra\Datatables\Facades\Datatables;
use Brainy\Framework\Controllers\BackendController;

class SubMenuBibliotecaController extends BackendController
{
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        $sub_menus = SubMenuBiblioteca::all();
        return view('cms::backend.sub_menus_biblioteca.index', compact('sub_menus'));
    }

    public function index_ajax()
    {
        $sub_menus = SubMenuBiblioteca::all();

        return Datatables::of($sub_menus)
            ->editColumn('name', function($sub_menu){
                return $sub_menu->name;
            })
            ->editColumn('menu', function($sub_menu){
               $menu = MenuBiblioteca::where('id', $sub_menu->menu_biblioteca_id)->first();
               return $menu->name;
            })
            ->editColumn('order', function($sub_menu){
                return '<i class="icon-icon-peq-img-size"</i>'.$sub_menu->order;
            })
            ->addColumn('action', function ($sub_menu) {
                $rota=route('cms::backend::backend.cms.sub_menu_biblioteca.delete');
                $rotaeditar=route('cms::backend::backend.cms.sub_menus_biblioteca.edit',[$sub_menu->id]);
                $impossible=0;

                $string = '
                        <div class="btn-group pull-right">
                           <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                            <li><a  class="edit_item_datatable" href="'.$rotaeditar.'" ><i class="icon-editar"></i>  ' . trans("cms::backend.model.common.edit") . '</a></li>
                            <li><a data-impossible-delete="'.$impossible.'" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $sub_menu->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("cms::backend.model.common.delete") . ' </a></li>
                          </ul>
                        </div>

                       ';
                return $string;
            })
            ->addColumn('rota_final', function ($sub_menu) {
                return route('cms::backend::backend.cms.sub_menus_biblioteca.edit', array($sub_menu->id));
            })
            ->make(true);
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return Response
    */    
    public function create()
    {
        $menus = MenuBiblioteca::all();

        return view('cms::backend.sub_menus_biblioteca.create', compact('menus'));
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  
    * @return Response
    */
    public function edit($id)
    {
        $menus = MenuBiblioteca::all();
        $sub_menu = SubMenuBiblioteca::find($id);
        return view('cms::backend.sub_menus_biblioteca.edit', compact('sub_menu','menus'));
    }

     /**
    * Store a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function store()
    {
        $sub_menu = SubMenuBiblioteca::create(['menu_biblioteca_id' => request('menu_id'), 'name' => request('title'), 'description' => request('description')]);

        if(request('submit') === 'stay'){
            return redirect()->route('cms::backend::backend.cms.sub_menus_biblioteca.edit',[$sub_menu->id])->with('sucess-message', trans('cms::backend.messages.success.global_create'));
        }

        return redirect()->route('cms::backend::backend.cms.sub_menus_biblioteca.index')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }

    /**
    * Update a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function update($id)
    {
        $sub_menu = SubMenuBiblioteca::find($id);
        $sub_menu->update(['menu_biblioteca_id' => request('menu_id'), 'name' => request('title'), 'description' => request('description')]);

        if(request('submit') === 'stay'){
            return redirect()->route('cms::backend::backend.cms.sub_menus_biblioteca.edit',[$sub_menu->id])->with('sucess-message', trans('cms::backend.messages.success.global_create'));
        }

        return redirect()->route('cms::backend::backend.cms.sub_menus_biblioteca.index')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param Request 
    * @return Response
    */
    public function delete()
    {
        $rows=explode(',',request('deleted_rows'));

        foreach($rows as $id){
            $sub_menu = SubMenuBiblioteca::findOrFail($id);
            $sub_menu->delete();
            SubMenuBiblioteca::where('order','>',$sub_menu->order)->decrement('order');
        }

        return redirect()->route('cms::backend::backend.cms.sub_menus_biblioteca.index')->with('sucess-message', trans('cms::backend.messages.success.global_delete'));
    }
}
