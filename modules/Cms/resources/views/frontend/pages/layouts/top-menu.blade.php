<div class="menu-opened">
	<ul class="menu-links-list">
		<li><a href="{{ route('cms::frontend::model.empresa') }}">institucional</a></li>
		<li>
			<div class="open-list list_on_menu">
			<span class="sel-helper">Vendas<span class="drop-s"><img src="{{ asset('front/imgs/icons/seta-drop-blue.svg') }}"/></span></span>
			<select id="choose_3" class="select" onchange="location = this.value;">
				<option class="hidden" disabled selected></option>
				<option value="{{route('cms::frontend::model.vendas')}}">Todas</option>
				@foreach($categories as $category)

				<option value="{{route('cms::frontend::model.vendas', $category->id)}}">{{$category->name}}</option>
				@endforeach
			</select>
		</div></li>
		<li><a href="{{ route('cms::frontend::model.biblioteca') }}">biblioteca</a></li>
		<li><a href="{{ route('cms::frontend::model.contacts') }}">contactos</a></li>
	</ul>
</div>
<div class="search-opened">
	<div class="search-mobile-options">
		@if(count($categories))
		<div class="open-list">
			<span class="sel-helper">Selecione um tipo de venda<span class="drop-s"><img src="{{ asset('front/imgs/icons/seta-drop-white.svg') }}"/></span></span>
			<select id="choose" class="select" onchange="location = this.value;">
				<option class="hidden" disabled selected></option>
				@foreach($categories as $category)

				<option value="{{route('cms::frontend::model.vendas', $category->id)}}">{{$category->name}}</option>
				@endforeach
			</select>
		</div>
		<div class="sep_line">
		</div>
		@endif
		@if(count($sub_type_of_goods))
		<div class="open-list">
			<span class="sel-helper">Selecione um tipo de bens<span class="drop-s"><img src="{{ asset('front/imgs/icons/seta-drop-white.svg') }}"/></span></span>
			<select id="choose_2" class="select" onchange="location = this.value;">
				<option class="hidden" disabled selected></option>
				@foreach($sub_type_of_goods as $sub_type)
					<li>
						<option value="{{route('cms::frontend::model.filter_by_sub_types', [$sub_type->id, $sub_type->type])}}">{{$sub_type->name}}</option>
					</li>
				@endforeach
				
			</select>
		</div>
		<div class="sep_line">
		</div>
		@endif

		<div class="link_go_to">
			<a href="{{ route('cms::frontend::model.avancada')}}">Pesquisa Avançada</a>
		</div>
		<div class="sep_line">
		</div>
		
	</div>
</div>

<div id="top-menu-mobile" class="hidden-md hidden-lg">
	<div class="logo-menu">
		<a href="{{ route('cms::frontend::index') }}"><img src="{{ asset('front/imgs/vias_logo.svg') }}"/></a>
	</div>
	<div class="menu-icons">
		<div class="icon_pesquisa">
			<div class="open-search">
				<img src="{{ asset('front/imgs/icons/search.svg') }}"/>
			</div>
		</div>
		<div class="icon_menu">
			<div class="open-menu">
				<img src="{{ asset('front/imgs/icons/menu.svg') }}"/>
			</div>
		</div>
	</div>

</div>

<div id="top-menu">
	<div class="logo-menu hidden-xs hidden-sm">
		<a href="{{ route('cms::frontend::index') }}"><img src="{{ asset('front/imgs/vias_logo.svg') }}"/></a>
	</div>
	<div class="links-menu">
		<ul class="links-list">
			<li><a href="#newsletter" data-toggle="tab" class="top-click">Newsletter</a></li>
<!-- 			<li><a href="#login" data-toggle="tab" class="top-click">Login</a></li>
			<li><a href="#registo" data-toggle="tab" class="top-click">Novo Registo</a></li> -->
		</ul>
			
		<div class="tab-content">
			<div id="newsletter" class="tab-pin pane-div">
				<div class="input_wrap">
						<form method="post" target="_blank" id="easyform_5etecBYe79ONqWuUle48bf2--4" class="easyform_5etecBYe79ONqWuUle48bf2--4 fm" enctype="multipart/form-data" action="https://35.e-goi.com//w/5etecBYe79ONqWuUle48bf2--4">
						  <input type="hidden" name="lista" value="5">
						  <input type="hidden" name="cliente" value="45418">
						  <input type="hidden" name="lang" id="lang_id" value="pt">
						  <input type="hidden" name="formid" id="formid" value="28">
						   <input name="email_56" easylabel="E-mail" alt="" id="email_56" class="validate[required,custom[email]] top_inp" value="" easysync="email" type="email" placeholder="insira o seu email">

							<span class="btn-line">
							<button type="submit" name="submit" id="subform" value="subscrever">Subscrever</button>
							</span>
						</form>

					
					 <!-- <form method="post" id="easyform_5etecBYe79ONqWuUle48bf2--4" class="easyform_5etecBYe79ONqWuUle48bf2--4 fmm" enctype="multipart/form-data" action="https://35.e-goi.com//w/5etecBYe79ONqWuUle48bf2--4">
					  <input type="hidden" name="lista" value="5">
					  <input type="hidden" name="cliente" value="45418">
					  <input type="hidden" name="lang" id="lang_id" value="pt">
					  <input type="hidden" name="formid" id="formid" value="28">
					  <table id="egoitable" class="easygoi-template" style="background-color: rgb(245, 245, 245); font-family: Arial,Lucida; font-size: 13px;" cellspacing="0" cellpadding="0" width="100%" border="0">
					    <tr>
					      <td class="easygoi-left-spacer" valign="top" width="50%">
					       &nbsp;
					      </td>
					      <td class="easygoi-structure" valign="top">
					        <table class="easygoi-drag-structure" cellspacing="0" cellpadding="0" width="700" border="0">
					          <tr>
					            <td style="width: 700px;" class="easygoi-column">
					             <div class="easygoi-struct" style="background-color: rgb(181, 204, 223); padding: 0px; border: 0px none rgb(0, 0, 0);">
					                <table style="background-color: rgb(181, 204, 223); width: 100%;" class="easygoi-sub-structure" cellspacing="20" cellpadding="0" width="" border="0">
					                  <tr>
					                    <td class="easygoi-column" style="width: 530px;" valign="top" align="left">
					                      <table class="easygoi-title" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;width: 100%;" cellspacing="0" cellpadding="0" border="0">
					                        <tr>
					                          <td style="padding: 10px 0px 0px; mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-line-height-rule:exactly;" valign="top">
					                            <p style="font-family: arial,helvetica,sans-serif; font-size: 18px; font-weight: bold; margin: 0px; color: rgb(255, 255, 255);">Vias &amp; Rumos</p>
					                          </td>
					                        </tr>
					                      </table>
					                    </td>
					                    <td class="easygoi-column" style="width: 110px;" valign="top" align="left">
					                      <table class="easygoi-image" style="border-collapse: collapse; width: 110px;" cellspacing="0" cellpadding="0" width="110" height="auto" border="0">
					                        <tr>
					                          <td style="line-height: 0px; width: 110px; background-color: transparent; text-align: left; padding: 0px;" valign="top" align="left" height="auto">
					                            <img src="https://35.e-goi.com/recursos/1346b409c386ccd4cf67d544ed917d3f/.Easygoi/image.gif" alt="image" style="display: block; width: 110px; height: 73px; border: 0px none; outline: medium none; text-decoration: none; vertical-align: bottom;" width="110" height="73" border="0">
					                          </td>
					                        </tr>
					                      </table>
					                    </td>
					                  </tr>
					                </table>
					              </div>
					              <div class="easygoi-struct" style="background-color: rgb(255, 255, 255);">
					                <table style="width: 100%;" class="easygoi-sub-structure" cellspacing="0" cellpadding="20" width="" border="0">
					                  <tr>
					                    <td class="easygoi-column" style="width: 660px;" valign="top" align="left">
					                      <div class="easygoi-struct easygoi-form" style="">
					                        <table style="width: 100%;" class="easygoi-sub-structure easygoi-struct-form" cellspacing="0" cellpadding="0" width="" border="0">
					                          <tr>
					                            <td class="easygoi-column" style="width: 100%;" valign="top" align="left" width="100%">
					                              <table class="easygoi-content" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;width: 100%;" cellspacing="0" cellpadding="0" border="0">
					                                <tr>
					                                  <td style="padding-bottom: 20px; mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-line-height-rule:exactly;" valign="top">
					                                    <p style="font-family: arial,helvetica,sans-serif; font-size: 14px; font-weight: normal; margin: 0px; color: rgb(51, 51, 51);">Subscreva a nossa newsletter e receba regularmente as nossas sugest&#245;es, not&#237;cias e ofertas especiais. A inscri&#231;&#227;o &#233; gratuita!</p>
					                                  </td>
					                                </tr>
					                              </table>
					                              <div style="padding-bottom: 20px; max-width: 660px;" class="easygoi-emailfield">
					                                <label for="email_56" easylabel="E-mail">
					                                  <span class="labeling" style="color: rgb(0, 0, 0); font-size: 14px; font-family: arial,helvetica,sans-serif;">E-mail</span>
					                                  <span id="required_1_html" style="color:red;" class="required_field"> *</span>
					                                </label>
					                                <span style="color: rgb(0, 0, 0); font-size: 14px; font-family: arial,helvetica,sans-serif;" class="help_field"></span>
					                                <input name="email_56" easylabel="E-mail" alt="" id="email_56" class="validate[required,custom[email]]" value="" style="font-size: 13px; font-family: null; color: rgb(0, 0, 0); display: inline;" easysync="email" type="email">
					                              </div>
					                              <div class="easygoi-button-element" style="padding-bottom: 5px;" data-width="660">
					                                <input type="submit" value="submit">
					                                <div class="easygoi-publicidade" style="margin-top: 10px; margin-bottom: 10px; width: 100%;">
					                                </div>
					                              </div>
					                              <table class="easygoi-content" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;width: 100%;" cellspacing="0" cellpadding="0" border="0">
					                                <tr>
					                                  <td style=" mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;mso-line-height-rule:exactly;" valign="top">
					                                    <p style="font-family: arial,helvetica,sans-serif; font-size: 14px; font-weight: normal; margin: 0px; text-align: center; color: rgb(51, 51, 51);"><span style="font-size:10px;">Os seus dados n&#227;o ser&#227;o transmitidos a terceiros</span></p>
					                                  </td>
					                                </tr>
					                              </table>
					                            </td>
					                          </tr>
					                        </table>
					                      </div>
					                    </td>
					                  </tr>
					                </table>
					              </div>
					            </td>
					          </tr>
					        </table>
					      </td>
					      <td class="easygoi-right-spacer" valign="top" width="50%">
					        &nbsp;
					      </td>
					    </tr>
					  </table>
					</form> -->
				
					
				</div>
			</div>
			

<!-- 			<div id="login" class="tab-pane pane-div">
				<div class="input_wrap">
					<input class="top_inp" placeholder="email" type="email"/>
					<input class="top_inp mar-inp" placeholder="password" type="password"/>
					<span class="btn-line">
						<button class="subscrever">
							entrar
						</button>
					</span>
				</div>
			</div>
			<div id="registo" class="tab-pane pane-div">
				<div class="input_wrap">
					<input class="top_inp" placeholder="email" type="email"/>
					<input class="top_inp mar-inp" placeholder="password" type="password"/>
					<input class="top_inp mar-inp" placeholder="repetir password" type="password"/>
					<span class="btn-line">
						<button class="subscrever">
							registar
						</button>
					</span>
				</div>
			</div> -->
		</div>
	</div>
</div>

@section('module-scripts')

@endsection