@foreach($menu_items as $menu)
    @ifAllowMenu($menu)
    <li  class="">
        @php
            $submenu = explode( '.', $menu->route);
        @endphp

    <a class=" @activeSubMenu([$menu, $submenu[0], $submenu[1]])"  @if(count($menu->sub_menus) > 0) data-toggle="collapse" href="#{{$menu->css_id}}" @else href="@menuRoute($menu)" @endif>
        <i class="{{$menu->css_class}}"></i> @lang($menu->label)
        @if(count($menu->sub_menus) > 0) <i class="icon-baixo"></i>@endif
    </a>
    @if(count($menu->sub_menus) > 0)

        <ul class="collapse" id="{{$menu->css_id}}">
            @include('layouts.backend.menu', ['menu_items' => $menu->sub_menus])
        </ul>
    @endif
    </li>
    @endifAllowMenu
@endforeach

