<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22-04-2016
 * Time: 14:58
 */

namespace Brainy\Gallery\Models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MediaFolder extends Model{

    use SoftDeletes;

    protected $fillable = ['title','localization','parent_folder_id','last_update','n_files','n_folders','size','container_name','garbage'];
    protected $dates = ['deleted_at','updated_at','created_at'];

    public function media_items()
    {
        return $this->hasMany(MediaItem::class);
    }



    public function hasFilesOfType($types){

        return $this->media_items()->whereIn('format',$types)->get()->count();
    }

    public function numberOfFiles(){

        return $this->media_items()->count();
    }

    public function numberOfFolders(){

        return MediaFolder::where('parent_folder_id',$this->id)->get()->count();
    }



}