@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/gallery/backend.css')}}">
<link rel="stylesheet" href="{{asset('back/css/context_menu.css')}}" >
@endsection

@section('module-scripts')
<script src="{{asset('js/gallery/backend.js')}}"></script>
<script src="{{asset('back/js/topmenuselects.js')}}"></script>
<script>
    var btn_route="{{route('framework::settings::update-state')}}";
</script>
<script src="{{asset('back/js/brainy-framework/settings-button.js')}}"></script>

@if(\Illuminate\Support\Facades\Session::has('errors'))
    <script>
        $('#myModal2').modal();
    </script>
@endif




<script>
    $(document).ready(function(){
        
        $.storagelib();

        var routeToSavaInFolder="{{route('gallery::backend::copy_media_asset',array($id_pasta))}}";

        if("{{$folder->parent_folder_id}}" != 0){

            $(function(){

                $.contextMenu({
                    selector: '.context-menu-one',
                    trigger: 'none',
                    callback: function(key, options) {

                    },
                    items: {
                        firstCommand: {
                            name: '' +
                            '<li>' +
                            '<div class="listagem_pastas listagem_pastas2">'+
                                    '<div class="container-fluid">'+
                            '<div class="row">'+
                            '<div class="col-lg-12 no_padding">'+
                            '<div class="row-eq-height-center">'+
                            '<div class="col-lg-2 back_destiny"><i class="icon-voltar"></i></div>'+
                            '<div class="col-lg-8 pasta_atual">@lang('gallery::backend.model.upload.gallery')</div>'+
                            '<div class="col-lg-10 nova_pasta_escond">'+

                            '<div class="input-group">'+
                            '<span  class="input-group-addon btn_cancel_new_folder"><i class="icon-voltar"></i></span>'+
                            '<input  type="text" class="form-control input_pasta" name="title" value="nova pasta">'+
                            '<input id="pasta_atual_pai"  type="hidden"  name="parent_folder_id" value="">'+
                            '<a  class="input-group-addon btn_save_new_folder" data-href="{{route('gallery::backend::save_folder_upload')}}"><i class="icon-permissoes icon15"></i></a>'+
                            '</div>'+


                            '</div>'+
                            '<div class="col-lg-2 fecha_list_pastas"><i class="icon-opcoes-eliminar"></i></div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="col-lg-12 sec_pastas_centro">'+
                            '<div class="new_folder_selected"></div>'+
                            '<div class="row">'+
                            '<div class="col-lg-12 no_padding">'+
                            '<ul id="selectable" class="destinys">'+

                            '</ul>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="col-lg-12 foot_destiny no_padding">'+
                            '<div class="innactive_btns"></div>'+
                            '<div class="container-fluid">'+
                            '<div class="row row-eq-height-center">'+
                            '<div class="col-lg-8 no_padding">'+
                            '<div class="innactive_save"></div>'+
                            '<a class="btn-yellow btn_save_folder move_para">@lang('gallery::backend.model.upload.save_here')</a>'+
                            '<div class="new_folder_selected"></div>'+
                            '</div>'+
                            '<div class="col-lg-4">'+
                            '<div class="new_folder_selected"></div>'+
                            '<i class="icon-criar-pasta icon15 new_folder_here" ></i>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+

                            '</div>' +
                            '</li>',
                            isHtmlName: true,
                            className: 'coustom_menu_new_folder',
                            callback: function(key, opt){
                                window.console && console.log(key);
                                $('.fecha_list_pastas').click(function(e){
                                    e.preventDefault();
                                    $('.context-menu-one').contextMenu('hide');
                                });



                                $('.move_para').click(function(e){

                                    $('#final_fol').val($(this).attr('data-id'));
                                    $('#form_move_files').submit();
                                });




                                return false;
                        }
                        }
                    }
                });


                $.contextMenu({
                    // define which elements trigger this menu
                    selector: "#selectable_cont",
                    reposition: false,
                    // define the elements of the menu
                    items: {
                        nova: {name: "Nova Pasta", icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-criar-pasta icon15"></i> @lang("gallery::backend.model.index.new_folder")');
                                    return true;
                                },callback: function(key, opt){

                            $('#myModal2').modal();
                        }},
                        colar_item: {name: "Colar",icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-copiar icon15"></i> @lang("gallery::backend.model.index.paste")');
                                    return true;
                                },callback: function(key, opt){

                            $.ajax({
                                type: "post",
                                url: routeToSavaInFolder,
                                data:$.getStorageObject('brainy_clipboard_for_user_'+"{{auth()->user()->id}}"),
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success: function (data) {

                                    //clear storage objects
                                    $.clearStorageObjects('brainy_clipboard_for_user_'+"{{auth()->user()->id}}");
                                    location.reload();


                                }
                            });
                        }}
                    }

                });
            });


            $(function(){
                $.contextMenu({
                    selector: ".item_galery",
                    className: 'icon20 roda_dentada_topo',
                    items: {
                        editar: {name: "Editar", icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-editar icon12"></i> @lang("gallery::backend.model.index.edit")');
                                    return true;
                                }, callback: function(key, opt){

                            window.location =opt.$trigger.attr('data-href');

                        },disabled:function(key, opt){


                            if($(".ui-selected").length != 1){
                                return true;
                            }
                            if(opt.$trigger.hasClass('folder_ref')){
                                return true;
                            }
                            return false;
                        }},

                        eliminar: {name: "Eliminar",icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-opcoes-eliminar icon12"></i> @lang("gallery::backend.model.index.delete")');
                                    return true;
                                }, callback: function(key, opt){
                            //window.console && console.log(opt.$trigger.attr('data-delete_route'));
                            //get all the selected send to delete

                            var items={};
                            items.folders=[];
                            items.files=[];

                            $(".item_galery.ui-selected").each(function(){

                                if(!$(this).hasClass('ui-draggable-dragging')){
                                    if($(this).hasClass('pasta_accept'))
                                    {
                                        items.folders.push($(this).attr('data-id'));
                                    }else{
                                        items.files.push($(this).attr('data-id'));
                                    }
                                }

                            });

                            //window.console && console.log(items.files);
                            $('#deleted_files').val(items.files);
                            $('#deleted_folders').val(items.folders);
                            $('#delete_media').modal('show');
                        }},

                        copiar: {name: "Copiar",icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-opcoes-copiar icon12"></i> @lang("gallery::backend.model.index.copy")');
                                    return true;
                                }, callback: function(key, opt){

                            var items={};
                            items.folders=[];
                            items.files=[];

                            $(".item_galery.ui-selected").each(function(){

                                if(!$(this).hasClass('ui-draggable-dragging')){
                                    if($(this).hasClass('pasta_accept'))
                                    {
                                        items.folders.push($(this).attr('data-id'));
                                    }else{
                                        items.files.push($(this).attr('data-id'));
                                    }
                                }

                            });


                            $.setStorageObject('brainy_clipboard_for_user_'+"{{auth()->user()->id}}",items);
                        }},
                        mover: {name: "Mover para",icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-mover-ficheiro icon12 context-menu-one"></i> @lang("gallery::backend.model.index.move")');
                                    return true;
                                }, callback: function(key, opt){


                            $('.context-menu-one').contextMenu();

                            var folders='{!! $all_folders !!}';
                            var inicial="{{$id_pasta}}";

                            var token=$('meta[name="csrf-token"]').attr('content');



                            var items_to_move={};
                            items_to_move.folders=[];
                            items_to_move.files=[];

                            $(".item_galery.ui-selected").each(function(){

                                if(!$(this).hasClass('ui-draggable-dragging')){
                                    if($(this).hasClass('pasta_accept'))
                                    {
                                        items_to_move.folders.push($(this).attr('data-id'));
                                    }else{
                                        items_to_move.files.push($(this).attr('data-id'));
                                    }
                                }

                            });

                            //window.console && console.log(items.files);
                            $('#to_move_files').val(items_to_move.files);
                            $('#to_move_folders').val(items_to_move.folders);


                            $('.listagem_pastas').selectslidemenu({ dados: folders,inicial:inicial,token: token, gallery:inicial,blocked_folders:items_to_move.folders});





                            return true;
                        }},

                        download: {name: "Download",icon:
                                function(opt, $itemElement, itemKey, item){
                                    $itemElement.html('<i class="icon-opcoes-download icon12"></i> @lang("gallery::backend.model.index.download")');
                                    return true;
                                },
                            callback: function(key, opt){

                                var items={};
                                items.folders=[];
                                items.files=[];

                                $(".item_galery.ui-selected").each(function(){

                                    if(!$(this).hasClass('ui-draggable-dragging')){
                                        if($(this).hasClass('pasta_accept'))
                                        {
                                            items.folders.push($(this).attr('data-id'));
                                        }else{
                                            items.files.push($(this).attr('data-id'));
                                        }
                                    }

                                });

                                $('#files_to_download').val(items.files);
                                $('#folders_to_download').val(items.folders);
                                $('#form_download').submit();



                            }}
                    },
                    callback: function(key, options) {

                    }
                });
            });
        }








        var caminho="{{route('gallery::backend::storage_assets',array($id_pasta))}}";
        var caminho_muda_item="{{route('gallery::backend::change_item_location')}}";
        var token=$('meta[name="csrf-token"]').attr('content');
        var modo_lista="{{$list_mode}}";



        var media_finder=$('.gallery_content').mediaFinder({ caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:0 });


        $("#tipo option:selected").removeAttr("selected");

        $('#tipo').multiselect({
            buttonText: function(options, select) {
                return 'tipo';
            },
            onChange: function(option, checked, select) {
                var selected=[];
                var selectedOptions = $('#tipo option:selected');
                $(selectedOptions).each(function(index, brand){
                    selected.push($(this).val());
                });

                if(selected.length > 0 ){
                    $('.gallery_content').mediaFinder({caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:0, onfiltertype: 'format', filterObjects:selected});
                }else{
                    $('.gallery_content').mediaFinder({ caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:0 });
                }

            }
        });


        $('#search_gallery').on('input', function() {
            var search_string = $(this).val();

            if(search_string.length > 0){
                $('.gallery_content').mediaFinder({caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:0, onfiltertype: 'search',filterObjects:search_string});
            }else{
                $('.gallery_content').mediaFinder({ caminho: caminho, token_laravel:token, modo_lista:modo_lista, caminho_muda_items: caminho_muda_item, arquivado:0 });
            }

        });

        $('.btn-settings').on("settings.gallery.list-mode", function(event) {
            $('.gallery_content').mediaFinder({ caminho: caminho, token_laravel:token, modo_lista:event.value, caminho_muda_items: caminho_muda_item, arquivado:0 });
        });



    })
</script>

@endsection

@section('content')
    <div class="conteudo_central">
        <div class="container-fluid">
            <div class="row">

                <div class="col-lg-12">


                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                   
                            <ul class="list-inline lista_menu">
                                <li class="">
                                    <select id="tipo" multiple="multiple">


                                        <optgroup label="Imagens" data-icon="icon-icon-upload-img">
                                            <option value="jpg">JPG</option>
                                            <option value="jpeg">JPEG</option>
                                            <option value="png">PNG</option>
                                            <option value="gif">Gif</option>
                                        </optgroup>

                                        <optgroup class="" label='Vídeo'>
                                            <option value="mkv">MKV</option>
                                            <option value="avi">AVI</option>
                                            <option value="mp4">MP4</option>
                                            <option value="h.264">H.264</option>
                                        </optgroup>

                                        <optgroup class="" label='Áudio'>
                                            <option value="wav">WAV</option>
                                            <option value="mp3">MP3</option>
                                        </optgroup>

                                        <optgroup class="" label='Documentos'>
                                            <option value="pdf">PDF</option>
                                            <option value="doc">DOC</option>
                                            <option value="docx">DOCX</option>
                                            <option value="xls">XLS</option>
                                            <option value="ppt">PPT</option>
                                        </optgroup>


                                        <optgroup class="" label='Links'>
                                            <option value="link">Embed link</option>
                                        </optgroup>

                                    </select>
                                </li>
                                <li class="">

                                </li>
                            </ul>
                        </div>
                    </div>



                    <div class="row tabs-back top_menu_page">
                        <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">

                            <ul class="list-inline fonte_12_lato">
                                <li class=" ativo "><a class="icon_voltar" href="{{route('gallery::backend::galery',array(1,'all'))}}">@lang("gallery::backend.model.index.gallery")</a></li><li><a class="icon_voltar" href="{{route('gallery::backend::galery_archive')}}" >@lang("gallery::backend.model.index.gallery_archive")</a></li>
                            </ul>
                        </div>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 text-right no_padding">
                            <ul class="list-inline">
                                @if($folder->parent_folder_id != 0)
                                    <li><a class="icon_voltar nova_pasta" data-toggle="modal" data-target="#myModal2"><i class="icon-criar-pasta "></i>@lang("gallery::backend.model.index.new_folder")</a></li>@endif @if($folder->parent_folder_id != 0)<li><a href="{{route('gallery::backend::upload_asset',array($id_pasta))}}" class="icon_eliminar">
                                        <i class="icon-icon-peq-upload"></i>
                                            @lang("gallery::backend.model.index.upload")
                                    </a>
                                </li>
                                @endif

                            </ul>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="search_gallery" class="search search_icon" type="text"  placeholder="@lang("gallery::backend.model.index.seach_gallery")">
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-10 col-md-9 col-sm-9 col-xs-9 no_padding">
                            <ul class="breadcrumb">
                                @foreach($localization as $l)
                                    <li><a href="{{route('gallery::backend::galery',array($l->id,$l->title))}}">{{$l->title}}</a></li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-3 col-xs-3  breadcrumb text-right icon_select_gallery">

                            {!! button('user-route', 'btn-brainy icon-vistas-galeria-list icon15  ', 'gallery.list-mode', 1, true) !!}
                            {!! button('user-route', 'btn-brainy icon-vistas-galeria-cards icon15', 'gallery.list-mode', 0, true) !!}


                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  msgs_gallery">



                                    @if(\Illuminate\Support\Facades\Session::get('sucess-message'))
                                        <div class="alert alert-sucess alert-dismissible " role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <i class="icon-permissoes icon9 pading_right" ></i>{{\Illuminate\Support\Facades\Session::get('sucess-message')}}
                                        </div>
                                    @endif

                                    @if(\Illuminate\Support\Facades\Session::get('error-message'))
                                        <div class="alert alert-erro alert-dismissible " role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <i class="icon-opcoes-eliminar icon9  pading_right"></i>{!!\Illuminate\Support\Facades\Session::get('error-message')!!}
                                        </div>
                                    @endif

                                        <div class="alert alert-sucess alert-dismissible location_message" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <i class="icon-permissoes icon9 pading_right" ></i>@lang("gallery::backend.messages.success.location")
                                        </div>

                                        <div class="alert alert-erro alert-dismissible location_message2" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <i class="icon-opcoes-eliminar icon9  pading_right"></i>@lang("gallery::backend.messages.error.location")
                                        </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid gallery_grid">
        <div id="loading_folders"><img src="{{asset('back/icons/loader_brainy.gif')}}" alt="loader_brainy"></div>
            <div id="empty_folder_gallery">
                <div class="col-lg-12 no_padding">
                    <p class="fonte_10_lato_light padding-top-20">@lang('gallery::backend.model.index.no_results')</p>
                    {{--<a href="{{route('gallery::backend::upload_asset',array($id_pasta))}}" class="btn btn-default btn-yellow"> <i class="icon-icon-peq-upload"></i>@lang('gallery::backend.model.index.upload')</a>--}}
                    {{--<a class="btn btn-default btn-yellow"  data-toggle="modal" data-target="#myModal2"> <i class="icon-criar-pasta "></i>@lang('gallery::backend.model.index.new_folder')</a>--}}
                </div>

            </div>
            <div id="selectable_cont" class="row gallery_content">
            </div>
            <div class="tooltip"></div>
        </div>
    </div>


    <!----------------------------------------------------- MODAL NEW FOLDER----------------------------------------------------->
    <div class="modal modal-aux" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux" role="document">
            <form class="form_elimina_user" method="POST" action="{{route('gallery::backend::backend.gallery.media-folder.store')}}" >
                <input name="parent_folder_id" type="hidden" value="{{$id_pasta}}">
                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-criar-pasta icon_20"></i>@lang("gallery::backend.model.index.new_folder")</span></div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                                <ul class="breadcrumb">

                                    <li>@lang("gallery::backend.model.index.gallery")</li>
                                    @foreach($localization as $l)
                                        <li><a href="{{route('gallery::backend::galery',array($l->id,$l->title))}}">{{$l->title}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>

                        <div class="form-group fom-style-hidden2 @if($errors->first('title') ) has-error @endif">
                            <label for="title">Título da pasta</label>
                            <input type="text" name="title" class="form-control " value="{{old('title')}}" placeholder="ex. Produto 1">
                            {!! $errors->first('title','<span class="validator_errors">:message</span>')!!}
                        </div>
                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30">
                        <button type="submit" class="btn btn-default btn-yellow">@lang("gallery::backend.model.index.create")</button>
                        <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("gallery::backend.model.index.cancel") </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!---------------------------------------------------MODAL NEW FOLDER------------------------------------------------------>



    <!----------------------------------------------------- MODAL DELETE RECORD ----------------------------------------------------->
    <div class="modal modal-aux" id="delete_media" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux" role="document">
            <form class="form_elimina_media" method="POST" action="{{route('gallery::backend::delete_media_items')}}" >
                <input id="deleted_files" name="deleted_files" type="hidden" value="">
                <input id="deleted_folders" name="deleted_folders" type="hidden" value="">
                                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-opcoes-eliminar"></i> @lang("gallery::backend.model.index.delete")</span></div>
                    <div class="modal-body">
                        <div class="col-md-12 padding-top-10"><p class="text-center"><i class="icon-eliminar-imagem icon40"></i></p></div>
                        <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">@lang("gallery::backend.model.index.remove_selected")</p></div>
                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30">
                        <button type="submit" class="btn btn-default btn-yellow">@lang("gallery::backend.model.index.delete")</button>
                        <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang("gallery::backend.model.index.cancel") </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!---------------------------------------------------MODAL DELETE RECORD  ----------------------------------------------------->


    <!----------------------------------------------------- FORM TO DOWNLOAD----------------------------------------------------->

   <form id="form_download" method="POST" action="{{route('gallery::backend::download_items_gallery')}}" >
        <input id="files_to_download" name="files" type="hidden" value="">
        <input id="folders_to_download" name="folders" type="hidden" value="">
        {!! csrf_field() !!}
    </form>
    <!---------------------------------------------------END FORM TO DOWNLOAD  ----------------------------------------------------->

    <!----------------------------------------------------- FORM TO MOVEITEMS----------------------------------------------------->

   <form id="form_move_files" method="POST" action="{{route('gallery::backend::move_media_items')}}" >
        <input id="to_move_files" name="deleted_files" type="hidden" value="">
        <input id="to_move_folders" name="deleted_folders" type="hidden" value="">
        <input id="final_fol" name="final_folder" type="hidden" value="">
        {!! csrf_field() !!}
    </form>
    <!---------------------------------------------------END FORM TO MOVE ITEMS  ----------------------------------------------------->

@endsection
