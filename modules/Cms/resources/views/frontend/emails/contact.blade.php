@extends('cms::frontend.emails.layouts.main')
@section('mail')

<div>
    <div class="email-field">
        <span>Nome:</span> {{$name}}
    </div>
    <br>
    <div class="email-field">
        <span>Telefone:</span> {{$phone}}
    </div>
    <br>
    <div class="email-field">
        <span>Email:</span> {{$email}}
    </div>
    <br>
    <div class="email-field">
        <span>Mensagem:</span> {{ $subject }}
    </div>
</div>
@stop