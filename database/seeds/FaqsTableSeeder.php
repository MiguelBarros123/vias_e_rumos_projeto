<?php

use Illuminate\Database\Seeder;
use Brainy\Framework\Models\Tag;
use Brainy\Faqs\Models\Faq;
use Brainy\Faqs\Models\FaqTranslation;
use Brainy\Faqs\Models\Theme;

class FaqsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       /* $faker = \Faker\Factory::create();

        $langs = config('translatable.locales'); // ['pt', 'en', 'fr'];

        factory(Faq::class, 'faqs', 7)->create()->each(function ($faq) use ($faker, $langs) {

            $faq->tags()->sync($faker->randomElements(Tag::lists('id')->toArray(), 7));

            if ($faker->boolean(7)) {
                $faq->theme()->associate(Theme::all()->shuffle()->first());
            }

            foreach ($langs as $lang) {
                $faq->translateOrNew($lang)->question = substr_replace($faker->sentence, '?', -1);
                $faq->translateOrNew($lang)->answer = $faker->sentence;
            }


            $faq->save();
        });*/
    }
}