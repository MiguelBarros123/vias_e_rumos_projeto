<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use Brainy\Faqs\Models as Models;

$factory->defineAs(Models\Faq::class, 'faqs', function (Faker\Generator $faker) {
    return [
        'link' => $faker->url,
        'visible' => $faker->boolean(70),
    ];
});