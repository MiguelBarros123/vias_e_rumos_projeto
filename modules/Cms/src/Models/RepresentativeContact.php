<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class RepresentativeContact extends Model
{
    protected $table = 'representative_contacts';

    public $timestamps = false;

    public function representative()
    {
    	return $this->belongsTo(Representative::class);
    }
}