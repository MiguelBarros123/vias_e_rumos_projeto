<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 09:30
 */

namespace Brainy\Cms\Models;
use Brainy\ECommerce\Models\Order;
use Illuminate\Database\Eloquent\Model;

class FrontUser extends Model
{
    protected $fillable = ['nif', 'gender', 'company', 'registration_method'];

    public function user()
    {
        return $this->belongsTo(\App\User::class, 'id');
    }

    public function addresses(){
        return $this->hasMany(UserAddress::class, 'id');
    }
}
