@extends('layouts.frontend.app')

@section('title', 'frontend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/permissions/frontend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/permissions/frontend.js')}}"></script>
@endsection

@section('content')
<p>@lang('permissions::frontend.model.index.welcome')</p>
@endsection
