<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('group_permissions')->delete();
        \Illuminate\Support\Facades\DB::table('groups')->delete();
        \Illuminate\Support\Facades\DB::table('user_details')->delete();
        \Illuminate\Support\Facades\DB::table('users')->delete();

        $group = factory(\Brainy\Framework\Models\Group::class)->create(['name'=>'Administração']);

        $group->permissions()->saveMany([
            new \Brainy\Framework\Models\GroupPermission(['type' => 'gallery::backend.permissions.see', 'permission' => 'gallery::backend.permissions.see']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'gallery::backend.permissions.create', 'permission' => 'gallery::backend.permissions.create']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'gallery::backend.permissions.edit', 'permission' => 'gallery::backend.permissions.edit']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'gallery::backend.permissions.delete', 'permission' => 'gallery::backend.permissions.delete']),

            new \Brainy\Framework\Models\GroupPermission(['type' => 'permissions::backend.permissions.see_users', 'permission' => 'permissions::backend.permissions.see_users']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'permissions::backend.permissions.create_users', 'permission' => 'permissions::backend.permissions.create_users']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'permissions::backend.permissions.edit_users', 'permission' => 'permissions::backend.permissions.edit_users']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'permissions::backend.permissions.delete_users', 'permission' => 'permissions::backend.permissions.delete_users']),


            new \Brainy\Framework\Models\GroupPermission(['type' => 'permissions::backend.permissions.see_groups', 'permission' => 'permissions::backend.permissions.see_groups']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'permissions::backend.permissions.create_groups', 'permission' => 'permissions::backend.permissions.create_groups']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'permissions::backend.permissions.edit_groups', 'permission' => 'permissions::backend.permissions.edit_groups']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'permissions::backend.permissions.delete_groups', 'permission' => 'permissions::backend.permissions.delete_groups']),


            new \Brainy\Framework\Models\GroupPermission(['type' => 'catalog::permission.attributes.see', 'permission' => 'catalog::permission.attributes.see']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'catalog::permission.attributes.create', 'permission' => 'catalog::permission.attributes.create']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'catalog::permission.products.see', 'permission' => 'catalog::permission.products.see']),
            new \Brainy\Framework\Models\GroupPermission(['type' => 'catalog::permission.products.create', 'permission' => 'catalog::permission.products.create']),

            ]);

        $details = [
            'photo' => 'back/icons/profile.jpg',
            'surname' => 'super',
            'group_id' => $group->id,
        ];

        $user = \App\User::create([
            'name' => 'admin',
            'email' => 'gestor@e-marianos.com',
            'password' => bcrypt('freire_venancio'),
        ]);
        $userDetails = factory(\Brainy\Framework\Models\UserDetail::class)->make($details);
        $userDetails->is_active = true;
        $userDetails->is_super_admin = true;
        $user->details()->save($userDetails);

        $user = \App\User::create([
            'name' => 'admin 2',
            'email' => 'admin@tsf.pt',
            'password' => bcrypt('123'),
        ]);
        $userDetails = factory(\Brainy\Framework\Models\UserDetail::class)->make($details);
        $userDetails->is_active = true;
        $userDetails->is_super_admin = true;
        $user->details()->save($userDetails);
    }
}
