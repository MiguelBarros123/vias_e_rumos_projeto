var myTemplate_types_table = "<tr id='sel_item_list2_types_table_${id}'>" +
    "<td> ${name} <input type='hidden' name='types[]' value='${id}' /></td> " +
    "<td class='text-right'><a id='delete_selected_item_datatable_list_types_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
    "</tr>";

$(document).ready(function () {
    $('.lang_tabs li:first').tab('show');
    $('.lang_content div:first').addClass('active');

    if (typeof angular != 'undefined') {
        angular.bootstrap('body', ['BootstrapToggler']);
    }

    $('.visible').bootstrapToggle();

    if(start_range != '' && end_range != ''){
        var start_date = start_range;
        var end_date = end_range;
    } else {
        var start_date = moment();
        var end_date = moment().add(5, 'days');
    }

    $('#date_range').daterangepicker({
        "locale": {
            "format": 'DD/MM/YYYY'
        },
        "minDate": start_date,
        "startDate": start_date,
        "endDate": end_date
    });

    if($('select[name="type_value"]').val() == 'relative'){
        $('.relative_signal').show();
    } else {
        $('.relative_signal').hide();
    }

    if($('select[name="type_value"]').val() == 'percentage'){
        $('.season_prices').hide();
        $('.season_percentage').show();
    } else {
        $('.season_prices').show();
        $('.season_percentage').hide();
    }

    $('select[name="type_value"]').on('change', function(){
        if($('select[name="type_value"]').val() == 'relative'){
            $('.relative_signal').show();
        } else {
            $('.relative_signal').hide();
        }

        if($('select[name="type_value"]').val() == 'percentage'){
            $('.season_prices').hide();
            $('.season_percentage').show();
        } else {
            $('.season_prices').show();
            $('.season_percentage').hide();
        }

    });

    //---------------------------------------------------------------start types
    var optionsTypes = {
        rota: selecting_types,
        colunas: [
            {data: 'name', name: 'name'},
            {data: null, defaultContent: '', orderable: false, searchable: false}
        ],
        config: {
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                //for already selected rows
                $(nRow).attr('id', 'sel_row_'+aData['id']);
                if(aData['already_selected']==1){
                    optionsTypes.datatable.row(nRow).select();
                }

            },

            initComplete: function(settings, json) {
                $(function() {
                    $.logicDatatable(optionsTypes);
                    $.selectDatatableLogic(optionsTypes);
                    $('.types_check').bootstrapToggle();


                })
            }



        },
        searchableElementId: 'types_search'
    };

    $('#types_table').mydatatable(optionsTypes);

    $('#select_types_table').on('shown.bs.modal', function (e) {

        optionsTypes.datatable.columns.adjust().draw();

    });

    $('#select_types_table').on('hide.bs.modal', function (e) {

        $('.types_check').bootstrapToggle();

    });

    $('#save_selection_types_table').on('click', function(){
        $('#select_types_table').modal('toggle');
    });

    //---------------------------------------------------------------end types

    //-----------------------------------------------------------------prices
    $('.no_iva_input').on('change keyup',function () {
        var sanitized = $(this).val().replace(/[^0-9\.\/]/g, '');
        $(this).val(sanitized);

        if($(this).val()!='' ){
            var iva=$('#selected_tax').val();
            var inicial=parseFloat($(this).val());
            var tot_iva=(inicial*iva/100);
            $(this).parent().parent().next().find('.w_iva_input').val((inicial+tot_iva).toFixed(2));
        }else{
            $(this).parent().parent().next().find('.w_iva_input').val(null)
        }
    });

    $('.w_iva_input').on('change keyup',function() {

        var sanitized = $(this).val().replace(/[^0-9\.\/]/g, '');
        $(this).val(sanitized);

        if($(this).val()!='') {
            var iva = $('#selected_tax').val();
            var inicial = parseFloat($(this).val());
            var tot_iva = parseFloat(1 + (iva / 100));
            console.log(tot_iva);
            $(this).parent().parent().prev().find('.no_iva_input').val((inicial / tot_iva).toFixed(2));
        }else{
            $(this).parent().parent().prev().find('.no_iva_input').val(null);
        }
    });
    //-----------------------------------------------------------------end prices

});
//# sourceMappingURL=seasons.js.map
