@extends('layouts.backend.app')

@section('title', trans('permissions::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
@endsection

@section('module-scripts')
    <script src="{{asset('js/permissions/backend.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>

    <script>
        var options= {
            rota: "{{ route('permissions::backend::users_archived_ajax') }}",
            colunas: [
                { data: 'foto', name: 'foto' },
                { data: 'eliminado', name: 'eliminado', orderable: true, searchable: true},
                { data: 'cargo', name: 'cargo', orderable: true, searchable: true},
                { data: 'email', name: 'email', orderable: true, searchable: true},
                { data: 'grupo', name: 'grupo' , orderable: true, searchable: true},
                { data: 'action', name: 'action', orderable: false, searchable: false},
                { data: null, defaultContent: '', orderable: false,searchable: false }
            ],
            config: {
                initComplete: function(settings, json) {
                    $(function(){
                        $.logicDatatable(options);
                    });
                },
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    //for clicable rows
                    $(nRow).attr('data-rota-see', aData['rota_final']);
                    var tr2 = $(nRow).closest('tr');
                    tr2.find("td:first").addClass('details-control');
                    tr2.find("td:not(:last, :nth-last-child(2))").addClass('clickable_url');
                }
            },
            searchableElementId: 'users_search'
        };

        var datatable = $('.default-table').mydatatable(options);

        $('.default-table').on('click', 'td.clickable_url', function () {
            var tr = $(this).closest('tr');
            window.location.href = tr.attr('data-rota-see');

        } );
    </script>

    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    @if(session('errors'))
        <script>
            $('#modalCreateUser').modal();
        </script>
    @endif
@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('permissions::backend.model.layouts.top_menu')
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="users_search" class="search search_icon" type="text"  placeholder="@lang('permissions::backend.user.archived.search')">
                        </div>
                    </div>
                    @include('layouts.backend.messages')

                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.users.archive') !!}

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                            <table id="users_table" class="table default-table">
                                <thead>
                                <tr>
                                    <th>{{ ucfirst(trans_choice('permissions::backend.users', 1)) }}</th>
                                    <th>{{ ucfirst(trans('permissions::backend.deleted')) }}</th>
                                    <th>{{ ucfirst(trans('permissions::backend.user.model.position')) }}</th>
                                    <th>{{ ucfirst(trans('permissions::backend.user.model.email')) }}</th>
                                    <th>{{ ucfirst(trans_choice('permissions::backend.groups', 1)) }}</th>
                                    <th class="multiple-options">
                                        @include('layouts.backend.partials.restore_datatables',['id'=>'users_table','route'=>'permissions::backend::restore_archived_users'])
                                    </th>
                                    <th>
                                        <label class="checkbox-default">
                                            <input type='checkbox' id="sel_all_perm_users_table">
                                            <span></span>
                                        </label>
                                    </th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="action_multiple">

        <input type="text" name="groups_id" id="groups_id" value="" class="groups_id" hidden>
        <input type="text" name="group_id" value="" hidden>

        <div class="wrap_action_multiple text-right">
            <button class="btn-change-group btn-delete-groups" type="submit"><i class="icon-opcoes-eliminar "></i>@lang('permissions::backend.model.archived.delete')</button>
        </div>
    </div>

    @include('layouts.backend.partials.restore_form',['delete_message'=>'cms::backend.common.sure_delete'])

@endsection

