<?php

namespace Brainy\Profiles\Controllers\Frontend;

use Brainy\Framework\Controllers\FrontendController;

class ModelFrontendController extends FrontendController
{
    public function index()
    {
        return view('profiles::frontend.model.index');
    }
}
