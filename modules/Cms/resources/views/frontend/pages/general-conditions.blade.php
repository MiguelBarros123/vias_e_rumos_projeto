@extends('layouts.frontend.app')

@inject('page', 'Brainy\Cms\Models\Page')

@php
$seo = $page->find(2);
@endphp

@section('title', $seo->title)
@section('description', $seo->meta_description)

@inject('inject', 'Brainy\Cms\Models\ServicePages')

@php
$content = $inject->getPageContent(2)
@endphp

<!--
divisao_ficheiro-->
