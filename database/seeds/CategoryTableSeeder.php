<?php

use Illuminate\Database\Seeder;

use Laracasts\TestDummy\Factory as TestDummy;

class CategoryTableSeeder extends Seeder {

    public function run()
    {
        $category = \Brainy\Rent\Models\Category::create([
            'order'=> 1,
            'image'=>'598055079d4947.77988428.jpeg',
            'pt' => ['name' => 'Ligeiro'],
            'en' => ['name' => 'Ligeiro'],
            'fr' => ['name' => 'Ligeiro']
        ]);
        $category = \Brainy\Rent\Models\Category::create([
            'order'=> 2,
            'image'=>'5980561dd0fd97.74474297.jpeg',
            'pt' => ['name' => 'Comercial'],
            'en' => ['name' => 'Comercial'],
            'fr' => ['name' => 'Comercial']
        ]);
    }

}