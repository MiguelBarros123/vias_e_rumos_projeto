@extends('layouts.frontend.app')

@section('title', 'Vias e Rumos')
@section('content')
<div class="mar-top-5">
    @include('cms::frontend.pages.layouts.search-bar')
</div>
<form action="{{ route('cms::frontend::model.advanced-search')}}" type="GET">
    <div class="pesquisa-avancada">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="top_av">
                    <h3>Pesquisa Avançada</h3>
                </div>

            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="line-separator">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="inp_label">Ver:</div>
                <select class="select_av" name="categoria">
                    <option value="all">Todos</option>
                    @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                    @endforeach                
                </select>

                <div class="inp_label">Tipo:</div>
                <select class="select_av" onchange="getSubTipos(this)" name="tipo">
                    <option selected value="all" >todos os tipos</option>
                    @foreach($type_goods as $type_good)
                    <option value="{{$type_good->id}}">{{$type_good->name}}</option>
                    @endforeach
                </select>

                <div class="inp_label">Sub Tipo:</div>
                <select id="subtipo" class="select_av" name="subtipo">
                    <option value="all">Selecione um tipo</option>
                </select>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="inp_label">Distrito:</div>

                <select id="distrito" class="select_av" name="distrito" onchange="getConcelho(this)">
                    <option selected value="all">Todos os distritos</option>
                    @foreach($districts as $district)
                    <option value="{{ $district->id }}">{{ $district->Designacao }}</option>     
                    @endforeach
                </select>

                <div class="inp_label">Concelho:</div>

                <select id="concelho" class="select_av" name="concelho" onchange="getFreguesia(this)">
                    <option selected value="all">Selecione um distrito</option>
                </select>

                <div class="inp_label">Freguesia:</div>

                <select id="freguesia" class="select_av" name="freguesia" >
                    <option selected value="all">Selecione uma freguesia</option>
                </select>

            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="inp_label">Valor Base:</div>
                <div class="inps_wrap"><span class="inp_sp tb_cell">de</span><input class="inp_av tb_cell" placeholder="0,00€" name="valor_base_min"/><span class="inp_sp tb_cell">de</span><input class="inp_av tb_cell" placeholder="0,00€" name="valor_base_max"/></div>
                <div class="inp_label">Valor Abertura:</div>
                <div class="inps_wrap"><span class="inp_sp tb_cell">de</span><input class="inp_av tb_cell" placeholder="0,00€" name="valor_abertura_min"/><span class="inp_sp tb_cell">de</span><input class="inp_av tb_cell" placeholder="0,00€" name="valor_abertura_max"/></div>
                <div class="inp_label">Valor Mínimo:</div>
                <div class="inps_wrap"><span class="inp_sp tb_cell">de</span><input class="inp_av tb_cell" placeholder="0,00€" name="valor_minimo_min"/><span class="inp_sp tb_cell">de</span><input class="inp_av tb_cell" placeholder="0,00€" name="valor_minimo_max"/></div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="inp_label">Processo:</div>
                <input class="inp_av inp_w_100" name="processo"/>

                <div class="inp_label">Designação:</div>
                <input class="inp_av inp_w_100" name="designacao"/>

                <div class="inp_label">Nossa referência:</div>
                <input class="inp_av inp_w_100" name="referencia"/>

            </div>

        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="pesquisar-btn-wrap">

                    <span class="btn-line">
                        <button type="submit" class="pesquisar">pesquisar
                        </button>
                    </span>
                </div>
            </div>
        </div>
    </div>
</form>

@endsection

@section('module-scripts')

<script>

    function getConcelho(distrito){
        $('#concelho').html('<option selected disabled>A carregar...</option>');
        $('#freguesia').html('<option selected value="all">Selecione um concelho</option>');
        $.ajax({
            type: 'get',
            url: "{{ route('cms::frontend::model.get_counties')}}",
            data: {
                id: distrito.value
            },
            success: function(data)
            {
                $('#concelho').html('<option selected value="all">Todos os concelhos</option>');
                $('#concelho').append(data);
            }
        });
    }

    function getFreguesia(concelho){
        $.ajax({
            type: 'get',
            url: "{{ route('cms::frontend::model.get_parishs')}}",
            data: {
                id: concelho.value
            },
            success: function(data)
            {
                $('#freguesia').html('<option selected value="all">Todos as freguesias</option>');
                $('#freguesia').append(data);
            }
        });
    }

    function getSubTipos(tipo){
        $('#subtipo').html('<option selected disabled>A carregar...</option>')

        $.ajax({
                type: 'get',
                url: "{{ route('cms::frontend::model.subtipos')}}",
                data: {
                    id: tipo.value
                },
                success: function(data)
                {
                    $('#subtipo').html('<option value="all">Todos os subtipos</option>')
                    $('#subtipo').append(data);
                }
            });
    }

</script>
@endsection
