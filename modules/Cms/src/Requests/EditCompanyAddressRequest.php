<?php

namespace Brainy\Cms\Requests;

use App\Http\Requests\Request;

class EditCompanyAddressRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'edit_name' => 'required',
            'edit_address' => 'required',
            'edit_zip_code' => 'required',
            'edit_phone' => 'required'
        ];
    }

}