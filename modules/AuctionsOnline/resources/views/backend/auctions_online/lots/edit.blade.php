@extends('layouts.backend.app')
@inject('folders', 'Brainy\Gallery\Models\MediaFolder')

@section('title', 'Leilao Presencial')

@section('module-styles')
<link rel="stylesheet" href="{{asset('css/rent/backend.css')}}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.css">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="{{asset('css/gallery/backend.css')}}">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
<!-- <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"/> -->
@endsection

@section('module-scripts')
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/rowreorder/1.1.0/js/dataTables.rowReorder.min.js"></script>
<script src="{{asset('back/js/datatablesselect.js')}}"></script>
<script src="{{asset('back/js/datatables.js')}}"></script>
<script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
<script src="{{asset('back/js/spectrum.js')}}"></script>
<script src="{{asset('js/cms/backend.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/magicsuggest/2.1.4/magicsuggest.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-sanitize.js"></script>
<script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>
<script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
<script src="{{ asset('angular/directives/toggler/toggler.directive.js') }}"></script>
<script src="{{asset('angular/directives/image_picker/multiple_image_picker.directive.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.ui.widget.js')}}"></script>
<script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>
<script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<script src="{{asset('js/cms/fileupload/jquery.iframe-transport.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-process.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-image.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-audio.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-video.js')}}"></script>
<script src="{{asset('js/cms/fileupload/jquery.fileupload-validate.js')}}"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script> -->
<script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>

<script src="{{route('framework::backend::brainy', 'gallery')}}"></script>

<script>
    $('.lang_tabs li:first-child').tab('show');
    $('.lang_content').children().first().addClass('active');
</script>

<script>
    $(document).ready(function () {
        var rotaTypeGoods = "{{route('rent::backend::backend.rent.auctions.sub_type_of_goods')}}";
        var suggest=$('#tipos_bens').magicSuggest({
                placeholder: "",
                method: 'get',
                data: rotaTypeGoods,
                allowDuplicates: false,
                valueField: 'id'
            });
    });
</script>

@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
    <div class="container-fluid">
        <div class="row tabs-back">
            <div class="col-lg-10 col-md-10 col-sm-10 no_padding">
                <ul class="list-inline">
                    <li>
                        <a href="{{route('auctionsonline::backend::auctions_online.edit', $auction->id)}}" class="icon_voltar">
                            <i class="icon-voltar pading_right"></i> Voltar
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        @include('layouts.backend.messages')
       
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                <div class="tab-content menu-content">
                    <div class="tab-pane active" id="dados" role="tabpanel">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="panel">
                                    <div class="panel-titulo">
                                        <span class="titulo">
                                            <i class="icon-editar-utilizador icon20"></i> Dados do Lote
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <div class="lots">
                                            <input type="hidden" name="lots[id]" value="{{$lot->id}}">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <h3>{{$lot->designacao}}</h3>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('designacao')) has-error @endif">
                                                        <label for="designacao_lots-field">@lang('rent::backend.common.designacao')</label>
                                                        <div>{{$lot->designacao}}</div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('name')) has-error @endif">
                                                        <label for="name_lots-field">@lang('rent::backend.common.name')</label>
                                                        <div>{{ $lot->name }}</div>
                                                    </div>
                                                </div>
                                            </div>    
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <div class="form-group @if($errors->has('tipos_bens')) has-error @endif" >
                                                        <label for="tipos_bens-field">@lang('rent::backend.common.tipos_bens')</label>
                                                        <div></div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3">
                                                    <div class="form-group @if($errors->has('price_lots')) has-error @endif">
                                                        <label for="price_lot-field">@lang('rent::backend.common.price')</label>
                                                        <div>{{number_format((float)$lot->price/100,2,'.','') }}</div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3">
                                                    <div class="form-group @if($errors->has('opening_price_lots')) has-error @endif">
                                                        <label for="opening_price_lot-field">@lang('rent::backend.common.opening_price')</label>
                                                        <div>{{number_format((float)$lot->opening_price/100,2,'.','') }}</div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-3">
                                                    <div class="form-group @if($errors->has('min_price_lots')) has-error @endif">
                                                        <label for="price_lot-field">@lang('rent::backend.common.min_price')</label>
                                                        <div>{{number_format((float)$lot->min_price/100,2,'.','')}}</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="category-field">@lang('rent::backend.common.district')</label>
                                                        <div>{{$lot->district}}</div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="category-field">@lang('rent::backend.common.county')</label>
                                                        <div>{{$lot->county}}</div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="form-group">
                                                        <label for="category-field">@lang('rent::backend.common.parish')</label>
                                                        <div>{{$lot->parish}}</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="form-group @if($errors->has('address_lots')) has-error @endif">
                                                        <label for="address_lot-field">@lang('rent::backend.common.address')</label>
                                                        <div>{{ $lot->address }}</div>
                                                    </div> 
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="form-group @if($errors->has('local_lot')) has-error @endif">
                                                        <label for="local_lot_-field">@lang('rent::backend.common.local')</label>
                                                        <div>{{$lot->local}}</div>
                                                    </div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <div class="form-group @if($errors->has('cod_postal_lots')) has-error @endif">
                                                        <label for="cod_postal_lots-field">@lang('rent::backend.common.cod_postal')</label>
                                                        <div>{{$lot->cod_postal}}</div>
                                                    </div> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('latitude_lots')) has-error @endif">
                                                        <label for="latitude_lots-field">@lang('rent::backend.common.latitude')</label>
                                                        <div>{{$lot->latitude}}</div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('longitude_lots')) has-error @endif">
                                                        <label for="longitude_lots-field">@lang('rent::backend.common.longitude')</label>
                                                        <div>{{$lot->longitude}}</div>
                                                    </div> 
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('tipologia_lots')) has-error @endif">
                                                        <label for="tipologia_lots-field">@lang('rent::backend.common.tipologia')</label>
                                                        <div>{{ $lot->tipologia }}</div>
                                                    </div> 
                                                </div>

                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('area_priv_lots')) has-error @endif">
                                                        <label for="area_priv_lots-field">@lang('rent::backend.common.area_priv')</label>
                                                        <div>{{ $lot->area_priv }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('area_dep_lots')) has-error @endif">
                                                        <label for="area_dep_lots-field">@lang('rent::backend.common.area_dep')</label>
                                                        <div>{{ $lot->area_dep }}</div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('area_total_lots')) has-error @endif">
                                                        <label for="area_total_lots-field">@lang('rent::backend.common.area_total')</label>
                                                        <div>{{ $lot->area_total }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('description_lots')) has-error @endif">
                                                        <label for="description_lots-field">@lang('rent::backend.common.description')</label>
                                                        <div>{{$lot->description}}</div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('comments_lots')) has-error @endif">
                                                        <label for="comments_lots-field">@lang('rent::backend.common.comments')</label>
                                                        <div>{!! $lot->comments !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('visits_lots')) has-error @endif">
                                                        <label for="visits_lots-field">@lang('rent::backend.common.visits_lots')</label>
                                                        <div>{{ $lot->visits }}</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('field_extra_lots')) has-error @endif">
                                                        <label for="field_extra_lots-field">@lang('rent::backend.common.field_extra_lots')</label>
                                                        <div>{{ $lot->field_extra }}</div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group @if($errors->has('another_field_extra_lots')) has-error @endif">
                                                        <label for="another_field_extra_lots-field">@lang('rent::backend.common.another_field_extra_lots')</label>
                                                        <div>{{ $lot->another_field_extra }}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="licitacoes" role="tabpanel"> 
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="panel">
                                    <div class="panel-titulo">
                                        <span class="titulo">
                                            <i class="icon-editar-utilizador icon20"></i> Licitações
                                        </span>
                                    </div>
                                    <div class="panel-body">
                                        <label for="">Licitações</label>
                                         @foreach($lot->bidding_systems as $profile)
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <h5>Foi efetuado uma licitação por parte do utilizador <a href="{{route('profilereservedarea::backend::profilereservedarea.edit', $profile->id)}}">{{$profile->firstname}} {{$profile->lastname}} [{{$profile->email}}]</a> licitou {{ $profile->pivot->price }} € em {{ $profile->pivot->created_at }}</h5>
                                                    <hr>
                                                </div>
                                            </div>
                                         @endforeach           
                                    </div>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="panel">
                    <div class="panel-titulo">
                        <span class="titulo">
                            @lang('profilereservedarea::backend.common.tabs')
                        </span>
                    </div>
                    <div class="panel-body no_padding_panel">
                        <div class="row">
                            <div class="col-lg-12 no_padding">
                                <ul class="nav nav-tabs menu-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#dados" aria-controls="dados" role="tab" data-toggle="tab">
                                            Dados
                                        </a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#licitacoes" aria-controls="licitacoes" role="tab" data-toggle="tab">
                                            Licitações
                                        </a>
                                    </li>    
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
