<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <title>Brainy</title>

        <!--  CSS FILES -->
        <link rel="stylesheet" href="{{asset('back/css/brainy.css')}}" >
        <link rel="stylesheet" href="{{asset('back/css/brainy2.css')}}" >
        <link rel="stylesheet" href="{{asset('back/font_icons/styles.css')}}" >
        @yield('module-styles')
        <!-- END CSS FILES -->
        <link rel="author" href="humans.txt">
    </head>
    <body>

    <!-- top nav bar -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand "> <img class="main_logo" src="{{asset('back/icons/logo_backoffice.svg')}}"></a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">


            <li class="dropdown " >
                <a href="#" class="dropdown-toggle remove_top_bottom_padding" data-toggle="dropdown" >
                    <div  class="row">
                        <div class="col-lg-12">
                            {{--<img class="avatar_user hidden-xs" src="{{asset(auth()->user()->details->photo)}}" >--}}
                            <i class="hidden-xs icon-icon-lateral-perfis icon30"></i>
                            {{auth()->user()->name}}<b class="caret"></b>

                        </div>
                    </div>
                </a>


                <ul class="dropdown-menu width_dropdown_menu_user">
                    <li>
                        <a href="{{ url('/logout') }}"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                    </li>
                </ul>
            </li>
        </ul>
    </nav>
    <!-- end top nav bar -->
        @include('layouts.backend.containers')
        @include('layouts.backend.modules-menu')
        @yield('content')
        @include('layouts.backend.footer')

        <!------------------------------  JS FILES -------------------------------->

        <script src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
        <script src="{{route('framework::backend::brainy',brainy_current_module())}}"></script>
        <script>

            $(document).ready(function(){
                $('.menu_lateral ul.module.active .collapse').collapse();
            });

        </script>
        <script src="{{asset('back/js/notifications.js')}}"></script>
        @yield('module-scripts')
        <!-- END JS FILES -->
    </body>
</html>
