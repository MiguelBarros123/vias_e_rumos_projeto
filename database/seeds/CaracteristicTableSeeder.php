<?php

use Illuminate\Database\Seeder;

use Laracasts\TestDummy\Factory as TestDummy;

class CaracteristicTableSeeder extends Seeder {

    public function run()
    {
        $caracteristic = \Brainy\Rent\Models\Caracteristic::create([
            'type'=>'value',
            'visibility'=>true,
            'icon'=>'598055079d4947.77988428.jpeg',
            'order'=>'1',
            'pt' => ['name' => 'Portas'],
            'en' => ['name' => 'Portas'],
            'fr' => ['name' => 'Portas']
        ]);

        $caracteristic = \Brainy\Rent\Models\Caracteristic::create([
            'type'=>'value',
            'visibility'=>true,
            'icon'=>'598055079d4947.77988428.jpeg',
            'order'=>'1',
            'pt' => ['name' => 'Portas'],
            'en' => ['name' => 'Portas'],
            'fr' => ['name' => 'Portas']
        ]);

        $caracteristic = \Brainy\Rent\Models\Caracteristic::create([
            'type'=>'value',
            'visibility'=>true,
            'icon'=>'5980561dd0fd97.74474297.jpeg',
            'order'=>'2',
            'pt' => ['name' => 'Lugares'],
            'en' => ['name' => 'Lugares'],
            'fr' => ['name' => 'Lugares']
        ]);

        $caracteristic = \Brainy\Rent\Models\Caracteristic::create([
            'type'=>'option',
            'visibility'=>true,
            'icon'=>'5980561dd0fd97.74474297.jpeg',
            'order'=>'3',
            'pt' => ['name' => 'Caixa'],
            'en' => ['name' => 'Caixa'],
            'fr' => ['name' => 'Caixa']
        ]);

        $caracteristic->options()->create([
            'pt' => ['name' => 'Manual'],
            'en' => ['name' => 'Manual'],
            'fr' => ['name' => 'Manual']
        ]);

        $caracteristic->options()->create([
            'pt' => ['name' => 'Automático'],
            'en' => ['name' => 'Automático'],
            'fr' => ['name' => 'Automático']
        ]);

        $caracteristic = \Brainy\Rent\Models\Caracteristic::create([
            'type'=>'option',
            'visibility'=>true,
            'icon'=>'5980561dd0fd97.74474297.jpeg',
            'order'=>'4',
            'pt' => ['name' => 'Combustivel'],
            'en' => ['name' => 'Combustivel'],
            'fr' => ['name' => 'Combustivel']
        ]);

        $caracteristic->options()->create([
            'pt' => ['name' => 'Gasolina'],
            'en' => ['name' => 'Gasolina'],
            'fr' => ['name' => 'Gasolina']
        ]);

        $caracteristic->options()->create([
            'pt' => ['name' => 'Diesel'],
            'en' => ['name' => 'Diesel'],
            'fr' => ['name' => 'Diesel']
        ]);
    }

}