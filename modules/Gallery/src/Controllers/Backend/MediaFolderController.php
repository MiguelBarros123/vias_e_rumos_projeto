<?php

namespace Brainy\Gallery\Controllers\Backend;

use Brainy\Framework\Controllers\BackendController;
use Brainy\Gallery\Models\MediaFolder;
use Brainy\Gallery\Requests\CreateFolderRequest;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MediaFolderController extends BackendController
{
    public function store(CreateFolderRequest $request)
    {

        $imp = array_except($request->input(), ['_token']);
        $parent = MediaFolder::findOrFail($imp['parent_folder_id']);
        $imp['localization'] = "$parent->localization/". $imp['title'];
        $imp['container_name'] = $parent->container_name;
        MediaFolder::create($imp);
        return redirect()->back()->with('sucess', 'Sucess messsage');

    }

    public function storeFolderOnUpload(Request $request)
    {

        $imp = array_except($request->input(), ['_token']);
        $parent  =MediaFolder::find($imp['parent_folder_id']);
        $imp['localization'] = "$parent->localization/" . $imp['title'];
        $imp['container_name'] = $parent->container_name;
        MediaFolder::create($imp);
        $contents = MediaFolder::all()->toJson();
        
        return response()->json([
            'conteudo' => $contents,
            'currnt_f' => $parent->id
        ]);
    }
}