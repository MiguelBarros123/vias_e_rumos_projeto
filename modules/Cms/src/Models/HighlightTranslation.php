<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 10:10
 */

namespace Brainy\Cms\Models;
use Illuminate\Database\Eloquent\Model;

class HighlightTranslation extends Model {

    public $timestamps = false;
    protected $fillable = ['title', 'description', 'subtitle', 'name_link', 'image_alt'];
    
}