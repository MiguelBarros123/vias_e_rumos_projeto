<div class="row tabs-back top_menu_page">
    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">
        <ul class="list-inline fonte_12_lato">
            <li class="@activePageTopMenu('auctionsonline::backend.auctions_online.index')">
                <a class="icon_voltar" href="{{route('auctionsonline::backend::auctions_online.index')}}">@lang('auctionsonline::backend.menu.auctions_online')</a>
            </li>
        </ul>
    </div>
</div>
