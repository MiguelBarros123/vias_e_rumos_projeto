$(document).ready(function(){

    var url = route_segments_view;
    var urlTable = route_table;

    // urlSendersTable = urlTable.replace(':id', campaign_id);

    $.get(urlTable, function(data){
        $('.segment_content').html(data);
    });

    // urlView = url.replace(':id', 0);
    // $.get(urlView, function(data){
    //     $('.segment_content').html(data);
    // });

    $('#toggle-segment').bootstrapToggle();
    $('#toggle-define-segment').bootstrapToggle();

    $('#toggle-define-segment').on('change', function(e){
        e.preventDefault();
        if($(this).prop('checked')){
            $('#toggle-segment').prop('checked', false).change();
            $('.segments_section').fadeOut();
            $('.segment_content').html('');
            $('.select_segment').val('');
            $('.use_segment').removeClass('hide_use_segment');

            urlView = url.replace(':id', 0);
            $.get(urlView, function(data){
                $('.segment_content').html(data);
            });

        } else {
            $('.segments_section').fadeOut();
            $('.segment_content').html('');
            $('.select_segment').val('');
            $('.use_segment').addClass('hide_use_segment');

            $.get(urlTable, function(data){
                $('.segment_content').html(data);
            });

        }
    });

    $('#toggle-segment').on('change', function (e) {
        e.preventDefault();
        if($(this).prop('checked')){
            $('.segments_section').fadeIn();
            $('.segment_content').html('');
            $('.select_segment').val('');
        } else {
            $('.segments_section').fadeOut();
            $('.segment_content').html('');
            $('.select_segment').val('');

            urlView = url.replace(':id', 0);
            $.get(urlView, function(data){
                $('.segment_content').html(data);
            });
        }
    });

    $('#add_new_segment').on('click', function(){
        $.ajax({
            url: $('#create_segment_form').attr('action'),
            type: 'post',
            data : $('#edit_segment_form, #create_segment_form').serialize(),
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(data){

                var msg = data['message'];

                if(data['type'] == 'success'){
                    var message = '<div class="alert alert-sucess alert-dismissible fadeInDown initial_alert" role="alert">'+
                            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                            '<span aria-hidden="true">&times;</span></button>'+
                            '<i class="icon-permissoes icon15 pading_right"></i>'+msg +''+
                            '</div>';

                    $('.ajax_message').html('');
                    $('.ajax_message').html( message);
                }

            }
        });
    });


    $('.select_segment').on('change', function(){

        var segment_id = $(this).val();

        if(segment_id != ''){
            urlView = url.replace(':id', segment_id);
            $.get(urlView, function(data){
                $('.segment_content').html(data);
            });
        }
        
    });

});
//# sourceMappingURL=edit_campaigns_receivers.js.map
