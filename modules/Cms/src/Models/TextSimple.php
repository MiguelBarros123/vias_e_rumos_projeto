<?php

namespace Brainy\Cms\Models;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class TextSimple extends Model{
    use Translatable;

    protected $table = 'texts_simple';

    public $fillable = ['panel_name'];

    public $translatedAttributes = ['description'];

    public $translationModel = TextSimpleTranslation::class;
}
