/**
 * Created by User on 27-09-2016.
 */

$(document).ready(function() {
    var options = {
        rota: rota,
        colunas: [
            {data: 'name', name: 'name'},
            {data: 'tax', name: 'tax'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
            {data: null, defaultContent: '', orderable: false, searchable: false}
        ],
        config: {
            initComplete: function (settings, json) {
                $(function () {
                    $.logicDatatable(options);
                    $('.edit_item_datatable').click(function(){

                        $('#form_update_tax').attr('action',$(this).attr('data-route'));
                        $('#name_tax').val($(this).attr('data-name'));
                        $('#tax_tax').val($(this).attr('data-tax'));
                    });
                });
            },
            fnCreatedRow: function (nRow, aData, iDataIndex) {

            }

        },
        searchableElementId: 'search'
    };

    $('.default-table').mydatatable(options);

});

//# sourceMappingURL=database_taxes.js.map
