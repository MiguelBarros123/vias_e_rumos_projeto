<?php

namespace Brainy\Cms\Providers;

use Brainy\Cms\Commands\CreatePages;
use Illuminate\Support\Facades\Blade;
use Brainy\Cms\Models\Page;
use Brainy\Cms\Models\Highlight;
use Brainy\Cms\Models\Composed;
use Brainy\Cms\Providers\URL;
use Illuminate\Support\ServiceProvider as LaravelProvider;

class CmsServiceProvider extends LaravelProvider
{

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        Blade::directive('frontActiveRoute', function ($array) {
            list($routeName, $id) = explode(',',str_replace(['([','])',' '], '', $array));

            return "<?php echo (Route::currentRouteName() == $routeName && Route::current()->getParameter('cat_id') == $id )? 'active' : '' ?>";
        });

        Blade::directive('frontIcon', function ($array) {
            list($routeName, $id) = explode(',',str_replace(['([','])',' '], '', $array));

            return "<?php echo (Route::currentRouteName() == $routeName && Route::current()->getParameter('cat_id') == $id )? 'icon-menos' : 'icon-mais' ?>";
        });

        Blade::directive('activePageCmsMenu', function ($id) {

            return "<?= (Route::currentRouteName() == 'cms::backend::pages.edit_page' && (Route::current()->getParameter('id') == $id || ($id == 1 && Route::current()->getParameter('id') == '')) ) ? 'ativo' : '' ?>";

        });
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->registerCommands();
    }

    private function registerCommands(){
        $this->commands([CreatePages::class]);
    }
}
