<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22-07-2016
 * Time: 18:03
 */

namespace Brainy\Profiles\Models;


use App\User;
use Brainy\Blog\Models\Author;
use Brainy\Catalog\Models\Variant;
use Brainy\ECommerce\Models\Cart;
use Brainy\ECommerce\Models\Order;
use Brainy\Framework\Models\Group;
use Brainy\Framework\Models\Tag;
use Brainy\Reservations\Models\Reservation;
use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Profile extends Model{

    use SoftDeletes;


    protected $fillable= [
        'type',
        'name',
        'surname',
        'nif',
        'url',
        'birth_at',
        'thumbnail',
        'job',
        'gender',
        'setor',
        'subscription_date',
        'subscript',
        'unsubscript',
        'company',
        'registration_method',
        'is_active',
        'reserved_area',
        'language',
        'identification',
        'identification_number',
        'driving_licence',
        'licence_date',
        'profile_group_id'
    ];
    protected $appends=['defaultemail'];
    protected $dates=['deleted_at','birth_at','subscription_date'];

    public function reservations()
    {
        return $this->hasMany(Reservation::class);
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function custom_fields()
    {
        return CustomField::whereIn('id',$this->custom_options()->groupBy('custom_field_id')->pluck('custom_field_id')->toArray())->get();
    }

    public function getDefaultemailAttribute()
    {
        return (is_null($this->user))? '': $this->user->email;
    }

    public function profile_contacts(){
        return $this->hasMany(ProfileContact::class);
    }

    public function profile_connections(){
        return $this->hasMany(ProfileConnection::class);
    }


    public function profile_group(){
        return $this->belongsTo(ProfileGroup::class);
    }

    public function custom_options()
    {
        return $this->belongsToMany(CustomOption::class, 'profile_custom_options', 'profile_id', 'custom_option_id')->withPivot('value');
    }

    public function users(){
        return $this->hasMany(Profile::class, 'company_id');
    }

    public function company(){
        return $this->belongsTo(Profile::class, 'company_id');
    }

    public function profile_addresses(){
        return $this->hasMany(ProfileAddress::class);
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'user_id');
    }


    public function myusers(){

        $col=collect([]);
        $all_my_users=$this->users;
        foreach ( $all_my_users as $user){
            $col->push($user);
        }


        return $col;
    }

    public function hasAcessToReservedArea(){
        if($this->reserved_area == 1){
            return true;
        }
        return false;
    }
    //encontra utilizadores cujas empresas possuam acesso a area reservada
    public function hasFullAcessToReservedArea(){

        if(auth()->check()){
            // se tiver acesso a area reservada
            if($this->hasAcessToReservedArea()){

                //encontra a empresa do utilizador
                if($this->company()->first()==null){
                    return false;
                }

                //encontra o grupo da empresa
                $my_group=$this->company()->first()->profile_group;
                
                if($my_group==null){

                    return false;

                }elseif($my_group->condition->first() == null){

                    return false;
                }else{
                    return true;
                }
            }else{
                return false;
            }

        }else{
            return false;
        }

    }

    public function scopeRegularUsers($query)
    {
        return $query;
    }


    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function mytagsnames(){
        return $this->tags()->select('id','name')->get();
    }

    public function author(){
        return $this->hasOne(Author::class);
    }

    public function mycompany(){
        return $this->company()->select('id','name')->get();
    }

    public function deliveryDate()
    {
        if($this->hasFullAcessToReservedArea()){
            $categories = collect();

            $variants = Cart::getSafeCart();
            $cart_pack = Cart::getSafePacksCart();

            if($variants){
                foreach ($variants as $v){
                    $variant = Variant::findOrFail($v->id);

                    $cart_variants = $variant->categories()->where('min_date', '!=', null)->where('max_date', '!=', null)->get(['categories.id','categories.min_date','categories.max_date']);

                    foreach ($cart_variants as $cv){
                        if(!$categories->contains(['id' => $cv->id, 'min_date' => $cv->min_date, 'max_date' => $cv->max_date])){
                            $categories->push(['id' => $cv->id, 'min_date' => $cv->min_date, 'max_date' => $cv->max_date]);
                        }
                    }

                }
            }

            if($cart_pack){

                foreach ($cart_pack as $pack){
                    $cp = \Brainy\ECommerce\Models\CartPack::where('pack_id', $pack->id)->where('cart_id', $pack->options->cart_id)->first();

                    foreach ($cp->cart_pack_variants as $variant){

                        $pack_variants = $variant->categories()->where('min_date', '!=', null)->where('max_date', '!=', null)->get(['categories.id','categories.min_date','categories.max_date']);

                        foreach ($pack_variants as $pv){
                            if(!$categories->contains(['id' => $pv->id, 'min_date' => $pv->min_date, 'max_date' => $pv->max_date])){
                                $categories->push(['id' => $pv->id, 'min_date' => $pv->min_date, 'max_date' => $pv->max_date]);
                            }
                        }

                    }

                }

            }

            if( ($categories->isEmpty()) || $categories->count() != 1 ) {
                $data['deliver'] = false;
                return $data;
            } else {
                $comercial_condition = $this->profile_group->comercial_condition->first();
                if($comercial_condition->payment_term->direct_payment == 0){
                    $data['deliver'] = true;
                    $data['min_date'] = $categories[0]['min_date'];
                    $data['max_date'] = $categories[0]['max_date'];
                    return $data;
                }
            }

        }

        $data['deliver'] = false;
        return $data;

    }

    public function getResponse($lang)
    {

        $hr = date("H");
        if($hr >= 12 && $hr<20) {
            $resp = trans('cms::frontend.messages.boa_tarde',[],$lang);}
        else if ($hr >= 0 && $hr <12 ){
            $resp = trans('cms::frontend.messages.bom_dia',[],$lang);}
        else {
            $resp = trans('cms::frontend.messages.boa_noite',[],$lang);
        }

        return $resp;
    }


}
