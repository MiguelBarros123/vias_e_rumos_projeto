<?php

namespace Brainy\Seo\Controllers\Backend;

use Brainy\Cms\Models\CHighlight;
use Brainy\Cms\Models\Highlight;
use Brainy\Cms\Models\Page;
use Brainy\Framework\Controllers\BackendController;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;

class SeoBackendController extends BackendController
{
    private $locales;

    public function __construct()
    {
        parent::__construct();
        $this->locales = config('translatable.locales');
    }

    public function index()
    {
        return view('seo::backend.seo.index', ['locales' => $this->locales]);
    }

    public function favicon()
    {
        return view('seo::backend.seo.index_favicon');
    }

    public function save_favicon(Request $request)
    {

        $file = $request->file;

        if($file->getClientOriginalName() === "favicons.zip"){

            $zip = new \ZipArchive();
            $zip->open($file->getRealPath(),\ZipArchive::CHECKCONS);

            if($zip == true){
                $zip->extractTo(base_path());
                $zip->close();
            }

        } else {
            return redirect()->back()->with('error-message', trans('seo::backend.messages.error.error_favicon'));
        }

        return redirect()->back()->with('sucess-message', trans('seo::backend.messages.success.update_favicon'));

    }

    public function allPagesAjax()
    {
        $pages = Page::all();

        return Datatables::of($pages)
            ->editColumn('pages', function ($page) {

                return $page->name;
            })
            ->editColumn('images', function ($page) {
                $images = 0;
                foreach($page->highlights as $highlight){
                   if($highlight->image != ''){
                       $images++;
                   }
                }

                foreach($page->composeds as $composed){
                    if($composed->c_hilights){
                        foreach ($composed->c_hilights as $c_highlights){
                            if($c_highlights->photo != ''){
                                $images++;
                            }
                        }
                    }
                }

                return $images;
            })
            ->make(true);
    }

    public function ajax_my_description($id)
    {
        $page = Page::findOrFail($id);
        $highlights = $page->highlights;
        $composeds = $page->composeds;

        return view("seo::backend.seo.partials.page_description")
            ->with('locales', $this->locales)
            ->with("page", $page)
            ->with("highlights", $highlights)
            ->with("composeds", $composeds)
            ->render();
    }

    public function UpdateDescription(Request $request, $id)
    {

        $page = Page::findOrFail($id);
        $input=array_except($request->input(),['_method','_token']);

        $locales=config('translatable.locales');

        foreach ($locales as $locale) {
            $page->translateOrNew($locale)->route_url = str_slug($input['route_url_'.$locale], '-');
            $page->translateOrNew($locale)->title = $input['title_'.$locale];
            $page->translateOrNew($locale)->meta_description = $input['meta_description_'.$locale];
        }

        $highlights = $request->input('highlight');

        if($highlights){
            foreach ($input['highlight'] as $key => $high){
                $highlight = Highlight::findOrFail($key);
                foreach ($locales as $locale) {
                    $highlight->translateOrNew($locale)->image_alt = $high[$locale]['image_alt'];
                }
                $highlight->save();
            }
        }

        $c_highlights = $request->input('c_highlight');

        if($c_highlights){
            foreach ($input['c_highlight'] as $key => $c_high){
                $c_highlight = CHighlight::findOrFail($key);
                foreach ($locales as $locale) {
                    $c_highlight->translateOrNew($locale)->image_alt = $c_high[$locale]['image_alt'];
                }
                $c_highlight->save();
            }
        }

        $page->save();

        return redirect()->back()->with('sucess-message', trans('seo::backend.messages.success.update_page_description'));
    }
}