<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->engine='InnoDB';
            $table->increments('id');
            $table->text('name');
            $table->text('surname');
            $table->string('thumbnail');
            $table->date('birth_at')->nullable();
            $table->enum('type',['back_user','no_login_user','login_user','reserved_area_user','company'])->default('no_login_user');
            $table->text('job')->nullable();
            $table->string('gender')->nullable();
            $table->text('nif')->nullable();
            $table->text('url')->nullable();
            $table->text('company')->nullable();
            $table->enum('registration_method',['register','order'])->default('register');
            $table->boolean('is_active')->default(false);
            $table->boolean('reserved_area')->default(false);
            $table->boolean('subscript')->default(false);
            $table->boolean('unsubscript')->default(false);
            $table->date('subscription_date')->nullable();
            $table->text('setor')->nullable();
            $table->string('language')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('company_id')->unsigned()->nullable();
            $table->foreign('company_id')->references('id')->on('profiles');
            $table->integer('profile_group_id')->unsigned()->nullable();
            $table->string('identification')->nullable();
            $table->string('identification_number')->nullable();
            $table->string('driving_licence')->nullable();
            $table->string('licence_date')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
