<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});

$factory->define(\Brainy\Framework\Models\UserDetail::class, function (Faker\Generator $faker) {
    return [
        'photo' => 'back/icons/profile.jpg',
        'surname' => $faker->name,
        'flags' => 1,
    ];
});

$factory->define(\Brainy\Framework\Models\Group::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'created_by' => $faker->safeEmail,
        'updated_by' => $faker->safeEmail,

    ];
});

$factory->defineAs(\Brainy\Framework\Models\Tag::class, 'tags', function (Faker\Generator $faker) {
    return [
        'name' => $faker->word,
    ];
});

$factory->defineAs(\Brainy\Cms\Model\Faq::class, 'faqs', function (Faker\Generator $faker) {
    return [
        'link' => $faker->url,
        'visible' => $faker->boolean(70),
    ];
});

$factory->defineAs(\Brainy\Cms\Model\FaqTranslation::class, 'faqsTranslations', function (Faker\Generator $faker) {
    return [
        'locale' => 'PT_pt',
        'question' => "$faker->sentence ?",
        'answer' => $faker->sentence,
    ];
});
