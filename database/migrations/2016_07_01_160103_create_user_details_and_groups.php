<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserDetailsAndGroups extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::create('groups', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('created_by');
            $table->string('updated_by');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('group_permissions', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('group_id')->unsigned();
            $table->string('type');
            $table->string('permission');

            $table->unique(['group_id', 'permission']);
            $table->foreign('group_id')->references('id')->on('groups');
        });

        Schema::create('user_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->integer('id')->unsigned();
            $table->integer('group_id')->unsigned()->nullable();

            $table->string('photo')->nullable();
            $table->string('surname')->nullable();
            $table->string('job_position')->nullable();
            // Flags are a bit mask to store user state
            // If bit 0 is set the user is active
            // If bit 1 is set the user is super admin
            $table->tinyInteger('flags')->unsigned()->default(0);

            $table->timestamps();
            $table->timestamp('last_login')->nullable();
            $table->softDeletes();

            $table->primary('id');
            $table->foreign('id')->references('id')->on('users');
            $table->foreign('group_id')->references('id')->on('groups');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::drop('user_details');
        Schema::drop('group_permissions');
        Schema::drop('groups');
    }
}
