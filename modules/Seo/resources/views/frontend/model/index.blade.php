@extends('layouts.frontend.app')

@section('title', 'frontend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/seo/frontend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/seo/frontend.js')}}"></script>
@endsection

@section('content')
<p>@lang('seo::frontend.model.index.welcome')</p>
@endsection
