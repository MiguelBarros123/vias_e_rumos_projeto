<?php

return [
    'model' => [
        'index' => [
            //@lang('seo::backend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view backend page',
        ],
    ],
    'messages' => [
        //@lang('seo::backend.messages.notifications.new-task')
        'notifications' => [
            'new-task' => 'You have a new pending task',
        ],
    ],
];
