<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lots', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->unsignedInteger('auction_id');
            $table->string('designacao');
            $table->unsignedInteger('price')->nullable();
            $table->unsignedInteger('opening_price')->nullable();
            $table->unsignedInteger('min_price')->nullable();
            $table->string('name');
            $table->string('district');
            $table->string('parish');
            $table->string('county');
            $table->string('local');
            $table->string('address');
            $table->string('cod_postal');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('tipologia')->nullable();
            $table->string('area_priv')->nullable();
            $table->string('area_dep')->nullable();
            $table->string('area_total')->nullable();
            $table->string('description');
            $table->string('comments');
            $table->string('visits');
            $table->string('field_extra')->nullable();
            $table->string('another_field_extra')->nullable();

            $table->timestamps();

            $table->foreign('auction_id')->references('id')->on('auctions')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lots');
    }
}
