@extends('layouts.backend.app')

@section('title', 'Registo Leiloeira')

@section('module-styles')

@endsection

@section('module-scripts')
<script>
     var label_content = '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>Escolha um ficheiro</span>';

    'use strict';

;( function( $, window, document, undefined )
{
    $( '.inputfile' ).each( function()
    {
        var $input   = $( this ),
            $label   = $input.next( 'label' ),
            labelVal = $label.html();

        $input.on( 'change', function( e )
        {
            var fileName = '';

            if( this.files && this.files.length > 1 )
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            else if( e.target.value )
                fileName = e.target.value.split( '\\' ).pop();

            if( fileName ){
                $('#reset_'+ $(this).attr("id")).removeClass('disabled');
                $label.find( 'span' ).html( fileName );
            }
            else{
                $label.html( labelVal );
            }

            console.log($input.val())
            if($input.val().length === 0){
                $('#reset_'+ $(this).attr("id")).addClass('disabled');
            }
        });

        // Firefox bug fix
        $input
        .on( 'focus', function(){ $input.addClass( 'has-focus' ); })
        .on( 'blur', function(){ $input.removeClass( 'has-focus' ); });
    });
})( jQuery, window, document );


        $('.reset_btn').on('click', function(){
            var label = $('label[for="'+$(this).attr('thefile')+'"]');
            $(this).addClass('disabled');
            label.html(label_content);
            var inp = $('#'+$(this).attr('thefile'));
            inp.val("");
            $(this).blur();
        });

</script>

@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
    <div class="container-fluid">
        <div class="row tabs-back">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                <ul class="list-inline">
                    <li><a href="{{route('cms::backend::backend.cms.registo_leiloeira.index')}}"
                       class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                   </ul>
               </div>
           </div>
           @include('layouts.backend.messages')
           <div class="row">
            <form method="POST" action="{{ route('cms::backend::backend.cms.registo_leiloeira.update', $registo_leiloeira)}}" autocomplete="off" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                {{ method_field('PUT')}}
                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20">
                                </i>
                                Registo Leiloeira
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">
                        

                                    <div class="row">
                                        <div class="col-lg-3">
                                             <span class="title_file">Escolha um ficheiro para o <b>Registo de Leiloeira</b>.</span>
                                             <div class="upload_wrap">
                                                <input type="file" name="registo_leiloeira" id="registo_leiloeira" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" accept="application/pdf" value="{{$registo_leiloeira->registo_leiloeira}}"/>
                                                <label for="registo_leiloeira" class="btn btn-yellow"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>@if($registo_leiloeira->registo_leiloeira){{ $registo_leiloeira->registo_leiloeira }}@else Escolha um ficheiro @endif</span></label>
                                                @if($registo_leiloeira->registo_leiloeira)<a class="pdf_icon" target="_blank" href="{{ route('cms::frontend::storage_pdf',[$registo_leiloeira->registo_leiloeira])  }}"><span class="icon-icon-pdf"></span></a>@endif
                                            </div>

                                            <button type="button" name="reset_registo_leiloeira" thefile="registo_leiloeira" class="btn btn-default btn-cancelar reset_btn @if(!$registo_leiloeira->registo_leiloeira) disabled @endif" id="reset_registo_leiloeira">Apagar Registo de Leiloeira</button>

                                        </div>
                                    </div>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('rent::backend.common.save')</button>
                    <a class="btn btn-default btn-cancelar" href="{{route('cms::backend::backend.cms.registo_leiloeira.index')}}">@lang('rent::backend.common.cancel')</a>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection



