<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 28-06-2016
 * Time: 15:29
 */

namespace Brainy\Cms\Commands;

use Brainy\Catalog\Models\Location;
use Brainy\Catalog\Models\Variant;
use Brainy\Cms\Models\Composed;
use Brainy\Cms\Models\ContactsInput;
use Brainy\Cms\Models\Page;
use Brainy\Cms\Models\PageContact;
use Brainy\Framework\Facades\Brainy;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class CreatePages extends Command{

    protected $file;
    private $files;
    protected $name = 'pages:create';
    protected $nameView = 'make:view';
    protected $description = 'Command for Creation of cms pages.';

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $config=$this->moduleConfigFile();
        $this->file= require $config;

        $this->files = $files;

    }


    public function handle()
    {
        if ($this->confirm('Pretende criar/alterar as paginas existentes? [y|N]')) {
            DB::statement("SET foreign_key_checks=0");
            DB::table('composed_highlight_translations')->truncate();
            DB::table('c_highlights')->truncate();
            DB::table('composeds')->truncate();
            DB::table('highlight_translations')->truncate();
            DB::table('highlights')->truncate();
            DB::table('highlight_links_translations')->truncate();
            DB::table('highlight_links')->truncate();
            DB::table('text_translations')->truncate();
            DB::table('texts')->truncate();
            DB::table('texts_simple')->truncate();
            DB::table('text_simple_translations')->truncate();
            DB::table('pages')->truncate();
            DB::table('contacts_inputs')->truncate();
            DB::table('page_contacts')->truncate();
            DB::statement("SET foreign_key_checks=1");

            foreach($this->file['menu-items'] as $menu){



                $page=Page::create(['name'=>$menu['label'],'is_contacts'=>$menu['is_contacts']]);

                foreach(config('translatable.locales') as $la){
                    $page->translateOrNew($la)->route_name=$menu['route'];
                    $page->translateOrNew($la)->route_url=$menu['route_url'];
                }
                $page->save();

                if($page->is_contacts){
                    PageContact::create([]);
                }

                $page->save();

                foreach($menu['page-content'] as $content){


                    switch ($content['type']){
                        case 'highlight':
                            $page->highlights()->create(['panel_name'=>$content['panelName'],'hasPResources'=>$content['hasPResources']]);
                            if($content['hasPResources']){
                                if(class_exists('\Brainy\Catalog\Models\Location')){
                                    Location::firstOrCreate(['resource_location'=>$content['panelName'],'is_a_page_section'=>true]);
                                }
                            }
                            break;
                        case 'text':
                            $page->texts()->create(['panel_name'=>$content['panelName'],'hasPResources'=>$content['hasPResources']]);
                            if($content['hasPResources']){
                                if(class_exists('\Brainy\Catalog\Models\Location')){
                                    Location::firstOrCreate(['resource_location'=>$content['panelName'],'is_a_page_section'=>true]);
                                }
                            }
                            break;
                        case 'text-simple':
                            $page->texts_simple()->create(['panel_name' => $content['panelName']]);    
                            break;    
                        case 'composed':
                            $composed=new Composed(['panel_name'=>$content['panelName']]);
                            $page->composeds()->save($composed);
                            break;

                        case 'input_field':
                            ContactsInput::create((['label'=>$content['label'],'section'=>$content['section'],'key'=>$content['key'],'value'=>$content['value']]));
                            break;
                        default:
                            break;
                    }
                }

                $this->createViews(strtolower(str_slug($page->name)), $page->id);

            }

            //add seeds



            $this->info('Paginas criadas com sucesso!');
        }


    }

    protected function moduleConfigFile()
    {
        return base_path("modules/Cms/config/pages.php");
    }

    protected function createViews($view, $id)
    {
        $search = '/(divisao_ficheiro)/';

        if ($this->files->exists($path = $this->getPath($view))) {
            if($this->confirm("A vista $view ja existe pretende criar novamente? [Y|n]")) {

                $matches = preg_split($search, file_get_contents($path));

                $this->files->put($path, str_replace($matches[0], view('cms::frontend.template.end', [
                    'lc_module_name' => 'cms',
                    'id' => $id
                ])->render(), file_get_contents($path)));

            }
        } else {
            $this->makeDirectory($path);

            $this->files->put($path, view('cms::frontend.template.end_division', [
                'lc_module_name' => 'cms',
                'id' => $id
            ])->render());

        }

        $this->info("Vista $path criada com sucesso.");

    }

    private function makeDirectory($path)
    {
        $dir = dirname($path);

        if (!$this->files->isDirectory($dir)) {
            $this->files->makeDirectory($dir, 0777, true, true);
        }
    }

    public function getPath($view)
    {
        return './modules/cms/resources/views/frontend/pages/' . str_replace('.', '/', $view) . '.blade.php';
    }

    public function fire()
    {
        return $this->makeView();
    }

}
