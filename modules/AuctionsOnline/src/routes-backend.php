<?php

Route::group(['namespace' => 'Brainy\AuctionsOnline\Controllers\Backend'], function () {
    Route::get('auctions_online','AuctionsOnlineBackendController@index')->name('auctions_online.index');
    Route::get('auctions_online/index-ajax','AuctionsOnlineBackendController@index_ajax')->name('auctions_online.index_ajax');
    Route::get('auctions_online/edit/{id}','AuctionsOnlineBackendController@edit')->name('auctions_online.edit');
    Route::delete('auctions_online/delete','AuctionsOnlineBackendController@delete')->name('auctions_online.delete');

    Route::get('auctions_online/lots/{id}/index-ajax','AuctionsOnlineBackendController@index_ajax_lots')->name('auctions_online.index_ajax_lots');
    Route::get('auctions_online/lots/edit/{id}','AuctionsOnlineBackendController@edit_lots')->name('auctions_online_lots.edit');
});
