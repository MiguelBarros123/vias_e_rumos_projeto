<?php

namespace Brainy\Cms\Controllers\Backend;


use Brainy\Cms\Models\MediaItem;
use Brainy\Cms\Models\Member;
use Brainy\Framework\Controllers\BackendController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Facades\Datatables;
use Illuminate\Http\Request;

class MemberBackendController extends BackendController
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $members = Member::orderBy('id', 'desc')->paginate(10);

        return view('cms::backend.members.index', compact('members'));
    }

    public function index_ajax()
    {

        $members = Member::all();

        return Datatables::of($members)
            ->addColumn('action', function ($tax) {
                $rota=route('cms::backend::backend.cms.members.delete');
                $rotaeditar=route('cms::backend::backend.cms.members.edit',[$tax->id]);
                $route_see=route('cms::backend::backend.cms.members.update',[$tax->id]);

                $impossible=0;


                $string = '
                        <div class="btn-group pull-right">
                           <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                                 <li><a  class="edit_item_datatable"  href="'.$route_see.'"><i class="icon-visivel"></i>  ' . trans("rent::backend.common.see") . '</a></li>
                            <li><a  class="edit_item_datatable" href="'.$rotaeditar.'" ><i class="icon-editar"></i>  ' . trans("rent::backend.common.edit") . '</a></li>
                            <li><a data-impossible-delete="'.$impossible.'" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $tax->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("rent::backend.common.delete") . ' </a></li>
                          </ul>
                        </div>

                       ';
                return $string;
            })
            ->addColumn('rota_final', function ($product) {
                return route('cms::backend::backend.cms.members.show', array($product->id));
            })
            ->make(true);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

        $locales = config('translatable.locales');

        $data = [
            'locales' => $locales
        ];

        return view('cms::backend.members.create')->with($data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {

        $member = Member::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'email' => $request->email
        ]);

        $input=array_except($request->input(),['_method','_token']);
        $locales=config('translatable.locales');
        foreach ($locales as $locale) {
            $member->translateOrNew($locale)->job = $input['job_'.$locale];
        }

        if($request['image_id'] != ''){

            $image = MediaItem::findOrFail($request['image_id']);
            $image->used = 1;
            $image->save();
            $member->photo = $image->identifier_token;
        }

        $member->save();
        
        if($request->submit === 'stay'){
            return redirect()->route('cms::backend::backend.cms.members.edit',[$member->id])->with('sucess-message', trans('cms::backend.messages.success.global_create'));
        }

        return redirect()->route('cms::backend::backend.cms.members.index')->with('sucess-message', trans('cms::backend.messages.success.global_create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $member = Member::findOrFail($id);

        $locales = config('translatable.locales');

        $data = [
            'locales' => $locales
        ];

        return view('cms::backend.members.show', compact('member'))->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $member = Member::findOrFail($id);

        $locales = config('translatable.locales');

        $data = [
            'locales' => $locales
        ];

        return view('cms::backend.members.edit', compact('member'))->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param Request $request
     * @return Response
     */
    public function update(Request $request, $id)
    {

        $member = Member::findOrFail($id);

        $member->name = $request->name;
        $member->phone = $request->phone;
        $member->email = $request->email;

        $input=array_except($request->input(),['_method','_token']);

        $locales=config('translatable.locales');
        foreach ($locales as $locale) {
            $member->translateOrNew($locale)->job = $input['job_'.$locale];
        }

        if($request['image_id'] != ''){
            $image = MediaItem::findOrFail($request['image_id']);
            $image->used = 1;
            $image->save();
            $member->photo = $image->identifier_token;
        }

        $member->save();

        if($request->submit === 'stay'){
            return redirect()->back()->with('sucess-message', trans('cms::backend.messages.success.global_update'));
        }

        return redirect()->route('cms::backend::backend.cms.members.index')->with('sucess-message', trans('cms::backend.messages.success.global_update'));
    }

    /**
     * Delete the specified resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function delete(Request $request){

        $rows=explode(',',$request->deleted_rows);

        foreach($rows as $id){
            $member = Member::findOrFail($id);
            $member->delete();
        }

        return redirect()->route('cms::backend::backend.cms.members.index')->with('sucess-message', trans('cms::backend.messages.success.global_delete'));
    }

}