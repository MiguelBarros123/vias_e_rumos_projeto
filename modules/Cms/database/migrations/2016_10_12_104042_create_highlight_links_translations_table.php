<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHighlightLinksTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('highlight_links_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('locale')->index();
            $table->string('name_link');
            $table->string('link');
            $table->integer('highlight_link_id')->unsigned();
            $table->timestamps();

            $table->unique(['highlight_link_id','locale']);
            $table->foreign('highlight_link_id')->references('id')->on('highlight_links')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('highlight_links_translations');
    }
}
