@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/profiles/backend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/profiles/backend.js')}}"></script>
@endsection

@section('content')
<p>@lang('profiles::backend.model.index.welcome')</p>
@endsection
