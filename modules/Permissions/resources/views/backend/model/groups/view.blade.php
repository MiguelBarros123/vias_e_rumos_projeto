@extends('layouts.backend.app')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/profiles/backend.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/backend.css') }}">
@endsection

@section('module-scripts')



@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <a class="pull-right icon_eliminar icon-editar pading_right icon_delete_inside_view_show"
                       href="{{ route('permissions::backend::group.edit', array($grupo->id)) }}">
                        Editar
                    </a>
                    <ul class="list-inline">
                        <li><a href="{{route('permissions::backend::group.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.groups.show',$grupo) !!}
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo"><i class="icon-ficha icon10"></i>@lang('permissions::backend.common.group')</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <div class="col-lg-12 margem_20">

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group">
                                                <label for="name">@lang('profiles::backend.common.name')</label>
                                                <p class="form-control-static">{{ $grupo->name }}</p>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo"><i class="icon-editar-utilizador icon20"></i>@lang('permissions::backend.common.users')</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <table class="table dataTable default-table2">
                                    <thead>
                                    <tr class="profiles-view-table">
                                        <th>@lang('profiles::backend.menu.profiles')</th>
                                        <th>@lang('permissions::backend.common.email')</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if($grupo->users()->regularUsers()->count())

                                        @foreach($grupo->users()->regularUsers()->get() as $user)
                                            <tr>
                                                <td>
                                                    <div class="company_user_img">
                                                        @if($user->photo === '')
                                                            <div class="pic-circle-corner2 text-center profile_no_pic">
                                                                <i class="icon-avatar-person icon10 "></i>
                                                            </div>
                                                        @else
                                                            <img class="image_user_picked2" src="{{route('gallery::frontend::storage_image_p',[$user->photo,50,50])}}">
                                                        @endif
                                                    </div>
                                                    <div class="company_user_info">
                                                        <div>{{ $user->user->name }} {{ $user->surname }}</div>
                                                        <div class="profile_input">{{ $user->job }}</div>
                                                    </div>
                                                </td>
                                                <td>
                                                    @if($user->user->email != '')
                                                        <i class="icon-mail"></i> {{ $user->user->email }}
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @else
                                        <tr>
                                            <td class="text-center" colspan="4">
                                                @lang('definitionsecommerce::backend.common.no_results')
                                            </td>
                                        </tr>
                                    @endif
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="panel ">
                        <div class="panel-titulo">
                            <span class="titulo"><i class="icon-editar-utilizador icon20"></i>@lang('permissions::backend.common.permissions')</span>
                        </div>
                        <div class="panel-body">
                            <div class="row ">
                                <ul class="list-group panel treeview_permissions">
                                    @foreach($modulos as $modulo)

                                        @if($modulo->authorizesGroup($grupo, false))

                                            <li>
                                                <label class="checkbox-default  checkbox_permissions">
                                                    <a href="#modulo_{{$modulo->name}}" class="list-group-item collapsed"
                                                       data-toggle="collapse">
                                                        @lang($modulo->label)
                                                        @if(count($modulo->modules)>0)<i
                                                                class="icon-baixo"></i>@endif
                                                    </a>
                                                </label>


                                                <ul class="collapse list-group-submenu" id="modulo_{{$modulo->name}}">
                                                    @foreach(\Brainy\Framework\Facades\Brainy::containerModules($modulo->name) as $assunto)

                                                        @if($assunto->authorizesGroup($grupo,false))

                                                            <li>
                                                                <label class="checkbox-default  checkbox_permissions">
                                                                    <a href="#assunto_{{$assunto->name}}"
                                                                       class="list-group-item collapsed" data-toggle="collapse"
                                                                       data-parent="#assunto_{{$assunto->name}}">
                                                                        @lang($assunto->label)
                                                                        @if(count($assunto->permissions)>0)<i
                                                                                class="icon-baixo"></i>@endif
                                                                    </a>
                                                                </label>

                                                                <ul class="collapse list-group-submenu"
                                                                    id="assunto_{{$assunto->name}}">
                                                                    @foreach($assunto->permission_types as $database_permition)

                                                                        @if($grupo->hasPermission($database_permition))
                                                                            <li class="perm_spacer">
                                                                                <label class="checkbox-default  checkbox_permissions group_view_permissions">
                                                                                    @lang($database_permition)
                                                                                </label>
                                                                            </li>
                                                                        @endif

                                                                    @endforeach
                                                                </ul>

                                                            </li>

                                                        @endif

                                                    @endforeach
                                                </ul>
                                            </li>

                                        @endif

                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>

@endsection