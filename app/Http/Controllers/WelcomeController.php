<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 21-10-2016
 * Time: 14:32
 */

namespace App\Http\Controllers;


class WelcomeController  extends Controller{

    public function index()
    {
        return redirect()->route('cms::frontend::index')->with('c_lang',1);
    }
}