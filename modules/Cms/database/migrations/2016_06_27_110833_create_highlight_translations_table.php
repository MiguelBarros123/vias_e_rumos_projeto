<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHighlightTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('highlight_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('locale')->index();
            $table->integer('highlight_id')->unsigned();

            $table->string('title');
            $table->text('description');
            $table->string('subtitle');
            $table->string('name_link');
            $table->text('image_alt');


            $table->unique(['highlight_id','locale']);
            $table->foreign('highlight_id')->references('id')->on('highlights')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('highlight_translations');
    }
}
