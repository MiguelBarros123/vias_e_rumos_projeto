<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class SubMenuBiblioteca extends Model
{
    protected $guarded = [];

    public function menus_biblioteca()
    {
        return $this->belongsTo(MenuBiblioteca::class);
    }

    public function getRouteKeyName()
    {
        return 'name';
    }
}
