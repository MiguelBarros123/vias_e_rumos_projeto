<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class ChangeCoin
{
    public function handle($request, Closure $next)
    {
        if(!Session::has('coin'))
        {
            Session::put('coin', 'EUR');
        }

        return $next($request);
    }
}
