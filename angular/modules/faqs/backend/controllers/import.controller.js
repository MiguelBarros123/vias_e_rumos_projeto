(function () {
	'use strict';

	function ImportFaqsThemesController($scope) {

		var $fileSelector = $('#themes-faqs');

		$scope.openFileSelector = function() {
			$fileSelector.click();
		};
	}

	angular.module('brainy.faqs.backend').controller('ImportFaqsThemesController', ['$scope', ImportFaqsThemesController]);	
})();