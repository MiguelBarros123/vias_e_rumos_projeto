<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 24/05/2016
 * Time: 18:18
 */

namespace Brainy\Profiles\Repositories;





use App\User;
use Brainy\DefinitionsEcommerce\Models\Country;
use Brainy\ECommerce\Models\Emails;
use Brainy\Profiles\Models\ProfileEmails;
use Brainy\Framework\Models\Group;
use Brainy\Framework\Models\Tag;
use Brainy\Framework\Models\UserDetail;
use Brainy\Gallery\Models\MediaItem;
use Brainy\Profiles\Models\CustomOption;
use Brainy\Profiles\Models\GlobalCountry;
use Brainy\Profiles\Models\Profile;
use Brainy\Profiles\Models\ProfileAddress;
use Brainy\Profiles\Models\ProfileGroup;
use Brainy\Profiles\Models\ProfileUser;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProfileRepository implements ProfileRepositoryInterface
{

    protected $profile;

    public function _construct(Profile $profile){
        $this->profile = $profile;
    }


    public function edit($id)
    {
        $data = array();

        if(class_exists('Brainy\DefinitionsEcommerce\Models\Country')){
            $countries=Country::all();
        } else {
            $countries=GlobalCountry::all();
        }


        $back_groups=Group::all();
        $front_groups=ProfileGroup::all();
        $profile=Profile::find($id);
        $tags = $profile->mytagsnames()->toJson();
        $front_companys=Profile::where('type','company')->get();

        $addresses = ProfileAddress::where('profile_id', $profile->id)->get();

        return $data = [
            'profile' => $profile,
            'countries' => $countries,
            'back_groups' => $back_groups,
            'front_groups' => $front_groups,
            'front_companys' => $front_companys,
            'addresses' => $addresses
        ];
    }

    public function create()
    {

        $data = array();

        if(class_exists('Brainy\DefinitionsEcommerce\Models\Country')){
            $countries=Country::all();
        } else {
            $countries=GlobalCountry::all();
        }

        $back_groups=Group::all();
        $front_groups=ProfileGroup::all();
        $front_companys=Profile::where('type','company')->get();

        return $data = [
            'countries' => $countries,
            'back_groups' => $back_groups,
            'front_groups' => $front_groups,
            'front_companys' => $front_companys,
        ];
    }

    public function store($request)
    {

        $thumbnail='';

        if(isset($request->image_id)){
            $file = $request->image_id;
            $thumbnail = $file->getClientOriginalName();
            $file->move(Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix(), $file->getClientOriginalName());
            $request->request->add(['profile_image' => Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$file->getClientOriginalName()]);
        }

        $birth_at=$request->birth_year.'-'.$request->birth_month.'-'.$request->birth_day;
        $type=$request->type;
        if(array_key_exists('is_user',$request->input())==false){
            $type='no_login_user';
        }

        $group = null;

        if($request->is_company==='company'){
            $type='company';
            $group = null;
        }

        if(array_key_exists('front_group_id',$request->input())){
            $group = $request->front_group_id;
        }

        $url = null;

        if(array_key_exists('url',$request->input())){
            $url=$request->url;
        }

        $surname = '';

        if(array_key_exists('surname',$request->input())){
            $surname=$request->surname;
        }

        $profile = Profile::create([
            'name'=>$request->name,
            'surname'=>$surname,
            'nif'=>$request->nif,
            'url'=>$url,
            'thumbnail'=>$thumbnail,
            'type'=>$type,
            'birth_at'=>$birth_at,
            'job'=>$request->job,
            'gender'=>$request->gender,
            'setor'=>$request->setor,
            'language'=>$request->language,
            'profile_group_id'=>$group
        ]);

        //for custom fields
        if(array_key_exists('custom_field',$request->input())){
            $profile->custom_options()->sync($request->custom_field);
            $profile->save();
        }
        if(array_key_exists('custom_field_multiple',$request->input())){

            foreach ($request->custom_field_multiple as $custom_multiple){
               $cus_option=CustomOption::findOrFail($custom_multiple);
                $profile->custom_options()->attach($cus_option->id,['value'=>$cus_option->name]);
                $profile->save();
            }

        }


        //for tags
        if(isset($request['tags'])){
            $ids=[];
            foreach($request['tags']as $t){
                $tag=Tag::where('name',$t)->first();
                if($tag!=null){
                    array_push($ids,$tag->id);
                }else{
                    $new=Tag::create(['name'=>$t]);
                    array_push($ids,$new->id);
                }
            }
            $profile->tags()->sync($ids);
            $profile->save();
        }else{
            $profile->tags()->sync([]);
            $profile->save();
        }

        if(array_key_exists('profile_ids',$request->input())){
            foreach ($request->profile_ids as $user){
                $p_user = Profile::findOrFail($user);
                $p_user->company_id = $profile->id;

                if(array_key_exists('profile_ra',$request->input())){
                    if(array_key_exists($p_user->id,$request['profile_ra'])){
                        $p_user->reserved_area = 1;
                    } else {
                        $p_user->reserved_area = 1;
                    }
                } else {
                    $p_user->reserved_area = 1;
                }

                $p_user->save();

            }
        }

        if($type !== 'no_login_user' && $type !== 'company'){

            $user = User::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password)
            ]);

            $data = [
                'user' => $user,
                'email' => $request->email,
                'password' => $request->password,
                'lang' => session('locale')
            ];

            $profile->user_id = $user->id;
            $profile->is_active = 1;

            $profile->save();

            if($type === 'back_user'){

                $back_group = null;

                if(array_key_exists('back_group_id',$request->input())){
                    $back_group = $request->back_group_id;
                }

                $details = [
                    'photo' => $thumbnail,
                    'surname' => $surname
                ];

                $userDetails = factory(\Brainy\Framework\Models\UserDetail::class)->make($details);
                $userDetails->is_active = true;
                $userDetails->is_super_admin = false;
                $userDetails->group_id = $back_group;
                $user->details()->save($userDetails);
            }

            User::sendUserContacts($data, $request->email);

        }

        if(array_key_exists('company_id', $request->input())){

            $company = Profile::where('id', $request['company_id'][0])->first();

            if(!$company)
                $company = Profile::create([
                    'name' => $request['company_id'][0],
                    'type' => 'company'
                ]);

            $profile->company_id = $company->id;

            $profile->save();

        }

        $profile->reserved_area = 0;

        $profile->save();

        $this->save_profile_addresses($profile, $request);

        $this->save_profile_contacts($profile, $request);

        return $profile;

    }

    public function update($request, $id)
    {

        $thumbnail='';

        if(isset($request->image_id)){
            $file = $request->image_id;
            $thumbnail = $file->getClientOriginalName();
            $file->move(Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix(), $file->getClientOriginalName());
            $request->request->add(['profile_image' => Storage::disk('galeria')->getDriver()->getAdapter()->getPathPrefix().$file->getClientOriginalName()]);
        }


        $birth_at=$request->birth_year.'-'.$request->birth_month.'-'.$request->birth_day;
        $type=$request->type;
        if(array_key_exists('is_user',$request->input())==false){
            $type='no_login_user';
        }

        $group = null;

        if($request->is_company==='company'){
            $type='company';
            $group = null;
        }

        if(array_key_exists('front_group_id',$request->input())){
            $group = $request->front_group_id;
        }

        $url = null;

        if(array_key_exists('url',$request->input())){
            $url=$request->url;
        }

        $surname = '';

        if(array_key_exists('surname',$request->input())){
            $surname=$request->surname;
        }

        $profile = Profile::findOrFail($id);

        $profile->update([
            'name'=>$request->name,
            'surname'=>$surname,
            'nif'=>$request->nif,
            'url'=>$url,
            'thumbnail'=>$thumbnail,
            'type'=>$type,
            'birth_at'=>$birth_at,
            'job'=>$request->job,
            'gender'=>$request->gender,
            'setor'=>$request->setor,
            'language'=>$request->language,
            'profile_group_id'=>$group
        ]);

        $profile->custom_options()->sync([]);
        //for custom fields
        if(array_key_exists('custom_field',$request->input())){
            $profile->custom_options()->sync($request->custom_field);
            $profile->save();
        }
        if(array_key_exists('custom_field_multiple',$request->input())){

            foreach ($request->custom_field_multiple as $custom_multiple){
                $cus_option=CustomOption::findOrFail($custom_multiple);
                $profile->custom_options()->attach($cus_option->id,['value'=>$cus_option->name]);
                $profile->save();
            }

        }

        //for tags
        if(isset($request['tags'])){
            $ids=[];
            foreach($request['tags']as $t){
                $tag=Tag::where('name',$t)->first();
                if($tag!=null){
                    array_push($ids,$tag->id);
                }else{
                    $new=Tag::create(['name'=>$t]);
                    array_push($ids,$new->id);
                }
            }
            $profile->tags()->sync($ids);
            $profile->save();
        }else{
            $profile->tags()->sync([]);
            $profile->save();
        }

        foreach ($profile->users as $user){
            $user->company_id = null;

            $user->save();
        }

        if(array_key_exists('profile_ids',$request->input())){

            foreach ($request->profile_ids as $user){
                $p_user = Profile::findOrFail($user);
                $p_user->company_id = $profile->id;

                if(array_key_exists('profile_ra',$request->input())){
                    if(array_key_exists($p_user->id,$request['profile_ra'])){
                        $p_user->reserved_area = 1;
                    } else {
                        $p_user->reserved_area = 0;
                    }
                } else {
                    $p_user->reserved_area = 0;
                }

                $p_user->save();
            }
        }

        if($type === 'no_login_user'){

            $user = User::find($profile->user_id);

            if($user){
                $user->details()->delete();
            }

            $profile->type = 'no_login_user';

            $profile->save();

        }


        if($type !== 'no_login_user' && $type !== 'company'){


            if(array_key_exists('email', $request->input())){

                $user = User::create([
                    'name' => $request->name,
                    'email' => $request->email,
                    'password' => bcrypt($request->password)
                ]);

                $data = [
                    'user' => $user,
                    'email' => $request->email,
                    'password' => $request->password,
                    'lang' => session('locale')
                ];

                User::sendUserContacts($data, $request->email);
            }


            if($profile->user_id){

                $user = User::findOrFail($profile->user_id);

                $userD = UserDetail::withTrashed()->where('id', $profile->user_id)->first();

                if($userD){
                    $userD->restore();
                }

                if($type === 'back_user'){

                    $back_group = null;

                    if(array_key_exists('back_group_id',$request->input())){
                        $back_group = $request->back_group_id;
                    }

                    $details = [
                        'photo' => $thumbnail,
                        'surname' => $surname
                    ];

                    if($userD){
                        $userD->group_id = $back_group;
                        $userD->save();
                    } else {
                        $userDetails = factory(\Brainy\Framework\Models\UserDetail::class)->make($details);
                        $userDetails->is_active = true;
                        $userDetails->is_super_admin = false;
                        $userDetails->group_id = $back_group;
                        $user->details()->save($userDetails);
                    }

                }

            }

            $profile->is_active = 1;

            $profile->save();

        }


        if(array_key_exists('company_id', $request->input())){

            $company = Profile::where('id', $request['company_id'][0])->first();

            if(!$company)
                $company = Profile::create([
                    'name' => $request['company_id'][0],
                    'type' => 'company'
                ]);

            $profile->company_id = $company->id;

            $profile->save();

        }

        $profile->reserved_area = 0;

        $profile->save();

        $userDetails = UserDetail::where('id', $profile->user_id)->first();

        $profile->profile_addresses()->delete();
        $profile->profile_contacts()->delete();



        $this->save_profile_addresses($profile, $request);
        $this->save_profile_contacts($profile, $request);
    }

    private function save_profile_contacts($profile, $request)
    {

        if(array_key_exists('personal_emails', $request->input())){
            foreach ($request->personal_emails as $p_email){
                if($p_email !== ''){
                    $profile->profile_contacts()->create([
                        'type'=>'personal_email',
                        'contact'=> $p_email
                    ]);
                }
            }
        }

        if(array_key_exists('company_emails', $request->input())){
            foreach ($request->company_emails as $c_email){
                if($c_email !== ''){
                    $profile->profile_contacts()->create([
                        'type'=>'company_email',
                        'contact'=> $c_email
                    ]);
                }
            }
        }

        if(array_key_exists('personal_phones', $request->input())){
            foreach ($request->personal_phones as $p_phone){
                if($p_phone !== ''){
                    $profile->profile_contacts()->create([
                        'type'=>'personal_phone',
                        'contact'=> $p_phone
                    ]);
                }
            }
        }

        if(array_key_exists('company_phones', $request->input())){
            foreach ($request->company_phones as $c_phone){
                if($c_phone !== ''){
                    $profile->profile_contacts()->create([
                        'type'=>'company_phone',
                        'contact'=> $c_phone
                    ]);
                }
            }
        }

        if(array_key_exists('personal_mobile', $request->input())){
            foreach ($request->personal_mobile as $p_mobile){
                if($p_mobile !== ''){
                    $profile->profile_contacts()->create([
                        'type'=>'personal_mobile',
                        'contact'=> $p_mobile
                    ]);
                }
            }

        }

        if(array_key_exists('company_mobile', $request->input())){
            foreach ($request->company_mobile as $c_mobile){
                if($c_mobile !== ''){
                    $profile->profile_contacts()->create([
                        'type'=>'company_mobile',
                        'contact'=> $c_mobile
                    ]);
                }
            }
        }

        if(array_key_exists('personal_faxes', $request->input())){
            foreach ($request->personal_faxes as $p_fax){
                if($p_fax !== ''){
                    $profile->profile_contacts()->create([
                        'type'=>'personal_fax',
                        'contact'=> $p_fax
                    ]);
                }
            }
        }

        if(array_key_exists('company_faxes', $request->input())){
            foreach ($request->company_faxes as $c_fax){
                if($c_fax !== ''){
                    $profile->profile_contacts()->create([
                        'type'=>'company_fax',
                        'contact'=> $c_fax
                    ]);
                }
            }
        }

        if($request->social_facebook != ''){
            $profile->profile_contacts()->create([
                'type'=>'facebook',
                'contact'=> $request->social_facebook
            ]);
        }

        if($request->social_instagram != ''){
            $profile->profile_contacts()->create([
                'type'=>'instagram',
                'contact'=> $request->social_instagram
            ]);
        }

        if($request->social_google != ''){
            $profile->profile_contacts()->create([
                'type'=>'google',
                'contact'=> $request->social_google
            ]);
        }

        if($request->social_youtube != ''){
            $profile->profile_contacts()->create([
                'type'=>'youtube',
                'contact'=> $request->social_youtube
            ]);
        }

        foreach ($request->other as $other) {
            if($other !== ''){
                $profile->profile_contacts()->create([
                    'type'=>'other',
                    'contact'=> $other
                ]);
            }
        }

    }

    private function save_profile_addresses($profile, $request)
    {

        foreach ($request->moradas as $morada){
            if($profile->profile_addresses()->count() == 0){
                $active = 1;
            } else {
                $active = 0;
            }

            $surname = '';

            if(array_key_exists('surname',$request->input())){
                $surname=$request->surname;
            }

            $profile->profile_addresses()->save( new ProfileAddress([
                'name' => $request->name,
                'last_name' => $surname,
                'address' => $morada['moradas_send_address'],
                'zipcode' => $morada['moradas_send_zip_code'],
                'company' => $morada['moradas_send_company'],
                'town' => $morada['moradas_send_locality'],
                'phone' => $morada['moradas_send_phone'],
                'city' => $morada['moradas_send_city'],
                'country' => $morada['moradas_send_country'],
                'active_address' => $active
            ]));
        }

    }

    public function getMonths()
    {
        return $months = [
            1 => 'Janeiro',
            2 => 'Fevereiro',
            3 => 'Março',
            4 => 'Abril',
            5 => 'Maio',
            6 => 'Junho',
            7 => 'Julho',
            8 => 'Agosto',
            9 => 'Setembro',
            10 => 'Outubro',
            11 => 'Novembro',
            12 => 'Dezembro'
        ];
    }

    public function getLanguages()
    {
        return $languages = [
            "AF"=>"Afrikanns",
            "SQ"=>"Albanian",
            "AR"=>"Arabic",
            "HY"=>"Armenian",
            "EU"=>"Basque",
            "BN"=>"Bengali",
            "BG"=>"Bulgarian",
            "CA"=>"Catalan",
            "KM"=>"Cambodian",
            "ZH"=>"Chinese (Mandarin)",
            "HR"=>"Croation",
            "CS"=>"Czech",
            "DA"=>"Danish",
            "NL"=>"Dutch",
            "EN"=>"English",
            "ET"=>"Estonian",
            "FJ"=>"Fiji",
            "FI"=>"Finnish",
            "FR"=>"French",
            "KA"=>"Georgian",
            "DE"=>"German",
            "EL"=>"Greek",
            "GU"=>"Gujarati",
            "HE"=>"Hebrew",
            "HI"=>"Hindi",
            "HU"=>"Hungarian",
            "IS"=>"Icelandic",
            "ID"=>"Indonesian",
            "GA"=>"Irish",
            "IT"=>"Italian",
            "JA"=>"Japanese",
            "JW"=>"Javanese",
            "KO"=>"Korean",
            "LA"=>"Latin",
            "LV"=>"Latvian",
            "LT"=>"Lithuanian",
            "MK"=>"Macedonian",
            "MS"=>"Malay",
            "ML"=>"Malayalam",
            "MT"=>"Maltese",
            "MI"=>"Maori",
            "MR"=>"Marathi",
            "MN"=>"Mongolian",
            "NE"=>"Nepali",
            "NO"=>"Norwegian",
            "FA"=>"Persian",
            "PL"=>"Polish",
            "PT"=>"Portuguese",
            "PA"=>"Punjabi",
            "QU"=>"Quechua",
            "RO"=>"Romanian",
            "RU"=>"Russian",
            "SM"=>"Samoan",
            "SR"=>"Serbian",
            "SK"=>"Slovak",
            "SL"=>"Slovenian",
            "ES"=>"Spanish",
            "SW"=>"Swahili",
            "SV"=>"Swedish ",
            "TA"=>"Tamil",
            "TT"=>"Tatar",
            "TE"=>"Telugu",
            "TH"=>"Thai",
            "BO"=>"Tibetan",
            "TO"=>"Tonga",
            "TR"=>"Turkish",
            "UK"=>"Ukranian",
            "UR"=>"Urdu",
            "UZ"=>"Uzbek",
            "VI"=>"Vietnamese",
            "CY"=>"Welsh",
            "XH"=>"Xhosa",
        ];
    }

    public function addProfile($request)
    {

//        dd($request->except('_token'));
//
        $type=$request->type;


        if(array_key_exists('is_user',$request->input())==false){
            $type='no_login_user';
        }

        $group = null;

        if($type === 'back_user'){
            if(array_key_exists('profile_b_front_group_id',$request->input())){
                $group = $request->profile_b_front_group_id;
            }
        } else {
            if(array_key_exists('profile_f_front_group_id',$request->input())){
                $group = $request->profile_f_front_group_id;
            }
        }

        $profile = Profile::create([
            'name'=>$request->profile_name,
            'surname'=>$request->profile_surname,
            'type'=>$type,
            'profile_group_id'=>$group
        ]);


        if($type !== 'no_login_user'){

            $user = User::create([
                'name' => $request->profile_name,
                'email' => $request->profile_email,
                'password' => bcrypt($request->profile_password)
            ]);

            $data = [
                'user' => $user,
                'email' => $request->profile_email,
                'password' => $request->profile_password,
                'lang' => session('locale')
            ];

            $profile->user_id = $user->id;
            $profile->is_active = 1;

            $profile->save();

            if($type === 'back_user'){

                $back_group = null;

                if(array_key_exists('profile_back_group_id',$request->input())){
                    $back_group = $request->profile_back_group_id;
                }

                $details = [
                    'surname' => $request->profile_surname
                ];

                $userDetails = factory(\Brainy\Framework\Models\UserDetail::class)->make($details);
                $userDetails->is_active = true;
                $userDetails->is_super_admin = false;
                $userDetails->group_id = $back_group;
                $user->details()->save($userDetails);
            }

            User::sendUserContacts($data, $request->profile_email);

        }


        $profile->reserved_area = 0;

        $profile->save();

        return $profile;

    }

}
