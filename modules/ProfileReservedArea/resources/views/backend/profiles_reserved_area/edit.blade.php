@extends('layouts.backend.app')

@section('title', trans('rent::backend.model.index.title'))

@section('module-styles')
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css"/>
@endsection

@section('module-scripts')
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<script>
    $('#data_validade').daterangepicker({
        "timePicker": false,
        "singleDatePicker": true,
        "showDropdowns": false,
        "sideBySide": false,
        "locale": {
            "format": 'DD/MM/YYYY',
            "cancelLabel": 'Limpar',
            "applyLabel": 'Aplicar'
        },
    }, function (start, end, label) {
        $('#range span').html(start.format('DD/MM/YYYY'));
    });
</script>
@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
    <div class="container-fluid">
        <div class="row tabs-back">
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                <ul class="list-inline">
                    <li><a href="{{route('profilereservedarea::backend::profilereservedarea.index')}}" class="icon_voltar"><i
                        class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>
            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('profiles.edit') !!}
            <div class="row">
                <form action="{{route('profilereservedarea::backend::profilereservedarea.update', $profile->id)}}" method="POST" autocomplete="off">
                    {!! csrf_field() !!}
                    {{ method_field('PUT')}}
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                        <div class="tab-content menu-content">
                            <div class="tab-pane active" id="dados" role="tabpanel">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel">
                                            <div class="panel-titulo">
                                                <span class="titulo">
                                                    <i class="icon-editar-utilizador icon20"></i>Perfil - Dados para Faturação
                                                </span>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row no_margin_row">
                                                    <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group fom-style-hidden2 @if($errors->first('nome_fiscal') ) has-error @endif">
                                                            <label>Nome Fiscal</label>
                                                            <input type="text" name="nome_fiscal" class="form-control user_form_properties " value="{{$profile->nome_fiscal}}">
                                                            {!! $errors->first('nome_fiscal','<span class="validator_errors">:message</span>')!!}
                                                        </div>
                                                    </div>

                                                    <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group fom-style-hidden2 @if($errors->first('nipc') ) has-error @endif">
                                                            <label>NIF/NIPC</label>
                                                            <input type="text" name="nipc" class="form-control user_form_properties" value="{{$profile->nipc}}">
                                                            {!! $errors->first('nipc','<span class="validator_errors">:message</span>')!!}
                                                        </div>
                                                    </div>

                                                    <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group fom-style-hidden2 @if($errors->first('country') ) has-error @endif">
                                                            <label>País</label>
                                                            <select class="form-control" name="country">
                                                                @foreach($countries as $key => $a)
                                                                <option value="{{ $a->id }}" {{ (($a->id) == old('country'))? 'selected': ''}}>{{ $a->value}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group fom-style-hidden2 @if($errors->first('address') ) has-error @endif">
                                                            <label>Morada</label>
                                                            <input type="text" name="address" class="form-control user_form_properties " value="{{$profile->address}}">
                                                            {!! $errors->first('address','<span class="validator_errors">:message</span>')!!}
                                                        </div>
                                                    </div>

                                                    <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group fom-style-hidden2 @if($errors->first('cod_postal') ) has-error @endif">
                                                            <label>Código Postal</label>
                                                            <input type="text" name="cod_postal" class="form-control user_form_properties " value="{{$profile->cod_postal}}">
                                                            {!! $errors->first('cod_postal','<span class="validator_errors">:message</span>')!!}
                                                        </div>
                                                    </div>
                                                    <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                        <div class="form-group fom-style-hidden2 @if($errors->first('local') ) has-error @endif">
                                                            <label>Localidade</label>
                                                            <input type="text" name="local" class="form-control user_form_properties " value="{{$profile->local}}">
                                                            {!! $errors->first('local','<span class="validator_errors">:message</span>')!!}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div> </div>
                                    <div class="col-lg-12">
                                        <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('rent::backend.common.save')</button>
                                        <button type="submit" name="submit" value="stay" class="btn btn-default btn-yellow btn-stay btn-submit-all">@lang('rent::backend.common.save_and_stay')</button>
                                        <a class="btn btn-default btn-cancelar"
                                        href="{{route('profilereservedarea::backend::profilereservedarea.index')}}">@lang('rent::backend.common.cancel')</a>
                                    </div>   
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                    <div class="panel">
                                        <div class="panel-titulo">
                                            <span class="titulo">
                                                <i class="icon-editar-utilizador icon20"></i>Perfil - Dados Pessoais
                                            </span>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row no_margin_row">
                                                <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('firstname') ) has-error @endif">
                                                        <label>Nome</label>
                                                        <input type="text" name="firstname" class="form-control user_form_properties " value="{{$profile->firstname}}">
                                                        {!! $errors->first('firstname','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>

                                                <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('lastname') ) has-error @endif">
                                                        <label>Apelido</label>
                                                        <input type="text" name="lastname" class="form-control user_form_properties" value="{{$profile->lastname}}">
                                                        {!! $errors->first('lastname','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>

                                                <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('nif') ) has-error @endif">
                                                        <label>Nif</label>
                                                        <input type="text" name="nif" class="form-control user_form_properties " value="{{$profile->nif}}">
                                                        {!! $errors->first('nif','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>

                                                <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('cartao_cidadao') ) has-error @endif">
                                                        <label>Cartão de Cidadão</label>
                                                        <input type="text" name="cartao_cidadao" class="form-control user_form_properties " value="{{$profile->cartao_cidadao}}">
                                                        {!! $errors->first('cartao_cidadao','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>

                                                <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('data_validade') ) has-error @endif">
                                                        <label>Data de Validade</label>
                                                        <input type="text" name="data_validade" class="form-control"  id="data_validade" value="{{\Carbon\Carbon::parse($profile->data_validade)->format('d/m/Y')}}"/>
                                                        <span></span>
                                                        {!! $errors->first('data_validade','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>

                                                <div  class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                                    <div class="form-group fom-style-hidden2 @if($errors->first('telefone') ) has-error @endif">
                                                        <label>Telefone / Telemóvel</label>
                                                        <input type="text" name="telefone" class="form-control user_form_properties " value="{{$profile->telefone}}">
                                                        {!! $errors->first('telefone','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="historico" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <div class="panel">
                                            <div class="panel-titulo">
                                                <span class="titulo">
                                                    Histórico
                                                </span>
                                            </div>
                                            <div class="panel-body">
                                                @foreach($profile->bidding_systems as $bidding)
                                                    <div class="row">
                                                        <div class="col-lg-12 no_padding">
                                                            <h5>Fez uma licitação de {{ $bidding->pivot->price }} € em {{ $bidding->pivot->created_at }} no lote {{$bidding->designacao}}</h5>
                                                        </div>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                        <div class="panel">
                            <div class="panel-titulo">
                                <span class="titulo">
                                    @lang('profilereservedarea::backend.common.tabs')
                                </span>
                            </div>
                            <div class="panel-body no_padding_panel">
                                <div class="row">
                                    <div class="col-lg-12 no_padding">
                                        <ul class="nav nav-tabs menu-tabs" role="tablist">
                                            <li role="presentation" class="active">
                                                <a href="#dados" aria-controls="definitions" role="tab" data-toggle="tab">
                                                    Dados
                                                </a>
                                            </li>
                                            <li role="presentation">
                                                <a href="#historico" aria-controls="rules" role="tab" data-toggle="tab">
                                                    Histórico
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>    
        </div>
    </div>
    @endsection
