<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 15-07-2016
 * Time: 18:00
 */
namespace Brainy\Gallery\Exceptions;

class ChunkUploadException extends \Exception{

    public function __construct($message)
    {
        parent::__construct($message);
    }

}

