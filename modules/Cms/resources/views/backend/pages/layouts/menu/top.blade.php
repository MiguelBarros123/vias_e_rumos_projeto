@inject('pages', 'Brainy\Cms\Models\Page')

<div class="row tabs-back top_menu_page">
    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">
        <ul class="list-inline fonte_12_lato">
            @foreach($pages->all() as $pa)
            <li class="@activePageTopMenu($page->route)">
                <a class="icon_voltar" href="{{ route('cms::backend::pages.edit_page', [$pa->id]) }}">{{ $pa->name }}</a>
            </li>
           @endforeach
        </ul>
    </div>
    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 text-right tabs-back no_padding">
        <ul class="list-inline ">
            @if($page->highlights->count() > 0 || $page->texts->count() > 0 || $page->is_contacts)
                <li>
                    <a class="icon_eliminar" href="{{ route('permissions::backend::group.create') }}">
                        <i class="icon-group pading_right"></i>Guardar
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>