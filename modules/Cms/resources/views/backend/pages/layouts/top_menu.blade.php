@inject('pages', 'Brainy\Cms\Models\Page')

<div class="row tabs-back top_menu_page">
    <div class="col-lg-9 col-md-8 col-sm-6 col-xs-6 no_padding">
        <ul class="list-inline fonte_12_lato">
            @foreach($pages->all() as $pa)
            <li class="@activePageCmsMenu($pa->id)">
                <a class="icon_voltar" href="{{ route('cms::backend::pages.edit_page', [$pa->id]) }}">{{ $pa->pageTranslation() }}</a>
            </li>
           @endforeach
           <li class="@activePageTopMenu('cms::backend::backend.cms.menu_institucional.index') }}">
                <a href="{{ route('cms::backend::backend.cms.menu_institucional.index') }}" class="icon_voltar">Menu Institucional</a>
            </li>
            <li class="@activePageTopMenu('cms::backend::backend.cms.menu_biblioteca.index') }}">
                <a href="{{ route('cms::backend::backend.cms.menu_biblioteca.index') }}" class="icon_voltar">Menu Biblioteca</a>
            </li>
            <li class="@activePageTopMenu('cms::backend::backend.cms.sub_menus_biblioteca.index') }}">
                <a href="{{ route('cms::backend::backend.cms.sub_menus_biblioteca.index') }}" class="icon_voltar">Sub-Menu Biblioteca</a>
            </li>
            <li class="@activePageTopMenu('cms::backend::backend.cms.registo_leiloeira.index') }}">
                <a href="{{ route('cms::backend::backend.cms.registo_leiloeira.index') }}" class="icon_voltar">Registo Leiloeira</a>
            </li>
        </ul>
    </div>
</div>
