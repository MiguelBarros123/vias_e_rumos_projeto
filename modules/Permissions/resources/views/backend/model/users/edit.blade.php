@extends('layouts.backend.app')

@section('title', trans('permissions::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/permissions/backend.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/backend.css') }}">
    <link rel="stylesheet" href="{{asset('back/css/select2.min.css')}}">
@endsection

@section('module-scripts')

    <script src="{{asset('back/js/select2.min.js')}}"></script>

    <script>
        $(document).ready(function(){

            $(".back_groups").select2();

            $('.recover-user-password').click(function(){
                $('#modalRecoverUserPassword').modal('show');
            });

            $('.delete-user').click(function(){
                $('.form_elimina_user').attr('action', $(this).attr('data-href'));
                $('.user-name').text($(this).attr('data-user'));
                $('#myModal').modal('show');
            });

            $('.multiselect-container li a').on('click', function(e){
                e.keyup(13);
            });

            $('#user-image').on('click', function (e) {
                e.preventDefault();
                $('#fileupload').click();
            });

            $("#fileupload").on('change', function(){
                readURL(this);
            });


            $('#btn-unlock-user').on('click', function () {
                var url = "{{ route('permissions::backend::model.change_user_state', ':id') }}";
                url = url.replace(':id', {{ $user->id }});

                $.ajax({
                    url         : url,
                    type        : 'post',
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data        : {'id':{{ $user->id }}},
                    success:function(data)
                    {
                        $('.alert-user-locked').hide();
                        $('#btn-unlock-user').hide();
                    }
                });
            })

        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    document.getElementById('user-image').style.backgroundImage = 'url(' + e.target.result +')';
                    $('#user-image img').hide();
                    $('#user-image i').hide();
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

    </script>

@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{ route('permissions::backend::model.index') }}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>
            </div>
            @include('layouts.backend.messages')

            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.users.edit',$user->details) !!}
            <div class="row">
                    <form class="" method="POST" action="{{ route('permissions::backend::model.update',array($user->id)) }}" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PUT">
                    {!! csrf_field() !!}
                    <div class="col-lg-12">
                        <div class="panel ">
                            <div class="panel-titulo">
                                <span class="titulo"><i class="icon-editar-utilizador icon20"></i>@lang('permissions::backend.user.edit.title')</span>
                                @if(! $user->details->is_active )
                                    <span class="alert-user-locked">
                                        <i class="icon-bloqueado icon20" ></i>
                                        @lang('permissions::backend.user.edit.info-blocked-account-n-tries')
                                    </span>

                                    <a id="btn-unlock-user" href="javascript:;" class="btn-unlock-user">Desbloquear conta</a>
                                @endif
                            </div>
                            <div class="panel-body">
                                  <div class="row border-bottom-user-edit">
                                      <div class="col-lg-2">
                                          @php
                                                $img = \Brainy\Gallery\Models\MediaItem::where('identifier_token', $user->details->photo)->first();
                                          @endphp
                                          <a href="javascript:;" type="button" data-target="#upload_resource" data-toggle="modal">
                                              <div id="user-image" class=" {{ ($user->details->photo) ? 'margin_user_thunbnail': ''}} pic-circle-corner-create-user profile_no_pic text-center">
                                                  @if($user->details->photo)
                                                      <img class="image_user_picked2 user_thumbnail_image" src="{{ route('gallery::frontend::storage_image_p',[$user->details->photo,150,150]) }}"/>
                                                  @else
                                                      <i class="icon-avatar-person icon50 icon-create-user-color-padding"></i>
                                                  @endif
                                              </div>
                                              <div class="pic-circle-corner2 image_user_picked margin_user_thunbnail"><img id="preview_file" class="image_user_picked2 user_thumbnail_image" /></div>
                                          </a>
                                          @if($img)
                                              <input id="input_image" type="hidden" name="image_id" value="{{ $img->id }}">
                                          @else
                                              <input id="input_image" type="hidden" name="image_id">
                                          @endif

                                      </div>
                                      <input id="fileupload" type="file" name="profile_image">
                                      <div class="col-lg-4" style="border-right: 1px solid #F0F1F1;margin-bottom: 20px;padding-bottom: 20px">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="form-group fom-style-hidden3 @if($errors->first('name') ) has-error @endif">
                                                        <label for="name">@lang('permissions::backend.user.index.name')</label>

                                                        <input type="text" class="form-control" value="{{ $user->name }}" name="name">
                                                        {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group fom-style-hidden3 @if($errors->first('surname') ) has-error @endif">
                                                        <label for="surname">@lang('permissions::backend.user.index.surname')</label>

                                                        <input type="text" class="form-control" value="{{ $user->details->surname }}" name="surname">
                                                        {!! $errors->first('surname','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <div class="form-group fom-style-hidden3 @if($errors->first('job_position') ) has-error @endif">
                                                        <label for="job_position">@lang('permissions::backend.user.index.position')</label>

                                                        <input type="text" class="form-control" value="{{ $user->details->job_position }}" name="job_position">
                                                        {!! $errors->first('job_position','<span class="validator_errors">:message</span>')!!}
                                                    </div>
                                                </div>
                                          </div>
                                          <div class="row padding-top-35">
                                              <div class="form-group fom-style-hidden3">
                                                  <div class="col-lg-4">
                                                      <label style="padding-top: 6px"><i class="icon-email icon15"></i>
                                                        {{ ucfirst(trans('permissions::backend.user.model.email')) }}
                                                      </label>
                                                  </div>
                                                  <div class="col-lg-8">
                                                      <div class="form-group fom-style-hidden3 @if($errors->first('email') ) has-error @endif">
                                                          <input type="text" class="form-control" value="{{ $user->email }}" name="email">
                                                          {!! $errors->first('email','<span class="validator_errors">:message</span>')!!}
                                                      </div>
                                                  </div>
                                              </div>
                                              <div class="col-lg-12 padding-top-20">
                                                <div class="form-group fom-style-hidden ">
                                                  <a class="btn btn-default btn-cancelar recover-user-password"><i class="icon-recuperar-password-cabecalho icon15"></i>
                                                    @lang('permissions::backend.user.edit.actions.recover-password')
                                                  </a>
                                                </div>
                                              </div>
                                          </div>
                                      </div>
                                      <div class="col-lg-5 col-lg-offset-1">
                                          <div class="row">
                                              <div class="form-group fom-style-hidden3">
                                                  <div class="col-lg-4">
                                                      <label style="padding-top: 6px">
                                                        <i class="icon-ultimo-login icon15"></i>
                                                        @lang('permissions::backend.user.edit.last-login')
                                                      </label>
                                                  </div>
                                                    <div class="col-lg-8">
                                                      <input readonly="yes" disabled class="form-control form-control-disabled"
                                                      value="{{ isset($user->details->last_login) ? '$user->details->last_login' : trans('permissions::backend.user.edit.no-login-record') }}">
                                                    </div>
                                              </div>
                                          </div>
                                          <div class="row padding-top-20">
                                              <div class="form-group fom-style-hidden3">

                                                  <div  class="col-lg-12">
                                                      <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                                          <label>@lang('profiles::backend.common.group_front')</label>
                                                          <div>
                                                              @include('layouts.backend.partials.select_object_input', ['array'=>$front_groups, 'name'=>'front_group_id', 'selected'=> $user->profile->profile_group_id, 'class'=> 'back_groups'])
                                                          </div>
                                                      </div>
                                                  </div>
                                                  <div  class="col-lg-12">
                                                      <div class="form-group fom-style-hidden2 @if($errors->first('subbrand') ) has-error @endif">
                                                          <label>@lang('profiles::backend.common.group_admin')</label>
                                                          <div>
                                                              @include('layouts.backend.partials.select_object_input', ['array'=>$back_groups, 'name'=>'back_group_id', 'selected'=> $user->details->group_id, 'class'=> 'back_groups'])
                                                          </div>
                                                      </div>
                                                  </div>

                                              </div>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="row padding-top-20">
                                      <div class="col-lg-12">
                                          <div class="form-group fom-style-hidden ">
                                              <a href="{{route('profiles::backend::profiles.edit',[$user->profile->id,'person'])}}" class="btn btn-default btn-cancelar">
                                                  @lang('permissions::backend.user.edit.edit_profile')
                                              </a>
                                          </div>
                                      </div>
                                  </div>
                            </div>
                        </div>
                        <div class="row">
                          <div class="col-lg-12">
                                <button type="submit" class="btn btn-default btn-save-user ">
                                  {{ strtoupper(trans('permissions::frontend.model.edit.save')) }}
                                </button>
                                <span class="margin-right-20"></span>
                                <a href="{{ route('permissions::backend::model.index') }}" class="btn btn-default btn-cancelar ">
                                    {{ strtoupper(trans('permissions::frontend.model.edit.cancel')) }}
                                </a>
                            </div>
                        </div>
                    </div>
                    </form>
            </div>
          <div class="modal modal-aux" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-dialog-aux" role="document">
                <form class="form_elimina_user" 
                      method="POST"
                      action="{{ route('permissions::backend::users.delete_multiple') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" name="_method" value="DELETE">
                    <input type="text" name="deleted_rows" id="deleted_rows" value="{{ $user->id }}" class="groups_id" hidden>

                    <div class="modal-content">
                        <div class="panel-titulo"><span class="titulo"><i class="icon-opcoes-eliminar"></i>
                        {{ strtoupper(trans('permissions::backend.user.edit.actions.remove')) }}
                        </span></div>
                        <div class="modal-body">
                            <div class="col-md-12 padding-top-10"><p class="text-center"><i class="icon-eliminar-user-destaque icon30"></i></p></div>
                            <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato"><p class="text-center">
                              {{ trans_choice('permissions::backend.user.ask.delete', 1, ['user' => $user->name]) }}
                            </div>
                        </div>
                        <div class="modal-footer modal-eliminar padding-bottom-30">
                            <button type="submit" class="btn btn-default btn-yellow">
                              {{ strtoupper(trans('permissions::backend.archive')) }}
                            </button>
                            <button type="button" class="btn fonte_12_lato_regular" data-dismiss="modal">
                              {{ strtoupper(trans('permissions::backend.cancel')) }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="modal modal-aux" id="modalRecoverUserPassword" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-dialog-aux" role="document">
                <form class="form_recover_user" method="POST" action="{{ route('permissions::backend::model.recoverUserPassword') }}">
                    {!! csrf_field() !!}
                    <input type="hidden" class="form-control"
                    placeholder="@lang('permissions::backend.placeholder.email')"
                    name="@lang('permissions::backend.user.model.email')"
                    value="{{ $user->email }}">

                    <div class="modal-content">
                        <div class="panel-titulo">
                          <span class="titulo"><i class="icon-recuperar-password-cabecalho icon20"></i>
                          @lang('permissions::backend.user.edit.actions.recover-password'))
                          </span>
                        </div>
                        <div class="modal-body">

                            <div class="col-md-12 padding-top-20 padding-bottom-20 fonte_15_lato">
                                <p class="text-center">{{ trans('permissions::backend.user.ask.recover-password', ['user' => $user->name]) }}</p>
                            </div>
                        </div>
                        <div class="modal-footer modal-eliminar padding-bottom-30">
                            <button type="submit" class="btn btn-default btn-yellow">
                            {{ strtoupper(trans('permissions::backend.recover')) }}
                            </button>
                            <button type="button" class="btn fonte_12_lato_regular" data-dismiss="modal">
                            {{ strtoupper(trans('permissions::backend.cancel')) }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        
        </div>
    </div>

    {{-- 
    <div class="action_multiple" style="background-color: red; z-index: 9999999999999999999999999999; top: 45%">
        <div class="wrap_action_multiple text-right">
            <a> <img src="{{ asset('back/icons/opcoes_eliminar.png') }}">{{ strtoupper(trans('permissions::backend.remove')) }}</a>
        </div>
    </div>
     --}}
@endsection



