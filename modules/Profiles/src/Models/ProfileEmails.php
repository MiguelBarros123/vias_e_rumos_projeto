<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-10-2016
 * Time: 09:56
 */

namespace Brainy\Profiles\Models;


use Illuminate\Support\Facades\Mail;

class ProfileEmails {


    public static function sendEmails($data,$order){
        //encomenda para user
        $sucessos=Mail::send('ecommerce::backend.emails.success_buy_user', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', trans('ecommerce::frontend.messages.msg_email_automatico6'));

//            $message->to($order->order_addresses()->billing()->first()->email)->subject(trans('ecommerce::frontend.messages.msg_email_automatico6').' - '.$order->id);

            /*LOCAL*/
            $message->to('jose.venancio@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('ecommerce::frontend.messages.msg_email_automatico6').' - '.$order->id);
            /*LOCAL*/


        });



        //encomenda para shop

        $sucessos=Mail::send('ecommerce::backend.emails.success_buy_company', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', 'Nova encomenda');

            //email real $message->to('online.store@biblicallust.com')->subject('Nova encomenda - '.$order->id);
            $message->to('hugo.freire@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject('Nova encomenda - '.$order->id);

        });


    }

    //para os pagamentos multibanco
    public static function sendEmailToCompany($data,$order){

        //para a loja
        $sucessos=Mail::send('ecommerce::backend.emails.confirmed_atm_payment_store', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', 'Referências multibanco');
            // email real $message->to('online.store@biblicallust.com')->subject('Confirmação de pagamento - ' . $order->id);
            $message->to('hugo.freire@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject('Confirmação de pagamento - ' . $order->id);

        });

        //para o cliente
        $sucessos=Mail::send('ecommerce::backend.emails.confirmed_atm_payment_user', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', 'Referências multibanco');

//            $message->to($order->order_addresses()->billing()->first()->email)->subject('Confirmação de pagamento - ' . $order->id);

            /*LOCAL*/
            $message->to('jose.venancio@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject('Confirmação de pagamento - ' . $order->id);
            /*LOCAL*/

        });
    }

    //para as encomendas enviadas
    public static function sendOrderEmail($data,$order){

        //para o cliente
        $sucessos=Mail::send('ecommerce::backend.emails.success_send_order', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', trans('ecommerce::frontend.messages.msg_email_automatico121', ['number' => $order->id]));

//            $message->to($order->order_addresses()->shipping()->first()->email)->subject(trans('ecommerce::frontend.messages.msg_email_automatico121'). ' - ' . $order->id);

            /*LOCAL*/
            $message->to('jose.venancio@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('ecommerce::frontend.messages.msg_email_automatico121', ['number' => $order->id]));
            /*LOCAL*/


        });
    }
    
    public static function sendUserContacts($data, $email)
    {
        //para o cliente
        $sucessos=Mail::send('profiles::backend.emails.new_user', $data, function($message) use($email)
        {
            $message->from('online.store@biblicallust.com', trans('cms::frontend.pages.login.new-client'));

//            $message->to($email)->subject(trans('cms::frontend.pages.login.new-client'));

            /*LOCAL*/
            $message->to('jose.venancio@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('cms::frontend.pages.login.new-client'));
            /*LOCAL*/


        });
    }

    public static function goodDay(){
        $hr = date("H");
        if($hr >= 12 && $hr<20) {
            $resp = trans('ecommerce::backend.messages.boa_tarde');}
        else if ($hr >= 0 && $hr <12 ){
            $resp = trans('ecommerce::backend.messages.bom_dia');}
        else {
            $resp = trans('ecommerce::backend.messages.boa_noite');
        }
        return $resp;
    }

    //para os pagamentos àrea reservada
    public static function reservedEmails($data,$order){
        //encomenda para user
        $sucessos=Mail::send('ecommerce::backend.emails.success_reserved_user', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', trans('ecommerce::frontend.messages.msg_email_automatico61', ['number' => $order->id]));

//            $message->to($order->order_addresses()->billing()->first()->email)->subject(trans('ecommerce::frontend.messages.msg_email_automatico6').' - '.$order->id);

            /*LOCAL*/
            $message->to('ana.carreira@thesilverfactory.pt')->bcc('jacv_88@hotmail.com')->subject(trans('ecommerce::frontend.messages.msg_email_automatico61', ['number' => $order->id]));
            /*LOCAL*/


        });



        //encomenda para shop

        $sucessos=Mail::send('ecommerce::backend.emails.success_reserved_company', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', trans('ecommerce::frontend.messages.msg_email_automatico6'));

            //email real $message->to('online.store@biblicallust.com')->subject('Nova encomenda - '.$order->id);
            $message->to('hugo.freire@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('ecommerce::frontend.messages.msg_email_automatico6'));

        });


    }

    //para os pagamentos àrea reservada
    public static function backofficeEmails($data,$order){
        //encomenda para user
        $sucessos=Mail::send('ecommerce::backend.emails.success_back_user', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', trans('ecommerce::frontend.messages.msg_email_automatico61', ['number' => $order->id]));

//            $message->to($order->order_addresses()->billing()->first()->email)->subject(trans('ecommerce::frontend.messages.msg_email_automatico6').' - '.$order->id);

            /*LOCAL*/
            $message->to($order->order_addresses()->billing()->first()->email)->bcc('hugo.freire@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('ecommerce::frontend.messages.msg_email_automatico61', ['number' => $order->id]));
            /*LOCAL*/


        });



        //encomenda para shop

        $sucessos=Mail::send('ecommerce::backend.emails.success_back_company', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', trans('ecommerce::frontend.messages.msg_email_automatico63', ['number' => $order->id]));

            //email real $message->to('online.store@biblicallust.com')->subject('Nova encomenda - '.$order->id);
            $message->to('hugo.freire@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('ecommerce::frontend.messages.msg_email_automatico63', ['number' => $order->id]));

        });


    }

    public static function sendProcessedEmail($data, $order)
    {
        //encomenda para user
        $sucessos=Mail::send('ecommerce::backend.emails.success_processed_order', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', trans('ecommerce::frontend.messages.msg_email_automatico62', ['number' => $order->id]));

//            $message->to($order->order_addresses()->billing()->first()->email)->subject(trans('ecommerce::frontend.messages.msg_email_automatico6').' - '.$order->id);

            /*LOCAL*/
            $message->to('jose.venancio@thesilverfactory.pt')->bcc('hugo.freire@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('ecommerce::frontend.messages.msg_email_automatico62', ['number' => $order->id]));
            /*LOCAL*/


        });
    }


    public static function sendStateEmails($data, $order)
    {
        //encomenda para user
        $sucessos=Mail::send('ecommerce::backend.emails.send_state_order', $data, function($message) use($order)
        {
            $message->from('online.store@biblicallust.com', trans('ecommerce::frontend.messages.msg_email_automatico121', ['number' => $order->id]));

//            $message->to($order->order_addresses()->billing()->first()->email)->subject(trans('ecommerce::frontend.messages.msg_email_automatico6').' - '.$order->id);

            /*LOCAL*/
            $message->to('jose.venancio@thesilverfactory.pt')->bcc('hugo.freire@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('ecommerce::frontend.messages.msg_email_automatico121', ['number' => $order->id]));
            /*LOCAL*/


        });
    }

    public static function sendSubscriptionEmail($data, $email)
    {
        $sucessos=Mail::send('campaigns::frontend.emails.subscribe', $data, function($message) use($email)
        {
            $message->from('online.store@biblicallust.com', trans('cms::frontend.pages.subscribe.add_user'));

            $message->to($email)->subject(trans('cms::frontend.pages.login.new-client'));

//            /*LOCAL*/
//            $message->to('jose.venancio@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('cms::frontend.pages.login.new-client'));
//            /*LOCAL*/


        });
    }

    public static function sendUnsubscriptionEmail($data, $email)
    {
        $sucessos=Mail::send('campaigns::frontend.emails.unsubscribe', $data, function($message) use($email)
        {
            $message->from('online.store@biblicallust.com', trans('cms::frontend.pages.unsubscribe.remove_user'));

            $message->to($email)->subject(trans('cms::frontend.pages.login.new-client'));

//            /*LOCAL*/
//            $message->to('jose.venancio@thesilverfactory.pt')->bcc('wagner.fernandes@thesilverfactory.pt')->bcc('andreia.faisca@thesilverfactory.pt')->subject(trans('cms::frontend.pages.login.new-client'));
//            /*LOCAL*/


        });
    }
    
}