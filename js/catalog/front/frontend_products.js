$(document).ready(function(){
    var win = $(window);

    var infinite = new Waypoint.Infinite({
        element: $('#products-list'),
        loadingClass: 'loding_icon_products',
        onAfterPageLoad: function ($items) {
            $( ".infinite-item" ).fadeIn();
            $items.children('.products-source').css('opacity', 1);
        }
    });

    $('.list-brands a').on('click', function(){
        $href = $(this).attr('href');
        $menu = $(this).parent();
        $('.list-attributes .sub-menu').hide();
        $('.list-attributes li:first-child').removeClass('active');
        $('.list-attributes').children('i').removeClass('icon-menos');
        $('.list-attributes').children('i').addClass('icon-mais');
        $subMenu = $(this).parent().children('ul');

        if($subMenu.length && $href === 'javascript:;'){
            if($subMenu.is(':visible')){
                $menu.removeClass('active');
                $subMenu.hide();
                $menu.children('i').removeClass('icon-menos');
                $menu.children('i').addClass('icon-mais');
            } else {

                $('.list-brands .sub-menu').hide();
                $('.list-brands li:first-child').removeClass('active');
                $('.list-brands').children('i').removeClass('icon-menos');
                $('.list-brands').children('i').addClass('icon-mais');

                $menu.addClass('active');
                $subMenu.show();
                $menu.children('i').removeClass('icon-mais');
                $menu.children('i').addClass('icon-menos');
            }
        }
    });

    $('.list-brands .sub-menu a').on('click', function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }

        $subMenus = $('.list-brands .sub-menu li');

        $filters = $subMenus.children('.active');

        console.log($filters);

        var $list_brands = [];

        $.each($filters, function (index, value) {
            $list_brands.push($(value).data('register'));
        });

        if($list_brands.length){
            $.each($('.products-source'), function (index, value) {
                var list = $list_brands;
                var brands = $(value).data('brands');
                var product = brands.filter(function(index){
                    if(jQuery.inArray(index, list) != -1){
                        return true;
                    }
                });

                if( product.length ){
                    $('.no-products').hide();
                    $(value).parent().show();
                } else {
                    $(value).parent().hide();
                }
            });

            if($('.products-source').parents().is(':visible')){
                $('.no-products').hide();
            } else {
                $('.no-products').show();
            }

        } else {
            $('.no-products').hide();
            $('.products-source').parents().show();
        }


    });

    $(document).on('click', '.list-attributes a', function(){
        $href = $(this).attr('href');
        $menu = $(this).parent();
        $('.list-brands .sub-menu').hide();
        $('.list-brands li:first-child').removeClass('active');
        $('.list-brands').children('i').removeClass('icon-menos');
        $('.list-brands').children('i').addClass('icon-mais');
        $subMenu = $(this).parent().children('ul');

        if($subMenu.length && $href === 'javascript:;'){
            if($subMenu.is(':visible')){
                $menu.removeClass('active');
                $subMenu.hide();
                $menu.children('i').removeClass('icon-menos');
                $menu.children('i').addClass('icon-mais');
            } else {

                $('.list-attributes .sub-menu').hide();
                $('.list-attributes li:first-child').removeClass('active');
                $('.list-attributes').children('i').removeClass('icon-menos');
                $('.list-attributes').children('i').addClass('icon-mais');

                $menu.addClass('active');
                $subMenu.show();
                $menu.children('i').removeClass('icon-mais');
                $menu.children('i').addClass('icon-menos');
            }
        }
    });

    $('.list-attributes .sub-menu a').on('click', function () {
        if($(this).hasClass('active')){
            $(this).removeClass('active');
        } else {
            $(this).addClass('active');
        }

        $subMenus = $('.list-attributes .sub-menu li');

        $filters = $subMenus.children('.active');

        var $list_filters = [];

        $.each($filters, function (index, value) {
            $list_filters.push($(value).data('register'));
        });

        if($list_filters.length){
            $.each($('.products-source'), function (index, value) {
                var list = $list_filters;
                var attributes = $(value).data('attributes');
                var product = attributes.filter(function(index){
                    if(jQuery.inArray(index, list) != -1){
                        return true;
                    }
                });

                if( product.length ){
                    $('.no-products').hide();
                    $(value).parent().show();
                } else {
                    $(value).parent().hide();
                }
            });

            if($('.products-source').parents().is(':visible')){
                $('.no-products').hide();
            } else {
                $('.no-products').show();
            }

        } else {
            $('.no-products').hide();
            $('.products-source').parents().show();
        }

    });

    $('.products-source').css('opacity', 1);

    $(document).on('mouseenter', '.first-img .products-images', function () {
        $('.secound-img .products-images').css('opacity', 1);
    })

});
//# sourceMappingURL=frontend_products.js.map
