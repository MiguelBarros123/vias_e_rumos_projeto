@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>
    <script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
    <script src="{{asset('back/js/datatablesselect.js')}}"></script>
    <script src="{{asset('back/js/datatables.js')}}"></script>
    <script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
    <script>

            var options = {
                rota: "{{ route('permissions::backend::grupos_ajax')}}",
                colunas: [
                    { data: 'nome', name: 'nome' },
                    { data: 'created_at', name: 'created_at' },
                    { data: 'updated_at', name: 'updated_at' },
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                    { data: null, defaultContent: '', orderable: false,searchable: false }
                ],
                config: {
                    initComplete: function(settings, json) {
                        $(function() {
                            $.logicDatatable(options);
                        })
                    },
                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                        //for clicable rows
                        $(nRow).attr('data-rota-see', aData['rota_final']);
                        var tr2 = $(nRow).closest('tr');
                        tr2.find("td:first").addClass('details-control');
                        tr2.find("td:not(:last, :nth-last-child(2))").addClass('clickable_url');
                    }
                },
                searchableElementId: 'group_search'
            };

            $('.default-table').mydatatable(options);
            $('.default-table').on('click', 'td.clickable_url', function () {
                var tr = $(this).closest('tr');
                window.location.href = tr.attr('data-rota-see');

            } );


    </script>

@endsection

@section('content')

    <div class="conteudo_central margin-top-no-menu ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    @include('permissions::backend.model.layouts.top_menu')
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="group_search" class="search search_icon" type="text"  placeholder="@lang('permissions::backend.group.index.search')">
                        </div>
                    </div>
                    @include('layouts.backend.messages')

                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.groups') !!}

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                            <table id="groups_table" class="table default-table">
                                <thead>
                                    <tr>
                                        <th>{{ ucfirst(trans(('permissions::backend.name'))) }}</th>
                                        <th>@lang('permissions::backend.timestamps.updated-at')</th>
                                        <th>@lang('permissions::backend.group.index.updated-by')</th>
                                        <th class="multiple-options">
                                            @include('layouts.backend.partials.delete_datatables',['id'=>'groups_table','route'=>'permissions::backend::group.delete_multiple'])
                                        </th>
                                        <th>
                                            <label class="checkbox-default">
                                                <input type='checkbox' id="sel_all_perm_groups_table">
                                                <span></span>
                                            </label>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    @include('layouts.backend.partials.delete_form',['delete_message'=>'cms::backend.common.sure_delete'])

@endsection