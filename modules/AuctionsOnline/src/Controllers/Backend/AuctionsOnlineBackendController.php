<?php 

namespace Brainy\AuctionsOnline\Controllers\Backend;

use Brainy\Rent\Models\Lot;
use Brainy\Rent\Models\Auction;
use Brainy\Rent\Models\Concelho;
use Brainy\Rent\Models\Distrito;
use Brainy\Rent\Models\FreguesiaActual;
use Yajra\Datatables\Facades\Datatables;
use Brainy\Framework\Controllers\BackendController;

class AuctionsOnlineBackendController extends BackendController{
    /**
    * Display a listing of the resource.
    *
    * @return Response
    */
    public function index()
    {
        return view('auctionsonline::backend.auctions_online.index');
    }

    public function index_ajax()
    {
        $auctions_online = Auction::withoutGlobalScopes()->where('category_id', 4)->get();

        return Datatables::of($auctions_online)
        ->editColumn('title', function($auction){
            return $auction->title;
        })
        ->editColumn('ref_intern', function($auction){
            return $auction->ref_intern;
        })
        ->editColumn('date', function($auction){
            return $auction->date;
        })
        ->editColumn('local', function($auction){
            return $auction->local;
        })
        ->editColumn('featured', function($auction){
            return $auction->featured ? 'Sim' : 'Não';
        })
        ->editColumn('active', function($auction){
            return $auction->active ? 'Sim' : 'Não';
        })

        ->addColumn('action', function ($auction) {
            $rota=route('auctionsonline::backend::auctions_online.delete');
            $rotaeditar=route('auctionsonline::backend::auctions_online.edit',[$auction->id, $auction->category]);
            $impossible=0;

            $string = '
            <div class="btn-group pull-right">
            <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="icon-roda-dentada icon10"></i>
            </button>
            <ul class="dropdown-menu">
            <li><a  class="edit_item_datatable" href="'.$rotaeditar.'" ><i class="icon-editar"></i>  ' . trans("rent::backend.common.edit_and_see") . '</a></li>
            <li><a data-impossible-delete="'.$impossible.'" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $auction->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("rent::backend.common.delete") . ' </a></li>
            </ul>
            </div>
            ';
            return $string;
        })
        ->addColumn('rota_final', function($auction){
            return route('rent::backend::backend.rent.auctions.edit', [$auction->id, $auction->category_id]);
        })
        ->make(true);
    }

    public function index_ajax_lots($id)
    {
        $lots = Lot::where('auction_id',$id)->get();

        return Datatables::of($lots)
        ->editColumn('designacao', function($lot){
            return $lot->designacao;
        })
        ->editColumn('local', function($lot){
            return $lot->local;
        })
        ->editColumn('distrito', function($lot){
            return $lot->district;
        })
        ->editColumn('concelho', function($lot){
            return $lot->county;
        })
        ->editColumn('freguesia', function($lot){
            return $lot->parish;
        })
        ->editColumn('price', function($lot){
            return $lot->price;
        })

        ->addColumn('action', function ($lot) {
            $rotaeditar=route('auctionsonline::backend::auctions_online_lots.edit', $lot->id);
            $impossible=0;

            $string = '
            <div class="btn-group pull-right">
            <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="icon-roda-dentada icon10"></i>
            </button>
            <ul class="dropdown-menu">
            <li><a  class="edit_item_datatable" href="'.$rotaeditar.'" ><i class="icon-editar"></i>  ' . trans("rent::backend.common.edit_and_see") . '</a></li>
            </ul>
            </div>
            ';
            return $string;
        })
        ->addColumn('rota_final', function($lot){
            return route('auctionsonline::backend::auctions_online_lots.edit', $lot->id);
        })
        ->make(true);
    }

    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  
    * @return Response
    */
    public function edit($id)
    {
        $auction = Auction::withoutGlobalScopes()->find($id);

        $districts = Distrito::all();
        $concelhos = Concelho::all();
        $freguesias = FreguesiaActual::all();
        $locales = config('translatable.locales');

        return view('auctionsonline::backend.auctions_online.edit',compact('auction','districts','concelhos','freguesias','locales'));
    }

    public function delete()
    {
        $rows = explode(',', request('deleted_rows'));
        foreach($rows as $id){
            $auction = Auction::withoutGlobalScopes()->find($id);
            $auction->delete();
        }

        return redirect()->route('auctionsonline::backend::auctions_online.index');
    }

    public function edit_lots($id)
    {
        $lot = Lot::find($id);
        $auction = Auction::withoutGlobalScopes()->find($lot->auction_id);
        $district_lots = Distrito::all();
        $county_lots = Concelho::all();
        $parish_lots = FreguesiaActual::all();
        return view('auctionsonline::backend.auctions_online.lots.edit',compact('lot', 'auction','district_lots','county_lots','parish_lots'));
    }
}
