var schedule;

$("#slider-range").slider({
    range: false,
    min: 60000,
    max: 300000,
    step: 60000,
    value: save_value,
    slide: function slide(event, ui) {

        schedule = ui.value / 60000;

        var style = $('#slider-range span').attr('style');

        $("#skedule_hours").val(schedule * 60000);

        $('.range-label').html(schedule + "min");

        $('.range-label').attr('style', style);

    }
});

schedule = save_value / 60000;

$("#custom-handle").append('<div class="range-label"></div>');

$("#skedule_hours").val(schedule * 60000);

$('.range-label').html(schedule + "min");

//# sourceMappingURL=slider.js.map
