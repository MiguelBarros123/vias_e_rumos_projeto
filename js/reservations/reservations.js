$('.visible').bootstrapToggle();

if(start_range != '' && end_range != ''){
    var start_date = start_range;
    var end_date = end_range;
} else {
    var start_date = moment();
    var end_date = moment().add(5, 'days');
}

$('#date_range').daterangepicker({
    "locale": {
        "format": 'DD/MM/YYYY'
    },
    "minDate": start_date,
    "startDate": start_date,
    "endDate": end_date
});
//# sourceMappingURL=reservations.js.map
