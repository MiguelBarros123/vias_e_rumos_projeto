$(document).ready(function(){

    $(window).on('scroll', function(){
        var top = $(window).scrollTop();

        if(top > 0){
            $('.header-menu').addClass('sections-menu');
            $('.header-menu .top_menu .menu-logo-home').addClass('menu-hidden');
            $('.header-menu .top_menu .menu-logo').removeClass('menu-hidden');
        } else {
            $('.header-menu').removeClass('sections-menu');
            $('.header-menu .top_menu .menu-logo-home').removeClass('menu-hidden');
            $('.header-menu .top_menu .menu-logo').addClass('menu-hidden');
        }
    });

    $('#reservations_modal').on('click', function(){
        $('.modal-reservations').fadeIn();
    });

    $('#close-modal-reservations').on('click', function(){
        $('.modal-reservations').fadeOut();
    });

    var image = {
        url: map_pin,
        origin: new google.maps.Point(0,0),
        anchor: new google.maps.Point(0,0)
    };

    function initialize() {

        var mapStyle = [
            {
                featureType: "administrative",
                elementType: "labels",
                stylers: [
                    { visibility: "off" }
                ]
            },{
                featureType: "poi",
                elementType: "labels",
                stylers: [
                    { visibility: "off" }
                ]
            },{
                featureType: "water",
                elementType: "labels",
                stylers: [
                    { visibility: "off" }
                ]
            }
        ];


        var mapOptions = {
            zoom: 17,
            scrollwheel: false,
            center: new google.maps.LatLng("39.63117","-8.6779559")
        };
        var map = new google.maps.Map(document.getElementById('contacts-map'),
            mapOptions);
        map.set('styles', mapStyle);
        setMarkers(map);
    }

    function setMarkers(map) {
        var myLatLng = new google.maps.LatLng("39.63117","-8.6779559");
        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: 'Essêncial Marianos',
            icon: image,
            optimized: false,
            flat: true,
            visible: true
        });

        marker.addListener('click', toggleBounce);

        function toggleBounce() {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);

                setTimeout(function(){
                    marker.setAnimation(false);
                }, 2000);

            }
        }

    }

    google.maps.event.addDomListener(window, 'load', initialize);

});