var myTemplate_caracteristics_table = "<tr id='sel_item_list2_caracteristics_table_${id}'>" +
    "<td>${icon}</td>" +
    "<td> ${name} <input type='hidden' name='caracteristics[]' value='${id}' /></td> " +
    "<td> ${is_main_caracteristic} </td> " +
    "<td class='text-right'><a id='delete_selected_item_datatable_list_caracteristics_table' data-id='${id}'><i class='icon-opcoes-eliminar'></i></a></td>" +
    "</tr>";

$(document).ready(function () {
    $('.lang_tabs li:first').tab('show');
    $('.lang_content div:first').addClass('active');

    if (typeof angular != 'undefined') {
        angular.bootstrap('body', ['BootstrapToggler']);
    }

    $('.visible').bootstrapToggle();
    $('#adapted_unit').bootstrapToggle();

    //---------------------------------------------------------------start caracteristics
    var optionsPacks = {
        rota: selecting_caracteristics,
        colunas: [
            {data: 'icon', name: 'icon'},
            {data: 'name', name: 'name'},
            {data: 'is_main_caracteristic', name: 'is_main_caracteristic'},
            {data: null, defaultContent: '', orderable: false, searchable: false}
        ],
        config: {
            fnCreatedRow: function (nRow, aData, iDataIndex) {
                //for already selected rows
                $(nRow).attr('id', 'sel_row_'+aData['id']);
                if(aData['already_selected']==1){
                    optionsPacks.datatable.row(nRow).select();
                }

            },

            initComplete: function(settings, json) {
                $(function() {
                    $.logicDatatable(optionsPacks);
                    $.selectDatatableLogic(optionsPacks);
                    $('.caracteristics_check').bootstrapToggle();


                })
            }



        },
        searchableElementId: 'caracteristics_search'
    };

    $('#caracteristics_table').mydatatable(optionsPacks);

    $('#select_resource_caracteristics_table').on('shown.bs.modal', function (e) {

        optionsPacks.datatable.columns.adjust().draw();

    });

    $('#select_resource_caracteristics_table').on('hide.bs.modal', function (e) {

        $('.caracteristics_check').bootstrapToggle();

    });

    //---------------------------------------------------------------end caracteristics

    //-----------------------------------------------------------------prices
    $('.no_iva_input').on('change keyup',function () {
        var sanitized = $(this).val().replace(/[^0-9\.\/]/g, '');
        $(this).val(sanitized);

        var coin_id = $(this).attr('data-coin-id');

        if($(this).val()!='' ){
            var iva=$('#selected_tax').val();
            var inicial=parseFloat($(this).val());
            var tot_iva=(inicial*iva/100);
            $(this).parent().parent().next().find('.w_iva_input').val((inicial+tot_iva).toFixed(2));

            var inicial_eur = inicial / $(this).attr('data-coin-rate');

            $.each($(".variant-price-table tr"), function () {
               if($(this).attr('data-coin') != coin_id){
                    var total_coin = inicial_eur * $(this).attr('data-coin-rate');
                    var total_coin_iva = ((inicial_eur * $(this).attr('data-coin-rate'))*iva/100);

                   $(this).find('.no_iva_input').val((total_coin).toFixed(2));
                   $(this).find('.w_iva_input').val((total_coin + total_coin_iva).toFixed(2));

               }
            });

        }else{
            $(this).parent().parent().next().find('.w_iva_input').val(null)
        }
    });

    $('.w_iva_input').on('change keyup',function() {
        var sanitized = $(this).val().replace(/[^0-9\.\/]/g, '');
        $(this).val(sanitized);

        var coin_id = $(this).attr('data-coin-id');

        if($(this).val()!='') {
            var iva = $('#selected_tax').val();
            var inicial = parseFloat($(this).val());
            var tot_iva = parseFloat(1 + (iva / 100));
            console.log(tot_iva);
            $(this).parent().parent().prev().find('.no_iva_input').val((inicial / tot_iva).toFixed(2));

            var inicial_eur = inicial / $(this).attr('data-coin-rate');

            $.each($(".variant-price-table tr"), function () {
                if($(this).attr('data-coin') != coin_id){
                    var total_coin_iva = inicial_eur * $(this).attr('data-coin-rate');
                    // var total_coin_iva = ((inicial_eur * $(this).attr('data-coin-rate'))*iva/100);

                    $(this).find('.w_iva_input').val((total_coin_iva).toFixed(2));
                    $(this).find('.no_iva_input').val((total_coin_iva / tot_iva).toFixed(2));

                }
            });


        }else{
            $(this).parent().parent().prev().find('.no_iva_input').val(null);
        }
    });
    //-----------------------------------------------------------------end prices

});
//# sourceMappingURL=rent_items.js.map
