(function() {
	'use strict';

    function FaqsBackendController($scope, $http) {

        $scope.breadcrumb = [
            'Brainy',
            'FAQs',
            'Backend'
        ];

        $scope.search = '';
        $scope.urlFaqsThemes = '';
        $scope.shouldMakeHttpRequest = true;
        $scope.selectedNode = {};

        $scope.isEmptySelection = function () {
            return $.isEmptyObject($scope.selectedNode);
        };

        $scope.clearSelection = function () {
            $scope.selectedNode = {};
        };
        
        $scope.escapeHtml = function (str) {
            return $(str).text();
        };

        function httpSuccess(response) {
            $scope.locales = response.locales;
            $scope.locale = response.locale || 'en'; // case undefined -> fallback en

            createFAQNodes(response.faqs);
            createThemeNodes(response.themes);
        }

        function httpError(response) {

        }

        $scope.fetch = function(urlFaqsThemes) {
            $scope.urlFaqsThemes = urlFaqsThemes;

            $http({ method: 'GET', url: $scope.urlFaqsThemes }).success(httpSuccess).error(httpError);
        };

        $scope.find = function() {
            $scope.faqsTree.instance.jstree().search($scope.search);
        };

        $scope.setLocale = function(locale) {
            $scope.locale = locale;
            renderFaqsTree();
        };

        function getTextPropertyByNodeType(type) {
            return type === 'theme' ? 'name' : type;
        }

        function getAllNodes(flat) {
            return angular.fromJson($scope.faqsTree.instance.jstree().get_json('#', { flat: flat }));
        }

        function renderFaqsTree() {
            var nodes = getAllNodes(true);

            $scope.shouldMakeHttpRequest = false;

            for (var i = 0; i < nodes.length; ++i) {
                var node = nodes[i];
                var data = $scope.faqsTree.mapper.node2data[node.id];
                var newText = getTextByLocale(data, getTextPropertyByNodeType(node.type));
                $scope.faqsTree.instance.jstree().rename_node(node, newText);
            }

            $scope.shouldMakeHttpRequest = true;
            //$scope.faqsTree.instance.jstree().redraw(true);
        }

        function getSelectedNodes() {
            return $scope.faqsTree.instance.jstree().get_selected(true);
        }

        function hasLocale(locale) {
            return function(element) {
                return element.locale === locale;
            };
        }

        function getTextByLocale(object, property) {
            if (!object.translations || !object.translations.length) {
                return object[property];
            }

            var result = object.translations.findIndex(hasLocale($scope.locale));

            if (result === -1) {
                return object[property];
            }

            return object.translations[result][property];
        }

        function setPropertyByLocale(object, property, node) {
            if ($scope.locale === undefined || (!object.translations || !object.translations.length)) {
                object[property] = node.text;
            }

            var result = object.translations.findIndex(hasLocale($scope.locale));

            if (result == -1) {
                object[property] = node.text;
            }

            object.translations[result][property] = node.text;
        }

        function handlerRenameNode(event, obj) {
            var data = $scope.faqsTree.mapper.node2data[obj.node.id];
            var property = getTextPropertyByNodeType(obj.node.type);

            if (data !== undefined) {
                setPropertyByLocale(data, property, obj.node);
            }

            if ($scope.shouldMakeHttpRequest) {
                var request = {};
                var i, locale;
                if (property == 'name') {
                    for (i = 0; i < $scope.locales.length; ++i) {
                        locale = $scope.locales[i];
                        request['name_' + locale] = data.translations[data.translations.findIndex(hasLocale(locale))].name;
                    }

                    $http.put('http://localhost/brainy-web/public/backend/faqs/theme/' + data.id + '/name', request);
                } else {
                    for (i = 0; i < $scope.locales.length; ++i) {
                        locale = $scope.locales[i];
                        request['question_' + locale] = data.translations[data.translations.findIndex(hasLocale(locale))].question;
                        request['answer_' + locale] = data.translations[data.translations.findIndex(hasLocale(locale))].answer;
                    }

                    $http.put('http://localhost/brainy-web/public/backend/faqs/faq/' + data.id + '/qa', request);
                }
            }
        }

        function httpSuccessDelete(response) {
            var dirty = $scope.faqsTree.mapper.data2nodes[response];
            
            for (var i = 0; i < dirty.length; ++i) {
                delete $scope.faqsTree.mapper.node2data[dirty[i].id];
            }

            delete $scope.faqsTree.mapper.data2nodes[response];
        }

        function httpErrorDelete(response) {
            console.log('error :: node not deleted', response);
            // insert again in jsTree
        }

        function handlerDeleteNode(event, obj) {

            var data = $scope.faqsTree.mapper.node2data[obj.node.id];
            var model = obj.node.type === 'theme' ? 'theme' : 'faq';

            $http.delete('http://localhost/brainy-web/public/backend/faqs/' + model + '/' + data.id)
                .success(httpSuccessDelete)
                .error(httpErrorDelete);
        }

        function handlerChangedSelection(event, obj) {

        }

        function handlerRedraw(event, nodes) {}

        function httpSuccessChangedPosition(response) {
            var nodes = $scope.faqsTree.mapper.data2nodes[response.id];
            if (nodes !== undefined && nodes.length) {
                $scope.faqsTree.mapper.node2data[nodes[0].id] = response;
            }
        }

        function httpErrorChangedPosition(response) {
            console.log('error at changing the position');
        }

        function handlerMoveNode(event, obj) {
            var data = $scope.faqsTree.mapper.node2data[obj.node.id];

            if (data !== undefined) {
                var model = obj.node.type === 'theme' ? 'theme' : 'faq';

                var request = {
                    position: obj.position
                };

                if (obj.parent !== '#') {
                    request.theme_id = $scope.faqsTree.mapper.node2data[obj.parent].id;
                }

                if ($scope.shouldMakeHttpRequest) {
                    $http.put('http://localhost/brainy-web/public/backend/faqs/' + model + '/' + data.id + '/position', request)
                         .success(httpSuccessChangedPosition)
                         .error(httpErrorChangedPosition);
                }
            }
        }

        function faqsTreeError(error) {
            console.log('[ERROR] :: REASON => ' + error.reason);
        }

        function isNotNodeDraggable(node) {
            return node.type === 'answer';
        }

        function isDraggale(nodes, event) {
            return !nodes.find(isNotNodeDraggable);
        }

        function httpPostSuccess(callbackCreate) {
            return function(response) {
                callbackCreate([response]);
            };
        }

        function contextMenuItems (node) {

            function createThemeRoot() {

            }

            function createFAQRoot() {

            }

            function createThemeNode() {
                var theme = {
                    id: 'new-theme-' + Date.now(),
                    name: {
                        translations: {}
                    },
                    theme_id: $scope.faqsTree.mapper.node2data[node.id].id,
                    visible: true,
                    link: null,
                    tags: [],
                };

                for (var i = 0; i < $scope.locales.length; ++i) {
                    theme.name.translations[$scope.locales[i]] = '[substitua este texto pelo título do tema]';
                }

                $http.post('http://localhost/brainy-web/public/backend/faqs/theme', theme)
                    .success(httpPostSuccess(createThemeNodes));
            }

            function createFAQNode() {
                 var faq = {
                    id: 'new-faq-' + Date.now(),
                    question: {
                        translations: {}
                    },
                    answer: {
                        translations: {}
                    },
                    theme_id: $scope.faqsTree.mapper.node2data[node.id].id,
                    visible: true,
                    link: null,
                    tags: []
                };

                for (var i = 0; i < $scope.locales.length; ++i) {
                    faq.question.translations[$scope.locales[i]] = '[substitua este texto pela questão]';
                    faq.answer.translations[$scope.locales[i]] = '[substitua este texto pela resposta]';
                }

                $http.post('http://localhost/brainy-web/public/backend/faqs/faq', faq)
                    .success(httpPostSuccess(createFAQNodes));
            }

            function openModal(modalType) {
                var data = $scope.faqsTree.mapper.node2data[node.id];

                if (data !== undefined) {
                    $scope.selectedNode = data;
                }

                $(modalType).modal('show');
            }

            function editFAQNode() {
                openModal('#modal-faq');
            }

            function editThemeNode() {
                $scope.themeType = node.parent === '#' ? 'theme' : 'subtheme';
                openModal('#modal-theme');
            }

            function renameNode() {
                $scope.faqsTree.instance.jstree().edit(node);
            }

            function deleteThemeNode() {
                $scope.faqsTree.instance.jstree().delete_node(node);
            }

            function deleteFAQNode() {
                var data = $scope.faqsTree.mapper.node2data[node.id];
                var nodes = $scope.faqsTree.mapper.data2nodes[data.id];

                $scope.faqsTree.instance.jstree().delete_node(nodes);
            }

            var actions = {
                createFAQRoot: {
                    label: 'Nova FAQ',
                    icon: '',
                    action: createFAQRoot
                },
                createFAQNode: {
                    label: 'FAQ',
                    icon: '',
                    action: createFAQNode
                },
                createThemeRoot: {
                    label: 'Novo Tema',
                    icon: '',
                    action: createThemeRoot
                },
                createThemeNode: {
                    label: 'Subtema',
                    icon: '',
                    action: createThemeNode
                },
                renameNode: {
                    label: 'Mudar o nome',
                    icon: '',
                    action: renameNode
                },
                editThemeNode: {
                    label: 'Editar',
                    icon: '',
                    action: editThemeNode,
                    separator_before: true,
                    separator_after: true
                },
                editFAQNode: {
                    label: 'Editar',
                    icon: '',
                    action: editFAQNode,
                    separator_before: true,
                    separator_after: true
                },
                deleteThemeNode: {
                    label: 'Eliminar',
                    icon: '',
                    action: deleteThemeNode
                },
                deleteFAQNode: {
                    label: 'Eliminar',
                    icon: '',
                    action: deleteFAQNode
                }
            };

            switch (node.type) {
                case 'theme':
                    return {
                        create: {
                            label: 'Criar',
                            submenu: {
                                theme: actions.createThemeNode,
                                faq: actions.createFAQNode
                            }//,
                            //separator_after: true
                        },
                        edit: actions.editThemeNode,
                        rename: actions.renameNode,
                        delete: actions.deleteThemeNode
                    };
                case 'question':
                case 'answer':
                    return {
                        edit: actions.editFAQNode,
                        rename: actions.renameNode,
                        delete: actions.deleteFAQNode
                    };
                default:
                    return {
                        createThemeRoot: actions.createThemeRoot,
                        createFAQRoot: actions.createFAQRoot
                    };
            }
        }

        $scope.faqsTree = {
            nodeModel: {
                id: '',
                text: '',
                parent: '#',
                type: 'default',
                state: {
                    'opened': true
                }
            },
            config: {
                core: {
                    check_callback: true,
                    multiple: false,
                    animation: 170,
                    error: faqsTreeError,
                    strings: {
                        'Loading ...': 'A carregar ...'
                    }
                },
                search: {
                    fuzzy: true,
                    show_only_matches: true
                },
                dnd: {
                    is_draggable: isDraggale
                },
                contextmenu: {
                    show_at_node: false,
                    items: contextMenuItems
                },
                types: {
                    '#': {
                        valid_children: [
                            'question', 'theme'
                        ]
                    },
                    /* default is question (question) */
                    theme: {
                        icon: 'icon-group icon20',
                        valid_children: [
                            'theme', 'question'
                        ]
                    },
                    question: {
                        icon: 'icon-vistas-galeria-list icon20',
                        valid_children: [
                            'answer'
                        ]
                    },
                    answer: {
                        icon: 'icon-conversas icon20',
                        valid_children: []
                    }
                },
                plugins: [
                    'types',
                    'search',
                    'changed',
                    'contextmenu',
                    'dnd',
                    'wholerow'
                ],
                version: 1
            },
            instance: {},
            events: {
                'rename_node': handlerRenameNode,
                'move_node': handlerMoveNode,
                'delete_node': handlerDeleteNode,
                'changed': handlerChangedSelection,
                'redraw': handlerRedraw
            },
            mapper: {
                node2data: [],
                data2nodes: []
            },
            incrementer: 0
        };

        function getNodeParentId(data) {
            return data.theme_id && $scope.faqsTree.mapper.data2nodes[data.theme_id] ? $scope.faqsTree.mapper.data2nodes[data.theme_id][0] : '#';
        }

        function createFAQNodes(faqs) {
            for (var i = 0; i < faqs.length; ++i) {
                var faq = faqs[i];

                var nodeQuestion = $.extend({}, $scope.faqsTree.nodeModel),
                    nodeAnswer = $.extend({}, $scope.faqsTree.nodeModel);

                nodeQuestion.id = ++$scope.faqsTree.incrementer;
                nodeQuestion.parent = getNodeParentId(faq);
                nodeQuestion.text = getTextByLocale(faq, 'question');
                nodeQuestion.type = 'question';

                nodeAnswer.id = ++$scope.faqsTree.incrementer;
                nodeAnswer.parent = nodeQuestion.id;
                nodeAnswer.text = getTextByLocale(faq, 'answer');
                nodeAnswer.type = 'answer';

                $scope.faqsTree.mapper.node2data[nodeQuestion.id] = faq;
                $scope.faqsTree.mapper.node2data[nodeAnswer.id] = faq;
                $scope.faqsTree.mapper.data2nodes[faq.id] = [nodeQuestion, nodeAnswer];

                $scope.faqsTree.instance.jstree().create_node(nodeQuestion.parent, nodeQuestion, faq.position);
                $scope.faqsTree.instance.jstree().create_node(nodeQuestion.id, nodeAnswer, faq.position);
            }
        }

        function createThemeNodes(themes) {
            for (var i = 0; i < themes.length; ++i) {
                var theme = themes[i];

                var nodeTheme = $.extend({}, $scope.faqsTree.nodeModel);

                nodeTheme.id = ++$scope.faqsTree.incrementer;
                var themeText = getTextByLocale(theme, 'name');
                nodeTheme.text = themeText;
                nodeTheme.type = 'theme';

                $scope.faqsTree.mapper.node2data[nodeTheme.id] = theme;
                $scope.faqsTree.mapper.data2nodes[theme.id] = [nodeTheme];

                $scope.faqsTree.instance.jstree().create_node(getNodeParentId(theme), nodeTheme, theme.position);

                if (theme.faqs && theme.faqs.length) {
                    createFAQNodes(theme.faqs);
                }

                if (theme.children && theme.children.length) {
                    createThemeNodes(theme.children);
                }
            }
        }

        /* -------------------------------- CHILDREN EVENTS  -------------------------------- */
        function handlerNewFaq(event, faq) {
            createFAQNodes([faq]);
        }

        function handlerNewTheme(event, theme) {
            createThemeNodes([theme]);
        }

        function handlerEditFaq(event, faq) {
            var [question, answer] = $scope.faqsTree.mapper.data2nodes[faq.id];

            var dirty = $scope.faqsTree.mapper.node2data[question.id];

            $scope.faqsTree.mapper.node2data[question.id] = faq;
            $scope.faqsTree.mapper.node2data[answer.id] = faq;

            $scope.shouldMakeHttpRequest = false;

            $scope.faqsTree.instance.jstree().rename_node(question, getTextByLocale(faq, 'question'));
            $scope.faqsTree.instance.jstree().rename_node(answer, getTextByLocale(faq, 'answer'));
            
            if (dirty.theme_id !== faq.theme_id) {
                $scope.faqsTree.instance.jstree().move_node(question.id, getNodeParentId(faq));
            }

            $scope.shouldMakeHttpRequest = true;
        }

        function handlerEditTheme(event, theme) {
            var node = $scope.faqsTree.mapper.data2nodes[theme.id][0];

            var dirty = $scope.faqsTree.mapper.node2data[node.id];

            $scope.faqsTree.mapper.node2data[node.id] = theme;

            $scope.shouldMakeHttpRequest = false;
            $scope.faqsTree.instance.jstree().rename_node(node, getTextByLocale(theme, 'name'));

            if (dirty.theme_id !== theme.theme_id) {
                $scope.faqsTree.instance.jstree().move_node(node.id, getNodeParentId(theme));
            }

            $scope.shouldMakeHttpRequest = true;
        }

        $scope.$on('new faq', handlerNewFaq);
        $scope.$on('new theme', handlerNewTheme);

        $scope.$on('edit faq', handlerEditFaq);
        $scope.$on('edit theme', handlerEditTheme);
        /* ------------------------------ END CHILDREN EVENTS ------------------------------ */



        /* -------------------------------- TAGS  -------------------------------- */
        var tags = [];

        function httpTagsSuccess(response) {
            tags = response;
        }

        function httpTagsError(response) {
            tags = [];
        }

        $http({ method: 'GET', url: 'http://localhost/brainy-web/public/framework/tags' })
            .success(httpTagsSuccess)
            .error(httpTagsError);

        $scope.getTags = function () {
            return tags;
        };
        /* ------------------------------ END TAGS ------------------------------ */

        $scope.dismiss = function (id) {
            $(id).modal('hide');
        };
    }

    function allowedParents() {
        function hasValueInObj(key, value) {
            return function (obj) {
                return obj[key] === value;
            };
        }

        function hasParent(element, idKey, parentKey, arrayData, id) {
            if (! element[parentKey]) {
                return false;
            }

            if (element[parentKey] === id) {
                return true;
            }

            var next = arrayData.find(hasValueInObj(idKey, element[parentKey]));

            return hasParent(next, idKey, parentKey, arrayData, id);
        }

        return function (input, id, idKey, parentKey) {
            var output = [];

            angular.forEach(input, function(element) {
                if (element[idKey] !== id && ! hasParent(element, idKey, parentKey, input, id)) {
                    output.push(element);
                }
            });
            
            return output;
        };
    }

    angular.module('brainy.faqs.backend')
        .controller('FaqsBackendController', ['$scope', '$http', FaqsBackendController])
        .filter('allowedParents', allowedParents);
})();
