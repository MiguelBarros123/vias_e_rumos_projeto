<!DOCTYPE html>
<html>
<head>
    <title>Ajax Polling</title>
    <meta charset="utf-8">
</head>
<body>
    <p>Licitacoes: </p>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <script>
        
        $(document).ready(function(){
                pollInterval = setInterval(function(){
                    pool();
                }, 2000);
            });

            (function() {
            var licitacao = $('[name="licitacao"]');
            var valor_licitacao = $('#valor_licitacao');
            var url_lote = "{{route('cms::frontend::model.lote_teste',[':id'])}}";
            var route = url_lote.replace(':id','1329');
            pool = function(){
                $.ajax({
                    url: route,
                    data: {
                        bidding:licitacao.val()
                    },
                    type: 'get',
                    success: function(data){
                        valor_licitacao.val('22');
                        pool();
                    }
                });
            }
        })();
    </script>
    <body>
        <input type="text" name="licitacao" data-lote-id="1" value="">
        <button class="click_here" type="button">Click here</button>
        <input type="text" name="valor_licitacao" id="valor_licitacao">
    </body>
</body>
</html>
