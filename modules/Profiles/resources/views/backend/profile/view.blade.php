@extends('layouts.backend.app')

@section('title', trans('profiles::backend.model.index.title'))

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/profiles/backend.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/gallery/backend.css') }}">
@endsection

@section('module-scripts')



@endsection

@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <a class="pull-right icon_eliminar icon-editar pading_right icon_delete_inside_view_show"
                       href="{{ route('profiles::backend::profiles.edit', [$profile->id, 'person']) }}">
                        Editar
                    </a>
                    <ul class="list-inline">
                        <li><a href="{{route('profiles::backend::profiles.index')}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
            @include('layouts.backend.messages')
            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.profiles.show',$profile) !!}

            <div class="row">
                <form class="" method="POST" action="" enctype="multipart/form-data">
                    <input name="_method" type="hidden" value="PUT">
                    {!! csrf_field() !!}
                    <div class="col-lg-12">
                        <div class="panel ">
                            <div class="panel-titulo">
                                <span class="titulo"><i class="icon-ficha icon10"></i>@lang('profiles::backend.common.contact_sheet')</span>
                            </div>
                            <div class="panel-body">
                                <div class="row ">
                                    <div id="user-image" class="col-lg-2 text-center">
                                        @if($profile->thumbnail!= null)
                                        <img id="preview_file" class="pic-circle-corner2" src="{{route('gallery::frontend::storage_image_p',[$profile->thumbnail,90,90])}}"  >
                                            @else
                                            <div class="pic-circle-corner text-center"><i class="icon-avatar-person icon80 "></i></div>
                                        @endif
                                    </div>
                                    <input id="fileupload" type="file" name="profile_image">
                                    <div class="col-lg-4 profile_divider" >
                                        <div class="row">
                                            <div class="col-lg-12 profile_name">

                                                <div class="profile_name_18">{{$profile->name}} {{$profile->surname}}</div>
                                                @if($company)
                                                    <div><b>{{$profile->job}}</b> <span class="profile_input">na empresa</span> <b>{{ $company->name }}</b></div>
                                                @endif

                                            </div>
                                            <div class="col-lg-12">
                                                <div class="form-group fom-style-hidden3 @if($errors->first('surname') ) has-error @endif">
                                                    <label for="surname"><i class="icon-mail"></i> @lang('profiles::backend.common.emails')</label>


                                                    @if($profile->type != 'no_login_user' && $profile->type != 'company')

                                                        <div class="profile_input"><i class="icon-avatar-person"></i> {{$profile->user->email}}</div>

                                                    @endif

                                                    @foreach($profile->profile_contacts()->where('type','personal_email')->get() as $email)
                                                        <div class="profile_input"><i class="icon-avatar-person"></i> {{$email->contact}}</div>
                                                    @endforeach

                                                    @foreach($profile->profile_contacts()->where('type','company_email')->get() as $email)
                                                        <div class="profile_input"><i class="icon-trabalho"></i> {{$email->contact}}</div>
                                                    @endforeach

                                                </div>
                                            </div>

                                            <div class="col-lg-12">
                                                <div class="form-group fom-style-hidden3 @if($errors->first('surname') ) has-error @endif">
                                                    <label for="surname"><i class="icon-telefone"></i>@lang('profiles::backend.common.phones')</label>

                                                    @foreach($profile->profile_contacts()->where('type','personal_phone')->get() as $email)
                                                        <div class="profile_input"><i class="icon-avatar-person"></i> {{$email->contact}}</div>
                                                    @endforeach

                                                    @foreach($profile->profile_contacts()->where('type','company_phone')->get() as $email)
                                                        <div class="profile_input"><i class="icon-trabalho"></i> {{$email->contact}}</div>
                                                    @endforeach

                                                    @foreach($profile->profile_contacts()->where('type','personal_mobile')->get() as $email)
                                                        <div class="profile_input"><i class="icon-avatar-person"></i> {{$email->contact}}</div>
                                                    @endforeach

                                                    @foreach($profile->profile_contacts()->where('type','company_mobile')->get() as $email)
                                                        <div class="profile_input"><i class="icon-trabalho"></i> {{$email->contact}}</div>
                                                    @endforeach

                                                    @foreach($profile->profile_contacts()->where('type','personal_fax')->get() as $email)
                                                        <div class="profile_input"><i class="icon-avatar-person"></i> {{$email->contact}}</div>
                                                    @endforeach

                                                    @foreach($profile->profile_contacts()->where('type','company_fax')->get() as $email)
                                                        <div class="profile_input"><i class="icon-trabalho"></i> {{$email->contact}}</div>
                                                    @endforeach

                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-lg-offset-1">

                                        <div class="row padding-top-20">

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label class="profile_input"><i class="icon-genero"></i> @lang('profiles::backend.common.gender')</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <div>
                                                        @if($profile->gender == 'male')
                                                            Mascúlino
                                                        @elseif($profile->gender == 'female')
                                                            Feminino
                                                        @endif
                                                    </div>
                                                </div>

                                        </div>

                                        <div class="row">

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label class="profile_input"><i class="icon-nascimento"></i> @lang('profiles::backend.common.birth_date')</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <div>{{ date('d-n-Y', time($profile->birth_at) ) }}</div>
                                                </div>

                                        </div>

                                        @if($profile->profile_addresses()->count())
                                            <div class="row padding-top-20">

                                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                    <label class="profile_input"><i class="icon-morada"></i> @lang('profiles::backend.common.address')</label>
                                                </div>
                                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                    <div>{{ $profile->profile_addresses()->first()->address }}, {{ $profile->profile_addresses()->first()->town }}</div>
                                                </div>

                                            </div>
                                        @endif

                                        <div class="row">

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <label class="profile_input"><i class="icon-idioma"></i> @lang('profiles::backend.common.language')</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <div>{{ $language }}</div>
                                            </div>

                                        </div>

                                        <div class="row">

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <label class="profile_input"><i class="icon-novo-utilizador-1"></i> @lang('profiles::backend.common.created_on')</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                                                <div>{{ date('d-m-Y', strtotime($profile->created_at)) }}</div>
                                            </div>

                                        </div>

                                        <div class="row padding-top-20">

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <label class="profile_input"><i class="icon-redes-sociais"></i> @lang('profiles::backend.common.social_networks')</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                                                    @if($facebook)
                                                        <a href="{{ $facebook->contact }}" target="_blank">
                                                            <i class="icon-face social-profile social-facebook"></i>
                                                        </a>
                                                    @endif

                                                    @if($instagram)
                                                        <a href="{{ $instagram->contact }}" target="_blank">
                                                            <i class="icon-instagram-1 social-profile social-instagram"></i>
                                                        </a>
                                                    @endif

                                                    @if($google)
                                                        <a href="{{ $google->contact }}" target="_blank">
                                                            <i class="icon-google-plus social-profile social-google"></i>
                                                        </a>
                                                    @endif

                                                    @if($youtube)
                                                        <a href="{{ $youtube->contact }}" target="_blank">
                                                            <i class="icon-youtube-1 social-profile social-youtube"></i>
                                                        </a>
                                                    @endif

                                            </div>

                                        </div>

                                        @if(count($others))

                                        <div class="row padding-top-20">

                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                                <label class="profile_input"><i class="icon-redes-sociais"></i> @lang('profiles::backend.common.other')</label>
                                            </div>
                                            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

                                                @foreach($others as $other)
                                                    <a href="{{ $other->contact }}" target="_blank">
                                                        {{ $other->contact }}
                                                    </a>
                                                @endforeach

                                            </div>

                                        </div>

                                        @endif

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>

            {{--<div class="row">--}}
                    {{--<div class="col-lg-6">--}}
                        {{--<div class="panel ">--}}
                            {{--<div class="panel-titulo">--}}
                                {{--<span class="titulo"><i class="icon-links icon10"></i>@lang('profiles::backend.common.connections')</span>--}}
                            {{--</div>--}}
                            {{--<div class="panel-body">--}}
                                {{--<div class="row ">--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-lg-6">--}}
                        {{--<div class="panel ">--}}
                            {{--<div class="panel-titulo">--}}
                                {{--<span class="titulo"><i class="icon-timeline icon10"></i>@lang('profiles::backend.common.timeline')</span>--}}
                            {{--</div>--}}
                            {{--<div class="panel-body">--}}
                                {{--<div class="row ">--}}

                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</form>--}}
            {{--</div>--}}



        </div>
    </div>

@endsection



