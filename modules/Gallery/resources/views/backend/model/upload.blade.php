@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/gallery/backend.css')}}">
@endsection

@section('module-scripts')
    <script src="{{asset('js/gallery/backend.js')}}"></script>

    @if(\Illuminate\Support\Facades\Session::has('errors'))
        <script>
            $('#add_embed').modal();
        </script>
    @endif


    <script src="{{asset('back/js/ckeditor/ckeditor.js')}}"></script>

            <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
        <script src="{{asset('js/gallery/fileupload/jquery.ui.widget.js')}}"></script>

        <!-- The Load Image plugin is included for the preview images and image resizing functionality -->
        <script src="//blueimp.github.io/JavaScript-Load-Image/js/load-image.all.min.js"></script>

        <!-- The Canvas to Blob plugin is included for image resizing functionality -->
        <script src="//blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>

        <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
        <script src="{{asset('js/gallery/fileupload/jquery.iframe-transport.js')}}"></script>

        <!-- The basic File Upload plugin -->
        <script src="{{asset('js/gallery/fileupload/jquery.fileupload.js')}}"></script>


        <!-- The File Upload processing plugin -->
        <script src="{{asset('js/gallery/fileupload/jquery.fileupload-process.js')}}"></script>
        <!-- The File Upload image preview & resize plugin -->
        <script src="{{asset('js/gallery/fileupload/jquery.fileupload-image.js')}}"></script>
        <!-- The File Upload audio preview plugin -->
        <script src="{{asset('js/gallery/fileupload/jquery.fileupload-audio.js')}}"></script>
        <!-- The File Upload video preview plugin -->
        <script src="{{asset('js/gallery/fileupload/jquery.fileupload-video.js')}}"></script>
        <!-- The File Upload validation plugin -->
        <script src="{{asset('js/gallery/fileupload/jquery.fileupload-validate.js')}}"></script>

        @foreach($locales as $loc)
            <script>
                CKEDITOR.replace('editor_embed_d_{{ $loc }}');
            </script>
        @endforeach

        <script>
            $(function () {
                'use strict';

                var $uploadForm = $('div#files div.cont_inicial');

                $("[data-toggle=popover]").popover({
                    html : true,
                    template:'<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content gallery_popover"></div></div>'
                });


                var url = "{{route('gallery::backend::save_asset')}}";

                var filelist = [];

                $('#fileupload').fileupload({
                    url: url,
                    autoUpload: false,
                    maxChunkSize: 524288,
//                    multipart:true,
                    singleFileUploads: false,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    disableImageResize: /Android(?!.*Chrome)|Opera/.test(window.navigator.userAgent),
                    previewMaxWidth: 170,
                    previewMaxHeight: 170,
                    previewCrop: true
                }).on('fileuploadadd', function (e, data) {

                    for(var i = 0; i < data.files.length; i++){
                        filelist.push(data.files[i]);
                    }

                    console.log('list', filelist);

                    $('.cont_inicial').hide();
                    $(".btn_upload_files").off('click').on('click', function () {
//                        data.formData = {pasta_final: $('#dest_f_inp').val(), files: [filelist]};
                        data.files = filelist;
                        data.submit();
//                        $('#fileupload').fileupload('send', {files: filelist});
                    });


                    $.each(data.files, function (index, file) {

                        var $fileUpload = $('<div class="file-upload"><div/>');

//                        var $canvasContainer = $fileUpload.children().first().eq(0);
//                        $canvasContainer.addClass('canvas-container').css({
//                            'width': 'fit-content',
//                            'height': '175px',
//                            'margin': 'auto'
//                        });

                        var context =  data.context = $fileUpload.appendTo('#files');

                        var node = $("<div class=\"file\"></div>")
                                .append('<i class=\"file-cancel icon-opcoes-eliminar"></i><div class="file-info"><p>' + file.name + '</p><p>' + file.size + ' KB</p></div>');
                        node.appendTo(data.context);
//
//                        if (file.preview) {
//                            context
//                                    .prepend(file.preview);
//                        } else {
//                            context.prepend('<i class="icon-icon-upload-doc texto_amarelo icon100"></i>');
//                        }
//                        if (file.error) {
//                            context
//                                    .append($('<span class="text-danger"/>').text(file.error));
//                        }
//                        if (index + 1 === data.files.length) {
//                            data.context.find('button')
//                                    .text('Upload')
//                                    .prop('disabled', !!data.files.error);
//                        }
                    });

                    $(".file-cancel").on("click",function(e){
                        e.stopPropagation();
                        console.log('element', $(this).parents('.file-upload').index());
                        if($(this).parents('.file-upload').index() != -1){
                            filelist.splice($(this).parents('.file-upload').index() - 1, 1);
                            console.log('new list', filelist);
                        }
                        $(this).parents('.file-upload').remove();
                        if (! $('i.file-cancel').length) {
                            $uploadForm.css('display', 'block');
                        }
                    });

                }).on('fileuploadsubmit', function (e, data) {

                    $('.upload_progress').fadeIn();
                    $('.btn_upload_files').hide();
                    $('.cancel_btn').show();
                    data.formData = {pasta_final: $('#dest_f_inp').val()};

                }).on('fileuploadchunksend', function (e, data) {
                    if((data.uploadedBytes + data.chunkSize ) == data.total){
                        data.data.append('last_chunk', 1);
                    }else{
                        data.data.append('last_chunk', 0);
                    }

                }).on('fileuploadprocessalways', function (e, data) {

                    var index = data.index,
                            file = data.files[index],
                            node = $(data.context.children()[index]);
//                    if (file.preview) {
//                        node
//                                .prepend(file.preview);
//                    } else {
//                        node.prepend('<i class="icon-icon-upload-doc texto_amarelo icon100"></i>');
//                    }
//                    if (file.error) {
//                        node
//                                .append($('<span class="text-danger"/>').text(file.error));
//                    }
//                    if (index + 1 === data.files.length) {
//                        data.context.find('button')
//                                .text('Upload')
//                                .prop('disabled', !!data.files.error);
//                    }

//                    $.each($('.file-upload'), function (index, file) {

//                        var $fileUpload = $('<div class="file-upload"><div/>');
//
//                        var $canvasContainer = $fileUpload.children().first().eq(0);
//                        $canvasContainer.addClass('canvas-container').css({
//                            'width': 'fit-content',
//                            'height': '175px',
//                            'margin': 'auto'
//                        });
//
//                        var context =  data.context = $fileUpload.appendTo('#files');
//
//                        var node = $("<div class=\"file\"></div>")
//                                .append('<i class=\"file-cancel icon-opcoes-eliminar"></i><div class="file-info"><p>' + file.name + '</p><p>' + file.size + ' KB</p></div>');
//                        node.appendTo(data.context);
//

                    console.log('files length', data.files.length);
                    var reference = $('.file-upload').length - data.files.length + data.index;

                    var element = $('.file-upload:eq('+reference+')');

                        if (file.preview) {
                            element
                                    .prepend(file.preview);
                        } else {
                            element.prepend('<i class="icon-icon-upload-doc texto_amarelo icon100"></i>');
                        }
                        if (file.error) {
                            element
                                    .append($('<span class="text-danger"/>').text(file.error));
                        }
                        if (index + 1 === data.files.length) {
                            data.context.find('button')
                                    .text('Upload')
                                    .prop('disabled', !!data.files.error);
                        }

                }).on('fileuploadprogressall', function (e, data) {

                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('.tot_prog').text(progress);
                    $('#progress .progress-bar').css(
                            'width',
                            progress + '%'
                    );

                }).on('fileuploaddone', function (e, data) {
                    //var resul=jQuery.parseJSON(data.result);
                    var resul=data.result;

                    $('.btn_upload_files').show();
                    $('.cancel_btn').hide();

                    $('.err_mesages').empty();
                    $('.msg_sucesso_ajax').hide();
                    $('.msg_erro_ajax').hide();


                    if(resul['errors']){
                        $('.msg_erro_ajax').fadeIn();
                        $.each(resul['errors'],function( index , el) {
                            $('.err_mesages').append('<p>'+el+'</p>');
                        });
                    }else{
                        $('.msg_sucesso_ajax').fadeIn();
                        $('.file-upload').remove();
                        filelist = [];
                        $uploadForm.show();

                        $.each(data.result.files, function (index, file) {
                            if (file.url) {
                                var link = $('<a>')
                                        .attr('target', '_blank')
                                        .prop('href', file.url);
                                $(data.context.children()[index])
                                        .wrap(link);
                            } else if (file.error) {
                                var error = $('<span class="text-danger"/>').text(file.error);
                                $(data.context.children()[index])
                                        .append(error);
                            }

                        });

                    }

                    setTimeout(function(){$('.upload_progress').fadeOut()},1000);

                }).on('fileuploadfail', function (e, data) {
                    $('.btn_upload_files').show();
                    $('.cancel_btn').hide();

                    $('.err_mesages').empty();
                    $('.msg_sucesso_ajax').hide();
                    $('.msg_erro_ajax').hide();


                        $('.msg_erro_ajax').fadeIn();
                        $.each(data.jqXHR.responseJSON.errors,function( index , el) {
                            $('.err_mesages').append('<p>'+el+'</p>');
                        });
                    setTimeout(function(){$('.upload_progress').fadeOut()},1000);

                }).prop('disabled', !$.support.fileInput)
                        .parent().addClass($.support.fileInput ? undefined : 'disabled');
            });

            $(document).ready(function(){
                $('#files').click(function(){
                    $('#fileupload').click();
                });

                var folders='{!! $all_folders !!}';
                var inicial="{{$pasta->id}}";

                var token=$('meta[name="csrf-token"]').attr('content');

                $('.listagem_pastas').selectslidemenu({ dados: folders,inicial:inicial,token: token});


                $('#open_select_folder').click(function(){
                    $('.listagem_pastas').toggle();
                });
                $('.fecha_list_pastas').click(function(){
                    $('.listagem_pastas').hide();
                });

                $('.lang_tabs li:first-child').tab('show');
                $('.lang_content').children().first().addClass('active');

            });
        </script>

@endsection



@section('content')
    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">

            <div class="row tabs-back">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <ul class="list-inline">
                        <li><a href="{{route('gallery::backend::galery',array($pasta->id,$pasta->title))}}" class="icon_voltar"><i class="icon-voltar pading_right"></i>@lang('gallery::backend.model.upload.go_back')</a></li>
                    </ul>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                    <ul class="breadcrumb">
                        <li>@lang('gallery::backend.model.upload.gallery')</li>
                        <li>@lang('gallery::backend.model.upload.upload')</li>
                    </ul>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  msgs_gallery">



                    @if(\Illuminate\Support\Facades\Session::get('sucess-message'))
                        <div class="alert alert-sucess alert-dismissible " role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <i class="icon-permissoes icon9 pading_right" ></i>{{\Illuminate\Support\Facades\Session::get('sucess-message')}}
                        </div>
                    @endif

                    @if(\Illuminate\Support\Facades\Session::get('error-message'))
                        <div class="alert alert-erro alert-dismissible " role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <i class="icon-opcoes-eliminar icon9  pading_right"></i>{!!\Illuminate\Support\Facades\Session::get('error-message')!!}
                        </div>
                    @endif

                        <div class="alert alert-sucess alert-dismissible  msg_sucesso_ajax" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <i class="icon-permissoes icon9 pading_right" ></i>@lang('gallery::backend.messages.success.uploaded_image')
                        </div>



                        <div class="alert alert-erro alert-dismissible msg_erro_ajax" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <i class="icon-opcoes-eliminar icon9  pading_right"></i>
                            <span class="err_mesages"></span>
                        </div>


                </div>
            </div>


            <div class="row">
                <div class="col-lg-12">

                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                            <div class="titulo2" ><i class="icon-icon-peq-upload"></i>@lang('gallery::backend.model.upload.upload')</div>
                        </div>
                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">

                            <div class="container-fluid">
                            <div class="row">

                                <div class="col-lg-4 pos_relative no_padding">
                                    <a id="open_select_folder">@lang('gallery::backend.model.upload.destiny')</a> <span ><i class="icon-icon-peq-pasta icon15 margem_icon"></i><span id="dest_f" ></span></span> <input id="dest_f_inp" value="{{$pasta->id}}" type="hidden" name="folder_id">
                                    <div class="listagem_pastas">
                                        <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-lg-12 no_padding">
                                                <div class="row-eq-height-center">
                                                    <div class="col-lg-2 back_destiny"><i class="icon-voltar"></i></div>
                                                    <div class="col-lg-8 pasta_atual">@lang('gallery::backend.model.upload.gallery')</div>
                                                    <div class="col-lg-10 nova_pasta_escond">

                                                        <div class="input-group">
                                                            <span  class="input-group-addon btn_cancel_new_folder"><i class="icon-voltar"></i></span>
                                                            <input  type="text" class="form-control input_pasta" name="title" value="nova pasta">
                                                            <input id="pasta_atual_pai"  type="hidden"  name="parent_folder_id" value="">
                                                            <a  class="input-group-addon btn_save_new_folder" data-href="{{route('gallery::backend::save_folder_upload')}}"><i class="icon-permissoes icon15"></i></a>
                                                        </div>


                                                    </div>
                                                    <div class="col-lg-2 fecha_list_pastas"><i class="icon-opcoes-eliminar"></i></div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 sec_pastas_centro">
                                                <div class="new_folder_selected"></div>
                                                <div class="row">
                                                <div class="col-lg-12 no_padding">
                                                    <ul id="selectable" class="destinys">

                                                    </ul>
                                                </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 foot_destiny no_padding">
                                                <div class="innactive_btns"></div>
                                                <div class="container-fluid">

                                                    <div class="row row-eq-height-center">
                                                        <div class="col-lg-8 no_padding">
                                                            <a class="btn-yellow btn_save_folder">@lang('gallery::backend.model.upload.save_here')</a>
                                                            <div class="new_folder_selected"></div>
                                                        </div>
                                                        <div class="col-lg-4">
                                                            <div class="new_folder_selected"></div>
                                                            <i class="icon-criar-pasta icon15 new_folder_here" ></i>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>

                                    </div>

                                </div>

                                <div class="col-lg-8 text-right padding30right">
                                    <i class="icon-icon-peq-embed icon20"></i><span class="embed_link_span">Embed links</span><a class="btn-cancelar" data-toggle="modal" data-target="#add_embed">@lang('gallery::backend.model.upload.add')</a>
                                </div>

                            </div>

                        </div>
                        </div>
                    </div>


                        <div class="panel-body uploadfilesgalery">

                            <input id="fileupload" type="file" name="files[]" multiple>
                            <!-- The container for the uploaded files -->
                            <div id="files" class="files row">
                                <div class="cont_inicial">
                                    <p class="fonte_15_lato">@lang('gallery::backend.model.upload.drag_and_drop')</p>
                                    <p class="fonte_12_lato_regular">@lang('gallery::backend.model.upload.drag_and_click')</p>
                                    <div class="row">
                                        <div class="col-lg-6 col-lg-offset-3 fonte_12_lato_light_cinza documentos_perm">
                                            <div class="row">
                                                <div class="col-lg-15"><i class="icon-icon-upload-doc texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.documents')</p></div>
                                                <div class="col-lg-15"><i class="icon-icon-upload-zip texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.zipped_folders')</p></div>
                                                <div class="col-lg-15"><i class="icon-icon-upload-img texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.images')</p> </div>
                                                <div class="col-lg-15"><i class="icon-icon-upload-video texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.videos')</p></div>
                                                <div class="col-lg-15"><i class="icon-icon-upload-som texto_amarelo icon60"></i><p>@lang('gallery::backend.model.upload.audio')</p></div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div>
                                <p class="fonte_12_lato_light allowed_upload">

                                    @lang('gallery::backend.model.upload.allowed_upload')
                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus" data-content="
                                    <div>PDF</div>
                                    <div>DOC</div>
                                    <div>DOCX</div>
                                    <div>XLS</div>
                                    <div>PPT</div>
                                    "
                                            > <i class="icon-icon-upload-doc icon15 pading_right"></i>@lang('gallery::backend.model.upload.documents'); <i class="icon-info-extra-1 icon-cinza"></i></a>
                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus"  data-content="
                                    <div>JPEG</div>
                                    <div>PNG</div>
                                    <div>GIF</div>
                                    "
                                            ><i class="icon-icon-upload-img icon15 pading_right"></i>@lang('gallery::backend.model.upload.images'); <i class="icon-info-extra-1 icon-cinza"></i></a>
                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus"  data-content="
                                   <div>MKV</div>
                                    <div>AVI</div>
                                    <div>MP4</div>
                                    <div>H.264</div>
                                    "><i class="icon-icon-upload-video icon15 pading_right"></i>@lang('gallery::backend.model.upload.videos'); <i class="icon-info-extra-1 icon-cinza"></i></a>
                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus"  data-content="
                                    <div>Zip</div>
                                    "><i class="icon-icon-upload-zip icon15 pading_right"></i>@lang('gallery::backend.model.upload.zipped_folders'); <i class="icon-info-extra-1 icon-cinza"></i></a>
                                    <a tabindex="0" role="button" data-toggle="popover" data-trigger="focus"  data-content="
                                    <div>WAV</div>
                                    <div>MP3</div>
                                    <div>RAW</div>
                                    "><i class="icon-icon-upload-som icon15 pading_right"></i> @lang('gallery::backend.model.upload.audio'). <i class="icon-info-extra-1 icon-cinza"></i></a>
                                </p>
                            </div>


                        </div>
                    </div>

                    <div class="col-lg-12">
                        <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-1 ">
                                <button class="btn btn-default btn-yellow btn_upload_files"> @lang('gallery::backend.model.upload.upload')</button>
                                <button class="btn btn-default btn-cancelar cancel_btn"> @lang('gallery::backend.model.upload.cancel')</button>
                            </div>
                            <div class="col-lg-3 upload_progress">

                                <p class="fonte_10_lato_regular">@lang('gallery::backend.model.upload.progress_upload') <span class="tot_prog"></span>%</p>
                                <div id="progress" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>
                            </div>
                            </div>


                        </div>
                    </div>




                </div>
            </div>

            <div class="row">

            </div>
        </div>

    <!----------------------------------------------------- MODAL ADD EMBED LINK ----------------------------------------------------->
    <div class="modal modal-aux" id="add_embed" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-aux" role="document">
            <form class="form_elimina_media" method="POST" action="{{route('gallery::backend::store_embed_link')}}" >
                <input  name="media_folder_id" type="hidden" value="{{$pasta->id}}">
                                {!! csrf_field() !!}
                <div class="modal-content">
                    <div class="panel-titulo"><span class="titulo"><i class="icon-icon-peq-embed"></i> EMBED LINKS</span></div>
                    <div class="modal-body">

                        <!-- Nav tabs -->
                        <ul class="tabs_brainy lang_tabs" role="tablist">
                            @foreach($locales as $loc)
                                <li role="presentation" class=""><a href="#{{$loc}}"  role="tab" data-toggle="tab">{{$loc}}</a></li>
                            @endforeach
                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content lang_content">


                                @foreach($locales as $loc)
                                    <div role="tabpanel" class="tab-pane fade in  margem_20" id="{{$loc}}">


                                        <div class="form-group fom-style-hidden2 @if($errors->first('titulo_'.$loc) ) has-error @endif">
                                            <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.title')</label>
                                            <input type="text" class="form-control" id="exampleInputEmail1" name="titulo_{{$loc}}" value="{{old('titulo_'.$loc)}}">

                                            {!! $errors->first('titulo_'.$loc,'<span class="validator_errors">:message</span>')!!}
                                        </div>

                                        <div class="form-group fom-style-hidden @if($errors->first('descricao_'.$loc) ) has-error @endif">
                                            <label for="exampleInputEmail1">@lang('gallery::backend.model.edit.description')</label>
                                            <textarea class="form-control" rows="3" name="descricao_{{$loc}}" id="editor_embed_d_{{ $loc }}">{{old('descricao_'.$loc)}}</textarea>
                                            {!! $errors->first('descricao_'.$loc,'<span class="validator_errors">:message</span>')!!}
                                        </div>

                                    </div>
                                @endforeach
                            </div>




                        <div class="form-group fom-style-hidden2 @if($errors->first('identifier_token') ) has-error @endif">
                            <label for="exampleInputEmail1">@lang('gallery::backend.model.upload.add_link')</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" name="identifier_token" value="{{old('identifier_token')}}">
                            {!! $errors->first('identifier_token','<span class="validator_errors">:message</span>')!!}
                        </div>


                    </div>
                    <div class="modal-footer modal-eliminar padding-bottom-30">
                        <button type="submit" class="btn btn-default btn-yellow">@lang('gallery::backend.model.upload.add')</button>
                        <button type="button" class="btn btn-default btn-cancelar" data-dismiss="modal"> @lang('gallery::backend.model.upload.cancel') </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!---------------------------------------------------MODAL ADD EMBED LINK   ----------------------------------------------------->
@endsection