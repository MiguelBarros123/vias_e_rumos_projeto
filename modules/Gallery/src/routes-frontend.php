<?php

Route::group(['namespace' => 'Brainy\Gallery\Controllers\Frontend'], function () {

    Route::get('/', 'ModelFrontendController@index')->name('model.index');

    Route::get('/get-storage-image/{id}', array('as'=>'storage_image','uses'=>'ModelFrontendController@getImageThumbnail'));
    Route::get('/get-storage-image-w-paramps/{id}/{with}/{height}', array('as'=>'storage_image_p','uses'=>'ModelFrontendController@getImageParams'));
    
    Route::get('/storage-image/{template}/{filename}', array('as'=>'cache_image','uses'=>'\Intervention\Image\ImageCacheController@getResponse'));

});
