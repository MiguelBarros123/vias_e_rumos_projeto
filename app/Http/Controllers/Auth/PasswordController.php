<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use Brainy\Profiles\Models\Profile;
use App\Http\Controllers\Controller;
use Brainy\Cms\Models\ProfileReservedArea;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetForm(Request $request, $token = null)
    {
        if (is_null($token)) {
            return $this->getEmail();
        }

        $email = $request->input('email');
        if (property_exists($this, 'resetView')) {
            return view($this->resetView)->with(compact('token', 'email'));
        }

        if (view()->exists('auth.passwords.reset')) {
            $user = User::where('email',$email)->first();
            $profile = Profile::where('user_id', $user->id)->first();
            $profile_reserved_area = ProfileReservedArea::where('user_id', $user->id)->first();
            if($profile_reserved_area){
                return view('cms::frontend.pages.reset_password_area_reserved')->with(compact('token','email'));
            }
            return view('auth.passwords.reset')->with(compact('token', 'email'));
        }
    }
}
