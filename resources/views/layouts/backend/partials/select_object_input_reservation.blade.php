<select class="form-control {{$class}}" name="{{$name}}">
    @foreach($array as $key => $a)
        <option value="{{ $a->id }}" {{ (($a->id) == $selected)? 'selected': ''}}>{{ $a->type_rent_item()->first()->name }}  {{$a->name}}</option>
    @endforeach
</select>