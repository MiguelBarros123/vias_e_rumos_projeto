<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22-07-2016
 * Time: 18:03
 */

namespace Brainy\Profiles\Models;


use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProfileContact extends Model{


    protected $fillable=['type','contact'];



}