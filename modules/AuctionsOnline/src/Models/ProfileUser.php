<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 22-07-2016
 * Time: 18:03
 */

namespace Brainy\Profiles\Models;


use Dimsav\Translatable\Translatable;
use Illuminate\Database\Eloquent\Model;

class ProfileUser extends Model{

    protected $fillable=['profile_id', 'company_id'];

//    public function user(){
//        return $this->hasOne(Profile::class);
//    }

    public function users(){
        return $this->belongsToMany(Profile::class);
    }

    public function company(){
        return $this->belongsTo(Profile::class);
    }

}