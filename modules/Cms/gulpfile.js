var elixir = require('laravel-elixir');

elixir(function(mix) {
    mix.styles([
        'backend-styles.css'
    ], '../../public/css/cms/backend.css');

    mix.styles([
            'backend-styles.css'
        ], 'public/css/backend.css')
        .styles([
            'frontend-styles.css'
        ], 'public/css/frontend.css')
        .scripts([
            'backend-app.js',
            'mediafinder.js',
            'context_menu.js',
            'jquery-ui.min.js',
            'local_storage.js',
            'selectslidemenu.js',
            'ValidationBidding.js',
        ], '../../public/js/cms/backend.js')
        .scripts([
            'frontend-app.js'
        ], 'public/js/frontend.js');

    mix.scripts([
        'frontend_contacts.js'
    ], '../../public/js/cms/front/frontend_contacts.js');


    mix.browserify('resources/assets/js/generator-cms/generator.js', '../../public/js/cms/generator-cms/generator.js');

    mix.copy('resources/assets/icons', '../../public/icons/cms');
    mix.copy('resources/assets/js/fileupload', '../../public/js/cms/fileupload');
    mix.copy('resources/assets/js/tagsinput.js', '../../public/js/cms/tagsinput.js');
});
