<?php

namespace Brainy\Cms\Models;

use Illuminate\Database\Eloquent\Model;

class RepresentativeEmail extends Model
{
    protected $table = 'representative_emails';

    public $timestamps = false;

    public function representative()
    {
    	return $this->belongsTo(Representative::class);
    }
}