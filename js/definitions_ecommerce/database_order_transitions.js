/**
 * Created by User on 27-09-2016.
 */

$(document).ready(function() {
    var options = {
        rota: rota,
        colunas: [
            {data: 'first_state', name: 'first_state'},
            {data: null, defaultContent: '', name: 'for', orderable: false, searchable: false},
            {data: 'second_state', name: 'second_state'},
            {data: 'change', name: 'change'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        config: {
            initComplete: function (settings, json) {
                $(function () {
                    $.logicDatatable(options);
                    $('.edit_item_datatable').click(function(){

                        $('#form_update_tax').attr('action',$(this).attr('data-route'));
                        $('#quantity_transitions').val($(this).attr('data-quantity'));
                        $('#unit_transitions option[value="'+$(this).attr('data-unit')+'"]').prop('selected', true);

                    });

                });
            },
            fnCreatedRow: function (nRow, aData, iDataIndex) {

            },
            'fnDrawCallback': function () {

            }

        },
        searchableElementId: 'search'
    };

    $('.default-table4').mydatatable(options);


    var optionsBtb = {
        rota: rotaBtb,
        colunas: [
            {data: 'first_state', name: 'first_state'},
            {data: null, defaultContent: '', name: 'for', orderable: false, searchable: false},
            {data: 'second_state', name: 'second_state'},
            {data: 'change', name: 'change'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        config: {
            initComplete: function (settings, json) {
                $(function () {
                    $.logicDatatable(optionsBtb);
                    $('.edit_item_datatable').click(function(){

                        $('#form_update_tax_btb').attr('action',$(this).attr('data-route'));
                        $('#quantity_transitions_btb').val($(this).attr('data-quantity'));
                        $('#unit_transitions_btb option[value="'+$(this).attr('data-unit')+'"]').prop('selected', true);

                    });

                });
            },
            fnCreatedRow: function (nRow, aData, iDataIndex) {

            },
            'fnDrawCallback': function () {

            }

        },
        searchableElementId: 'search_btb'
    };

    $('.default-table4_btb').mydatatable(optionsBtb);



});
//# sourceMappingURL=database_order_transitions.js.map
