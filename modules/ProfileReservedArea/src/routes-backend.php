<?php

Route::group(['namespace' => 'Brainy\ProfileReservedArea\Controllers\Backend'], function () {
    Route::post('profiles_reserved_area/change-profile-state/{id}', 'ProfileReservedAreaBackendController@changeProfileState')->name('profilereservedarea.change_profile_state');
    Route::get('profiles_reserved_area','ProfileReservedAreaBackendController@index')->name('profilereservedarea.index');
    Route::get('profiles_reserved_area/index-ajax','ProfileReservedAreaBackendController@index_ajax')->name('profilereservedarea.index_ajax');
    Route::get('profiles_reserved_area/reorder','ProfileReservedAreaBackendController@reorder')->name('profilereservedarea.reorder');
    Route::get('profiles_reserved_area/create','ProfileReservedAreaBackendController@create')->name('profilereservedarea.create');
    Route::get('profiles_reserved_area/edit/{id?}','ProfileReservedAreaBackendController@edit')->name('profilereservedarea.edit');
    Route::post('profiles_reserved_area/store','ProfileReservedAreaBackendController@store')->name('profilereservedarea.store');
    Route::put('profiles_reserved_area/update/{id}','ProfileReservedAreaBackendController@update')->name('profilereservedarea.update');
    Route::delete('profiles_reserved_area/delete','ProfileReservedAreaBackendController@delete')->name('profilereservedarea.delete');

});
