<?php
namespace Brainy\Profiles\Controllers\Backend;

use App\Http\Requests;

use Brainy\Profiles\Models\CustomField;
use Brainy\Profiles\Models\CustomOption;
use Brainy\Profiles\Models\Profile;
use Brainy\Profiles\Requests\PersonalizedFieldRequest;
use Illuminate\Http\Request;
use Brainy\Framework\Controllers\BackendController;
use Yajra\Datatables\Facades\Datatables;

class CustomFieldController extends BackendController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$custom_fields = CustomField::orderBy('id', 'desc')->paginate(10);

		return view('profiles::backend.custom_fields.index', compact('custom_fields'));
	}

	public function index_ajax()
    	{

    		$taxes = CustomField::all();

                    return Datatables::of($taxes)
                ->editColumn('type',function ($tax){
                    if($tax->type=='single'){
                        return trans('profiles::backend.common.text_field');
                    }

                    $collection=$tax->custom_options->toArray();
                    $mapeado=array_map(function($value) {
                        return '<span class="item_yellow">'.$value['name'].'</span>'; },
                        $collection);

                    return $mapeado;



                })

                ->addColumn('action', function ($tax) {
                $rota=route('profiles::backend::backend.profiles.custom_fields.delete');
                $rotaeditar=route('profiles::backend::backend.profiles.custom_fields.edit',[$tax->id]);
                $route_see=route('profiles::backend::backend.profiles.custom_fields.update',[$tax->id]);

                    $impossible=0;


                $string = '
                        <div class="btn-group pull-right">
                           <button type="button" class="btn btn-default btn-xs btn-user-options dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <i class="icon-roda-dentada icon10"></i>
                          </button>
                          <ul class="dropdown-menu">
                                 <li><a  class="edit_item_datatable"  href="'.$route_see.'"><i class="icon-visivel"></i>  ' . trans("profiles::backend.common.see") . '</a></li>
                            <li><a  class="edit_item_datatable" href="'.$rotaeditar.'" ><i class="icon-editar"></i>  ' . trans("profiles::backend.common.edit") . '</a></li>
                            <li><a data-impossible-delete="'.$impossible.'" data-target="#delete_resource" data-toggle="modal" class="delete_item_table" data-id="' . $tax->id . '" data-route="'.$rota.'"><i class="icon-opcoes-eliminar"></i> ' . trans("profiles::backend.common.delete") . ' </a></li>
                          </ul>
                        </div>

                       ';
                return $string;
            })
                        ->addColumn('rota_final', function ($client) {
                            return route('profiles::backend::backend.profiles.custom_fields.show',[$client->id]);
                        })

                        ->make(true);

    	}



    	public function index_custom_fields_ajax($id=null)
    	{

            $profile=Profile::find($id);

    		$taxes = CustomField::all();

                    return Datatables::of($taxes)
                ->editColumn('type',function ($tax){
                    if($tax->type=='single'){
                        return trans('profiles::backend.common.text_field');
                    }

                    $collection=$tax->custom_options->toArray();
                    $mapeado=array_map(function($value) {
                        return '<span class="item_yellow">'.$value['name'].'</span>'; },
                        $collection);

                    return implode(' ',$mapeado);



                })
                        ->addColumn('template',function($item)use($profile){
                            $value_selected=null;
                            if($profile!=null){
                                if($profile->custom_fields()->contains($item->id)){
                                    $query=$profile->custom_options()->where('custom_field_id',$item->id)->first();
                                    $value_selected=$query;
                                }
                            }
                            return view('profiles::backend.profile.partials.personalized_field_profile',['item'=>$item,'value_selected'=>$value_selected])->render();
                        })
                        ->addColumn('already_selected', function ($custom)use($profile) {
                            if($profile!=null){
                                //logic for edition
                                if($profile->custom_fields()->contains($custom->id)){
                                    return 1;
                                }
                            }
                            return 0;
                        })

                        ->make(true);

    	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('profiles::backend.custom_fields.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function store(PersonalizedFieldRequest $request)
	{


		$custom_field = new CustomField();

		$custom_field->name = $request->input("name");
        $custom_field->save();

        if(array_key_exists('subscript',$request->input())){
            $custom_field->type = 'multiple';
            foreach ($request->fields as $new_field){
                $custom_field->custom_options()->save(new CustomOption(['name'=>$new_field]));
            }

        }else{
            $custom_field->type = 'single';
            $custom_field->custom_options()->save(new CustomOption(['name'=>$request->input("name")]));
        }

		$custom_field->save();


		if($request->submit === 'stay'){
			return redirect()->route('profiles::backend::backend.profiles.custom_fields.edit',[$custom_field->id])->with('sucess-message', trans('profiles::backend.messages.success.create_personalized'));
		} else {
			return redirect()->route('profiles::backend::backend.profiles.custom_fields.index')->with('sucess-message', trans('profiles::backend.messages.success.create_personalized'));
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$custom_field = CustomField::findOrFail($id);

		return view('profiles::backend.custom_fields.show', compact('custom_field'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$custom_field = CustomField::findOrFail($id);

		return view('profiles::backend.custom_fields.edit', compact('custom_field'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param Request $request
	 * @return Response
	 */
	public function update(PersonalizedFieldRequest $request, $id)
	{
		$custom_field = CustomField::findOrFail($id);
        $custom_field->name = $request->input("name");

        $custom_field->custom_options()->delete();


        if(array_key_exists('subscript',$request->input())){
            $custom_field->type = 'multiple';
            foreach ($request->fields as $new_field){
                $custom_field->custom_options()->save(new CustomOption(['name'=>$new_field]));
            }

        }else{
            $custom_field->type = 'single';
            $custom_field->custom_options()->save(new CustomOption(['name'=>$request->input("name")]));

        }

        $custom_field->save();


		if($request->submit === 'stay'){
			return redirect()->back()->with('sucess-message',trans('profiles::backend.messages.success.update_personalized'));
		} else {
			return redirect()->route('profiles::backend::backend.profiles.custom_fields.index')->with('sucess-message',trans('profiles::backend.messages.success.update_personalized'));
		}



	}


/**
	 * Delete the specified resource in storage.
	 *
	 * @param Request $request
	 * @return Response
	 */
	public function delete(Request $request){

	        $rows=explode(',',$request->deleted_rows);

            foreach($rows as $id){
                $custom_field = CustomField::findOrFail($id);
                $custom_field->custom_options()->delete();
               $custom_field->delete();
            }

    		return redirect()->route('profiles::backend::backend.profiles.custom_fields.index')->with('sucess-message', trans('profiles::backend.messages.success.delete_personalized'));
	}

}
