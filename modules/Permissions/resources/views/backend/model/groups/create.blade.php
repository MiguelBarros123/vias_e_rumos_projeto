@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
    <link rel="stylesheet" type="text/css" href="{{asset('css/permissions/backend.css')}}">
@endsection

@section('module-scripts')
    <script src="{{asset('js/permissions/backend.js')}}"></script>

@endsection

@section('content')


    <div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">

            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">
                        <li>
                            <a href="{{route('permissions::backend::group.index')}}" class="icon_voltar">
                                <i class="icon-voltar pading_right"></i>@lang('permissions::backend.return')
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

            {!! DaveJamesMiller\Breadcrumbs\Facade::render('admin.permissions.groups.create') !!}

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @if(session('sucess-message'))
                        <div class="alert alert-sucess alert-dismissible " role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <i class="icon-permissoes icon10" ></i>{{ session('sucess-message') }}
                        </div>
                    @endif

                    {!! $errors->first('error-message','
                    <div class="alert alert-erro alert-dismissible " role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close" ><span aria-hidden="true">&times;</span></button>
                        <i class="icon-opcoes-eliminar" style="padding-bottom: 1px;"></i> :message
                    </div>
                    ')!!}
                </div>
            </div>


            <form method="POST" action="{{route('permissions::backend::group.store')}}">
                {!! csrf_field() !!}
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel ">

                            <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-group"></i> @lang('permissions::backend.group.create.new')
                            </span>
                            </div>
                            <div class="panel-body">

                                <div class="form-group fom-style-hidden2 @if($errors->first('name') ) has-error @endif">
                                    <label for="exampleInputEmail1">@lang('permissions::backend.group.create.name')</label>
                                    <input type="text" name="name" class="form-control " value="{{old('name')}}">
                                    {!! $errors->first('name','<span class="validator_errors">:message</span>')!!}
                                </div>

                            </div>
                        </div>


                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">

                        <div class="panel ">

                            <div class="panel-titulo">
                                <span class="titulo">
                                    <i class="icon-permissoes icon20"></i>@lang('permissions::backend.group.create.current_permissions')
                                </span>
                            </div>
                            <div class="panel-body">

                                {!! $errors->first('permissoes','<span class="validator_errors">:message</span>')!!}

                                <ul class="list-group panel treeview_permissions">
                                    @foreach($modulos as $modulo)


                                        <li>
                                            <label class="checkbox-default  checkbox_permissions">
                                                <input type='checkbox'
                                                       class="check_perm">
                                                <span class="check_span"></span>
                                                <a href="#modulo_{{$modulo->name}}" class="list-group-item collapsed"
                                                   data-toggle="collapse">
                                                    @lang($modulo->label)
                                                    @if(count($modulo->modules)>0)<i
                                                            class="icon-baixo"></i>@endif
                                                </a>
                                            </label>

                                            <ul class="collapse list-group-submenu" id="modulo_{{$modulo->name}}">
                                                @foreach(\Brainy\Framework\Facades\Brainy::containerModules($modulo->name) as $assunto)


                                                    <li>
                                                        <label class="checkbox-default  checkbox_permissions">
                                                            <input type='checkbox'
                                                                   class="check_perm">
                                                            <span class="check_span"></span>
                                                            <a href="#assunto_{{$assunto->name}}"
                                                               class="list-group-item collapsed" data-toggle="collapse"
                                                               data-parent="#assunto_{{$assunto->name}}">

                                                                @lang($assunto->label)
                                                                @if(count($assunto->permissions)>0)<i
                                                                        class="icon-baixo"></i>@endif
                                                            </a>
                                                        </label>


                                                        <ul class="collapse list-group-submenu"
                                                            id="assunto_{{$assunto->name}}">

                                                            @foreach($assunto->permission_types as $database_permition)

                                                                <li class="perm_spacer">
                                                                    <label class="checkbox-default  checkbox_permissions">
                                                                        <input class="check_perm" name="permissoes[]"
                                                                               value="{{$database_permition}}"
                                                                               type='checkbox'>
                                                       <span class=""
                                                             data-parent="#SubMenu1">@lang($database_permition)</span>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        </ul>

                                                    </li>

                                                @endforeach
                                            </ul>
                                        </li>
                                    @endforeach
                                </ul>

                            </div>
                        </div>

                        <button type="submit" class="btn btn-default btn-yellow">@lang('permissions::backend.save')</button>
                        <a href="{{route('permissions::backend::group.index')}}" class="btn btn-default btn-cancelar ">@lang('permissions::backend.cancel')</a>


                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="action_multiple">

        <div class="wrap_action_multiple text-right">
            <a> <img src="{{asset('back/icons/opcoes_eliminar.png')}}">@lang('permissions::backend.model.groups.create.delete')</a>
        </div>
    </div>

@endsection




