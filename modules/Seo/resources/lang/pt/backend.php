<?php

return [
    'module-label'=>'Seo',
    'permission'=>[
        'seo'=>[
            'manage'=>'Gerir seo'
        ],
        'favicon'=>[
            'manage'=>'Gerir favicon'
        ]
    ],

    'model' => [
        'index' => [
            //@lang('seo::backend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view backend page',
        ],
        'menu' => [
            'seo' => 'Seo',
            'favicon' => 'Favicon',
        ],
    ],
    'common' => [
      'search' => 'Pesquisar',
      'pages' => 'Páginas',
      'images' => 'Imagens',
      'route_url' => 'Friendly Url',
      'title' => 'Title tag',
      'meta_description' => 'Meta Description',
      'image_alt' => 'Alt tag',
      'upload_favicon' => 'Upload Favicon',
      'add_zip' => 'Adicione um zip com os seus favicons',
      'zip_upload' => 'Upload de Zip'
    ],
    'messages' => [
        //@lang('seo::backend.messages.notifications.new-task')
        'notifications' => [
            'new-task' => 'You have a new pending task',
        ],
        'success' => [
            'update_page_description' => 'Seo editado com sucesso',
            'update_favicon' => 'Favicon alterado com sucesso',
        ],
        'error' => [
            'error_favicon' => 'Não foi possivel alterar o seu favicon, por favor verifique se o nome do ficheiro é favicon.zip'
        ],
    ],
];
