/**
 * Created by User on 27-09-2016.
 */

$(document).ready(function() {
    var options = {
        rota: rota,
        colunas: [
            {data: 'image', name: 'image'},
            {data: 'name', name: 'name'},
            {data: 'state', name: 'state'},
            {data: 'direct_payment', name: 'direct_payment'},
            {data: 'action', name: 'action', orderable: false, searchable: false}
        ],
        config: {
            initComplete: function (settings, json) {
                $(function () {
                    //$.logicDatatable(options);
                    //$('.edit_item_datatable')
                });
            },
            fnCreatedRow: function (nRow, aData, iDataIndex) {

            },
            'fnDrawCallback': function () {

                $('*[id^="toggle-one"]').bootstrapToggle();
                $('*[id^="toggle-two"]').bootstrapToggle();

                $('*[id^="toggle-one"]').on('change', function(){
                    var line = this.getAttribute('data-row');
                    console.log(line);

                    var url = changeVisibility;
                    url = url.replace(':id', line);

                    $.ajax({
                        url: url,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        dataType: 'json',
                        success:function(data)
                        {


                        }
                    });

                });

                $('*[id^="toggle-two"]').on('change', function(){
                    var line = this.getAttribute('data-row');
                    console.log(line);

                    var url = payment;
                    url = url.replace(':id', line);

                    $.ajax({
                        url: url,
                        type: 'post',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        },
                        dataType: 'json',
                        success:function(data)
                        {


                        }
                    });

                });

            }

        },
        searchableElementId: 'search'
    };

    $('.default-table4').mydatatable(options);





});

//# sourceMappingURL=database_payment_methods.js.map
