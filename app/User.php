<?php

namespace App;

use Brainy\Cms\Models\ProfileReservedArea;
use Brainy\Cms\Models\FrontUser;
use Brainy\Cms\Models\UserAddress;
use Brainy\Profiles\Models\Profile;
use Brainy\Framework\Models\UserDetail;
use Brainy\Profiles\Models\ProfileUser;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'confirmed','confirmation_code','confirmed_by_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function details()
    {
        return $this->hasOne(UserDetail::class, 'id');
    }

    public function front()
    {
        return $this->hasOne(Profile::class, 'id');
    }

    public function profileReservedArea()
    {
        return $this->hasOne(ProfileReservedArea::class);
    }

    public function profile()
    {
        return $this->hasOne(Profile::class);
    }

    public function user_addresses(){
        return $this->hasMany(UserAddress::class);
    }

    public function profile_users(){
        return $this->hasOne(ProfileUser::class);
    }

    public function is_backend_user()
    {
        $profiles_id = ProfileReservedArea::pluck('id')->toArray();
        array_push($profiles_id, 2);
        if(in_array($this->id, $profiles_id)){
            return true;
        }
        return false;
    }

    public function getResponse($lang)
    {

        $hr = date("H");
        if($hr >= 12 && $hr<20) {
            $resp = trans('cms::frontend.messages.boa_tarde',[],$lang);}
        else if ($hr >= 0 && $hr <12 ){
            $resp = trans('cms::frontend.messages.bom_dia',[],$lang);}
        else {
            $resp = trans('cms::frontend.messages.boa_noite',[],$lang);
        }

        return $resp;
    }

    public function isDeveloper()
    {
        return in_array($this->email, config('developers.emails'));
    }
}
