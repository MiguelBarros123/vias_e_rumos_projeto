@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/rent/backend.css')}}">
<link rel="stylesheet" type="text/css" href="{{asset('back/css/spectrum.css')}}">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/rowreorder/1.1.2/css/rowReorder.dataTables.min.css">
@endsection

@section('module-scripts')
<!-- <script src="{{asset('js/rent/backend.js')}}"></script> -->
<script src="https://cdn.datatables.net/1.10.11/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/rowreorder/1.1.0/js/dataTables.rowReorder.min.js"></script>
<script src="{{asset('back/js/datatablesselect.js')}}"></script>
<script src="{{asset('back/js/datatables.js')}}"></script>
<script src="{{asset('back/js/datatables_select_logic.js')}}"></script>
<script src="{{asset('back/js/spectrum.js')}}"></script>

<script>
    $(document).ready(function () {

        var options = {
            rota: "{{ route('auctionsonline::backend::auctions_online.index_ajax')}}",
                // rowReorder: {
                //     selector: 'i.icon-icon-peq-img-size'
                // },
                colunas: [
                {data: 'title', name: 'title'},
                {data: 'ref_intern', name: 'ref_intern'},
                {data: 'date', name: 'date'},
                {data: 'local', name: 'local'},
                {data: 'featured', name: 'featured'},
                {data: 'active', name: 'active'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
                {data: null, defaultContent: '', orderable: false, searchable: false}
                ],
                config: {
                    initComplete: function (settings, json) {
                        $(function () {
                            $.logicDatatable(options);

                        });
                    },
                    fnCreatedRow: function (nRow, aData, iDataIndex) {
                        //for clicable rows
                        $(nRow).attr('data-rota-see', aData['rota_final']);
                        var tr2 = $(nRow).closest('tr');
                        tr2.find("td:first").addClass('details-control');
                        tr2.find("td:not(:first, :last, :nth-last-child(2))").addClass('clickable_url');
                    },

                },
                searchableElementId: 'search'
            };


            $('.default-table').mydatatable(options);

            $('.default-table').on('click', 'td.clickable_url', function () {
                var tr = $(this).closest('tr');
                window.location.href = tr.attr('data-rota-see');

            });

            options.datatable.on('row-reorder', function (e, diff, edit) {
                var elem = options.datatable.row(edit.triggerRow.selector.rows).data();
                var chosen = diff.find(function(element) { return options.datatable.row(element.node).data() === elem });
                var oldPosition = elem.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');

                for ( var i=0, ien=diff.length ; i<ien ; i++ ) {
                    if(diff[i].oldPosition === chosen.newPosition){
                        var rowData = options.datatable.row( diff[i].node ).data();
                        var newPosition = rowData.order.replace('<i class=\"icon-icon-peq-img-size\"><\/i>','');
                        $.ajax({
                            type: 'get',
                            url: '{{route('rent::backend::auctions.reorder')}}',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                id: elem.id,
                                fromPosition: oldPosition,
                                toPosition: newPosition
                            },
                            success: function (data) {
                                options.datatable.ajax.reload(null, false);
                            }
                        });
                    }
                }

            });

            $('.icon_delete_inside_view').on('click', function(){
                $('#choose_auction_type').modal('show');
            });
        });
    </script>

    @endsection

    @section('content')
    <div class="conteudo_central margin-top-no-menu ">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 ">

                    @include('auctionsonline::backend.layouts.top_menu')

                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-m-12 no_padding">
                            <input id="search" class="search search_icon" type="text"
                            placeholder="@lang('profiles::backend.common.search')">
                        </div>
                    </div>
                    @include('layouts.backend.messages')

                    {!! DaveJamesMiller\Breadcrumbs\Facade::render('auctions_online.index') !!}


                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no_padding">
                            <table id="unpaid_table" class="table default-table table-hover">
                                <thead>
                                    <tr>
                                        <!-- <th></th> -->
                                        <th>@lang('rent::backend.common.title')</th>
                                        <th>@lang('rent::backend.common.ref_intern')</th>
                                        <th>@lang('rent::backend.common.date')</th>
                                        <th>@lang('rent::backend.common.local')</th>
                                        <th>@lang('rent::backend.common.featured')</th>
                                        <th>@lang('rent::backend.common.active')</th>
                                        <th class="multiple-options">
                                            @include('layouts.backend.partials.delete_datatables',['id'=>'unpaid_table','route'=>'rent::backend::backend.rent.auctions.delete'])
                                        </th>
                                        <th>
                                            <label class="checkbox-default">
                                                <input type='checkbox' id="sel_all_perm_unpaid_table">
                                                <span></span>
                                            </label>
                                        </th> 
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
    @include('layouts.backend.partials.delete_form',['delete_message'=>'cms::backend.common.sure_delete'])
    @endsection
