<div class="row">
    <div class="col-lg-12">
        <div class="panel ">
            <div class="panel-titulo">
                <span class="titulo">
                    <i class="icon-alterar-permissoes-1 "></i>
                    @lang($composed->panel_name)
                </span>
            </div>
            <div class="panel-body no_padding">
                <input class="search search_icon" type="text" id="searchbox_{{ $composed->id }}" placeholder="@lang('cms::backend.model.common.search')">


                    <a class="add_btn" href="{{route('cms::backend::pages.create',[ $composed->id ])}}">
                        <i class="icon-adicionar icon10"></i>
                        @lang('cms::backend.model.common.new')
                    </a>

                <table id="default-table_{{$composed->id}}" class="table default-table default-table_{{ $composed->id }}">
                    <thead>
                        <tr>
                            <th></th>
                            <th>@lang('cms::backend.model.common.photo')</th>
                            <th>@lang('cms::backend.model.common.title')</th>
                            <th>@lang('cms::backend.model.common.identifier')</th>
                            <th>@lang('cms::backend.model.common.date')</th>
                            <th></th>
                            <th class="table_no_order">
                                    @lang('cms::backend.model.common.state')
                            </th>
                            <th class="multiple-options">
                                @include('layouts.backend.partials.delete_datatables',['id'=>'default-table_'.$composed->id,'route'=>'cms::backend::pages.delete_multiple_composeds'])
                            </th>
                            <th>
                                <label class="checkbox-default">
                                    <input type='checkbox' id="sel_all_perm_default-table_{{ $composed->id }}">
                                    <span></span>
                                </label>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>
