<?php

namespace Brainy\Definitions\Controllers\Backend;

use App\BiddingsDefinitions;
use Brainy\Framework\Controllers\BackendController;

class DefinitionsBackendController extends BackendController
{
    public function index()
    {
        $biddings = BiddingsDefinitions::all();
        return view('definitions::backend.model.index',compact('biddings'));
    }        

    public function store()
    {
        BiddingsDefinitions::truncate();
        foreach(request('licitacoes') as $bidding)
        {
            BiddingsDefinitions::create([
                'min_bidding' => intval($bidding['min_licitacao']*100),
                'max_bidding' => intval($bidding['max_licitacao']*100),
                'next_bidding' => intval($bidding['proxima_licitacao']*100),
            ]);
        }
        return redirect()->route('definitions::backend::definitions.index');
    }

    public function addMoreConditions()
    {
        return view('definitions::backend.partials.add_more_conditions');
    }
}
