@extends('layouts.frontend.app')

@section('content')
<div class="mar-top-5">
    @include('cms::frontend.pages.layouts.search-bar')
</div>
<div class="empresa-body">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <h2>RESOLUÇÃO ALTERNATIVA DE LITÍGIOS DE CONSUMO</h2>
<p>De acordo com a Lei 144/2015 de 8 de Setembro de 2015, relativa à resolução alternativa de litígios de consumo, informamos que em caso de litígio o consumidor pode recorrer a uma Entidade de Resolução Alternativa de Litígio de Consumo:</p>

<p>Resolução de Litígios em Linha</p>
<p>Web: <a href="https://webgate.ec.europa.eu/odr/main/index.cfm" target="_blank">https://webgate.ec.europa.eu/odr/main/index.cfm</p>
<p>Mais informações em Portal do Consumidor : <a href="http://www.consumidor.pt" target="_blank">www.consumidor.pt</a></p>

        </div>
    </div>
</div>
@endsection
