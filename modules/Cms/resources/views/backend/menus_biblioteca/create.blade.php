@extends('layouts.backend.app')

@section('title','Criacao Menu Biblioteca')

@section('module-styles')

@endsection

@section('module-scripts')

@endsection

@section('content')
<div class="conteudo_central margin-top-no-menu">
        <div class="container-fluid">
            <div class="row tabs-back">
                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10 no_padding">
                    <ul class="list-inline">

                        <li><a href="{{route('cms::backend::backend.cms.menu_biblioteca.index')}}" class="icon_voltar"><i
                                        class="icon-voltar pading_right"></i>Voltar</a></li>
                    </ul>
                </div>

            </div>
        @include('layouts.backend.messages')
        {!! DaveJamesMiller\Breadcrumbs\Facade::render('cms.menu_biblioteca.create') !!}
        <div class="row">
            <form method="POST" action="{{route('cms::backend::backend.cms.menu_biblioteca.store') }}" autocomplete="off">
                {{ csrf_field() }}

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-titulo">
                            <span class="titulo">
                                <i class="icon-editar-utilizador icon20"></i>Menu Biblioteca
                            </span>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 margem_20">
                                    <div class="form-group @if($errors->has('name')) has-error @endif">
                                        <label for="name-field">Nome</label>
                                        <input type="text" name="name" id="name" class="form-control" value="{{old('name')}}">
                                        @if($errors->has("name"))
                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                        <button type="submit" name="submit" value="save" class="btn btn-default btn-yellow btn-submit-all">@lang('cms::backend.model.common.save')</button>
                        <button type="submit" name="submit" value="stay" class="btn btn-default btn-yellow btn-stay btn-submit-all">@lang('cms::backend.model.common.save_and_stay')</button>
                        <a class="btn btn-default btn-cancelar"
                           href="{{route('cms::backend::backend.cms.menu_biblioteca.index')}}">@lang('cms::backend.model.common.cancel')</a>
                    </div>
            </form>
        </div>
    </div>
</div>
@endsection
