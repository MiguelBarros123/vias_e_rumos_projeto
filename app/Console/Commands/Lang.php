<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Lang extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lang:join
                            {--module : execute langs join only for this module}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Join all langs of integrated modules into main project lang folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $path = base_path() . '/modules';

        $modules = glob($path, GLOB_ONLYDIR);

        foreach ($modules as $module) {
            $this->info("*** Loading lang files from module: $module ***");

            $locales = scandir("$path/$module/resources/lang");

            foreach ($locales as $locale) {

                if (is_dir($locale)) {

                    $this->info('=> Loading local: ' . $locale);
                    $langs = scandir($locale);

                    foreach ($langs as $lang) {

                        $result = copy("$locale/$lang/*", base_path() . "/resources/lang/$locale/");
                
                        if ($copy) {
                            $this->success('=> Copy succeded!');
                        } else {
                            $this->error('=> Copy failed!');
                        }
                    }
                }
            }
        }
    }
}
