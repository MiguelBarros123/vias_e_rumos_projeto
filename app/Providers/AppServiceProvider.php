<?php

namespace App\Providers;
use Illuminate\Support\Facades\Hash;
use Brainy\Cms\Models\MenuBiblioteca;
use Brainy\Cms\Models\RegistoLeiloeira;
use Illuminate\Support\ServiceProvider;
use Brainy\DefinitionsRent\Models\Category;
use Brainy\DefinitionsRent\Models\TypeGoods;
use Brainy\DefinitionsRent\Models\SubTypeGoods;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view){
            $type_goods = TypeGoods::where('list_principal',1)->get();
            $type_goods->map(function($type_good){
                return $type_good['type'] = 'Tipo de bens';
            });
            $sub_type_of_goods = SubTypeGoods::where('list_principal',1)->get();
            $sub_type_of_goods->map(function($sub_type_of_good){
                return $sub_type_of_good['type'] = 'Sub tipo de bens';
            });
            $sub_type_of_goods->each(function($sub_type_of_good) use($type_goods){
                $type_goods->push($sub_type_of_good);
            });

            // $collection = $sub_type_of_goods->merge($type_goods);
            $sub_type_of_goods = $type_goods->slice(0,6);
            $categories = Category::all();
            $registo_leiloeira = RegistoLeiloeira::first();

            $view->with('sub_type_of_goods', $sub_type_of_goods)->with('registo_leiloeira', $registo_leiloeira)->with('categories', $categories);
        });

        \Validator::extend('old_password', function ($attribute, $value, $parameters, $validator) {
            return Hash::check($value, current($parameters));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
