(function() {
	'use strict';

	function breadcrumb() {
		return {
			restrict: 'EA',
			template: '<ul class="breadcrumb"><li ng-repeat="path in breadcrumb" ng-bind="path"></li></ul>'
		};
	}

	angular.module('brainy').directive('breadcrumb', breadcrumb);
})();