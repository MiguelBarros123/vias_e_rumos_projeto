<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // $this->call(CampaignTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(GallerySeeder::class);
         $this->call(CmsTableSeeder::class);
        // $this->call(SalesSeeder::class);
         //$this->call(CatalogSeeder::class);
        // $this->call(FaqsSeeder::class);
         $this->call(RepresentativeTableSeeder::class);
         // $this->call(RentItemTableSeeder::class);
         // $this->call(TypeRentItemTableSeeder::class);

         //$this->call(DefinitionsEcommerceSeeder::class);
    }
}
