<?php

return [
    // The label is used to represent the module's name
    'label' => 'gallery::backend.module-label',

    // Permissions are organized by groups
    // The level 1 key denotes the group name (label)
    // The level 1 value is an associative array
    // The level 2 key denotes the permission name (label)
    // The level 2 value is an array with the routes that requires the permission
    'permissions' => [
        'gallery::backend.permissions.see'=>[
        'gallery::backend.permissions.see'=>[
            'gallery::backend::breadcrumb',
            'gallery::backend::folder.index',
            'gallery::backend::model.index',
            'gallery::backend::model.show',
            'gallery::backend::storage_assets',
            'gallery::backend::change_gallery_archive_mode',
            'gallery::backend::galery',
            'gallery::backend::change_gallery_mode',
            'gallery::backend::storage_image',
            'gallery::backend::galery_archive',
            'gallery::backend::archived_storage_assets',
            'gallery::backend::copy_media_asset',
            'gallery::backend::download_items_gallery',
            'gallery::backend::move_media_items',
            'gallery::backend::fetch_new_files'
        ],
    ],

        'gallery::backend.permissions.create' => [
            'gallery::backend.permissions.create' => [
                'gallery::backend::model.create',
                'gallery::backend::model.store',
                'gallery::backend::upload_asset',
                'gallery::backend::backend.gallery.media-folder.store',
                'gallery::backend::save_asset',
                'gallery::backend::save_folder_upload',
                'gallery::backend::upload_blueimp_asset',
                 'gallery::backend::model.edit',
                'gallery::backend::model.update',
                'gallery::backend::backend.gallery.media-item.edit',
                'gallery::backend::edit_image_asset',
                'gallery::backend::change_item_location',
                'gallery::backend::download_item_gallery',
                'gallery::backend::store_embed_link',
                'gallery::backend::backend.gallery.media-item.update',
                'gallery::backend::store_image_asset'
            ],
        ],
        'gallery::backend.permissions.delete' => [
            'gallery::backend.permissions.delete' => [
                'gallery::backend::model.destroy',
                'gallery::backend::delete_media_items',
                'gallery::backend::destroy_media_items',
                'gallery::backend::recover_media_items'
            ],
        ],
    ],
    // Menu items denote the menus that this module renders
    'menu-items' => [
        [
            // Label for the menu item (mandatory)
            'label' => 'gallery::backend.model.menu.list',
            //if the menu has no childs)
            'route' => 'gallery::backend::model.index',
            // Css class can be used to render the icon
            'css-class' => 'icon-icon-upload-img icon15menu',
            // Unique html id for the item
            'css-id' => null,
            // list of submenus
            'sub-menus' => [],
        ],

    ],
    // List of required providers for the module to work
    'providers' => [
        Brainy\Gallery\Providers\GalleryServiceProvider::class,
    ],

    // List of facades used by the module
    'aliases' => [
        // 'Recaptcha' => Greggilbert\Recaptcha\Facades\Recaptcha::class,
    ],

    // List of route middleware to register
    'middleware' => [
        //'acl' => \Brainy\Gallery\Middleware\SomeMiddleware::class,
    ],

    'commands' => [
        // \Brainy\Gallery\Commands\SomeCommand::class,
    ],

];
