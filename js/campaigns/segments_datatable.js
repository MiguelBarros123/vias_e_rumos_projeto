$( function() {

    $(document).ready(function () {

        var options = {
            rota: rota_profiles,
            metodo: 'POST',
            dados: function (d) {
                d.filter = $('#edit_segment_form').serialize();
            },
            colunas: [
                {data: 'name', name: 'name'},
                {data: 'job', name: 'job'},
                {data: 'contacts', name: 'contacts'},
                {data: 'type', name: 'type'}
            ],
            config: {
                initComplete: function (settings, json) {
                    $(function () {
                        $.logicDatatable(options);

                    });
                },
                fnCreatedRow: function (nRow, aData, iDataIndex) {
                    //for clicable rows
                    $(nRow).attr('data-rota-see', aData['rota_final']);
                    var tr2 = $(nRow).closest('tr');
                    tr2.find("td:first").addClass('details-control');
                    tr2.find("td").addClass('clickable_url');
                }

            },
            searchableElementId: 'search'
        };


        $('.default-table2').mydatatable(options);

        $('.default-table2').on('click', 'td.clickable_url', function () {
            var tr = $(this).closest('tr');
            window.open(tr.attr('data-rota-see'));
            //window.location.href = tr.attr('data-rota-see');

        });


        $('.btn-refresh-table').click(function () {
            //options.datatable.draw();
            options.datatable.ajax.url(rota_profiles_search).load();
        });

    });

});
//# sourceMappingURL=segments_datatable.js.map
