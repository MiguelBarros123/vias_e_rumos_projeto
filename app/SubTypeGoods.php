<?php

namespace Brainy\DefinitionsRent\Models;

use Illuminate\Database\Eloquent\Model;

use Brainy\Rent\Models\Lot;

class SubTypeGoods extends Model
{
    protected $guarded = [];

    public function lots()
    {
        return $this->morphedByMany(Lot::class, 'typegoodstable');
    }
}
