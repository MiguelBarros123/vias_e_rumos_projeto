@extends('layouts.backend.app')

@section('title', 'backend index')

@section('module-styles')
<link rel="stylesheet" type="text/css" href="{{asset('css/seo/backend.css')}}">
@endsection

@section('module-scripts')
<script src="{{asset('js/seo/backend.js')}}"></script>
@endsection

@section('content')
<p>@lang('seo::backend.model.index.welcome')</p>
@endsection
