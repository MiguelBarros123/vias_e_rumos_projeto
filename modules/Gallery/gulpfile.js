var elixir = require('laravel-elixir');

elixir(function(mix) {
     mix.styles([
                'backend-styles.css',
                'context_menu.css',
                'jquery-ui.min.css'
         ], '../../public/css/gallery/backend.css')
        .styles([
            'frontend-styles.css'
         ], '../../public/css/gallery/frontend.css')
         .scripts([
             'backend-app.js',
             'mediafinder.js',
             'context_menu.js',
             'jquery-ui.min.js',
             'local_storage.js',
             'selectslidemenu.js'
         ], '../../public/js/gallery/backend.js')
         .scripts([
             'frontend-app.js'
         ], '../../public/js/gallery/frontend.js');

    mix.copy('resources/assets/js/fileupload', '../../public/js/gallery/fileupload');
    mix.copy('resources/assets/js/tagsinput.js', '../../public/js/gallery/tagsinput.js');

  //  mix.styles([
  //           'backend-styles.css'
  //       ], 'public/css/backend.css')
  //       .styles([
  //           'frontend-styles.css'
  //       ], 'public/css/frontend.css')
  //       .scripts([
  //           'backend-app.js'
  //       ], 'public/js/backend.js')
  //       .scripts([
  //           'frontend-app.js'
  //       ], 'public/js/frontend.js');
});
