@extends('layouts.app')

@section('content')
<div class="container reset-form">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <div class="panel-titulo">
                    <span class="titulo">
                        <i class="icon-recuperar-password icon20"></i>
                        Alterar Password
                    </span>
                </div>

                <div class="panel-body login-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }} login-input">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="email" class="col-md-12"><i class="icon-email icon10 icon-padding"></i> Email</label>
                                <div class="login-error">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $email or old('email') }}" placeholder="ex. brainy@tsf.pt">
                                    @if($errors->has('email'))
                                        <i class="icon-opcoes-eliminar"></i>
                                    @endif
                                </div>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} login-input">
                            <div class="col-md-8 col-md-offset-2">
                                <label for="password" class="col-md-12"><i class="icon-bloquear-1 icon10 icon-padding"></i> Password</label>
                                <div class="login-error">
                                    <input id="password" type="password" class="form-control" name="password" placeholder="ex. omeubackoffice">
                                    @if($errors->has('password'))
                                        <i class="icon-opcoes-eliminar"></i>
                                    @endif
                                </div>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} login-input">
                            
                            <div class="col-md-8 col-md-offset-2">
                                <label for="password-confirm" class="col-md-12"><i class="icon-recuperar-password icon10 icon-padding"></i> Repetir Password</label>
                                <div class="login-error">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="ex. omeubackoffice">
                                    @if($errors->has('password_confirmation'))
                                        <i class="icon-opcoes-eliminar"></i>
                                    @endif
                                </div>

                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="login-btn">
                                    <button type="submit" class="btn btn-default btn-yellow">
                                        Alterar Password
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
