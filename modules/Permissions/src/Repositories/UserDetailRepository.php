<?php

namespace Brainy\Permissions\Repositories;

use App\User;
use Brainy\Framework\Models\Group;
use Brainy\Framework\Models\UserDetail;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Password;


class UserDetailRepository implements UserDetailRepositoryInterface
{

    private $user;

    public function _construct(UserDetail $user)
    {
        $this->$user = $user;
    }

    public function storeUser($user_request)
    {

        $dados_user = [
            'name' => $user_request['name'],
            'email' => $user_request['email'],
        ];

        $grupo=Group::findOrFail($user_request['group_id']);

        $file = 'profile.jpg';

        if(isset($user_request['profile_image'])){
            $file = $user_request['profile_image'];
        }

        $user = User::create($dados_user);
        $user_detail= new UserDetail([
            'photo' => $file,
            'surname' => $user_request['surname'],
            'group_id' => $user_request['group_id'],
            'job_position' => $user_request['job_position'],
        ]);
        $user_detail->is_active = true;
        $user_detail->is_super_admin = false;
        $user_detail->group()->associate($grupo);
        $user->details()->save($user_detail);




        Mail::send('permissions::backend.model.emails.registrationConfirm', ['user' => $user], function($message) use($user)
        {
            $message->from('brainy@thesilverfactory.pt', 'Brainy');
            $message->to($user->email)->subject(trans('permissions::backend.'));
        });


       $userName = $user->name." ".$user->details->surname;

        return $userName;
    }


    /**
     * @param  int -> User id
     * @return bool -> new User state (@see UserDetail@getIsActiveAttribute)
     *
     * returns the new state for json response
     */
    public function changeUserState($id)
    {
        $user = UserDetail::findOrFail($id);
        $user->setIsActiveAttribute(! $user->getIsActiveAttribute());
        $user->update();
        return $user->getIsActiveAttribute();
    }

    public function destroyUsers($users)
    {

        $sucessMessage=collect();
        $messages['error']="";
        $messages['success']="";

        foreach ($users as $user) {
            $usr = User::findOrFail($user);
            $name=$usr->name;
            User::destroy($usr->id);
            $sucessMessage->push($name);
        }


        if($sucessMessage->count()>0){
            $messages['success']=trans('permissions::backend.user.index.delete_user_sucess', ['user'=>implode(',',$sucessMessage->toArray())]);
        }


        return $messages;
    }


    public function recoverPassword($user_email)
    {

        $user_email = array('email'=>$user_email);

        $response = Password::sendResetLink($user_email, function (Message $message) {
            $message->from('geral@sarafauto.pt', 'Sarafauto');
            $message->subject(trans('permissions::backend.user.messages.success.recovey_subject'));
        });

        return $response;

    }

}










