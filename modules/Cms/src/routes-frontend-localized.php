<?php

Route::group(['namespace' => 'Brainy\Cms\Controllers\Frontend'], function () {

//    app()->setLocale('pt');

    Route::get('news', 'ModelFrontendController@news')->name('model.news');
    Route::get('news/{id}', 'ModelFrontendController@news_detail')->name('model.news_detail');

    $pageRouteNames = [
        'cms::frontend::model.home',
        'cms::frontend::model.contacts',
        'cms::frontend::model.general-conditions',
    ];
    // TODO: melhorar e usar o ultimo componente da rota como nome
    if(Schema::hasTable('pages')) {
        $pages = \Brainy\Cms\Models\PageTranslation::where('locale', app()->getLocale())
            ->whereIn('route_name', $pageRouteNames)
            ->get();

        foreach ($pages as $page){

            $name = substr($page->route_name, strrpos($page->route_name, '.') + 1);

            Route::get('/'.$page->route_url, 'ModelFrontendController@'.str_replace('-', '_', $name ))->name('model.'.$name);
        }

    }
    Route::get('/'.\Brainy\Cms\Models\Page::convertUrl('cms::frontend::index',app()->getLocale()), 'ModelFrontendController@homePage')->name('index');
    Route::get('search-results', 'ModelFrontendController@searchResults')->name('model.search-results');
    Route::get('message-reservation/{reservation}','ModelFrontendController@messageReservation')->name('model.message-reservations');

    Route::get('login', 'ModelFrontendController@login')->name('model.login');
    Route::get('forgot-password', 'ModelFrontendController@forgotPassword')->name('model.forgot-password');
    Route::get('reserved-area', 'ModelFrontendController@reservedArea')->name('model.reserved-area');
    Route::get('reservation-details/{id}', 'ModelFrontendController@reservationDetails')->name('model.reservation-details');
    Route::get('vendas/{id?}', 'ModelFrontendController@vendas')->name('model.vendas');
    Route::get('avancada', 'ModelFrontendController@avancada')->name('model.avancada');
    Route::get('mapa', 'ModelFrontendController@mapa')->name('model.mapa');
    Route::get('leilao/{id}', 'ModelFrontendController@leilao')->name('model.leilao');
    Route::get('contacts', 'ModelFrontendController@contacts')->name('model.contacts');
    Route::get('sub-tipos', 'ModelFrontendController@subtipos')->name('model.subtipos');
    Route::get('detalhe_lote/{id}','ModelFrontendController@lotes_detalhes')->name('model.lotes_detalhes');
    Route::get('empresa', 'ModelFrontendController@empresa')->name('model.empresa');
    Route::get('biblioteca', 'ModelFrontendController@biblioteca')->name('model.biblioteca');
    Route::get('biblioteca-menu/{name1}/{name2}', 'ModelFrontendController@insolvencia')->name('model.insolvencia');
    Route::get('cookies', 'ModelFrontendController@cookies')->name('model.cookies');
    Route::get('litigio', 'ModelFrontendController@litigio')->name('model.litigio');
    Route::get('privacidade', 'ModelFrontendController@privacidade')->name('model.privacidade');
    Route::get('filtrar/{id}/{type}', 'ModelFrontendController@filter_by_sub_types')->name('model.filter_by_sub_types');
    Route::get('/get-storage-pdf-file/{id}', array('as'=>'storage_pdf','uses'=>'ModelFrontendController@getStoragePdf'));
    Route::get('pesquisa-avancada', 'ModelFrontendController@pesquisaAvancada')->name('model.advanced-search');
    Route::get('pesquisa/{dia}/{mes}/{ano}','ModelFrontendController@pesquisaDatas')->name('model.data-search');
    Route::post('send-email','ModelFrontendController@sendEmail')->name('model.send_email');
    Route::get('registar','ModelFrontendController@registar')->name('model.registar');
    Route::get('registar/verificar_conta/{confirmationCode}','ModelFrontendController@confirmarConta')->name('model.confirmar_conta');
    Route::get('entrar','ModelFrontendController@entrar')->name('model.entrar');
    Route::get('area-reservada','ModelFrontendController@areaReservada')->name('model.area-reservada');
    Route::post('registo-area-reservada','ModelFrontendController@createUserAreaReservada')->name('model.create_user');
    Route::post('login','ModelFrontendController@login')->name('model.login');
    Route::put('alterar-dados-factuaracao/{id}','ModelFrontendController@changeInvoiceInformation')->name('model.invoice_personal_information');
    Route::put('alterar-dados-pessoais/{id}','ModelFrontendController@changePersonalInformation')->name('model.change_personal_information');
    Route::get('lote',function(){
        return view('lote_teste');
    });
    Route::get('lote_teste/{id}','ModelFrontendController@loteTeste')->name('model.lote_teste');
    //Route::get('image-water/{img}/{ref}','ModelFrontendController@imageWater')->name('model.image-water');
//    Route::get('subscribe', 'ModelFrontendController@subscribe')->name('model.subscribe');
//    Route::get('unsubscribe', 'ModelFrontendController@unsubscribe')->name('model.unsubscribe');
//    Route::put('subscribe-user', 'ModelFrontendController@subscribeUser')->name('subscribe.subscribe');
//    Route::put('unsubscribe-user', 'ModelFrontendController@unsubscribeUser')->name('subscribe.unsubscribe');
    Route::get('errors-frontend', function(){
        return view('errors.custom-front');
    });

    Route::post('broadcasting-event', 'ModelFrontendController@broadcastEvent')->name('model.broadcasting-event');
    Route::post('check_if_valid','ModelFrontendController@checkIfPriceValid')->name('model.check_if_valid');
    Route::get('recover_password','ModelFrontendController@recoveryPassword')->name('model.recovery_password');
    Route::post('changePassword','ModelFrontendController@changePassword')->name('model.change_password');
});
