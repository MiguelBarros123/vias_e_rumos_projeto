(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

/**
 * Created by User on 15-02-2017.
 */
(function ($) {

    $.fn.showBrainyNotification = function () {
        var sel_elem_brainy = this;

        sel_elem_brainy.show().removeClass('success_saving_email').addClass('fadeInDown');
        setTimeout(function () {
            sel_elem_brainy.removeClass('fadeInDown').addClass('fadeOutUp');
            setTimeout(function () {
                sel_elem_brainy.remove();
            }, 400);
        }, 3000);

        return this;
    };

    $(document).ready(function () {
        setTimeout(function () {
            $('.initial_alert').removeClass('fadeInDown').addClass('fadeOutUp');
            setTimeout(function () {
                $('.initial_alert').remove();
            }, 400);
        }, 3000);
    });
})(jQuery);

},{}]},{},[1]);

//# sourceMappingURL=notifications.js.map
