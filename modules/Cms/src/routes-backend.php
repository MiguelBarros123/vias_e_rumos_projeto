<?php

Route::group(['namespace' => 'Brainy\Cms\Controllers\Backend', 'middleware' => \App\Http\Middleware\BackofficeLanguage::class], function () {
    Route::get('edit-page/{id?}', 'PagesBackendController@index_edit_page')->name('pages.edit_page');
    Route::put('save-page', 'PagesBackendController@save_page')->name('pages.save_page');
    Route::get('get-ajax/{id}', 'PagesBackendController@get_ajax')->name('pages.ajax_table');
    Route::get('get-section-ajax/{id}', 'PagesBackendController@get_section_ajax')->name('pages.ajax_section_table');
    Route::post('/change-composed-visibility/{id}', 'PagesBackendController@changeComposedVisibility')->name('model.change_composed_visibility');
    Route::get('create-section/{id}', 'PagesBackendController@create_composed_highlight')->name('pages.create');
    Route::get('edit-section/{id}', 'PagesBackendController@edit_composed_highlight')->name('pages.edit');
    Route::delete('delete-multiple-composeds', 'PagesBackendController@deleteMultipleComposeds')->name('pages.delete_multiple_composeds');

    Route::put('store-section/{id}', 'PagesBackendController@storeCompoded')->name('composed.store');

    Route::delete('resource/{id}', 'PagesBackendController@deleteResource')->name('resource.destroy');
    Route::get('resource/{id}', 'PagesBackendController@create_resource')->name('resource.create');
    Route::get('reorder-resource-elements/{id}', 'PagesBackendController@reorderResourceElements')->name('resource.reorder');
    Route::put('store-resource/{id}', 'PagesBackendController@storeResource')->name('resource.store');
    Route::get('products-variant/{id}', 'PagesBackendController@getProductsVariant')->name('products.variant');
    Route::get('variants-resource/{prod_id}/{var_id}', 'PagesBackendController@getVariantsResource')->name('variants.resource');
    Route::delete('delete-multiple-resources', 'PagesBackendController@deleteMultipleResources')->name('pages.delete_multiple_resources');

    Route::get('representatives', 'PagesBackendController@getRepresentatives')->name('representatives');
    Route::put('representatives/reorder', 'PagesBackendController@reorderRepresentative')->name('representatives.reorder');
    Route::put('representatives', 'PagesBackendController@storeRepresentative')->name('representative.store');
    Route::put('representatives/{id}', 'PagesBackendController@updateRepresentative')->name('representative.update');
    Route::delete('representatives/{id}', 'PagesBackendController@deleteRepresentative')->name('representative.destroy');
    Route::delete('delete-multiple-representatives', 'PagesBackendController@deleteMultipleRepresentatives')->name('pages.contacts_delete_multiple');
    Route::put('edit-representative/{id}', 'PagesBackendController@edit_representative')->name('pages.edit-representative');
    Route::put('save-representative/{id}', 'PagesBackendController@save_representative')->name('pages.save_representative');

    Route::put('save-section/{id}', 'PagesBackendController@save_highlight')->name('pages.save_highlight');
    Route::put('save-contacts', 'PagesBackendController@update_contacts')->name('contacts.update');
    Route::put('upload-image', 'PagesBackendController@upload_image')->name('upload.image');
    Route::post('/save-storage-asset/', array('as'=>'save_asset','uses'=>'PagesBackendController@saveAsset'));

    Route::get('reorder-composed-elements/{id}', 'PagesBackendController@reorderComposedElements')->name('composed.reorder');

    Route::get('members/ajax-items', 'MemberBackendController@index_ajax')->name('cms.members.index_ajax');
    Route::delete('members/delete', 'MemberBackendController@delete')->name('backend.cms.members.delete');
    Route::resource("members","MemberBackendController");

    Route::get('menu_institucional','MenuInstitucionalBackendController@index')->name('backend.cms.menu_institucional.index');
    Route::post('menu_institucional','MenuInstitucionalBackendController@store')->name('backend.cms.menu_institucional.store');
    Route::put('menu_institucional/1','MenuInstitucionalBackendController@update')->name('backend.cms.menu_institucional.update');

    Route::get('menus_biblioteca/ajax-items','MenuBibliotecaBackendController@index_ajax')->name('backend.cms.menu_biblioteca.index_ajax');
    Route::delete('menus_biblioteca/delete','MenuBibliotecaBackendController@delete')->name('backend.cms.menu_biblioteca.delete');
    Route::get('menus_biblioteca/reorder','MenuBibliotecaBackendController@reorder')->name('backend.cms.menu_biblioteca.reorder');
    Route::resource('menu_biblioteca','MenuBibliotecaBackendController');

    Route::get('sub_menus_biblioteca/ajax-items','SubMenuBibliotecaController@index_ajax')->name('backend.cms.sub_menu_biblioteca.index_ajax');
    Route::delete('sub_menus_biblioteca/delete','SubMenuBibliotecaController@delete')->name('backend.cms.sub_menu_biblioteca.delete');
    Route::get('sub_menus_biblioteca/reorder','SubMenuBibliotecaController@reorder')->name('backend.cms.sub_menu_biblioteca.reorder');
    Route::resource('sub_menus_biblioteca','SubMenuBibliotecaController');

    Route::get('registo_leiloeira','RegistoLeiloeiraBackendController@index')->name('backend.cms.registo_leiloeira.index');
    Route::post('registo_leiloeira', 'RegistoLeiloeiraBackendController@store')->name('backend.cms.registo_leiloeira.store');
    Route::put('registo_leiloeira/{registo_leiloeira}', 'RegistoLeiloeiraBackendController@update')->name('backend.cms.registo_leiloeira.update');

    Route::get('errors', function(){
        return view('errors.custom');
    });
});
