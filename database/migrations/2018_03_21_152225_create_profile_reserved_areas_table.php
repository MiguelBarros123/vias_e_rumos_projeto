<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileReservedAreasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_reserved_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('email')->unique();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('nif');
            $table->string('cartao_cidadao');
            $table->datetime('data_validade');
            $table->string('telefone');
            $table->string('nome_fiscal');
            $table->string('nipc');
            $table->string('country');
            $table->string('address');
            $table->string('cod_postal');
            $table->string('local');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_reserved_areas');
    }
}
