/**
 * Created by User on 13-05-2016.
 */
(function($) {
    "use strict";
    var SelectSlideMenu = function(element, options) {
        this.$el = $(element);
        this.options = options || {};
        this.init();
        var menu = this;
        $('.back_destiny').click(function() {
            if ($(this).attr('data-id') !== 0) {
                menu.open_folder($(this).attr('data-id'));
            }

        });

        $('.btn_save_folder').click(function() {
            
            if (options.gallery === 0) {
                menu.setFinalFolder($(this).attr('data-id'));
                $('.listagem_pastas').hide();
            }



        });

        $('.new_folder_here').click(function() {
            $('#pasta_atual_pai').val($('.pasta_atual').attr('data-id'));

            $('.nova_pasta_escond').show();
            $('.input_pasta').focus().select();
            $('.new_folder_selected').show();
        });

        $('.btn_cancel_new_folder').click(function() {
            $('.new_folder_selected').hide();
            $('.nova_pasta_escond').hide();

        });

        $('.btn_save_new_folder').click(function() {


            if ($.trim($('.input_pasta').val())) {

                $.ajax({
                    type: "post",
                    data: { parent_folder_id: $('#pasta_atual_pai').val(), title: $('.input_pasta').val() },
                    url: $(this).attr('data-href'),
                    headers: {
                        'X-CSRF-TOKEN': menu.options.token
                    },
                    success: function(data) {
                        $('.new_folder_selected').hide();
                        $('.nova_pasta_escond').hide();

                        menu.options.dados = data.conteudo;
                        menu.open_folder(data.currnt_f);

                        /*$('.listagem_pastas').selectslidemenu({ dados: data.conteudo,inicial:data.currnt_f });*/
                        /* menu.open_folder(data.currnt_f);*/



                    }
                });


            } else {
                console.log('nao submete');
            }


        });


    };



    SelectSlideMenu.prototype.init = function() {

        var menu = this;
        var inicial = this.options.inicial;

        menu.open_folder(inicial);
        this.setFinalFolder(inicial);


    };


    SelectSlideMenu.prototype.open_folder = function(inicial) {
        var menu = this;
        var dados = jQuery.parseJSON(this.options.dados);
        var galery = this.options.gallery;
        if (galery == inicial) {
            //hide the save button
            $('.innactive_save').show();
        } else {
            $('.innactive_save').hide();
        }

        var folders = $.grep(dados, function(element, index) {
            return element.parent_folder_id == inicial;
        });


        this.setCurrentFolder(inicial);
        this.setSubfolders(folders);
        this.setPreviousFolder(inicial);


    };


    SelectSlideMenu.prototype.setFinalFolder = function(inicial) {


        var dados = jQuery.parseJSON(this.options.dados);
        var pasta = $.grep(dados, function(element, index) {
            return element.id == inicial;
        });

        $('.btn_save_folder').attr('data-id', pasta[0].id);
        $('#dest_f').text(pasta[0].title);
        $('#dest_f_inp').val(pasta[0].id);
        $('*[id^="dest_f_"]').text(pasta[0].title);
        $('*[id^="dest_f_inp_"]').val(pasta[0].id);
    };


    SelectSlideMenu.prototype.setSubfolders = function(folders) {

        var menu = this;
        $('.destinys').empty();

        var blocked = this.options.blocked_folders;


        console.log(blocked);

        if (folders.length === 0) {
            $('.destinys').append('' +
                '<li class="empty-folder">' +
                'A pasta está vazia.' +
                '</li>');
        } else {
            $.each(folders, function(index, value) {
                if ($.inArray("" + value.id + "", blocked) != -1) {
                    $('.destinys').append('' +
                        '<li class="selected_item innactive_option">' +
                        '<div class="innactive_option"></div>' +
                        '<i class="icon-icon-peq-pasta icon_titulo_pasta"></i>' + value.title +
                        '<a class="go_to_folder" data-id="' + value.id + '"><i class="icon-seta-direita"></i></a>' +
                        '</li>');

                } else {
                    $('.destinys').append('' +
                        '<li id="' + 'li-select-' + index + '" class="selected_item">' +
                        '<div></div>' +
                        '<i class="icon-icon-peq-pasta icon_titulo_pasta"></i>' + value.title +
                        '<a class="go_to_folder" data-id="' + value.id + '"><i class="icon-seta-direita"></i></a>' +
                        '</li>');
                }
            });

            $("li[id^='li-select-']").click(function(event) {
                var $element = $(event.target);

                var isRootFolder = $('.back_destiny').attr('data-id'),
                    currentFolder = $element.first().hasClass('innactive_option');

                var $btnSaveFolder = $('.btn_save_folder');

                if (isRootFolder  == '0' || currentFolder) {
                    return;
                } else {
                    $element.toggleClass('selectedfilter');

                    if ($element.hasClass('selectedfilter')) {
                        $btnSaveFolder.text('GUARDAR')
                                      .attr({ 'data-id': $($element).children().last().attr('data-id') });
                    } else {
                        $btnSaveFolder.text('GUARDAR AQUI')
                                      .attr({ 'data-id': $('.pasta_atual').attr('data-id') });
                    }
                }
            });
        }

        $('.go_to_folder').click(function() {
            menu.open_folder($(this).attr('data-id'));
        });




    };
    SelectSlideMenu.prototype.setPreviousFolder = function(inicial) {



        var dados = jQuery.parseJSON(this.options.dados);
        var pasta = $.grep(dados, function(element, index) {
            return element.id == inicial;
        });

        console.log(pasta[0].parent_folder_id);

        if (pasta[0].parent_folder_id !== 0) {
            $('.back_destiny').removeClass('opacidade_back');

            $('.back_destiny').attr('data-id', pasta[0].parent_folder_id);
        } else {
            $('.back_destiny').attr('data-id', 0);
            $('.back_destiny').addClass('opacidade_back');
        }



    };


    SelectSlideMenu.prototype.setCurrentFolder = function(current) {
        var dados = jQuery.parseJSON(this.options.dados);
        var pasta = $.grep(dados, function(element, index) {
            return element.id == current;
        });

        $('.pasta_atual').html(pasta[0].title);
        $('.pasta_atual').attr('data-id', pasta[0].id);

        if (pasta[0].parent_folder_id === 0) {
            $('.innactive_btns').show();
        } else {
            $('.innactive_btns').hide();
            $('.btn_save_folder').text('GUARDAR AQUI').attr('data-id', pasta[0].id);
        }

    };













    // -------------------PLUGIN DEFINITION-----------------------------------
    SelectSlideMenu.DEFAULTS = {
        dados: null,
        inicial: null,
        token: null,
        gallery: 0,
        blocked_folders: 0

    };

    var old = $.fn.selectslidemenu;

    $.fn.selectslidemenu = function(option) {
        var args = arguments;

        return this.each(function() {
            var $this = $(this);
            var data = $this.data('oc.selectslidemenu');

            var options = $.extend({}, SelectSlideMenu.DEFAULTS, $this.data(), typeof option == 'object' && option);
            if (!data) {

            }

            $this.data('oc.selectslidemenu', (data = new SelectSlideMenu(this, options)));

            /*var options =  (!option)? SelectSlideMenu.DEFAULTS: option*/

            /* new SelectSlideMenu(this, options)*/
        });
    };

    $.fn.selectslidemenu.Constructor = SelectSlideMenu;

    $.fn.selectslidemenu.noConflict = function() {
        $.fn.selectslidemenu = old;
        return this;
    };




}(window.jQuery));

//# sourceMappingURL=backend.js.map
