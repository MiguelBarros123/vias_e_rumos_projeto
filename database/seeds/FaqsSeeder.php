<?php

use Illuminate\Database\Seeder;
use Brainy\Framework\Models\Tag;
use Brainy\Faqs\Models\Faq;
use Brainy\Faqs\Models\FaqTranslation;
use Brainy\Faqs\Models\Theme;

class FaqsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(TagsTableSeeder::class);
        //$this->call(ThemesTableSeeder::class);
        //$this->call(FaqsTableSeeder::class);
    }
}