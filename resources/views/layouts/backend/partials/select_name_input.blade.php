<select {{ isset($id) ? 'id="'.$id.'"': ''}} class="form-control {{(isset($class)? $class: '')}}" name="{{$name}}" >
    @foreach($array as $key => $a)
        <option value="{{ $a }}" {{ ($a == $selected)? 'selected': ''}}>{{$a}}</option>
    @endforeach
</select>