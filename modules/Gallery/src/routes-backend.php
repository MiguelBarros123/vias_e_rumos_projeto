<?php

Route::group(['namespace' => 'Brainy\Gallery\Controllers\Backend', 'middleware' => \App\Http\Middleware\BackofficeLanguage::class], function () {

    Route::get('model', 'ModelBackendController@index')->name('model.index');
    Route::get('model/create', 'ModelBackendController@create')->name('model.create');
    Route::post('model', 'ModelBackendController@store')->name('model.store');
    Route::get('model/{model}', 'ModelBackendController@show')->name('model.show');
    Route::get('model/{model}/edit', 'ModelBackendController@edit')->name('model.edit');
    Route::put('model/{model}', 'ModelBackendController@update')->name('model.update');
    Route::delete('model/{model}', 'ModelBackendController@delete')->name('model.destroy');

    Route::get('folder/{pathId}', 'ModelBackendController@scanFolderContents')->name('folder.index');
    Route::get('breadcrumb/{pathId}', 'ModelBackendController@getBreadcrumb')->name('breadcrumb');

    Route::get('/change-gallery-mode/{id}', array('as'=>'change_gallery_mode','uses'=>'ModelBackendController@changeGalleryMode'));
    Route::get('/upload-asset/{id}', array('as'=>'upload_asset','uses'=>'ModelBackendController@showUploadAsset'));
    Route::get('/change-gallery-assets-mode/{id}', array('as'=>'change_gallery_archive_mode','uses'=>'ModelBackendController@changeGalleryAssetMode'));
    Route::post('/change-item-location', array('as'=>'change_item_location','uses'=>'ModelBackendController@changeItemsLocation'));
    Route::get('/galeria/{id}/{nome}', array('as'=>'galery','uses'=>'ModelBackendController@getGalery'));
    Route::get('/get-storage-assets/{id}', array('as'=>'storage_assets','uses'=>'ModelBackendController@getAssets'));
    Route::get('/get-storage-image/{id}/{with?}/{height?}', array('as'=>'storage_image','uses'=>'ModelBackendController@getImageThumbnail'));
    Route::post('/save-storage-asset/', array('as'=>'save_asset','uses'=>'MediaItemController@saveAsset'));
    Route::post('/upload-storage-asset/', array('as'=>'upload_blueimp_asset','uses'=>'MediaItemController@uploadAssetHandler'));
    Route::post('/save-folder-upload/', array('as'=>'save_folder_upload','uses'=>'MediaFolderController@storeFolderOnUpload'));

    Route::resource('media-folder', 'MediaFolderController');
    Route::resource('media-item', 'MediaItemController');
    Route::post('/delete-media-asset/', array('as'=>'delete_media_items','uses'=>'ModelBackendController@deleteItems'));
    Route::post('/recover-media-asset/', array('as'=>'recover_media_items','uses'=>'ModelBackendController@recoverItems'));

    Route::post('/move-media-asset/', array('as'=>'move_media_items','uses'=>'ModelBackendController@moveItems'));

    Route::post('/destroy-media-asset/', array('as'=>'destroy_media_items','uses'=>'ModelBackendController@destroyItems'));
    Route::post('/copy-media-asset/{id}', array('as'=>'copy_media_asset','uses'=>'ModelBackendController@copyMediaAsset'));

    Route::get('/edit-image-asset/{id}', array('as'=>'edit_image_asset','uses'=>'MediaItemController@editImage'));
    Route::post('/store-image-asset/{id}', array('as'=>'store_image_asset','uses'=>'MediaItemController@storeImage'));
    Route::post('/store-embed-link', array('as'=>'store_embed_link','uses'=>'MediaItemController@storeEmbedLink'));

    Route::get('/gallery-archive-assets/', array('as'=>'galery_archive','uses'=>'ModelBackendController@showArchive'));
    Route::get('/gallery-archive-assets-ajax/', array('as'=>'archived_storage_assets','uses'=>'ModelBackendController@getArchivedAssets'));

    Route::post('/download-items-gallery/', array('as'=>'download_items_gallery','uses'=>'ModelBackendController@downloadItems'));

    Route::get('/download-item-gallery/{way}', array('as'=>'download_item_gallery','uses'=>'MediaItemController@downloadItem'));

    Route::get('/fetch-new-files/', array('as'=>'fetch_new_files','uses'=>'MediaItemController@fetchNewFiles'));

});
