<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 03-01-2017
 * Time: 09:46
 */



DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions', function($breadcrumbs)
{
    $breadcrumbs->parent('admin');
    $breadcrumbs->push('Gestão de permissões', route('permissions::backend::model.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.groups', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.permissions');
    $breadcrumbs->push('Grupos', route('permissions::backend::group.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.users', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.permissions');
    $breadcrumbs->push('Utilizadores', route('permissions::backend::model.index'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.users.archive', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.permissions');
    $breadcrumbs->push('Arquivo de utilizadores', route('permissions::backend::archived_users_index'));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.groups.create', function($breadcrumbs)
{
    $breadcrumbs->parent('admin.permissions.groups');
    $breadcrumbs->push('Novo grupo', route('permissions::backend::model.create'));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.groups.show', function($breadcrumbs, $group)
{
    $breadcrumbs->parent('admin.permissions.groups');
    $breadcrumbs->push($group->name, route('permissions::backend::group.show', $group->id));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.groups.edit', function($breadcrumbs,$group)
{
    $breadcrumbs->parent('admin.permissions.groups.show',$group);
    $breadcrumbs->push('Editar grupo', route('permissions::backend::group.edit',$group->id));
});

DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.group_change_users_group_view', function($breadcrumbs,$group)
{
    $breadcrumbs->parent('admin.permissions.groups.show',$group);
    $breadcrumbs->push('Mudar de grupo', route('permissions::backend::group_change_users_group_view',$group->id));
});




DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.users.show', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('admin.permissions.users');
    $breadcrumbs->push($user->user->name, route('permissions::backend::model.show', $user->id));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.users.edit', function($breadcrumbs,$user)
{
    $breadcrumbs->parent('admin.permissions.users.show',$user);
    $breadcrumbs->push('Editar utilizador', route('permissions::backend::model.edit',$user->id));
});


DaveJamesMiller\Breadcrumbs\Facade::register('admin.permissions.users.archived.show', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('admin.permissions.users.archive');
    $breadcrumbs->push($user->user->name, route('permissions::backend::model.show', $user->id));
});