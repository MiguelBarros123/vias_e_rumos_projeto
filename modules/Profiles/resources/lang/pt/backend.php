<?php

return [
    'module-label'=>'Perfis',
    'menu' => [
            'profiles' => 'Perfis',
            'clients' => 'Pessoas',
            'companys' => 'Empresas',
            'create_new_person' => 'Criar Nova Pessoa',
            'create_new_company' => 'Criar Nova Empresa',
            'groups' => 'Grupos',
            'create_new_group' => 'Novo grupo',
            'personalized_fields' => 'Campos personalizados',
            'create_new_personalized_fields' => 'Criar Campo personalizado',

    ],
    'common'=>[
        'delete' => 'Eliminar',
        'cancel' => 'Cancelar',
        'see' => 'Ver',
        'options' => 'Opções',
        'text_field' => 'Campo de texto',
        'multiple' => 'Escolha multipla',
        'select_personalized_field' => 'Selecione um campo personalizado',
        'select_personalized_fields' => 'Selecionar campos personalizados',
        'value' => 'Valor',
        'empty_personalized' => 'Sem campos personalizados',
        'text' => 'Texto',
        'fields' => 'Campos',
        'field' => 'Nome do campo',
        'fields_message' => 'Insira o nome dos campos separados por vírgula.',
        'search' => 'Procurar',
        'contacts' => 'Contactos',
        'created_at' => 'Criado em',
        'added_via' => 'Adicionado via',
        'work' => 'Cargo',
        'type' => 'Tipo',
        'view' => 'Ver',
        'contact_sheet' => 'FICHA DE CONTACTO',
        'group_sheet' => 'FICHA DE GRUPO',
        'emails' => 'Emails',
        'phones' => 'Telefone',
        'gender' => 'Género',
        'birth_date' => 'Data de nascimento',
        'address' => 'Morada',
        'language' => 'Idioma',
        'social_networks' => 'Redes Sociais',
        'connections' => 'Conexões',
        'representatives' => 'Representantes',
        'timeline' => 'Linha temporal',
        'create_person' => 'criar pessoa',
        'create_profile' => 'criar perfil',
        'name' => 'Nome',
        'profile_image' => 'Imagem do perfil',
        'is_user' => 'É utilizador do website?',
        'subscript' => 'Subscrever Perfil?',
        'personal_info' => 'Informação profissional',
        'faxes' => 'Faxes',
        'add_image' => ' Adicionar imagem ao perfil',
        'company' => 'Empresa',
        'new_emai' => 'Novo email',
        'new_personal_emai' => 'Novo email pessoal',
        'personal_emails' => 'Email pessoal',
        'company_emails' => 'Email profissional',
        'new_company_emails' => 'Novo email profissional',
        'personal_phone' => 'Pessoal',
        'new_personal_phone' => 'Novo telefone pessoal',
        'new_personal_mobile' => 'Novo telemóvel pessoal',
        'company_phone' => 'Profissional',
        'new_company_phone' => 'Novo telefone profissional',
        'new_company_mobile' => 'Novo telemóvel profissional',
        'personal_fax' => 'Fax pessoal',
        'new_personal_fax' => 'Novo fax pessoal',
        'company_fax' => 'Fax profissional',
        'new_company_fax' => 'Novo fax profissional',
        'new_emails' => 'Novo email',
        'new_phone' => 'Novo telefone',
        'new_mobile' => 'Novo telemóvel',
        'new_fax' => 'Novo fax',
        'email' => 'Email',
        'password' => 'Password',
        'password_again' => 'Repetir Password',
        'addresses' => 'Moradas',
        'group_reserved_area' => 'Grupo para area reservada',
        'group_front' => 'Grupo para acesso ao FrontOffice',
        'group_admin' => 'Grupo para acesso ao BackOffice',
        'no_groups' => 'Não foram encontrados grupos',
        'edit_person' => 'Editar Pessoa',
        'edit_profile' => 'Editar Perfil',
        'info' => 'Informaçoes',
        'sector' => 'Setor',
        'edit' => 'Editar',
        'surname' => 'Apelido',
        'mobile_phones' => 'Telemóvel',
        'other' => 'Outros',
        'functionaries' => 'Funcionários',
        'add_profiles' => 'Perfis',
        'companys_reserved_area' => 'Empresas',
        'companys_reserved_area_not_found' => 'Não foram encontradas empresas',
        'total_users' => 'total de perfis',
        'create_group' => 'Criar grupo',
        'profiles' => 'Perfis',
        'edit_group' => 'Editar Grupo',
        'sure_delete_profiles' => 'Tem a certeza que pretende eliminar os perfis selecionados?',
        'nif' => 'NIF',
        'url' => 'Url',
        'created_on' => 'Criado em',
        'select_company' => 'Selecione uma Empresa',
        'group' => 'Grupo',
        'select_group' => 'Selecione um grupo',
        'employees' => 'Funcionários',
        'add_new_profile' => 'Adicionar um perfil ainda não registado',
        'save' => 'Guardar',
        'save_and_stay' => 'Guardar e continuar',
        'profile' => 'Perfil',
        'profile_type' => 'Tipo de perfil',
        'person' => 'Pessoa',
        'reserved_area' => 'Area reservada',
        'users_reserved_area' => 'Utilizadores da Empresa têm acesso à Area reservada',
        'loading' => 'Enviar...',
        'select_profiles' => 'Selecionar Perfis',
        'empty_profiles' => 'Sem Perfis selecionados',
        'has_access_reserved_area' => 'Este Grupo tem acesso à area reservada',
        'has_not_access_reserved_area' => 'Este Grupo não tem acesso à area reservada',
        'description' => 'Descrição',
        'cancel' => 'Cancelar',
        'no_results' => 'Não foram encontrados resultados'
    ],
    'messages' => [
        'success' => [
            'create_personalized' => 'Campo personalizado criado com sucesso!',
            'create_profile' => 'Perfil criado com sucesso!',
            'update_profile' => 'Perfil editado com sucesso!',
            'create_group' => 'Grupo criado com sucesso!',
            'update_group' => 'Grupo atualizado com sucesso!',
            'update_personalized' => 'Campo personalizado atualizado com sucesso!',
            'delete_clients' => 'Perfil(s) apagado(s) com sucesso.',
            'delete_companies' => 'Empresa(s) apagada(s) com sucesso.',
            'delete_personalized' => 'Campo(s) personalizado(s) apagado(s) com sucesso.',
            'delete_groups' => 'Grupo(s) apagado(s) com sucesso.',
            'delete_users' => 'Utilizador(es) removido(s) com sucesso.',
            'restore_users' => 'Utilizador(es) restaurado(s) com sucesso.',
            'saved_profile' => 'Perfil guardado com sucesso.',
        ],
        'error' => [
            'existing_user' => 'Já existe um utilizador com o email especificado!',
            'delete_groups' => 'Não é possivel eliminar o grupo :name',
            'email_password_required' => 'Para um perfil de utilizador são necessários o email e password!'
        ],
        'email' => [
            'msg_account_email' => 'Email:',
            'msg_account_password' => 'Password:',
        ],
    ],

    'permission' => [
        'profiles' => [
            'see' => 'ver perfis',
            'create' => 'criar/editar perfis',
            'delete' => 'eliminar perfis',
        ],
        'personalized_fields'=>[
            'see' => 'ver campos personalizados',
            'create' => 'criar/editar campos personalizados',
            'delete' => 'eliminar campos personalizados',
        ]
    ],
];
