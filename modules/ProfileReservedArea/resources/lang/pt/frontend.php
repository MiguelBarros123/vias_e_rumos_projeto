<?php

return [
    'model' => [
        'index' => [
            //@lang('profiles::frontend.model.index.welcome')
            'welcome' => 'Welcome message on the model\'s index view frontend page',
        ],
    ],
    'messages' => [
        //@lang('profiles::frontend.messages.notifications.new-task')
        'notifications' => [
            'new-task' => 'You have a new pending task',
        ],
    ],
];
